Require Import List Bool.
Require Import ssreflect ssrbool ssrfun.

Require Import Tactics ListUtils.
Require Import SemPath Syntax.

Require Import Maps.

Import ListNotations.

Section ARGS_PARAMS.

Context {A: Type}.

Definition args_params (params: list ident) (args: list A)
                       : option (list (A * ident)) :=
  map2 pair args params.

Definition filter_args_by_perm (pe: penv) (p: permission)
                                 (pp: list (A * ident))
                                 : list (A * ident) :=
  filter (fun '(_, i) => match pe!i with
                         | Some p' => permission_beq p p'
                         | None    => false
                         end) pp.

Definition mut_args pe pp := filter_args_by_perm pe Mutable pp.

Definition shr_args pe pp := filter_args_by_perm pe Shared pp.

Definition own_args pe pp := filter_args_by_perm pe Owned pp.

Lemma filter_args_by_perm_incl:
  forall pe p pp, incl (filter_args_by_perm pe p pp) pp.
Proof.
  move=> pe p. elim/list_ind => // - [? i] pp IH /=. case: pe!i => [p'|].
  case: (perm_reflect p p') => _ > /=.
  - case=> [<-|/IH]. by left. by right.
  - move/IH. by right.
  - move=>> /IH. by right.
Qed.

Lemma mut_args_incl:
  forall pe pp, incl (mut_args pe pp) pp.
Proof. exact (fun pe => filter_args_by_perm_incl pe Mutable). Qed.

Lemma shr_args_incl:
  forall pe pp, incl (shr_args pe pp) pp.
Proof. exact (fun pe => filter_args_by_perm_incl pe Shared). Qed.

Lemma own_args_incl:
  forall pe pp, incl (own_args pe pp) pp.
Proof. exact (fun pe => filter_args_by_perm_incl pe Owned). Qed.

Lemma incl_pp_filter_penv fd:
  forall (args: list A) pp,
    args_params (fd_params fd) args = Some pp ->
    let mut := mut_args (fd_penv fd) pp in
    let shr := shr_args (fd_penv fd) pp in
    let own := own_args (fd_penv fd) pp in
    List.incl pp (mut ++ shr ++ own).
Proof.
  have: forall i, In i (fd_params fd) -> exists t, (fd_tenv fd)!i = Some t.
  { move=> i; have:= proj1 (fd_tenv_vars fd i); case fd =>> /= H ?; apply: H => //.
    apply in_or_app; by left. }
  elim: (fd_params fd) => /= [| > IH] H [] //=.
  - move=>> [<-] //.
  - move=>>; case AP: args_params =>> //= [<-] /= > /= [<-|].
    + have:= H _ (or_introl _ eq_refl) => /(fd_tenv_fd_penv fd) [p ->].
      apply in_or_app; case: p; [|left; by apply in_eq|]; right; apply in_or_app.
      left; by apply in_eq. right; by apply in_eq.
    + move/(IH (fun i X => H i (or_intror _ X)) _ _ AP).
      case/(in_app_or (mut_args _ _)); [|case/(in_app_or (shr_args _ _))] => I.
      * case (fd_penv fd)!_ => [[]|] /=. 2: right.
        all: apply in_or_app; now left.
      * case (fd_penv fd)!_ => [[]|] /=. 2: right.
        all: apply in_or_app; right => /=. right.
        all: apply in_or_app; now left.
      * case (fd_penv fd)!_ => [[]|] /=. 2: right.
        all: apply in_or_app; right => /=. right.
        all: apply in_or_app; right => /=. 3: right. all: assumption.
Qed.

Lemma args_params_nth_error_l:
  forall params (args: list A) pp n p i,
    args_params params args = Some pp ->
    nth_error pp n = Some (p, i) ->
    nth_error args n = Some p.
Proof.
  elim/list_ind => [|param params IH] [|p args] //=.
  - by move=> ? [|n] > [<-].
  - case PP: args_params => //.
    move=> ? [|n] > [<-] /=. by case=> <-. by apply: IH.
Qed.

Lemma args_params_nth_error_r:
  forall params (args: list A) pp n p i,
    args_params params args = Some pp ->
    nth_error pp n = Some (p, i) ->
    nth_error params n = Some i.
Proof.
  elim/list_ind => [|param params IH] [|p args] //=.
  - by move=> ? [|n] > [<-].
  - case PP: args_params => //.
    move=> ? [|n] > [<-] /=. by case=> _ <-. exact (IH _ _ _ _ _ PP).
Qed.

Lemma args_params_nth_error' {p i}:
  forall params (args: list A) pp n,
    args_params params args = Some pp ->
    nth_error args n = Some p ->
    nth_error params n = Some i ->
    nth_error pp n = Some (p, i).
Proof.
  elim/list_ind.
  - by move=> ++ [].
  - move=>> IH [|p' args] // ? [|n] > /=; case H: args_params => //=.
    + by move=> [<-] [<-] [<-].
    + move=> [<-]. exact (IH _ _ _ H).
Qed.

Lemma args_params_incl:
  forall params (args: list A) pp,
    args_params params args = Some pp ->
    List.incl (combine args params) pp.
Proof.
  induction params; intros [|p args]; try easy.
  simpl. case args_params eqn:H; try easy. simpl. intros * [= <-].
  intros x Hin. simpl in *. case Hin as [<-|Hin]. now left.
  right. now apply IHparams with (1 := H).
Qed.

Lemma args_params_nth_error_In:
  forall params (args: list A) pp j p i,
    args_params params args = Some pp ->
    nth_error args j = Some p ->
    nth_error params j = Some i ->
    In (p, i) pp.
Proof.
  induction params; intros [] ? []; try easy; simpl;
    case args_params eqn:H; try easy; intros * [= <-].
  - intros [= <-] [= <-]. now left.
  - intros H1 H2. right. now apply IHparams with (2 := H1).
Qed.

Lemma args_params_list_incl_l:
  forall params (args: list A) pp,
    args_params params args = Some pp ->
    incl (map fst pp) args.
Proof.
  elim/list_ind => /= [|p params IH] [|a args] // [] //=. easy. easy.
  move: (IH args). case: args_params => //=.
  move=> pp /(_ pp eq_refl) H > [<- <-] /=.
  apply: incl_cons. exact: in_eq. by apply: incl_tl.
Qed.

Lemma args_params_list_incl_r:
  forall params (args: list A) pp,
    args_params params args = Some pp ->
    incl (map snd pp) params.
Proof.
  elim/list_ind => /= [|p params IH] [|a args] // [] //=. easy. easy.
  move: (IH args). case: args_params => //=.
  move=> pp /(_ pp eq_refl) H > [<- <-] /=.
  apply: incl_cons. exact: in_eq. by apply: incl_tl.
Qed.

Lemma args_params_l:
  forall params (args: list A) pp,
    args_params params args = Some pp ->
    map fst pp = args.
Proof.
  elim/list_ind => //=.
  - case=>> //= - [<-] //.
  - move=>> IH [//|] > /=.
    case PP: args_params => //= - [<-] /=.
    by rewrite (IH _ _ PP).
Qed.

Lemma args_params_r:
  forall params (args: list A) pp,
    args_params params args = Some pp ->
    map snd pp = params.
Proof.
  elim/list_ind => //=.
  - case=>> //= - [<-] //.
  - move=>> IH [//|] > /=.
    case PP: args_params => //= - [<-] /=.
    by rewrite (IH _ _ PP).
Qed.

End ARGS_PARAMS.

Section ARGS_ALIASING.

Context {A: Type}.
Variable P : A -> A -> bool.

Fixpoint arg_aliasing (arg: A) (args: list A) :=
  match args with
  | [] => false
  | arg' :: args =>
      if P arg arg' then mem P arg args
      else arg_aliasing arg args
  end.

Fixpoint args_aliasing (args: list A) (pp: list (A * ident)) : bool :=
  match pp with
  | [] => false
  | (arg, _) :: pp =>
      arg_aliasing arg args || args_aliasing args pp
  end.

Lemma arg_aliasing_spec:
  forall arg args,
    arg_aliasing arg args <->
    exists x y p1 p2, x <> y /\
           nth_error args x = Some p1 /\
           nth_error args y = Some p2 /\
           P arg p1 /\
           P arg p2.
Proof.
  move=> arg. elim/list_ind => /=.
  - split => //. case=> - [] > [>] [>] [>]; easy.
  - move=> arg' l IH. case Harg': P; split.
    + case/mem_nth_error => n [p] [Hnth Halias]. by exists 0, (S n), arg', p => /=.
    + case=> - [|x] [+] [p1] [p2] /=; [case=> [|y]|move=> y] => - [] //= _.
      move=> [_] [Hnth] [_] Halias. apply mem_nth_error. by exists y, p2.
      move=> [Hnth] [_] [Halias] _. apply mem_nth_error. by exists x, p1.
    + move/IH => [x] [y] [p1] [p2] [?] ?. exists (S x), (S y), p1, p2. eauto.
    + move=> H. apply: (proj2 IH).
      case: H => - [|x] [[|y]] /= [p1] [p2] [?] [?] [?] [?] ?; try congruence.
      exists x, y, p1, p2. auto.
Qed.

Lemma args_aliasing_spec (args: list A) (pp: list (A * ident)):
  incl (map fst pp) args ->
  args_aliasing args pp = true <->
  exists x y z p1 p2 p id, x <> y /\
                      nth_error args x = Some p1 /\
                      nth_error args y = Some p2 /\
                      nth_error pp z = Some (p, id) /\
                      P p p1 /\ P p p2.
Proof.
  move: args. elim/list_ind: pp => /=.
  - move=>> _. split => // - [?] [?] [[|?]] [?] [?] [?] [?] /=; easy.
  - move=> [p id] marrs IH args /= Hincl.
    have [Hin Hincl'] := incl_cons_inv Hincl => {Hincl}. split.
    + case Hc: arg_aliasing => /=.
      move: Hc => /arg_aliasing_spec [x] [y] [p1] [p2] H _.
      exists x, y, 0, p1, p2, p, id => /=. tauto.
      move/(IH _ Hincl') => [x] [y] [z] [p1] [p2] [p'] [id'].
      by exists x, y, (S z), p1, p2, p', id'.
    + case Harg: arg_aliasing => //.
      case=> x [y] [[|z]] [p1] [p2] [p'] [id'] /=.
      * move=> [?] [?] [?] [[<- _]] [?] ?.
        exfalso. apply: (eq_true_false_abs _ _ Harg).
        apply: (proj2 (arg_aliasing_spec _ _)). by exists x, y, p1, p2.
      * move=> H. apply: (proj2 (IH _ Hincl')). by exists x, y, z, p1, p2, p', id'.
Qed.

End ARGS_ALIASING.


Section ARGS_ALIASING_IMPL.

Context {A B: Type}.
Variable P: A -> A -> bool.
Variable Q: B -> B -> bool.
Variable R: A -> B -> Prop.

Hypothesis Hab: forall a a' b b', R a b -> R a' b' -> P a a' -> Q b b'.

Lemma arg_aliasing_impl:
  forall la lb a b,
    R a b ->
    Forall2 R la lb ->
    arg_aliasing P a la ->
    arg_aliasing Q b lb.
Proof.
  move=> la lb a b H Hl X.
  have [x [y [p1 [p2 [NEQ [N1 [N2 [P1 P2]]]]]]]] := proj1 (arg_aliasing_spec _ _ _) X.
  apply: (proj2 (arg_aliasing_spec _ _ _)).
  have [b1 [N1' P1']] := Forall2_nth_error _ _ _ Hl _ _ N1.
  have [b2 [N2' P2']] := Forall2_nth_error _ _ _ Hl _ _ N2.
  have Q1 := Hab _ _ _ _ H P1' P1. have Q2 := Hab _ _ _ _ H P2' P2. eauto 10.
Qed.

Lemma args_aliasing_impl:
  forall la lb ppa ppb,
    Forall2 R la lb ->
    Forall2 R (map fst ppa) (map fst ppb) ->
    incl (map fst ppa) la ->
    incl (map fst ppb) lb ->
    args_aliasing P la ppa ->
    args_aliasing Q lb ppb.
Proof.
  move=> la lb ppa ppb Hl Hpp INCLa INCLb X.
  have [x [y [z [p1 [p2 [p [i [NEQ [N1 [N2 [N [P1 P2]]]]]]]]]]]] :=
    proj1 (args_aliasing_spec _ _ _ INCLa) X.
  apply: (proj2 (args_aliasing_spec _ _ _ INCLb)).
  have [b1 [N1' P1']] := Forall2_nth_error _ _ _ Hl _ _ N1.
  have [b2 [N2' P2']] := Forall2_nth_error _ _ _ Hl _ _ N2.
  have:= map_nth_error fst _ _ N => /= N'.
  have [p' [M H]] := Forall2_nth_error _ _ _ Hpp _ _ N'.
  have Q1 := Hab _ _ _ _ H P1' P1. have Q2 := Hab _ _ _ _ H P2' P2.
  have:= nth_error_map fst z ppb. rewrite M.
  case M': nth_error => [[]|] //= [T]. rewrite -T {T} in M'.
  repeat eexists; eassumption.
Qed.

End ARGS_ALIASING_IMPL.
