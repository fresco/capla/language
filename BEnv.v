Require Import Coq.Strings.String.
Require Import Coq.Lists.List.
Require Import BinNums BinInt BinPos.

Import ListNotations.

Require Import Types Ops BValues.
Require Import SemPath Syntax BUtils ListUtils.

Require Import Integers Floats Maps Errors.

Definition env := PTree.t value.
Definition genv := PTree.t fundef.

Fixpoint sub_map' {A: Type} (m m': PTree.t A) (l: list positive) : PTree.t A :=
  match l with
  | [] => m'
  | p :: l =>
      match m!p with
      | None => sub_map' m m' l
      | Some a => sub_map' m (PTree.set p a m') l
      end
  end.

Definition sub_map {A: Type} (m: PTree.t A) (l: list positive) := sub_map' m (PTree.empty A) l.


Fixpoint build_func_env (e: env)
                        (params: list ident)
                        (vargs: list value)
  : option env :=
  match params, vargs with
  | [], [] => Some e
  | id :: params, v :: vargs =>
      match build_func_env e params vargs with
      | None => None
      | Some e => Some (PTree.set id v e)
      end
  | _, _ => None
  end.

Definition build_env {A: Type} (env: PTree.t A) (l: list (ident * A)) : PTree.t A :=
  fold_left (fun env x => PTree.set (fst x) (snd x) env) l env.

Lemma build_func_env_not_Vundef:
  forall params vargs e e' i v,
    build_func_env e params vargs = Some e' ->
    e'!i = Some v ->
    v <> Vundef ->
    e!i = Some v \/ exists n, nth_error params n = Some i.
Proof.
  induction params; intros []; try easy.
  - intros * [= <-]. eauto.
  - intros *. simpl. case build_func_env eqn:H; try easy. intros [= <-].
    case (Pos.eqb_spec i a) as [<-|Hneq].
    + intros _ _. right. now exists 0%nat.
    + rewrite PTree.gso by easy. intros He Hv0.
      case (IHparams _ _ _ _ _ H He Hv0) as [T|[n T]].
      now left. right. now exists (S n).
Qed.

Theorem build_func_env_spec:
  forall params vargs e e',
    Coqlib.list_norepet params ->
    build_func_env e params vargs = Some e' ->
    forall n i v,
      nth_error params n = Some i ->
      nth_error vargs n = Some v ->
      e'!i = Some v.
Proof.
  induction params; intros []; try easy.
  - now intros * _ [= <-] [].
  - intros *. simpl. case build_func_env eqn:H; try easy.
    inversion 1; subst. intros [= <-]. intros [|n] *.
    + intros [= ->] [= <-]. now rewrite PTree.gss.
    + simpl. case (Pos.eqb_spec i a) as [->|].
      intro H'. now apply nth_error_In in H'.
      rewrite PTree.gso by easy. now apply IHparams with (2 := H).
Qed.

Lemma list_in_diff {A: Type} (l: list A):
  forall x y, In x l ->
              ~In y l ->
              x <> y.
Proof.
  induction l; intros.
  + contradiction.
  + simpl in H, H0. apply Decidable.not_or in H0. destruct H0. destruct H.
    - rewrite H in H0. assumption.
    - eauto.
Qed.

Theorem build_func_env_spec':
  forall params vargs e e' i v,
    build_func_env e params vargs = Some e' ->
    e'!i = Some v ->
    e!i = Some v \/ exists n, nth_error params n = Some i /\
                         nth_error vargs  n = Some v.
Proof.
  induction params; intros [|v' vargs] e e' i v; try easy.
  - intros [= <-] ->. now left.
  - simpl. case build_func_env eqn:B; try easy. intros [= <-].
    case (Pos.eqb_spec i a) as [->|NEQ].
    + rewrite PTree.gss. intros [= <-]. right. now exists 0%nat.
    + rewrite PTree.gso by easy. intro Hi.
      case (IHparams _ _ _ _ _ B Hi) as [->|[n H]]. now left. right. now exists (S n).
Qed.

Theorem build_env_correct {A: Type} (l: list (ident * A)):
  Coqlib.list_norepet (map fst l) ->
  forall e,
    (forall id, In id (map fst l) -> e!id = None) ->
    forall x, (In x (PTree.elements e) \/ In x l) <-> In x (PTree.elements (build_env e l)).
Proof.
  induction l; cbn; intros.
  + tauto.
  + destruct a as [id a].
    simpl in H. inversion_clear H.
    specialize IHl with (1 := H2). split; intro.
    - apply IHl; intros.
      * pose proof (list_in_diff (map fst l) _ _ H3 H1).
        rewrite PTree.gsspec. rewrite Coqlib.peq_false; eauto.
      * destruct H; try destruct H.
        ** destruct x as [id0 a0]. left.
           apply PTree.elements_complete in H. specialize (H0 id0). simpl in H0.
           apply PTree.elements_correct. rewrite PTree.gsspec.
           case (Pos.eq_dec id id0); intro H'.
           *** apply or_introl with (B := In id0 (map fst l)) in H'.
               apply H0 in H'. rewrite H' in H. discriminate.
           *** rewrite Coqlib.peq_false; eauto.
        ** revert H; intros [= <-].
           left. apply PTree.elements_correct. rewrite PTree.gsspec.
           rewrite Coqlib.peq_true. reflexivity.
        ** eauto.
    - apply IHl in H; intros.
      * destruct x as [id0 a0]. destruct H.
        ** apply PTree.elements_complete in H. rewrite PTree.gsspec in H.
           case (Pos.eq_dec id0 id); intro H'.
           *** revert H'; intros [= <-]. rewrite Coqlib.peq_true in H.
               revert H; intros [= <-]. eauto.
           *** rewrite Coqlib.peq_false with (1 := H') in H.
               left. apply PTree.elements_correct. assumption.
        ** eauto.
      * pose proof (list_in_diff (map fst l) _ _ H4 H1).
        rewrite PTree.gsspec. rewrite Coqlib.peq_false; eauto.
Qed.

Corollary build_env_correct_empty {A: Type} (l: list (ident * A)):
  Coqlib.list_norepet (map fst l) ->
  forall x, (In x l <-> In x (PTree.elements (build_env (PTree.empty A) l))).
Proof.
  intros.
  pose proof (build_env_correct _ H _ (fun id _ => PTree.gempty A id) x).
  simpl in H0. tauto.
Qed.

Definition genv_of_program (p: program) : genv :=
  build_env (PTree.empty fundef) p.(prog_defs).

Definition set_optenv (e: env) (optid: option ident) (v: value) :=
  match optid with
  | None    => e
  | Some id => PTree.set id v e
  end.

Local Open Scope option_bool_monad_scope.

Fixpoint get_value_path (pl: list sem_path_elem) (v: value) : option value :=
  match pl with
  | [] => Some v
  | Pcell idx :: pl =>
      match v with
      | Varr lv =>
          doo v <- nth_error lv idx;
          get_value_path pl v
      | _ => None
      end
  end.

Definition get_env_path (e: env) (p: sem_path) :=
  let (id, pl) := p in
  doo v <- e!id;
  get_value_path pl v.

Fixpoint get_env_path_list (e: env) (lp: list sem_path) :=
  match lp with
  | [] => Some []
  | p :: lp =>
      doo v  <- get_env_path e p;
      doo lv <- get_env_path_list e lp;
      Some (v :: lv)
  end.