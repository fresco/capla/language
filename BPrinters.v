Require Import String.

Require Import Types SemPath.

Parameter string_of_type: Types.typ -> string.
Parameter string_of_fct_ident: SemPath.ident -> string.
Parameter string_of_var_ident: SemPath.ident -> string.
