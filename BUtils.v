Require Import String.
Require Import Coq.Lists.List Coq.Lists.ListDec.
Require Import Coq.Bool.Bool.
Require Import Lia.
Require Import ZArith.
Require Import Eqdep_dec.
Require Import ssreflect ssrbool ssrfun.

Import ListNotations.

Require Import Integers Maps.
Require Import Errors.

Require Import Tactics.

Lemma proj2_3: forall {A B C}, A /\ B /\ C -> B.
Proof. intros * [_ [X _]]. exact X. Qed.

Lemma proj3_3: forall {A B C}, A /\ B /\ C -> C.
Proof. intros * [_ [_ X]]. exact X. Qed.

Definition option_get {A: Type} (o: option A) (default: A) :=
  match o with
  | None => default
  | Some a => a
  end.

Lemma option_not_None_exists {A: Type} (x: option A):
  x <> None -> exists y, x = Some y.
Proof.
  intros. induction x.
  - exists a. reflexivity.
  - exfalso. contradiction.
Qed.

Fixpoint replace_nth {A: Type} (l: list A) (n: nat) (x: A) : option (list A) :=
  match l with
  | [] => None
  | y :: l =>
      match n with
      | 0%nat => Some (x :: l)
      | S n => option_map (fun l => y :: l) (replace_nth l n x)
      end
  end.

Lemma replace_nth_Some1 {A: Type} (n: nat) (x: A):
  forall (l: list A),
    replace_nth l n x <> None -> n < length l.
Proof.
  + induction n, l.
    - intro. simpl in H. contradiction.
    - intro. simpl. lia.
    - intro. cbn in H. contradiction.
    - intro. cbn in H.
      unfold option_map in H.
      destruct (replace_nth l n x) eqn:H0; try contradiction.
      cbn. apply (Nat.succ_lt_mono n).
      specialize (IHn l). apply IHn.
      intro. rewrite H0 in H1. discriminate.
Qed.

Lemma replace_nth_Some2 {A: Type} (n: nat) (x: A):
  forall (l: list A),
    n < length l -> replace_nth l n x <> None.
Proof.
  + induction n, l.
    - intro. simpl in H. lia.
    - intro. simpl. discriminate.
    - intro. cbn in H. lia.
    - intro. cbn in H. apply Nat.succ_lt_mono in H.
      specialize (IHn l H). simpl.
      destruct (replace_nth l n x); try easy.
Qed.

Lemma replace_nth_Some {A: Type} (l: list A) (n: nat) (x: A):
  replace_nth l n x <> None <-> n < length l.
Proof.
  split. apply replace_nth_Some1. apply replace_nth_Some2.
Qed.

Lemma replace_nth_None1 {A: Type} (n: nat) (x: A):
  forall (l: list A),
    replace_nth l n x = None -> length l <= n.
Proof.
  + induction n, l.
    - intro. auto.
    - intro. simpl in H. discriminate.
    - intro. simpl. lia.
    - intro. simpl in H.
      unfold option_map in H.
      destruct (replace_nth l n x) eqn:H0; try discriminate.
      cbn. apply (Nat.succ_le_mono _ n).
      specialize (IHn l H0). assumption.
Qed.

Lemma replace_nth_None2 {A: Type} (n: nat) (x: A):
  forall (l: list A),
    length l <= n -> replace_nth l n x = None.
Proof.
  + induction n, l.
    - intro. auto.
    - intro. simpl in H. lia.
    - intro. auto.
    - intro. simpl in H. apply Nat.succ_le_mono in H.
      specialize (IHn l H). simpl.
      destruct (replace_nth l n x); try easy.
Qed.

Lemma replace_nth_None {A: Type} (l: list A) (n: nat) (x: A):
  replace_nth l n x = None <-> length l <= n.
Proof.
  split. apply replace_nth_None1. apply replace_nth_None2.
Qed.

Lemma replace_nth_length {A: Type}:
  forall (l: list A) (n: nat) (x: A) (l': list A),
    replace_nth l n x = Some l' ->
    length l = length l'.
Proof.
  induction l; simpl; intros. easy.
  destruct n. revert H; intros [= <-]. easy.
  destruct replace_nth eqn:Hr; try discriminate. revert H; intros [= <-]. simpl. eauto.
Qed.

Lemma replace_nth_nth_error_same {A: Type}:
  forall (l l': list A) (n: nat) (x: A),
    replace_nth l n x = Some l' ->
    nth_error l' n = Some x.
Proof.
  induction l; simpl; intros. easy.
  destruct n. revert H; intros [= <-]. easy.
  destruct replace_nth eqn:Hr; try discriminate. revert H; intros [= <-]. simpl. eauto.
Qed.

Lemma replace_nth_nth_error_other {A: Type}:
  forall (l l': list A) (n: nat) (x: A),
    replace_nth l n x = Some l' ->
    forall n',
      n <> n' ->
      nth_error l' n' = nth_error l n'.
Proof.
  induction l; simpl; intros. easy.
  destruct n. revert H; intros [= <-]. destruct n'. contradiction. reflexivity.
  destruct replace_nth eqn:Hr; try discriminate. revert H; intros [= <-].
  destruct n'. reflexivity. destruct (PeanoNat.Nat.eq_dec n n').
  apply eq_S in e. contradiction. simpl. eauto.
Qed.

Lemma nth_error_replace_nth {A: Type}:
  forall (l: list A) (n: nat) (x x': A),
    nth_error l n = Some x ->
    exists l', replace_nth l n x' = Some l'.
Proof.
  elim/list_ind => /=. by case.
  move=> a l IH [|n] x x' /=. eauto.
  move/(IH _ x x') => [> ->] /=. eauto.
Qed.

Lemma replace_nth_In_same {A: Type}:
  forall (l l': list A) (n: nat) (x: A),
    replace_nth l n x = Some l' ->
    In x l'.
Proof.
  induction l; simpl; intros. discriminate.
  destruct n. revert H; intros [= <-]. exact (in_eq _ _).
  destruct (replace_nth l n x) eqn:H'; try discriminate. simpl in H.
  revert H; intros [= <-]. right. apply IHl with (1 := H').
Qed.

Lemma in_map_fst_pair {A B: Type} (l: list (A * B)) (x: A):
  In x (map fst l) -> exists y, In (x, y) l.
Proof.
  induction l; cbn; intros.
  + contradiction.
  + destruct H.
    exists (snd a). rewrite <- H. eauto using surjective_pairing.
    destruct (IHl H) as [y Hy]. eauto.
Qed.

Lemma in_map_snd_pair {A B: Type} (l: list (A * B)) (y: B):
  In y (map snd l) -> exists x, In (x, y) l.
Proof.
  induction l; cbn; intros.
  + contradiction.
  + destruct H.
    exists (fst a). rewrite <- H. eauto using surjective_pairing.
    destruct (IHl H) as [x Hx]. eauto.
Qed.

Lemma in_map_pair_fst {A B: Type} (l: list (A * B)) (x: A) (y: B):
  In (x, y) l -> In x (map fst l).
Proof.
  induction l; cbn. eauto. intros. destruct H. rewrite H. eauto. eauto.
Qed.

Lemma in_map_pair_snd {A B: Type} (l: list (A * B)) (x: A) (y: B):
  In (x, y) l -> In y (map snd l).
Proof.
  induction l; cbn. eauto. intros. destruct H. rewrite H. eauto. eauto.
Qed.


Fixpoint list_assoc_pos {A: Type} (l: list (positive * A)) (x: positive) : option A :=
  match l with
  | [] => None
  | (k, v) :: l =>
      if (k =? x)%positive then Some v
      else list_assoc_pos l x
  end.

Lemma list_assoc_pos_Some:
  forall A l x (y : A),
    list_assoc_pos l x = Some y -> In (x, y) l.
Proof.
  induction l; intros.
  + easy.
  + simpl. simpl in H. destruct a as [k v].
    case ((k =? x)%positive) eqn:Heq in H.
    - apply Peqb_true_eq in Heq. revert Heq H; intros [= ->] [= ->]. left. reflexivity.
    - eauto.
Qed.

Lemma list_assoc_pos_Some2:
  forall A l x (y: A),
    In (x, y) l -> exists z, list_assoc_pos l x = Some z.
Proof.
  induction l; cbn; intros.
  + easy.
  + destruct a as [k v]. destruct H.
    - inversion_clear H. rewrite Pos.eqb_refl. eexists. reflexivity.
    - specialize (IHl _ _ H). destruct IHl as [z Hz].
      case ((k =? x)%positive); eexists; eauto.
Qed.

Lemma list_assoc_pos_Some_norepet_fst {A: Type} (l: list (positive * A)):
  forall x y,
    Coqlib.list_norepet (map fst l) ->
    In (x, y) l ->
    list_assoc_pos l x = Some y.
Proof.
  induction l; simpl; intros.
  + contradiction.
  + destruct a. destruct H0.
    - inversion_clear H0. rewrite Pos.eqb_refl. reflexivity.
    - inversion_clear H. destruct ((p =? x)%positive) eqn:Heq.
      * apply Peqb_true_eq in Heq. rewrite Heq in H1.
        apply in_map_pair_fst in H0. exfalso. exact (H1 H0).
      * eauto.
Qed.

Lemma Coqlib_list_norepet_fst_injective {A: Type} (l: list (positive * A)):
  forall x y y',
    Coqlib.list_norepet (map fst l) ->
    In (x, y) l ->
    In (x, y') l ->
    y = y'.
Proof.
  intros. apply list_assoc_pos_Some_norepet_fst in H0, H1; try eassumption.
  revert H0 H1; intros [= ->] [= <-]. reflexivity.
Qed.

Lemma Coqlib_list_norepet_snd_injective {A: Type} (l: list (A * positive)):
  forall x x' y,
    Coqlib.list_norepet (map snd l) ->
    In (x,  y) l ->
    In (x', y) l ->
    x = x'.
Proof.
  intros. set (f := fun (A B: Type) (x: A * B) => let (x, y) := x in (y, x)).
  apply in_map with (f := f A positive) in H0, H1. simpl in H0, H1.
  assert (map snd l = map fst (map (f A positive) l)) as X.
  { rewrite map_map; apply nth_error_ext =>>.
    rewrite !nth_error_map; case nth_error=> [[]|] //. }
  rewrite X in H.
  by apply Coqlib_list_norepet_fst_injective with (x := y) (1 := H).
Qed.

Local Open Scope bool_scope.

Declare Scope option_error_monad_scope.

Declare Scope option_bool_monad_scope.

Definition option_bind {A B: Type} (o: option A) (f: A -> option B) : option B :=
  match o with
  | Some x => f x
  | None => None
  end.

Definition option_bool_bind {A: Type} (o: option A) (f: A -> bool) : bool :=
  match o with
  | Some x => f x
  | None => false
  end.

Definition option_res_bind {A B: Type} (o: option A) (f: A -> res B) : res B :=
  match o with
  | Some x => f x
  | None => Error [ MSG "" ]
  end.

Definition option_res_custom_msg_bind {A B: Type} (msg: string)
                                      (o: option A) (f: A -> res B) : res B :=
  match o with
  | Some x => f x
  | None => Error [ MSG msg ]
  end.

Notation "'doo' X <- A ; B" :=
  (option_bind A (fun X => B))
    (at level 200, X name, A at level 100, B at level 200) : option_bool_monad_scope.

Notation "'dob' X <- A ; B" :=
  (option_bool_bind A (fun X => B))
    (at level 200, X name, A at level 100, B at level 200) : option_bool_monad_scope.

Notation "'do*' X <- A ; B" :=
  (option_res_bind A (fun X => B))
    (at level 200, X name, A at level 100, B at level 200) : option_error_monad_scope.

Notation "'do**' X <- [ M ] A ; B" :=
  (option_res_custom_msg_bind M A (fun X => B))
    (at level 200, X name, M at level 100, A at level 100, B at level 200)
    : option_error_monad_scope.

Notation "'dop' ( X , Y ) <- A ; B" :=
  (bind A (fun '(X, Y) => B))
  (at level 200, X name, Y name, A at level 100, B at level 200) : error_monad_scope.

Local Open Scope option_error_monad_scope.
Local Open Scope error_monad_scope.

Fixpoint list_map_error {A B: Type} (f: A -> res B) (l: list A) : res (list B) :=
  match l with
  | [] => OK []
  | x :: l =>
      do y  <- f x;
      do l' <- list_map_error f l;
      OK (y :: l')
  end.

Lemma in_list_map_error {A B: Type} (f: A -> res B) (l1: list A) (l2: list B) (x: A):
  list_map_error f l1 = OK l2 ->
  In x l1 -> exists y, f x = OK y /\ In y l2.
Proof.
  intros. apply in_split in H0. destruct H0 as [u1 [u2 H0]].
  revert H0; intros [= ->]. revert l2 H. induction u1; simpl; intros.
  + destruct f eqn:Hf, list_map_error; try discriminate.
    revert H; intros [= <-]. simpl. eexists. tauto.
  + destruct (f a), list_map_error eqn:H0; try discriminate.
    revert H; intros [= <-]. specialize (IHu1 _ eq_refl).
    destruct IHu1 as [y [H1 H2]]. eexists. simpl. eauto.
Qed.

Lemma in_list_map_error_iff {A B: Type} (f: A -> res B) (l1: list A) (l2: list B) (y: B):
  list_map_error f l1 = OK l2 ->
  In y l2 <-> exists x, f x = OK y /\ In x l1.
Proof.
  split; intro.
  + apply in_split in H0. destruct H0 as [u1 [u2 [= ->]]].
    revert l1 H. induction u1; simpl; intros.
    - destruct l1; try discriminate.
      simpl in H. destruct f eqn:Hf, list_map_error; try discriminate.
      revert H; intros [= <-]. exists a. split; simpl; tauto.
    - destruct l1; try discriminate.
      simpl in H. destruct f, list_map_error eqn:H'; try discriminate.
      revert H; intros [= <-]. revert H0; intros [= ->].
      destruct (IHu1 _ H') as [x [H1 H2]]. exists x. split; simpl; tauto.
  + destruct H0 as [x [H1 H2]]. apply in_split in H2.
    destruct H2 as [u1 [u2 [= ->]]].
    revert l2 H. induction u1; simpl; intros.
    - destruct f eqn:Hf, list_map_error; try discriminate.
      revert H; intros [= <-]. revert H1; intros [= <-]. simpl. tauto.
    - destruct (f a), list_map_error eqn:H'; try discriminate.
      revert H; intros [= <-]. simpl. eauto.
Qed.

Lemma length_succ_cons {A: Type}:
  forall (l: list A) n,
    S n = length l <-> exists x l', l = x :: l' /\ n = length l'.
Proof.
  intros. split; intros.
  + destruct l. discriminate. exists a, l. split; eauto.
  + destruct H as [x [l' [H1 H2]]]. rewrite H1 H2. reflexivity.
Qed.

Theorem in_split_last_length {A: Type}:
  (forall x y: A, {x = y} + {x <> y}) ->
  forall x (n: nat) (l: list A),
    n = length l -> In x l -> exists l1 l2, l = l1 ++ x :: l2 /\ ~ In x l2.
Proof.
  intros Hdec x.
  change (forall (n: nat),
             (fun n => forall l,
                  n = Datatypes.length l ->
                  In x l ->
                  exists l1 l2, l = l1 ++ x :: l2 /\ ~ In x l2) n).
  apply (well_founded_induction Wf_nat.lt_wf). intros.
  destruct x0.
  + apply eq_sym in H0. apply length_zero_iff_nil in H0.
    rewrite H0 in H1. contradiction.
  + apply length_succ_cons in H0. destruct H0 as [a [l' [[= ->] H0]]].
    specialize (H x0 (PeanoNat.Nat.lt_succ_diag_r x0) l' H0).
    destruct H1. revert H1; intros [= ->].
    destruct (In_dec Hdec x l').
    - specialize (H i). destruct H as [l1 [l2 [[= ->] H]]].
      exists (x :: l1), l2. eauto.
    - exists [], l'. eauto.
    - specialize (H H1). destruct H as [l1 [l2 [[= ->] H]]].
      exists (a :: l1), l2. eauto.
Qed.

Theorem in_split_last {A: Type}:
  (forall x y: A, {x = y} + {x <> y}) ->
  forall x (l: list A),
    In x l -> exists l1 l2, l = l1 ++ x :: l2 /\ ~ In x l2.
Proof.
  intros Heqdec x l Hin.
  exact (in_split_last_length Heqdec x _ l eq_refl Hin).
Qed.

Local Close Scope error_monad_scope.

Lemma option_None_not_Some:
  forall A x,
    x = None <-> forall (y: A), x <> Some y.
Proof.
  split; intros.
  + intro. rewrite H0 in H. discriminate.
  + destruct x. specialize (H a). contradiction. reflexivity.
Qed.

Lemma option_None_not_exists:
  forall A x,
    x = None -> ~ exists (y: A), x = Some y.
Proof. intros A x H H0. now destruct H0 as [y ->]. Qed.

Fixpoint index_of_rec {A: Type} (eqb: A -> A -> bool)
                                (l: list A) (x: A) (i: nat) : option nat :=
  match l with
  | [] => None
  | y :: l => if eqb x y then Some i
              else index_of_rec eqb l x (i + 1)
  end.

Definition index_of {A: Type} (eqb: A -> A -> bool)
                              (l: list A) (x: A) : option nat :=
  index_of_rec eqb l x 0.

Lemma index_of_rec_S_Some_0 {A: Type} (eqb: A -> A -> bool):
  forall (l: list A) x i,
    ~(index_of_rec eqb l x (S i) = Some 0).
Proof. induction l; simpl; intros; [| destruct eqb]; easy. Qed.

Lemma index_of_rec_le_Some {A: Type} (eqb: A -> A -> bool):
  forall (l: list A) x n i,
    index_of_rec eqb l x n = Some i ->
    n <= i.
Proof.
  induction l; simpl; intros.
  + discriminate.
  + destruct (eqb x a).
    revert H; intros [= <-]. easy.
    specialize (IHl _ _ _ H). lia.
Qed.

Lemma index_of_nth_error {A: Type} (eqb: A -> A -> bool)
                                   (Heq: forall x y, reflect (x = y) (eqb x y)):
  forall i (l: list A) x,
    index_of eqb l x = Some i ->
    nth_error l i = Some x.
Proof.
  unfold index_of. intros until x. rewrite <- (Nat.sub_0_r i) at 2.
  generalize 0. revert l i. induction l; simpl; intros i n.
  + discriminate.
  + unfold index_of. case (Heq x a) as [->|Hneq].
    intros [= <-]. rewrite PeanoNat.Nat.sub_diag. reflexivity.
    intros H0. destruct i.
    - destruct l. discriminate.
      rewrite PeanoNat.Nat.add_1_r in H0.
      apply index_of_rec_S_Some_0 in H0. contradiction.
    - specialize (IHl _ _ H0). rewrite PeanoNat.Nat.add_1_r in IHl. simpl in IHl.
      apply index_of_rec_le_Some in H0.
      rewrite -> Nat.sub_succ_l. eauto. lia.
Qed.

Lemma index_of_None {A: Type} (eqb: A -> A -> bool)
                              (Heq: forall x y, reflect (x = y) (eqb x y)):
  forall (l: list A) x,
    index_of eqb l x = None ->
    ~ In x l.
Proof.
  unfold index_of. intros *. generalize 0.
  induction l. easy. simpl. intro n.
  case (Heq x a) as [|Hneq]. easy. intro H. apply IHl in H.
  intros []; congruence.
Qed.

Lemma index_of_In  {A: Type} (eqb: A -> A -> bool)
                             (Heq: forall x y, reflect (x = y) (eqb x y)):
  forall l x i,
    index_of eqb l x = Some i ->
    In x l.
Proof.
  unfold index_of. intros *. generalize 0.
  revert i. induction l; simpl. discriminate.
  case (Heq x a) as [<-|Hneq]. auto.
  intros * H. apply IHl in H. auto.
Qed.

Lemma index_of_rec_S {A: Type} (eqb: A -> A -> bool)
                               (Heq: forall x y, reflect (x = y) (eqb x y)):
  forall (l: list A) (n i: nat) (x: A),
    index_of_rec eqb l x n = Some i ->
    index_of_rec eqb l x (S n) = Some (S i).
Proof.
  induction l; simpl; intros. discriminate.
  revert H. case (Heq x a) as [->|Hneq].
  + intros [= <-]. reflexivity.
  + eauto.
Qed.

Lemma nth_error_index_of {A: Type} (eqb: A -> A -> bool)
                                   (Heq: forall x y, reflect (x = y) (eqb x y)):
  forall (l: list A) (i: nat) (x: A),
    Coqlib.list_norepet l ->
    nth_error l i = Some x ->
    index_of eqb l x = Some i.
Proof.
  induction l; simpl; intros.
  + rewrite Coqlib.nth_error_nil in H0. discriminate.
  + cbn. inversion_clear H. case (Heq x a) as [->|Hneq].
    - destruct i. reflexivity. cbn in H0.
      apply nth_error_In in H0. contradiction.
    - destruct i.
      * apply eq_sym in H0. injection H0. intro. contradiction.
      * specialize (IHl _ _ H2 H0).
        apply index_of_rec_S with (1 := Heq). assumption.
Qed.

Lemma Coqlib_norepet_nth_error_injective:
  forall {A: Type} (l: list A) (i j: nat) (x: A),
    Coqlib.list_norepet l ->
    nth_error l i = Some x ->
    nth_error l j = Some x ->
    i = j.
Proof.
  induction l; simpl; intros. destruct i; discriminate.
  inversion_clear H. destruct i, j; eauto; simpl in *.
  injection H0; intros [= ->]. now apply Coqlib.nth_error_in in H1.
  injection H1; intros [= ->]. now apply Coqlib.nth_error_in in H0.
Qed.

Lemma Zcomparison_eq_dec:
  forall x y: Datatypes.comparison, {x = y} + {x <> y}.
Proof. decide equality. Qed.

Lemma Z_lt_eq_proofs:
  forall (n m : Z) (x y: (n < m)%Z), x = y.
Proof. intros. unfold Z.lt in x, y. exact (UIP_dec Zcomparison_eq_dec x y). Qed.

Lemma Int64_neq_unsigned (x y: int64):
  x <> y ->
  Int64.unsigned x <> Int64.unsigned y.
Proof.
  intros H H0. apply H.
  destruct x, y. simpl in *. revert H0; intros [= <-].
  destruct intrange as [low high], intrange0 as [low0 high0].
  now rewrite (Z_lt_eq_proofs _ _ low low0) (Z_lt_eq_proofs _ _ high high0).
Qed.

Lemma Int64_neq_lt_or_gt (x y: int64):
  x <> y ->
  Int64.ltu x y = true \/ Int64.ltu y x = true.
Proof.
  intro. apply Int64_neq_unsigned, Z.lt_gt_cases in H.
  destruct H; unfold Int64.ltu; rewrite -> Coqlib.zlt_true with (1 := H); eauto.
Qed.

Lemma Ptrofs_neq_unsigned (x y: ptrofs):
  x <> y ->
  Ptrofs.unsigned x <> Ptrofs.unsigned y.
Proof.
  intros H H0. apply H.
  destruct x, y. simpl in *. revert H0; intros [= <-].
  destruct intrange as [low high], intrange0 as [low0 high0].
  now rewrite (Z_lt_eq_proofs _ _ low low0) (Z_lt_eq_proofs _ _ high high0).
Qed.

Lemma Ptrofs_neq_lt_or_gt (x y: ptrofs):
  x <> y ->
  Ptrofs.ltu x y = true \/ Ptrofs.ltu y x = true.
Proof.
  intro. apply Ptrofs_neq_unsigned, Z.lt_gt_cases in H.
  destruct H; unfold Ptrofs.ltu; rewrite -> Coqlib.zlt_true with (1 := H); eauto.
Qed.

Lemma Int64_Int_modulus:
  (Int.modulus < Int64.modulus)%Z.
Proof.
  change Int64.modulus with 18446744073709551616%Z.
  change Int.modulus with 4294967296%Z. lia.
Qed.

Lemma Int64_Int_max_unsigned:
  (Int.max_unsigned < Int64.max_unsigned)%Z.
Proof. unfold Int64.max_unsigned, Int.max_unsigned. pose proof Int64_Int_modulus. lia. Qed.

Lemma Int_unsigned_range_2_64:
  forall i,
    (0 <= Int.unsigned i <= Int64.max_unsigned)%Z.
Proof.
  intro. pose proof (Int.unsigned_range_2 i).
  pose proof (Int64_Int_max_unsigned). lia.
Qed.

Lemma Zmult_le_nonneg_nonneg_r:
  forall (m n p : Z),
    (0 < m)%Z -> (0 < p)%Z ->
    (m * n <= p)%Z ->
    (n <= p)%Z.
Proof.
  intros. apply Zorder.Zmult_le_reg_r with (p := m). lia.
  rewrite Z.mul_comm. assert (p <= p * m)%Z.
  apply Z.le_mul_diag_r. lia. lia. lia.
Qed.

Lemma Ptrofs_max_unsigned_gt_8:
  (8 < Ptrofs.max_unsigned)%Z.
Proof.
  unfold Ptrofs.max_unsigned, Ptrofs.modulus, Ptrofs.wordsize, Wordsize_Ptrofs.wordsize;
    destruct Archi.ptr64; simpl; lia.
Qed.

Lemma Coqlib_list_norepet_cons_inv:
  forall {A: Type} (a: A) l,
    Coqlib.list_norepet (a :: l) ->
    ~In a l /\ Coqlib.list_norepet l.
Proof. intros. inversion H. tauto. Qed.

Lemma Coqlib_option_map_Some:
  forall {A B: Type} (f: A -> B) (x: option A),
    (exists z, x = Some z) <->
    (exists y, Coqlib.option_map f x = Some y).
Proof.
  split; intros.
  destruct H as [z [= ->]]. now eexists.
  destruct H as [z H]. unfold Coqlib.option_map in H.
  destruct_match_clear H. now eexists.
Qed.

Lemma if_true:
  forall {A: Type} {x y: A} {b}, b = true -> (if b then x else y) = x.
Proof. intros until b; now intros [= ->]. Qed.

Lemma if_false:
  forall {A: Type} {x y: A} {b}, b = false -> (if b then x else y) = y.
Proof. intros until b; now intros [= ->]. Qed.

Lemma orb_prop_false_first:
  forall b1 b2,
    b1 || b2 = true -> b1 = true \/ (b1 = false /\ b2 = true).
Proof. intros. unfold orb in H. destruct b1 eqn:Hb1; tauto. Qed.

Lemma Z_land_separated_zero (a b p: Z):
  (0 < p)%Z ->
  (0 <= a)%Z ->
  (0 <= b < 2 ^ p)%Z ->
  Z.land (a * 2 ^ p) b = 0%Z.
Proof.
  intros. unfold Z.land.
  destruct (a * 2 ^ p)%Z eqn:Ha, b; try lia.
  destruct (Z.eq_dec a 0) as [->|Hn0]. discriminate.
  rewrite <- (Z2Pos.id (a * 2 ^ p)) in Ha by lia. apply Pos2Z.inj in Ha.
  rewrite -> Z2Pos.inj_mul, Z2Pos.inj_pow in Ha by lia.
  destruct a as [|a|a], p; try lia. assert (p1 < 2 ^ p)%positive by lia. clear H1.
  simpl in Ha. rewrite <- N2Z.inj_0. apply N2Z.inj_iff.
  rewrite <- Ha. clear Ha p0 H H0 Hn0. apply N.bits_inj_0.
  intro. destruct (N.lt_ge_cases n (N.pos p)); rewrite N.pos_land_spec.
  + pose proof N.mul_pow2_bits_low (N.pos a) _ _ H. unfold N.testbit in H0.
    destruct (N.pos a * 2 ^ N.pos p)%N eqn:Ha; try lia.
    unfold N.mul, N.pow in Ha. inversion Ha. clear Ha.
    rewrite <- H3 in H0. now rewrite H0.
  + apply andb_false_intro2.
    pose proof N.mod_pow2_bits_high (N.pos p1) _ _ H.
    rewrite -> N.mod_small in H0 by lia. easy.
Qed.

Lemma Int64_unsigned_recompose n:
  Int64.unsigned n = (Int.unsigned (Int64.hiword n) * 2 ^ 32
                      + Int.unsigned (Int64.loword n))%Z.
Proof.
  rewrite <- (Int64.ofwords_recompose n) at 1.
  unfold Int64.ofwords, Int64.shl.
  rewrite -> (Int64.unsigned_repr (Int.unsigned _)).
  2: { pose proof (Int.unsigned_range (Int64.hiword n)).
       change Int64.max_unsigned with 18446744073709551615%Z.
       change Int.modulus with 4294967296%Z in H. lia. }
  change (Int64.unsigned (Int64.repr Int.zwordsize)) with 32%Z.
  rewrite -> Z.shiftl_mul_pow2 by lia.
  epose proof (Int64.unsigned_repr (Int.unsigned (Int64.hiword n) * 2 ^ 32) _).
  epose proof (Int64.unsigned_repr (Int.unsigned (Int64.loword n)) _).
  rewrite <- Int64.add_is_or.
  2: { unfold Int64.and. rewrite -> H, H0. apply f_equal.
       apply Z_land_separated_zero; lia || apply Int.unsigned_range. }
  unfold Int64.add. rewrite -> H, H0. rewrite -> Int64.unsigned_repr. reflexivity.
  Unshelve.
  all: pose proof (Int.unsigned_range (Int64.hiword n)).
  all: pose proof (Int.unsigned_range (Int64.loword n)).
  all: change Int.modulus with 4294967296%Z in *.
  all: change Int64.max_unsigned with 18446744073709551615%Z.
  all: lia.
Qed.

Require Import ssreflect ssrbool.

Lemma nandP':
  forall {b1 b2 : bool}, reflect (~~ b1 \/ (b1 /\ ~~ b2)) (~~ (b1 && b2)).
Proof.
  move=> [] [] /=.
  - by apply: ReflectF => - [|[]].
  - apply: ReflectT. by apply: or_intror.
  - apply: ReflectT. by apply: or_introl.
  - apply: ReflectT. by apply: or_introl.
Qed.

Fixpoint indexp' {A: Type} (l: list A) (p: positive) : list positive :=
  match l with
  | [] => []
  | _ :: l => p :: indexp' l (Pos.succ p)
  end.

Lemma indexp'_length {A: Type}:
  forall (l: list A) p, length (indexp' l p) = length l.
Proof. elim/list_ind => //= _ l IH p. by rewrite IH. Qed.

Lemma indexp'_length' {A B: Type}:
  forall (l: list A) (l': list B) p,
    length l = length l' ->
    indexp' l p = indexp' l' p.
Proof.
  elim/list_ind => /= [|_ > IH] [] //= _ >.
  move/eq_add_S. by move/IH => ->.
Qed.

Definition indexp {A: Type} (l: list A) := indexp' l 1%positive.

Lemma indexp_length {A: Type}:
  forall (l: list A), length (indexp l) = length l.
Proof. move=> l. by apply: indexp'_length. Qed.

Lemma indexp_length' {A B: Type}:
  forall (l: list A) (l': list B),
    length l = length l' ->
    indexp l = indexp l'.
Proof. move=> l l'. by apply: indexp'_length'. Qed.
