Require Import Coq.Lists.List.
Require Import BinNums ZArith Lia Bool.

Import ListNotations.

Require Import Integers Floats.

Require Import Types BUtils.

Inductive value : Type :=
| Vundef
| Vbool: bool -> value
| Vint: int -> value
| Vint64: int64 -> value
| Vfloat32: float32 -> value
| Vfloat64: float -> value
| Varr: list value -> value.

Definition Vtrue := Vbool true.
Definition Vfalse := Vbool false.

Definition shrink (t: typ) (v: value) :=
  match v, t with
  | Vint n, Tint I8  Unsigned => Vint (Int.zero_ext 8 n)
  | Vint n, Tint I8  Signed   => Vint (Int.sign_ext 8 n)
  | Vint n, Tint I16 Unsigned => Vint (Int.zero_ext 16 n)
  | Vint n, Tint I16 Signed   => Vint (Int.sign_ext 16 n)
  | _, _ => v
  end.

Theorem shrink_shrink:
  forall t v, shrink t v = shrink t (shrink t v).
Proof.
  intros. destruct v; try reflexivity.
  destruct t; try reflexivity.
  destruct i0, s; try reflexivity; simpl;
    ((rewrite Int.sign_ext_idem by lia) || (rewrite Int.zero_ext_idem by lia));
    reflexivity.
Qed.

Definition primitive_value (v: value) : bool :=
  match v with
  | Varr _ => false
  | _      => true
  end.

Lemma primitive_value_shrink:
  forall v t,
    primitive_value v = true ->
    primitive_value (shrink t v) = true.
Proof.
  induction v; simpl; intros; try easy.
  destruct t; try easy. now destruct s, i0.
Qed.

Definition float32_eq (f1 f2: float32) : bool :=
  if Float32.eq_dec f1 f2 then true else false.

Lemma float32_reflect:
  forall f1 f2, reflect (f1 = f2) (float32_eq f1 f2).
Proof.
  unfold float32_eq. intros. destruct (Float32.eq_dec f1 f2).
  now apply ReflectT. now apply ReflectF.
Qed.

Definition float64_eq (f1 f2: float) : bool :=
  if Float.eq_dec f1 f2 then true else false.

Lemma float64_reflect:
  forall f1 f2, reflect (f1 = f2) (float64_eq f1 f2).
Proof.
  unfold float64_eq. intros. destruct (Float.eq_dec f1 f2).
  now apply ReflectT. now apply ReflectF.
Qed.

Fixpoint value_beq (v1 v2: value) : bool :=
  match v1, v2 with
  | Vundef, Vundef => true
  | Vbool     b1, Vbool     b2 => bool_beq b1 b2
  | Vint      n1, Vint      n2 => Int.eq n1 n2
  | Vint64    n1, Vint64    n2 => Int64.eq n1 n2
  | Vfloat32  f1, Vfloat32  f2 => float32_eq f1 f2
  | Vfloat64  f1, Vfloat64  f2 => float64_eq f1 f2
  | Varr     lv1, Varr     lv2 => list_beq _ value_beq lv1 lv2
  | _, _ => false
  end.

Lemma value_reflect:
  forall v1 v2,
    reflect (v1 = v2) (value_beq v1 v2).
Proof.
  fix IH 1. destruct v1, v2; try now apply ReflectF.
  + now apply ReflectT.
  + destruct b, b0; now (apply ReflectT || apply ReflectF).
  + simpl; pose proof (Int.eq_spec i i0); destruct (Int.eq i i0).
    now (rewrite H; apply ReflectT). apply ReflectF; now intros [= <-].
  + simpl; pose proof (Int64.eq_spec i i0); destruct (Int64.eq i i0).
    now (rewrite H; apply ReflectT). apply ReflectF; now intros [= <-].
  + simpl. case (float32_reflect f f0). intros ->; now apply ReflectT.
    intros. apply ReflectF; now intros [= <-].
  + simpl. case (float64_reflect f f0). intros ->; now apply ReflectT.
    intros. apply ReflectF; now intros [= <-].
  + simpl; destruct list_beq eqn: Hl.
    - apply internal_list_dec_bl in Hl. rewrite Hl; now apply ReflectT.
      intros. specialize (IH x y). rewrite H in IH. now inversion IH.
    - apply ReflectF. intros [= <-]. rewrite internal_list_dec_lb in Hl; try easy.
      intros. specialize (IH x y). rewrite H in IH at 1. now inversion IH.
Qed.

Lemma value_ind':
  forall P : value -> Prop,
    P Vundef ->
    (forall b : bool, P (Vbool b)) ->
    (forall i : int, P (Vint i)) ->
    (forall i : int64, P (Vint64 i)) ->
    (forall f3 : float32, P (Vfloat32 f3)) ->
    (forall f4 : float, P (Vfloat64 f4)) ->
    (forall (l : list value) (IH: Forall P l), P (Varr l)) -> forall v : value, P v.
Proof.
  intros. revert v. fix IH 1. destruct v.
  exact H. apply H0. apply H1. apply H2. apply H3. apply H4.
  apply H5. induction l. exact (Forall_nil P).
  apply Forall_cons. apply IH. exact IHl.
Qed.

Section WellTypedness.

Inductive well_typed_value: typ -> value -> Prop :=
| well_typed_Vundef:
  well_typed_value Tvoid Vundef
| well_typed_Vbool: forall b,
  well_typed_value Tbool (Vbool b)
| well_typed_Vint: forall n sz sig,
  well_typed_value (Tint sz sig) (Vint n)
| well_typed_Vint64: forall n sig,
  well_typed_value (Tint64 sig) (Vint64 n)
| well_typed_Vfloat32: forall f,
  well_typed_value (Tfloat F32) (Vfloat32 f)
| well_typed_Vfloat64: forall f,
  well_typed_value (Tfloat F64) (Vfloat64 f)
| well_typed_Varr: forall t lv,
  Forall (well_typed_value t) lv ->
  well_typed_value (Tarr t) (Varr lv).

Derive Inversion inv_well_typed_value with
  (forall t v, well_typed_value t v) Sort Prop.

Inductive well_typed_valuelist: list typ -> list value -> Prop :=
| well_typed_valuelist_Nil:
  well_typed_valuelist [] []
| well_typed_valuelist_Cons: forall v t lv lt,
  well_typed_value t v ->
  well_typed_valuelist lt lv ->
  well_typed_valuelist (t :: lt) (v :: lv) .

Lemma well_typed_value_shrink:
    forall v t,
      well_typed_value t v <->
      well_typed_value t (shrink t v).
Proof.
  destruct v; simpl; intros; try easy. destruct t; try easy.
  destruct i0, s; split; econstructor.
Qed.

Lemma well_typed_valuelist_nth_error:
  forall lv lt n arg,
    well_typed_valuelist lt lv ->
    nth_error lv n = Some arg ->
    exists t, nth_error lt n = Some t /\ well_typed_value t arg.
Proof.
  induction lv; simpl; intros. now destruct n.
  inversion_clear H. destruct n.
  + revert H0; intros [= <-]. eexists; simpl; eauto.
  + simpl in *; eauto.
Qed.

Lemma well_typed_valuelist_nth_error':
  forall lt lv n t,
    well_typed_valuelist lt lv ->
    nth_error lt n = Some t ->
    exists v, nth_error lv n = Some v /\ well_typed_value t v.
Proof.
  induction lt; simpl; intros. now destruct n.
  inversion_clear H. destruct n.
  + revert H0; intros [= <-]. eexists; simpl; eauto.
  + simpl in *; eauto.
Qed.

Lemma well_typed_valuelist_length:
  forall lt lv,
    well_typed_valuelist lt lv ->
    length lt = length lv.
Proof.
  induction lt.
  - now inversion 1.
  - inversion_clear 1; simpl; now rewrite (IHlt _ H1).
Qed.

Fixpoint well_typed_value_bool (t: typ) (v: value) :=
  match v, t with
  | Vundef,  Tvoid => true
  | Vbool b, Tbool => true
  | Vint     _, Tint _ _ => true
  | Vint64   _, Tint64 _ => true
  | Vfloat32 _, Tfloat F32 => true
  | Vfloat64 _, Tfloat F64 => true
  | Varr lv, Tarr t =>
      forallb (well_typed_value_bool t) lv
  | _, _ => false
  end.

Lemma well_typed_value_bool_correct:
  forall t v,
    well_typed_value_bool t v = true -> well_typed_value t v.
Proof.
  induction t; simpl; intros; destruct v; try discriminate.
  + apply well_typed_Vundef.
  + apply well_typed_Vbool.
  + apply well_typed_Vint.
  + apply well_typed_Vint64.
  + destruct f; try discriminate; apply well_typed_Vfloat32.
  + destruct f; try discriminate; apply well_typed_Vfloat64.
  + apply well_typed_Varr. apply Forall_forall. intros x Hin.
    apply IHt. now apply forallb_forall with (2 := Hin).
Qed.

Lemma well_typed_value_bool_complete:
  forall t v,
    well_typed_value t v -> well_typed_value_bool t v = true.
Proof.
  induction t; intros; inversion_clear H; simpl; eauto.
  apply forallb_forall. intros x Hin. apply IHt.
  now apply Forall_forall with (2 := Hin).
Qed.

Lemma primitive_type_primitive_value:
  forall t v,
    well_typed_value_bool t v = true ->
    primitive_type t = primitive_value v.
Proof. now intros [] []. Qed.

End WellTypedness.

Local Open Scope option_bool_monad_scope.

Fixpoint natlist_of_Vint64 (lv: list value) : option (list nat) :=
  match lv with
  | [] => Some []
  | Vint64 n :: lv =>
    doo ln <- natlist_of_Vint64 lv;
    Some ((Z.to_nat (Int64.unsigned n)) :: ln)
  | _ => None
  end.

Fixpoint build_index' (lidx: list value) (lsz: list nat)
                      (acc: nat) : option nat :=
  match lidx, lsz with
  | [], [] => Some acc
  | Vint64 n :: lidx, sz :: lsz =>
    build_index' lidx lsz (acc * sz + Z.to_nat (Int64.unsigned n))
  | _, _ => None
  end.

Definition build_index lidx lsz := build_index' lidx lsz 0.