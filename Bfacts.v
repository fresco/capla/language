Require Import ssreflect ssrbool ssrfun.
Require Import ZArith Lia.

Require Import Types BValues Ops BEnv Syntax.
Require Import SemPath SemanticsCommon.
Require Import BUtils Tactics PTreeaux.
Require Import Validity ExprSem.
Require SemanticsBlocking.
Module B := SemanticsBlocking.

Require Import Maps Integers.

Section EVAL_EXPR_FIXPOINT_MATCH.

Variables (e: env) (f: function).

Local Open Scope outcome_scope.

Ltac cbn' := cbn -[get_env_path].
Ltac cbn_ := cbn -[eval_expr].

Theorem eval_identlist_fixpoint_match:
  forall lids lv,
    B.eval_identlist e f lids lv <->
    eval_ident_list e f lids = Val lv.
Proof.
  elim/list_ind.
  - split=>> H. by inv H. case: H => <-. by econstructor.
  - move=>> IH >; split => H.
    + inv H => /=. rewrite H2 H3 /=. by rewrite (proj1 (IH _) H5).
    + move: H => /=. case E: e!_ => //=. case S: (fn_szenv' f)!_ => [[]|] //=.
      all: case I: eval_ident_list =>> //= [<-].
      have:= proj2 (IH _) I. by econstructor.
Qed.

Theorem eval_expr_fixpoint_match_Some:
  forall exp v,
    eval_expr e f exp = Val v -> B.eval_expr e f exp v.
Proof.
  fix IH 1. case.
  - case=> [i l] v. cbn'.
    set F := fix __f _ (l0: list syn_path_elem) := _.
    have H: forall llsz l', F llsz l = Val l' ->
                            B.eval_path_elem_list e f l llsz l'.
    { elim/list_ind: l => /=.
      - case=>> [<-]; by econstructor.
      - case=> lidx > IH' [] > //=.
        set eval_expr' := (fix eval_expr (exp: expr) := _
                           with eval_path_elem lsz (s : syn_path_elem) := _
                           for eval_expr).
        set G := fix __f (l: list expr) := _.
        have H: forall lv, G lidx = Val lv ->
                            B.eval_exprlist e f lidx lv.
        { elim/list_ind: lidx.
          - move=>> [<-]; by econstructor.
          - move=>> IHlidx > /=. case E: eval_expr' => //=.
            case El: G =>> //= [<-]. econstructor.
            by apply IH. by apply IHlidx. }
        case Elsz: eval_ident_list => //=. case S: natlist_of_Vint64 => //=.
        case Elidx: G => //=. case Bidx: build_index => //=.
        case V: valid_index => //=. case El: F =>> //= [<-].
        have V' := valid_index_product _ _ _ _ S Bidx V.
        move/eval_identlist_fixpoint_match in Elsz.
        move/H in Elidx. move/IH' in El. econstructor; eassumption. }
    case S: (fn_szenv' f)!_ => //; cbn'. case E: F => //; cbn'.
    case P: get_env_path => //=. case Pr: primitive_value =>> // [<-].
    move/H in E. eauto with semantics.
  - case=>> [<-]; econstructor.
  - move=>>; cbn. case E: eval_expr => //=. case S: sem_cast =>> //=.
    move=> [<-]. move/IH in E. econstructor; eassumption.
    by case (_ && _).
  - move=>>; cbn. case E: eval_expr => //=; case S: sem_unop =>> //= [<-].
    move/IH in E. econstructor; eassumption.
  - move=>>; cbn. case E1: eval_expr => //=; case E2: eval_expr => //=.
    case S: sem_binarith_operation =>> //=.
    move=> [<-]. move/IH in E1. move/IH in E2. econstructor; eassumption.
    by case binarith_operation_allowed.
  - move=>>; cbn. case E1: eval_expr => //=; case E2: eval_expr => //=.
    case S: sem_cmp_operation =>> //= [<-].
    move/IH in E1. move/IH in E2. econstructor; eassumption.
  - move=>>; cbn. case E1: eval_expr => //=; case E2: eval_expr => //=.
    case S: sem_cmpu_operation =>> //= [<-].
    move/IH in E1. move/IH in E2. econstructor; eassumption.
Qed.

End EVAL_EXPR_FIXPOINT_MATCH.

Lemma eval_identlist_incl e e' f:
  incl' e e' ->
  forall lidx lvidx,
    B.eval_identlist e  f lidx lvidx ->
    B.eval_identlist e' f lidx lvidx.
Proof. move=> INCL. elim/list_ind => [|> ?] > T; inv T; econstructor; eauto. Qed.

Lemma eval_expr_incl e e' f:
  incl' e e' ->
  forall exp v,
    B.eval_expr e  f exp v ->
    B.eval_expr e' f exp v.
Proof.
  move=> INCL. fix IH 1. case.
  2-7: move=>> T; inv T; econstructor; eauto.
  case=> i l > T; inv T. apply B.eval_Access with (p' := p') => //.
  - inv H0. econstructor; eauto. elim/list_ind: l llsz sempl H6 {H1 H4} => //=.
    + move=>> T; inv T; econstructor.
    + move=> [lidx] > ? > T; inv T; econstructor; eauto.
      eapply eval_identlist_incl; eassumption.
      elim/list_ind: lidx lvidx H4 {H5} => [|> ?] > T; inv T; econstructor; eauto.
  - case: p' H1 {H0} =>> /=. case H: e!_ => //=.
    rewrite (INCL _ _ H) //.
Qed.