Require Import ssreflect ssrbool ssrfun.

Require Import ZArith BinNums List Lia.

Require Import Integers Floats Maps Errors Events Coqlib.
Require Import Smallstep.
Module SS := Smallstep.

Require Import Types Syntax Ops.
Require Import BEnv BValues.
Require Import Typing Alias Validity.
Require Import Tactics.

Require Import SemanticsCommon ExprSem SemanticsNonBlocking.

Import ListNotations.

Section BIGSTEP_SEMANTICS.

Variable ge: genv.

Inductive outcome: Type :=
| Out_normal: outcome
| Out_exit: nat -> outcome
| Out_return: option value -> outcome
| Out_error: outcome.

Definition outcome_result_value (out: outcome) :=
  match out with
  | Out_normal => Some Vundef
  | Out_return None => Some Vundef
  | Out_return (Some v) => Some v
  | Out_exit n => Some Vundef
  | Out_error => None
  end.

Definition outcome_block (out: outcome) :=
  match out with
  | Out_exit O => Out_normal
  | Out_exit (S n) => Out_exit n
  | _ => out
  end.

Definition set_optenv2 (e: env) (idv: option SemPath.ident) (v: option value) :=
  match idv, v with
  | Some i, Some v => PTree.set i v e
  | _, _ => e
  end.

Definition outcome_call (vres: option value) : outcome :=
  match vres with
  | Some _ => Out_normal
  | None => Out_error
  end.

Definition well_typed_option_value (t: typ) (v: option value) :=
  match v with
  | None => True
  | Some v => well_typed_value t v
  end.

Definition primitive_option_value (v: option value) :=
  match v with
  | Some v => primitive_value v
  | None => true
  end.

Definition valid_function_outcome (out: outcome) :=
  match out with
  | Out_normal | Out_return _ | Out_error => True
  | _ => False
  end.

(*
Inductive eval_funcall:
  fundef -> list value -> env -> option value -> Prop :=
| eval_funcall_internal: forall f vargs e e' e'' out r,
  build_func_env (PTree.empty value) f.(fn_params) vargs = Some e ->
  instantiate_size_vars e f = OK e' ->
  exec_stmt f e' f.(fn_body) e'' out ->
  valid_function_outcome out ->
  out <> Out_error ->
  outcome_result_value out r ->
  well_typed_option_value (sig_res (fn_sig f)) r ->
  primitive_option_value r = true ->
  eval_funcall (Internal f) vargs e'' r

with exec_stmt:
  function ->
  env -> stmt ->
  env -> outcome -> Prop :=
| step_skip: forall f e,
  exec_stmt f e Sskip e Out_normal
| exec_assign: forall f e p p' t exp v e',
  eval_full_path e f p (Some p') ->
  get_tenv_path f.(fn_tenv) p' = Some t ->
  eval_expr e f exp (Some v) ->
  primitive_value v = true ->
  option_map (perm_le Mutable) (fn_penv f)!(fst p) = Some true ->
  well_typed_value t v ->
  update_env_path' e p' (shrink t v) = Some e' ->
  exec_stmt f e (Sassign p exp) e' Out_normal
| exec_assign_ERR: forall f e p exp v,
  eval_full_path e f p None ->
  eval_expr e f exp (Some v) ->
  primitive_value v = true ->
  option_map (perm_le Mutable) (fn_penv f)!(fst p) = Some true ->
  exec_stmt f e (Sassign p exp) e Out_error
| exec_assign_ERR_val: forall f e p exp,
  eval_expr e f exp None ->
  exec_stmt f e (Sassign p exp) e Out_error
| exec_alloc: forall f e i t sz isz v n,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  default_value t = Some v ->
  eval_expr e f sz (Some (Vint64 n)) ->
  Int64.unsigned n * typ_sizeof t <= Int64.max_unsigned ->
  exec_stmt f e (Salloc i)
            (PTree.set i (Varr (repeat v (Z.to_nat (Int64.unsigned n))))
              (PTree.set isz (Vint64 n) e))
            Out_normal
| exec_alloc_ERR_sz: forall f e i sz,
  (fn_szenv f)!i = Some [[sz]] ->
  eval_expr e f sz None ->
  exec_stmt f e (Salloc i) e Out_error
| exec_alloc_ERR_overflow: forall f e i t sz isz n,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  eval_expr e f sz (Some (Vint64 n)) ->
  Int64.unsigned n * typ_sizeof t > Int64.max_unsigned ->
  exec_stmt f e (Salloc i) e Out_error
| exec_free: forall f e i t sz isz lv,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  e!i = Some (Varr lv) ->
  exec_stmt f e (Sfree i) (PTree.remove i (PTree.remove isz e)) Out_normal
| exec_call_Internal: forall f e idvar idf args pargs vargs pp mut shr own f' lvtests e1 vres e',
  ge!idf = Some (Internal f') ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  enough_permission (fn_penv f) (fn_penv f') pp = true ->
  pargs_params (fn_params f') pargs = Some pp ->
  mut_args (fn_penv f') pp = mut ->
  shr_args (fn_penv f') pp = shr ->
  own_args (fn_penv f') pp = own ->
  valid_call_tests_values f (Internal f') e args = Val lvtests ->
  equal_tests lvtests = true ->
  length args = length f'.(fn_sig).(sig_args) ->
  match_types (fn_tenv f) (fn_tenv f') pp = true ->
  well_typed_valuelist f'.(fn_sig).(sig_args) vargs ->
  all_root_paths own = true ->
  pargs_params_aliasing pargs (mut ++ own) = false ->
  eval_funcall (Internal f') vargs e1 vres ->
  marrs_varr e e1 mut = true ->
  update_env e1 (remove_own_params own e) mut = Some e' ->
  primitive_option_value vres = true ->
  exec_stmt f e (Scall idvar idf args)
            (set_optenv2 e' idvar vres) (outcome_call vres)
| exec_call_External: forall f e idvar idf ef args pargs vargs pp mut shr own vmarrs r e' lvtests,
  ge!idf = Some (External ef) ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  pargs_params (ef_params ef) pargs = Some pp ->
  mut_args (ef_penv ef) pp = mut ->
  shr_args (ef_penv ef) pp = shr ->
  own_args (ef_penv ef) pp = own ->
  valid_call_tests_values f (External ef) e args = Val lvtests ->
  equal_tests lvtests = true ->
  length args = length (ef_sig ef).(sig_args) ->
  well_typed_valuelist (ef_sig ef).(sig_args) vargs ->
  all_root_paths own = true ->
  pargs_params_aliasing pargs (mut ++ own) = false ->
  external_call ef (combine pargs vargs) = (vmarrs, r) ->
  (idvar <> None -> primitive_value r = true) ->
  update_env (build_env PTree.Empty (combine (map snd mut) vmarrs))
             (remove_own_params own e) mut = Some e' ->
  exec_stmt f e (Scall idvar idf args) (set_optenv e' idvar r) Out_normal
| exec_call_ERR_args: forall f e idvar idf fd args,
  ge!idf = Some fd ->
  eval_path_list e f args None ->
  exec_stmt f e (Scall idvar idf args) e Out_error
| exec_call_ERR_tests: forall f e idvar idf fd args pargs vargs pp marrs oarrs,
  ge!idf = Some fd ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  pargs_params (fd_params fd) pargs = Some pp ->
  mut_args (fd_penv fd) pp = marrs ->
  own_args (fd_penv fd) pp = oarrs ->
  valid_call_tests_values f fd e args = Err ->
  valid_env_szenv e (fn_szenv' f) (map fst args) ->
  length args = length (fd_sig fd).(sig_args) ->
  well_typed_valuelist (fd_sig fd).(sig_args) vargs ->
  pargs_params_aliasing pargs (marrs ++ oarrs) = false ->
  exec_stmt f e (Scall idvar idf args) e Out_error
| step_call_ERR_invalid: forall f e idvar idf fd args pargs vargs pp marrs oarrs lvtests,
  ge!idf = Some fd ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  pargs_params (fd_params fd) pargs = Some pp ->
  mut_args (fd_penv fd) pp = marrs ->
  own_args (fd_penv fd) pp = oarrs ->
  valid_call_tests_values f fd e args = Val lvtests ->
  equal_tests lvtests = false ->
  length args = length (fd_sig fd).(sig_args) ->
  well_typed_valuelist (fd_sig fd).(sig_args) vargs ->
  pargs_params_aliasing pargs (marrs ++ oarrs) = false ->
  exec_stmt f e (Scall idvar idf args) e Out_error
| exec_return_None: forall f e,
  well_typed_value f.(fn_sig).(sig_res) Vundef ->
  exec_stmt f e (Sreturn None) e (Out_return None)
| exec_return_Some: forall f e exp v,
  eval_expr e f exp (Some v) ->
  well_typed_value f.(fn_sig).(sig_res) v ->
  primitive_value v = true ->
  exec_stmt f e (Sreturn (Some exp)) e (Out_return (Some v))
| exec_return_ERR: forall f e exp,
  eval_expr e f exp None ->
  exec_stmt f e (Sreturn (Some exp)) e Out_error
| exec_assert: forall f e cond b,
  eval_expr e f cond (Some (Vbool b)) ->
  exec_stmt f e (Sassert cond) e (if b then Out_normal else Out_error)
| exec_assert_ERR: forall f e cond,
  eval_expr e f cond None ->
  exec_stmt f e (Sassert cond) e Out_error
| exec_ifthenelse: forall f e cond b s1 s2 e' out,
  eval_expr e f cond (Some (Vbool b)) ->
  exec_stmt f e (if b then s1 else s2) e' out ->
  exec_stmt f e (Sifthenelse cond s1 s2) e' out
| exec_ifthenelse_ERR: forall f e cond s1 s2,
  eval_expr e f cond None ->
  exec_stmt f e (Sifthenelse cond s1 s2) e Out_error
| exec_seq_continue: forall f e s1 e1 s2 e2 out,
  exec_stmt f e s1 e1 Out_normal ->
  exec_stmt f e1 s2 e2 out ->
  exec_stmt f e (Sseq s1 s2) e2 out
| exec_seq_stop: forall f e s1 e1 s2 out,
  exec_stmt f e s1 e1 out ->
  out <> Out_normal ->
  exec_stmt f e (Sseq s1 s2) e1 out
| exec_loop_loop: forall f e s e1 out1 e2 out2,
  exec_stmt f e s e1 out1 ->
  outcome_loop_continue out1 ->
  exec_stmt f e1 (Sloop s) e2 out2 ->
  exec_stmt f e (Sloop s) e2 out2
| exec_loop_stop: forall f e s e1 out1,
  exec_stmt f e s e1 out1 ->
  ~ outcome_loop_continue out1 ->
  exec_stmt f e (Sloop s) e1 (outcome_loop out1)
| exec_continue: forall f e,
  exec_stmt f e Scontinue e Out_continue
| exec_break: forall f e,
  exec_stmt f e Sbreak e Out_break
| exec_error: forall f e,
  exec_stmt f e Serror e Out_error.
*)

Inductive exec_stmt
  (eval_funcall: fundef -> list value -> env -> option value -> Prop)
  (f: function):
  env -> stmt ->
  env -> outcome -> Prop :=
| step_skip: forall e,
  exec_stmt eval_funcall f e Sskip e Out_normal
| exec_assign: forall e p p' t exp v e',
  eval_full_path e f p (Some p') ->
  get_tenv_path f.(fn_tenv) p' = Some t ->
  eval_expr e f exp (Some v) ->
  primitive_value v = true ->
  option_map (perm_le Mutable) (fn_penv f)!(fst p) = Some true ->
  well_typed_value t v ->
  update_env_path' e p' (shrink t v) = Some e' ->
  exec_stmt eval_funcall f e (Sassign p exp) e' Out_normal
| exec_assign_ERR: forall e p exp v,
  eval_full_path e f p None ->
  eval_expr e f exp (Some v) ->
  primitive_value v = true ->
  option_map (perm_le Mutable) (fn_penv f)!(fst p) = Some true ->
  exec_stmt eval_funcall f e (Sassign p exp) e Out_error
| exec_assign_ERR_val: forall e p exp,
  eval_expr e f exp None ->
  exec_stmt eval_funcall f e (Sassign p exp) e Out_error
| exec_alloc: forall e i t sz isz v n,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  default_value t = Some v ->
  eval_expr e f sz (Some (Vint64 n)) ->
  Int64.unsigned n * typ_sizeof t <= Int64.max_unsigned ->
  exec_stmt eval_funcall f e (Salloc i)
            (PTree.set i (Varr (repeat v (Z.to_nat (Int64.unsigned n))))
              (PTree.set isz (Vint64 n) e))
            Out_normal
| exec_alloc_ERR_sz: forall e i sz,
  (fn_szenv f)!i = Some [[sz]] ->
  eval_expr e f sz None ->
  exec_stmt eval_funcall f e (Salloc i) e Out_error
| exec_alloc_ERR_overflow: forall e i t sz isz n,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  eval_expr e f sz (Some (Vint64 n)) ->
  Int64.unsigned n * typ_sizeof t > Int64.max_unsigned ->
  exec_stmt eval_funcall f e (Salloc i) e Out_error
| exec_free: forall e i t sz isz lv,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  e!i = Some (Varr lv) ->
  exec_stmt eval_funcall f e (Sfree i) (PTree.remove i (PTree.remove isz e)) Out_normal
| exec_call_Internal: forall e idvar idf args pargs vargs pp mut shr own f' lvtests e1 vres e',
  ge!idf = Some (Internal f') ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  enough_permission (fn_penv f) (fn_penv f') pp = true ->
  pargs_params (fn_params f') pargs = Some pp ->
  mut_args (fn_penv f') pp = mut ->
  shr_args (fn_penv f') pp = shr ->
  own_args (fn_penv f') pp = own ->
  valid_call_tests_values f (Internal f') e args = Val lvtests ->
  equal_tests lvtests = true ->
  length args = length f'.(fn_sig).(sig_args) ->
  match_types (fn_tenv f) (fn_tenv f') pp = true ->
  well_typed_valuelist f'.(fn_sig).(sig_args) vargs ->
  all_root_paths own = true ->
  pargs_params_aliasing pargs (mut ++ own) = false ->
  eval_funcall (Internal f') vargs e1 vres ->
  marrs_varr e e1 mut = true ->
  update_env e1 (remove_own_params own e) mut = Some e' ->
  primitive_option_value vres = true ->
  exec_stmt eval_funcall f e (Scall idvar idf args)
            (set_optenv2 e' idvar vres) (outcome_call vres)
| exec_call_External: forall e idvar idf ef args pargs vargs pp mut shr own vmarrs r e' lvtests,
  ge!idf = Some (External ef) ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  pargs_params (ef_params ef) pargs = Some pp ->
  mut_args (ef_penv ef) pp = mut ->
  shr_args (ef_penv ef) pp = shr ->
  own_args (ef_penv ef) pp = own ->
  valid_call_tests_values f (External ef) e args = Val lvtests ->
  equal_tests lvtests = true ->
  length args = length (ef_sig ef).(sig_args) ->
  well_typed_valuelist (ef_sig ef).(sig_args) vargs ->
  all_root_paths own = true ->
  pargs_params_aliasing pargs (mut ++ own) = false ->
  external_call ef (combine pargs vargs) = (vmarrs, r) ->
  (idvar <> None -> primitive_value r = true) ->
  update_env (build_env PTree.Empty (combine (map snd mut) vmarrs))
             (remove_own_params own e) mut = Some e' ->
  exec_stmt eval_funcall f e (Scall idvar idf args) (set_optenv e' idvar r) Out_normal
| exec_call_ERR_args: forall e idvar idf fd args,
  ge!idf = Some fd ->
  eval_path_list e f args None ->
  exec_stmt eval_funcall f e (Scall idvar idf args) e Out_error
| exec_call_ERR_tests: forall e idvar idf fd args pargs vargs pp marrs oarrs,
  ge!idf = Some fd ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  pargs_params (fd_params fd) pargs = Some pp ->
  mut_args (fd_penv fd) pp = marrs ->
  own_args (fd_penv fd) pp = oarrs ->
  valid_call_tests_values f fd e args = Err ->
  valid_env_szenv e (fn_szenv' f) (map fst args) ->
  length args = length (fd_sig fd).(sig_args) ->
  well_typed_valuelist (fd_sig fd).(sig_args) vargs ->
  pargs_params_aliasing pargs (marrs ++ oarrs) = false ->
  exec_stmt eval_funcall f e (Scall idvar idf args) e Out_error
| exec_call_ERR_invalid: forall e idvar idf fd args pargs vargs pp marrs oarrs lvtests,
  ge!idf = Some fd ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  pargs_params (fd_params fd) pargs = Some pp ->
  mut_args (fd_penv fd) pp = marrs ->
  own_args (fd_penv fd) pp = oarrs ->
  valid_call_tests_values f fd e args = Val lvtests ->
  equal_tests lvtests = false ->
  length args = length (fd_sig fd).(sig_args) ->
  well_typed_valuelist (fd_sig fd).(sig_args) vargs ->
  pargs_params_aliasing pargs (marrs ++ oarrs) = false ->
  exec_stmt eval_funcall f e (Scall idvar idf args) e Out_error
| exec_return_None: forall e,
  well_typed_value f.(fn_sig).(sig_res) Vundef ->
  exec_stmt eval_funcall f e (Sreturn None) e (Out_return None)
| exec_return_Some: forall e exp v,
  eval_expr e f exp (Some v) ->
  well_typed_value f.(fn_sig).(sig_res) v ->
  primitive_value v = true ->
  exec_stmt eval_funcall f e (Sreturn (Some exp)) e (Out_return (Some v))
| exec_return_ERR: forall e exp,
  eval_expr e f exp None ->
  exec_stmt eval_funcall f e (Sreturn (Some exp)) e Out_error
| exec_assert: forall e cond b,
  eval_expr e f cond (Some (Vbool b)) ->
  exec_stmt eval_funcall f e (Sassert cond) e (if b then Out_normal else Out_error)
| exec_assert_ERR: forall e cond,
  eval_expr e f cond None ->
  exec_stmt eval_funcall f e (Sassert cond) e Out_error
| exec_ifthenelse: forall e cond b s1 s2 e' out,
  eval_expr e f cond (Some (Vbool b)) ->
  exec_stmt eval_funcall f e (if b then s1 else s2) e' out ->
  exec_stmt eval_funcall f e (Sifthenelse cond s1 s2) e' out
| exec_ifthenelse_ERR: forall e cond s1 s2,
  eval_expr e f cond None ->
  exec_stmt eval_funcall f e (Sifthenelse cond s1 s2) e Out_error
| exec_seq_continue: forall e s1 e1 s2 e2 out,
  exec_stmt eval_funcall f e s1 e1 Out_normal ->
  exec_stmt eval_funcall f e1 s2 e2 out ->
  exec_stmt eval_funcall f e (Sseq s1 s2) e2 out
| exec_seq_stop: forall e s1 e1 s2 out,
  exec_stmt eval_funcall f e s1 e1 out ->
  out <> Out_normal ->
  exec_stmt eval_funcall f e (Sseq s1 s2) e1 out
| exec_loop_loop: forall e s e1 e2 out,
  exec_stmt eval_funcall f e s e1 Out_normal ->
  exec_stmt eval_funcall f e1 (Sloop s) e2 out ->
  exec_stmt eval_funcall f e (Sloop s) e2 out
| exec_loop_stop: forall e s e1 out,
  exec_stmt eval_funcall f e s e1 out ->
  out <> Out_normal ->
  exec_stmt eval_funcall f e (Sloop s) e1 out
| exec_block: forall e s e1 out,
  exec_stmt eval_funcall f e s e1 out ->
  exec_stmt eval_funcall f e (Sblock s) e1 (outcome_block out)
| exec_exit: forall e n,
  exec_stmt eval_funcall f e (Sexit n) e (Out_exit n)
| exec_error: forall e,
  exec_stmt eval_funcall f e Serror e Out_error.

Inductive eval_funcall: fundef -> list value -> env -> option value -> Prop :=
| eval_funcall_internal: forall f vargs e e' e'' out r,
  build_func_env (PTree.empty value) f.(fn_params) vargs = Some e ->
  instantiate_size_vars e f = OK e' ->
  exec_stmt eval_funcall f e' f.(fn_body) e'' out ->
  valid_function_outcome out ->
  r = outcome_result_value out ->
  well_typed_option_value (sig_res (fn_sig f)) r ->
  primitive_option_value r = true ->
  eval_funcall (Internal f) vargs e'' r.

(* Scheme eval_funcall_ind2 := Minimality for eval_funcall Sort Prop
  with exec_stmt_ind2 := Minimality for exec_stmt Sort Prop.
Combined Scheme eval_funcall_exec_stmt_ind2
  from eval_funcall_ind2, exec_stmt_ind2. *)
(*
CoInductive evalinf_funcall:
  fundef -> list value -> Prop :=
| evalinf_funcall_internal: forall f vargs e e',
  build_func_env (PTree.empty value) f.(fn_params) vargs = Some e ->
  instantiate_size_vars e f = OK e' ->
  execinf_stmt f e' f.(fn_body) ->
  evalinf_funcall (Internal f) vargs

with execinf_stmt: function -> env -> stmt -> Prop :=
| execinf_call: forall f e idvar idf args pargs vargs pp mut shr own f' lvtests,
  ge!idf = Some (Internal f') ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  enough_permission (fn_penv f) (fn_penv f') pp = true ->
  pargs_params (fn_params f') pargs = Some pp ->
  mut_args (fn_penv f') pp = mut ->
  shr_args (fn_penv f') pp = shr ->
  own_args (fn_penv f') pp = own ->
  valid_call_tests_values f (Internal f') e args = Val lvtests ->
  equal_tests lvtests = true ->
  length args = length f'.(fn_sig).(sig_args) ->
  match_types (fn_tenv f) (fn_tenv f') pp = true ->
  well_typed_valuelist f'.(fn_sig).(sig_args) vargs ->
  all_root_paths own = true ->
  pargs_params_aliasing pargs (mut ++ own) = false ->
  evalinf_funcall f' vargs ->
  execinf_stmt f e (Scall idvar idf args)
| execinf_ifthenelse: forall f e cond b s1 s2,
  eval_expr e f cond (Some (Vbool b)) ->
  execinf_stmt f e (if b then s1 else s2) ->
  execinf_stmt f e (Sifthenelse cond s1 s2)
| execinf_seq_1: forall f e s1 s2,
  execinf_stmt f e s1 ->
  execinf_stmt f e (Sseq s1 s2)
| execinf_seq_2: forall f e s1 e1 s2,
  exec_stmt f e s1 e1 Out_normal ->
  execinf_stmt f e1 s2 ->
  execinf_stmt f e (Sseq s1 s2)
| execinf_loop_body: forall f e s,
  execinf_stmt f e s ->
  execinf_stmt f e (Sloop s)
| execinf_loop_loop: forall f e s e1 out1,
  exec_stmt f e s e1 out1 ->
  outcome_loop_continue out1 ->
  execinf_stmt f e1 (Sloop s) ->
  execinf_stmt f e (Sloop s).
*)

CoInductive execinf_stmt
  (evalinf_funcall: fundef -> list value -> Prop)
  (f: function): env -> stmt -> Prop :=
| execinf_call: forall e idvar idf args pargs vargs pp mut shr own f' lvtests,
  ge!idf = Some (Internal f') ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  enough_permission (fn_penv f) (fn_penv f') pp = true ->
  pargs_params (fn_params f') pargs = Some pp ->
  mut_args (fn_penv f') pp = mut ->
  shr_args (fn_penv f') pp = shr ->
  own_args (fn_penv f') pp = own ->
  valid_call_tests_values f (Internal f') e args = Val lvtests ->
  equal_tests lvtests = true ->
  length args = length f'.(fn_sig).(sig_args) ->
  match_types (fn_tenv f) (fn_tenv f') pp = true ->
  well_typed_valuelist f'.(fn_sig).(sig_args) vargs ->
  all_root_paths own = true ->
  pargs_params_aliasing pargs (mut ++ own) = false ->
  evalinf_funcall f' vargs ->
  execinf_stmt evalinf_funcall f e (Scall idvar idf args)
| execinf_ifthenelse: forall e cond b s1 s2,
  eval_expr e f cond (Some (Vbool b)) ->
  execinf_stmt evalinf_funcall f e (if b then s1 else s2) ->
  execinf_stmt evalinf_funcall f e (Sifthenelse cond s1 s2)
| execinf_seq_1: forall e s1 s2,
  execinf_stmt evalinf_funcall f e s1 ->
  execinf_stmt evalinf_funcall f e (Sseq s1 s2)
| execinf_seq_2: forall e s1 e1 s2,
  exec_stmt eval_funcall f e s1 e1 Out_normal ->
  execinf_stmt evalinf_funcall f e1 s2 ->
  execinf_stmt evalinf_funcall f e (Sseq s1 s2)
| execinf_loop_body: forall e s,
  execinf_stmt evalinf_funcall f e s ->
  execinf_stmt evalinf_funcall f e (Sloop s)
| execinf_loop_loop: forall e s e1,
  exec_stmt eval_funcall f e s e1 Out_normal ->
  execinf_stmt evalinf_funcall f e1 (Sloop s) ->
  execinf_stmt evalinf_funcall f e (Sloop s)
| execinf_block: forall e s,
  execinf_stmt evalinf_funcall f e s ->
  execinf_stmt evalinf_funcall f e (Sblock s).

Inductive evalinf_funcall:
  fundef -> list value -> Prop :=
| evalinf_funcall_internal: forall f vargs e e',
  build_func_env (PTree.empty value) f.(fn_params) vargs = Some e ->
  instantiate_size_vars e f = OK e' ->
  execinf_stmt evalinf_funcall f e' f.(fn_body) ->
  evalinf_funcall (Internal f) vargs.

End BIGSTEP_SEMANTICS.

Inductive bigstep_program_terminates (p: program) : trace -> int -> Prop :=
| bigstep_program_terminates_intro: forall f e i,
  let ge := genv_of_program p in
  ge!(prog_main p) = Some (Internal f) ->
  f.(fn_sig) = {| sig_args := []; sig_res := Tint I32 Signed |} ->
  eval_funcall ge (Internal f) [] e (Some (Vint i)) ->
  bigstep_program_terminates p E0 i.

Inductive bigstep_program_diverges (p: program) : traceinf -> Prop :=
| bigstep_program_diverges_intro: forall f t,
  let ge := genv_of_program p in
  ge!(prog_main p) = Some (Internal f) ->
  f.(fn_sig) = {| sig_args := []; sig_res := Tint I32 Signed |} ->
  evalinf_funcall ge (Internal f) [] ->
  bigstep_program_diverges p t
| bigstep_program_error: forall f e t,
  let ge := genv_of_program p in
  ge!(prog_main p) = Some (Internal f) ->
  f.(fn_sig) = {| sig_args := []; sig_res := Tint I32 Signed |} ->
  eval_funcall ge (Internal f) [] e None ->
  bigstep_program_diverges p t.

Definition bigstep_semantics (p: program) :=
  SS.Bigstep_semantics (bigstep_program_terminates p)
                       (bigstep_program_diverges p).

Fixpoint destructContToLoop (k: cont) :=
  match k with
  | Kseq _ k => destructContToLoop k
  | _ => k
  end.

Section BIGSTEP_SILENT_SOUNDNESS.

Record bigstep_sound (B: SS.bigstep_semantics)
                     (L: SS.semantics) : Prop :=
  Bigstep_sound {
    bigstep_terminates_sound:
      forall t r,
        SS.bigstep_terminates B t r ->
        exists s1 s2,
          SS.initial_state L s1 /\
          Star L s1 t s2 /\
          SS.final_state L s2 r;
    bigstep_diverges_sound:
      forall T,
        bigstep_diverges B T ->
        exists s1,
          SS.initial_state L s1 /\
          forever_silent (step L) (globalenv L) s1
}.

End BIGSTEP_SILENT_SOUNDNESS.

Section BIGSTEP_TO_TRANSITION.

Variable prog: program.
Let ge := genv_of_program prog.

Inductive outcome_state_match (e: env) (f: function) (k: cont):
  outcome -> state -> Prop :=
| osm_normal:
  outcome_state_match e f k Out_normal (State e f Sskip k)
| osm_exit: forall n,
  outcome_state_match e f k (Out_exit n) (State e f (Sexit n) k)
| osm_error: forall e' f' k',
  outcome_state_match e f k Out_error (State e' f' Serror k')
| osm_return_none: forall k',
  destructCont k = destructCont k' ->
  outcome_state_match e f k (Out_return None)
                      (State e f (Sreturn None) k')
| osm_return_some: forall k' a v,
  destructCont k = destructCont k' ->
  eval_expr e f a (Some v) ->
  outcome_state_match e f k (Out_return (Some v))
                      (State e f (Sreturn (Some a)) k').

Lemma marrs_varr_remove_own_params pe e e1:
  forall params pargs pp,
    let mut := mut_args pe pp in
    let own := own_args pe pp in
    marrs_varr e e1 mut ->
    pargs_params params pargs = Some pp ->
    ~~ pargs_params_aliasing pargs (mut ++ own) ->
    all_root_paths own ->
    marrs_varr (remove_own_params own e) e1 mut.
Proof.
  intros * H PP NOALIAS ROOT.
  apply forallb_forall => - [[i0 l0] i] /=.
  move/forallb_forall: H => X I.
  rewrite remove_own_params_other; [|by apply: X I].
  move=> i0' l0' i' /[dup] I'. have:= I.
  case/(filter_In _ _ pp) => I1. case Pi: pe!i => [[]|] // _.
  case/(filter_In _ _ pp) => I2 Pi'.
  have NEQ: i <> i'.
  { move=> EQ; move: Pi'; by rewrite -EQ Pi. }
  have: l0' = [].
  { move/forallb_forall: ROOT => /(_ _ I'); by case l0'. }
  intros -> ->.
  have: incl (map fst (mut ++ own)) pargs.
  { have:= args_params_list_incl_l _ _ _ PP.
    move/(@incl_tran _ (map fst (mut ++ own))) => C; apply: C.
    { apply List.incl_map. move=>>; case/(in_app_or mut own).
      by apply mut_args_incl. by apply own_args_incl. } }
  case/pargs_params_aliasing_spec => _ A; exploit A => {A}.
  { case/(In_nth_error pp): I1 => n1 N1.
    case/(In_nth_error pp): I2 => n2 N2.
    case/(In_nth_error own): I' => n N.
    have: n1 <> n2.
    { move=> EQ; move: N1 N2; rewrite EQ => ->; congruence. }
    exists n1, n2, (n + length mut)%nat; repeat eexists => //.
    eapply args_params_nth_error_l with (1 := PP); eassumption.
    eapply args_params_nth_error_l with (1 := PP); eassumption.
    rewrite nth_error_app2. lia.
    rewrite -Nat.add_sub_assoc. lia.
    rewrite Nat.sub_diag Nat.add_comm; eassumption.
    all: by rewrite /= Pos.eqb_refl. }
  move=> A; move: NOALIAS; by rewrite A.
Qed.

Fixpoint valid_cont (f: function) (k: cont) :=
  match k with
  | Kstop =>
    match sig_res (fn_sig f) with
    | Tint _ _ => True
    | _ => False
    end
  | Kreturnto _ _ f _ k => valid_cont f k
  | Kseq _ k | Kblock k => valid_cont f k
  end.

Definition state_cont (s: state) :=
  match s with
  | Callstate _ _ k => k
  | State _ _ _ k => k
  | Returnstate _ _ _ k => k
  end.

Definition state_func (s: state) :=
  match s with
  | Callstate f _ _ => f
  | State _ f _ _ => f
  | Returnstate _ f _ _ => f
  end.

#[local]
Ltac seauto x := eauto using x, outcome_state_match with semanticsnb.

(*
Lemma exec_stmt_steps':
  forall f e s e' out,
    exec_stmt ge (eval_funcall ge) f e s e' out ->
    (match s with Scall _ _ _ => False | _ => True end) ->
    forall k,
      valid_cont f k ->
      exists S,
        star step_event ge (State e f s k) E0 S
        /\ outcome_state_match e' f k out S
        /\ valid_cont f (state_cont S).
Proof.
  intros * H. induction H; intros * => // _.
  (* apply eval_funcall_exec_stmt_ind2; intros *.
  (* funcall internal *)
  - case: out => //= [|[vres|]] B I E + _ O V +; rewrite V => + WT P k K VK.
    + case/(_ k VK) =>> [H [X VK']]; inv X.
      eapply star_left with (t1 := E0) => //. split=> //. econstructor; eassumption.
      eapply star_right with (1 := H) => {H} //. split=> //.
      case: k K VK {VK'} =>> _ //=. by inv WT. by econstructor.
    + case/(_ k VK) =>> [H [X VK']]; inv X.
      eapply star_left with (t1 := E0) => //. split=> //. econstructor; eassumption.
      eapply star_right with (1 := H) => {H} //. split=> //.
      have:= step_return_Some ge _ _ k' _ _ H2 WT P.
      rewrite -H1 {H1}; by case: k K VK.
    + case/(_ k VK) =>> [H [X VK']]; inv X.
      eapply star_left with (t1 := E0) => //. split=> //. econstructor; eassumption.
      eapply star_right with (1 := H) => {H} //. split=> //.
      have:= step_return_None ge _ _ k' WT.
      rewrite -H0 {H0}; by case: k K VK. *)
  (* skip *)
  - eexists; repeat split; by try econstructor.
  (* assign, normal case *)
  - intros; eexists; repeat split; seauto star_one.
  (* assign, invalid path *)
  - intros; eexists; repeat split; seauto star_one.
  (* assign, error during expression evaluation *)
  - intros; eexists; repeat split; seauto star_one.
  (* alloc, normal case *)
  - intros; eexists; repeat split; seauto star_one.
  (* alloc, error during size expression evaluation *)
  - intros; eexists; repeat split; seauto star_one.
  (* alloc, array too large *)
  - intros; eexists; repeat split; seauto star_one.
  (* free *)
  - intros; eexists; repeat split; seauto star_one.
  (* return None *)
  - intros; eexists; repeat split; by try econstructor.
  (* return Some, normal case *)
  - intros; eexists; repeat split; by try econstructor.
  (* return Some, error during returned expression evaluation *)
  - intros; eexists; repeat split; seauto star_one.
  (* assertion, normal case *)
  - case: b H; intros; eexists; repeat split; seauto star_one.
  (* assertion, error during condition evaluation *)
  - intros; eexists; repeat split; seauto star_one.
  (* conditional, normal case *)
  - case: b H H0 IHexec_stmt => ??. + k VK; case/(_ k VK) =>> [?[??]].
    all: intros; eexists; (split; [eapply star_left|]); eauto with semanticsnb.
  (* conditional, error during condition evaluation *)
  - intros; eexists; repeat split; seauto star_one.
  (* seq, normal case *)
  - intros VK. case/(_ (Kseq s2 k) VK): IHexec_stmt1 =>> [S1 [O1 V1]]; inv O1.
    case/(_ k VK): IHexec_stmt2 =>> [S2 [O2 V2]].
    eexists; repeat split; try eassumption.
    eapply star_left. split=> //; by econstructor.
    eapply star_trans. eassumption.
    eapply star_left. split=> //; by econstructor. eassumption.
    all: reflexivity.
  (* seq, continue / break / return / error in first part *)
  - intros VK. case/(_ (Kseq s2 k) VK): IHexec_stmt =>> [? [X V]].
    eexists; repeat split; seauto star_left.
    inv X => //; by econstructor.
  (* loop, normal case *)
  - intros VK. case/(_ (Kloop s k) VK): IHexec_stmt1 =>> [S1 [O1 V1]].
    case/(_ k VK): IHexec_stmt2 =>> [S2 [O2 V2]]. inv S2. { by inv O2. }
    case: H2; intros ->; inv 1.
    eexists; repeat split; try eassumption.
    destruct out1 => //; inv O1.
    + eapply star_left. split=> //; by econstructor.
      eapply star_trans. eassumption.
      eapply star_left. split=> //; by econstructor.
      eassumption. reflexivity. reflexivity. by destruct t2.
    + eapply star_left. split=> //; by econstructor.
      eapply star_trans. eassumption.
      { clear S1 V1. induction k' => //.
        - eapply star_left. split=> //; by econstructor.
          apply (IHk' H2). reflexivity.
        - injection H2 as <- <-.
          eapply star_left. split=> //; by econstructor.
          eassumption. reflexivity. }
      reflexivity. by destruct t2.
  (* loop, stop case (return, break or error) *)
  - intros VK. case/(_ (Kloop s k) VK): IHexec_stmt =>> [S1 [O1 V1]].
    move: H0; destruct out1 => //= _; inv O1.
    + eexists; repeat split; [|econstructor|eassumption].
      eapply star_left. split=> //; by econstructor.
      eapply star_trans. eassumption.
      { clear S1 V1; induction k' => //.
        - eapply star_left. split=> //; by econstructor.
          apply (IHk' H0). reflexivity.
        - injection H0 as <- <-.
          apply star_one; split=> //; by econstructor. }
      all: reflexivity.
    + eexists; repeat split; seauto star_left.
    + eexists; repeat split; seauto star_left.
    + eexists; repeat split; seauto star_left.
  (* continue *)
  - eexists; repeat split; by try econstructor.
  (* break *)
  - eexists; repeat split; by try econstructor.
  (* error *)
  - eexists; repeat split; by try econstructor.
Defined.
*)
Lemma exec_stmt_steps':
  (forall fd args e res,
    eval_funcall ge fd args e res ->
    match fd, res with
    | Internal f, Some vres =>
      forall k,
        is_Kreturnto_or_Kstop k ->
        valid_cont f k ->
        star step_event ge (Callstate f args k)
                        E0 (Returnstate e f vres k)
    | Internal f, None =>
      forall k,
        is_Kreturnto_or_Kstop k ->
        valid_cont f k ->
        exists f' e' k',
          valid_cont f' k' /\
          star step_event ge (Callstate f args k)
                          E0 (State e' f' Serror k')
    | _, _ => False
    end) ->
  (forall f e s e' out,
    exec_stmt ge (eval_funcall ge) f e s e' out ->
    forall k,
      valid_cont f k ->
      exists S,
        star step_event ge (State e f s k) E0 S
        /\ outcome_state_match e' f k out S
        /\ valid_cont (state_func S) (state_cont S)).
Proof.
  intros IHcall * H. induction H; intros *.
  (* skip *)
  - eexists; repeat split; by try econstructor.
  (* assign, normal case *)
  - intros; eexists; repeat split; seauto star_one.
  (* assign, invalid path *)
  - intros; eexists; repeat split; seauto star_one.
  (* assign, error during expression evaluation *)
  - intros; eexists; repeat split; seauto star_one.
  (* alloc, normal case *)
  - intros; eexists; repeat split; seauto star_one.
  (* alloc, error during size expression evaluation *)
  - intros; eexists; repeat split; seauto star_one.
  (* alloc, array too large *)
  - intros; eexists; repeat split; seauto star_one.
  (* free *)
  - intros; eexists; repeat split; seauto star_one.
  (* internal call *)
  - intros; destruct vres => //.
    (* function exits normally *)
    + eexists; repeat split.
      eapply star_left with (t1 := E0) => //. split=> //.
      eapply step_call_Internal; eassumption.
      eapply star_right with (t1 := E0) => //.
      by apply (IHcall _ _ _ _ H14).
      split=> //; apply step_returnstate; try eassumption.
      { move: H15 H3 H13 H12 => ++ /negbT; rewrite -H4 -H6.
        by eapply marrs_varr_remove_own_params. }
      { rewrite -H4; apply forallb_forall =>> /mut_args_incl.
        by apply (proj1 (forallb_forall _ _) H10). }
      econstructor. eassumption.
    (* function raises an error *)
    + case/(_ (Kreturnto idvar (remove_own_params own e) f mut k)): (IHcall _ _ _ _ H14) => //.
      move=> ferr [eerr] [kerr] [Vkerr] Serr.
      eexists; repeat split.
      eapply star_left with (t1 := E0) => //. split=> //.
      eapply step_call_Internal; eassumption.
      eassumption. econstructor. assumption.
  (* external call *)
  - intros; eexists; repeat split; seauto star_one.
  (* call, invalid argument *)
  - intros; eexists; repeat split; seauto star_one.
  (* call, a size test raises an error during evaluation *)
  - intros; eexists; repeat split; seauto star_one.
  (* call, invalid size parameters *)
  - intros; eexists; repeat split; seauto star_one.
  (* return None *)
  - intros; eexists; repeat split; by try econstructor.
  (* return Some, normal case *)
  - intros; eexists; repeat split; by try econstructor.
  (* return Some, error during returned expression evaluation *)
  - intros; eexists; repeat split; seauto star_one.
  (* assertion, normal case *)
  - case: b H; intros; eexists; repeat split; seauto star_one.
  (* assertion, error during condition evaluation *)
  - intros; eexists; repeat split; seauto star_one.
  (* conditional, normal case *)
  - move=> VK; case/(_ k VK): IHexec_stmt =>> [?[??]].
    eexists; split; [eapply star_left|]; eauto with semanticsnb.
  (* conditional, error during condition evaluation *)
  - intros; eexists; repeat split; seauto star_one.
  (* seq, normal case *)
  - intros VK.
    case/(_ (Kseq s2 k) VK): IHexec_stmt1 =>> [S1 [O1 V1]]; inv O1.
    case/(_ k VK): IHexec_stmt2 =>> [S2 [O2 V2]].
    eexists; repeat split; try eassumption.
    eapply star_left. split=> //; by econstructor.
    eapply star_trans. eassumption.
    eapply star_left. split=> //. by econstructor. eassumption.
    all: reflexivity.
  (* seq, exit / return / error in first part *)
  - intros VK. case/(_ (Kseq s2 k) VK): IHexec_stmt =>> [? [X V]].
    inv X => //.
    2-4: eexists; repeat split; seauto star_left.
    eexists; repeat split.
    eapply star_left. split=> //; econstructor.
    eapply star_trans. eassumption.
    apply star_one. split=> //; econstructor.
    all: econstructor || easy.
  (* loop, normal case *)
  - intros VK. case/(_ (Kseq (Sloop s) k) VK): IHexec_stmt1 =>> [S1 [O1 V1]].
    case/(_ k VK): IHexec_stmt2 =>> [S2 [O2 V2]].
    inv O1. eexists; repeat split; try eassumption.
    eapply star_left. split=> //; by econstructor.
    eapply star_trans. eassumption.
    eapply star_left. split=> //; by econstructor.
    eassumption. all: reflexivity.
  (* loop, stop case (return, break or error) *)
  - intros VK. case/(_ (Kseq (Sloop s) k) VK): IHexec_stmt =>> [S1 [O1 V1]].
    inv O1 => //.
    + eexists; repeat split.
      eapply star_left. split=> //; by econstructor.
      eapply star_trans. eassumption.
      eapply star_one. split=> //; by econstructor.
      all: econstructor || easy.
    + eexists; repeat split; seauto star_left.
    + eexists; repeat split; seauto star_left.
    + eexists; repeat split; seauto star_left.
  (* block *)
  - intros VK. case/(_ (Kblock k) VK): IHexec_stmt =>> [S1 [O1 V1]].
    inv O1 => //. 2: destruct n.
    1-4: eexists; repeat split;
         [ eapply star_left; [split=> //; econstructor| |reflexivity];
           eapply star_trans; [eassumption| |reflexivity];
           eapply star_one; split=> //; econstructor
         |econstructor|eassumption].
    all: eexists; repeat split; seauto star_left.
  (* exit *)
  - eexists; repeat split; by try econstructor.
  (* error *)
  - eexists; repeat split; by try econstructor.
Defined.

Lemma eval_funcall_steps':
  forall fd args e res,
    eval_funcall ge fd args e res ->
    match fd, res with
    | Internal f, Some vres =>
      forall k,
        is_Kreturnto_or_Kstop k ->
        valid_cont f k ->
        star step_event ge (Callstate f args k)
                        E0 (Returnstate e f vres k)
    | Internal f, None =>
      forall k,
        is_Kreturnto_or_Kstop k ->
        valid_cont f k ->
        exists f' e' k',
          valid_cont f' k' /\
          star step_event ge (Callstate f args k)
                          E0 (State e' f' Serror k')
    | _, _ => False
    end.
Proof.
  fix IH 5.
  intros *. case. intros *.
  case: out => //= [|[vres|]|] B I E _ -> WT P k K VK.
  - have:= exec_stmt_steps' IH _ _ _ _ _ E.
    case/(_ k VK) =>> [H [X VK']]; inv X.
    eapply star_left with (t1 := E0) => //. split=> //. econstructor; eassumption.
    eapply star_right with (1 := H) => {H} //. split=> //.
    case: k K VK {VK'} =>> _ //=. by inv WT. by econstructor.
  - have:= exec_stmt_steps' IH _ _ _ _ _ E.
    case/(_ k VK) =>> [H [X VK']]; inv X.
    eapply star_left with (t1 := E0) => //. split=> //. econstructor; eassumption.
    eapply star_trans with (1 := H) => {H} //.
    eapply star_left with (t1 := E0) => //. split=> //; econstructor; eassumption.
    elim: k' H1 {VK'}.
    + case: k K {VK} => // _ _. apply star_refl.
    + move=>> IHk /= EQ.
      eapply star_left. split=> //; by econstructor.
      apply: IHk EQ. reflexivity.
    + move=>> IHk /= EQ.
      eapply star_left. split=> //; by econstructor.
      apply: IHk EQ. reflexivity.
    + move=>> _; case: k K {VK} =>> //= _ [-> -> -> -> ->]. apply star_refl.
  - have:= exec_stmt_steps' IH _ _ _ _ _ E.
    case/(_ k VK) =>> [H [X VK']]; inv X.
    eapply star_left with (t1 := E0) => //. split=> //. econstructor; eassumption.
    eapply star_trans with (1 := H) => {H} //.
    eapply star_left with (t1 := E0) => //. split=> //; econstructor; eassumption.
    elim: k' H0 {VK'}.
    + case: k K {VK} => // _ _. apply star_refl.
    + move=>> IHk /= EQ.
      eapply star_left. split=> //; by econstructor.
      apply: IHk EQ. reflexivity.
    + move=>> IHk /= EQ.
      eapply star_left. split=> //; by econstructor.
      apply: IHk EQ. reflexivity.
    + move=>> _; case: k K {VK} =>> //= _ [-> -> -> -> ->]. apply star_refl.
  - have:= exec_stmt_steps' IH _ _ _ _ _ E.
    case/(_ k VK) =>> [H [X VK']]. inv X.
    repeat eexists; last first.
    eapply star_left with (t1 := E0) => //.
    split=> //; econstructor; eassumption.
    eassumption. assumption.
Qed.

Lemma eval_funcall_steps:
  forall fd args e vres,
    eval_funcall ge fd args e (Some vres) ->
    match fd with
    | Internal f =>
      forall k,
        is_Kreturnto_or_Kstop k ->
        valid_cont f k ->
        star step_event ge (Callstate f args k)
                        E0 (Returnstate e f vres k)
    | _ => False
    end.
Proof. intros *; apply eval_funcall_steps'. Qed.

Lemma eval_funcall_steps_error:
  forall fd args e,
    eval_funcall ge fd args e None ->
    match fd with
    | Internal f =>
      forall k,
        is_Kreturnto_or_Kstop k ->
        valid_cont f k ->
        exists f' e' k',
          valid_cont f' k' /\
          star step_event ge (Callstate f args k)
                          E0 (State e' f' Serror k')
    | _ => False
    end.
Proof. intros *; apply eval_funcall_steps'. Qed.

Lemma exec_stmt_steps:
  forall f e s e' out,
    exec_stmt ge (eval_funcall ge) f e s e' out ->
    forall k,
      valid_cont f k ->
      exists S,
        star step_event ge (State e f s k) E0 S
        /\ outcome_state_match e' f k out S
        /\ valid_cont (state_func S) (state_cont S).
Proof. intros *; apply exec_stmt_steps'. exact eval_funcall_steps'. Qed.

Lemma forever_silent_star {genv state: Type}:
  forall (step: genv -> state -> trace -> state -> Prop)
         ge s1 s2,
    star step ge s1 E0 s2 ->
    forever_silent step ge s2 ->
    forever_silent step ge s1.
Proof.
  intros *; have: E0 = E0 => //. move: {1 3}E0. induction 2 => //.
  move: IHstar H2 H0 H1; rewrite H; case: t1 => //; case: t2 => //.
  move=> + _ S1 S2 => /(_ eq_refl) /[apply].
  by apply forever_silent_intro.
Qed.

CoInductive forever_silent_plus {genv state: Type}
  (step: genv -> state -> trace -> state -> Prop) (ge: genv): state -> Prop :=
| forever_silent_plus_intro: forall s1 s2,
    plus step ge s1 E0 s2 ->
    forever_silent_plus step ge s2 ->
    forever_silent_plus step ge s1.

Theorem forever_silent_plus_forever_silent {genv state}
    {step: genv -> state -> trace -> state -> Prop}:
  forall (ge: genv) s,
    forever_silent_plus step ge s ->
    forever_silent step ge s.
Proof.
  cofix IH.
  intros * H; inv H.
  inv H0. apply eq_sym, Eapp_E0_inv in H3 as [-> ->]. inv H2.
  - econstructor. eassumption. by apply IH.
  - apply eq_sym, Eapp_E0_inv in H4 as [-> ->].
    econstructor. eassumption.
    apply IH. econstructor.
    econstructor; eauto. eassumption.
Qed.

Lemma evalinf_funcall_forever_plus:
  forall fd args k,
    evalinf_funcall ge fd args ->
    match fd with
    | Internal f =>
      valid_cont f k ->
      forever_silent_plus step_event ge (Callstate f args k)
    | _ => False
    end.
Proof.
  case=> //. 2: by inv 2. cofix IH.
  have @S : (forall f e s k,
            execinf_stmt ge (evalinf_funcall ge) f e s ->
            valid_cont f k ->
            forever_silent_plus step_event ge (State e f s k)).
  {
  cofix S.
  move=> ??? k X VK; inv X.
  - econstructor. apply plus_one; split=> //; econstructor; by try eassumption.
    by apply IH.
  - econstructor. apply plus_one; split=> //; econstructor; eassumption.
    destruct b; by apply S.
  - econstructor. apply plus_one; split=> //; econstructor; eassumption.
    by apply S.
  - case/exec_stmt_steps/(_ (Kseq s2 k)): H => // > [T [O V]]. inv O.
    econstructor.
    eapply plus_left'. split=> //; econstructor; eassumption.
    eapply plus_right. eassumption.
    split=> //; econstructor. reflexivity. reflexivity.
    by apply S.
  - econstructor. apply plus_one; split=> //; econstructor; eassumption.
    by apply S.
  - case/exec_stmt_steps/(_ (Kseq (Sloop s) k)): H => // > [T [O V]]. inv O.
    econstructor.
    eapply plus_left'. split=> //; econstructor.
    eapply plus_right. eassumption.
    split=> //; econstructor. reflexivity. reflexivity.
    by apply S.
  - econstructor. apply plus_one. split=> //; econstructor.
    by apply S.
  }
  clearbody S.
  { intros *; inv 1. econstructor.
    apply plus_one. split=> //; econstructor; eassumption. eauto. }
Qed.

Lemma evalinf_funcall_forever:
  forall fd args k,
    evalinf_funcall ge fd args ->
    match fd with
    | Internal f =>
      valid_cont f k ->
      forever_silent step_event ge (Callstate f args k)
    | _ => False
    end.
Proof.
  move=> fd ? k /evalinf_funcall_forever_plus => /(_ k).
  case: fd =>> // H VK.
  apply forever_silent_plus_forever_silent; by apply H.
Qed.

Lemma forever_error:
  forall e f k,
    forever_silent step_event ge (State e f Serror k).
Proof.
  cofix IH.
  intros; eapply forever_silent_intro.
  split=> //; apply step_error. apply IH.
Qed.

Theorem bigstep_semantics_sound:
  bigstep_sound (bigstep_semantics prog) (semantics prog).
Proof.
  constructor; intros.
(* termination *)
  inv H. econstructor; econstructor.
  split. econstructor; eauto.
  split. simpl.
  apply (eval_funcall_steps _ _ _ _ H2 Kstop eq_refl).
  by rewrite /= H1. red; auto.
  econstructor.
(* divergence *)
  inv H. econstructor.
  split. econstructor; eauto.
  have X := evalinf_funcall_forever (Internal f) [] Kstop.
  apply X => //. by rewrite /= H1.
(* error *)
  econstructor; split. econstructor; eauto.
  exploit (eval_funcall_steps_error _ _ _ H2 Kstop eq_refl).
  { by rewrite /= H1. }
  case=> f' [e'] [k'] [K'] S.
  eapply forever_silent_star. eassumption.
  by apply forever_error.
Qed.

End BIGSTEP_TO_TRANSITION.