Require Import String Coq.Lists.List.
Require Import BinNums BinInt.
Require Import Coq.Bool.Bool ssrbool.
Require Import PeanoNat.
Require Import Lia.

Import ListNotations.

Require Import Integers Floats Maps Errors Smallstep.
Require Import AST Memory Values Events Cminor Csharpminor.
Require Import Globalenvs.
Require Coqlib.

Module CValues := Values.

Require Import BUtils ListUtils Tactics PTreeaux.
Require Import BValues Types Ops Typing.
Require Import BEnv SemPath SemanticsCommon.
Require SemanticsBlocking.
Module B := Syntax.
Module Bsem := SemanticsBlocking.

Local Open Scope error_monad_scope.
Local Open Scope option_error_monad_scope.

Definition abort_sig :=
  {| AST.sig_args := [];
     AST.sig_res  := AST.Tvoid;
     AST.sig_cc   := AST.cc_default |}.

Definition calloc_sig :=
  {| AST.sig_args := [Tlong; Tlong];
     AST.sig_res  := Tptr;
     AST.sig_cc   := AST.cc_default |}.

Definition abort := AST.EF_external "abort" abort_sig.

Definition calloc := AST.EF_external "calloc" calloc_sig.

Axiom abort_sem:
  forall ge vargs m, Events.external_call abort ge vargs m E0 CValues.Vundef m.

Record fixed_ids := {
  ident_abort:  ident;
  ident_calloc: ident;
  label_error:  ident;
  label_code:   ident;
  label_code_neq_error: label_code <> label_error;
}.

Section BtoCSharpMinor.

Variable fids : fixed_ids.

Definition transl_typ (t: typ) : AST.typ :=
  match t with
  | Tvoid      => AST.Tint
  | Tbool      => AST.Tint
  | Tint _ _   => AST.Tint
  | Tint64 _   => AST.Tlong
  | Tfloat F32 => AST.Tsingle
  | Tfloat F64 => AST.Tfloat
  | Tarr _   => AST.Tlong
  end.

Definition transl_typ_to_rettype (t: typ) : AST.rettype :=
  match t with
  | Tvoid => AST.Tvoid
  | Tint I8 Unsigned => AST.Tint8unsigned
  | Tint I8 Signed => AST.Tint8signed
  | Tint I16 Unsigned => AST.Tint16unsigned
  | Tint I16 Signed => AST.Tint16signed
  | _ => AST.Tret (transl_typ t)
  end.

Definition transl_signature (sig: signature) : AST.signature :=
  AST.mksignature
    (map transl_typ sig.(sig_args))
    (transl_typ_to_rettype sig.(sig_res))
    AST.cc_default.

Definition typ_to_memory_chunk (t: typ) : memory_chunk :=
  match t with
  | Tvoid             => Mint8unsigned
  | Tbool             => Mint8unsigned
  | Tint I8 Unsigned  => Mint8unsigned
  | Tint I8 Signed    => Mint8signed
  | Tint I16 Unsigned => Mint16unsigned
  | Tint I16 Signed   => Mint16signed
  | Tint I32 _        => Mint32
  | Tint64 _          => Mint64
  | Tfloat F32        => Mfloat32
  | Tfloat F64        => Mfloat64
  | Tarr _          => Mptr
  end.

Definition transl_external_function (ef: B.external_function) : AST.external_function :=
  EF_external (B.ef_name ef) (transl_signature (B.ef_sig ef)).

Definition transl_const (c: Syntax.constant) : Csharpminor.constant :=
  match c with
  | B.Cint _ _ n => Ointconst n
  | B.Cint64 _ n => Olongconst n
  | B.Cfloat32 f => Osingleconst f
  | B.Cfloat64 f => Ofloatconst f
  | B.Cbool b    => Ointconst (if b then Int.one else Int.zero)
  end.

Definition transl_cast (from to: typ)
           : res (list Csharpminor.unary_operation) :=
  match from, to with
  | Tint   _        _, Tint    I8   Signed => OK [   Ocast8signed]
  | Tint   _        _, Tint    I8 Unsigned => OK [ Ocast8unsigned]
  | Tint   _        _, Tint   I16   Signed => OK [  Ocast16signed]
  | Tint   _        _, Tint   I16 Unsigned => OK [Ocast16unsigned]
  | Tint   _        _, Tint     _        _ => OK []
  | Tint   _   Signed, Tint64   _          => OK [   Olongofint]
  | Tint   _ Unsigned, Tint64   _          => OK [  Olongofintu]
  | Tint   _   Signed, Tfloat F32          => OK [ Osingleofint]
  | Tint   _ Unsigned, Tfloat F32          => OK [Osingleofintu]
  | Tint   _   Signed, Tfloat F64          => OK [  Ofloatofint]
  | Tint   _ Unsigned, Tfloat F64          => OK [ Ofloatofintu]
  | Tint64          _, Tint    I8   Signed => OK [Ointoflong;    Ocast8signed]
  | Tint64          _, Tint    I8 Unsigned => OK [Ointoflong;  Ocast8unsigned]
  | Tint64          _, Tint   I16   Signed => OK [Ointoflong;   Ocast16signed]
  | Tint64          _, Tint   I16 Unsigned => OK [Ointoflong; Ocast16unsigned]
  | Tint64          _, Tint   I32        _ => OK [    Ointoflong]
  | Tint64          _, Tint64   _          => OK []
  | Tint64     Signed, Tfloat F32          => OK [ Osingleoflong]
  | Tint64   Unsigned, Tfloat F32          => OK [Osingleoflongu]
  | Tint64     Signed, Tfloat F64          => OK [  Ofloatoflong]
  | Tint64   Unsigned, Tfloat F64          => OK [ Ofloatoflongu]
  | Tfloat        F32, Tint    I8   Signed => OK [  Ointofsingle;    Ocast8signed]
  | Tfloat        F32, Tint    I8 Unsigned => OK [ Ointuofsingle;  Ocast8unsigned]
  | Tfloat        F32, Tint   I16   Signed => OK [  Ointofsingle;   Ocast16signed]
  | Tfloat        F32, Tint   I16 Unsigned => OK [ Ointuofsingle; Ocast16unsigned]
  | Tfloat        F32, Tint   I32   Signed => OK [  Ointofsingle]
  | Tfloat        F32, Tint   I32 Unsigned => OK [ Ointuofsingle]
  | Tfloat        F32, Tint64       Signed => OK [ Olongofsingle]
  | Tfloat        F32, Tint64     Unsigned => OK [Olonguofsingle]
  | Tfloat        F32, Tfloat F32          => OK []
  | Tfloat        F32, Tfloat F64          => OK [Ofloatofsingle]
  | Tfloat        F64, Tint    I8   Signed => OK [   Ointoffloat;    Ocast8signed]
  | Tfloat        F64, Tint    I8 Unsigned => OK [  Ointuoffloat;  Ocast8unsigned]
  | Tfloat        F64, Tint   I16   Signed => OK [   Ointoffloat;   Ocast16signed]
  | Tfloat        F64, Tint   I16 Unsigned => OK [  Ointuoffloat; Ocast16unsigned]
  | Tfloat        F64, Tint   I32   Signed => OK [   Ointoffloat]
  | Tfloat        F64, Tint   I32 Unsigned => OK [  Ointuoffloat]
  | Tfloat        F64, Tint64       Signed => OK [  Olongoffloat]
  | Tfloat        F64, Tint64     Unsigned => OK [ Olonguoffloat]
  | Tfloat        F64, Tfloat F32          => OK [Osingleoffloat]
  | Tfloat        F64, Tfloat F64          => OK []
  | Tbool            , Tint     _        _ => OK []
  | Tbool            , Tint64            _ => OK [Olongofintu]
  |                 _,                   _ => Error [MSG "B->C#minor: Cannot translate cast."]
  end.

Definition transl_unary_operation (op: unary_operation) (k: operation_kind)
    : res Csharpminor.unary_operation :=
  match op, k with
  | Onotint,       OInt => OK Cminor.Onotint
  | Onotint,     OInt64 => OK Onotl
  | Oneg,          OInt => OK Onegint
  | Oneg,        OInt64 => OK Onegl
  | Oneg,      OFloat32 => OK Onegfs
  | Oneg,      OFloat64 => OK Onegf
  | Oabsfloat, OFloat32 => OK Oabsfs
  | Oabsfloat, OFloat64 => OK Oabsf
  |         _,        _ => Error [MSG "B->C#minor: Cannot translate unary operator."]
  end.

Definition transl_binarith_operation (op: binarith_operation) (k: operation_kind)
    : res Csharpminor.binary_operation :=
  match op, k with
  | Oadd,      OInt => OK Cminor.Oadd
  | Oadd,    OInt64 => OK Oaddl
  | Oadd,  OFloat32 => OK Oaddfs
  | Oadd,  OFloat64 => OK Oaddf
  | Osub,      OInt => OK Cminor.Osub
  | Osub,    OInt64 => OK Osubl
  | Osub,  OFloat32 => OK Osubfs
  | Osub,  OFloat64 => OK Osubf
  | Omul,      OInt => OK Cminor.Omul
  | Omul,    OInt64 => OK Omull
  | Omul,  OFloat32 => OK Omulfs
  | Omul,  OFloat64 => OK Omulf
  | Odivs,     OInt => OK Odiv
  | Odivs,   OInt64 => OK Odivl
  | Odivs, OFloat32 => OK Odivfs
  | Odivs, OFloat64 => OK Odivf
  | Odivu,     OInt => OK Cminor.Odivu
  | Odivu,   OInt64 => OK Odivlu
  | Omods,     OInt => OK Omod
  | Omods,   OInt64 => OK Omodl
  | Omodu,     OInt => OK Cminor.Omodu
  | Omodu,   OInt64 => OK Omodlu
  | Oand,      OInt => OK Cminor.Oand
  | Oand,    OInt64 => OK Cminor.Oandl
  | Oor,       OInt => OK Cminor.Oor
  | Oor,     OInt64 => OK Cminor.Oorl
  | Oxor,      OInt => OK Cminor.Oxor
  | Oxor,    OInt64 => OK Cminor.Oxorl
  | Oshl,      OInt => OK Cminor.Oshl
  | Oshl,    OInt64 => OK Cminor.Oshll
  | Oshr,      OInt => OK Cminor.Oshr
  | Oshr,    OInt64 => OK Cminor.Oshrl
  | Oshru,     OInt => OK Cminor.Oshru
  | Oshru,   OInt64 => OK Cminor.Oshrlu
  |     _,        _ => Error [MSG "B->C#minor: Cannot translate binary arithmetic operation."]
  end.

Definition transl_comparison (op: cmp_operation) (k: operation_kind) :=
  match op, k with
  | Oeq,     OInt => Ocmp   Ceq
  | One,     OInt => Ocmp   Cne
  | Olt,     OInt => Ocmp   Clt
  | Ole,     OInt => Ocmp   Cle
  | Ogt,     OInt => Ocmp   Cgt
  | Oge,     OInt => Ocmp   Cge
  | Oeq,   OInt64 => Ocmpl  Ceq
  | One,   OInt64 => Ocmpl  Cne
  | Olt,   OInt64 => Ocmpl  Clt
  | Ole,   OInt64 => Ocmpl  Cle
  | Ogt,   OInt64 => Ocmpl  Cgt
  | Oge,   OInt64 => Ocmpl  Cge
  | Oeq, OFloat32 => Ocmpfs Ceq
  | One, OFloat32 => Ocmpfs Cne
  | Olt, OFloat32 => Ocmpfs Clt
  | Ole, OFloat32 => Ocmpfs Cle
  | Ogt, OFloat32 => Ocmpfs Cgt
  | Oge, OFloat32 => Ocmpfs Cge
  | Oeq, OFloat64 => Ocmpf  Ceq
  | One, OFloat64 => Ocmpf  Cne
  | Olt, OFloat64 => Ocmpf  Clt
  | Ole, OFloat64 => Ocmpf  Cle
  | Ogt, OFloat64 => Ocmpf  Cgt
  | Oge, OFloat64 => Ocmpf  Cge
  end.

Definition transl_unsigned_comparison (op: cmpu_operation) (k: operation_kind) :=
  match op, k with
  | Oltu,   OInt => OK (Ocmpu  Clt)
  | Oleu,   OInt => OK (Ocmpu  Cle)
  | Ogtu,   OInt => OK (Ocmpu  Cgt)
  | Ogeu,   OInt => OK (Ocmpu  Cge)
  | Oltu, OInt64 => OK (Ocmplu Clt)
  | Oleu, OInt64 => OK (Ocmplu Cle)
  | Ogtu, OInt64 => OK (Ocmplu Cgt)
  | Ogeu, OInt64 => OK (Ocmplu Cge)
  |    _,      _ => Error [MSG "B->C#minor: Cannot translate unsigned comparison."]
  end.

Definition foo : unit. Proof. exact tt. Qed.
Definition ptr64 := match foo with tt => Archi.ptr64 end.

Lemma ptr64_value: ptr64 = Archi.ptr64. Proof. unfold ptr64. now destruct foo. Qed.
Lemma ptr64_true: ptr64 = true -> Archi.ptr64 = true. Proof. now rewrite ptr64_value. Qed.
Lemma ptr64_false: ptr64 = false -> Archi.ptr64 = false. Proof. now rewrite ptr64_value. Qed.

Fixpoint build_index_expr' (lidx: list expr) (lsz: list ident)
                           (acc:  expr) : option expr :=
  match lidx, lsz with
  | [], [] => Some acc
  | idx :: lidx, sz :: lsz =>
    build_index_expr' lidx lsz (Ebinop Oaddl (Ebinop Omull acc (Evar sz)) idx)
  | _, _ => None
  end.

Definition build_index_expr lidx lsz :=
  match lidx, lsz with
  | [], [] => Some (Econst (Olongconst Int64.zero))
  | idx :: lidx, _ :: lsz => build_index_expr' lidx lsz idx
  | _, _ => None
  end.

Fixpoint transl_expr (te: tenv) (sze: B.szenvi) (exp: B.expr) : res expr :=
  match exp with
  | B.Econst c => OK (Econst (transl_const c))
  | B.Ecast e from to =>
      do te  <- transl_expr te sze e;
      match from, to with
      | Tint _ _, Tbool => OK (Ebinop (Ocmp  Cne) te (Econst (Ointconst Int.zero)))
      | Tint64 _, Tbool => OK (Ebinop (Ocmpl Cne) te (Econst (Olongconst Int64.zero)))
      | _, _ =>
        do ops <- transl_cast from to;
        OK (fold_left (fun e op => Eunop op e) ops te)
      end
  | B.Eunop Onotbool _ e =>
      do te <- transl_expr te sze e;
      OK (Ebinop (Ocmpu Ceq) te
                             (Econst (Ointconst Int.zero)))
  | B.Eunop op k e =>
      do te <- transl_expr te sze e;
      do op <- transl_unary_operation op k;
      OK (Eunop op te)
  | B.Ebinop_arith ((Oshl | Oshr | Oshru) as op) k e1 e2 =>
      do op  <- transl_binarith_operation op k;
      do te1 <- transl_expr te sze e1;
      do te2 <- transl_expr te sze e2;
      match k with
      | OInt   =>
        OK (Ebinop op te1
              (Ebinop Cminor.Oand te2
                      (Econst (Ointconst (Int.sub Int.iwordsize Int.one)))))
      | OInt64 =>
        OK (Ebinop op te1
              (Ebinop Cminor.Oand te2
                      (Econst (Ointconst (Int.sub Int64.iwordsize' Int.one)))))
      | _ =>
        Error [MSG "B->C#minor: Cannot apply shifts to floating point numbers."]
      end
  | B.Ebinop_arith op k e1 e2 =>
      do op  <- transl_binarith_operation op k;
      do te1 <- transl_expr te sze e1;
      do te2 <- transl_expr te sze e2;
      OK (Ebinop op te1 te2)
  | B.Ebinop_cmp op k e1 e2 =>
      do te1 <- transl_expr te sze e1;
      do te2 <- transl_expr te sze e2;
      OK (Ebinop (transl_comparison op k) te1 te2)
  | B.Ebinop_cmpu op k e1 e2 =>
      do op  <- transl_unsigned_comparison op k;
      do te1 <- transl_expr te sze e1;
      do te2 <- transl_expr te sze e2;
      OK (Ebinop op te1 te2)
  | B.Eacc (i, sp) =>
      do** t    <- ["B->C#minor: " ++ (BinaryString.of_pos i) ++ " is not declared."]
                   te!i;
      do** llsz <- ["B->C#minor: " ++ (BinaryString.of_pos i) ++ " is not an array."]
                   sze!i;
      let transl_syn_path_elem_list := fix f t llsz sp e {struct sp} :=
        match sp, t, llsz with
        | [], _, _ => OK e
        | s :: sp, Tarr t, lsz :: llsz =>
            do telt <- transl_syn_path_elem te sze lsz s t e;
            f t llsz sp telt
        | _, _, _ => Error [MSG "B->C#minor: Invalid path."]
        end in
      transl_syn_path_elem_list t llsz sp (Evar i)
  end
with transl_syn_path_elem te sze lsz s t e :=
  match s with
  | B.Scell lidx =>
      let transl_idxlist :=
        fix transl_idxlist lidx :=
          match lidx with
          | [] => OK []
          | idx :: lidx =>
            do r    <- transl_idxlist lidx;
            do tidx <- transl_expr te sze idx;
            OK (tidx :: r)
          end in
      do  tlidx <- transl_idxlist lidx;
      do* idx   <- build_index_expr tlidx lsz;
      OK (Eload
            (typ_to_memory_chunk t)
            (Ebinop Oaddl e
               (Ebinop Omull
                  (Econst (Olongconst (Int64.repr (typ_sizeof t)))) idx)))
  end.


Definition transl_exprlist (te: tenv) (sze: B.szenvi) :=
  fix transl_exprlist (le: list B.expr) : res (list expr) :=
  match le with
  | [] => OK []
  | e :: le =>
    do tle <- transl_exprlist le;
    do te  <- transl_expr te sze e;
    OK (te :: tle)
  end.

Definition transl_syn_path_elem_list (te: tenv) (sze: B.szenvi) :=
  fix f (t: typ) (llsz: list (list ident))
        (sp: list B.syn_path_elem) (e: expr) {struct sp} :=
  match sp, t, llsz with
  | [], _, _ => OK e
  | s :: sp, Tarr t, lsz :: llsz =>
      do telt <- transl_syn_path_elem te sze lsz s t e;
      f t llsz sp telt
  | _, _, _ => Error [MSG "B->C#minor: Invalid path."]
  end.

Definition transl_syn_path (te: tenv) (sze: B.szenvi) (p: B.syn_path)
                           : res expr :=
  let (i, sp) := p in
  do** t    <- ["B->C#minor: " ++ (BinaryString.of_pos i) ++ " is not declared."]
                 te!i;
  do** llsz <- ["B->C#minor: " ++ (BinaryString.of_pos i) ++ " is not an array."]
                 sze!i;
  transl_syn_path_elem_list te sze t llsz sp (Evar i).

Definition transl_syn_path_list (te: tenv) (sze: B.szenvi) (lp: list B.syn_path) :=
  list_map_error (transl_syn_path te sze) lp.

Lemma transl_Eacc:
  forall te sze p,
    transl_expr te sze (B.Eacc p) = transl_syn_path te sze p.
Proof. reflexivity. Qed.

Lemma transl_Scell:
  forall te sze lsz lidx t e,
    transl_syn_path_elem te sze lsz (B.Scell lidx) t e
    = do  tlidx <- transl_exprlist te sze lidx;
      do* idx   <- build_index_expr tlidx lsz;
      OK (Eload
            (typ_to_memory_chunk t)
            (Ebinop Oaddl e
               (Ebinop Omull
                  (Econst (Olongconst (Int64.repr (typ_sizeof t)))) idx))).
Proof. reflexivity. Qed.

Fixpoint transl_stmt (ge: genv) (f: B.function) (s: Syntax.stmt)
                     : res stmt :=
  let te := f.(B.fn_tenv) in
  let sze := f.(B.fn_szenv') in
  match s with
  | B.Sskip => OK Sskip
  | B.Sassign p exp =>
      do** ty <- ["B->C#minor: Invalid access."] B.get_tenv_syn_path (B.fn_tenv f) p;
      do t    <- transl_syn_path te sze p;
      do texp <- transl_expr te sze exp;
      match t with
      | Eload ch e => OK (Sstore ch e texp)
      | Evar i =>
          let e := match ty with
                   | Tint I8 Unsigned  => Eunop Ocast8unsigned texp
                   | Tint I8 Signed    => Eunop Ocast8signed texp
                   | Tint I16 Unsigned => Eunop Ocast16unsigned texp
                   | Tint I16 Signed   => Eunop Ocast16signed texp
                   | _ => texp
                   end in
          OK (Sset i e)
      | _ => Error [MSG "B->C#minor: Wrong path access translation."]
      end
  | B.Salloc i =>
      do** llsz <- ["B->C#minor: Undefined variable."] (B.fn_szenv f)!i;
      do** llsz' <- ["B->C#minor: Undefined variable."] (B.fn_szenv' f)!i;
      do** t <- ["B->C#minor: Undefined variable."] (B.fn_tenv f)!i;
      match llsz, llsz', t with
      | (sz :: []) :: [], (isz :: []) :: [], Tarr t =>
        do tsz <- transl_expr te sze sz;
        OK (Sseq
             (Sset isz tsz)
             (Sseq (Scall (Some i) calloc_sig (Eaddrof (ident_calloc fids))
                      [tsz; Econst (Olongconst (Int64.repr (typ_sizeof t)))])
                   (Sifthenelse
                     (Ebinop (Ocmplu Ceq) (Evar i) (Econst (Olongconst Int64.zero)))
                     (Sgoto (label_error fids)) Sskip)))
      | _, _, Tarr _ => Error [MSG ("B->C#minor: Only one dimensional arrays" ++
                                    " containing primitive values can be" ++
                                    " dynamically allocated.")]
      | _, _, _ => Error [MSG "B->C#minor: Only arrays can be dynamically allocated."]
      end
  | B.Sfree i =>
      do** llsz <- ["B->C#minor: Undefined variable."] (B.fn_szenv f)!i;
      do** t <- ["B->C#minor: Undefined variable."] (B.fn_tenv f)!i;
      match llsz, t with
      | (_ :: []) :: [], Tarr t =>    
        OK (Sbuiltin None EF_free [Evar i])
      | _, Tarr _ => Error [MSG ("B->C#minor: Only one dimensional arrays" ++
                                 " containing primitive values can be" ++
                                 " freed.")]
      | _, _ => Error [MSG "B->C#minor: Only arrays can be freed."]
      end
  | B.Scall idvar idf args =>
      do** fd  <- ["B->C#minor: Function doesn't exists."] ge!idf;
      do targs <- transl_syn_path_list (B.fn_tenv f) (B.fn_szenv' f) args;
      match fd with
      | B.Internal f =>
          OK (Scall idvar (transl_signature (B.fn_sig f))
                (Eaddrof idf) targs)
      | B.External ef =>
          OK (Scall idvar (transl_signature (B.ef_sig ef))
                (Eaddrof idf) targs)
      end
  | B.Sreturn None =>
      OK (Sreturn None)
  | B.Sreturn (Some exp) =>
      do texp <- transl_expr te sze exp;
      OK (Sreturn (Some texp))
  | B.Sseq s1 s2 =>
      do ts1 <- transl_stmt ge f s1;
      do ts2 <- transl_stmt ge f s2;
      OK (Sseq ts1 ts2)
  | B.Sassert c =>
      do tc <- transl_expr te sze c;
      OK (Sifthenelse tc Sskip (Sgoto (label_error fids)))
  | B.Sifthenelse c s1 s2 =>
      do ts1 <- transl_stmt ge f s1;
      do ts2 <- transl_stmt ge f s2;
      do tc <- transl_expr te sze c;
      OK (Sifthenelse tc ts1 ts2)
  | B.Sloop s =>
      do ts <- transl_stmt ge f s;
      OK (Sloop ts)
  | B.Sblock s =>
      do ts <- transl_stmt ge f s;
      OK (Sblock ts)
  | B.Sexit n => OK (Sexit n)
  | B.Serror =>
    OK (Sgoto (label_error fids))
  end.

Definition size_vars_assignments (f: B.function) : res stmt :=
  fold_right_res (fun s i =>
    do* llsz  <- (B.fn_szenv f)!i;
    do* llsz' <- (B.fn_szenv' f)!i;
    fold_right2_res (fun s lsz lsz' =>
        fold_right2_res (fun s exp i =>
                   do te <- transl_expr (B.fn_tenv f) (B.fn_szenv' f) exp;
                   OK (Sseq (Sset i te) s)
        ) lsz lsz' s
    ) llsz llsz' s
  ) (B.fn_params f) Sskip.

Definition transl_function (ge: genv) (f: B.function) : res function :=
  do assigns <- size_vars_assignments f;
  do tbody <- transl_stmt ge f f.(B.fn_body);
  let tbody := 
    Sseq
      (Sseq (Sgoto (label_code fids))
            (Slabel (label_error fids)
                    (Sseq (Scall None abort_sig (Eaddrof (ident_abort fids)) [])
                          (Sgoto (label_error fids)))))
      (Slabel (label_code fids) (Sseq assigns tbody)) in
  OK (mkfunction (transl_signature f.(B.fn_sig))
                 f.(B.fn_params) [] f.(B.fn_vars) tbody).

Definition transl_fundef (ge: genv) (fd: B.fundef) : res fundef :=
  match fd with
  | B.Internal f =>
      do tf <- transl_function (ge: genv) f;
      OK (Internal tf)
  | B.External ef =>
      OK (External (transl_external_function ef))
  end.

Definition transl_fundefs (ge: genv) (lfd: list (ident * B.fundef))
  : res (list (ident * globdef fundef unit)) :=
  list_map_error (fun fd =>
    do tfd <- transl_fundef ge (snd fd);
    OK (fst fd, Gfun tfd)) lfd.

Definition globdefs_list {F V: Type} (fds: list (ident * F)) :=
  map (fun def => (fst def, Gfun (V := V) (snd def))) fds.

Lemma globdefs_list_fst:
  forall F V fds,
    map fst (@globdefs_list F V fds) = map fst fds.
Proof. induction fds. auto. simpl. rewrite IHfds. auto. Qed.

Definition transl_program' (hfuncs: list (ident * fundef))
                           (ident_abort_in_hfuncs: In (ident_abort fids, External abort) hfuncs)
                           (ident_calloc_in_hfuncs: In (ident_calloc fids, External calloc) hfuncs)
                           (p: B.program) : res program :=
  let ge := build_env (PTree.empty B.fundef) p.(B.prog_defs) in
  do tfds <- transl_fundefs ge p.(B.prog_defs);
  OK (mkprogram (globdefs_list hfuncs ++ tfds)
                (map fst p.(B.prog_defs)) p.(B.prog_main)).

Definition find_abort_ident (hfuncs: list (ident * Csharpminor.fundef))
                           : option {id | In (id, AST.External abort) hfuncs}.
  set (res := filter (fun '(id, fd) => match fd with
                                       | AST.External (AST.EF_external "abort" s) =>
                                          match AST.signature_eq s abort_sig with
                                          | left _ => true
                                          | right _ => false
                                          end
                                       | _ => false end) hfuncs).
  refine (match res
                as t return res = t -> option {id | In (id, AST.External abort) hfuncs} with
          | [] => fun _ => None
          | x :: _ => fun H => Some _
          end eq_refl).
  destruct x as [id fd]. exists id. unfold res in H.
  pose proof (in_eq (id, fd) l) as Hin. rewrite <- H in Hin.
  apply filter_In in Hin as [Hin H']. clear H. repeat destruct_match H'.
  unfold abort. congruence.
Defined.

Definition find_calloc_ident (hfuncs: list (ident * Csharpminor.fundef))
                             : option {id | In (id, AST.External calloc) hfuncs}.
  set (res := filter (fun '(id, fd) => match fd with
                                       | AST.External (AST.EF_external "calloc" s) =>
                                          match AST.signature_eq s calloc_sig with
                                          | left _ => true
                                          | right _ => false
                                          end
                                       | _ => false end) hfuncs).
  refine (match res
                as t return res = t -> option {id | In (id, AST.External calloc) hfuncs} with
          | [] => fun _ => None
          | x :: _ => fun H => Some _
          end eq_refl).
  destruct x as [id fd]. exists id. unfold res in H.
  pose proof (in_eq (id, fd) l) as Hin. rewrite <- H in Hin.
  apply filter_In in Hin as [Hin H']. clear H. repeat destruct_match H'.
  unfold calloc. congruence.
Defined.

End BtoCSharpMinor.

Definition transl_program (hfuncs: list (ident * Csharpminor.fundef))
                          (label_error label_code: ident)
                          (p: B.program) :=
  match find_abort_ident hfuncs with
  | None => Error [MSG "B->C#minor: abort is not declared."]
  | Some (exist _ id_abort Habort) =>
    match find_calloc_ident hfuncs with
    | None => Error [MSG "B->C#minor: calloc is not declared."]
    | Some (exist _ id_calloc Hcalloc) =>
      match Coqlib.list_norepet_dec Pos.eq_dec (map fst hfuncs) with
      | left Hnorepet =>
        match Coqlib.list_disjoint_dec Pos.eq_dec (map fst (B.prog_defs p)) (map fst hfuncs) with
        | left Hdisjoint => 
          match Pos.eqb_spec label_code label_error with
          | ReflectT _ _ =>
            Error [MSG "B->C#minor: error and code labels are the same."]
          | ReflectF _ Hcode =>
            let fids := {| ident_abort := id_abort;
                          ident_calloc := id_calloc;
                          label_error := label_error;
                          label_code  := label_code;
                          label_code_neq_error := Hcode
                        |} in
            transl_program' fids hfuncs Habort Hcalloc p
          end
        | right _ => Error [MSG "B->C#minor: Redefining builtin functions is not allowed."]
        end
      | right _ => Error [MSG "B->C#minor: A builtin function is defined twice."]
      end
    end
  end.
