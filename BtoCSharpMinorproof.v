Require Import ssreflect ssrbool ssrfun.
Require Import String Coq.Lists.List.
Require Import BinNums BinInt.
Require Import Coq.Bool.Bool.
Require Import PeanoNat.
Require Import Lia.

Import ListNotations.

Require Import Integers Floats Maps Errors Smallstep.
Require Import AST Memory Values Events Cminor Csharpminor.
Require Import Globalenvs.
Require Coqlib.

Module CValues := Values.

Require Import BUtils ListUtils Tactics PTreeaux.
Require Import BValues Types Ops Typing.
Require Import BEnv SemPath Alias Validity SemanticsCommon.
Require SemanticsBlocking Bfacts.
Module B := Syntax.
Module Bsem := SemanticsBlocking.

Require Import BtoCSharpMinor.

Section BtoCSharpMinorProof.

Variable fids : fixed_ids.

Let transl_stmt := transl_stmt fids.
Let transl_function := transl_function fids.
Let transl_fundef := transl_fundef fids.
Let transl_fundefs := transl_fundefs fids.
Let transl_program' := transl_program' fids.

Local Open Scope option_error_monad_scope.
Local Open Scope error_monad_scope.

(*****************************************************************************)
(*                                                                           *)
(*                     TRANSLATION OF SEMANTICS STATES                       *)
(*                                                                           *)
(*****************************************************************************)

Inductive status := Visible | Hidden (p: sem_path).

Definition trmem := nat -> sem_path -> option (block * ptrofs * status).

Definition transl_value (tr: trmem) (n: nat) (p: option sem_path) (v: value) :=
  match v with
  | Vundef      => CValues.Vundef
  | Vbool false => CValues.Vfalse
  | Vbool true  => CValues.Vtrue
  | Vint n      => CValues.Vint n
  | Vint64 n    => Vlong n
  | Vfloat32 f  => Vsingle f
  | Vfloat64 f  => Vfloat f
  | Varr lv     => match p with
                   | None   => CValues.Vundef
                   | Some p =>
                       match tr n p with
                       | None => CValues.Vundef
                       | Some (b, ofs, s) => Vptr b ofs
                       end
                   end
  end.

Definition transl_valuelist (tr: trmem) (n: nat)
                            (lv: list (option sem_path * value)) : list val :=
  map (fun '(p, v) => transl_value tr n p v) lv.

Fixpoint complete_env (vars: list ident) (e: env) : env :=
  match vars with
  | []         => e
  | id :: vars =>
      match e!id with
      | None   => complete_env vars (PTree.set id Vundef e)
      | Some _ => complete_env vars e
      end
  end.

Definition match_env (tr: trmem) (n: nat) (e: env) (te: temp_env) :=
  forall (i: ident) v,
    e!i = Some v ->
    te!i = Some (transl_value tr n (Some (i, [])) v).

Fixpoint match_cont (tr: trmem) (n: nat)
                    (ge: genv)  (f: option B.function)
                    (k: Bsem.cont) (tk: cont) : Prop :=
  match k, tk with
  | Bsem.Kstop, Kstop => True
  | Bsem.Kseq s k, Kseq ts tk =>
      match f with
      | None    => False
      | Some f' =>
          transl_stmt ge f' s = OK ts /\ match_cont tr n ge f k tk
      end
  | Bsem.Kblock k, Kblock tk => match_cont tr n ge f k tk
  | Bsem.Kreturnto id e f _ k, Kcall id' tf PTree.Empty te tk =>
      match n with
      | O    => False
      | S n' =>
          id = id' /\
          transl_function ge f = OK tf /\
          match_env tr n' e te /\
          match_cont tr n' ge (Some f) k tk
      end
  | _, _ => False
  end.

Lemma match_cont_None_Some:
  forall tr N ge f k tk,
    match_cont tr N ge None k tk ->
    match_cont tr N ge (Some f) k tk.
Proof. induction k; do 4 case=> //=. Qed.

Notation "X 'defined'" := (exists x, X = Some x) (at level 100, no associativity).

Lemma not_defined_is_None {A: Type}:
  forall (x: option A), ~ (x defined) -> x = None.
Proof. intros []. intro H. elim H. eauto. easy. Qed.

Section INVARIANTS.

Import Syntax SemanticsCommon SemanticsBlocking.

Record wf_trmem (tr: trmem) (N: nat) : Prop :=
  mk_wf_trmem {
      trmem_prefix:
        forall n id pl1, tr n (id, pl1) defined ->
                    forall pl2, prefix sem_path_elem_beq pl2 pl1 = true ->
                           tr n (id, pl2) defined;
      trmem_max:
        forall n p, (n > N)%nat -> ~ (tr n p defined);
      trmem_hid_sep:
        forall n p p' b1 o1 p1 b2 o2 p2, tr n p  = Some (b1, o1, Hidden p1) ->
                                    tr n p' = Some (b2, o2, Hidden p2) ->
                                    p  <> p' ->
                                    p1 <> p2;
      trmem_hid:
        forall n i l b o i' l', (n < N)%nat ->
                           tr n (i, l) = Some (b, o, Hidden (i', l')) ->
                           exists s, tr (S n) (i', l') = Some (b, o, s)
    }.

Definition visible_separated (tr: trmem) (n: nat) (pe: penv) :=
  forall i l b o p, tr n (i, l) = Some (b, o, Visible) ->
               pe!i = Some p ->
               perm_le Mutable p ->
               (forall m p' b' o', (m < n)%nat ->
                              tr m p' = Some (b', o', Visible) ->
                              b <> b') /\
               (forall p' b' o' s', (i, l) <> p' ->
                               tr n p' = Some (b', o', s') ->
                               b <> b').

Definition hidden_marrs (tr: trmem) (n: nat) (marrs: list (sem_path * ident)) :=
  forall i l i' l',
    (option_map snd (tr n (i, l)) = Some (Hidden (i', l')) ->
     exists lc, In ((i, lc), i') marrs /\ lc ++ l' = l) /\
    (tr n (i, l) defined ->
     (exists lc, In ((i, lc), i') marrs /\ lc ++ l' = l) ->
     option_map snd (tr n (i, l)) = Some (Hidden (i', l'))).

Definition origin_defined (tr: trmem) (n: nat)
                          (marrs: list (sem_path * ident)) :=
  forall ic lc i l, In ((ic, lc), i) marrs ->
               (tr (S n) (i, l) defined) <-> (tr n (ic, lc ++ l) defined).

Definition marrs_no_aliasing (marrs: list (sem_path * ident)) :=
  forall p i p' i', In (p, i) marrs ->
                    In (p', i') marrs ->
                    (p, i) <> (p', i') ->
                    ~~ sem_path_prefix p p'.

Definition hidden_is_mut (tr: trmem) (n: nat) (pe: penv) :=
  forall p i l b o, tr n p = Some (b, o, Hidden (i, l)) ->
                    pe!i = Some Mutable.

Definition mutable_list (m: list (sem_path * ident)) (pe: penv) :=
  forall i l i', In (i, l, i') m -> pe!i' = Some Mutable.

Lemma mutable_list_hidden_is_mut tr n pe m:
  hidden_marrs tr n m ->
  mutable_list m pe ->
  hidden_is_mut tr n pe.
Proof.
  move=> HM MLIST [i l] i' l' b o T.
  have:= proj1 (HM i l i' l'). rewrite T.
  case/(_ eq_refl) =>> [IN _]. exact: MLIST IN.
Qed.

Definition owned_at_offset_zero (tr: trmem) (n: nat) (pe: penv) :=
  forall i l b o s, tr n (i, l) = Some (b, o, s) ->
                    pe!i = Some Owned ->
                    o = Ptrofs.zero.

Definition blk_ofs_preserved (tr: trmem) (n: nat) :=
  forall i l b o i' l' b' o' s',
    tr n (i, l) = Some (b, o, Hidden (i', l')) ->
    tr (S n) (i', l') = Some (b', o', s') ->
    b = b' /\ o = o'.

Fixpoint valid_trmem_cont (tr: trmem) (n: nat) (pe: penv) (k: Bsem.cont) :=
  match k with
  | Kstop => True
  | Kseq _ k | Bsem.Kblock k => valid_trmem_cont tr n pe k
  | Kreturnto _ E F marrs k =>
      match n with
      | O   => False
      | S m =>
          visible_separated tr m (B.fn_penv F) /\
          hidden_marrs tr m marrs /\
          origin_defined tr m marrs /\
          blk_ofs_preserved tr m /\
          marrs_no_aliasing marrs /\
          Coqlib.list_norepet marrs /\
          mutable_list marrs pe /\
          owned_at_offset_zero tr m (B.fn_penv F) /\
          valid_trmem_cont tr m (B.fn_penv F) k
      end
  end.

Fixpoint match_tr_value (tr: trmem) (n: nat) (p: sem_path) (v: value) (t: typ) :=
  match v, t with
  | Varr lv, Tarr t =>
      match tr n p with
      | Some (_, _, _) =>
          let (id, pl) := p in
          forall idx,
            (idx < length lv)%nat ->
            match_tr_value tr n (id, pl ++ [Pcell idx]) (nth idx lv Vundef) t
      | _ => False
      end
  | _, _ => True
  end.

Fixpoint match_mem_value (tr: trmem) (n: nat) (p: sem_path) (m: Memory.mem)
                         (v: value) (t: typ) :=
  match v, t with
  | Varr lv, Tarr t' =>
      let chunk := typ_to_memory_chunk t' in
      match tr n p with
      | None => False
      | Some (b, ofs, Visible) =>
          let (id, pl) := p in
          (Mem.load Mptr m b (- size_chunk Mptr)
            = Some (Vptrofs (Ptrofs.mul (Ptrofs.repr (typ_sizeof t'))
                                        (Ptrofs.repr (Z.of_nat (length lv))))) /\
           Mem.valid_access m Mptr b (- size_chunk Mptr) Freeable) /\
          forall idx,
            (idx < length lv)%nat ->
            Ptrofs.unsigned ofs +
              typ_sizeof t' * Z.of_nat idx <= Ptrofs.max_unsigned - typ_sizeof t' /\
            Mem.valid_access m chunk b
              (Ptrofs.unsigned
                 (Ptrofs.add ofs
                    (Ptrofs.mul
                       (Ptrofs.repr (typ_sizeof t'))
                       (Ptrofs.repr (Z.of_nat idx))))) Freeable /\
            Mem.loadv chunk m
              (Vptr b (Ptrofs.add ofs
                         (Ptrofs.mul
                            (Ptrofs.repr (typ_sizeof t'))
                            (Ptrofs.repr (Z.of_nat idx)))))
            = Some (transl_value tr n (Some (id, pl ++ [Pcell idx]))
                                      (nth idx lv Vundef)) /\
            match_mem_value tr n (id, pl ++ [Pcell idx]) m (nth idx lv Vundef) t'
      | Some _ => match_tr_value tr n p v t
      end
  | _, _ => True
  end.

Lemma match_mem_value_spec tr n m:
  forall v t i l,
    match_mem_value tr n (i, l) m v t <->
    forall l' lv t', get_value_path l' v = Some (Varr lv) ->
                   get_type_path l' t = Some (Tarr t') ->
                   (forall l'' s, prefix sem_path_elem_beq l'' l' = true ->
                             l'' <> l' ->
                             option_map snd (tr n (i, l ++ l'')) = Some s ->
                             s = Visible) ->
                   let chunk := typ_to_memory_chunk t' in
                   exists b o s, tr n (i, l ++ l') = Some (b, o, s) /\
                            (s = Visible ->
                             (Mem.load Mptr m b (- size_chunk Mptr)
                              = Some (Vptrofs (Ptrofs.mul (Ptrofs.repr (typ_sizeof t'))
                                                          (Ptrofs.repr (Z.of_nat (length lv))))) /\
                              Mem.valid_access m Mptr b (- size_chunk Mptr) Freeable) /\
                             forall idx,
                               (idx < length lv)%nat ->
                               Ptrofs.unsigned o +
                                 typ_sizeof t' * Z.of_nat idx <= Ptrofs.max_unsigned - typ_sizeof t' /\
                               Mem.valid_access m chunk b
                                 (Ptrofs.unsigned
                                    (Ptrofs.add o
                                       (Ptrofs.mul
                                          (Ptrofs.repr (typ_sizeof t'))
                                          (Ptrofs.repr (Z.of_nat idx))))) Freeable /\
                               Mem.loadv chunk m
                                 (Vptr b (Ptrofs.add o
                                            (Ptrofs.mul
                                               (Ptrofs.repr (typ_sizeof t'))
                                               (Ptrofs.repr (Z.of_nat idx)))))
                               = Some (transl_value tr n (Some (i, l ++ l' ++ [Pcell idx]))
                                                         (nth idx lv Vundef))) /\
                            ((exists p, s = Hidden p) -> match_tr_value tr n (i, l ++ l')
                                                    (Varr lv) (Tarr t')).
Proof.
  induction v using value_ind'; destruct t; simpl; intros.
  all: split; [destruct l'; [|destruct s0 || destruct s]|].
  all: try (discriminate || easy).
  - simpl. intros * [= ->] [= <-]. rewrite app_nil_r.
    destruct (tr n (i, l0)) as [[[blk ofs] s]|]; try contradiction.
    exists blk, ofs, s. split; [reflexivity|]. split; [intros ->; split|].
    easy. intros * Hidx. decompose [and] (proj2 H _ Hidx). auto.
    now intros [? ->].
  - simpl. case nth_error eqn:Hv; try discriminate. simpl.
    destruct (tr n (i, l0)) as [[[]]|] eqn:Htr; try contradiction. destruct s.
    + assert (n0 < length l)%nat as Hn0 by (apply nth_error_Some; now rewrite Hv).
      decompose [and] (proj2 H _ Hn0). clear H. rewrite (nth_error_nth _ _ _ Hv) in H4.
      intros * Hvl' Htl'. apply nth_error_In in Hv.
      apply (proj1 (Forall_forall _ _)) with (2 := Hv) in IH.
      pose proof (proj1 (IH _ _ _) H4 _ _ _ Hvl' Htl') as H. clear H4. simpl in H.
      intro HVis. case H as (blk & ofs & s & Htr' & H).
      intro l''. specialize (HVis (Pcell n0 :: l'')). simpl in HVis.
      rewrite (internal_nat_dec_lb _ _ eq_refl) in HVis. rewrite <- app_assoc.
      intros * Hpref Hneq. apply HVis. assumption. congruence.
      exists blk, ofs, s. split. rewrite <- app_assoc in Htr'. assumption.
      destruct H as [HV HH]. split; [intros ->; split|]. by case (HV eq_refl).
      intros Hs idx. have HV' := proj2 (HV eq_refl) Hs idx.
      now rewrite <- app_assoc in HV'. now rewrite <- app_assoc in HH.
    + clear IH. intros * Hvl' Htl' HVis.
      specialize (HVis [] (Hidden p) eq_refl (@nil_cons _ _ _)).
      rewrite app_nil_r in HVis. rewrite Htr in HVis. now specialize (HVis eq_refl).
  - intro H. pose proof H as H'.
    specialize (H [] l t eq_refl eq_refl). rewrite app_nil_r in H.
    case H as (b' & o' & s & Htr & H); try easy. intros *. now case l''.
    destruct H as [HV HH]. rewrite Htr. destruct s.
    + specialize (HV eq_refl). split. by case HV. intros * Hidx.
      decompose [and] (proj2 HV _ Hidx). repeat (split; [try easy|]).
      assert (In (nth idx l Vundef) l) as Hin by now apply nth_In.
      apply (proj1 (Forall_forall _ _)) with (x := nth idx l Vundef)
                                             (2 := Hin) in IH.
      apply IH. intros l' lv t' Hvl' Htl'. simpl. clear H IH H1 H2.
      specialize (H' (Pcell idx :: l') lv t'). simpl in H'.
      rewrite -> nth_error_nth' with (d := Vundef) (1 := Hidx) in H'.
      specialize (H' Hvl' Htl'). rewrite <- app_assoc. simpl.
      intro HVis. case H' as (b'' & o'' & s & -> & H').
      intros l'' * Hpref Hneq. destruct l''.
      rewrite app_nil_r Htr. now intros [= <-].
      simpl in Hpref. apply andb_prop in Hpref as [Hs0 Hpref].
      destruct (sem_path_elem_reflect s0 (Pcell idx)) as [->|]; try discriminate.
      specialize HVis with (1 := Hpref). rewrite <- app_assoc in HVis.
      apply HVis. congruence.
      exists b'', o'', s. split. reflexivity.
      split. intros Hs; split. by case (proj1 H' Hs).
      intro idx'. rewrite <- app_assoc.
      now specialize (proj2 (proj1 H' Hs) idx'). easy.
    + rewrite Htr in HH. now apply (HH (ex_intro _ _ eq_refl)).
Qed.

Lemma match_tr_value_get_value_path:
  forall tr n v t i l,
    match_tr_value tr n (i, l) v t ->
    forall l' lv t', get_value_path l' v = Some (Varr lv) ->
                get_type_path  l' t = Some (Tarr t') ->
                exists r, tr n (i, l ++ l') = Some r.
Proof.
  intros tr n. induction v using value_ind'; destruct t; simpl.
  all: intros * H []; try discriminate; simpl.
  all: try ((destruct s0 || destruct s); discriminate).
  - rewrite app_nil_r. destruct (tr n (i, l0)); [eauto|contradiction].
  - destruct (tr n (i, l0)) as [[[_ _] _]|]; try contradiction.
    intros *. case s. intro idx. case nth_error eqn:Hidx; try discriminate.
    assert (idx < length l)%nat by (apply nth_error_Some; now rewrite Hidx).
    specialize (H idx H0). rewrite -> nth_error_nth with (1 := Hidx) in H.
    simpl. apply nth_error_In in Hidx.
    apply (proj1 (Forall_forall _ _)) with (x := v) (2 := Hidx) in IH.
    specialize IH with (l' := l1) (1 := H). rewrite <- app_assoc in IH.
    now apply IH.
Qed.

Lemma match_mem_value_get_value_path:
  forall tr n v t i l m,
    match_mem_value tr n (i, l) m v t ->
    forall l' lv t', get_value_path l' v = Some (Varr lv) ->
                get_type_path  l' t = Some (Tarr t') ->
                exists r, tr n (i, l ++ l') = Some r.
Proof.
  intros tr n. induction v using value_ind'; destruct t.
  all: intros id pl m H []; try discriminate.
  all: try ((destruct s0 || destruct s); discriminate).
  - rewrite app_nil_r. simpl in *. destruct (tr n (id, pl)); [eauto|contradiction].
  - unfold match_mem_value in H.
    destruct (tr n (id, pl)) as [[[blk ofs] []]|]; try contradiction.
    + case s. intros idx *. simpl. case nth_error eqn:Hidx; try discriminate.
      assert (idx < length l)%nat by (apply nth_error_Some; now rewrite Hidx).
      case: H => SZ H. specialize (H idx H0).
      rewrite -> nth_error_nth with (1 := Hidx) in H.
      simpl. apply nth_error_In in Hidx.
      apply (proj1 (Forall_forall _ _)) with (x := v) (2 := Hidx) in IH.
      destruct H as (_ & _ & _ & H).
      specialize IH with (l' := l0) (1 := H). rewrite <- app_assoc in IH.
      now apply IH.
    + apply match_tr_value_get_value_path with (1 := H).
Qed.

Definition match_mem_env (tr: trmem) (n: nat) (te: tenv) (e: env) (m: Memory.mem) :=
  forall (id: SemPath.ident) (v: value) (t: typ),
    e!id   = Some v ->
    te!id  = Some t ->
    match_mem_value tr n (id, []) m v t.

Lemma match_mem_env_get_env_path:
  forall tr n te e m,
    match_mem_env tr n te e m ->
    forall p lv t, get_env_path  e  p = Some (Varr lv) ->
              get_tenv_path te p = Some (Tarr t) ->
              exists r, tr n p = Some r.
Proof.
  intros * H *. case p as [i l]. simpl.
  case e!i eqn:He; case te!i eqn:Hte; try discriminate.
  specialize (H _ _ _ He Hte). simpl.
  apply match_mem_value_get_value_path with (1 := H).
Qed.

Lemma match_tr_value_eq:
  forall tr tr' n m v t i1 l1 i2 l2,
    (forall l, tr n (i1, l1 ++ l) = tr' m (i2, l2 ++ l)) ->
    match_tr_value tr  n (i1, l1) v t ->
    match_tr_value tr' m (i2, l2) v t.
Proof.
  intros until m. induction v using value_ind'; destruct t; try easy.
  simpl. intros * H. pose proof (H []). rewrite !app_nil_r in H0. rewrite <- H0.
  case (tr n (i1, l1)) as [[[blk ofs]]|]; try easy. clear H0.
  intros H' idx Hidx. specialize (H' idx Hidx).
  apply (proj1 (Forall_forall _ _)) with (x := nth idx l Vundef) in IH.
  - apply IH with (2 := H').
    intro l'. rewrite <- !app_assoc. exact (H ([Pcell idx] ++ l')).
  - exact (nth_In _ _ Hidx).
Qed.

Lemma match_mem_value_eq:
  forall TM tr tr' n m v t i1 l1 i2 l2,
    (forall l, tr n (i1, l1 ++ l) = tr' m (i2, l2 ++ l)) ->
    match_mem_value tr  n (i1, l1) TM v t ->
    match_mem_value tr' m (i2, l2) TM v t.
Proof.
  intros until m. induction v using value_ind'; destruct t; try easy.
  unfold match_mem_value. fold match_mem_value.
  intros * H. pose proof (H []). rewrite !app_nil_r in H0. rewrite <- H0.
  case (tr n (i1, l1)) as [[[blk ofs]]|]; try easy. clear H0. case s.
  - intros [SZ H']. split=> //. intros idx Hidx. decompose [and] (H' idx Hidx).
    clear H'. repeat split=> //.
    unfold transl_value. now rewrite <- H.
    apply (proj1 (Forall_forall _ _)) with (x := nth idx l Vundef) in IH.
    + apply IH with (2 := H4).
      intro l'. rewrite <- !app_assoc. exact (H ([Pcell idx] ++ l')).
    + exact (nth_In _ _ Hidx).
  - intros _. now apply match_tr_value_eq.
Qed.

Definition no_hidden (tr: trmem) (n: nat) :=
  forall p blk ofs p',
    tr n p <> Some (blk, ofs, Hidden p').

Definition valid_blocks (tr: trmem) (N: nat) (m: Mem.mem) :=
  forall n p b o s,
    (n <= N)%nat ->
    tr n p = Some (b, o, s) ->
    (b < Mem.nextblock m)%positive.

Fixpoint match_mem_cont (tr: trmem) (n: nat) (k: Bsem.cont) (m: Memory.mem) :=
  match k with
  | Bsem.Kstop => match n with
                  | O   => True
                  | S _ => False
                  end
  | Bsem.Kseq _ k | Bsem.Kblock k => match_mem_cont tr n k m
  | Bsem.Kreturnto _ e f _ k =>
      match n with
      | O    => False
      | S n' => match_mem_env tr n' (B.fn_tenv f) e  m /\
                match_mem_cont tr n' k m
      end
  end.

End INVARIANTS.

Inductive match_states (p: B.program)
                       (hfuncs: list (ident * fundef))
                       (Habort: In (ident_abort fids, External abort) hfuncs)
                       (Hcalloc: In (ident_calloc fids, External calloc) hfuncs)
                       : Bsem.state -> state -> Prop :=
| match_states_State: forall tp tr n e f s k tm te tf ts tk
    (WF    : wf_trmem tr n)
    (VS    : visible_separated tr n (B.fn_penv f))
    (OWNZ  : owned_at_offset_zero tr n (B.fn_penv f))
    (VK    : valid_trmem_cont tr n (B.fn_penv f) k)
    (TP    : transl_program' hfuncs Habort Hcalloc p = OK tp)
    (TF    : transl_function (genv_of_program p) f = OK tf)
    (TS    : transl_stmt (genv_of_program p) f s = OK ts)
    (MK    : match_cont tr n (genv_of_program p) (Some f) k tk)
    (ME    : match_env tr n e te)
    (NOHID : no_hidden tr n)
    (VBLKS : valid_blocks tr n tm)
    (MME   : match_mem_env tr n (B.fn_tenv f) e tm)
    (MMK   : match_mem_cont tr n k tm),
    match_states p hfuncs Habort Hcalloc
                 (Bsem.State e f s k)
                 (State tf ts tk PTree.Empty te tm)
| match_states_Callstate: forall tp tr n f vargs k tm tf pargs tvargs tk
    (WF    : wf_trmem tr n)
    (VS    : visible_separated tr n (B.fn_penv f))
    (OWNZ  : owned_at_offset_zero tr n (B.fn_penv f))
    (VK    : valid_trmem_cont tr n (B.fn_penv f) k)
    (TP    : transl_program' hfuncs Habort Hcalloc p = OK tp)
    (TF    : transl_function (genv_of_program p) f = OK tf)
    (KRET  : Bsem.is_Kreturnto_or_Kstop k)
    (PARGS : map2 (fun i v => (Some (i, []), v)) (B.fn_params f) vargs = Some pargs)
    (MARGS : forall j i v t, nth_error vargs j = Some v ->
                        nth_error (B.fn_params f) j = Some i ->
                        (B.fn_tenv f) ! i = Some t ->
                        match_mem_value tr n (i, []) tm v t)
    (TARGS : transl_valuelist tr n pargs = tvargs)
    (VBLKS : valid_blocks tr n tm)
    (MK    : match_cont tr n (genv_of_program p) None k tk)
    (NOHID : no_hidden tr n)
    (MMK   : match_mem_cont tr n k tm),
    match_states p hfuncs Habort Hcalloc
                 (Bsem.Callstate f vargs k)
                 (Callstate (Internal tf) tvargs tk tm)
| match_states_Returnstate: forall tp tr n e f r k tm tv tk
    (WF    : wf_trmem tr n)
    (VS    : visible_separated tr n (B.fn_penv f))
    (OWNZ  : owned_at_offset_zero tr n (B.fn_penv f))
    (VK    : valid_trmem_cont tr n (B.fn_penv f) k)
    (TP    : transl_program' hfuncs Habort Hcalloc p = OK tp)
    (MK    : match_cont tr n (genv_of_program p) (Some f) k tk)
    (TV    : transl_value tr n None r = tv)
    (NOHID : no_hidden tr n)
    (VBLKS : valid_blocks tr n tm)
    (MME   : match_mem_env tr n (B.fn_tenv f) e tm)
    (MMK   : match_mem_cont tr n k tm),
    match_states p hfuncs Habort Hcalloc
                 (Bsem.Returnstate e f r k)
                 (Returnstate tv (call_cont tk) tm)
| match_states_Serror: forall tp e f k tm te tf ts tk
    (TP    : transl_program' hfuncs Habort Hcalloc p = OK tp)
    (TF    : transl_function (genv_of_program p) f = OK tf)
    (TS    : transl_stmt (genv_of_program p) f B.Serror = OK ts),
    match_states p hfuncs Habort Hcalloc
                 (Bsem.State e f B.Serror k)
                 (State tf ts tk PTree.Empty te tm).

Lemma complete_env_correct:
  forall vars e id v,
    e ! id = Some v ->
    (complete_env vars e) ! id = Some v.
Proof.
  induction vars. eauto.
  simpl. intros. destruct e!a eqn:Ha. eauto.
  apply IHvars. rewrite PTree.gsspec.
  destruct (Coqlib.peq id a). rewrite e0 Ha in H. discriminate. eauto.
Qed.

Lemma complete_env_set:
  forall vars e id v,
    complete_env vars (PTree.set id v e) = PTree.set id v (complete_env vars e).
Proof.
  induction vars. eauto.
  simpl. intros. rewrite PTree.gsspec. destruct Coqlib.peq.
  + rewrite e0. destruct e!id eqn:H. eauto.
    rewrite (IHvars e id Vundef). rewrite PTree.set2. eauto.
  + destruct e!a. eauto. rewrite <- (IHvars _ id v).
    rewrite -> PTree_set_order_independent with (1 := n). reflexivity.
Qed.

Lemma complete_env_set_get_neq:
  forall l e i a,
    i <> a ->
    (complete_env l e) ! i = (complete_env l (PTree.set a Vundef e)) ! i.
Proof. intros. now rewrite -> complete_env_set, PTree.gsspec, Coqlib.peq_false with (1 := H). Qed.

Lemma complete_env_correct2:
  forall vars e id,
    e ! id = None ->
    In id vars ->
    (complete_env vars e) ! id = Some Vundef.
Proof.
  induction vars; simpl; intros. contradiction.
  destruct H0. revert H0 H; intros [= ->] [= ->].
  apply complete_env_correct. now rewrite PTree.gsspec Coqlib.peq_true.
  destruct (Pos.eq_dec id a) as [[= ->]|Hneq].
  rewrite H. apply complete_env_correct. now rewrite PTree.gsspec Coqlib.peq_true.
  destruct e!a. eauto. rewrite <- complete_env_set_get_neq with (1 := Hneq). eauto.
Qed.

Lemma complete_env_correct3:
  forall vars e id,
    e ! id = None ->
    ~In id vars ->
    (complete_env vars e) ! id = None.
Proof.
  induction vars; simpl; intros. assumption.
  destruct (Pos.eq_dec id a) as [[= ->]|Hneq]. tauto.
  destruct e!a. eauto. rewrite <- complete_env_set_get_neq with (1 := Hneq). eauto.
Qed.

Lemma transl_value_primitive:
  forall tr n v p,
    primitive_value v = true ->
    transl_value tr n None v = transl_value tr n (Some p) v.
Proof. intros. now destruct v. Qed.

Lemma match_env_set:
  forall tr n e te (i: ident) v,
    match_env tr n e te ->
    match_env tr n (PTree.set i v e) (PTree.set i (transl_value tr n (Some (i, [])) v) te).
Proof.
  intros tr n e te i v ME i' v'. case (Pos.eqb_spec i i') as [<-|Hneq].
  - rewrite 2!PTree.gss. now intros [= <-].
  - rewrite -> 2!PTree.gso by lia. now apply ME.
Qed.

Lemma match_env_set_primitive:
  forall tr n e te (i: ident) v,
    primitive_value v ->
    match_env tr n e te ->
    match_env tr n (PTree.set i v e) (PTree.set i (transl_value tr n None v) te).
Proof.
  intros * H. rewrite (transl_value_primitive _ _ _ (i, [])). easy.
  now apply match_env_set.
Qed.

Lemma transl_identlist_sem_preservation:
  forall tr n ge tm e f te lids lv
    (ME:    match_env tr n e te)
    (EVEXP: Bsem.eval_identlist e f lids lv),
    eval_exprlist ge PTree.Empty
                  te tm (map Evar lids) (transl_valuelist tr n
                                        (combine (map (fun id => Some (id, [])) lids) lv)).
Proof.
  intros. revert lids lv EVEXP. induction lids; simpl; intros.
  + inversion_clear EVEXP. exact (eval_Enil _ _ _ _).
  + inversion_clear EVEXP. cbn. apply eval_Econs; eauto.
    apply eval_Evar. now apply ME.
Qed.

Lemma int64_unsigned_to_nat_id:
  forall i, i = Int64.repr (Z.of_nat (Z.to_nat (Int64.unsigned i))).
Proof.
  intro. rewrite -> Znat.Z2Nat.id by apply Int64.unsigned_range.
  now rewrite Int64.repr_unsigned.
Qed.

Theorem eval_build_index_expr:
  forall ge tm tr n e f te lsz lvsz shape lidx lvidx idx lcidx cidx
    (ME: match_env tr n e te)
    (IH: forall exp cexp v,
            In exp lidx ->
            transl_expr f.(B.fn_tenv) f.(B.fn_szenv') exp = OK cexp ->
            Bsem.eval_expr e f exp v ->
            eval_expr ge PTree.Empty te tm cexp (transl_value tr n None v)),
    Bsem.eval_identlist e f lsz lvsz ->
    natlist_of_Vint64 lvsz = Some shape ->
    Bsem.eval_exprlist e f lidx lvidx ->
    build_index lvidx shape = Some idx ->
    transl_exprlist (B.fn_tenv f) (B.fn_szenv' f) lidx = OK lcidx ->
    build_index_expr lcidx lsz = Some cidx ->
    eval_expr ge PTree.Empty te tm cidx (Vlong (Int64.repr (Z.of_nat idx))).
Proof.
  intros * ME IH. destruct lsz.
  + inversion_clear 1. intros [= <-]. inversion_clear 1; try (now destruct v).
    intros [= <-] [= <-] [= <-]. apply eval_Econst. reflexivity.
  + inversion_clear 1. simpl. unfold option_bind.
    do 2 destruct_match_goal. revert Heq; intros -> [= <-] H.
    inversion H. discriminate. subst. clear H. destruct v; try discriminate.
    cbn -[Int64.max_unsigned]. intros IDX T. monadInv T. simpl.
    revert H2 Heq0 H4 IDX EQ.
    pose proof (IH _ _ _ (in_eq _ _) EQ1 H3) as H'. revert H'. clear H3 EQ1 H0.
    simpl. clear lvsz. rewrite -> int64_unsigned_to_nat_id at 1.
    specialize (fun exp cexp v (H: In exp le) => IH exp cexp v (or_intror _ H)).
    clear e0. revert IH. generalize (Z.to_nat (Int64.unsigned i1)) as m.
    revert lsz lv l le lv0 x x0.
    induction lsz; simpl; intros lvsz shape lidx lvidx lcidx e0 m IH.
    - intro. inversion_clear 1. intros [= <-].
      destruct lvidx; try (now destruct v). inversion_clear 1.
      intros [= <-] [= <-] [= <-]. assumption.
    - intro He0. inversion_clear 1. simpl. unfold option_bind.
      do 3 destruct_match_goal. intros [= <-]. destruct lvidx; try discriminate.
      destruct v0; try discriminate. simpl.
      intro T. inversion T. subst. clear T. intros IDX TIDX. monadInv TIDX. simpl.
      eapply IHlsz with (3 := H3) (4 := Heq0) (5 := H7); simpl in IH; eauto.
      eapply eval_Ebinop. eapply eval_Ebinop. eassumption.
      apply eval_Evar. apply ME. eassumption.
      reflexivity. exact (IH _ _ _ (in_eq _ _) EQ1 H6).
      simpl. do 2 apply f_equal. unfold Int64.add, Int64.mul.
      rewrite Znat.Nat2Z.inj_add Znat.Nat2Z.inj_mul.
      apply Int64.eqm_samerepr. rewrite -> 2!Znat.Z2Nat.id by apply Int64.unsigned_range.
      apply Int64.eqm_add; [|apply Int64.eqm_refl].
      eapply Int64.eqm_trans. apply Int64.eqm_sym. apply Int64.eqm_unsigned_repr.
      apply Int64.eqm_mult; [|apply Int64.eqm_refl].
      apply Int64.eqm_sym. apply Int64.eqm_unsigned_repr.
Qed.

Theorem transl_expr_sem_preservation:
  forall tr n p hfuncs Habort Hcalloc tp tm e f te tf exp cexp v, forall
    (TP    : transl_program' hfuncs Habort Hcalloc p = OK tp)
    (ME    : match_env tr n e te)
    (MME   : match_mem_env tr n (B.fn_tenv f) e tm)
    (TF    : transl_function (genv_of_program p) f = OK tf)
    (TEXP  : transl_expr (B.fn_tenv f) (B.fn_szenv' f) exp = OK cexp)
    (NOHID : no_hidden tr n)
    (EVEXP : Bsem.eval_expr e f exp v),
    eval_expr (Genv.globalenv tp)
      PTree.Empty te tm cexp (transl_value tr n None v).
Proof.
  intros. revert exp cexp v TEXP EVEXP. fix IH 1. destruct exp; intros.
  - inv EVEXP. destruct p0 as [i synpl].
    inv H0. rewrite transl_Eacc in TEXP.
    simpl in TEXP. rewrite H4 in TEXP. simpl in TEXP.
    destruct (B.fn_tenv f)!i eqn:Hte; try discriminate. simpl in TEXP, H1.
    destruct e!i eqn:He in H1; try discriminate. simpl in H1.
    specialize (MME i _ _ He Hte).
    clear H4 Hte.
    assert (eval_expr (Genv.globalenv tp) PTree.Empty te tm (Evar i)
              (transl_value tr n (Some (i, [])) v0)) as He0
        by (apply eval_Evar; now apply ME).
    revert He0 TEXP MME H1 H6. clear He.
    revert sempl llsz v0 t. generalize (Evar i) at 1 2 as e0.
    generalize ([]: list sem_path_elem) as lsem_prec.
    induction synpl.
    + intros * He0 [= <-] MV GETV H. inv H. revert GETV; intros [= <-].
      now rewrite <- transl_value_primitive with (1 := H2) in He0.
    + intros * He0. destruct t, llsz; try discriminate. simpl.
      destruct transl_syn_path_elem eqn:TRpelem; try discriminate.
      intros TR MV GETV H. simpl in TR. destruct a as [lidx]. inv H.
      * revert TRpelem. rewrite transl_Scell.
        destruct transl_exprlist eqn:TRlidx; try discriminate. simpl.
        destruct build_index_expr eqn:Htlidx; try discriminate. simpl.
        intros [= <-]. destruct v0; try discriminate. simpl in MV.
        specialize (NOHID (i, lsem_prec)).
        destruct (tr n (i, lsem_prec)) as [[[blk ofs] [|p0]]|] eqn:Htr.
        2: now specialize (NOHID blk ofs p0).
        2: contradiction.
        clear NOHID. simpl in GETV.
        destruct nth_error eqn:Hnth in GETV; try discriminate. simpl in GETV.
        assert (idx < length l1)%nat as Hidx
            by (apply nth_error_Some; now rewrite Hnth).
        move/proj2: MV => /(_ idx Hidx). intros (Hidx' & Hvalid & Hload & MV).
        assert (Z.of_nat idx <= Int64.max_unsigned) as Hidx2.
        { change Ptrofs.max_unsigned with Int64.max_unsigned in Hidx'.
          pose proof (Ptrofs.unsigned_range ofs).
          pose proof (typ_sizeof_bounds t). nia. }
        move IHsynpl at bottom.
        specialize IHsynpl with (2 := TR). clear TR.
        specialize (IHsynpl (lsem_prec ++ [Pcell idx]) sempl0 v0).
        apply IHsynpl; try easy.
        ** eapply eval_Eload.
           eapply eval_Ebinop. exact He0.
           eapply eval_Ebinop. now apply eval_Econst.
           { apply eval_build_index_expr with (1 := ME) (3 := H5) (4 := H6) (5 := H7)
                                              (6 := H8) (7 := TRlidx) (8 := Htlidx).
             clear H7 TRlidx. induction lidx. contradiction.
             intros * [<-|Hin]. exact (IH _ _ _). exact (IHlidx _ _ _ Hin). }
           reflexivity. reflexivity. simpl. rewrite Htr. simpl.
           change Archi.ptr64 with true. simpl.
           erewrite Ptrofs.agree64_of_int_eq. rewrite -> Hload.
           now rewrite -> nth_error_nth with (1 := Hnth).
           apply (Ptrofs.agree64_mul eq_refl);
             apply (Ptrofs.agree64_repr eq_refl).
        ** now rewrite <- nth_error_nth with (1 := Hnth) (d := Vundef).
  - inversion EVEXP; revert TEXP; intros [= <-]; rewrite_equalities;
    apply eval_Econst; try destruct b; reflexivity.
  - inv EVEXP. destruct t; try discriminate; rewrite_equalities; destruct t0;
    try destruct sig; try destruct sz; try destruct i; try destruct s;
    try destruct f0; try destruct (f1: floatsize); try discriminate.
    all: simpl in TEXP; monadInv TEXP.
    all: try (do 2 destruct_match EQ1; try (revert EQ1; intros [= <-]); simpl).
    all: destruct v1; try discriminate.
    all: simpl in H4; destruct_match H4; revert H4; intros [= <-].
    all: try (destruct b; apply IH with (1 := EQ) (2 := H3)).
    all: try (eapply eval_Eunop;
              [eauto|simpl; try rewrite Heq; try reflexivity]); eauto.
    all: try (eapply eval_Eunop;
              [eauto|simpl; try rewrite Heq; reflexivity]); eauto.
    all: try (eapply eval_Ebinop; [eauto|now eapply eval_Econst|reflexivity]).
    all: destruct b; reflexivity.
  - inv EVEXP. simpl in TEXP. destruct_match TEXP; monadInv TEXP.
    all: eapply eval_Eunop || eapply eval_Ebinop; eauto.
    now apply eval_Econst.
    destruct v1; try discriminate. revert H4; intros [= <-]. now destruct b.
    all: destruct o, v1; try discriminate; simpl; revert EQ1; intros [= <-].
    all: try destruct i; try destruct s.
    all: now revert H4; intros [= <-].
  - inv EVEXP. unfold option_bind in TEXP. destruct b; monadInv TEXP.
    all: destruct o; try discriminate; rewrite_equalities.
    all: eapply eval_Ebinop; eauto.
    all: try eapply eval_Ebinop; eauto.
    all: try eapply eval_Eunop; eauto.
    all: try eapply eval_Econst; try reflexivity.
    all: destruct v1, v2; try discriminate; simpl in H6; do 2 destruct_match H6.
    all: try apply eq_intsize_correct in Heq2; rewrite_equalities; simpl; try reflexivity.
    all: try now destruct b, b0.
    all: try do 3 destruct_match H6; rewrite_equalities; try reflexivity.
    all: try unfold divs, divs64, divu, divu64, mods, mods64, modu, modu64 in H6.
    all: try (now destruct_match H6; rewrite_equalities).
    all: unfold Int.ltu; rewrite -> Coqlib.zlt_true; simpl;
         [erewrite <- Int.modu_and by reflexivity|]; try reflexivity.
    all: try (unfold Int.shl, Int64.shl', Int64.shr, Int64.shr', Int64.shru, Int64.shru';
              repeat apply f_equal; unfold Int.modu, Int64.modu).
    all: try (change (Int.unsigned Int64.iwordsize') with 64).
    all: erewrite <- Int.modu_and; try reflexivity.
    all: unfold Int.modu; change (Int.unsigned Int.iwordsize) with 32;
                          change (Int.unsigned Int64.iwordsize') with 64.
    all: pose proof (Z.mod_pos_bound (Int.unsigned i0) 32).
    all: pose proof (Z.mod_pos_bound (Int.unsigned i0) 64).
    all: rewrite -> Int.unsigned_repr by
           (change Int.max_unsigned with 4294967295; lia).
    all: lia.
  - inv EVEXP. monadInv TEXP. eapply eval_Ebinop; eauto.
    destruct c, o, v1, v2; try discriminate.
    all: simpl in H6; do 4 destruct_match H6; now rewrite_equalities.
  - inv EVEXP. monadInv TEXP. eapply eval_Ebinop; eauto.
    destruct c, o, v1, v2; try discriminate.
    all: simpl in H6; do 3 destruct_match H6; now rewrite_equalities.
Qed.

Lemma transl_syn_path_elem_list_app:
  forall e f synpl1 llsz1 sempl1 synpl2 llsz2 e0 t0,
    Bsem.eval_path_elem_list e f synpl1 llsz1 sempl1 ->
    transl_syn_path_elem_list (B.fn_tenv f) (B.fn_szenv' f) t0
                              (llsz1 ++ llsz2) (synpl1 ++ synpl2) e0 =
    do  e <- transl_syn_path_elem_list (B.fn_tenv f) (B.fn_szenv' f) t0 llsz1 synpl1 e0;
    do* t <- get_type_path sempl1 t0;
    transl_syn_path_elem_list (B.fn_tenv f) (B.fn_szenv' f) t
      (skipn (length synpl1) llsz1 ++ llsz2) synpl2 e.
Proof.
  intros e f. induction synpl1; intros.
  - inv H. reflexivity.
  - case t0; try reflexivity. intro t. inv H.
    cbn -[transl_syn_path_elem]. case transl_syn_path_elem; [|easy].
    simpl. intro. now apply IHsynpl1.
Qed.

Lemma transl_syn_path_sem_preservation:
  forall tr n hfuncs Habort Hcalloc p tp tm e f te tf synp semp v ce
    (TP    : transl_program' hfuncs Habort Hcalloc p = OK tp)
    (ME    : match_env tr n e te)
    (MME   : match_mem_env tr n (B.fn_tenv f) e tm)
    (TF    : transl_function (genv_of_program p) f = OK tf)
    (TR    : transl_syn_path (B.fn_tenv f) (B.fn_szenv' f) synp = OK ce)
    (EVp   : Bsem.eval_path e f synp semp)
    (GET   : get_env_path e semp = Some v)
    (NOHID : no_hidden tr n),
    eval_expr (Genv.globalenv tp) PTree.Empty te tm ce (transl_value tr n (Some semp) v).
Proof.
  intros. inv EVp. simpl in TR. change positive with ident in id.
  destruct (B.fn_tenv f)!id eqn:Hte; try discriminate.
  rewrite H in TR. simpl in TR.
  simpl in GET. destruct e!id eqn:He; try discriminate. simpl in GET.
  specialize (MME id _ _ He Hte) as MME'.
  clear Hte.
  assert (eval_expr (Genv.globalenv tp) PTree.Empty te tm (Evar id)
            (transl_value tr n (Some (id, [])) v0)) as He0
      by (apply eval_Evar; now apply ME).
  clear H.
  revert He0 TR MME' GET H0. clear He.
  rewrite -{3}(app_nil_l sempl).
  revert sempl llsz v0 t. generalize (Evar id) at 1 2 as e0.
  generalize ([]: list sem_path_elem) as lsem_prec.
  induction synpl.
  + intros * He0 [= <-] MV GETV H. inv H. revert GETV; intros [= <-].
    now rewrite app_nil_r.
  + intros * He0. destruct t, llsz; try discriminate. simpl.
    destruct transl_syn_path_elem eqn:TRpelem; try discriminate.
    intros TR MV GETV H. simpl in TR. destruct a as [lidx]. inv H.
    * revert TRpelem. rewrite transl_Scell.
      destruct transl_exprlist eqn:TRlidx; try discriminate. simpl.
      destruct build_index_expr eqn:Htlidx; try discriminate. simpl.
      intros [= <-]. destruct v0; try discriminate. simpl in MV.
      specialize (NOHID (id, lsem_prec)) as NOHID'.
      destruct (tr n (id, lsem_prec)) as [[[blk ofs] [|p0]]|] eqn:Htr.
      2: now specialize (NOHID' blk ofs p0).
      2: contradiction.
      simpl in GETV.
      destruct nth_error eqn:Hnth in GETV; try discriminate. simpl in GETV.
      assert (idx < length l1)%nat as Hidx
          by (apply nth_error_Some; now rewrite Hnth).
      move/proj2: MV => /(_ idx Hidx). intros (Hidx' & Hvalid & Hload & MV).
      assert (Z.of_nat idx <= Int64.max_unsigned) as Hidx2.
      { change Ptrofs.max_unsigned with Int64.max_unsigned in Hidx'.
        pose proof (Ptrofs.unsigned_range ofs).
        pose proof (typ_sizeof_bounds t). nia. }
      move IHsynpl at bottom.
      specialize IHsynpl with (2 := TR). clear TR.
      specialize (IHsynpl (lsem_prec ++ [Pcell idx]) sempl0 v0).
      change (?a :: ?b) with ([a] ++ b). rewrite app_assoc.
      apply IHsynpl; try easy.
      ** eapply eval_Eload.
         eapply eval_Ebinop. exact He0.
         eapply eval_Ebinop. now apply eval_Econst.
         { apply eval_build_index_expr with (1 := ME) (3 := H4) (4 := H5) (5 := H6)
                                            (6 := H7) (7 := TRlidx) (8 := Htlidx).
           clear H6 TRlidx. induction lidx. contradiction.
           intros * [<-|Hin]. intros T E.
           eapply transl_expr_sem_preservation with (5 := T) (7 := E); eassumption.
           exact (IHlidx _ _ _ Hin). }
         reflexivity. reflexivity. simpl. rewrite Htr. simpl.
         change Archi.ptr64 with true. simpl.
         erewrite Ptrofs.agree64_of_int_eq. rewrite -> Hload.
         now rewrite -> nth_error_nth with (1 := Hnth).
         apply (Ptrofs.agree64_mul eq_refl);
           apply (Ptrofs.agree64_repr eq_refl).
      ** now rewrite <- nth_error_nth with (1 := Hnth) (d := Vundef).
Qed.

Lemma transl_syn_path_list_sem_preservation:
  forall tr n hfuncs Habort Hcalloc p tp tm e f te tf lsynp lsemp lv lce
    (TP    : transl_program' hfuncs Habort Hcalloc p = OK tp)
    (ME    : match_env tr n e te)
    (MME   : match_mem_env tr n (B.fn_tenv f) e tm)
    (TF    : transl_function (genv_of_program p) f = OK tf)
    (TR    : transl_syn_path_list (B.fn_tenv f) (B.fn_szenv' f) lsynp = OK lce)
    (EVp   : Bsem.eval_path_list e f lsynp lsemp)
    (GET   : get_env_path_list e lsemp = Some lv)
    (NOHID : no_hidden tr n),
    eval_exprlist (Genv.globalenv tp) PTree.Empty te tm
      lce (transl_valuelist tr n (combine (map Some lsemp) lv)).
Proof.
  induction lsynp; simpl; intros; inv EVp.
  revert GET TR; intros [= <-] [= <-]. simpl. now apply eval_Enil.
  unfold transl_syn_path_list in TR. simpl in TR, GET.
  destruct transl_syn_path eqn:TRa; try discriminate.
  destruct list_map_error eqn:TRl; try discriminate.
  destruct get_env_path eqn:GETp; try discriminate.
  destruct get_env_path_list eqn:GETl; try discriminate.
  revert GET TR; intros [= <-] [= <-].
  apply eval_Econs. eapply transl_syn_path_sem_preservation; eauto.
  apply IHlsynp; eauto.
Qed.

Lemma transl_fundefs_exists:
  forall ge fds tfds idf f,
    transl_fundefs ge fds = OK tfds ->
    In (idf, f) fds ->
    exists f', In (idf, Gfun f') tfds /\ transl_fundef ge f = OK f'.
Proof.
  induction fds; simpl; intros. contradiction. destruct a as [id fd].
  unfold transl_fundefs in H. cbn in H. monadInv H. monadInv EQ.
  destruct H0.
  injection H; intros [= ->] [= ->]. simpl. eauto.
  specialize (IHfds _ _ _ EQ1 H) as [f' [H1 H2]]. simpl. eauto.
Qed.

Lemma transl_fundefs_eq_map_fst:
  forall ge (fds: list (ident * B.fundef)) tfds,
    transl_fundefs ge fds = OK tfds ->
    map fst fds = map fst tfds.
Proof.
  induction fds; simpl; intros. revert H; intros [= <-]. reflexivity.
  destruct a as [id fd]. unfold transl_fundefs in H. cbn in H. monadInv H. monadInv EQ.
  simpl. now erewrite IHfds by eassumption.
Qed.

Lemma transl_fundefs_no_repet_fst:
  forall fds tfds ge,
    transl_fundefs ge fds = OK tfds ->
    Coqlib.list_norepet (map fst fds) ->
    Coqlib.list_norepet (map fst tfds).
Proof. intros. now rewrite <- transl_fundefs_eq_map_fst with (1 := H). Qed.

Lemma transl_program'_genv_preservation:
  forall hfuncs Habort Hcalloc p tp idf f,
    Coqlib.list_norepet (map fst hfuncs) ->
    Coqlib.list_disjoint (map fst (B.prog_defs p)) (map fst hfuncs) ->
    transl_program' hfuncs Habort Hcalloc p = OK tp ->
    (genv_of_program p) ! idf = Some f ->
    exists loc fd, Globalenvs.Genv.find_symbol (Genv.globalenv tp) idf = Some loc
                   /\ Globalenvs.Genv.find_funct_ptr (Genv.globalenv tp) loc = Some fd
                   /\ transl_fundef (genv_of_program p) f = OK fd.
Proof.
  intros until f. intros Hhfuncs_norepet Hhfuncs_disjoint. intros.
  destruct p. unfold transl_program' in H. monadInv H.
  unfold genv_of_program in H0. simpl in H0. apply PTree.elements_correct in H0.
  apply build_env_correct_empty in H0; try assumption.
  pose proof (transl_fundefs_no_repet_fst _ _ _ EQ prog_nodup) as Hnodup.
  apply transl_fundefs_exists with (1 := EQ) in H0. destruct H0 as [f' [H1 H2]].
  unfold Globalenvs.Genv.find_funct_ptr. simpl in Hhfuncs_disjoint.
  rewrite -> transl_fundefs_eq_map_fst with (fds := prog_defs) (1 := EQ) in *.
  assert (Coqlib.list_norepet (map fst (globdefs_list hfuncs ++ x))) as Hnorepet'.
  { rewrite map_app. apply Coqlib.list_disjoint_sym in Hhfuncs_disjoint.
    rewrite globdefs_list_fst. now apply Coqlib.list_norepet_append. }
  assert (In (idf, Gfun f') (globdefs_list hfuncs ++ x)) as Hin'.
  { apply in_or_app. now right. }
  eapply PTree_Properties.of_list_norepet with (1 := Hnorepet') in Hin'.
  simpl in *. cbn in *.
  specialize (Globalenvs.Genv.find_def_symbol) with (p := {|
    prog_defs := globdefs_list hfuncs ++ x;
    prog_public := map fst x;
    prog_main := prog_main
  |}) (id := idf) (g := Gfun f') as [H0 _]. specialize (H0 Hin').
  destruct H0 as [b [H H0]]. repeat eexists. eassumption.
  unfold Genv.globalenv, Genv.add_globals in H0. simpl in H0. now rewrite H0. assumption.
Qed.

Lemma bind_parameter_set:
  forall vars args le id v,
    ~In id vars ->
    bind_parameters vars args (PTree.set id v le)
    = option_map (fun le => PTree.set id v le) (bind_parameters vars args le).
Proof.
  induction vars; simpl; intros.
  + destruct args; reflexivity.
  + destruct args. reflexivity.
    rewrite <- IHvars.
    destruct (Pos.eq_dec id a).
    rewrite e in H. tauto.
    rewrite -> PTree_set_order_independent with (1 := n). reflexivity. tauto.
Qed.

Lemma match_env_build_preservation:
  forall f e tr n vargs,
    Coqlib.list_norepet (B.fn_params f) ->
    build_func_env (PTree.empty value) (B.fn_params f) vargs = Some e ->
    exists te, bind_parameters (B.fn_params f)
                          (transl_valuelist tr n (map (fun '(p, v) => (Some (p, []), v))
                                                 (combine (B.fn_params f) vargs)))
                          (create_undef_temps (B.fn_vars f)) = Some te /\
          match_env tr n e te.
Proof.
  intros f. induction (B.fn_params f).
  - intros. destruct vargs; try discriminate.
    revert H0; intros [= <-]. simpl. eexists. split. reflexivity. discriminate.
  - simpl; intros. destruct vargs; try discriminate.
    destruct build_func_env eqn:He0; try discriminate.
    revert H0; intros [= <-]. inversion_clear H.
    specialize IHl with (1 := H1) (2 := He0).
    simpl. rewrite -> bind_parameter_set with (1 := H0).
    case (IHl tr n) as [te [-> ME]]. simpl. eexists. split. reflexivity.
    intros i v'. destruct (Pos.eq_dec i a) as [[= <-] | H'].
    + rewrite 2!PTree.gss. now intros [= <-].
    + rewrite -> 2!PTree.gso with (1 := H').
      clear IHl He0. apply ME.
Qed.

Lemma match_cont_destructCont:
  forall tr n p f k tk,
    match_cont tr n (genv_of_program p) (Some f) k tk ->
    match_cont tr n (genv_of_program p) None (Bsem.destructCont k) (call_cont tk).
Proof.
  induction k; simpl; intros.
  + now destruct tk.
  + destruct tk; try contradiction. destruct H. simpl. eauto.
  + destruct tk; try contradiction. simpl. eauto.
  + now destruct tk.
Qed.

Lemma valid_trmem_cont_destructCont:
  forall tr n pe k,
    valid_trmem_cont tr n pe k ->
    valid_trmem_cont tr n pe (Bsem.destructCont k).
Proof. induction k; simpl; easy. Qed.

Lemma match_mem_cont_destructCont:
  forall tr n k tm,
    match_mem_cont tr n k tm ->
    match_mem_cont tr n (Bsem.destructCont k) tm.
Proof. induction k; simpl; easy. Qed.

Lemma load_result_shrink:
  forall tr n t p v,
    well_typed_value t v ->
    Val.load_result (typ_to_memory_chunk t) (transl_value tr n p v)
    = transl_value tr n p (shrink t v).
Proof.
  inversion_clear 1; try reflexivity.
  now destruct b. now destruct sz, sig. simpl.
  destruct p; try easy. now destruct (tr n s) as [[[blk ofs] []]|].
Qed.

Lemma match_mem_env_set:
  forall tr n f e tm id v,
    match_mem_env tr n (B.fn_tenv f) e tm ->
    primitive_value v = true ->
    match_mem_env tr n (B.fn_tenv f) (PTree.set id v e) tm.
Proof.
  intros ** id' v' t'. case (Pos.eqb_spec id id') as [<-|Hneq].
  - rewrite PTree.gss. intros [= <-] Ht'. now destruct t', v.
  - rewrite -> PTree.gso with (1 := not_eq_sym Hneq). apply H.
Qed.

Lemma replace_nth_nth_same {A: Type}:
  forall (l l': list A) (n: nat) (x y: A),
    replace_nth l n x = Some l' ->
    nth n l' y = x.
Proof.
  induction l; simpl; intros. discriminate. destruct n.
  - injection H as <-. reflexivity.
  - destruct replace_nth eqn:Hrep; try discriminate.
    injection H as <-. simpl. exact (IHl _ n x y Hrep).
Qed.

Lemma replace_nth_nth_other {A: Type}:
  forall (l l': list A) (n m: nat) (x y: A),
    replace_nth l n x = Some l' ->
    n <> m ->
    nth m l' y = nth m l y.
Proof.
  induction l; simpl; intros. discriminate. destruct n.
  - injection H as <-. now destruct m.
  - destruct replace_nth eqn:Hrep; try discriminate.
    injection H as <-. destruct m; simpl. reflexivity.
    refine (IHl _ n m x y Hrep _). lia.
Qed.

Lemma size_chunk_typ_sizeof:
  forall t,
    size_chunk (typ_to_memory_chunk t) = typ_sizeof t.
Proof.
  destruct t; simpl; try reflexivity.
  now destruct s, i. now destruct f.
Qed.

Section TRMEM_UPDATE_FUNCTIONS.

  Definition tr_call (tr: trmem) (n: nat) (mut shr own: list (sem_path * ident)) : trmem :=
    fun m p =>
      if Nat.eqb m (S n) then
        let '(i, l) := p in
        match find (fun '(_, j) => Pos.eqb j i) (mut ++ shr ++ own) with
        | Some ((ic, lc), _) => tr n (ic, lc ++ l)
        | None => tr m (i, l)
        end
      else if Nat.eqb m n then
        let '(i, l) := p in
        match find (fun '(q, _) => sem_path_prefix q p) mut with
        | Some ((_, l'), j) =>
            match tr n p with
            | Some (blk, ofs, _) => Some (blk, ofs, Hidden (j, skipn (length l') l))
            | None => None
            end
        | None =>
            match find (fun '((j, _), _) => Pos.eqb j i) own with
            | Some _ => None
            | None => tr n p
            end
        end
      else if Nat.ltb m n then tr m p
      else None.

  Definition tr_return (tr: trmem) (n: nat) : trmem :=
    fun m p =>
      if Nat.eqb m n then
        match tr n p with
        | Some (blk, ofs, Hidden q) => tr (S n) q
        | x => x
        end
      else if Nat.ltb m n then tr m p
      else None.

End TRMEM_UPDATE_FUNCTIONS.

Section CALL_PRESERVATION_LEMMAS.

Lemma tr_call_lt_N_same_domain:
  forall tr N mut shr own n p,
    (n < N)%nat ->
    (tr n p defined) <->
    (tr_call tr N mut shr own n p defined).
Proof.
  intros tr N mut shr own n [i l] Hn. unfold tr_call.
  rewrite -> 2!(proj2 (Nat.eqb_neq _ _)) by lia.
  now rewrite -> (proj2 (Nat.ltb_lt _ _)) by lia.
Qed.

Lemma tr_call_N_incl_domain:
  forall tr N mut shr own p,
    (tr_call tr N mut shr own N p defined) ->
    (tr N p defined).
Proof.
  intros tr N mut shr own [i l]. unfold tr_call.
  rewrite -> (proj2 (Nat.eqb_neq _ _)) by lia.
  rewrite Nat.eqb_refl. case find as [[[]]|]. case tr; eauto.
  case find as [[[]]|]. now intros []. eauto.
Qed.

Lemma tr_call_SN_wf:
  forall tr N mut shr own i l,
    wf_trmem tr N ->
    tr_call tr N mut shr own (S N) (i, l) defined ->
    exists ic lc, In ((ic, lc), i) (mut ++ shr ++ own) /\
             tr_call tr N mut shr own (S N) (i, l) = tr N (ic, lc ++ l).
Proof.
  intros * WF. unfold tr_call. rewrite Nat.eqb_refl.
  case find as [[[]]|] eqn:Hfind.
  - apply find_some in Hfind as [Hin Hp]. apply Pos.eqb_eq in Hp as ->. eauto.
  - intro H. elim (trmem_max _ _ WF (S N) (i, l)). lia. easy.
Qed.

Lemma tr_call_le_N_Visible:
  forall tr N mut shr own n p b o,
    (n <= N)%nat ->
    tr_call tr N mut shr own n p = Some (b, o, Visible) ->
    tr_call tr N mut shr own n p = tr n p.
Proof.
  intros * Hn. unfold tr_call. rewrite -> (proj2 (Nat.eqb_neq _ _)) by lia.
  case (Nat.eqb_spec n N) as [->|]; [|case Nat.ltb_spec; [|discriminate]].
  case p as [i l]. case find as [[[]]|]. now case tr as [[[]]|].
  now case find as [[[]]|]. easy.
Qed.

Lemma tr_call_N_Visible_not_found:
  forall tr N mut shr own i l b o,
    tr_call tr N mut shr own N (i, l) = Some (b, o, Visible) ->
    (forall p j, In (p, j) mut -> ~~ sem_path_prefix p (i, l)) /\
    forall i' l' j, In ((i', l'), j) own -> i' <> i.
Proof.
  intros *. unfold tr_call. rewrite -> (proj2 (Nat.eqb_neq _ _)) by lia.
  rewrite Nat.eqb_refl. case find as [[[]]|] eqn:Hfind.
  - case tr as [[[]]|]; discriminate.
  - case (find (fun '(j, _, _) => Pos.eqb j i) own) as [[[]]|] eqn:Hfind'.
    discriminate.
    intros _; split; intros * H.
    apply find_none with (2 := H) in Hfind.  now rewrite Hfind.
    apply find_none with (2 := H) in Hfind'. now move/Pos.eqb_spec in Hfind'.
Qed.

Lemma tr_call_lt_N_blk_ofs_preserved:
  forall tr N (mut shr own: list (sem_path * ident)) n p blk ofs s,
    (n < N)%nat ->
    tr n p = Some (blk, ofs, s) ->
    exists s', tr_call tr N mut shr own n p = Some (blk, ofs, s').
Proof.
  intros * Hn. unfold tr_call.
  rewrite -> 2!(proj2 (Nat.eqb_neq _ _)) by lia.
  rewrite -> (proj2 (Nat.ltb_lt _ _)) by lia. eauto.
Qed.

Lemma tr_call_le_N_blk_ofs_preserved:
  forall tr N (mut shr own: list (sem_path * ident)) n p blk ofs s,
    (n <= N)%nat ->
    tr_call tr N mut shr own n p = Some (blk, ofs, s) ->
    exists s', tr n p = Some (blk, ofs, s').
Proof.
  intros * Hn. unfold tr_call.
  rewrite -> (proj2 (Nat.eqb_neq _ _)) by lia.
  case (Nat.eqb_spec n N) as [<-|].
  - case p; intros *. case find as [[[]]|].
    case tr as [[[]]|]; [|discriminate]. intros [= -> ->]. eauto.
    case find as [[[]]|]. discriminate. intros ->. eauto.
  - case (n <? N)%nat; [|discriminate]. intros ->. eauto.
Qed.

Lemma tr_call_lt_N:
  forall tr N (mut shr own: list (sem_path * ident)) n p,
    (n < N)%nat ->
    tr_call tr N mut shr own n p = tr n p.
Proof.
  intros. unfold tr_call. rewrite -> 2!(proj2 (Nat.eqb_neq _ _)) by lia.
  now rewrite (proj2 (Nat.ltb_lt _ _) H).
Qed.

Lemma tr_call_no_hidden:
  forall tr N mut shr own,
    wf_trmem tr N ->
    no_hidden tr N ->
    no_hidden (tr_call tr N mut shr own) (S N).
Proof.
  intros * WF. unfold no_hidden. intros H *.
  unfold tr_call. rewrite Nat.eqb_refl. case p as [i l].
  destruct find as [[[]]|]. apply H.
  intro H'. apply trmem_max with (1 := WF) (n := S N) (p := (i, l)). lia. eauto.
Qed.

Lemma hidden_is_mut_Sn:
  forall tr n p k tm,
    match_mem_cont tr (S n) k tm ->
    valid_trmem_cont tr (S n) p k ->
    hidden_is_mut tr n p.
Proof.
  induction k; eauto; simpl; try easy.
  move=>> _. intros (_ & HM & _ & _ & _ & _ & MLIST & _).
  apply: mutable_list_hidden_is_mut HM MLIST.
Qed.

Lemma wf_trmem_tr_call:
  forall tr N mut shr own pe,
    wf_trmem tr N ->
    no_hidden tr N ->
    (forall p i q j, In (p, i) (mut ++ shr ++ own) ->
                In (q, j) (mut ++ shr ++ own) ->
                (p, i) <> (q, j) -> i <> j) ->
    (forall p i q j, In (p, i) (mut ++ own) ->
                In (q, j) (mut ++ shr ++ own) ->
                (p, i) <> (q, j) ->
                ~~ sem_path_alias p q) ->
    (forall i l j, In ((i, l), j) own -> l = []) ->
    (forall n, N = S n -> hidden_is_mut tr n pe) ->
    (forall i l j, In ((i, l), j) own -> pe!i = Some B.Owned) ->
    wf_trmem (tr_call tr N mut shr own) (S N).
Proof.
  intros * WF NOHID NOREPET NOALIAS OWN HMUT OWNEDPE. apply mk_wf_trmem.
  - intros *. case (Nat.ltb_spec N n); [case (Nat.eqb_spec n (S N))|].
    + unfold tr_call. intros -> _. rewrite Nat.eqb_refl.
      case find as [[[ic lc] i]|] eqn:Hfind.
      * intros DEF * PREF. apply trmem_prefix with (1 := WF) (2 := DEF).
        apply prefix_app with (1 := sem_path_elem_reflect) (2 := PREF).
      * intro H. destruct (trmem_max _ _ WF (S N) (id, pl1)). lia. easy.
    + unfold tr_call. intros H H'. rewrite (proj2 (Nat.eqb_neq _ _) H). clear H.
      rewrite -> (proj2 (Nat.eqb_neq _ _)) by lia.
      case Nat.ltb_spec. lia. easy.
    + intros Hn DEF. apply Nat.le_lteq in Hn as [Hn| ->].
      * apply tr_call_lt_N_same_domain with (1 := Hn) in DEF.
        intros pl2 H. apply trmem_prefix with (1 := WF) (2 := DEF) in H.
        now apply tr_call_lt_N_same_domain with (1 := Hn).
      * intros pl2 H. revert DEF. unfold tr_call.
        rewrite -> (proj2 (Nat.eqb_neq _ _)), Nat.eqb_refl by lia.
        case find as [[[]]|] eqn:Hfind.
        ** case tr as [[[]]|] eqn:Htr. 2: now intros []. intros _.
           case (find (fun '(q, _) => sem_path_prefix q (id, pl2)))
             as [[[]]|] eqn:Hfind'.
           { apply (ex_intro (fun _ => _) (b, i1, s)) in Htr.
             apply trmem_prefix with (1 := WF) (3 := H) in Htr.
             destruct tr as [[[]]|]; eauto. }
           { case (find (fun '(j, _, _) => Pos.eqb j i) own) as [[[]]|] eqn:Hfind''.
             - exfalso.
               apply find_some in Hfind as [Hin Hpref], Hfind'' as [Hin' Heq].
               move/Pos.eqb_spec in Heq. revert Heq; intros ->.
               have:= (OWN _ _ _ Hin'). intros ->. destruct l.
               + apply find_none with (2 := Hin) in Hfind'.
                 move: Hpref Hfind' => /=. by case Pos.eqb.
               + apply (fun x => in_or_app _ (shr ++ own) _ (or_introl _ x)) in Hin.
                 apply (fun x => in_or_app mut _ _ (or_intror _ x)) in Hin'.
                 have: (i, [], i2) <> (i, s0 :: l, i0) by congruence.
                 move/(NOALIAS _ _ _ _ Hin' Hin) => /=. by rewrite Pos.eqb_refl.
             - apply find_some in Hfind as [Hin Hpref].
               case/andP: Hpref => /Pos.eqb_spec. intros <-.
               rewrite Hfind'' => Hpref.
               apply trmem_prefix with (1 := WF) (3 := H). rewrite Htr; eauto. }
        ** case (find (fun '(j, _, _) => Pos.eqb j id) own) as [[[]]|] eqn:Hfind'.
           now intros [].
           case (find (fun '(q, _) => sem_path_prefix q (id, pl2)) mut)
             as [[[]]|] eqn:X.
           { apply find_some in X as [Hin Hpref].
             apply find_none with (2 := Hin) in Hfind.
             simpl in Hfind, Hpref. destruct (Pos.eqb_spec i id); try easy.
             pose proof (prefix_transl _ sem_path_elem_reflect l pl2 pl1).
             simpl in Hpref, Hfind. rewrite Hpref H in H0.
             exfalso. apply eq_true_false_abs with (2 := Hfind). now apply H0. }
           { intro DEF. now apply trmem_prefix with (1 := WF) (3 := H). }
  - intros * Hn.
    unfold tr_call. case Nat.eqb_spec; [lia|intros _].
    case Nat.eqb_spec; [lia|intros _].
    case Nat.ltb_spec; [lia|intros _]. now intros [].
  - intros *. case (Nat.ltb_spec n N) as [Hn|Hn].
    + rewrite -> 2!tr_call_lt_N with (1 := Hn).
      apply trmem_hid_sep with (1 := WF).
    + case (Nat.eqb_spec n N) as [->|]; [|case (Nat.eqb_spec n (S N)) as [->|]].
      * unfold tr_call. rewrite -> (proj2 (Nat.eqb_neq _ _)) by lia.
        rewrite Nat.eqb_refl. case p as [i1 l1], p' as [i2 l2].
        case find as [[[ic1 lc1] j1]|] eqn:H1;
          case (find (fun '(q, _) => sem_path_prefix q (i2, l2)) mut)
            as [[[ic2 lc2] j2]|] eqn:H2.
        2,3,4: case (find (fun '(j, _, _) => Pos.eqb j _) own) as [[[]]|] => //.
        2: intros _ H; now specialize (NOHID (i2, l2) b2 o2 p2).
        2,3: intros H; now specialize (NOHID (i1, l1) b1 o1 p1).
        case (tr N (i1, l1)) as [[[b'1 o'1] s1]|] eqn:Htr1; try discriminate.
        case (tr N (i2, l2)) as [[[b'2 o'2] s2]|] eqn:Htr2; try discriminate.
        apply find_some in H1 as [H1 H1'], H2 as [H2 H2'].
        intros [= -> -> <-] [= -> -> <-]. unfold sem_path_prefix in H1', H2'.
        apply andb_prop in H1' as [Hi1 Hl1], H2' as [Hi2 Hl2].
        apply Pos.eqb_eq in Hi1 as ->, Hi2 as ->.
        apply (fun x => in_or_app _ (shr ++ own) _ (or_introl _ x)) in H1, H2.
        case (Pos.eqb_spec i1 i2) as [<-|].
        ** intros H. assert (l1 <> l2) by congruence. clear H.
           set (l'1 := skipn _ l1). set (l'2 := skipn _ l2).
           case (ListUtils.list_reflect _ sem_path_elem_reflect lc1 lc2).
           { intros ->. assert (l1 = lc2 ++ l'1).
             rewrite <- (firstn_skipn (Datatypes.length lc2) l1).
             now rewrite (prefix_firstn _ sem_path_elem_reflect _ _ Hl1).
             assert (l2 = lc2 ++ l'2).
             rewrite <- (firstn_skipn (Datatypes.length lc2) l2).
             now rewrite (prefix_firstn _ sem_path_elem_reflect _ _ Hl2).
             congruence. }
           { intro H. assert (j1 <> j2); [|congruence].
             apply NOREPET with (1 := H1) (2 := H2). congruence. }
        ** assert (j1 <> j2); [|congruence].
           apply NOREPET with (1 := H1) (2 := H2). congruence.
      * apply (tr_call_no_hidden _ _ mut shr own WF) in NOHID.
        now specialize (NOHID p b1 o1 p1).
      * unfold tr_call. rewrite (proj2 (Nat.eqb_neq _ _) n0).
        rewrite (proj2 (Nat.eqb_neq _ _) n1).
        rewrite (proj2 (Nat.ltb_ge _ _) Hn). discriminate.
  - intros * Hn. case (Nat.ltb_spec n N) as [Hn'|Hn'].
    + rewrite -> tr_call_lt_N with (1 := Hn'). intro H.
      pose proof (trmem_hid _ _ WF _ _ _ _ _ _ _ Hn' H) as [s H'].
      case (Nat.eqb_spec (S n) N) as [<-|Hneq].
      * clear Hn Hn'. unfold tr_call. rewrite (proj2 (Nat.eqb_neq _ _)). lia.
        rewrite Nat.eqb_refl. rewrite H'. case find as [[[]]|]. eauto.
        case find as [[[]]|] eqn:Hfind; eauto.
        apply find_some in Hfind as [Hin Heq].
        apply OWNEDPE in Hin. apply HMUT in H; [|reflexivity].
        move/Pos.eqb_spec: Heq => T. by rewrite -T Hin in H.
      * eapply tr_call_lt_N_blk_ofs_preserved. lia. exact H'.
    + assert (n = N) by lia. rewrite H. clear Hn Hn' H.
      unfold tr_call. rewrite -> (proj2 (Nat.eqb_neq _ _)) by lia.
      rewrite 2!Nat.eqb_refl. case find as [[[]]|] eqn:H.
      * apply find_some in H as [Hin Hpref].
        case find as [[[]]|] eqn:Hfind.
        { apply find_some in Hfind as [Hin' Heq]. move: Heq => /Pos.eqb_spec <-.
          case tr as [[[]]|] eqn:Htr; try discriminate. case=> <- <- T <-.
          rewrite -T in Hin' => {T}.
          move: Hin Hin'. case (eqSemPath (i0, l0) (i2, l1)) => [[<- <-]|].
          - case/andP: Hpref => /Pos.eqb_spec -> /prefix_firstn T.
            rewrite -{1 2 3}T => {T}. exact: sem_path_elem_reflect.
            rewrite firstn_skipn. eauto.
          - move=> Hneq Hin Hin'.
            elim: (NOREPET _ _ _ _ (in_or_app _ _ _ (or_introl _ Hin)) Hin').
            congruence. reflexivity. }
        { case tr as [[[]]|] eqn:Htr; try discriminate. case=> <- <- T.
          rewrite T in Hin => {T}.
          apply find_none with (2 := in_or_app _ _ _ (or_introl _ Hin)) in Hfind.
          by move: Hfind => /Pos.eqb_spec. }
      * case (find (fun '(j, _, _) => Pos.eqb j i) own) as [[[]]|] eqn:Hfind.
        discriminate.
        now specialize (NOHID (i, l) b o (i', l')).
Qed.

Lemma visible_separated_tr_call_le_N:
  forall tr N mut shr own n pe,
    (n <= N)%nat ->
    visible_separated tr n pe ->
    visible_separated (tr_call tr N mut shr own) n pe.
Proof.
  intros *. unfold visible_separated. intros Hn H * Htr Hpe Hp.
  case (Nat.ltb_spec n N).
  - intros Hn'. rewrite -> tr_call_lt_N with (1 := Hn') in Htr. split.
    + intros * ?. assert (m < N)%nat as Hm by lia.
      rewrite -> tr_call_lt_N with (1 := Hm).
      now apply H with (1 := Htr) (2 := Hpe) (3 := Hp).
    + intros *. rewrite -> tr_call_lt_N with (1 := Hn').
      now apply H with (1 := Htr) (2 := Hpe) (3 := Hp).
  - intro Hn'. assert (n = N) as -> by lia. clear Hn Hn'.
    rewrite -> tr_call_le_N_Visible with (2 := Htr) in Htr by lia. split.
    + intros n' * Hn'. rewrite -> tr_call_lt_N with (1 := Hn').
      now apply H with (1 := Htr) (2 := Hpe) (3 := Hp).
    + intros * Hp' Htr'.
      apply tr_call_le_N_blk_ofs_preserved in Htr' as [s Htr']; [|lia].
      revert Hp' Htr'. now apply H with (1 := Htr) (2 := Hpe) (3 := Hp).
Qed.

Lemma visible_separated_tr_call_SN:
  forall tr N mut shr own pe pe',
    (forall p p' i, In (p, i) (mut ++ shr ++ own) ->
               In (p', i) (mut ++ shr ++ own) ->
               p = p') ->
    (forall p p' (i i': ident), In (p, i) (mut ++ own) ->
                           In (p', i') (mut ++ shr ++ own) ->
                           (p, i) <> (p', i') ->
                           ~~ sem_path_alias p p') ->
    visible_separated tr N pe ->
    (forall ic lc i p', In ((ic, lc), i) (mut ++ shr ++ own) ->
                   pe'!i = Some p' ->
                   exists p, pe!ic = Some p /\ B.perm_le p' p) ->
    wf_trmem tr N ->
    (forall p i, In (p, i) (mut ++ shr) ->
            exists x, pe'!i = Some x /\ B.perm_le x B.Mutable) ->
    (forall p i x, In (p, i) (mut ++ shr ++ own) ->
              pe'!i = Some x ->
              B.perm_le B.Mutable x ->
              In (p, i) (mut ++ own)) ->
    visible_separated (tr_call tr N mut shr own) (S N) pe'.
Proof.
  intros * H NOALIAS VSEP PERM WF PLMUT MUT. unfold visible_separated.
  intros i l b o p' Htr Hpe' Hp'.
  case tr_call_SN_wf with (1 := WF)
                          (2 := ex_intro (fun _ => _) (b, o, Visible) Htr)
    as (ic & lc & Hin & Heq). rewrite Heq in Htr. clear Heq.
  specialize PERM with (1 := Hin) (2 := Hpe') as [p [Hpe Hp'p]].
  pose proof (B.perm_le_trans _ _ _ Hp' Hp'p) as Hp. clear Hp'p.
  assert (In ((ic, lc), i) (mut ++ own)) as Hinm.
  { apply MUT with (2 := Hpe'); [exact Hin|exact Hp']. }
  split; intros *.
  - intros Hm Htr'. pose proof Htr' as H'.
    rewrite -> tr_call_le_N_Visible with (2 := Htr') in Htr' by lia.
    case (Nat.eqb_spec m N) as [->|Hm'].
    + case p'0 as [i' l'].
      pose proof (tr_call_N_Visible_not_found _ _ _ _ _ _ _ _ _ H').
      apply (proj2 (VSEP _ _ _ _ _ Htr Hpe Hp) (i', l') b' o' Visible).
      intros [= <- <-]. case/(in_app_or mut): Hinm.
      * move/(proj1 H0). rewrite /sem_path_prefix.
        by rewrite Pos.eqb_refl (prefix_app' _ sem_path_elem_reflect).
      * by move/(proj2 H0).
      * exact Htr'.
    + apply (proj1 (VSEP _ _ _ _ _ Htr Hpe Hp) m p'0 b' o'). lia. exact Htr'.
  - case p'0 as [i' l']. intros Hneq Htr'.
    case tr_call_SN_wf with (1 := WF)
                            (2 := ex_intro (fun _ => _) (b', o', s') Htr')
    as (ic' & lc' & Hin' & Heq). rewrite Heq in Htr'. clear Heq.
    apply (proj2 (VSEP _ _ _ _ _ Htr Hpe Hp) (ic', lc' ++ l') b' o' s').
    assert (i <> i' \/ i = i' /\ l <> l') as [Hneq'|[<- Hneq']].
    { case (Pos.eqb_spec i i') as [<-|Hii'].
      right. split; congruence. now left. }
    + specialize (NOALIAS _ _ _ _ Hinm Hin').
      injection. intros Hl <-.
      assert (sem_path_alias (ic, lc) (ic, lc')).
      { simpl. rewrite Pos.eqb_refl.
        apply eq_app_case_prefix with (1 := sem_path_elem_reflect)
           in Hl as [->| ->]. easy. now rewrite orb_true_r. }
      assert ((ic, lc, i) <> (ic, lc', i')) by congruence.
      rewrite H1 in NOALIAS. eauto.
    + injection (H _ _ _ Hin Hin') as <- <-.
      injection. clear H0. intro H'. apply Hneq'.
      now apply app_inv_head with (1 := H').
    + exact Htr'.
Qed.

Lemma valid_trmem_cont_tr_call_same_cont:
  forall tr N mut shr own k n pe,
    (n <= N)%nat ->
    (forall i l j, In ((i, l), j) own -> pe!i = Some B.Owned) ->
    valid_trmem_cont tr n pe k ->
    valid_trmem_cont (tr_call tr N mut shr own) n pe k.
Proof.
  intros tr N mut shr own k n pe Hn OWNEDPE.
  assert (n = N -> pe = pe) by easy.
  revert H. generalize pe at 1 3 4 as pe'.
  revert k n Hn. induction k; eauto. intros [|n] Hn pe'. easy. simpl.
  intros Hpe' (VSEP & HID & ODEF & MPRES & NOALIAS & NOREPET & MLIST & OWNZ & VK).
  split; [|split; [|split; [|split; [|repeat split]]]].
  - apply visible_separated_tr_call_le_N. lia. easy.
  - unfold hidden_marrs. intros *.
    split; rewrite -> tr_call_lt_N by lia; apply (HID _ _ _ l').
  - unfold origin_defined. intros *. case (Nat.ltb_spec (S n) N) as [H|H].
    + rewrite -> 2!tr_call_lt_N by lia. apply ODEF.
    + assert (S n = N) as <- by lia. intro Hin. split; intro Htr.
      * apply tr_call_N_incl_domain in Htr.
        apply tr_call_lt_N_same_domain. lia.
        revert Hin Htr; apply ODEF.
      * apply tr_call_lt_N_same_domain in Htr; [|lia].
        have:= proj2 (HID _ _ i l0) Htr (ex_intro _ _ (conj Hin eq_refl)).
        case (tr n (ic, lc ++ l0)) as [[[]]|] eqn:Htr'; try discriminate.
        injection 1 as ->. clear Htr H.
        rewrite -> (Hpe' eq_refl) in *. clear Hpe'. unfold tr_call.
        rewrite (proj2 (Nat.eqb_neq _ _)). lia.
        rewrite Nat.eqb_refl. case find as [[[]]|].
        { specialize (proj2 (ODEF _ _ _ l0 Hin)). rewrite Htr'.
          case. eauto. intros [[]] ->. eauto. }
        { case find as [[[]]|] eqn:Hfind. apply find_some in Hfind as [Hin' H].
          generalize (proj1 (HID ic (lc ++ l0) i l0)). rewrite Htr'. simpl.
          intro X. specialize (X eq_refl). destruct X as [? [IN _]].
          move/MLIST: IN. move/Pos.eqb_spec: H => <-.
          by rewrite (OWNEDPE _ _ _ Hin').
          apply (ODEF _ _ _ l0 Hin). rewrite Htr'. eauto. }
  - move=> i0 l0 blk ofs i' l' b' o' s.
    rewrite tr_call_lt_N. lia.
    move/[swap]/tr_call_le_N_blk_ofs_preserved => /(_ Hn).
    by case=>> /MPRES/[apply].
  - exact NOALIAS.
  - exact NOREPET.
  - exact MLIST.
  - move=>>. rewrite tr_call_lt_N. lia. exact: OWNZ.
  - apply IHk; [lia|lia|easy].
Qed.

Lemma hidden_marrs_tr_call:
  forall tr N mut shr own,
    no_hidden tr N ->
    marrs_no_aliasing mut ->
    hidden_marrs (tr_call tr N mut shr own) N mut.
Proof.
  intros * NOHID NOALIAS.
  unfold hidden_marrs. intros *. split.
  - unfold tr_call.
    rewrite -> (proj2 (Nat.eqb_neq _ _)), Nat.eqb_refl by lia.
    destruct find as [[[]]|] eqn:Hfind.
    + apply find_some in Hfind as [Hin Hpref]. case H: tr => [[[]]|] //=.
      case=> <- <-. move: Hpref => /=.
      case Pos.eqb_spec => [<-|] //=.
      case/(prefix_cut _ sem_path_elem_reflect) =>> ->.
      rewrite skipn_app skipn_all /= Nat.sub_diag /=. by exists l0.
    + case find => //. case T: tr => [[[]]|] //= [X]. rewrite X {X} in T.
      by move/NOHID in T.
  - case=> [[[blk ofs] [|p]]] H.
    + rewrite H.
      intros [lc [Hin <-]]. unfold tr_call in H.
      rewrite -> (proj2 (Nat.eqb_neq _ _)), Nat.eqb_refl in H by lia.
      destruct find as [[[]]|] eqn:Hfind. now destruct tr as [[[]]|].
      apply find_none with (2 := Hin) in Hfind.
      simpl in Hfind. rewrite -> Pos.eqb_refl, prefix_app' in Hfind. easy.
      exact sem_path_elem_reflect.
    + unfold tr_call in *. rewrite -> (proj2 (Nat.eqb_neq _ _)) in * by lia.
      rewrite -> Nat.eqb_refl in *. destruct find as [[[]]|] eqn:Hfind.
      2: { destruct (find (fun '(j, _, _) => Pos.eqb j i) own) as [[[]]|] => //.
           now specialize (NOHID (i, l) blk ofs p). }
      destruct tr as [[[blk' ofs']]|] eqn:Htr; try discriminate.
      apply find_some in Hfind as [Hin Hpref]. simpl in Hpref.
      apply andb_prop in Hpref as [Hi0 Hpref]. apply Pos.eqb_eq in Hi0 as ->.
      intros [lc [Hin' <-]].
      assert ((i, l0, i1) = (i, lc, i')) as [= -> ->].
      { case (sem_path_ident_reflect (i, l0, i1) (i, lc, i')) as [->|Hneq].
        easy. apply prefix_app_cases in Hpref as [Hpref|Hpref].
        - specialize (NOALIAS _ _ _ _ Hin Hin' Hneq).
          simpl in NOALIAS. now rewrite Pos.eqb_refl Hpref in NOALIAS.
        - assert ((i, lc, i') <> (i, l0, i1)) as H0 by congruence.
          specialize (NOALIAS _ _ _ _ Hin' Hin H0).
          simpl in NOALIAS. now rewrite Pos.eqb_refl Hpref in NOALIAS.
        - exact sem_path_elem_reflect. }
      simpl. now rewrite skipn_app skipn_all Nat.sub_diag.
Qed.

Lemma blk_ofs_preserved_tr_call:
  forall tr n pp mut shr own,
    no_hidden tr n ->
    Coqlib.list_norepet (map snd pp) ->
    List.incl (mut ++ shr ++ own) pp ->
    blk_ofs_preserved (tr_call tr n mut shr own) n.
Proof.
  move=> tr n pp mut shr own NOHID NOREPET INCL.
  move=> /= i l b o i' l' b' o' s'. unfold tr_call.
  case Nat.eqb_spec; [lia|]. rewrite !Nat.eqb_refl => _.
  case F: find => [[[ic lc]]|].
  - case T: tr => [[[]]|] //=. injection 1 as -> -> ->.
    have [IN PREF] := find_some _ _ F.
    have IN0: In (ic, lc, i') pp.
    { apply INCL. apply in_or_app. by left. }
    case F': find => [[[ic' lc']]|].
    2: have:= find_none _ _ F' _ (in_or_app _ _ _ (or_introl _ IN));
       by rewrite Pos.eqb_refl.
    have [IN'] := find_some _ _ F'.
    move/Pos.eqb_spec => X. rewrite X {X} in IN'.
    have IN'0: In (ic', lc', i') pp by apply: INCL IN'.
    have [> N] := In_nth_error _ _ IN0. have [> N'] := In_nth_error _ _ IN'0.
    have:= map_nth_error snd _ _ N. have:= map_nth_error snd _ _ N'.
    move/(Coqlib_norepet_nth_error_injective _ _ _ _ NOREPET)/[apply] => E.
    rewrite E N {E} in N'. case: N'; intros <- <-. clear F F'.
    rewrite -H. case/andP: PREF => /Pos.eqb_spec; intros ->.
    move/(prefix_firstn _ sem_path_elem_reflect) => X.
    rewrite -{1}X {X} firstn_skipn T. case=> <- <- _ //.
  - case F': find => //=. by move/NOHID.
Qed.

Lemma valid_trmem_cont_tr_call:
  forall tr N pp mut shr own pe k e f idv,
    wf_trmem tr N ->
    visible_separated tr N (B.fn_penv f) ->
    no_hidden tr N ->
    owned_at_offset_zero tr N (B.fn_penv f) ->
    valid_trmem_cont tr N (B.fn_penv f) k ->
    marrs_no_aliasing mut ->
    Coqlib.list_norepet (map snd pp) ->
    Coqlib.list_norepet mut ->
    List.incl (mut ++ shr ++ own) pp ->
    (forall i p p', In (p, i) (mut ++ shr ++ own) ->
                    In (p', i) (mut ++ shr ++ own) -> p = p') ->
    (forall p i, In (p, i) mut -> pe!i = Some B.Mutable) ->
    (forall p i, In (p, i) own -> pe!i = Some B.Owned) ->
    enough_permission (B.fn_penv f) pe (mut ++ shr ++ own) ->
    valid_trmem_cont (tr_call tr N mut shr own) (S N) pe (Bsem.Kreturnto idv e f mut k).
Proof.
  intros * WF VSEP NOHID OWNZ VK NOALIAS NRPP NRMUT INCL INJ MUT OWN PERM. simpl.
  split; [|split; [|split; [|split; [|repeat split]]]].
  - apply visible_separated_tr_call_le_N. lia. exact VSEP.
  - now apply hidden_marrs_tr_call.
  - intros ic lc i l Hin. split; intro DEF.
    + case tr_call_SN_wf with (1 := WF) (2 := DEF) as (ic' & lc' & Hin' & Heq).
      rewrite Heq in DEF. clear Heq.
      assert ((ic, lc) = (ic', lc')) as [= <- <-]
          by apply INJ with (1 := in_or_app _ _ _ (or_introl _ Hin)) (2 := Hin').
      unfold tr_call. rewrite (proj2 (Nat.eqb_neq _ _)). lia.
      rewrite Nat.eqb_refl. case find as [[[]]|] eqn:Hfind.
      * apply find_some in Hfind as [Hin'' Hpref]. case DEF => [[[]]] > ->. eauto.
      * apply find_none with (2 := Hin) in Hfind.
        have:= prefix_app' _ sem_path_elem_reflect lc l.
        case/nandP: Hfind. by move/Pos.eqb_spec. by move/negP.
    + unfold tr_call. rewrite Nat.eqb_refl. case find as [[[ic' lc']]|] eqn:Hfind.
      2: { apply find_none with (2 := in_or_app _ _ _ (or_introl _ Hin)) in Hfind.
           now rewrite Pos.eqb_refl in Hfind. }
      apply find_some in Hfind as [Hin' Hp]. apply Pos.eqb_eq in Hp as ->.
      assert ((ic, lc) = (ic', lc')) as [= <- <-]
          by apply INJ with (1 := in_or_app _ _ _ (or_introl _ Hin)) (2 := Hin').
      by apply (tr_call_N_incl_domain _ N mut shr own).
  - eapply blk_ofs_preserved_tr_call; eassumption.
  - exact NOALIAS.
  - exact NRMUT.
  - intros i l i'. apply MUT.
  - move=>>. move/tr_call_le_N_blk_ofs_preserved. case=>>. lia. exact: OWNZ.
  - apply valid_trmem_cont_tr_call_same_cont. lia.
    { move=> i l j /[dup] /OWN H.
      move/(fun x => in_or_app (mut ++ shr) _ _ (or_intror _ x)). rewrite -app_assoc.
      move: PERM => /forallb_forall/[apply].
      case: (B.fn_penv f)!i => //. rewrite H. by case. }
    exact VK.
Qed.

Lemma match_mem_value_match_tr_value:
  forall tr n TM t v p,
    match_mem_value tr n p TM v t ->
    match_tr_value tr n p v t.
Proof.
  intros tr n TM. induction t; try easy.
  intros v [i l]. case v; try easy. intro lv.
  unfold match_mem_value. fold match_mem_value.
  unfold match_tr_value. fold match_tr_value.
  case (tr n (i, l)) as [[[] []]|] eqn:Htr at 1; try easy.
  rewrite Htr. intros H idx Hidx.
  decompose [and] (proj2 H idx Hidx). now apply IHt.
Qed.

Lemma tr_call_N_blk_ofs_preserved:
  forall tr N mut shr own i l blk ofs s,
    tr N (i, l) = Some (blk, ofs, s) ->
    (forall i' l' j, In ((i', l'), j) own -> i' <> i) ->
    exists s', tr_call tr N mut shr own N (i, l) = Some (blk, ofs, s').
Proof.
  move=> tr N ??? i l ?? s Htr OWN.
  rewrite/tr_call. rewrite (proj2 (Nat.eqb_neq _ _)). lia.
  rewrite Nat.eqb_refl. case find as [[[]]|].
  - rewrite Htr. eauto.
  - case find as [[[]]|] eqn:Hfind; [|eauto]. apply find_some in Hfind as [Hin T].
    move: T (OWN _ _ _ Hin) => /=. by case: Pos.eqb_spec.
Qed.

Lemma match_tr_value_tr_call_lt_N:
  forall tr N mut shr own t n v p,
    (n < N)%nat ->
    match_tr_value tr n p v t ->
    match_tr_value (tr_call tr N mut shr own) n p v t.
Proof.
  intros tr N mut shr own. induction t; try easy.
  intros n v [i l] Hn. case v; try easy. intros *. simpl.
  case tr as [[[]]|] eqn:Htr; try easy.
  apply (tr_call_lt_N_blk_ofs_preserved _ N mut shr own) in Htr as [_ ->].
  intros H idx Hidx. specialize (H idx Hidx). apply IHt. lia.
  exact: H. lia.
Qed.

Lemma match_tr_value_tr_call_le_N:
  forall tr N mut shr own t n v i l,
    (n <= N)%nat ->
    (forall i' l' j, In ((i', l'), j) own -> i' <> i) ->
    match_tr_value tr n (i, l) v t ->
    match_tr_value (tr_call tr N mut shr own) n (i, l) v t.
Proof.
  intros tr N mut shr own. induction t; try easy.
  intros n v i l Hn. case v; try easy. intros * OWN.
  case (Nat.ltb_spec0 n N); intros Hn'.
  - by apply match_tr_value_tr_call_lt_N.
  - simpl. case tr as [[[]]|] eqn:Htr; try easy.
    have T: n = N by lia. rewrite -> T in *. clear T Hn'.
    case/(tr_call_N_blk_ofs_preserved _ N mut shr own): Htr. exact: OWN.
    move=>> -> + idx => /[apply]. now apply: IHt.
Qed.

Lemma match_mem_value_tr_call_N:
  forall tr N mut shr own TM t v i l,
    no_hidden tr N ->
    (forall i' l' j, In ((i', l'), j) own -> i' <> i) ->
    match_mem_value tr N (i, l) TM v t ->
    match_mem_value (tr_call tr N mut shr own) N (i, l) TM v t.
Proof.
  intros tr N mut shr own TM. induction t; try easy.
  intros v i l NOHID. case v; try easy. intros lv OWN H.
  pose proof H as H'. revert H.
  unfold match_mem_value. fold match_mem_value.
  case tr as [[[b o] []]|] eqn:Htr; try easy.
  2: now specialize (NOHID (i, l) b o p).
  case/(tr_call_N_blk_ofs_preserved _ N mut shr own): Htr. exact: OWN.
  move=> s ->. case: s.
  - intro H; split. by case H. intros idx Hidx.
    decompose [and] (proj2 H idx Hidx). clear H. repeat split=> //.
    destruct (nth idx lv Vundef); try easy. simpl in H1.
    destruct tr as [[[]]|] eqn:Htr in H1; simpl.
    + case/(tr_call_N_blk_ofs_preserved _ N mut shr own): Htr.
      easy. by move=>> ->.
    + assert (~ (tr N (i, l ++ [Pcell idx]) defined))
          by (rewrite Htr; now intros []).
      rewrite (not_defined_is_None (tr_call _ _ _ _ _ _ _)).
      intro T. apply H. by move/tr_call_N_incl_domain: T. exact: H1.
    + now apply IHt.
  - intros _ _. apply match_tr_value_tr_call_le_N. lia. easy.
    now apply match_mem_value_match_tr_value with (TM := TM).
Qed.

Lemma match_mem_value_tr_call_lt_N:
  forall tr N mut shr own TM t n v p,
    (n < N)%nat ->
    match_mem_value tr n p TM v t ->
    match_mem_value (tr_call tr N mut shr own) n p TM v t.
Proof.
  intros tr N m c TM. induction t; try easy.
  intros n v [i l] Hn. case v; try easy. intros *. unfold match_mem_value.
  rewrite -> tr_call_lt_N by lia. case tr as [[[] []]|]; try easy.
  - intro H; split. by case H. intros idx Hidx.
    decompose [and] (proj2 H idx Hidx). clear H. repeat split=> //.
    unfold transl_value. rewrite -> tr_call_lt_N by lia. assumption.
    now apply IHt.
  - now apply match_tr_value_tr_call_lt_N.
Qed.

Lemma match_mem_cont_tr_call_same_cont:
  forall tr N mut shr own TM k n,
    (n <= N)%nat ->
    match_mem_cont tr n k TM ->
    match_mem_cont (tr_call tr N mut shr own) n k TM.
Proof.
  intros tr N m c TM. induction k; intros [|n]; eauto.
  intro Hn. simpl. intros [MME MMK]. split.
  - intros i v t He Ht. apply match_mem_value_tr_call_lt_N. lia.
    exact (MME _ _ _ He Ht).
  - apply IHk. lia. exact MMK.
Qed.

Lemma match_mem_cont_tr_call:
  forall tr N mut shr own TM k e f idv,
    no_hidden tr N ->
    match_mem_env tr N (B.fn_tenv f) e TM ->
    match_mem_cont tr N k TM ->
    match_mem_cont (tr_call tr N mut shr own) (S N)
                   (Bsem.Kreturnto idv (remove_own_params own e) f mut k) TM.
Proof.
  intros * NOHID MME MMK. split.
  - intros i v t He Ht.
    case (find (fun '((j, _), _) => Pos.eqb j i) own) as [[[]]|] eqn:Hfind.
    + apply find_some in Hfind as [Hin H].
      move/Pos.eqb_spec: H Hin => -> {p} Hin.
      by rewrite (remove_own_params_own own e _ _ _ Hin) in He.
    + rewrite (remove_own_params_other own e) in He.
      move=> i' ? j /(find_none _ _ Hfind) /Pos.eqb_spec //.
      specialize (MME _ _ _ He Ht).
      apply match_mem_value_tr_call_N; try easy.
      move=> i' ? j /(find_none _ _ Hfind) /Pos.eqb_spec //.
  - now apply match_mem_cont_tr_call_same_cont.
Qed.

Lemma transl_value_tr_call_lt_N:
  forall tr N mut shr own n p v,
    (n < N)%nat ->
    transl_value (tr_call tr N mut shr own) n (Some p) v =
    transl_value tr n (Some p) v.
Proof.
  intros tr N mut shr own n [i l] v Hn. case v; try easy. intro lv. simpl.
  unfold tr_call. rewrite -> (proj2 (Nat.eqb_neq _ _)) by lia.
  rewrite (proj2 (Nat.eqb_neq _ _)). lia.
  now rewrite -> (proj2 (Nat.ltb_lt _ _)) by lia.
Qed.

Lemma match_env_tr_call_lt_N:
  forall tr N mut shr own n e te,
    (n < N)%nat ->
    match_env tr n e te ->
    match_env (tr_call tr N mut shr own) n e te.
Proof. intros * ? ME i v. rewrite transl_value_tr_call_lt_N. lia. auto. Qed.

Lemma match_cont_tr_call_le_N:
  forall tr N ge mut shr own k tk n f,
    (n <= N)%nat ->
    match_cont tr n ge f k tk ->
    match_cont (tr_call tr N mut shr own) n ge f k tk.
Proof.
  induction k; simpl; intros; try easy; repeat destruct_match H0.
  - destruct H0; eauto.
  - eauto.
  - decompose [and] H0. repeat split; eauto.
    now apply match_env_tr_call_lt_N. apply IHk; [lia|easy].
Qed.

Lemma match_env_remove_own_params:
  forall tr N e te own,
    match_env tr N e te ->
    match_env tr N (remove_own_params own e) te.
Proof.
  move=> tr N e te own ME i v He.
  case (find (fun '(j, _, _) => Pos.eqb j i) own) as [[[]]|] eqn:H.
  + apply find_some in H as [Hin Halias].
    move/Pos.eqb_spec: Halias Hin => -> Hin.
    by rewrite (remove_own_params_own _ _ _ _ _ Hin) in He.
  + rewrite remove_own_params_other in He.
    { move/find_none: H => + > => /[apply]/Pos.eqb_spec //. }
    by rewrite (ME _ _ He).
Qed.

Lemma match_cont_tr_call:
  forall tr N mut shr own p id e f k te tf tk,
    match_env tr N e te ->
    match_cont tr N (genv_of_program p) (Some f) k tk ->
    transl_function (genv_of_program p) f = OK tf ->
    match_cont (tr_call tr N mut shr own) (S N) (genv_of_program p) None
      (Bsem.Kreturnto id (remove_own_params own e) f mut k)
      (Kcall id tf PTree.Empty te tk).
Proof.
  move=> tr N mut shr own > ME MK TF /=. repeat split; eauto.
  - move=> i v He.
    case (find (fun '(j, _, _) => Pos.eqb j i) own) as [[[]]|] eqn:H.
    + apply find_some in H as [Hin Halias].
      move/Pos.eqb_spec: Halias Hin => -> Hin.
      by rewrite (remove_own_params_own _ _ _ _ _ Hin) in He.
    + rewrite remove_own_params_other in He.
      { move/find_none: H => + > => /[apply]/Pos.eqb_spec //. }
      rewrite (ME _ _ He) /= {He}. case: v => //= lv.
      rewrite /tr_call. rewrite (proj2 (Nat.eqb_neq _ _)). lia.
      rewrite Nat.eqb_refl H. case (find _ mut) as [[[]]|] => //.
      by case tr as [[[]]|].
  - now apply match_cont_tr_call_le_N.
Qed.

Lemma valid_blocks_tr_call:
  forall tr n mut shr own tm,
    wf_trmem tr n ->
    valid_blocks tr n tm ->
    valid_blocks (tr_call tr n mut shr own) (S n) tm.
Proof.
  move=> tr n mut shr own tm WF H n' [i l] b o s. case/Nat.lt_eq_cases => [X|->].
  - have: (n' <= n)%nat by lia.
    move/tr_call_le_N_blk_ofs_preserved/[apply] => - [>] /H. lia.
  - unfold tr_call. rewrite Nat.eqb_refl. case find => [[[>]_]|].
    move/H; lia. move/(ex_intro _ (b, o, s))/(trmem_max _ _ WF). lia.
Qed.

Lemma owned_at_offset_zero_tr_call tr n f f':
  forall pp,
    wf_trmem tr n ->
    enough_permission (Syntax.fn_penv f) (Syntax.fn_penv f') pp = true ->
    owned_at_offset_zero tr n (B.fn_penv f) ->
    let mut := mut_args (Syntax.fn_penv f') pp in
    let own := own_args (Syntax.fn_penv f') pp in
    let shr := shr_args (Syntax.fn_penv f') pp in
    owned_at_offset_zero (tr_call tr n mut shr own) (S n) (B.fn_penv f').
Proof.
  move=> pp WF PERM H.
  set mut := mut_args _ _. set own := own_args _ _. set shr := shr_args _ _.
  move=> /= i l b o s + PE. unfold tr_call. rewrite Nat.eqb_refl.
  case F: find => [[[i' l']]|] //.
  2: move/(ex_intro _ (b, o, s))/(trmem_max _ _ WF); lia.
  have:= find_some _ _ F => - [/[swap] /Pos.eqb_spec -> IN].
  have: In (i', l', i) pp.
  { case/in_app_iff: IN. by move/mut_args_incl.
    case/in_app_iff. by move/shr_args_incl. by move/own_args_incl. }
  move/forallb_forall: PERM => /[apply]. rewrite PE.
  case PE': (B.fn_penv f)!i' => [[]|] // _. eauto.
Qed.

End CALL_PRESERVATION_LEMMAS.

Section RETURN_PRESERVATION_LEMMAS.

Lemma tr_return_lt_N:
  forall tr N n p,
    (n < N)%nat ->
    tr_return tr N n p = tr n p.
Proof.
  intros * Hn. unfold tr_return. rewrite -> (proj2 (Nat.eqb_neq _ _)) by lia.
  now rewrite (proj2 (Nat.ltb_lt _ _) Hn).
Qed.

Lemma tr_return_old_levels_same_domain:
  forall tr N m n p,
    (n <= N)%nat ->
    origin_defined tr N m ->
    hidden_marrs tr N m ->
    (tr_return tr N n p defined) <-> (tr n p defined).
Proof.
  intros * Hn ODEF HM. unfold tr_return. case (Nat.eqb_spec n N) as [->|].
  2: case Nat.ltb_spec; [easy|lia].
  case tr as [[[b o] [|[i' l']]]|] eqn:Htr; try easy. destruct p as [i l].
  have:= proj1 (HM i l i' l'); rewrite Htr => /(_ eq_refl) [lc [Hin T]].
  have:= ODEF _ _ _ l' Hin. by rewrite T Htr.
Qed.

Lemma tr_return_defined_le_N:
  forall tr N n p,
    tr_return tr N n p defined ->
    (n <= N)%nat.
Proof.
  intros *. unfold tr_return. case Nat.eqb_spec. lia.
  case Nat.ltb_spec. lia. now intros _ _ [].
Qed.

Lemma tr_return_no_hidden:
  forall tr N,
    no_hidden tr (S N) ->
    no_hidden (tr_return tr N) N.
Proof.
  intros * NOHID. intros p blk ofs p'.
  unfold tr_return. rewrite Nat.eqb_refl.
  case (tr N p) as [[[] []]|] eqn:Htr; try discriminate.
  apply NOHID.
Qed.

Lemma wf_trmem_tr_return:
  forall tr N m,
    wf_trmem tr (S N) ->
    no_hidden tr (S N) ->
    origin_defined tr N m ->
    hidden_marrs tr N m ->
    marrs_no_aliasing m ->
    wf_trmem (tr_return tr N) N.
Proof.
  intros * WF NOHID ODEF HM NOALIAS. apply mk_wf_trmem.
  - intros n i l1 DEF l2 PREF.
    assert (n <= N)%nat as Hn by now apply tr_return_defined_le_N in DEF.
    elim (tr_return_old_levels_same_domain tr N m n (i, l1) Hn); try easy.
    intros H _. apply H in DEF. clear H.
    apply (tr_return_old_levels_same_domain tr N m n (i, l2) Hn); try easy.
    apply trmem_prefix with (1 := WF) (2 := DEF) (3 := PREF).
  - intros *. unfold tr_return. case Nat.eqb_spec. lia.
    case Nat.ltb_spec. lia. now intros _ _ _ [].
  - intros * Htr.
    pose proof (tr_return_defined_le_N _ _ _ _ (ex_intro (fun _ => _) _ Htr)).
    case (Nat.eqb_spec n N) as [->|].
    + apply tr_return_no_hidden in NOHID. now specialize (NOHID p b1 o1 p1).
    + assert (n < N)%nat by lia. revert Htr. rewrite -> 2!tr_return_lt_N by lia.
      apply trmem_hid_sep with (1 := WF).
  - intros * Hn. rewrite -> tr_return_lt_N by lia.
    intro Htr. apply trmem_hid with (1 := WF) in Htr as [s Htr]; [|lia].
    unfold tr_return. case (Nat.eqb_spec (S n) N) as [<-|].
    + rewrite Htr. destruct s. eauto. case p as [i'' l''].
      now apply trmem_hid with (1 := WF) in Htr; [|lia].
    + case Nat.ltb_spec. eauto. lia.
Qed.

Lemma visible_separated_tr_return:
  forall tr N pe pe',
    visible_separated tr N pe ->
    visible_separated tr (S N) pe' ->
    no_hidden tr (S N) ->
    hidden_is_mut tr N pe' ->
    wf_trmem tr (S N) ->
    visible_separated (tr_return tr N) N pe.
Proof.
  intros * VSEPN VSEPSN NOHID HMUT WF i l b o p H Hpe Hp.
  assert (exists s, tr N (i, l) = Some (b, o, s)) as [[|[i1 l1]] Htr].
  { revert H. unfold tr_return. rewrite Nat.eqb_refl.
    case tr as [[[] [|[i' l']]]|] eqn:Htr; try easy. eauto.
    apply trmem_hid with (1 := WF) in Htr as [s ->]; [|lia].
    intros [= -> ->]. eauto. }
  - split.
    + intros n * Hn. rewrite -> tr_return_lt_N by easy.
      now apply (VSEPN i l b o p Htr Hpe Hp).
    + intros *. unfold tr_return. rewrite Nat.eqb_refl.
      case (tr N p') as [[[] [|[i1 l1]]]|] eqn:Htr'; try easy.
      intros Hp' [= -> ->]. revert Hp' Htr'.
      now apply (VSEPN i l b o p Htr Hpe Hp).
      intros Hp' Htr1. assert (s' = Visible) as ->.
      { destruct s' as [|x]. easy. now specialize (NOHID (i1, l1) b' o' x). }
      pose proof (HMUT _ _ _ _ _ Htr') as Hpe'. apply not_eq_sym. revert Htr.
      apply (VSEPSN _ _ _ _ _ Htr1 Hpe' (Syntax.perm_le_refl _)). lia.
  - unfold tr_return in H. rewrite Nat.eqb_refl Htr in H.
      pose proof (HMUT _ _ _ _ _ Htr) as Hpe'. split.
    + intros n * Hn. rewrite -> tr_return_lt_N by easy.
      apply (VSEPSN _ _ _ _ _ H Hpe' (Syntax.perm_le_refl _)). lia.
    + intros *. unfold tr_return. rewrite Nat.eqb_refl.
      case (tr N p') as [[[] [|[i2 l2]]]|] eqn:Htr'; try easy.
      intros Hp' [= -> ->]. revert Htr'.
      apply (VSEPSN _ _ _ _ _ H Hpe' (Syntax.perm_le_refl _)). lia.
      intros Hp' Htr1. assert (s' = Visible) as ->.
      { destruct s' as [|x]. easy. now specialize (NOHID (i2, l2) b' o' x). }
      assert ((i1, l1) <> (i2, l2)) as Hsep.
      { now apply trmem_hid_sep with (1 := WF) (2 := Htr) (3 := Htr'). }
      revert Hsep Htr1.
      apply (VSEPSN _ _ _ _ _ H Hpe' (Syntax.perm_le_refl _)).
Qed.

Lemma blk_ofs_preserved_tr_return tr N:
  forall n,
    (n < N)%nat ->
    blk_ofs_preserved tr n ->
    blk_ofs_preserved tr N ->
    blk_ofs_preserved (tr_return tr N) n.
Proof.
  move=> n Hn H H' i l b o i' l' b' o' s'.
  rewrite tr_return_lt_N; [lia|]. unfold tr_return.
  case Nat.eqb_spec => [X|NEQ] T.
  - rewrite -X in H'. rewrite -X {X}.
    case T': tr => [[[??] [|[i'' l'']]]|] //.
    + case=> <- <- _; by apply: H T T'.
    + move=> T''. have [<- <-]:= H' _ _ _ _ _ _ _ _ _ T' T''.
      by apply: H T T'.
  - case Nat.ltb_spec0 => // _. by apply: H T.
Qed.

Lemma valid_trmem_cont_tr_return:
  forall tr N m k n pe,
    (n <= N)%nat ->
    origin_defined tr N m ->
    hidden_marrs tr N m ->
    blk_ofs_preserved tr N ->
    valid_trmem_cont tr n pe k ->
    valid_trmem_cont (tr_return tr N) n pe k.
Proof.
  intros tr N m k n pe Hn ODEF HM MPRES. revert k n pe Hn.
  induction k; try easy.
  intros [|n] * Hn. easy. simpl. intros H. decompose [and] H. clear H.
  split; [|split; [|split; [|split; [|repeat split]]]].
  - unfold visible_separated. intros *. rewrite -> tr_return_lt_N by lia.
    intros Htr Hpe Hp. split; intros *; [intro H|].
    all: rewrite -> tr_return_lt_N by lia. revert H.
    all: now apply (H0 _ _ _ _ _ Htr Hpe Hp).
  - unfold hidden_marrs. intros *. rewrite -> tr_return_lt_N by lia. eauto.
  - unfold origin_defined. intros * Hin.
    pose proof (tr_return_old_levels_same_domain _ _ _ _ (i, l0) Hn ODEF HM).
    apply iff_sym. apply iff_sym in H. apply iff_stepl with (1 := H).
    rewrite -> tr_return_lt_N by lia. eauto.
  - apply blk_ofs_preserved_tr_return; eassumption.
  - assumption.
  - assumption.
  - assumption.
  - move=>>. rewrite tr_return_lt_N. lia. eauto.
  - apply IHk. lia. easy.
Qed.

Section match_mem_env_tr_return.
  Variables (tr: trmem) (N: nat) (m: list (sem_path * ident)).
  Variables (te te0: tenv).
  Variables (e e0 e': env).
  Variable (TM: Memory.mem).

  Hypothesis He'1:
    forall i l i' l',
      In ((i, l), i') m ->
      get_env_path e' (i, l ++ l') = get_env_path e0 (i', l').
  Hypothesis He'2:
    forall i v,
      e'!i = Some v ->
      exists v', e!i = Some v'.
  Hypothesis He'2':
    forall p lv,
      get_env_path e' p = Some (Varr lv) ->
      ~~ mem sub_path p (map fst m) ->
      exists lv', get_env_path e p = Some (Varr lv').
  Hypothesis He'3:
    forall i v v',
      e!i = Some v ->
      e'!i = Some v' ->
      unchanged i [] m v v'.

  Lemma match_mem_env_tr_return:
    match_mem_env tr (S N) te0 e0 TM ->
    match_mem_env tr N te e TM ->
    wf_trmem tr (S N) ->
    hidden_marrs tr N m ->
    origin_defined tr N m ->
    (forall p i, In (p, i) m -> te0!i = get_tenv_path te p) ->
    (forall p i, In (p, i) m -> exists lv lv', get_env_path e p = Some (Varr lv) /\
                                     e0!i = Some (Varr lv')) ->
    no_hidden tr (S N) ->
    match_mem_env (tr_return tr N) N te e' TM.
  Proof.
    intros MME0 MME WF HM ODEF MTYP MVARR NOHID i v0 t0 He'0 Ht'0.
    assert (get_value_path [] v0 = Some v0) as Hvl by reflexivity.
    assert (get_type_path [] t0 = Some t0) as Htl by reflexivity.
    destruct (He'2 _ _ He'0) as [x0 He0].
    pose proof (MME i x0 t0 He0 Ht'0) as Hmatch.
    pose proof (He'3 _ _ _ He0 He'0) as Hunchanged.
    revert Hunchanged Hmatch.
    assert (get_value_path [] x0 = Some x0) as Hxl by reflexivity.
    revert Hvl Htl Hxl.
    assert (forall l', prefix sem_path_elem_beq l' [] ->
                  l' <> [] ->
                  forall q j, In (q, j) m -> ~~ sem_path_prefix q (i, l'))
        by now intros []. revert H.
    generalize x0 at 2 3 4 as x.
    generalize ([]: list sem_path_elem) as l.
    generalize v0 at 2 3 4 as v, t0 at 2 3 4 as t.
    induction v using value_ind'; destruct t; try easy.
    unfold match_mem_value at 2. fold match_mem_value. rename l into lv.
    intros l v Hpref Hvl Htl Hxl.
    case (find (fun '(q, _) => sem_path_beq q (i, l)) m)
      as [[[ic lc] i']|] eqn:Hfind.
    - intros _ _. apply find_some in Hfind as [Hin Heq]. revert Heq.
      case (eqSemPath (ic, lc) (i, l)) as [[= -> ->]|Hneq]; try easy; intros _.
      unfold tr_return at 1. rewrite Nat.eqb_refl.
      assert (get_env_path e0 (i', []) = Some (Varr lv)) as He0_i'.
      { rewrite <- (He'1 i l i' [] Hin). unfold get_env_path.
        rewrite He'0. simpl. now rewrite app_nil_r. }
      assert (te0!i' = Some (Tarr t)) as Ht0.
      { rewrite (MTYP _ _ Hin). simpl. now rewrite Ht'0. }
      assert (tr (S N) (i', []) defined) as DEF_S.
      { apply match_mem_env_get_env_path with (1 := MME0) (2 := He0_i') (t := t).
        simpl. now rewrite Ht0. }
      assert (tr N (i, l) defined) as DEF.
      { apply (ODEF _ _ _ [] Hin) in DEF_S. now rewrite app_nil_r in DEF_S. }
      pose proof DEF as [[[blk ofs] s] Htr]. rewrite Htr.
      pose proof (proj2 (HM i l i' []) DEF) as H.
      specialize (H (ex_intro _ _ (conj Hin (app_nil_r _)))).
      rewrite Htr in H. injection H as ->.
      pose proof DEF_S as [[[blk' ofs'] s'] Htr']; rewrite Htr'.
      unfold get_env_path in He0_i'.
      destruct e0!i' eqn:He0_1; try discriminate. injection He0_i' as ->.
      specialize (MME0 i' (Varr lv) (Tarr t) He0_1 Ht0).
      assert (forall l' : list sem_path_elem,
                 tr (S N) (i', l')
                 = tr_return tr N N (i, (l ++ l'))) as T.
      { intros l'. unfold tr_return. rewrite Nat.eqb_refl.
        case (tr (S N) (i', l')) as [[[]]|] eqn:Htr''.
        - assert (option_map snd (tr N (i, l ++ l'))
                  = Some (Hidden (i', l'))).
          { pose proof (ex_intro (fun _ => _) (b, i0, s) Htr'') as T.
            apply (ODEF i l i' _ Hin) in T. apply HM; eauto. }
          case (tr N (i, l ++ l')) as [[[]]|].
          injection H as ->. simpl in Htr''. now rewrite Htr''. discriminate.
        - assert (~ (tr (S N) (i', l') defined)).
          { intros T. rewrite Htr'' in T. now case T. }
          apply (not_iff_compat (ODEF i l i' l' Hin)) in H.
          case (tr N (i, l ++ l')) as [[[]]|].
          elim H. eauto. reflexivity. }
      specialize match_mem_value_eq with (l1 := []) (1 := T) (2 := MME0).
      unfold match_mem_value at 1. fold match_mem_value.
      unfold tr_return at 1. now rewrite Nat.eqb_refl Htr Htr'.
    - assert (~~ mem sub_path (i, l) (map fst m)) as Hsub.
      { case (@negP (mem _ _ _)). easy.
        intro H. exfalso. apply H. clear H. intro H.
        apply mem_spec in H as [[i' l'] [Hin H]]. simpl in H.
        apply andb_prop in H as [Hi' Hpref']. apply Pos.eqb_eq in Hi' as ->.
        apply in_map_fst_pair in Hin as [i' Hin].
        case (list_reflect _ sem_path_elem_reflect l l') as [<-|Hneq].
        apply find_none with (1 := Hfind) in Hin.
        now apply sem_path_beq_false in Hin.
        specialize (Hpref l' Hpref' (not_eq_sym Hneq) _ _ Hin). simpl in Hpref.
        now rewrite Pos.eqb_refl (prefix_refl _ sem_path_elem_reflect) in Hpref. }
      assert (exists lv', get_env_path e (i, l) = Some (Varr lv')) as [lv' He].
      { apply (He'2' (i, l) lv). simpl. now rewrite He'0. easy. }
      assert (tr N (i, l) defined) as DEF.
      { apply match_mem_env_get_env_path with (1 := MME) (2 := He) (t := t).
        simpl. now rewrite Ht'0. }
      pose proof DEF as [[[blk ofs] s] Htr].
      assert (s = Visible) as ->.
      { destruct s as [|[i' l']]. easy.
        pose proof (proj1 (HM i l i' l')) as H. rewrite Htr in H.
        case (H eq_refl) as [x [Hin <-]].
        apply find_none with (2 := Hin) in Hfind.
        specialize (Hpref x (prefix_app' _ sem_path_elem_reflect _ _)).
        revert Hfind. case (eqSemPath (i, x) (i, x ++ l')). easy.
        intros Hneq _. assert (x <> x ++ l') as Hneq' by congruence.
        specialize (Hpref Hneq' _ _ Hin). revert Hpref. simpl.
        rewrite Pos.eqb_refl. now rewrite (prefix_refl _ sem_path_elem_reflect). }
      unfold tr_return at 1. simpl. rewrite Nat.eqb_refl Htr.
      simpl in He. rewrite He0 in He. simpl in He. rewrite Hxl in He.
      injection He as ->. intro U.
      apply unchanged_unchanged' with (1 := Hsub) in U.
      2: { intros l' Hin. apply in_map_fst_pair in Hin as [i' Hin].
           specialize (MVARR (i, l ++ l') i' Hin) as (x & x' & Hx & Hx').
           simpl in Hx. rewrite He0 in Hx. simpl in Hx.
           apply get_value_path_app in Hx as (y & Hy & Hx).
           exists x, x'. split. congruence.
           generalize (He'1 _ _ _ [] Hin). simpl.
           rewrite He'0 Hx'. simpl. rewrite app_nil_r. intro H. clear Hx'.
           apply get_value_path_app in H as (y' & Hy' & Hx').
           rewrite Hvl in Hy'. now injection Hy' as <-. }
      apply unchanged'_Varr with (1 := Hsub) in U as [Hlen U].
      intros [SZ H]; split. by rewrite -Hlen. intros idx Hidx.
      assert (idx < length lv')%nat as Hidx' by now rewrite <- Hlen in Hidx.
      decompose [and] (H idx Hidx'). clear H.
      assert (exists v, nth_error lv idx = Some v) as [v Hnth]
            by now apply nth_error_Some, option_not_None_exists in Hidx.
      assert (exists v, nth_error lv' idx = Some v) as [v' Hnth']
            by now apply nth_error_Some, option_not_None_exists in Hidx'.
      rewrite H1 (nth_error_nth _ _ _ Hnth) (nth_error_nth _ _ _ Hnth').
      specialize (U idx _ _ Hnth' Hnth).
      repeat split=> //.
      + destruct v', v; simpl in U; try discriminate; try now rewrite U. easy.
        simpl. unfold tr_return. rewrite Nat.eqb_refl.
        case (tr N (i, l ++ [Pcell idx])) as [[[blk' ofs'] [|[i' l']]]|] eqn:Htr'.
        * reflexivity.
        * case trmem_hid with (1 := WF) (3 := Htr') as [? ->]. lia. easy.
        * reflexivity.
      + apply nth_error_In in Hnth as Hin.
        apply (proj1 (Forall_forall _ _) IH v Hin t _ v').
        * intros l' Hpref' Hneq q j Hin'.
          case (list_reflect _ sem_path_elem_reflect l l') as [<-|].
          ** case (sem_path_prefix q (i, l)) eqn:Hpref''; try easy.
             assert (sub_path (i, l) q) by easy.
             assert (mem sub_path (i, l) (map fst m)) as Hmem.
             { apply mem_spec. exists q. split.
               apply in_map_pair_fst with (1 := Hin'). easy. }
             now rewrite Hmem in Hsub.
          ** apply Hpref with (j := j).
             now apply (prefix_app_not_eq _ sem_path_elem_reflect _ _ _ Hpref').
             congruence. easy.
        * apply get_value_path_app. rewrite Hvl. exists (Varr lv). split. easy.
          simpl. now rewrite Hnth.
        * apply get_type_path_app. rewrite Htl. eauto.
        * apply get_value_path_app. rewrite Hxl. exists (Varr lv'). split. easy.
          simpl. now rewrite Hnth'.
        * now apply unchanged'_unchanged.
        * now rewrite (nth_error_nth _ _ _ Hnth') in H4.
  Qed.

End match_mem_env_tr_return.

Lemma valid_blocks_tr_return:
  forall tr n tm,
    valid_blocks tr (S n) tm ->
    valid_blocks (tr_return tr n) n tm.
Proof.
  move=> tr n tm H n' [i l] b o s. case/Nat.lt_eq_cases => [X|->].
  - rewrite tr_return_lt_N. lia. move/H; lia.
  - unfold tr_return. rewrite Nat.eqb_refl.
    case T: tr => [[[??] []]|] //.
    + case=> <- _ _; move/H: T; lia.
    + move/H; lia.
Qed.

Lemma owned_at_offset_zero_tr_return tr n pe:
  blk_ofs_preserved tr n ->
  owned_at_offset_zero tr n pe ->
  owned_at_offset_zero (tr_return tr n) n pe.
Proof.
  move=> MPRES H i l b o s. unfold tr_return. rewrite Nat.eqb_refl.
  case T: tr => [[[??] [|[i' l']]]|] //.
  - case=> _ <- _; by apply: H T.
  - move=> T' PE.
    have [_ <-]:= MPRES _ _ _ _ _ _ _ _ _ T T'.
    by apply: H T PE.
Qed.

End RETURN_PRESERVATION_LEMMAS.

(** ** Semantics of calloc *)

Definition memory_chunk_default_value (ch: memory_chunk) :=
  match ch with
  | Mbool
  | Mint8signed | Mint8unsigned
  | Mint16signed | Mint16unsigned | Mint32 | Many32 => CValues.Vint (Int.zero)
  | Mint64 | Many64 => CValues.Vlong (Int64.zero)
  | Mfloat32 => CValues.Vsingle (Float32.zero)
  | Mfloat64 => CValues.Vfloat (Float.zero)
  end.

Inductive calloc_sem (ge: Senv.t) :  list val -> Mem.mem -> trace -> val -> Mem.mem -> Prop :=
| calloc_sem_success: forall ch b sz n m m' m'',
    size_chunk ch = Ptrofs.unsigned sz ->
    Ptrofs.unsigned (Ptrofs.mul sz n) = Ptrofs.unsigned sz * Ptrofs.unsigned n ->
    Mem.alloc m (- size_chunk Mptr) (Ptrofs.unsigned (Ptrofs.mul sz n)) = (m', b) ->
    Mem.load Mptr m'' b (- size_chunk Mptr) = Some (Vptrofs (Ptrofs.mul sz n)) ->
    Mem.valid_access m'' Mptr b (- size_chunk Mptr) Freeable ->
    (forall i,
      0 <= Z.of_nat i < Ptrofs.unsigned n ->
      Mem.valid_access m'' ch b (size_chunk ch * Z.of_nat i) Freeable /\
      Mem.load ch m'' b (size_chunk ch * Z.of_nat i)
                      = Some (memory_chunk_default_value ch)) ->
    Mem.unchanged_on (fun b _ => Mem.valid_block m b) m' m'' ->
    calloc_sem ge [Vptrofs n; Vptrofs sz] m E0 (Vptr b Ptrofs.zero) m''
| calloc_sem_overflow: forall sz n m,
    Ptrofs.unsigned sz * Ptrofs.unsigned n > Ptrofs.max_unsigned ->
    calloc_sem ge [Vptrofs n; Vptrofs sz] m E0 Vnullptr m.

Axiom calloc_sem_extcall: Events.external_call calloc = calloc_sem.

Local Open Scope option_bool_monad_scope.

Fixpoint foldi {A: Type} (f: A -> nat -> option A) (a0: A) (n: nat) :=
  match n with
  | O => Some a0
  | S m => doo a1 <- f a0 n; foldi f a1 m
  end.

Theorem calloc_sem_success_exists:
  forall ch sz n m,
    size_chunk ch = Ptrofs.unsigned sz ->
    Ptrofs.unsigned n * Ptrofs.unsigned sz <= Ptrofs.max_unsigned ->
    exists b m' m'',
      Mem.alloc m (- size_chunk Mptr) (Ptrofs.unsigned (Ptrofs.mul sz n)) = (m', b) /\
      Mem.load Mptr m'' b (- size_chunk Mptr) = Some (Vptrofs (Ptrofs.mul sz n)) /\
      Mem.valid_access m'' Mptr b (- size_chunk Mptr) Freeable /\
      (forall i, 
          0 <= Z.of_nat i < Ptrofs.unsigned n ->
          Mem.valid_access m'' ch b (size_chunk ch * Z.of_nat i) Freeable /\
          Mem.load ch m'' b (size_chunk ch * Z.of_nat i)
                      = Some (memory_chunk_default_value ch)) /\
      Mem.unchanged_on (fun b _ => Mem.valid_block m b) m' m''.
Proof.
  intros * SZCH V. case ALLOC: Mem.alloc => [m' b].
  have F: Mem.valid_access m' Mptr b (- size_chunk Mptr) Freeable.
  { eapply Mem.valid_access_alloc_same. exact ALLOC. lia.
    have:= Ptrofs.unsigned_range (Ptrofs.mul sz n); lia.
    unfold Mptr; change Archi.ptr64 with true; simpl;
      apply Z.divide_opp_r, Z.divide_refl. }
  have:= Mem.valid_access_implies _ _ _ _ _ Writable F (perm_F_any _).
  case/(Mem.valid_access_store _ _ _ _ (Vptrofs (Ptrofs.mul sz n))) => m'' STORE.
  have: forall i, - size_chunk Mptr <= i < - size_chunk Mptr + size_chunk Mptr ->
                  ~ Mem.valid_block m b.
  { move=>> _; rewrite/Mem.valid_block;
    rewrite (Mem.alloc_result _ _ _ _ _ ALLOC) /Coqlib.Plt; lia. }
  move/(Mem.store_unchanged_on (fun b _ => Mem.valid_block m b) _ _ _ _ _ _ STORE) => U.
  have: exists m''',
          foldi (fun m i =>
              Mem.store ch m b (size_chunk ch * (Z.pred (Z.of_nat i)))
                               (memory_chunk_default_value ch)
            ) m'' (Z.to_nat (Ptrofs.unsigned n)) = Some m''' /\
          Mem.load Mptr m''' b (- size_chunk Mptr) = Some (Vptrofs (Ptrofs.mul sz n)) /\
          Mem.valid_access m''' Mptr b (- size_chunk Mptr) Freeable /\
          (forall i, 
            (0 <= i < Z.to_nat (Ptrofs.unsigned n))%nat ->
            Mem.valid_access m''' ch b (size_chunk ch * Z.of_nat i) Freeable /\
            Mem.load ch m''' b (size_chunk ch * Z.of_nat i)
                        = Some (memory_chunk_default_value ch)) /\
          (forall i,
            Ptrofs.unsigned n <= i < Ptrofs.unsigned n ->
            Mem.valid_access m''' ch b (size_chunk ch * i) Freeable /\
            Mem.load ch m''' b (size_chunk ch * i) = Mem.load ch m'' b (size_chunk ch * i)) /\
          Mem.unchanged_on (fun b _ => Mem.valid_block m b) m' m'''.
  { have: forall i, (0 <= i < Z.to_nat (Ptrofs.unsigned n))%nat ->
                    Mem.valid_access m'' ch b (size_chunk ch * Z.of_nat i) Freeable.
    { move=>> [/Znat.inj_le ? /Znat.inj_lt].
      have Hn := Ptrofs.unsigned_range n. rewrite Znat.Z2Nat.id; [lia|] => ?.
      apply (Mem.store_valid_access_1 _ _ _ _ _ _ STORE).
      apply Mem.valid_access_implies with (p1 := Freeable); [|econstructor].
      eapply Mem.valid_access_alloc_same. exact ALLOC.
      change (size_chunk Mptr) with 8; case ch; cbn -[Z.mul Z.of_nat]; lia.
      move: V. rewrite/Ptrofs.mul -SZCH => V. rewrite Ptrofs.unsigned_repr.
      move: V. rewrite/Ptrofs.max_unsigned. case ch; cbn -[Z.mul Z.of_nat]; lia.
      rewrite -Z.mul_succ_r. case ch; cbn -[Z.mul Z.of_nat]; lia.
      apply Z.divide_mul_l. case ch; cbn; try easy;
      change 8 with (4 * 2); now apply Z.divide_mul_l. }
    Tactic Notation "cbn'" := cbn -[Z.pred Z.of_nat].
    set v := memory_chunk_default_value ch.
    have:= Mem.load_store_same _ _ _ _ _ _ STORE.
    have:= Mem.store_valid_access_1 _ _ _ _ _ _ STORE _ _ _ _ F => {STORE}.
    move: U; set P := fun b _ => Mem.valid_block m b.
    have: (Ptrofs.unsigned n <= Ptrofs.unsigned n) by lia.
    rewrite -{1 6}(Znat.Z2Nat.id (Ptrofs.unsigned n));
      [have:= Ptrofs.unsigned_range n; lia|].
    elim/nat_ind: {1 3 4 5}(Z.to_nat (Ptrofs.unsigned n)) m''; cbn'.
    - move=> m'' U A L; exists m''. do 4 split=> //. lia.
      split=> //. move=> i; split=> //.
      have:= H0 (Z.to_nat i). rewrite Znat.Z2Nat.id; [lia|].
      move=> T; apply: T; lia.
    - move=> n' IH m2 Hn' U A L W. rewrite Znat.Nat2Z.inj_succ Z.pred_succ.
      have: (0 <= n' < S n')%nat by lia.
      move=> X; have: (0 <= n' < Z.to_nat (Ptrofs.unsigned n))%nat by lia.
      move/W/Mem.valid_access_implies/(_ (perm_F_any Writable)).
      move/Mem.valid_access_store. case/(_ v) => m3 STORE. rewrite STORE /=.
      have:= Mem.store_unchanged_on P _ _ _ _ _ _ STORE.
      move/(_ (fun _ _ => Mem.fresh_block_alloc _ _ _ _ _ ALLOC)).
      move: U => /Mem.unchanged_on_trans/[apply] U.
      move: L; rewrite -(Mem.load_store_other _ _ _ _ _ _ STORE) => [|L].
      { right. left. change (size_chunk Mptr) with 8 => /=.
        case ch; cbn -[Z.mul Z.of_nat]; lia. }
      move/(Mem.store_valid_access_1 _ _ _ _ _ _ STORE) in A.
      rewrite Znat.Nat2Z.inj_succ in Hn'.
      case (IH m3 (Zorder.Zle_succ_le _ _ Hn') U A L) => [|m4 [->] [L' [A' [W' [S' U']]]]].
      { move=>> ?; apply Mem.store_valid_access_1 with (1 := STORE). apply W; lia. }
      exists m4. do 4 split=> //; [|split=> //].
      + move=> i. case (Nat.ltb_spec i n') => ??. apply: W'; lia.
        have [|+ ->] := S' (Z.of_nat i); [lia|]. have -> : i = n' by lia.
        rewrite (Mem.load_store_same _ _ _ _ _ _ STORE) /v; by case ch.
      + move=> i H. have [|+ ->] := S' i; [lia|]. split=> //.
        apply (Mem.load_store_other _ _ _ _ _ _ STORE); do 2 right.
        rewrite -Z.mul_succ_r. have: 0 <= size_chunk ch by case ch; cbn; lia. nia. }
  case=> m''' {U} [X] [L] [A] [SZ] [_] U. exists b, m', m'''.
  do 4 split=> //. move=>> ?; apply SZ; lia.
Qed.

Local Close Scope option_bool_monad_scope.

Axiom external_call_sem_preservation:
  forall tr n e f k p tp hfuncs Habort Hcalloc tm te ef pargs vargs pp pe vmarrs r e',
    transl_program' hfuncs Habort Hcalloc p = OK tp ->
    pargs_params (B.ef_params ef) pargs = Some pp ->
    match_mem_env tr n (B.fn_tenv f) e tm ->
    match_mem_cont tr n k tm ->
    match_env tr n e te ->
    valid_blocks tr n tm ->
    external_call ef (combine pargs vargs) = (vmarrs, r) ->
    update_env (build_env PTree.Empty (combine (map snd (mut_args pe pp)) vmarrs))
      (remove_own_params (own_args pe pp) e)
      (mut_args (Syntax.ef_penv ef) pp) = Some e' ->
    exists tm',
      match_mem_env  tr n (B.fn_tenv f) e' tm' /\
      match_mem_cont tr n k tm' /\
      match_env tr n e' te /\
      valid_blocks tr n tm' /\
      Events.external_call (transl_external_function ef) (Genv.globalenv tp)
        (transl_valuelist tr n (combine (map Some pargs) vargs)) tm E0
        (transl_value tr n None r) tm'.

Lemma map2_Some {A B C: Type}:
  forall f (l1: list A) (l2: list B),
    (exists (l': list C), map2 f l1 l2 = Some l') <-> length l1 = length l2.
Proof.
  induction l1; destruct l2; simpl.
  - split; eauto.
  - split. now intros []. discriminate.
  - split. now intros []. discriminate.
  - split. case map2 eqn:H; try now intros [].
    now rewrite (proj1 (IHl1 _) (ex_intro (fun _ => _) l H)).
    intro H. apply eq_add_S in H. case (proj2 (IHl1 _) H) as [? ->].
    simpl. eauto.
Qed.

Lemma transl_valuelist_tr_call':
  forall tr n mut shr own params vargs pargs pargs',
    wf_trmem tr n ->
    List.incl (combine pargs params) (mut ++ shr ++ own) ->
    (forall (p q : sem_path) (i j : ident),
        In (p, i) (mut ++ shr ++ own) ->
        In (q, j) (mut ++ shr ++ own) -> (p, i) <> (q, j) -> i <> j) ->
    map2 (fun (i : ident) (v : value) => (Some (i, []), v))
         params vargs = Some pargs' ->
    length pargs = length vargs ->
    transl_valuelist (tr_call tr n mut shr own) (S n) pargs' =
    transl_valuelist tr n (combine (map Some pargs) vargs).
Proof.
  induction params; simpl; intros [|v vargs] [|p pargs]; try easy.
  - now intros * _ _ _ [= <-].
  - intros *. simpl. case map2 eqn:Hmap; try discriminate.
    intros WF INCL NRSND [= <-] Hlen. simpl. apply eq_add_S in Hlen.
    rewrite -> IHparams with (1 := WF) (3 := NRSND) (4 := Hmap) (5 := Hlen).
    2: { intros x Hin. apply INCL. now right. }
    assert (transl_value (tr_call tr n mut shr own) (S n) (Some (a, [])) v =
            transl_value tr n (Some p) v) as ->; [|easy].
    case v; try easy. intro lv. simpl.
    unfold tr_call. rewrite Nat.eqb_refl.
    specialize (INCL _ (in_eq _ _)).
    case find as [[[ic lc] i]|] eqn:Hfind.
    2: { pose proof (find_none _ _ Hfind _ INCL).
         simpl in H. now rewrite Pos.eqb_refl in H. }
    apply find_some in Hfind as [Hin Hi]. apply Pos.eqb_eq in Hi as ->.
    assert (p = (ic, lc)) as ->.
    { specialize (NRSND _ _ _ _ INCL Hin).
      case (eqSemPath p (ic, lc)). easy. intro H. elim NRSND; congruence. }
    rewrite app_nil_r. easy.
Qed.

Lemma get_env_path_list_length:
  forall e lp lv,
    get_env_path_list e lp = Some lv ->
    length lp = length lv.
Proof.
  induction lp; simpl. now intros _ [= <-].
  intro. case get_env_path; try easy. intro.
  destruct get_env_path_list; try easy. intros * [= <-].
  now rewrite (IHlp _ eq_refl).
Qed.

Lemma eval_path_list_length:
  forall e f synpl sempl,
    Bsem.eval_path_list e f synpl sempl ->
    length synpl = length sempl.
Proof. induction synpl; intros; inv H; simpl; [|rewrite (IHsynpl _ H4)]; easy. Qed.

Lemma get_env_path_list_nth_error:
  forall e lv lp i v,
    get_env_path_list e lp = Some lv ->
    nth_error lv i = Some v ->
    exists p, nth_error lp i = Some p /\ get_env_path e p = Some v.
Proof.
  induction lv; intros [] []; try easy; simpl;
    case get_env_path eqn:H; try easy;
    case get_env_path_list eqn:H'; try easy; simpl; intros * [= <- <-].
  - intros [= ->]. eauto.
  - intros Hnth. apply (IHlv _ _ _ H' Hnth).
Qed.

Lemma map2_map_combine {A A' B B' C: Type}:
  forall (fa: A -> A') (fb: B -> B') f (la: list A) (lb: list B) (l: list C),
    map2 (fun a b => f ((fa a), (fb b))) la lb = Some l ->
    l = map f (combine (map fa la) (map fb lb)).
Proof.
  intros fa fb f. induction la; intros []; try easy.
  - now intros * [= <-].
  - simpl. case map2 eqn:H; try easy. simpl. intros * [= <-].
    now rewrite -> IHla with (1 := H).
Qed.

Lemma match_mem_env_path:
  forall tr n te e tm,
    no_hidden tr n ->
    match_mem_env tr n te e tm ->
    forall p v t,
      get_env_path e p = Some v ->
      get_tenv_path te p = Some t ->
      match_mem_value tr n p tm v t.
Proof.
  intros * NOHID MME [i l] v t. simpl.
  case e!i eqn:He; try easy. case te!i eqn:Hte; try easy. simpl.
  specialize (MME _ _ _ He Hte) as MV. revert MV.
  rewrite <- (app_nil_l l) at 3.
  assert (get_value_path [] v0 = Some v0) by reflexivity.
  assert (get_type_path [] t0 = Some t0) by reflexivity.
  revert H H0. generalize ([] : list sem_path_elem) as l0.
  generalize t0 at 2 3 4. revert l. generalize v0 at 2 3 4.
  induction v1 using value_ind'.
  1-6: intros [|[]]; try easy; intros * _ _ H [= <-] [= <-]; now rewrite app_nil_r.
  rename l into lv. intros [|[idx] l].
  - intros * _ _ H [= <-] [= <-]. now rewrite app_nil_r.
  - intros * Hv0 Ht0 H. simpl. case nth_error eqn:Hnth; try easy.
    simpl. destruct t1; try easy. intro.
    change ((Pcell idx) :: l) with ([Pcell idx] ++ l). rewrite app_assoc.
    pose proof (nth_error_In _ _ Hnth) as Hin.
    apply (proj1 (Forall_forall _ _) IH _ Hin) with (4 := H0).
    apply get_value_path_app. exists (Varr lv). simpl. now rewrite Hnth.
    apply get_type_path_app. exists (Tarr t1). easy.
    unfold match_mem_value in H. fold match_mem_value in H.
    pose proof (match_mem_env_get_env_path _ _ _ _ _ MME (i, l0) lv t1).
    simpl in H1. rewrite He Hte in H1. simpl in H1. specialize (H1 Hv0 Ht0).
    case H1 as [[[b o] [|p]] Htr]. rewrite Htr in H.
    2: now specialize (NOHID (i, l0) b o p).
    assert (idx < length lv)%nat by (apply nth_error_Some; now rewrite Hnth).
    specialize (proj2 H idx H1) as (_ & _ & _ & MV').
    now rewrite (nth_error_nth _ _ _ Hnth) in MV'.
Qed.

Lemma pargs_params_no_aliasing_marrs_no_aliasing:
  forall m,
    Coqlib.list_norepet m ->
    marrs_no_aliasing m ->
    pargs_params_aliasing (map fst m) m = false.
Proof.
  intros m NOREPET H. case pargs_params_aliasing eqn:T; [|easy].
  apply pargs_params_aliasing_spec in T; [|easy].
  case T as (x & y & z & p1 & p2 & p & i & Hneq & N1 & N2 & N & A1 & A2).
  rewrite nth_error_map in N1. rewrite nth_error_map in N2.
  destruct (nth_error m x) as [[px jx]|] eqn:N1'; try discriminate.
  destruct (nth_error m y) as [[py jy]|] eqn:N2'; try discriminate.
  injection N1 as <-. injection N2 as <-.
  case (Nat.eqb_spec x z).
  - intros <-. rewrite N1' in N. injection N as <- <-.
    destruct px as [ix lx], py as [iy ly].
    simpl in A2. destruct (Pos.eqb_spec ix iy) as [<-|]; [|easy].
    simpl in A2. apply orb_prop in A2 as [A|A].
    + elim (H (ix, lx) jx (ix, ly) jy).
      simpl. now rewrite Pos.eqb_refl A.
      now apply nth_error_In in N1'. now apply nth_error_In in N2'.
      injection 1 as <- <-.
      eapply Hneq, Coqlib_norepet_nth_error_injective; eassumption.
    + elim (H (ix, ly) jy (ix, lx) jx).
      simpl. now rewrite Pos.eqb_refl A.
      now apply nth_error_In in N2'. now apply nth_error_In in N1'.
      injection 1 as <- <-.
      eapply Hneq, Coqlib_norepet_nth_error_injective; eassumption.
  - destruct px as [ix lx], p as [iz lz].
    simpl in A1. destruct (Pos.eqb_spec iz ix) as [->|]; [|easy].
    simpl in A1. intro Hneq'. apply orb_prop in A1 as [A|A].
    + elim (H (ix, lz) i (ix, lx) jx).
      simpl. now rewrite Pos.eqb_refl A.
      now apply nth_error_In in N. now apply nth_error_In in N1'.
      injection 1 as <- <-.
      eapply Hneq', Coqlib_norepet_nth_error_injective; eassumption.
    + elim (H (ix, lx) jx (ix, lz) i).
      simpl. now rewrite Pos.eqb_refl A.
      now apply nth_error_In in N1'. now apply nth_error_In in N.
      injection 1 as <- <-.
      eapply Hneq', Coqlib_norepet_nth_error_injective; eassumption.
Qed.

Lemma Coqlib_list_norepet_filter {A: Type}:
  forall f (l: list A),
    Coqlib.list_norepet l ->
    Coqlib.list_norepet (filter f l).
Proof.
  induction l. easy. intro H. inv H. simpl. case (f a).
  - apply Coqlib.list_norepet_cons; eauto.
    intro H. now apply filter_In in H as [].
  - eauto.
Qed.

Lemma transl_value_tr_return:
  forall tr N n p v,
    (n < N)%nat ->
    transl_value tr n (Some p) v =
    transl_value (tr_return tr N) n (Some p) v.
Proof.
  intros tr N n [i l] v Hn. case v; try easy. intro lv. simpl.
  unfold tr_return. rewrite -> (proj2 (Nat.eqb_neq _ _)) by lia.
  now rewrite -> (proj2 (Nat.ltb_lt _ _)) by lia.
Qed.

Lemma match_env_tr_return_lt_N:
  forall tr N n e te,
    (n < N)%nat ->
    match_env tr n e te ->
    match_env (tr_return tr N) n e te.
Proof. intros * ? ME i v. rewrite <- transl_value_tr_return. eauto. lia. Qed.

Lemma match_cont_tr_return:
  forall tr N ge k tk n f,
    (n <= N)%nat ->
    match_cont tr n ge (Some f) k tk ->
    match_cont (tr_return tr N) n ge (Some f) k tk.
Proof.
  induction k; simpl; intros; try easy; repeat destruct_match H0.
  - destruct H0; eauto.
  - eauto.
  - decompose [and] H0. repeat split; eauto.
    now apply match_env_tr_return_lt_N. apply IHk; [lia|easy].
Qed.

Lemma match_mem_env_lt_N_tr_return:
  forall tr N tm te e n,
    (n < N)%nat ->
    match_mem_env tr n te e tm ->
    match_mem_env (tr_return tr N) n te e tm.
Proof.
  intros * Hn MME i v t He Hte. specialize (MME i v t He Hte).
  apply match_mem_value_eq with (2 := MME).
  intro l. simpl. unfold tr_return.
  now rewrite -> (proj2 (Nat.eqb_neq _ _)), (proj2 (Nat.ltb_lt _ _)) by lia.
Qed.

Lemma match_mem_cont_tr_return:
  forall tr N tm k n,
    (n <= N)%nat ->
    match_mem_cont tr n k tm ->
    match_mem_cont (tr_return tr N) n k tm.
Proof.
  induction k; simpl; try easy. intros [|n] Hn. easy. case; split.
  - now apply match_mem_env_lt_N_tr_return.
  - apply IHk. lia. easy.
Qed.

Lemma marrs_varr_spec:
  forall ecallee e marrs,
    marrs_varr e ecallee marrs = true ->
    forall p i,
      In (p, i) marrs ->
      exists lv lv' : list value, get_env_path e p = Some (Varr lv)
                             /\ ecallee ! i = Some (Varr lv').
Proof.
  induction marrs. easy.
  case a as [p' i']. simpl. intro T. apply andb_prop in T as [T M].
  intros * [[= <- <-]|Hin].
  ** destruct ecallee!i', get_env_path; try easy.
     destruct v0, v; try easy. eauto. now destruct v.
  ** now apply IHmarrs.
Qed.

Lemma match_env_tr_return_N:
  forall tr N ecallee marrs e te e',
    wf_trmem tr (S N) ->
    hidden_marrs tr N marrs ->
    marrs_varr e ecallee marrs = true ->
    ~ pargs_params_aliasing (map fst marrs) marrs ->
    update_env ecallee e marrs = Some e' ->
    match_env tr N e te ->
    match_env (tr_return tr N) N e' te.
Proof.
  intros * WF HM MVARR NOALIAS Hupde ME.
  intros i v' He'.
  case (update_env_spec_3 _ _ _ _ _ _ He' Hupde) as [v He].
  pose proof (update_env_unchanged _ _ _ _ Hupde _ _ _ He He').
  change positive with ident in i.
  case (mem sub_path (i, []) (map fst marrs)) eqn:Hmem.
  + assert (In (i, []) (map fst marrs)) as Hin.
    { apply mem_spec in Hmem as [[i' l'] [Hin Hp]].
      simpl in Hp. destruct (Pos.eqb_spec i' i) as [->|Hneq]; try easy.
      now destruct l'. }
    apply in_map_fst_pair in Hin as [i' Hin].
    case (marrs_varr_spec _ _ _ MVARR _ _ Hin) as (lv & lv' & Hlv & Hlv').
    revert Hlv; simpl; rewrite He; intros [= ->].
    specialize (update_env_spec_2 _ _ _ _ NOALIAS Hupde _ _ _ Hin Hlv').
    revert He'; simpl; intros -> [= ->]. simpl.
    unfold tr_return. rewrite Nat.eqb_refl.
    specialize (ME _ _ He). simpl in ME.
    case tr as [[[] [|[i'' l'']]]|] eqn:Htr; try easy.
    now case (trmem_hid _ _ WF) with (1 := Nat.lt_succ_diag_r N) (2 := Htr)
      as [? ->].
  + destruct v; simpl in H; rewrite Hmem in H; simpl in H.
    all: try (rewrite <- H in *; exact (ME _ _ He)).
    destruct v'; try easy. simpl.
    unfold tr_return. rewrite Nat.eqb_refl. specialize (ME _ _ He). simpl in ME.
    case tr as [[[] [|[i' l']]]|] eqn:Htr; try easy.
    pose proof (ex_intro (fun _ => _) (b, i0, Hidden (i', l')) Htr) as DEF.
    specialize (proj1 (HM i [] i' l')) as T.
    change positive with ident in Htr. rewrite Htr in T.
    specialize (T eq_refl) as [? [Hin T]]. destruct x, l'; try discriminate.
    apply in_map_pair_fst in Hin. revert Hmem.
    assert (mem sub_path (i, []) (map fst marrs) = true) as ->; [|easy].
    { apply mem_spec. exists (i, []). split. easy. simpl. now rewrite Pos.eqb_refl. }
Qed.

Lemma eval_var_addr_deterministic:
  forall ge e i b1 b2,
    eval_var_addr ge e i b1 ->
    eval_var_addr ge e i b2 ->
    b1 = b2.
Proof. move=> ge e i b1 b2 H1 H2. inv H1; inv H2; congruence. Qed.

Lemma eval_expr_deterministic:
  forall ge e te m exp v1 v2,
    eval_expr ge e te m exp v1 ->
    eval_expr ge e te m exp v2 ->
    v1 = v2.
Proof.
  move=> ge e te m. elim/expr_ind.
  - move=>> H1 H2. inv H1. inv H2. congruence.
  - move=>> H1 H2. inv H1. inv H2.
    by rewrite (eval_var_addr_deterministic _ _ _ _ _ H0 H1).
  - move=>> H1 H2. inv H1. inv H2. congruence.
  - move=>> IH > H1 H2. inv H1. inv H2. have:= IH _ _ H3 H1. congruence.
  - move=>> IH1 > IH2 > H1 H2. inv H1. inv H2.
    have:= IH1 _ _ H4 H3. have:= IH2 _ _ H6 H8. congruence.
  - move=>> IH > H1 H2. inv H1. inv H2. have:= IH _ _ H3 H1. congruence.
Qed.

Lemma match_mem_value_assign_separated:
  forall tr n pe tm i' l' perm blk ofs ch x d tm' p v t,
    visible_separated tr n pe ->
    no_hidden tr n ->
    pe ! i' = Some perm ->
    Syntax.perm_le Syntax.Mutable perm ->
    tr n (i', l') = Some (blk, ofs, Visible) ->
    ~~ sem_path_prefix p (i', l') ->
    Mem.storev ch tm (Vptr blk (Ptrofs.add ofs d)) x = Some tm' ->
    match_mem_value tr n p tm v t ->
    match_mem_value tr n p tm' v t.
Proof.
  intros *. move=> VS NOHID Hpe Hperm Htr' + MEM.
  move: v t p. elim/value_ind' => [|b|k|k|f|f|lv IH] [] // t [i l] /=.
  move: Htr' Hpe IH. case: (Pos.eqb_spec i i') => [<-|Hneq].
  - case Hpref: prefix => //= +++ _.
    case Htr: (tr n (i, l)) => [[[blk' ofs'] []]|] //= Htr' Hpe IH.
    move=> [SZ H]; split.
    2: move=> idx Hidx; case: (H idx Hidx) => {H} REPR [VALID [LOAD MV]].
    2: split; [|split; [|split]] => //.
    + rewrite (Mem.load_store_other _ _ _ _ _ _ MEM) //.
      left. apply: not_eq_sym.
      apply: (proj2 (VS i l' blk ofs perm Htr' Hpe Hperm) _ _ _ _ _ Htr).
      move=> [Hl'].
      by rewrite Hl' (prefix_refl _ sem_path_elem_reflect) in Hpref.
      case: SZ. split=> //. by apply (Mem.store_valid_access_1 _ _ _ _ _ _ MEM).
    + by apply: (Mem.store_valid_access_1 _ _ _ _ _ _ MEM).
    + rewrite (Mem.load_store_other _ _ _ _ _ _ MEM) //.
      left. apply: not_eq_sym.
      apply: (proj2 (VS i l' blk ofs perm Htr' Hpe Hperm) _ _ _ _ _ Htr).
      move=> [Hl'].
      by rewrite Hl' (prefix_refl _ sem_path_elem_reflect) in Hpref.
    + move/nth_error_Some in Hidx. move: Hidx.
      case Hnth: (nth_error lv idx) => [y|] // _.
      move: MV. rewrite (nth_error_nth _ _ _ Hnth).
      move/(nth_error_In lv) in Hnth.
      apply (proj1 (Forall_forall _ _) IH) with (1 := Hnth) => /=.
      rewrite Pos.eqb_refl. case Hpref1': prefix => //.
      move: Hpref1'. have:= prefix_app' _ sem_path_elem_reflect l [Pcell idx].
      move/prefix_transl/[apply] => /(_ sem_path_elem_reflect). by rewrite Hpref.
  - move=> Htr' Hpe IH _. case Htr: (tr n (i, l)) => [[[blk' ofs'] []]|] //.
    move=> [SZ H]; split.
    2: move=> idx Hidx; case: (H idx Hidx) => {H} REPR [VALID [LOAD MV]].
    2: split; [|split; [|split]] => //.
    + rewrite (Mem.load_store_other _ _ _ _ _ _ MEM) //.
      left. apply: not_eq_sym.
      apply: (proj2 (VS i' l' blk ofs perm Htr' Hpe Hperm) _ _ _ _ _ Htr).
      congruence.
      case: SZ. split=> //. by apply (Mem.store_valid_access_1 _ _ _ _ _ _ MEM).
    + by apply: (Mem.store_valid_access_1 _ _ _ _ _ _ MEM).
    + rewrite (Mem.load_store_other _ _ _ _ _ _ MEM) //.
      left. apply: not_eq_sym.
      apply: (proj2 (VS i' l' blk ofs perm Htr' Hpe Hperm) _ _ _ _ _ Htr).
      congruence.
    + move/nth_error_Some in Hidx. move: Hidx.
      case Hnth: (nth_error lv idx) => [y|] // _.
      move: MV. rewrite (nth_error_nth _ _ _ Hnth).
      move/(nth_error_In lv) in Hnth.
      apply (proj1 (Forall_forall _ _) IH) with (1 := Hnth) => /=.
      by rewrite (proj2 (Pos.eqb_neq _ _)).
Qed.

Lemma update_value_Pcell:
  forall v0 v v0' l idx l',
    update_value v0 v (l ++ [Pcell idx] ++ l') = Some v0' ->
    exists lv lv', get_value_path l v0 = Some (Varr lv)
              /\ get_value_path l v0' = Some (Varr lv').
Proof.
  elim/value_ind' => [|b|n|n|f|f|lv IH] => v v0' [|[idx'] l] idx l' //=.
  - case Hnth: nth_error => [v1|] //=.
    case UPDV: update_value => [v1'|] //=.
    case Hrep: replace_nth => [lv'|] //=. eauto.
  - case Hnth: nth_error => [v1|] //=.
    case UPDV: update_value => [v1'|] //=.
    case Hrep: replace_nth => [lv'|] //= [<-].
    rewrite (replace_nth_nth_error_same _ _ _ _ Hrep) /=.
    apply: (proj1 (Forall_forall _ _) IH).
    + exact: (nth_error_In _ _ Hnth).
    + exact: UPDV.
Qed.

Lemma update_value_not_primitive:
  forall v0 v v0' l,
    l <> [] ->
    update_value v0 v l = Some v0' ->
    primitive_value v0 = false /\ primitive_value v0' = false.
Proof.
  case=> [|b|n|n|f|f|lv] ?? [//|[idx] l] //=.
  case: nth_error => //= >. case: update_value => //= >.
  case: replace_nth => //= > _ [<-] //.
Qed.

Lemma match_mem_env_assign_non_empty_path:
  forall tr n hfuncs Habort Hcalloc p tp f tf e te tm,
  forall i synpl syne sempl seme llsz lsz,
  forall ep ch ep' vidx,
  forall v0 v1 v t0 t perm,
  forall e' tm',
    transl_program' hfuncs Habort Hcalloc p = OK tp ->
    match_env tr n e te ->
    match_mem_env tr n (B.fn_tenv f) e tm ->
    transl_function (genv_of_program p) f = OK tf ->
    visible_separated tr n (B.fn_penv f) ->
    no_hidden tr n ->
    (B.fn_penv f) ! i = Some perm ->
    Syntax.perm_le Syntax.Mutable perm ->
    (B.fn_tenv f)!i = Some t0 ->
    (B.fn_szenv' f)!i = Some (llsz ++ [lsz]) ->
    e!i = Some v0 ->
    get_type_path (sempl ++ [seme]) t0 = Some t ->
    get_value_path sempl v0 = Some v1 ->
    Bsem.eval_path_elem_list e f synpl (llsz ++ [lsz]) sempl ->
    Bsem.eval_path_elem_list e f [syne] [lsz] [seme] ->
    transl_syn_path_elem_list (B.fn_tenv f) (B.fn_szenv' f) t0 (llsz ++ [lsz]) synpl (Evar i) = OK ep ->
    transl_syn_path_elem (B.fn_tenv f) (B.fn_szenv' f) lsz syne t ep = OK (Eload ch ep') ->
    eval_expr (Genv.globalenv tp) PTree.Empty
      te tm ep' vidx ->
    primitive_value v = true ->
    well_typed_value t v ->
    update_env_path e (i, sempl ++ [seme]) (shrink t v) = Some e' ->
    Mem.storev (typ_to_memory_chunk t) tm vidx (transl_value tr n None v) = Some tm' ->
    match_mem_env tr n (B.fn_tenv f) e' tm'.
Proof.
  intros * => /[dup] TPROG + ME /[dup] MME + /[dup] TF + VS /[dup] NOHID.
  move/transl_syn_path_sem_preservation/[apply]/[apply]/[apply].
  move/(_ te (i, synpl) (i, sempl)) => /=.
  move=> + Hpe Hperm Hte Hsze He Ht0 Hv0 EVpl EVpe TRpl TRpe.
  rewrite Hte Hsze /= TRpl He /= Hv0.
  have H: (Bsem.eval_path e f (i, synpl) (i, sempl)).
  { apply: Bsem.eval_path_intro. exact: Hsze. exact: EVpl. }
  move/(_ _ _ ME eq_refl H eq_refl) => {H} EVep EVep' PRIM WT.
  case UPDV: update_value => [v0'|] //= [<-] Hm.
  have MV:= MME _ _ _ He Hte => j v' t' He' Hte'.
  move: EVpe Ht0 UPDV. case: seme => idx EVpe Ht0 UPDV.
  inv EVpe.
  move: TRpe. rewrite transl_Scell. case TRlidx: transl_exprlist => //= [tlidx].
  case Bidx: build_index_expr => //= [tidx]. case=> Hch.
  move: EVep' => /[swap] <- EVeidx.
  inv EVeidx. move: H11.
  rewrite (eval_expr_deterministic _ _ _ _ _ _ _ H2 EVep) => {H2} T1.
  have EVidx: eval_expr (Genv.globalenv tp) PTree.Empty te tm tidx
                (Vlong (Int64.repr (Z.of_nat idx))).
  { apply: eval_build_index_expr; try eassumption.
    generalize lidx. elim/list_ind => //= idx' lidx' IH.
    move=>> [<-|Hin]; eauto. move=> Tidx' Eidx'.
    apply: (transl_expr_sem_preservation tr n p hfuncs Habort Hcalloc tp);
      eassumption. }
  inv H4. inv H2. move: H12 T1.
  rewrite (eval_expr_deterministic _ _ _ _ _ _ _ H11 EVidx) => {H10}.
  case: H0 => <- [<-] []. change Archi.ptr64 with true => /=.
  move: Hm => /[swap] <- /=.
  case: (update_value_Pcell _ _ _ _ _ _ UPDV) => lv [_ [+ _]].
  rewrite Hv0 => [[H]]. move: EVep Hv0; rewrite H => /= {H}.
  case Htr: (tr n (i, sempl)) => [[[blk ofs] [|p']]|] //=.
  2: by have:= NOHID (i, sempl) blk ofs p'.
  change Archi.ptr64 with true => /= EVep Hv0 Hm {v2 v3 v4 v5 H11}.
  case (Pos.eqb_spec i j) as [<-|Hneq].
  - move: Hte' He'. rewrite Hte PTree.gss => - [<-] [<-] {v' t'}.
    have U:= update_value_unchanged [(i, sempl ++ [Pcell idx], 1%positive)] _ _ _ i [] _
                                    (in_eq _ _) UPDV.
    move: Hv0 Ht0 Htr EVep UPDV MV U {TRpl EVpl}.
    rewrite -{3 5}(app_nil_l sempl).
    move: {2 4 5 6 8}[]. move: {1 2 3}t0 v0' {1 2 3 4}v0 sempl.
    elim/typ_ind => [||sz sg|sg|fsz|telt]; try by case.
    move=> IH x' x sempl' sempl0.
    case: sempl' => [|seme' sempl'].
    + move=> [->] [H] ++++ U => /= Htr EVep.
      rewrite -H in Hm WT. rewrite -H => {H}.
      case Hnth: nth_error => //= [y]. case Hrep: replace_nth => //= [y'].
      move=> [H]. rewrite -H. rewrite -H in U => {H}. rewrite app_nil_r in U.
      rewrite app_nil_r in Htr. rewrite Htr.
      case=> - [LSZ VSZ] Hlv. move: Hrep.
      move/[dup]/(replace_nth_length lv) => Hlen. rewrite -Hlen => Hrep.
      have Hidx: (idx < length lv)%nat.
      { apply: (proj1 (replace_nth_Some lv idx _)). by rewrite -> Hrep. }
      split.
      { rewrite (Mem.load_store_other _ _ _ _ _ _ Hm) //.
        right. left. set z := Ptrofs.add _ _.
        have:= Ptrofs.unsigned_range z; lia.
        split=> //. by apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hm). }
      move=> j /Hlv => - [REPR [VALID [LOAD MV]]]. have [REPR' _] := Hlv idx Hidx.
      move: Hrep REPR VALID LOAD MV. case: (Nat.eqb_spec idx j) => [<-|Hneq].
      * move=> /(replace_nth_nth_error_same lv) => Hnth'.
        rewrite (nth_error_nth _ _ _ Hnth').
        have H: ~~ mem sub_path (i, sempl0)
                  (map fst [(i, sempl0 ++ [Pcell idx], 1%positive)]).
        { move=> /=. rewrite Pos.eqb_refl /=.
          move: {1 2}sempl0. elim/list_ind => //= > IH'.
          by case: sem_path_elem_reflect. }
        have:= proj2 (unchanged_Varr _ _ _ _ _ H U) _ _ _ Hnth Hnth' => U'.
        split; [|split; [|split]] => //.
        { by apply: (Mem.store_valid_access_1 _ _ _ _ _ _ Hm). }
        { erewrite <- Ptrofs.agree64_of_int_eq with (a := Ptrofs.mul _ _).
          2: apply: Ptrofs.agree64_mul => //; by apply: Ptrofs.agree64_repr.
          rewrite -> (Mem.load_store_same _ _ _ _ _ _ Hm). apply: f_equal.
          rewrite load_result_shrink => //.
          apply: transl_value_primitive. by apply: primitive_value_shrink. }
        { move: PRIM. case v, telt => //. case i1, s => //. }
      * move=> /(replace_nth_nth_other lv) => /(_ _ Vundef Hneq) ->.
        split; [|split; [|split]] => //.
        { by apply: (Mem.store_valid_access_1 _ _ _ _ _ _ Hm). }
        { rewrite (Mem.load_store_other _ _ _ _ _ _ Hm) //.
          right. erewrite Ptrofs.agree64_of_int_eq with (b := Int64.mul _ _).
          2: apply: Ptrofs.agree64_mul => //; by apply: Ptrofs.agree64_repr.
          rewrite/Ptrofs.add/Ptrofs.mul.
          have M: (Ptrofs.max_unsigned = 18446744073709551615) by reflexivity.
          have P:= Ptrofs.unsigned_range ofs.
          have S:= typ_sizeof_bounds telt.
          rewrite (Ptrofs.unsigned_repr (typ_sizeof _)); [lia|].
          rewrite (Ptrofs.unsigned_repr (Z.of_nat j)); [lia|].
          rewrite (Ptrofs.unsigned_repr (Z.of_nat idx)); [lia|].
          rewrite -> 2!(Ptrofs.unsigned_repr (_ * _)) by lia.
          rewrite -> 2!(Ptrofs.unsigned_repr (_ + _)) by lia.
          rewrite size_chunk_typ_sizeof.
          case (Nat.ltb_spec j idx). left; nia. right; nia. }
        apply: match_mem_value_assign_separated => /=; try eassumption.
        rewrite Pos.eqb_refl /=.
        apply: (prefix_not_comm _ sem_path_elem_reflect).
        by apply: (prefix_app' _ sem_path_elem_reflect).
        move=> H. have:= eq_refl (length sempl0). rewrite {1}H app_length /=. lia.
    + case: seme' => idx'. case: x => // lv' ++++ /[dup] UPDV ++ U => /=.
      case Hnth: nth_error => [v2|] //=. case UPDV': update_value => [v2'|] //=.
      case Hrep: replace_nth => //= ++++ H. move: U UPDV. case: H => <- U UPDV.
      rewrite -(replace_nth_length _ _ _ _ Hrep).
      case Htr': (tr n (i, sempl0)) => [[[blk' ofs'] [|p']]|] //=.
      2: by have:= NOHID (i, sempl0) blk' ofs' p'.
      move=> Hv1 Ht Htr EVep [[LSZ VSZ] Hlv]. split.
      { rewrite (Mem.load_store_other _ _ _ _ _ _ Hm) //.
        left. apply not_eq_sym.
        apply: (proj2 (VS _ _ _ _ _ Htr Hpe Hperm) _ _ _ _ _ Htr').
        case. rewrite -{2}(app_nil_r sempl0). by move/app_inv_head.
        split=> //. by apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hm). }
      move=> j Hj. move: (Hlv j Hj). case=> REPR [VALID [LOAD MV]].
      split; [|split; [|split]] => //.
      { by apply: (Mem.store_valid_access_1 _ _ _ _ _ _ Hm). }
      { move: LOAD. case: (Nat.eqb_spec idx' j) => [<-|Hneq].
        - rewrite (nth_error_nth _ _ _ Hnth) (replace_nth_nth_same _ _ _ _ _ Hrep).
          have: sempl' ++ [Pcell idx] <> [].
          { move=> H. have:= eq_refl (length (sempl' ++ [Pcell idx])).
            rewrite {1}H app_length /=. lia. }
          move/update_value_not_primitive => /(_ _ _ _ UPDV') [].
          move {UPDV' Hrep Hnth Hv1}. case: v2' => // >. case: v2 => // > _ _ LOAD.
          rewrite (Mem.load_store_other _ _ _ _ _ _ Hm) => //.
          left. apply: not_eq_sym.
          apply: (proj2 (VS _ _ _ _ _ Htr Hpe Hperm) _ _ _ _ _ Htr').
          case=> H. have:= eq_refl (length sempl0).
          rewrite -{1}H app_length /=. lia.
        - rewrite (replace_nth_nth_other _ _ _ _ _ _ Hrep Hneq).
          rewrite (Mem.load_store_other _ _ _ _ _ _ Hm) => //.
          left. apply: not_eq_sym.
          apply: (proj2 (VS _ _ _ _ _ Htr Hpe Hperm) _ _ _ _ _ Htr').
          case=> H. have:= eq_refl (length sempl0).
          rewrite -{1}H app_length /=. lia. }
      move: MV. case: (Nat.eqb_spec idx' j) => [<-|Hneq] MV.
      * change (Pcell idx' :: sempl') with ([Pcell idx'] ++ sempl') in Htr.
        rewrite app_assoc in Htr. rewrite (nth_error_nth _ _ _ Hnth) in MV.
        rewrite (replace_nth_nth_same _ _ _ _ _ Hrep).
        apply: (IH _ _ _ _ Hv1 Ht Htr EVep UPDV' MV).
        have Hsub: ~~ sub_path (i, sempl0)
                     (i, (sempl0 ++ Pcell idx' :: sempl') ++ [Pcell idx]).
        { move=> /=. rewrite Pos.eqb_refl /=.
          apply: (prefix_not_comm _ sem_path_elem_reflect).
          rewrite -app_assoc. apply: (prefix_app' _ sem_path_elem_reflect).
          move=> H. have:= eq_refl (length sempl0).
          rewrite {1}H 2!app_length /=. lia. }
        have:= unchanged_Varr _ _ _ _ _ _ U => /=. rewrite orb_false_r.
        move=> /(_ Hsub) [_] /(_ _ _ _ Hnth).
        rewrite (replace_nth_nth_error_same _ _ _ _ Hrep) => /(_ _ eq_refl).
        by rewrite -(app_assoc sempl0 [Pcell idx'] _).
      * rewrite (replace_nth_nth_other _ _ _ _ _ _ Hrep) => //.
        apply: match_mem_value_assign_separated; try eassumption.
        move: {1 2}sempl0. elim/list_ind => //=.
        ** rewrite Pos.eqb_refl /=.
           case H: internal_nat_beq => //.
           have:= internal_nat_dec_bl _ _ H. congruence.
        ** rewrite Pos.eqb_refl /= => a l.
           by case: sem_path_elem_reflect.
  - rewrite PTree.gso in He'; [lia|].
    have MV':= MME _ _ _ He' Hte'.
    apply: match_mem_value_assign_separated; try eassumption.
    move=> /=. rewrite (proj2 (Pos.eqb_neq _ _)) => //. lia.
Qed.

Lemma list_cases_rev {A: Type}:
  forall (l: list A),
    l = [] \/ exists x l', l = l' ++ [x].
Proof.
  elim/list_ind => /=. by left.
  move=> a l [->|[x [l' H]]]; right.
  by exists a, []. exists x, (a :: l'). rewrite H. by apply: app_comm_cons.
Qed.

Lemma eval_path_elem_list_app_llsz:
  forall e f synpl llsz1 llsz2 sempl,
    Bsem.eval_path_elem_list e f synpl llsz1 sempl ->
    Bsem.eval_path_elem_list e f synpl (llsz1 ++ llsz2) sempl.
Proof.
  induction synpl; intros * T; inv T.
  exact (Bsem.eval_path_elem_list_Nil e f _).
  econstructor; try eassumption. now apply IHsynpl.
Qed.

Lemma transl_syn_path_elem_list_app_llsz_useless:
  forall te sze synpl t llsz1 llsz2 e e',
    transl_syn_path_elem_list te sze t llsz1 synpl e = OK e' ->
    transl_syn_path_elem_list te sze t (llsz1 ++ llsz2) synpl e = OK e'.
Proof.
  intros te sze. induction synpl. easy.
  intros *. simpl. case t; try easy. intro t'.
  case llsz1; try easy. intros lsz llsz'. rewrite <- app_comm_cons.
  case transl_syn_path_elem; try discriminate. simpl. intro.
  apply IHsynpl.
Qed.

Lemma get_type_syn_path_eval_path_elem_list:
  forall e f synpl llsz sempl t,
    Bsem.eval_path_elem_list e f synpl llsz sempl ->
    B.get_type_syn_path synpl t = get_type_path sempl t.
Proof.
  move=> e f. elim/list_ind => [|[idx] synpl IH] llsz sempl t EV.
  - by inv EV.
  - inv EV => /=. case: t => // >. apply: IH; eassumption.
Qed.

Lemma get_tenv_syn_path_eval_path:
  forall e f synp semp,
    Bsem.eval_path e f synp semp ->
    B.get_tenv_syn_path (B.fn_tenv f) synp = get_tenv_path (B.fn_tenv f) semp.
Proof.
  move=> e f [i synpl] semp EV. inv EV => /=. case: ((B.fn_tenv f)!i) => //= {H1}.
  move=> t. apply: get_type_syn_path_eval_path_elem_list. exact: H3.
Qed.

Lemma match_mem_value_lt_N_assign:
  forall tr N n pe tm i l perm blk ofs ch d v' tm' v t p,
    (n < N)%nat ->
    visible_separated tr N pe ->
    pe!i = Some perm ->
    B.perm_le B.Mutable perm ->
    tr N (i, l) = Some (blk, ofs, Visible) ->
    Mem.storev ch tm (Vptr blk (Ptrofs.add ofs d)) v' = Some tm' ->
    match_mem_value tr n p tm v t ->
    match_mem_value tr n p tm' v t.
Proof.
  intros *. move=> Hn VS Hpe Hperm Htr Hm.
  have VS' := proj1 (VS i l blk ofs perm Htr Hpe Hperm).
  move: v t p. elim/value_ind' => [|b|k|k|f|f|lv IH] [] //= t [i' l'].
  case Htr': (tr n (i', l')) => [[[blk' ofs'] []]|] //= [[LSZ VSZ] H]. split.
  2: move=> idx Hidx; case: (H idx Hidx) => REPR [VALID [LOAD MV]] {H}.
  2: split; [|split; [|split]] => //.
  - rewrite (Mem.load_store_other _ _ _ _ _ _ Hm) //.
    left. apply: not_eq_sym. by apply: (VS' _ _ _ _ Hn Htr').
    split=> //. by apply (Mem.store_valid_access_1 _ _ _ _ _ _ Hm).
  - by apply: (Mem.store_valid_access_1 _ _ _ _ _ _ Hm).
  - rewrite (Mem.load_store_other _ _ _ _ _ _ Hm) => //.
    left. apply: not_eq_sym. by apply: (VS' _ _ _ _ Hn Htr').
  - move: Hidx => /nth_error_Some. case Hnth: nth_error => // _.
    move: MV. rewrite (nth_error_nth _ _ _ Hnth).
    move: Hnth => /(nth_error_In lv) Hin.
    by apply: (proj1 (Forall_forall _ _) IH).
Qed.

Lemma match_mem_env_lt_N_assign:
  forall tr N n pe te e tm i l perm blk ofs ch d v tm',
    (n < N)%nat ->
    visible_separated tr N pe ->
    pe!i = Some perm ->
    B.perm_le B.Mutable perm ->
    tr N (i, l) = Some (blk, ofs, Visible) ->
    Mem.storev ch tm (Vptr blk (Ptrofs.add ofs d)) v = Some tm' ->
    match_mem_env tr n te e tm ->
    match_mem_env tr n te e tm'.
Proof.
  intros *. move=> Hn VS Hpe Hperm Htr Hm MME.
  move=>> He Hte. have:= MME _ _ _ He Hte.
  apply: match_mem_value_lt_N_assign; eassumption.
Qed.

Lemma match_mem_cont_lt_N_assign:
  forall tr N pe tm i l perm blk ofs ch d v tm' k n,
    (n <= N)%nat ->
    visible_separated tr N pe ->
    pe!i = Some perm ->
    B.perm_le B.Mutable perm ->
    tr N (i, l) = Some (blk, ofs, Visible) ->
    Mem.storev ch tm (Vptr blk (Ptrofs.add ofs d)) v = Some tm' ->
    match_mem_cont tr n k tm ->
    match_mem_cont tr n k tm'.
Proof.
  intros *. move=> Hn VS Hpe Hperm Htr Hm. move: k n Hn.
  elim/Bsem.cont_ind => // > IH [|n] //= Hn [MME MMK]. apply: conj.
  - apply: match_mem_env_lt_N_assign; try eassumption.
  - apply: IH; try eassumption. lia.
Qed.

Lemma eval_path_elem_list_full_cut_on_last e f:
  forall synpl syne llsz sempl,
    Datatypes.length (synpl ++ [syne]) = Datatypes.length llsz ->
    Bsem.eval_path_elem_list e f (synpl ++ [syne]) llsz sempl ->
    exists sempl' llsz' seme lsz,
      sempl = sempl' ++ [seme] /\
      llsz = llsz' ++ [lsz] /\
      Bsem.eval_path_elem_list e f synpl llsz' sempl' /\
      Bsem.eval_path_elem_list e f [syne] [lsz] [seme].
Proof.
  elim/list_ind => /=.
  - move=> ? [] // ? [] // > _ T. inv T. inv H9.
    exists [], [] => /=. eauto 8 with semantics.
  - move=>> IH ? [] //= > /eq_add_S LEN T. inv T.
    case (IH _ _ _ LEN H9) => [> [> [> [> [-> [-> [E1 E2]]]]]]].
    rewrite 2!app_comm_cons. eauto 10 with semantics.
Qed.

Lemma size_vars_assignments_correct p hfuncs Habort Hcalloc tp tr n tk f tf tm:
  transl_program' hfuncs Habort Hcalloc p = OK tp ->
  transl_function (genv_of_program p) f = OK tf ->
  no_hidden tr n ->
  forall s s0 e te e',
    (forall i, ~ In i (B.fn_params f) -> e!i = None) ->
    match_env tr n e te ->
    match_mem_env tr n (B.fn_tenv f) e tm ->
    instantiate_size_vars e f = OK e' ->
    size_vars_assignments f = OK s ->
    incl' e e' /\
    exists te',
      plus step (Genv.globalenv tp)
      (State tf (Sseq s s0) tk PTree.Empty te tm) E0
      (State tf s0 tk PTree.Empty te' tm) /\
      match_env tr n e' te' /\
      match_mem_env tr n (B.fn_tenv f) e' tm.
Proof.
  move=> TPROG TFUNC NOHID s s0 e0 te e' UNDEF ME MME ISZ ASZ.
  have: incl' e0 e0 by easy. move: ME MME ISZ ASZ.
  have:= B.fn_szenv'_params f. rewrite/B.valid_szenvi_params.
  rewrite/instantiate_size_vars/size_vars_assignments.
  elim/list_ind: {2 3}(B.fn_params f) s s0 {1 2 4 6}e0 te e' => /=.
  - move=>> ??? [<-] [<-]. split=> //. eexists. split.
    eapply plus_left'. apply step_seq. eapply plus_one. apply step_skip_seq.
    reflexivity. eauto.
  - move=> i params IH s s0 e te e' SZP ME MME.
    case (B.fn_szenv f)!i => [llsz|] //=.
    case Hsze': (B.fn_szenv' f)!i => [llsz'|] //=.
    case X: (fold_left2_res _ e llsz llsz') => [e1|] //= X'.
    case U': (fold_right_res _ params Sskip) => [s1|] //= U INCL.
    have: incl' e0 e1 /\ exists te',
      plus step (Genv.globalenv tp)
        (State tf (Sseq s s0) tk PTree.Empty te tm) E0
        (State tf s1 (Kseq s0 tk) PTree.Empty te' tm) /\
        match_env tr n e1 te' /\ match_mem_env tr n (B.fn_tenv f) e1 tm.
    { move: ME MME INCL X U {IH X' U'}. have:= SZP _ _ Hsze' => {Hsze' SZP}.
      elim/list_ind: llsz llsz' s s0 e te e1 => /=.
      - case=>> // ???? [<-] [<-]. split=> //. eexists. split.
        apply plus_one, step_seq. eauto.
      - move=> lsz llsz IH [//|lsz' llsz'] s s0 e te e1 SZP ME MME INCL.
        case X: (fold_left2_res _ e lsz lsz') => [e2|] //= X'.
        case U': (fold_right2_res _ llsz llsz' s1) => [s2|] //= U.
        have: incl' e0 e2 /\ exists te',
          plus step (Genv.globalenv tp)
            (State tf (Sseq s s0) tk PTree.Empty te tm) E0
            (State tf s2 (Kseq s0 tk) PTree.Empty te' tm) /\
            match_env tr n e2 te' /\ match_mem_env tr n (B.fn_tenv f) e2 tm.
        { inv SZP. move: H1 ME MME INCL X U {IH X' U'}.
          elim/list_ind: lsz lsz' s s0 e te e2 => /=.
          - case=>> // ???? [<-] [<-]. split=> //. eexists. split.
            apply plus_one, step_seq. eauto.
          - move=> sz lsz IH [//|sz' lsz'] s s0 e te e2 SZP ME MME INCL.
            inv SZP.
            case E: ExprSem.eval_expr => [[| | |x| | |]| |] //= X'.
            case U': (fold_right2_res _ lsz lsz' s2) => [s3|] //=.
            case T: transl_expr => [tsz|] //= [<-].
            have ME1  := match_env_set _ _ _ _  sz' (Vint64 x) ME.
            have MME1 := match_mem_env_set _ _ _ _ _ sz' (Vint64 x) MME eq_refl.
            have INCL1: incl' e0 (PTree.set sz' (Vint64 x) e).
            { move=> j x' H. have NEQ: j <> sz'.
              { move=> X. have:= UNDEF _ H1. by rewrite -X H. }
              rewrite PTree.gso //. by apply INCL. }
            have [INCL']:= IH lsz' _ s0 _ (PTree.set sz' (Vlong x) te) _
                      H3 ME1 MME1 INCL1 X' U'.
            case=> te' [STEP [ME' MME']] {ME1 MME1}.
            split=> //. exists te'. split; [|by split].
            eapply plus_left' with (t1 := E0) (t2 := E0) => //. apply step_seq.
            eapply plus_left' with (t1 := E0) (t2 := E0) => //. apply step_seq.
            eapply plus_left' with (t1 := E0) (t2 := E0) => //. apply step_set.
            have E' := Bfacts.eval_expr_fixpoint_match_Some _ _ _ _ E.
            eapply transl_expr_sem_preservation; try eassumption.
            apply Bfacts.eval_expr_incl with (1 := INCL) (2 := E').
            inv STEP. inv H.
            eapply plus_left with (t1 := E0) (t2 := E0) => //. apply step_skip_seq.
            case: t2 H4 H0 => //. }
        case=> INCL2 [te2 [STEP1 [ME2 MME2]]]. inv SZP.
        have [INCL' [te' [STEP2 [ME' MME']]]]:= IH _ _ s0 _ _ _ H2 ME2 MME2 INCL2 X' U'.
        split=> //. exists te'. split; [|by split]. inv STEP2. inv H.
        eapply plus_star_trans; eassumption. }
    case=> INCL1 [te1 [STEP [ME1 MME1]]].
    have [INCL' [te' [STEP2 [ME' MME']]]]:= IH _ s0 _ _ _ SZP ME1 MME1 X' U' INCL1.
    split=> //. exists te'. split; [|by split]. inv STEP2. inv H.
    eapply plus_star_trans; eassumption.
Qed.

Lemma valid_access_unchanged_on P:
  forall m m' ch b ofs p,
    Mem.unchanged_on P m m' ->
    (forall i : Z, ofs <= i < ofs + size_chunk ch -> P b i) ->
    Mem.valid_access m  ch b ofs p ->
    Mem.valid_access m' ch b ofs p.
Proof.
  intros * I HP H. have X := Mem.valid_access_perm _ _ _ _ _ _ H.
  have:= Mem.valid_access_implies _ _ _ _ _ _ H (perm_any_N _).
  move/Mem.valid_access_valid_block => V.
  move: H. rewrite/Mem.valid_access/Mem.range_perm => - [H A]. split=> //.
  move=>> C. have:= H _ C. have:= HP _ C.
  have:= Mem.unchanged_on_perm _ _ _ I _ _ _ _ _ V; by do 2 move/[apply].
Qed.

Lemma match_mem_value_unchanged_on tr n m m':
  forall t v p,
    Mem.unchanged_on (fun b _ => Mem.valid_block m b) m m' ->
    match_mem_value tr n p m v t ->
    match_mem_value tr n p m' v t.
Proof.
  elim/typ_ind => // t IH [] // lv [i l] /= I.
  case (tr n (i, l)) => [[[b o] []]|] // [[LSZ /[dup] + VSZ] H].
  move/Mem.valid_access_implies => /(_ _ (perm_any_N _)).
  move/Mem.valid_access_valid_block => Vb. split.
  2: move=> idx => /H {H}; case=> [Hidx [Hacc [Hload H]]].
  2: split; [|split; [|split]] => //.
  - split. exact: Mem.load_unchanged_on I (fun _ _ => Vb) LSZ.
    case: VSZ => P ?. move=>>; split=> //.
    move=> ofs /P. by apply (Mem.unchanged_on_perm _ _ _ I).
  - exact: valid_access_unchanged_on I (fun _ _ => Vb) Hacc.
  - exact: Mem.load_unchanged_on I (fun _ _ => Vb) Hload.
  - exact: IH I H.
Qed.

Lemma match_mem_env_unchanged_on tr n m m' te e:
  Mem.unchanged_on (fun b _ => Mem.valid_block m b) m m' ->
  match_mem_env tr n te e m ->
  match_mem_env tr n te e m'.
Proof. move=> + H i v t /H/[apply]. exact: match_mem_value_unchanged_on. Qed.

Lemma match_mem_cont_unchanged_on tr m m':
  forall k n,
    Mem.unchanged_on (fun b _ => Mem.valid_block m b) m m' ->
    match_mem_cont tr n k m ->
    match_mem_cont tr n k m'.
Proof.
  elim/Bsem.cont_ind => // i e f mut k IH [|n] // I /=. case; split.
  - eapply match_mem_env_unchanged_on; eassumption.
  - eapply IH; eassumption.
Qed.

Lemma plus_nine:
  forall ge s1 t1 s2 t2 s3 t3 s4 t4 s5 t5 s6 t6 s7 t7 s8 t8 s9 t9 s10 t,
    step ge s1 t1 s2 -> step ge s2 t2 s3 ->
    step ge s3 t3 s4 -> step ge s4 t4 s5 ->
    step ge s5 t5 s6 -> step ge s6 t6 s7 ->
    step ge s7 t7 s8 -> step ge s8 t8 s9 ->
    step ge s9 t9 s10 ->
    t = t1 ** t2 ** t3 ** t4 ** t5 ** t6 ** t7 ** t8 ** t9 ->
    plus step ge s1 t s10.
Proof.
  do 5 (intros; eapply plus_left'; eauto). eapply plus_four; eauto.
Qed.

Lemma plus_twelve:
  forall ge s1 t1 s2 t2 s3 t3 s4 t4 s5 t5 s6 t6 s7 t7 s8 t8
      s9 t9 s10 t10 s11 t11 s12 t12 s13 t,
    step ge s1 t1 s2 -> step ge s2 t2 s3 ->
    step ge s3 t3 s4 -> step ge s4 t4 s5 ->
    step ge s5 t5 s6 -> step ge s6 t6 s7 ->
    step ge s7 t7 s8 -> step ge s8 t8 s9 ->
    step ge s9 t9 s10 -> step ge s10 t10 s11 ->
    step ge s11 t11 s12 -> step ge s12 t12 s13 ->
    t = t1 ** t2 ** t3 ** t4 ** t5 ** t6 ** t7 ** t8 ** t9 ** t10 ** t11 ** t12 ->
    plus step ge s1 t s13.
Proof.
  do 8 (intros; eapply plus_left'; eauto). eapply plus_four; eauto.
Qed.

Lemma eval_expr_wf_size_expr'_assign_preservation e f:
  forall isz sz r v,
    B.wf_size_expr' isz sz ->
    Bsem.eval_expr e f sz r ->
    Bsem.eval_expr (PTree.set isz v e) f sz r.
Proof.
  move=> isz. elim/B.expr_ind => /=.
  - case=> ? [] //= >. rewrite andbT => /Pos.eqb_spec NEQ.
    inv 1. inv H1. inv H7. econstructor. eauto with semantics.
    simpl. rewrite PTree.gso //. assumption.
  - inv 2; econstructor.
  - move=>> IH >; inv 2; econstructor; eauto.
  - move=>> IH >; inv 2; econstructor; eauto.
  - move=>> IH1 > IH2 > /andP []; inv 3; econstructor; eauto.
  - move=>> IH1 > IH2 > /andP []; inv 3; econstructor; eauto.
  - move=>> IH1 > IH2 > /andP []; inv 3; econstructor; eauto.
Qed.

Lemma eval_expr_size_assign_preservation e f:
  forall i llsz llsz',
    (B.fn_szenv f)!i = Some llsz ->
    (B.fn_szenv' f)!i = Some llsz' ->
    Forall2 (Forall2 (fun sz isz =>
      forall r v, Bsem.eval_expr e f sz r ->
                  Bsem.eval_expr (PTree.set isz v e) f sz r
    )) llsz llsz'.
Proof.
  intros * Si Si'. have:= B.fn_szenv_szenv'_str f _ _ _ Si Si'.
  case/Forall2_forall => LEN LEN'.
  apply Forall2_forall. split=> //. move=> j lsz lsz' Nj Nj'.
  move/(_ _ _ _ Nj Nj') in LEN'.
  apply Forall2_forall. split=> //. move=> k sz isz Nk Nk'.
  have:= B.fn_szenv_wf f _ _ _ Si Si'.
  case/forallb2_forall => _ /(_ _ _ _ Nj' Nj).
  case/forallb2_forall => _ /(_ _ _ _ Nk' Nk) => + >.
  by apply: eval_expr_wf_size_expr'_assign_preservation.
Qed.

Definition tr_alloc (tr: trmem) (n: nat) (i: ident) (b: block) :=
  fun n' (p: sem_path) =>
    let (j, l) := p in
    if Nat.eqb n n' && Pos.eqb i j && list_beq _ sem_path_elem_beq l []
    then Some (b, Ptrofs.zero, Visible)
    else tr n' (j, l).

Lemma tr_alloc_lt_N tr N i b:
  forall n p,
    (n < N)%nat ->
    tr_alloc tr N i b n p = tr n p.
Proof. move=> n [j l] /=. case Nat.eqb_spec => //=. lia. Qed.

Lemma tr_alloc_inv:
  forall tr N i b0 n j l b o s,
    tr_alloc tr N i b0 n (j, l) = Some (b, o, s) ->
    tr n (j, l) = Some (b, o, s) \/
    (n = N /\ j = i /\ l = [] /\ b = b0 /\ o = Ptrofs.zero /\ s = Visible).
Proof.
  move=> tr N i b0 n j l b o s /=.
  case Nat.eqb_spec => [<-|?] /=; eauto.
  case Pos.eqb_spec => [<-|?] /=; eauto.
  case (list_reflect _ sem_path_elem_reflect) => [->|?] /=; eauto.
  case=> <- <- <-. by right.
Qed.

Lemma wf_trmem_alloc:
  forall tr n i b,
    wf_trmem tr n ->
    n = 0%nat \/ (forall p b o l, tr (Nat.pred n) p <> Some (b, o, Hidden (i, l))) ->
    wf_trmem (tr_alloc tr n i b) n.
Proof.
  intros * [] N. apply mk_wf_trmem.
  - move=> n' id pl1 [>]; cbn -[Nat.eqb]. case Nat.eqb_spec => [<-|Hn] /=; eauto.
    case Pos.eqb_spec => [_|?] /=; eauto.
    case (list_reflect _ sem_path_elem_reflect) => [->|?] /= + [] //=; eauto.
  - move=> ? [j l] > H [s]; cbn -[Nat.eqb]. case Nat.eqb_spec => /=. lia.
    move=> _ T. apply (trmem_max0 _ (j, l) H). eauto.
  - move=> ? [j l] [j' l'] >; cbn -[Nat.eqb]. case Nat.eqb_spec => [<-|] /=; eauto.
    case Pos.eqb_spec => [<-|?] /=; case Pos.eqb_spec => [<-|?] /=; eauto;
    case (list_reflect _ sem_path_elem_reflect) => [->|?] //=; eauto.
    case (list_reflect _ sem_path_elem_reflect) => [->|?] //=; eauto.
  - move=> ? j l b' o' ? l' Hn. rewrite (tr_alloc_lt_N _ _ _ _ _ _ Hn).
    destruct n. lia. case: N => //. move/(_ (j, l) b' o' l') => /=.
    case Nat.eqb_spec => [<-|?] /=; eauto. case Pos.eqb_spec => [<-|?] /=; eauto.
    case (list_reflect _ sem_path_elem_reflect) => [->|?] /=; eauto. easy.
Qed.

Lemma visible_separated_alloc pe:
  forall tr N n m lo hi m' b i,
    (n <= N)%nat ->
    valid_blocks tr N m ->
    Mem.alloc m lo hi = (m', b) ->
    visible_separated tr n pe ->
    visible_separated (tr_alloc tr N i b) n pe.
Proof.
  intros * Hn VBLKS ALLOC H. case/Nat.le_lteq: (Hn).
  - move=> Hn' > T PE P. split=>>; [intro Hm|].
    all: move: T; rewrite !tr_alloc_lt_N //; try lia; move=> T.
    + move: Hm. exact: (proj1 (H _ _ _ _ _ T PE P)).
    + exact: (proj2 (H _ _ _ _ _ T PE P)).
  - move=> <- > T PE P. split.
    + move=>> Hm. rewrite tr_alloc_lt_N //. move: T => /=.
      rewrite Nat.eqb_refl /=. case Pos.eqb_spec => _ //=.
      case (list_reflect _ sem_path_elem_reflect) => _.
      * case=> <- _ /VBLKS => /(_ (Nat.lt_le_incl _ _ (Nat.lt_le_trans _ _ _ Hm Hn))).
        rewrite (Mem.alloc_result _ _ _ _ _ ALLOC). lia.
      * move=> T. exact: (proj1 (H _ _ _ _ _ T PE P)).
      * move=> T. exact: (proj1 (H _ _ _ _ _ T PE P)).
    + move=> [] >. move: T. rewrite (Mem.alloc_result _ _ _ _ _ ALLOC).
      case/tr_alloc_inv=> ++ T'; case/tr_alloc_inv: T'.
      * move/[swap]/H => /(_ _ PE P) /proj2. eauto.
      * intros (_ & _ & _ & -> & _) => /VBLKS => /(_ Hn). lia.
      * move/VBLKS => /(_ Hn). intros ? (_ & _ & _ & -> & _). lia.
      * by do 2 intros (_ & -> & -> & _).
Qed.

Lemma owned_at_offset_zero_alloc tr N b i pe:
  forall n,
    (n <= N)%nat ->
    owned_at_offset_zero tr n pe ->
    owned_at_offset_zero (tr_alloc tr N i b) n pe.
Proof.
  move=> n /Nat.lt_eq_cases [Hn|->] H j >.
  - rewrite tr_alloc_lt_N //. exact: H.
  - move=> /=. rewrite Nat.eqb_refl /=.
    case Pos.eqb_spec => [<-|NEQ] /=; [|exact: H].
    case list_beq; [|exact: H]. case=> _ <- _ //.
Qed.

Lemma blk_ofs_preserved_alloc tr N b i pe:
  forall n,
    (n < N)%nat ->
    (S n = N -> pe!i = Some B.Owned) ->
    (S n = N -> hidden_is_mut tr n pe) ->
    blk_ofs_preserved tr n ->
    blk_ofs_preserved (tr_alloc tr N i b) n.
Proof.
  move=> n Hn PE HMUT H j l b0 o0 j' l' b' o' s'.
  rewrite tr_alloc_lt_N; [lia|]. move=> T /tr_alloc_inv [].
  - by apply: H T.
  - intros (<- & -> & -> & -> & -> & ->).
    move/HMUT: T => /(_ eq_refl). by rewrite PE.
Qed.

Lemma valid_trmem_cont_alloc tr N m lo hi m' b i:
  forall k n pe,
    (n <= N)%nat ->
    (n = N -> pe!i = Some B.Owned) ->
    valid_blocks tr N m ->
    Mem.alloc m lo hi = (m', b) ->
    valid_trmem_cont tr n pe k ->
    valid_trmem_cont (tr_alloc tr N i b) n pe k.
Proof.
  move=> +++++ VBLOCKS ALLOC.
  elim/Bsem.cont_ind => // _ e f mut k IH [|n] pe Hn PE //=.
  intros (VS & HM & ODEF & MPRES & NOALIAS & NOREPET & MLIST & OWNZ & VK).
  split; [|split; [|split; [|split; [|repeat split]]]].
  - eapply visible_separated_alloc; try eassumption. lia.
  - move=>>. rewrite tr_alloc_lt_N. lia. by apply HM.
  - move=> ic lc i' l'. rewrite (tr_alloc_lt_N _ _ _ _ n). lia.
    move=> IN. split.
    + case=> [[[]]] >. case/tr_alloc_inv.
      move=> T. exact: (proj1 (ODEF _ _ _ _ IN) (ex_intro _ _ T)).
      intros (X & -> & -> & -> & -> & ->). rewrite app_nil_r.
      move/MLIST in IN. by rewrite (PE X) in IN.
    + move/ODEF => /(_ _ IN). rewrite/tr_alloc. case andP; eauto.
  - eapply blk_ofs_preserved_alloc; eauto.
    move=> *; eapply mutable_list_hidden_is_mut; eassumption.
  - assumption.
  - assumption.
  - assumption.
  - apply owned_at_offset_zero_alloc => //. lia.
  - apply: IH. lia. lia. assumption.
Qed.

Lemma match_env_alloc_lt_N tr N b i:
  forall n e te,
    (n < N)%nat ->
    match_env tr n e te ->
    match_env (tr_alloc tr N i b) n e te.
Proof.
  move=>> Hn H ? [] > /H -> //; cbn -[tr_alloc]; by rewrite tr_alloc_lt_N.
Qed.

Lemma match_cont_alloc tr N b i ge:
  forall k tk n f,
    (n <= N)%nat ->
    match_cont tr n ge f k tk ->
    match_cont (tr_alloc tr N i b) n ge f k tk.
Proof.
  elim/Bsem.cont_ind => //.
  - move=>> IH [] // ??? [] //= > Hn [+ /(IH _ _ _ Hn)] //.
  - by move=>> IH [].
  - move=>> IH /= >. repeat destruct_match_goal.
    move=> _ Hn [->] [->] [].
    move/Nat.le_succ_l: Hn => /[dup] /Nat.lt_le_incl Hn.
    move/match_env_alloc_lt_N/[apply] => ? /(IH _ _ _ Hn) //.
Qed.

Lemma no_hidden_alloc tr N b i:
  forall n,
    no_hidden tr n -> no_hidden (tr_alloc tr N i b) n.
Proof.
  move=>> H [] >. case T: tr_alloc => [[[??] []]|] //=.
  case/tr_alloc_inv: T. by move/H. easy.
Qed.

Lemma match_env_set_alloc tr n e te i b lv:
  match_env tr n e te ->
  match_env (tr_alloc tr n i b) n
    (PTree.set i (Varr lv) e)
    (PTree.set i (Vptr b Ptrofs.zero) te).
Proof.
  intros * ME i' v'. case (Pos.eqb_spec i i') as [<-|Hneq].
  - rewrite 2!PTree.gss. intros [= <-]. simpl.
    now rewrite Nat.eqb_refl Pos.eqb_refl.
  - rewrite -> 2!PTree.gso by lia. 
    move/ME => ->. case: v' =>> //=.
    rewrite Nat.eqb_refl. by case Pos.eqb_spec.
Qed.

Lemma default_value_memory_chunk_default_value tr n:
  forall t v,
    default_value t = Some v ->
    memory_chunk_default_value (typ_to_memory_chunk t) = transl_value tr n None v.
Proof. case=> [| |[] []|_|[]|] //= > [<-] //. Qed.

Lemma default_value_primitive:
  forall t v, default_value t = Some v -> primitive_value v.
Proof. case=> [| |[] []|_|[]|] //= > [<-] //. Qed.

Lemma match_tr_value_tr_alloc tr n i b j:
  forall t v s,
    i <> j ->
    match_tr_value tr n (j, s) v t ->
    match_tr_value (tr_alloc tr n i b) n (j, s) v t.
Proof.
  move=> +++ NEQ. elim/typ_ind => //= > IH [] //= >.
  rewrite Nat.eqb_refl (proj2 (Pos.eqb_neq _ _)) //=.
  change ident with positive.
  case tr => [[[??]?]|] //= H > /H. by apply: IH.
Qed.

Lemma match_tr_value_tr_alloc_lt_N tr N i b j n:
  forall t v s,
    (n < N)%nat ->
    match_tr_value tr n (j, s) v t ->
    match_tr_value (tr_alloc tr N i b) n (j, s) v t.
Proof.
  move=> +++ Hn. elim/typ_ind => // > IH [] // >; cbn -[tr_alloc].
  rewrite tr_alloc_lt_N //.
  case tr => [[[??]?]|] //= H > /H. by apply: IH.
Qed.

Lemma match_mem_env_alloc_N tr N te e tm:
  forall n t i v m' b' m'',
    default_value t = Some v ->
    te!i = Some (Tarr t) ->
    typ_sizeof t * Int64.unsigned n <= Ptrofs.max_unsigned ->
    Mem.alloc tm (- size_chunk Mptr)
      (Ptrofs.unsigned (Ptrofs.mul (Ptrofs.repr (typ_sizeof t)) (Ptrofs.of_int64 n)))
      = (m', b') ->
    Mem.load Mptr m'' b' (- size_chunk Mptr) =
      Some (Vptrofs (Ptrofs.mul (Ptrofs.repr (typ_sizeof t)) (Ptrofs.of_int64 n))) ->
    Mem.valid_access m'' Mptr b' (- size_chunk Mptr) Freeable ->
    (forall i,
      0 <= Z.of_nat i < Ptrofs.unsigned (Ptrofs.of_int64 n) ->
      Mem.valid_access m'' (typ_to_memory_chunk t) b'
        (size_chunk (typ_to_memory_chunk t) * Z.of_nat i) Freeable /\
      Mem.load (typ_to_memory_chunk t) m'' b'
        (size_chunk (typ_to_memory_chunk t) * Z.of_nat i) =
      Some (memory_chunk_default_value (typ_to_memory_chunk t))) ->
    Mem.unchanged_on (fun b : block => fun=> Mem.valid_block tm b) m' m'' ->
    match_mem_env tr N te e tm ->
    match_mem_env (tr_alloc tr N i b') N te
      (PTree.set i (Varr (repeat v (Z.to_nat (Int64.unsigned n)))) e) m''.
Proof.
  intros * Hv Ht H ALLOC LSZ VSZ LV U MME j v' t'. case (Pos.eqb_spec j i) => [->|NEQ].
  - rewrite PTree.gss => - [<-]. rewrite Ht => - [<-] /=.
    rewrite Nat.eqb_refl Pos.eqb_refl /=. split.
    { rewrite LSZ repeat_length Znat.Z2Nat.id //. by apply Int64.unsigned_range. }
    move=> idx Hidx.
    have Hidx' := Hidx. rewrite repeat_length in Hidx'. split.
    2: apply (and_assoc (Mem.valid_access _ _ _ _ _)); split.
    + change (Ptrofs.unsigned Ptrofs.zero) with 0 => /=.
      have:= typ_sizeof_bounds t. nia.
    + have: 0 <= Z.of_nat idx < Ptrofs.unsigned (Ptrofs.of_int64 n).
      { split. lia. rewrite/Ptrofs.of_int64. rewrite Ptrofs.unsigned_repr; [|lia].
        change Ptrofs.max_unsigned with Int64.max_unsigned;
          exact: Int64.unsigned_range_2. }
      case/LV. rewrite/Ptrofs.add/Ptrofs.mul.
      change (Ptrofs.unsigned Ptrofs.zero) with 0 => /=.
      have:= Int64.unsigned_range_2 n;
        change Int64.max_unsigned with Ptrofs.max_unsigned => Bn.
      have Bt:= typ_sizeof_bounds t.
      have MU: Ptrofs.max_unsigned = 18446744073709551615 by reflexivity.
      rewrite Ptrofs.repr_unsigned (Ptrofs.unsigned_repr (typ_sizeof _)); [lia|].
      rewrite (Ptrofs.unsigned_repr (Z.of_nat _)); [lia|].
      rewrite Ptrofs.unsigned_repr; [nia|].
      rewrite size_chunk_typ_sizeof => V L. split=> //.
      rewrite L. set l := repeat _ _. set tr' := tr_alloc _ _ _ _.
      have:= nth_error_nth' l Vundef Hidx. rewrite nth_error_repeat; [lia|].
      rewrite (default_value_memory_chunk_default_value tr' N _ _ Hv) => - [<-].
      have:= default_value_primitive _ _ Hv. by case v.
    + set l := repeat _ _. set tr' := tr_alloc _ _ _ _.
      have:= nth_error_nth' l Vundef Hidx. rewrite nth_error_repeat; [lia|].
      case=> <-. move: Hv. case t; by case v.
  - rewrite PTree.gso //. move/MME/[apply].
    elim/typ_ind: t' v' [] => // t' IH [] //= lv s. have NEQ' := not_eq_sym NEQ.
    have:= match_tr_value_tr_alloc tr N i b' j (Tarr t') (Varr lv) s NEQ' => /=.
    rewrite Nat.eqb_refl (proj2 (Pos.eqb_neq _ _)) //.
    case (tr N (j, s)) => [[[b o] []]|] //=.
    move=> _ [[LSZ' VSZ'] T]. have Vb: Mem.valid_block tm b.
    { eapply Mem.valid_access_valid_block, Mem.valid_access_implies.
      exact VSZ'. exact: perm_any_N. }
    split.
    { split. apply (Mem.load_unchanged_on _ _ _ _ _ _ _ U) => //.
      by apply (Mem.load_alloc_other _ _ _ _ _ ALLOC).
      case/(Mem.valid_access_alloc_other _ _ _ _ _ ALLOC): VSZ' => P ?.
      move=>>; split=> //. move=>> /P X.
      apply (Mem.unchanged_on_perm _ _ _ U) => //.
      by move/Mem.perm_valid_block in X. }
    move=>> /T. intros (? & V & L & M). split=> //. split; [|split].
    * move/(Mem.valid_access_alloc_other _ _ _ _ _ ALLOC): V.
      rewrite/Mem.valid_access => - [P A]; split=> //.
      move=>> /P. apply (Mem.unchanged_on_perm _ _ _ U _ _ _ _ Vb).
      exact (Mem.valid_block_alloc _ _ _ _ _ ALLOC _ Vb).
    * move/(Mem.load_alloc_other _ _ _ _ _ ALLOC): L.
      move/(Mem.load_unchanged_on _ _ _ _ _ _ _ U (fun _ _ => Vb)) => ->.
      case (nth _ lv _) =>> //=; rewrite Nat.eqb_refl. by case Pos.eqb_spec.
    * by apply: IH.
Qed.

Lemma match_mem_env_alloc_lt_N tr N N' te e tm:
  forall n t i m' b' m'',
    (N' < N)%nat ->
    Mem.alloc tm (- size_chunk Mptr)
      (Ptrofs.unsigned (Ptrofs.mul (Ptrofs.repr (typ_sizeof t)) (Ptrofs.of_int64 n)))
      = (m', b') ->
    (forall i,
      0 <= Z.of_nat i < Ptrofs.unsigned (Ptrofs.of_int64 n) ->
      Mem.valid_access m'' (typ_to_memory_chunk t) b'
        (size_chunk (typ_to_memory_chunk t) * Z.of_nat i) Freeable /\
      Mem.load (typ_to_memory_chunk t) m'' b'
        (size_chunk (typ_to_memory_chunk t) * Z.of_nat i) =
      Some (memory_chunk_default_value (typ_to_memory_chunk t))) ->
    Mem.unchanged_on (fun b : block => fun=> Mem.valid_block tm b) m' m'' ->
    match_mem_env tr N' te e tm ->
    match_mem_env (tr_alloc tr N i b') N' te e m''.
Proof.
  intros * HN' ALLOC LV U MME i v t => /MME/[apply].
  elim/typ_ind: t v [] => // t IH [] // lv s.
  have:= match_tr_value_tr_alloc_lt_N tr N i0 b' i N' (Tarr t) (Varr lv) s HN'.
  cbn -[tr_alloc Ptrofs.max_unsigned].
  rewrite tr_alloc_lt_N //. case tr => [[[b o] []]|] //.
  move=> _ [[LSZ VSZ] T]. have Vb: Mem.valid_block tm b.
  { eapply Mem.valid_access_valid_block, Mem.valid_access_implies.
    exact VSZ. exact: perm_any_N. }
  split.
  { split. apply (Mem.load_unchanged_on _ _ _ _ _ _ _ U) => //.
    by apply (Mem.load_alloc_other _ _ _ _ _ ALLOC).
    case/(Mem.valid_access_alloc_other _ _ _ _ _ ALLOC): VSZ => P ?.
    move=>>; split=> //. move=>> /P X.
    apply (Mem.unchanged_on_perm _ _ _ U) => //.
    by move/Mem.perm_valid_block in X. }
  move=>> /T. intros (? & V & L & M). split=> //. split; [|split].
  * move/(Mem.valid_access_alloc_other _ _ _ _ _ ALLOC): V.
    rewrite/Mem.valid_access => - [P A]; split=> //.
    move=>> /P. apply (Mem.unchanged_on_perm _ _ _ U _ _ _ _ Vb).
    exact (Mem.valid_block_alloc _ _ _ _ _ ALLOC _ Vb).
  * move/(Mem.load_alloc_other _ _ _ _ _ ALLOC): L.
    move/(Mem.load_unchanged_on _ _ _ _ _ _ _ U (fun _ _ => Vb)) => ->.
    unfold transl_value; by rewrite tr_alloc_lt_N.
  * by apply: IH.
Qed.

Lemma match_mem_cont_alloc tr N tm n i m' b' m'' t:
  forall k N',
    (N' <= N)%nat ->
    Mem.alloc tm (- size_chunk Mptr)
      (Ptrofs.unsigned (Ptrofs.mul (Ptrofs.repr (typ_sizeof t)) (Ptrofs.of_int64 n)))
      = (m', b') ->
    (forall i,
      0 <= Z.of_nat i < Ptrofs.unsigned (Ptrofs.of_int64 n) ->
      Mem.valid_access m'' (typ_to_memory_chunk t) b'
        (size_chunk (typ_to_memory_chunk t) * Z.of_nat i) Freeable /\
      Mem.load (typ_to_memory_chunk t) m'' b'
        (size_chunk (typ_to_memory_chunk t) * Z.of_nat i) =
      Some (memory_chunk_default_value (typ_to_memory_chunk t))) ->
    Mem.unchanged_on (fun b : block => fun=> Mem.valid_block tm b) m' m'' ->
    match_mem_cont tr N' k tm ->
    match_mem_cont (tr_alloc tr N i b') N' k m''.
Proof.
  move=> +++ ALLOC LV U; elim/Bsem.cont_ind =>> //= IH [] > // Hn.
  case=> MME MMK. split.
  eapply match_mem_env_alloc_lt_N; try eassumption; lia.
  apply IH => //; lia.
Qed.

Lemma valid_blocks_alloc tr n m m' lo hi b i:
  valid_blocks tr n m ->
  Mem.alloc m lo hi = (m', b) ->
  valid_blocks (tr_alloc tr n i b) n m'.
Proof.
  move=> H ALLOC ? [j l] > Hn. rewrite (Mem.nextblock_alloc _ _ _ _ _ ALLOC).
  case/tr_alloc_inv.
  - move/H => /(_ Hn). lia.
  - intros (_ & _ & _ & -> & _). rewrite (Mem.alloc_result _ _ _ _ _ ALLOC). lia.
Qed.

Definition tr_free (tr: trmem) (N: nat) (i: ident) :=
  fun n (p: sem_path) =>
    let (j, l) := p in
    if Nat.eqb n N && Pos.eqb i j then None
    else tr n (j, l).

Lemma tr_free_neq_N tr N i:
  forall n p,
    n <> N ->
    tr_free tr N i n p = tr n p.
Proof. move=> n [j l] Hn /=. by case Nat.eqb_spec. Qed.

Lemma tr_free_Some tr N i:
  forall n p x,
    tr_free tr N i n p = Some x ->
    tr n p = Some x.
Proof.
  move=> n [j l] > /=. case Nat.eqb_spec => [->|] //.
  by case Pos.eqb_spec.
Qed.

Lemma wf_trmem_free tr n i:
  wf_trmem tr n ->
  n = 0%nat \/ (forall p b o l, tr (Nat.pred n) p <> Some (b, o, Hidden (i, l))) ->
  wf_trmem (tr_free tr n i) n.
Proof.
  move=> WF X. apply mk_wf_trmem.
  - move=>> /=. case Nat.eqb_spec => [->|NEQ] /=.
    case Pos.eqb_spec => // _. all: by apply (trmem_prefix _ _ WF).
  - move=> ? [] > + /=. case Nat.eqb_spec => [->|NEQ] /=. lia.
    by apply (trmem_max _ _ WF).
  - move=> ? [j l] [j' l'] > /tr_free_Some + /tr_free_Some.
    by apply (trmem_hid_sep _ _ WF).
  - move=> ? j l > Hn /tr_free_Some. cbn -[Nat.eqb].
    case Nat.eqb_spec => [|_] /=. 2: by apply (trmem_hid _ _ WF).
    intros <-. case Pos.eqb_spec => [<-|_].
    + by case: X => //=/[apply].
    + by apply (trmem_hid _ _ WF).
Qed.

Lemma visible_separated_free tr N i pe:
  forall n,
    (n <= N)%nat ->
    visible_separated tr n pe ->
    visible_separated (tr_free tr N i) n pe.
Proof.
  move=> n Hn H j l b o p + PE P => /=. case Nat.eqb_spec => [<-|NEQ] /=.
  - case Pos.eqb_spec => [->|NEQ] // /H/(_ PE P) [H1 H2]; split.
    + move=>> ?; rewrite tr_free_neq_N; [lia|]; by apply H1.
    + move=>> + /tr_free_Some; by apply H2.
  - case/H/(_ PE P) => H1 H2; split.
    + move=>> ?; rewrite tr_free_neq_N; [lia|]; by apply H1.
    + move=>>; rewrite tr_free_neq_N; [lia|]; by apply H2.
Qed.

Lemma owned_at_offset_zero_free tr N i pe:
  forall n,
    (n <= N)%nat ->
    owned_at_offset_zero tr n pe ->
    owned_at_offset_zero (tr_free tr N i) n pe.
Proof. move=>> Hn H > /tr_free_Some; by apply H. Qed.

Lemma blk_ofs_preserved_free tr N i:
  forall n,
    (n < N)%nat ->
    blk_ofs_preserved tr n ->
    blk_ofs_preserved (tr_free tr N i) n.
Proof.
  move=> n Hn H j l b o j' l' b' o' s'.
  rewrite tr_free_neq_N; [lia|].
  move=> + /tr_free_Some. by apply: H.
Qed.

Lemma valid_trmem_cont_free tr N i:
  forall k n pe,
    (n <= N)%nat ->
    (n = N -> pe!i = Some B.Owned) ->
    valid_trmem_cont tr n pe k ->
    valid_trmem_cont (tr_free tr N i) n pe k.
Proof.
  elim/Bsem.cont_ind => //= _ _ f m k IH [|n] // pe Hn PE.
  intros (VS & HM & ODEF & MPRES & NOALIAS & NOREPET & MLIST & OWNZ & VK).
  split; [|split; [|split; [|split; [|repeat split]]]].
  - apply visible_separated_free. lia. assumption.
  - move=>>; rewrite tr_free_neq_N; [lia|]; by apply HM.
  - move=>> H. rewrite (tr_free_neq_N _ _ _ n); [lia|]; split.
    + case=> x /tr_free_Some/(ex_intro (fun _ => _) x); by apply ODEF.
    + move/[dup]/ODEF => /(_ _ H) + Tn. case=>>; cbn -[Nat.eqb].
      case Nat.eqb_spec => [HN|] /=; eauto. case Pos.eqb_spec; eauto.
      move/MLIST: H => /[swap] <-. by rewrite (PE HN).
  - eapply blk_ofs_preserved_free; eassumption.
  - assumption.
  - assumption.
  - assumption.
  - move=>> /tr_free_Some; by apply OWNZ.
  - apply: IH => //; lia.
Qed.

Lemma match_env_free_lt_N tr N e te:
  forall n i,
    (n < N)%nat ->
    match_env tr n e te ->
    match_env (tr_free tr N i) n e te.
Proof. move=>> Hn H ? [] > /H //= ->; case Nat.eqb_spec => //; lia. Qed.

Lemma match_env_free tr n e te:
  forall i isz,
    match_env tr n e te ->
    match_env (tr_free tr n i) n (PTree.remove i (PTree.remove isz e)) te.
Proof.
  move=> i isz H j [] > /=. 7: rewrite Nat.eqb_refl.
  all: case (Pos.eqb_spec i j) => [<-|?]; [by rewrite PTree.grs|].
  all: rewrite PTree.gro; eauto.
  all: case (Pos.eqb_spec j isz) => [->|?]; [by rewrite PTree.grs|].
  all: rewrite PTree.gro; eauto; try apply H.
Qed.

Lemma match_cont_free ge tr N i:
  forall k tk n f,
    (n <= N)%nat ->
    match_cont tr n ge f k tk ->
    match_cont (tr_free tr N i) n ge f k tk.
Proof.
  elim/Bsem.cont_ind => //.
  - move=>> IH [] //= ??? [] // > Hn [->]; eauto.
  - move=>> IH [] //= [] // [] // [] // ? [] // ?? [] // > Hn [->]; eauto.
  - move=>> IH [] //= ?? [] // ?? [] > // _ ? [->] [->].
    case; repeat split=> //. by apply match_env_free_lt_N.
    apply IH => //; lia.
Qed.

Lemma no_hidden_free tr N i:
  forall n,
    no_hidden tr n ->
    no_hidden (tr_free tr N i) n.
Proof. move=>> H > /tr_free_Some; by apply H. Qed.

Lemma valid_blocks_free tr N i:
  forall n m b lo hi m',
    Mem.free m b lo hi = Some m' ->
    valid_blocks tr n m ->
    valid_blocks (tr_free tr N i) n m'.
Proof.
  move=>> F H > + /tr_free_Some => /H/[apply].
  by rewrite (Mem.nextblock_free _ _ _ _ _ F).
Qed.

Lemma match_tr_value_free tr N i:
  forall n t v j l,
    (n < N)%nat \/ j <> i ->
    match_tr_value tr n (j, l) v t ->
    match_tr_value (tr_free tr N i) n (j, l) v t.
Proof.
  move=> n ++ j + Hn. elim/typ_ind => //= t IH [] // >.
  case Nat.eqb_spec => [EQ|NEQ]; case tr => [[[]]|] > //=.
  case Pos.eqb_spec => [EQ'|NEQ]. lia.
  all: move=> + > => /[apply]/IH //.
Qed.

Lemma match_mem_env_free_N tr n i te pe e m:
  forall isz b lo hi m',
    tr n (i, []) = Some (b, Ptrofs.zero, Visible) ->
    Mem.free m b lo hi = Some m' ->
    visible_separated tr n pe ->
    pe!i = Some B.Owned ->
    match_mem_env tr n te e m ->
    match_mem_env (tr_free tr n i) n te (PTree.remove i (PTree.remove isz e)) m'.
Proof.
  move=> isz b lo hi m' T F VS PE H j v t.
  case (Pos.eqb_spec j i) => [->|?]; [by rewrite PTree.grs|]. rewrite PTree.gro //.
  case (Pos.eqb_spec j isz) => [->|?]; [by rewrite PTree.grs|]. rewrite PTree.gro //.
  move/H/[apply].
  elim/typ_ind: t v [] => // t IH [] // lv l;
    cbn -[match_tr_value Ptrofs.max_unsigned].
  rewrite Nat.eqb_refl. case Pos.eqb_spec; [lia|].
  cbn -[match_tr_value Ptrofs.max_unsigned] => _.
  case T': tr => [[[b' o'] []]|] //. 2: apply match_tr_value_free; eauto.
  intros ((L & V) & MV). have SEP: b' <> b.
  { eapply not_eq_sym, (proj2 (VS _ _ _ _ _ T PE eq_refl)) with (2 := T').
    congruence. }
  split; [split|].
  - rewrite (Mem.load_free _ _ _ _ _ F) //; by left.
  - apply (Mem.valid_access_free_1 _ _ _ _ _ F) => //; by left.
  - move=>> /MV {MV}. intros (OFS & V' & L' & MV).
    split; [|split; [|split]] => //.
    + apply (Mem.valid_access_free_1 _ _ _ _ _ F) => //; by left.
    + rewrite (Mem.load_free _ _ _ _ _ F). by left.
      rewrite L'. case (nth _ _ Vundef) =>> //=.
      rewrite Nat.eqb_refl. case Pos.eqb_spec => //. congruence.
    + by apply IH.
Qed.

Lemma match_mem_env_free_lt_N tr N n i te pe e m:
  forall b lo hi m',
    (n < N)%nat ->
    tr N (i, []) = Some (b, Ptrofs.zero, Visible) ->
    Mem.free m b lo hi = Some m' ->
    visible_separated tr N pe ->
    pe!i = Some B.Owned ->
    match_mem_env tr n te e m ->
    match_mem_env (tr_free tr N i) n te e m'.
Proof.
  move=> b lo hi m' Hn T F VS PE H j v t. move/H/[apply].
  elim/typ_ind: t v [] => // t IH [] // lv l;
      cbn -[match_tr_value Ptrofs.max_unsigned].
  case Nat.eqb_spec => [|_]; [lia|]. cbn -[match_tr_value Ptrofs.max_unsigned].
  case T': tr => [[[b' o'] []]|] //. 2: apply match_tr_value_free; eauto.
  intros ((L & V) & MV). have SEP: b' <> b.
  { eapply not_eq_sym, (proj1 (VS _ _ _ _ _ T PE eq_refl)) with (2 := T').
    congruence. }
  split; [split|].
  + rewrite (Mem.load_free _ _ _ _ _ F) //; by left.
  + apply (Mem.valid_access_free_1 _ _ _ _ _ F) => //; by left.
  + move=>> /MV {MV}. intros (OFS & V' & L' & MV).
    split; [|split; [|split]] => //.
    * apply (Mem.valid_access_free_1 _ _ _ _ _ F) => //; by left.
    * rewrite (Mem.load_free _ _ _ _ _ F). by left.
      rewrite L'. case (nth _ _ Vundef) =>> //=. case Nat.eqb_spec => //; lia.
    * by apply IH.
Qed.

Lemma match_mem_cont_free tr N i pe m:
  forall b lo hi m' k n,
    (n <= N)%nat ->
    tr N (i, []) = Some (b, Ptrofs.zero, Visible) ->
    Mem.free m b lo hi = Some m' ->
    visible_separated tr N pe ->
    pe!i = Some B.Owned ->
    match_mem_cont tr n k m ->
    match_mem_cont (tr_free tr N i) n k m'.
Proof.
  move=> b lo hi m' +++ T F VS PE. elim/Bsem.cont_ind => //=.
  move=> _ > _ k IH [|n] // Hn [MME MMK]; split.
  - eapply match_mem_env_free_lt_N; eassumption.
  - apply IH => //; lia.
Qed.

Lemma destructCont_is_Kreturnto_or_Kstop:
  forall k, Bsem.is_Kreturnto_or_Kstop (Bsem.destructCont k).
Proof. by elim/Bsem.cont_ind. Qed.

Fixpoint cont_height (k: Bsem.cont) :=
  match k with
  | Bsem.Kstop => 0%nat
  | Bsem.Kseq _ k | Bsem.Kblock k | Bsem.Kreturnto _ _ _ _ k => S (cont_height k)
  end.

Definition measure (st: Bsem.state) :=
  match st with
  | Bsem.Callstate _ _ k => cont_height k
  | Bsem.State _ _ _ k => cont_height k
  | Bsem.Returnstate _ _ _ k => cont_height k
  end.

Theorem transl_stmt_sem_preservation:
  forall p hfuncs Habort Hcalloc tp s s' ts,
    Coqlib.list_norepet (map fst hfuncs) ->
    Coqlib.list_disjoint (map fst (B.prog_defs p)) (map fst hfuncs) ->
    transl_program' hfuncs Habort Hcalloc p = OK tp ->
    match_states p hfuncs Habort Hcalloc s ts ->
    Bsem.step_stmt (genv_of_program p) s s' ->
    (exists ts', plus step (Genv.globalenv tp) ts E0 ts' /\
                 match_states p hfuncs Habort Hcalloc s' ts')
    \/ (measure s' < measure s /\ match_states p hfuncs Habort Hcalloc s' ts)%nat.
Proof.
  intros p hfuncs Habort Hcalloc tp s s' ts Hnorepet Hdisjoint TPROG MSTATE HSTEP.
  inversion HSTEP; clear HSTEP; rewrite_equalities.
  (* Sskip, Kseq *)
  - inversion_clear MSTATE. destruct tk; try contradiction. destruct MK.
    revert TS; intros [= <-].
    left; eexists. split. eapply plus_one. apply step_skip_seq.
    eapply match_states_State; try eassumption.
  (* Sskip, Kblock *)
  - inversion_clear MSTATE. simpl in MK. repeat destruct_match MK. subst.
    revert TS; intros [= <-].
    left; eexists. split. eapply plus_one.
    apply step_skip_block.
    eapply match_states_State; try eassumption. reflexivity.
  (* Sskip, Kreturnto *)
  - inversion_clear MSTATE. destruct k; try contradiction.
    simpl in MK. repeat destruct_match MK. decompose [and] MK.
    revert TS; intros [= <-].
    left; eexists. split. apply plus_one. apply step_skip_call; easy.
    change (Kcall ?a ?b ?c ?d ?e) with (call_cont (Kcall a b c d e)).
    eapply match_states_Returnstate with (r := Vundef); try eassumption.
    reflexivity.
  (* Sassign *)
  - inversion_clear MSTATE. simpl in TS.
    rewrite -> get_tenv_syn_path_eval_path
      with (1 := Bsem.eval_full_path_eval_path _ _ _ _ H), H0 in TS.
    simpl in TS. destruct transl_syn_path eqn:Tp0; try discriminate.
    simpl in TS. destruct transl_expr eqn:Texp; try discriminate.
    simpl in TS. destruct p0 as [i l]. inv H. rename H9 into Hlen.
    simpl in Tp0. rewrite H8 in Tp0. rename l into synpl.
    destruct (B.fn_tenv f)!i eqn:Hte; try discriminate. simpl in Tp0.
    case (list_cases_rev synpl) as [->|[syne [synpl' ->]]].
    + revert Tp0; intros [= <-]. revert TS; intros [= <-]. inv H11.
      simpl in H0, H5. revert H5; intros [= <-].
      rewrite Hte in H0. injection H0 as ->.
      all: pose proof (primitive_value_shrink _ t H2).
      destruct t as [| |[] []| | |].
      all: left; eexists; split; [eapply plus_one; apply step_set|].
      5,7,9,11: eapply eval_Eunop.
      all: try (eapply transl_expr_sem_preservation; eassumption).
      3-6: reflexivity.
      all: eapply match_states_State; try (eassumption || reflexivity).
      all: try (inv H4; now apply match_env_set_primitive).
      all: try (rewrite <- transl_value_primitive by assumption; now inv H4).
      all: intros j v' t' He' Hte'; case (Pos.eqb_spec i j) as [<-|Hneq];
        [rewrite PTree.gss in He'; injection He' as <-; now case v, t'
        |rewrite -> PTree.gso in He' by lia; now apply MME].
    + have {H5} - H5: update_env_path e (i, sempl) (shrink t v) = Some e'.
      { move: H5 => /=. inv H11. by move: H5; case synpl'. easy. }
      simpl in H3.
      destruct (B.fn_penv f)!i eqn:Hpe; try discriminate.
      injection H3 as Hperm. simpl in TS.
      have H := eval_path_elem_list_full_cut_on_last _ _ _ _ _ _ Hlen H11.
      destruct H as (sempl' & llsz' & seme & lsz & -> & -> & Esynpl & Esyne).
      pose proof H5 as UPDE.
      simpl in H5. destruct e!i eqn:He; try discriminate. simpl in H5.
      destruct update_value eqn:UPDV; try discriminate.
      revert H5; intros [= <-].
      pose proof (eval_path_elem_list_app_llsz _ _ _ _ [lsz] _ Esynpl) as Esynpl'.
      rewrite -> transl_syn_path_elem_list_app with (1 := Esynpl) in Tp0.
      destruct transl_syn_path_elem_list eqn:Tsynpl; try discriminate.
      destruct get_type_path eqn:Ht; try discriminate.
      cbn -[transl_syn_path_elem_list] in Tp0.
      simpl in Hlen. rewrite 2!app_length in Hlen. simpl in Hlen.
      rewrite 2!Nat.add_1_r in Hlen. apply eq_add_S in Hlen.
      rewrite Hlen skipn_all in Tp0. simpl in Tp0.
      destruct t1; try discriminate.
      destruct transl_syn_path_elem eqn:Tsyne; try discriminate.
      revert Tp0; intros [= <-].
      apply transl_syn_path_elem_list_app_llsz_useless
        with (llsz2 := [lsz]) in Tsynpl.
      simpl in H0. rewrite Hte in H0. simpl in H0. pose proof H0 as Ht'.
      apply get_type_path_app in H0. rewrite Ht in H0.
      destruct H0 as [? [T1 T2]]. injection T1 as <-.
      destruct e3; try discriminate.
      { destruct syne as [idx]. rewrite transl_Scell in Tsyne.
        destruct transl_exprlist; try discriminate; simpl in Tsyne.
        destruct build_index_expr; discriminate. }
      destruct syne. pose proof Tsyne as Tsyne'.
      rewrite transl_Scell in Tsyne.
      destruct transl_exprlist eqn:Tlidx; try discriminate. simpl in Tsyne.
      destruct build_index_expr eqn:Bidx; try discriminate.
      revert Tsyne TS; intros [= <- <-] [= <-].
      pose proof Esyne as Esyne'. inv Esyne'. injection T2 as <-.
      case (update_value_get_value_path' _ _ _ _ UPDV) as [? GETV].
      apply get_value_path_app in GETV as [? [GETV' _]].
      specialize transl_syn_path_sem_preservation
        with (synp := (i, synpl')) (semp := (i, sempl')) (v := x0) (ce := e2)
             (1 := TPROG) (2 := ME) (3 := MME) (4 := TF) (8 := NOHID).
      simpl. rewrite H8 Hte He. simpl. intro Ee2.
      assert (Bsem.eval_path e f (i, synpl') (i, sempl')) as EV
        by now apply (Bsem.eval_path_intro _ _ _ _ (llsz' ++ [lsz]) _).
      specialize (Ee2 Tsynpl EV GETV').
      case (update_value_Pcell _ _ _ sempl' _ [] UPDV) as [lv [lv' [Hlv Hlv']]].
      rewrite Hlv in GETV'. injection GETV' as <-.
      specialize (match_mem_env_path _ _ _ _ _ NOHID MME (i, sempl') (Varr lv) (Tarr t1)).
      simpl. rewrite He Hte. simpl. intro H. specialize (H Hlv Ht).
      destruct (tr n (i, sempl')) as [[[blk ofs] [|p']]|] eqn:Htr; try contradiction.
      2: now specialize (NOHID (i, sempl') blk ofs p').
      assert (idx < length lv)%nat as Hidx.
      { apply update_value_get_value_path' in UPDV as [x' Hx'].
        apply get_value_path_app in Hx'. rewrite Hlv in Hx'.
        destruct Hx' as [? [[= <-] Hx']]. simpl in Hx'.
        destruct nth_error eqn:Hnth in Hx'; try discriminate.
        apply nth_error_Some. now rewrite Hnth. }
      specialize (proj2 H _ Hidx) as (_ & VALID & _ & _).
      move/Mem.valid_access_implies => /(_ _ (perm_F_any Writable)) in VALID.
      case (Mem.valid_access_store _ _ _ _ (transl_value tr n None v) VALID)
        as [tm' Htm'].
      assert (eval_expr (Genv.globalenv tp) PTree.Empty te tm
                (Ebinop Oaddl e2
                   (Ebinop Omull (Econst (Olongconst (Int64.repr (typ_sizeof t1))))
                      e0))
                (Vptr blk (Ptrofs.add ofs
                             (Ptrofs.mul (Ptrofs.repr (typ_sizeof t1))
                                (Ptrofs.repr (Z.of_nat idx)))))) as EVidx.
      { eapply eval_Ebinop. exact Ee2.
        eapply eval_Ebinop. now apply eval_Econst.
        eapply eval_build_index_expr; try eassumption.
        { generalize l as lidx'. induction lidx'. easy.
          intros * [->|Hin]; eauto. intros Texp0 EVexp0.
          eapply transl_expr_sem_preservation; eassumption. }
        reflexivity. simpl. rewrite Htr. simpl. change Archi.ptr64 with true.
        simpl. erewrite <- Ptrofs.agree64_of_int_eq with (a := Ptrofs.mul _ _).
        2: apply Ptrofs.agree64_mul; try easy; now apply Ptrofs.agree64_repr.
        reflexivity. }
      left; eexists. split. apply plus_one. eapply step_store; try eassumption.
      eapply transl_expr_sem_preservation; eassumption.
      eapply match_states_State; try eassumption.
      * reflexivity.
      * destruct sempl' as [|[] ?].
        ** revert Hlv Hlv'; intros [= ->] [= ->].
           intros i' v'. case (Pos.eqb_spec i' i) as [->|Hneq].
           { rewrite PTree.gss. specialize (ME _ _ He). now intros [= <-]. }
           { rewrite PTree.gso. easy. now apply ME. }
        ** simpl in Hlv, Hlv'. destruct v0, v1; try discriminate.
           intros i' v'. case (Pos.eqb_spec i' i) as [->|Hneq].
           { rewrite PTree.gss. specialize (ME _ _ He). now intros [= <-]. }
           { rewrite PTree.gso. easy. now apply ME. }
      * move=>> /VBLKS/[apply]. by rewrite (Mem.nextblock_store _ _ _ _ _ _ Htm').
      * eapply match_mem_env_assign_non_empty_path with (tp := tp); eassumption.
      * eapply match_mem_cont_lt_N_assign; try eassumption. reflexivity.
  (* Salloc, normal case *)
  - edestruct (proj1 (Genv.find_def_symbol tp (ident_calloc fids) (AST.Gfun (AST.External calloc))))
           as [b [Hfsymb Hfdef]].
    { unfold transl_program' in TPROG. monadInv TPROG.
      unfold prog_defmap. simpl. apply PTree_Properties.of_list_norepet.
      rewrite map_app. change PTree.elt with ident.
      rewrite <- (transl_fundefs_eq_map_fst _ _ _ EQ), globdefs_list_fst.
      apply Coqlib.list_norepet_append; try easy. exact (B.prog_nodup p).
      now apply Coqlib.list_disjoint_sym. apply in_or_app. left.
      exact (in_map (fun def => (fst def, AST.Gfun (V := unit) (snd def))) _ _ Hcalloc). }
    inversion_clear MSTATE. move: TS => /=. rewrite H H0 H1 /=.
    case Tsz: transl_expr => [tsz|] //= [<-].
    have ? := typ_sizeof_bounds t.
    have:= calloc_sem_success_exists (typ_to_memory_chunk t)
      (Ptrofs.repr (typ_sizeof t)) (Ptrofs.of_int64 n) tm.
    case=> [| | b' [m'] [m'']].
    { rewrite -size_chunk_typ_sizeof; by case typ_to_memory_chunk. }
    { move: H5. rewrite Ptrofs.unsigned_repr.
      change Ptrofs.max_unsigned with 18446744073709551615; lia.
      rewrite/Ptrofs.of_int64 Ptrofs.unsigned_repr //.
      exact: Int64.unsigned_range_2. }
    intros (ALLOC & LSZ & VSZ & ZERO & I).
    left; eexists. split. eapply plus_nine.
    + eapply step_seq.
    + apply step_set. eapply transl_expr_sem_preservation; eassumption.
    + apply step_skip_seq.
    + apply step_seq.
    + eapply step_call;
      [ apply eval_Eaddrof, eval_var_addr_global => //; exact Hfsymb
      | | unfold Genv.find_funct; destruct Ptrofs.eq_dec; try easy;
        unfold Genv.find_funct_ptr; by rewrite Hfdef
      | reflexivity ].
      apply eval_Econs.
      have:= eval_expr_size_assign_preservation e _ _ _ _ H0 H1.
      case/Forall2_forall => _ /(_ 0%nat _ _ eq_refl eq_refl).
      case/Forall2_forall => _ /(_ 0%nat _ _ eq_refl eq_refl _ (Vint64 n) H4).
      eapply transl_expr_sem_preservation; try eassumption.
      by apply match_env_set. by apply match_mem_env_set.
      apply eval_Econs. by apply eval_Econst. apply eval_Enil.
    + apply step_external_function. rewrite calloc_sem_extcall /=.
      have -> : Vlong n = Vptrofs (Ptrofs.of_int64 n).
      { rewrite/Vptrofs; change Archi.ptr64 with true => /=.
        by rewrite Ptrofs.to_int64_of_int64. }
      have -> : Vlong (Int64.repr (typ_sizeof t))
                  = Vptrofs (Ptrofs.repr (typ_sizeof t)).
      { rewrite/Vptrofs/Ptrofs.to_int64; change Archi.ptr64 with true => /=.
        rewrite Ptrofs.unsigned_repr //.
        change Ptrofs.max_unsigned with 18446744073709551615; lia. }
      eapply calloc_sem_success; try eassumption.
      * rewrite -size_chunk_typ_sizeof. by case typ_to_memory_chunk.
      * rewrite/Ptrofs.mul/Ptrofs.of_int64. rewrite Ptrofs.unsigned_repr //.
        rewrite !Ptrofs.unsigned_repr.
        all: change Ptrofs.max_unsigned with Int64.max_unsigned.
        change Int64.max_unsigned with 18446744073709551615; lia.
        exact: Int64.unsigned_range_2.
        have:= Int64.unsigned_range n; lia.
    + apply step_return.
    + apply step_skip_seq.
    + eapply step_ifthenelse.
      eapply eval_Ebinop. apply eval_Evar => /=. by rewrite PTree.gss.
      by apply eval_Econst.
      cbn; rewrite/Val.cmplu /=; change Archi.ptr64 with true => /=.
      have: Mem.valid_pointer m'' b' (-1).
      { apply Mem.valid_pointer_valid_access.
        have [R _]:= Mem.load_valid_access _ _ _ _ _ LSZ. rewrite/Mem.valid_access.
        split=> //. 2: now apply Z.divide_opp_r.
        move=> j /= ?. have -> : j = -1 by lia.
        apply Mem.perm_implies with (p1 := Readable); [|econstructor].
        apply: R; change Mptr with Mint64; easy. }
      change (Ptrofs.unsigned Ptrofs.zero) with 0 => /=.
      rewrite Int64.eq_true => ->; by rewrite orbT. econstructor.
    + reflexivity.
    + rewrite Int.eq_true /=.
      eapply match_states_State with (tr := tr_alloc tr n0 i b') (n := n0);
        try (eassumption || reflexivity).
      * apply wf_trmem_alloc with (1 := WF). destruct n0 as [|n0]. by left.
        right => /=. move/valid_trmem_cont_destructCont: VK.
        move/match_mem_cont_destructCont: MMK.
        have:= destructCont_is_Kreturnto_or_Kstop k.
        case (Bsem.destructCont k) => // ? e0 f0 marrs ? _ _.
        intros (_ & HM & _ & _ & _ & _ & MLIST & _).
        move=> [i0 l0] b0 o0 l0' T.
        have:= mutable_list_hidden_is_mut _ _ _ _ HM MLIST _ _ _ _ _ T.
        by rewrite H2.
      * eapply visible_separated_alloc; try eassumption; lia.
      * by apply owned_at_offset_zero_alloc.
      * eapply valid_trmem_cont_alloc; eauto.
      * by apply match_cont_alloc.
      * apply match_env_set_alloc; by apply match_env_set_primitive.
      * by apply no_hidden_alloc.
      * have X := valid_blocks_alloc _ _ _ _ _ _ _ i VBLKS ALLOC.
        move=> ? [j l] >. have:= Mem.unchanged_on_nextblock _ _ _ I.
        rewrite/Coqlib.Ple /=.
        have V := Mem.nextblock_alloc _ _ _ _ _ ALLOC.
        case Nat.eqb_spec => [<-|] /=. case Pos.eqb_spec => [<-|] /=.
        case (list_reflect _ sem_path_elem_reflect) => _.
        ** move=> + _ [<- _ _]; rewrite (Mem.alloc_result _ _ _ _ _ ALLOC); lia.
        ** move=> + /VBLKS/[apply]; lia.
        ** move=> ++ /VBLKS/[apply]; lia.
        ** move=> ++ /VBLKS/[apply]; lia.
      * eapply match_mem_env_alloc_N; try eassumption.
        change Ptrofs.max_unsigned with Int64.max_unsigned; lia.
        by apply match_mem_env_set.
      * eapply match_mem_cont_alloc; try eassumption; lia.
  (* Salloc, overflow case *)
  - edestruct (proj1 (Genv.find_def_symbol tp (ident_calloc fids) (AST.Gfun (AST.External calloc))))
           as [b [Hfsymb Hfdef]].
    { unfold transl_program' in TPROG. monadInv TPROG.
      unfold prog_defmap. simpl. apply PTree_Properties.of_list_norepet.
      rewrite map_app. change PTree.elt with ident.
      rewrite <- (transl_fundefs_eq_map_fst _ _ _ EQ), globdefs_list_fst.
      apply Coqlib.list_norepet_append; try easy. exact (B.prog_nodup p).
      now apply Coqlib.list_disjoint_sym. apply in_or_app. left.
      exact (in_map (fun def => (fst def, AST.Gfun (V := unit) (snd def))) _ _ Hcalloc). }
    inversion_clear MSTATE. move: TS => /=. rewrite H H0 H1 /=.
    case Tsz: transl_expr => [tsz|] //= [<-].
    have ? := typ_sizeof_bounds t.
    left; eexists. split. eapply plus_nine.
    + eapply step_seq.
    + apply step_set. eapply transl_expr_sem_preservation; eassumption.
    + apply step_skip_seq.
    + apply step_seq.
    + eapply step_call;
      [ apply eval_Eaddrof, eval_var_addr_global => //; exact Hfsymb
      | | unfold Genv.find_funct; destruct Ptrofs.eq_dec; try easy;
        unfold Genv.find_funct_ptr; by rewrite Hfdef
      | reflexivity ].
      apply eval_Econs.
      have:= eval_expr_size_assign_preservation e _ _ _ _ H0 H1.
      case/Forall2_forall => _ /(_ 0%nat _ _ eq_refl eq_refl).
      case/Forall2_forall => _ /(_ 0%nat _ _ eq_refl eq_refl _ (Vint64 n) H3).
      eapply transl_expr_sem_preservation; try eassumption.
      by apply match_env_set. by apply match_mem_env_set.
      apply eval_Econs. by apply eval_Econst. apply eval_Enil.
    + apply step_external_function. rewrite calloc_sem_extcall /=.
      have -> : Vlong n = Vptrofs (Ptrofs.of_int64 n).
      { rewrite/Vptrofs; change Archi.ptr64 with true => /=.
        by rewrite Ptrofs.to_int64_of_int64. }
      have -> : Vlong (Int64.repr (typ_sizeof t))
                  = Vptrofs (Ptrofs.repr (typ_sizeof t)).
      { rewrite/Vptrofs/Ptrofs.to_int64; change Archi.ptr64 with true => /=.
        rewrite Ptrofs.unsigned_repr //.
        change Ptrofs.max_unsigned with 18446744073709551615; lia. }
      eapply calloc_sem_overflow. rewrite/Ptrofs.of_int64.
      rewrite !Ptrofs.unsigned_repr.
      all: change Ptrofs.max_unsigned with Int64.max_unsigned.
      2: by apply Int64.unsigned_range_2.
      change Int64.max_unsigned with 18446744073709551615; lia.
      lia.
    + apply step_return.
    + apply step_skip_seq.
    + eapply step_ifthenelse.
      eapply eval_Ebinop. apply eval_Evar => /=. by rewrite PTree.gss.
      by apply eval_Econst. reflexivity.
      simpl. rewrite Int64.eq_true; econstructor.
    + reflexivity.
    + rewrite Int.eq_false //=.
      eapply match_states_Serror; eassumption || reflexivity.
  (* Sfree *)
  - inversion_clear MSTATE. move: TS => /=; rewrite H H0 /= => - [<-].
    have:= MME i _ _ H3 H => /=.
    case Htr: tr => [[[b o] []]|] //. 2: by move/NOHID in Htr.
    case=> - [LSZ VSZ] X. have:= OWNZ _ _ _ _ _ Htr H2; intros ->.
    have [m' Fm]: exists m', Mem.free tm b (- size_chunk Mptr)
            (typ_sizeof t * Z.of_nat (length lv)) = Some m'.
    { case (Mem.range_perm_free tm b (- size_chunk Mptr)
                (typ_sizeof t * Z.of_nat (length lv))); eauto.
      move=> j Hj. case (Z.lt_ge_cases j 0) => Zj.
      - have: - size_chunk Mptr <= j < 0 by lia. exact: (proj1 VSZ).
      - have Bt := typ_sizeof_bounds t. have: 0 < typ_sizeof t by lia.
        case: Hj => _ /Z.div_lt_upper_bound/[apply] Hj.
        have Pj: 0 <= j / typ_sizeof t by apply Z.div_pos; lia.
        have Hidx: (Z.to_nat (j / typ_sizeof t) < length lv)%nat.
        { move/Znat.Z2Nat.inj_lt in Hj. rewrite Znat.Nat2Z.id in Hj. lia. }
        have:= X _ Hidx. intros (Hofs & V & _).
        rewrite/Ptrofs.add/Ptrofs.mul in V.
        change (Ptrofs.unsigned Ptrofs.zero) with 0 in *. apply (proj1 V).
        have Hidx': j / typ_sizeof t <= Ptrofs.max_unsigned by nia.
        rewrite Znat.Z2Nat.id //. rewrite/Ptrofs.add/Ptrofs.mul.
        have M: Ptrofs.max_unsigned = 18446744073709551615 by reflexivity.
        rewrite (Ptrofs.unsigned_repr (typ_sizeof _)); [lia|].
        rewrite (Ptrofs.unsigned_repr (_ / _)); [lia|].
        rewrite (Ptrofs.unsigned_repr (_ * _)); [lia|].
        rewrite Ptrofs.unsigned_repr; [lia|]. simpl.
        rewrite (size_chunk_typ_sizeof t).
        split. apply Z.mul_div_le; lia.
        rewrite -Z.mul_succ_r; apply Z.mul_succ_div_gt; lia. }
    left; eexists; split. eapply plus_one.
    + eapply step_builtin.
      apply eval_Econs; [|apply eval_Enil]. apply eval_Evar. exact (ME i _ H3).
      simpl. rewrite Htr. have Bt := typ_sizeof_bounds t.
      have M: Ptrofs.max_unsigned = 18446744073709551615 by reflexivity.
      have?: typ_sizeof t * Z.of_nat (length lv) <= Ptrofs.max_unsigned.
      { case: (length lv) X => [|x] //. lia.
        move/(_ x (Nat.lt_succ_diag_r _)).
        case=> + _. change (Ptrofs.unsigned Ptrofs.zero) with 0. nia. }
      have LEN: Z.of_nat (length lv) <= Ptrofs.max_unsigned by nia.
      eapply extcall_free_sem_ptr. exact LSZ.
      change (Ptrofs.unsigned Ptrofs.zero) with 0 => /=.
      rewrite/Ptrofs.mul.
      rewrite (Ptrofs.unsigned_repr (typ_sizeof _)); [lia|].
      rewrite (Ptrofs.unsigned_repr (Z.of_nat _)); [lia|].
      rewrite Ptrofs.unsigned_repr. lia. exact Fm.
    + simpl. eapply match_states_State with (tr := tr_free tr n i) (n := n);
        try (eassumption || reflexivity).
      * apply wf_trmem_free with (1 := WF). destruct n as [|n]. by left.
        right => /=. move/valid_trmem_cont_destructCont: VK.
        move/match_mem_cont_destructCont: MMK.
        have:= destructCont_is_Kreturnto_or_Kstop k.
        case (Bsem.destructCont k) => // ? e0 f0 marrs ? _ _.
        intros (_ & HM & _ & _ & _ & _ & MLIST & _).
        move=> [i0 l0] b0 o0 l0' T.
        have:= mutable_list_hidden_is_mut _ _ _ _ HM MLIST _ _ _ _ _ T.
        by rewrite H2.
      * by apply visible_separated_free.
      * by apply owned_at_offset_zero_free.
      * by apply valid_trmem_cont_free.
      * by apply match_cont_free.
      * by apply match_env_free.
      * by apply no_hidden_free.
      * eapply valid_blocks_free; eassumption.
      * eapply match_mem_env_free_N; eassumption.
      * eapply match_mem_cont_free; try eassumption. lia.
  (* Scall, internal function *)
  - inversion_clear MSTATE. simpl in TS. rewrite H in TS. simpl in TS.
    set (mut := mut_args _ _) in *. set (own := own_args _ _) in *.
    set (shr := shr_args (Syntax.fn_penv f') pp) in *.
    apply transl_program'_genv_preservation
      with (1 := Hnorepet) (2 := Hdisjoint) (3 := TPROG) in H
        as [loc [g [Hloc [Hfd Htfd]]]].
    destruct transl_syn_path_list eqn:TRargs; try discriminate.
    revert TS; intros [= <-].
    pose proof (B.fn_tenv_sig f'). simpl in *. fold transl_function in Htfd.
    destruct transl_function eqn:Htf; try discriminate. revert Htfd; intros [= <-].
    pose proof (transl_syn_path_list_sem_preservation tr n hfuncs Habort Hcalloc p tp tm
                  e f te tf args pargs vargs l TPROG ME MME TF TRargs H0 H1 NOHID).
    pose proof Htf as Htf'.
    unfold transl_function, BtoCSharpMinor.transl_function in Htf.
    destruct size_vars_assignments; try discriminate.
    destruct BtoCSharpMinor.transl_stmt; try discriminate.
    revert Htf; intros [= <-].
    left; eexists. split. apply plus_one. eapply step_call.
    apply eval_Eaddrof. apply eval_var_addr_global; easy || eassumption.
    eassumption. eassumption. reflexivity.
    assert (Coqlib.list_norepet mut) as NODUP.
    { apply Coqlib_list_norepet_filter.
      exact (pargs_params_no_repet_params _ _ _ (B.fn_nodup_params f') H3). }
    pose proof (mut_args_no_repet_params_snd' _ _ _ (B.fn_nodup_params f') H3)
            as NRSND.
    assert (forall p q i j, In (p, i) (mut ++ shr ++ own) ->
                            In (q, j) (mut ++ shr ++ own) ->
                            (p, i) <> (q, j) -> i <> j) as NRSND'.
    { intros r i q j Hin Hin'. apply NRSND.
      all: apply (@incl_app _ mut (shr ++ own) _ (mut_args_incl _ _)).
      1,3: apply (@incl_app _ shr own _ (shr_args_incl _ _) (own_args_incl _ _)).
      all: assumption. }
    assert (length (B.fn_params f') = length vargs) as Hlen.
    { rewrite <- get_env_path_list_length with (1 := H1).
      rewrite <- eval_path_list_length with (1 := H0).
      rewrite H7. exact (proj1 (B.fn_tenv_sig f')). }
    apply map2_Some with (f := fun (i : ident) (v : value) =>
                                 (Some (i, []: list sem_path_elem), v))
                      in Hlen as [pargs' Hmap].
    eapply match_states_Callstate
      with (n := S n) (tr := tr_call tr n mut shr own); try eassumption.
    + apply wf_trmem_tr_call with (pe := B.fn_penv f).
      * exact WF.
      * exact NOHID.
      * eauto.
      * intros * Hin Hin'. now apply (pargs_params_aliasing_spec' _ _ _ _ H3 H11).
      * apply all_root_paths_spec with (1 := H10).
      * intros n' Hn'. apply (hidden_is_mut_Sn _ _ _ k tm); by rewrite -Hn'.
      * move=> i x j /filter_In.
        case P: (B.fn_penv f')!j => [z|] [Hin Heq] //.
        have:= proj1 (forallb_forall _ _) H2 _ Hin.
        move/B.perm_reflect in Heq. rewrite -Heq in P. rewrite P.
        case (B.fn_penv f)!i => //= - [] //.
    + apply visible_separated_tr_call_SN with (pe := B.fn_penv f); try assumption.
      * intros * Hin1 Hin2. specialize (NRSND' _ _ _ _ Hin1 Hin2).
        case (eqSemPath p0 p') as [->|Hneq]. easy.
        exfalso. apply NRSND'. congruence. easy.
      * intros * I1 I2.
        now apply pargs_params_aliasing_spec' with (1 := H3) (2 := H11).
      * intros * Hin Hpe'.
        assert (In (ic, lc, i) pp) as Hin'.
        { case/in_app_iff: Hin. exact: mut_args_incl.
          case/in_app_iff. exact: shr_args_incl. exact: own_args_incl. }
        pose proof (proj1 (forallb_forall _ _) H2 _ Hin') as T.
        simpl in T. destruct (B.fn_penv f)!ic; try discriminate.
        rewrite Hpe' in T. eauto.
      * intros * Hin.
        apply in_app_or in Hin as [Hin|Hin];
          pose proof (proj1 (filter_In _ _ _) Hin) as [_ T];
          case (B.fn_penv f')!i as [[]|]; discriminate || eauto.
      * intros * Hin. have Hin': In (p0, i) pp.
        { case/in_app_iff: Hin. exact: mut_args_incl.
          case/in_app_iff. exact: shr_args_incl. exact: own_args_incl. }
        case x => // Hpe _; apply in_app_iff.
        left.  apply filter_In. split. easy. now rewrite Hpe.
        right. apply filter_In. split. easy. now rewrite Hpe.
    + eapply owned_at_offset_zero_tr_call; eassumption.
    + apply valid_trmem_cont_tr_call with (pp := pp); try easy.
      * intros q i q' i' I1 I2 N.
        apply (@or_introl _ (In _ own)), in_or_app in I1.
        apply (@or_introl _ (In _ (shr ++ own))), in_or_app in I2.
        specialize (pargs_params_aliasing_spec' _ _ _ _ H3 H11 _ _ _ _ I1 I2 N).
        unfold sem_path_alias, sem_path_prefix.
        case q, q'. case Pos.eqb; [|easy]. case prefix; easy.
      * apply mut_args_no_repet_params_snd with (1 := B.fn_nodup_params f') (2 := H3).
      * move=>>. case/in_app_iff. by apply mut_args_incl.
        case/in_app_iff. by apply shr_args_incl. by apply own_args_incl.
      * intros * Hin1 Hin2. specialize (NRSND' _ _ _ _ Hin1 Hin2).
        case (eqSemPath p0 p') as [->|Hneq]. easy.
        exfalso. apply NRSND'. congruence. easy.
      * intros * Hin. pose proof (proj1 (filter_In _ _ _) Hin) as [_ T].
        now destruct (B.fn_penv f')!i as [[]|].
      * move=> q j /filter_In. case=> _. case (B.fn_penv f')!j =>> //.
        by move/(B.perm_reflect) <-.
      * apply forallb_forall => x Hin. apply (proj1 (forallb_forall _ _) H2).
        case/in_app_iff: Hin. exact: mut_args_incl.
          case/in_app_iff. exact: shr_args_incl. exact: own_args_incl.
    + easy.
    + intros * Hnthv Hnthi Ht.
      case (get_env_path_list_nth_error _ _ _ _ _ H1 Hnthv) as [[ic lc] [Hnthp He]].
      pose proof (args_params_nth_error_In _ _ _ _ _ _ H3 Hnthp Hnthi) as Hin.
      apply (match_mem_value_eq tm tr _ n _ _ _ ic lc).
      { intros l'. rewrite app_nil_l. unfold tr_call.
        apply (incl_pp_filter_penv (B.Internal f') _ _ H3) in Hin.
        rewrite Nat.eqb_refl. case find as [[[]]|] eqn:Hfind.
        2: apply find_none with (2 := Hin) in Hfind;
            now rewrite Pos.eqb_refl in Hfind.
        apply find_some in Hfind as [Hin' Hp]. apply Pos.eqb_eq in Hp as ->.
        assert ((i0, l0) = (ic, lc)) as [= -> ->]; [|easy].
        { case (eqSemPath (i0, l0) (ic, lc)) as [->|Hneq]. easy.
          elim (NRSND' _ _ _ _ Hin Hin'); congruence. } }
      pose proof (proj1 (forallb_forall _ _) H8 _ Hin) as T.
      cbn -[get_tenv_path] in T.
      destruct (get_tenv_path (B.fn_tenv f) (ic, lc)) eqn:Hte; try discriminate.
      simpl in T. rewrite Ht in T. simpl in T. apply typ_dec_bl in T as ->.
      apply (match_mem_env_path _ _ _ _ _ NOHID MME _ _ _ He Hte).
    + apply transl_valuelist_tr_call' with (4 := Hmap).
      exact WF.
      apply List.incl_tran with (2 := incl_pp_filter_penv (B.Internal f') _ _ H3).
      apply args_params_incl with (1 := H3). exact NRSND'.
      exact (get_env_path_list_length _ _ _ H1).
    + now apply valid_blocks_tr_call.
    + now apply match_cont_tr_call.
    + now apply tr_call_no_hidden.
    + now apply match_mem_cont_tr_call.
  (* Scall, external function *)
  - inversion_clear MSTATE. simpl in TS.
    rewrite H in TS. apply transl_program'_genv_preservation
                      with (1 := Hnorepet) (2 := Hdisjoint) (3 := TPROG) in H.
    destruct H as [loc [g [Hloc [Hfd Htfd]]]].
    destruct transl_syn_path_list eqn:TRargs; try discriminate.
    revert TS; intros [= <-]. simpl in *. revert Htfd; intros [= <-].
    pose proof (transl_syn_path_list_sem_preservation tr n hfuncs Habort Hcalloc p tp tm
                  e f te tf args pargs vargs l TPROG ME MME TF TRargs H0 H1 NOHID).
    case external_call_sem_preservation with (1 := TPROG) (2 := H2)
                                             (3 := MME) (4 := MMK) (5 := ME)
                                             (6 := VBLKS) (7 := H10) (8 := H12)
                                          as (tm' & MME' & MMK' & ME' & VBLKS' & EC).
    left; eexists. split. eapply plus_three. eapply step_call.
    apply eval_Eaddrof. apply eval_var_addr_global; easy || eassumption.
    eassumption. eassumption. reflexivity.
    apply step_external_function. exact EC.
    apply step_return. reflexivity.
    eapply match_states_State; try eassumption. reflexivity.
    { destruct idvar; simpl.
      + apply match_env_set_primitive => //. by apply H11.
      + exact ME'. }
    { destruct idvar as [idvar|]; simpl; try easy.
      assert (primitive_value r = true) by now apply H11.
      intros i v t He Hte. case (Pos.eqb_spec i idvar) as [<-|Hneq].
      rewrite PTree.gss in He. injection He as <-.
      destruct r; destruct t; easy.
      rewrite -> PTree.gso in He by easy. now specialize (MME' i v t He Hte). }
  (* Callstate *)
  - inversion_clear MSTATE. pose proof TF as TF'.
    unfold transl_function, BtoCSharpMinor.transl_function in TF'.
    destruct size_vars_assignments eqn:SZassigns; try discriminate.
    destruct BtoCSharpMinor.transl_stmt eqn:Htbody; try discriminate.
    case match_env_build_preservation with (tr := tr) (n := n)
                                           (1 := B.fn_nodup_params f) (2 := H).
    intros te [Hte ME0].
    have MME0: match_mem_env tr n (B.fn_tenv f) e tm.
    { intros i v t He Ht.
      assert (v = Vundef \/ v <> Vundef) as [->|Hneq]
          by (case v; (now left) || now right).
      case t; easy.
      case (build_func_env_not_Vundef _ _ _ _ _ _ H He Hneq) as [|[j Hnth]]. easy.
      assert (j < length vargs)%nat as Hj.
      { rewrite <- (proj1 (map2_Some _ _ _) (ex_intro _ pargs PARGS)).
        apply nth_error_Some. now rewrite Hnth. }
      apply nth_error_Some in Hj. case (nth_error vargs j) eqn:Hnth'; try easy.
      rewrite -> build_func_env_spec with (1 := B.fn_nodup_params f) (2 := H)
                                          (3 := Hnth) (4 := Hnth') in He.
      injection He as ->. now apply MARGS with (j := j). }
    have T: forall i : ident, ~ In i (B.fn_params f) -> e ! i = None.
    { move=> i NOTIN. case E: e!i => //=. elim NOTIN.
      case (build_func_env_spec' _ _ _ _ _ _ H E) => //.
      case=> j [N _]. by apply: nth_error_In N. }
    case size_vars_assignments_correct with (1 := TPROG) (2 := TF)  (3 := NOHID)
                                            (4 := T)     (5 := ME0) (6 := MME0)
                                            (7 := H0)    (8 := SZassigns)
                                            (tp := tp)   (tf := tf) (tm := tm)
                                            (s0 := s0)   (tk := tk).
    intros INCL [te' [step_assigns [ME' MME']]].
    revert TF'; intros [= <-].
    left; eexists. split. eapply plus_left' with (t1 := E0) (t2 := E0) => //.
    eapply step_internal_function
      with (e := PTree.Empty) (le := te) (m1 := tm).
    apply Coqlib.list_norepet_nil. apply B.fn_nodup_params.
    apply B.fn_disjoint. apply alloc_variables_nil. simpl. rewrite <- TARGS.
    rewrite -> map2_map_combine with (fa := id) (fb := id)
                                     (f := fun '(i, v) => (Some (i, []), v))
                                     (1 := PARGS).
    rewrite 2!map_id. exact Hte. simpl.
    eapply plus_left' with (t1 := E0) (t2 := E0) => //. apply step_seq.
    eapply plus_left' with (t1 := E0) (t2 := E0) => //. apply step_seq.
    eapply plus_left' with (t1 := E0) (t2 := E0) => //. apply step_goto.
    simpl. unfold AST.ident_eq. rewrite Coqlib.peq_true.
    rewrite Coqlib.peq_false //. exact (label_code_neq_error fids).
    have -> : call_cont tk = tk by destruct k, tk.
    exact step_assigns.
    eapply match_states_State with (tr := tr) (n := n); try eassumption.
    now apply match_cont_None_Some.
  (* Sreturn None *)
  - inversion_clear MSTATE. rewrite_equalities. left; eexists; split.
    + apply plus_one. apply step_return_0. reflexivity.
    + change Values.Vundef with (transl_value tr n None Vundef).
      eapply match_states_Returnstate with (tr := tr); try eassumption.
      reflexivity.
  (* Sreturn Some *)
  - inversion_clear MSTATE. simpl in TS.
    destruct (transl_expr _ _ exp) eqn:Texp; try discriminate.
    revert TS; intros [= <-].
    left; eexists; split.
    + apply plus_one. eapply step_return_1.
      eapply transl_expr_sem_preservation; eassumption.
      reflexivity.
    + eapply match_states_Returnstate with (tr := tr); try eassumption.
      reflexivity.
  (* Returnstate, Kseq *)
  - inversion_clear MSTATE. right. split=> /=. lia.
    case: tk MK =>> //= [? MK]. econstructor; eassumption.
  (* Returnstate, Kblock *)
  - inversion_clear MSTATE. right. split=> /=. lia.
    case: tk MK =>> //= MK. econstructor; eassumption.
  (* Returnstate, Kreturnto *)
  - inversion_clear MSTATE. destruct n; try contradiction.
    destruct tk; try contradiction. case MMK as [MME' MMK'].
    left; eexists. split. apply plus_one, step_return. destruct e0; try contradiction.
    destruct MK as [<- [TF [ME' MK]]].
    destruct VK as (VS' & HM & ODEF & MPRES & NOALIAS & NOREPET & MLIST & OWNZ' & VK).
    have HMUT := mutable_list_hidden_is_mut _ _ _ _ HM MLIST.
    eapply match_states_State with (tr := tr_return tr n) (n := n); try eassumption.
    + now apply wf_trmem_tr_return with (m := marrs).
    + now apply visible_separated_tr_return with (pe' := B.fn_penv callee).
    + apply owned_at_offset_zero_tr_return; eassumption.
    + now apply valid_trmem_cont_tr_return with (m := marrs).
    + reflexivity.
    + now apply match_cont_tr_return.
    + destruct optid; simpl. rewrite -TV.
      have ->: transl_value tr (S n) None v =
               transl_value (tr_return tr n) n None v by case v.
      apply match_env_set_primitive. easy.
      all: apply match_env_tr_return_N with (5 := H1); try eassumption.
      all: now rewrite pargs_params_no_aliasing_marrs_no_aliasing.
    + now apply tr_return_no_hidden.
    + by apply valid_blocks_tr_return.
    + assert (~pargs_params_aliasing (map fst marrs) marrs) as A.
      { now rewrite pargs_params_no_aliasing_marrs_no_aliasing. }
      assert (match_mem_env (tr_return tr n) n (B.fn_tenv f) e' tm) as ME''.
      { apply (match_mem_env_tr_return tr n marrs
                 (B.fn_tenv f) (B.fn_tenv callee) e ecallee e' tm); try eassumption.
        * intros i l i' l' Hin.
          case (get_env_path ecallee (i', l')) eqn:Hcallee.
          2: now apply (update_env_spec_2' _ _ _ _ A H1 _ _ _ l' Hin).
          simpl in Hcallee. case ecallee!i' eqn:Hcallee0; try discriminate.
          simpl in Hcallee.
          specialize update_env_spec_2 with (1 := A) (2 := H1) (3 := Hin)
                                            (4 := Hcallee0) as GET.
          simpl in *. destruct e'!i; try easy. simpl in *.
          apply get_value_path_app. eauto.
        * intros i v' He'. apply update_env_spec_3 with (1 := He') (2 := H1).
        * intros p' lv GET SUB. eapply update_env_spec_3'; eassumption.
        * apply update_env_unchanged with (1 := H1).
        * revert H0. generalize marrs. induction marrs0. easy.
          case a as [p' i']. simpl. intro T. apply andb_prop in T as [T M].
          intros * [[= <- <-]|Hin].
          ** destruct (B.fn_tenv callee)!i', get_tenv_path; try easy.
             now rewrite (typ_dec_bl _ _ T).
          ** now apply IHmarrs0.
        * revert H. generalize marrs. induction marrs0. easy.
          case a as [p' i']. simpl. intro T. apply andb_prop in T as [T M].
          intros * [[= <- <-]|Hin].
          ** destruct ecallee!i', get_env_path; try easy.
             destruct v1, v0; try easy. eauto. now destruct v0.
          ** now apply IHmarrs0. }
      destruct optid as [id|].
      * now apply match_mem_env_set.
      * exact ME''.
    + now apply match_mem_cont_tr_return.
  (* Sseq *)
  - inversion_clear MSTATE. simpl in TS.
    destruct (transl_stmt _ _ s1) eqn:Hs1, (transl_stmt _ _ s2) eqn:Hs2; try discriminate.
    revert TS; intros [= <-].
    left; eexists. split. apply plus_one, step_seq.
    eapply match_states_State; try eassumption.
    simpl. rewrite Hs2. easy.
  (* Sassert *)
  - inversion_clear MSTATE. simpl in TS.
    destruct transl_expr eqn:Hcond; try discriminate. revert TS; intros [= <-].
    left; eexists. split. eapply plus_one, step_ifthenelse with (b := b).
    eapply transl_expr_sem_preservation; eassumption. now destruct b.
    eapply match_states_State; try eassumption. now destruct b.
  (* Sifthenelse *)
  - inversion_clear MSTATE. simpl in TS.
    destruct (transl_stmt _ _ s1) eqn:Hs1,
             (transl_stmt _ _ s2) eqn:Hs2,
             transl_expr eqn:Hcond; try discriminate. simpl in TS.
    revert TS; intros [= <-].
    left; eexists. split. eapply plus_one, step_ifthenelse with (b := b).
    eapply transl_expr_sem_preservation; try eassumption. destruct b; easy.
    eapply match_states_State; try eassumption.
    destruct b; simpl; [rewrite Hs1|rewrite Hs2]; reflexivity.
  (* Sloop *)
  - inversion_clear MSTATE. simpl in TS.
    destruct (transl_stmt _ _ s0) eqn:Hs0; try discriminate.
    revert TS; intros [= <-].
    left; eexists. split. apply plus_one. apply step_loop.
    eapply match_states_State; try eassumption.
    simpl. rewrite Hs0. easy.
  (* Sblock *)
  - inversion_clear MSTATE. simpl in TS.
    destruct (transl_stmt _ _ s0) eqn:Hs0; try discriminate.
    revert TS; intros [= <-].
    left; eexists. split. apply plus_one. apply step_block.
    eapply match_states_State; eassumption.
  (* Sexit, Kseq *)
  - inversion_clear MSTATE. revert TS; intros [= <-].
    destruct tk; try contradiction. case MK as [TS MK].
    left; eexists. split. apply plus_one, step_exit_seq.
    eapply match_states_State; try eassumption. reflexivity.
  (* Sexit (S n), Kblock *)
  - inversion_clear MSTATE. revert TS; intros [= <-].
    simpl in MK. repeat destruct_match MK. subst.
    left; eexists. split. apply plus_one. apply step_exit_block_S.
    eapply match_states_State; try eassumption. reflexivity.
  (* Sexit O, Kblock *)
  - inversion_clear MSTATE. revert TS; intros [= <-].
    simpl in MK. repeat destruct_match MK. subst.
    left; eexists. split. apply plus_one. apply step_exit_block_0.
    eapply match_states_State; try eassumption. reflexivity.
  (* Serror *)
  - edestruct (proj1 (Genv.find_def_symbol tp (ident_abort fids) (AST.Gfun (AST.External abort))))
           as [b [Hfsymb Hfdef]].
    { unfold transl_program' in TPROG. monadInv TPROG.
      unfold prog_defmap. simpl. apply PTree_Properties.of_list_norepet.
      rewrite map_app. change PTree.elt with ident.
      rewrite <- (transl_fundefs_eq_map_fst _ _ _ EQ), globdefs_list_fst.
      apply Coqlib.list_norepet_append; try easy. exact (B.prog_nodup p).
      now apply Coqlib.list_disjoint_sym. apply in_or_app. left.
      exact (in_map (fun def => (fst def, AST.Gfun (V := unit) (snd def))) _ _ Habort). }
    inversion_clear MSTATE; revert TS; intros [= <-].
    all: move: TF => /[dup]; rewrite{1}/transl_function/BtoCSharpMinor.transl_function.
    all: case size_vars_assignments =>> //=.
    all: case BtoCSharpMinor.transl_stmt =>> //= [<-] /= TF.
    all: left; eexists; split.
    1,3: eapply plus_left' with (t1 := E0) (t2 := E0) => //; [eapply step_goto|].
    1,3: by rewrite/= /AST.ident_eq Coqlib.peq_true.
    1,2: eapply plus_left' with (t1 := E0) (t2 := E0) => //; [eapply step_seq|].
    1,2: eapply plus_four with (t1 := E0) (t2 := E0) (t3 := E0) (t4 := E0) => //.
    1,5: eapply step_call;
      [ apply eval_Eaddrof, eval_var_addr_global => //; exact Hfsymb
      | apply eval_Enil
      | unfold Genv.find_funct; destruct Ptrofs.eq_dec; try easy;
        unfold Genv.find_funct_ptr; by rewrite Hfdef
      | reflexivity ].
    1,4: apply step_external_function; apply abort_sem.
    1,3: apply step_return.
    1,2: apply step_skip_seq.
    all: eapply match_states_Serror; try eassumption; reflexivity.
Qed.

Lemma transl_fundefs_genv_public_symbol_add_globals:
  forall p ge ge1 ge2 l hfuncs id,
    transl_fundefs ge (B.prog_defs p) = OK l ->
    Genv.public_symbol ge1 id = Genv.public_symbol ge2 id  ->
    Genv.genv_public ge1 = Genv.genv_public ge2 ->
    (forall id, In id (map fst hfuncs) -> ~In id (Genv.genv_public ge1)) ->
    Genv.public_symbol
      (Genv.add_globals ge1 (hfuncs ++ l)) id =
    Genv.public_symbol
      (Genv.add_globals ge2 (Bsem.to_AST_globdef (B.prog_defs p))) id.
Proof.
  intros. unfold Genv.public_symbol. simpl.
  do 2 rewrite Genv.genv_public_add_globals.
  revert ge1 ge2 H0 H1 H2. induction hfuncs; simpl.
  - revert l H. induction (B.prog_defs p); simpl; intros.
    revert H; intros [= <-]. assumption.
    destruct a as [id' fd']. cbn in H. monadInv H. monadInv EQ.
    apply IHl with (ge1 := Genv.add_global ge1 (id', Gfun x1))
                   (ge2 := Genv.add_global ge2 (id', Gfun fd')) (1 := EQ1); auto.
    unfold Genv.public_symbol, Genv.find_symbol. simpl.
    do 2 rewrite PTree.gsspec. destruct (Coqlib.peq id id'). now rewrite H1. assumption.
  - intros. apply IHhfuncs with (ge1 := Genv.add_global ge1 a) (ge2 := ge2); auto.
    unfold Genv.public_symbol, Genv.find_symbol; simpl. rewrite PTree.gsspec.
    destruct (Coqlib.peq id (fst a)); [|assumption]. rewrite <- H1.
    specialize (H2 id (or_introl (In id (map fst hfuncs)) (eq_sym e))).
    destruct (in_dec AST.ident_eq id (Genv.genv_public ge1)). contradiction.
    now destruct ((Genv.genv_symb ge2) ! id).
Qed.

Lemma transl_fundefs_alloc_globals:
  forall p ge ge' l hfuncs m,
    transl_fundefs ge (B.prog_defs p) = OK l ->
    exists m',
      Genv.alloc_globals (Genv.add_globals ge' (globdefs_list hfuncs ++ l))
                         m (globdefs_list hfuncs ++ l) = Some m'.
Proof.
  intros p ge ge' l hfuncs. revert hfuncs ge' l. induction hfuncs; simpl.
  - induction (B.prog_defs p); simpl; intros. revert H; intros [= <-]. now exists m.
    destruct a as [id fd]. cbn in H. monadInv H. monadInv EQ.
    simpl. destruct Mem.alloc eqn:Halloc. pose proof (Mem.perm_alloc_2 _ _ _ _ _ Halloc).
    specialize H with (k := Cur). unfold Mem.drop_perm.
    apply decide_left with (decide := Mem.range_perm_dec m0 b 0 1 Cur Freeable). auto.
    intro. now apply IHl.
  - intros.
    specialize IHhfuncs with (1 := H) (ge' := Genv.add_global ge' (fst a, Gfun (snd a))).
    destruct a as [id def]. simpl.
    destruct Mem.alloc eqn:Halloc. pose proof (Mem.perm_alloc_2 _ _ _ _ _ Halloc).
    specialize H0 with (k := Cur). unfold Mem.drop_perm.
    apply decide_left with (decide := Mem.range_perm_dec m0 b 0 1 Cur Freeable). auto.
    intro. apply IHhfuncs.
Qed.

Theorem transl_program'_correct (hfuncs: list (ident * Csharpminor.fundef))
                                (Habort: In (ident_abort fids, AST.External abort) hfuncs)
                                (Hcalloc: In (ident_calloc fids, AST.External calloc) hfuncs)
                                (p: B.program):
  forall tp,
    Coqlib.list_norepet (map fst hfuncs) ->
    Coqlib.list_disjoint (map fst (B.prog_defs p)) (map fst hfuncs) ->
    transl_program' hfuncs Habort Hcalloc p = OK tp ->
    forward_simulation (Bsem.semantics p) (semantics tp).
Proof.
  intros tp Hnorepet Hdisjoint H.
  apply forward_simulation_star
   with (measure := measure)
        (match_states := match_states p hfuncs Habort Hcalloc); intros; simpl.
  - unfold transl_program', BtoCSharpMinor.transl_program' in H.
    destruct BtoCSharpMinor.transl_fundefs eqn:Htfds; try discriminate.
    revert H; intros [= <-]. unfold Genv.globalenv. simpl.
    apply transl_fundefs_genv_public_symbol_add_globals with (1 := Htfds).
    reflexivity. reflexivity. intros. simpl. intro. rewrite globdefs_list_fst in H.
    now apply (Hdisjoint id0 id0 H0 H).
  - pose proof H as TPROG. unfold transl_program', BtoCSharpMinor.transl_program' in H.
    destruct BtoCSharpMinor.transl_fundefs eqn:Htfds; try discriminate.
    revert H; intros [= <-]. simpl in H0. inversion H0.
    destruct (transl_fundefs_alloc_globals _ _
                (Genv.empty_genv fundef unit (map fst (B.prog_defs p)))
                _ hfuncs Mem.empty Htfds) as [tm Htm].
    destruct (transl_program'_genv_preservation _ _ _ _ _ _ _ Hnorepet Hdisjoint TPROG H)
          as [b [tf [Htf1 [Htf2 Htf3]]]].
    simpl in Htf3. destruct BtoCSharpMinor.transl_function eqn:Htf; try discriminate.
    revert Htf3; intros [= <-].
    have NoParams: B.fn_params f = [].
    { have [+ _] := B.fn_tenv_sig f. rewrite H1 /=. by apply length_zero_iff_nil. }
    eexists. split. eapply initial_state_intro; simpl.
    unfold Genv.init_mem, Genv.globalenv; simpl. eassumption.
    eassumption. eassumption.
    unfold BtoCSharpMinor.transl_function in Htf.
    unfold size_vars_assignments in Htf. rewrite NoParams in Htf. simpl in Htf.
    simpl. destruct BtoCSharpMinor.transl_stmt; try discriminate.
    revert Htf; intros [= <-]. now rewrite H1.
    eapply match_states_Callstate
      with (tr := fun n p => None) (n := 0%nat); try (eassumption || easy).
    + apply mk_wf_trmem; try easy. now intros ** [].
    + specialize (proj1 (B.fn_tenv_sig f)). rewrite H1. now case (B.fn_params f).
    + now intros [].
    + easy.
  - simpl in H1. inversion H1. rewrite_equalities.
    inv H0. destruct tk; try contradiction. simpl.
    apply final_state_intro.
  - case: H0 => ->.
    case/(transl_stmt_sem_preservation _ hfuncs Habort Hcalloc _ _ _ _
            Hnorepet Hdisjoint H H1) => [[> [STEP MATCH]]|[MEAS MATCH]].
    by left; eexists; split; eassumption. by right.
Qed.

End BtoCSharpMinorProof.

Theorem transl_program_correct (hfuncs: list (ident * Csharpminor.fundef))
                               (label_error label_code: ident)
                               (p: B.program):
  forall tp,
    transl_program hfuncs label_error label_code p = OK tp ->
    forward_simulation (Bsem.semantics p) (Csharpminor.semantics tp).
Proof.
  intros. unfold transl_program in H. do 7 destruct_match_noeq H.
  exact (transl_program'_correct _ hfuncs _ _ p tp l l0 H).
Qed.
