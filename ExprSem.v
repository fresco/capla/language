Require Import ssreflect ssrbool ssrfun.
Require Import List ZArith.
Import ListNotations.

Require Import BValues BEnv Syntax Ops SemPath.

Require Import Integers Maps.

Declare Scope outcome_scope.

Inductive outcome (A: Type) :=
| Val : A -> outcome A
| Err : outcome A
| Stuck : outcome A.

Arguments Val {_}.
Arguments Err {_}.
Arguments Stuck {_}.

Definition option_outcome {A: Type} (x: option A) : outcome A :=
  match x with
  | Some v => Val v
  | None => Stuck
  end.

Coercion option_outcome : option >-> outcome.

Definition option_outcome_err {A: Type} (x: option A) (y: outcome A) : outcome A :=
  match x with
  | Some v => Val v
  | None => y
  end.

Notation "A '|&' B" := (option_outcome_err A B) (at level 100) : outcome_scope.

Definition outcome_bind {A B: Type} (x: outcome A) (f: A -> outcome B) :=
  match x with
  | Val v => f v
  | Err => Err
  | Stuck => Stuck
  end.

Notation "'do' X <- A ; B" :=
  (outcome_bind A (fun X => B))
    (at level 200, X ident, A at level 100, B at level 200) : outcome_scope.

Local Open Scope outcome_scope.

Notation "A '|@2' L , M" :=
    (let __f := fix __f l m {struct m} :=
        match l, m with
        | _, [] => Val []
        | y :: l, z :: m => do v  <- A y z;
                            do lv <- __f l m;
                            Val (v :: lv)
        | _, _ => Stuck
        end in
        __f L M)
    (at level 100) : outcome_scope.

Notation "A '|@' L" :=
    (let __f := fix __f l :=
        match l with
        | [] => Val []
        | y :: l => do v  <- A y;
                    do lv <- __f l;
                    Val (v :: lv)
        end in
     __f L)
    (at level 100) : outcome_scope.

Local Close Scope outcome_scope.

Section EvalExpr.

Local Open Scope outcome_scope.

Variables (e: env) (f: function).

Fixpoint eval_ident_list (lids: list ident) :=
  match lids with
  | [] => Val []
  | x :: l =>
    match e!x with
    | Some v =>
      do llsz <- (fn_szenv' f)!x;
      do lv <- eval_ident_list l;
      match llsz with
      | [] => Val (v :: lv)
      | _  => Stuck
      end
    | None => Stuck
    end
  end.

Fixpoint valid_index (lidx: list value) (lsz: list value) : bool :=
  match lidx, lsz with
  | [], [] => true
  | Vint64 i :: lidx, Vint64 sz :: lsz =>
    (Int64.unsigned i <? Int64.unsigned sz)%Z && valid_index lidx lsz
  | _, _ => false
  end.

Fixpoint eval_expr (exp: expr) {struct exp} : outcome value :=
  match exp with
  | Econst (Cbool b)    => Val (Vbool b)
  | Econst (Cint _ _ n) => Val (Vint n)
  | Econst (Cint64 _ n) => Val (Vint64 n)
  | Econst (Cfloat32 f) => Val (Vfloat32 f)
  | Econst (Cfloat64 f) => Val (Vfloat64 f)
  | Ecast  exp t1 t2 =>
      do v <- eval_expr exp;
      sem_cast v t1 t2
      |& if cast_allowed t1 t2 && well_typed_value_bool t1 v
         then Err else Stuck
  | Eunop op k exp =>
      do v <- eval_expr exp;
      sem_unop op k v
  | Ebinop_arith op k exp1 exp2 =>
      do v1 <- eval_expr exp1;
      do v2 <- eval_expr exp2;
      sem_binarith_operation op k v1 v2
      |& if binarith_operation_allowed op k v1 v2
         then Err else Stuck
  | Ebinop_cmp op k exp1 exp2 =>
      do v1 <- eval_expr exp1;
      do v2 <- eval_expr exp2;
      sem_cmp_operation op k v1 v2
  | Ebinop_cmpu op k exp1 exp2 =>
      do v1 <- eval_expr exp1;
      do v2 <- eval_expr exp2;
      sem_cmpu_operation op k v1 v2
  | Eacc (i, l) =>
    do llsz <- (fn_szenv' f)!i;
    do l' <- eval_path_elem |@2 llsz, l;
    do v  <- get_env_path e (i, l');
    if primitive_value v then Val v
    else Stuck
  end
with eval_path_elem (lsz: list ident) (s: syn_path_elem) {struct s} :=
  match s with
  | Scell lidx =>
    do lvsz  <- eval_ident_list lsz;
    do shape <- natlist_of_Vint64 lvsz;
    do lvidx <- eval_expr |@ lidx;
    do idx   <- build_index lvidx shape;
    if valid_index lvidx lvsz then Val (Pcell idx)
    else Err
  end.

Fixpoint eval_expr_list (le: list expr) : outcome (list value) :=
  match le with
  | [] => Some []
  | exp :: le => do v  <- eval_expr exp;
                 do lv <- eval_expr_list le;
                 Val (v :: lv)
  end.

Fixpoint eval_path_elem_list llsz l {struct l} :=
  match llsz, l with
  | _, [] => Val []
  | y :: llsz, z :: l => do v  <- eval_path_elem y z;
                         do lv <- eval_path_elem_list llsz l;
                         Val (v :: lv)
  | _, _ => Stuck
  end.

Definition eval_path (p: syn_path) :=
  let (i, l) := p in
  do llsz <- (fn_szenv' f)!i;
  do l' <- eval_path_elem_list llsz l;
  Val (i, l').

Definition eval_full_path (p: syn_path) :=
  let (i, l) := p in
  do llsz <- (fn_szenv' f)!i;
  if (length l =? length llsz)%nat then
    do l' <- eval_path_elem_list llsz l;
    Val (i, l')
  else Stuck.

Fixpoint eval_path_list (l: list syn_path) : outcome (list sem_path) :=
  match l with
  | [] => Some []
  | x :: l => do p  <- eval_path x;
              do lp <- eval_path_list l;
              Val (p :: lp)
  end.

Lemma eval_expr_Eacc:
  forall i l,
    eval_expr (Eacc (i, l)) =
      do llsz <- (fn_szenv' f)!i;
      do l' <- eval_path_elem_list llsz l;
      do v  <- get_env_path e (i, l');
      if primitive_value v then Val v
      else Stuck.
Proof. reflexivity. Qed.

Lemma eval_ident_list_not_Err:
  forall lids, eval_ident_list lids <> Err.
Proof.
  elim/list_ind => //= > IH. case e!_ =>> //=. case (fn_szenv' f)!_ => [[]|] //=.
  case H: eval_ident_list => //=. move=> _ _. case H: eval_ident_list => //=.
Qed.

Lemma eval_expr_list_length:
  forall le lv,
    eval_expr_list le = Val lv ->
    length le = length lv.
Proof.
  elim/list_ind => [[]|>] //=. case eval_expr =>> //=.
  case eval_expr_list =>> //= /(_ _ eq_refl) -> > [<-] //.
Qed.

Lemma eval_expr_list_eq_app:
  forall lv1 lv2 le,
    eval_expr_list le = Val (lv1 ++ lv2) ->
    exists le1 le2,
      le = le1 ++ le2 /\
      eval_expr_list le1 = Val lv1 /\
      eval_expr_list le2 = Val lv2.
Proof.
  elim/list_ind => /=.
  - move=>> H. exists []; by eexists.
  - move=>> IH ? [] //= x >. case X: eval_expr =>> //=.
    case H: eval_expr_list =>> //= [<- T].
    rewrite T {T} in H. have [le1 [le2 [-> [H1 H2]]]] := IH _ _ H.
    exists (x :: le1), le2. repeat split => //=. by rewrite X H1.
Qed.

Close Scope outcome_scope.

End EvalExpr.