Require Import Utf8.
Require Import Lia.
Require Reals.
Require Import Rdefinitions Lra.

From Coq Require Import ssreflect ssrfun ssrbool.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Import BUtils Tactics.

Require Import Integers Floats.
Require Import IEEE754_extra.


Section Validity_Casts_Float_To_Int.

Lemma BofZ_pow_2:
  ∀ prec emax Hprec (Hemax: prec < emax) p,
    1 < prec ->
    0 < p -> p < emax ->
    Binary.is_finite prec emax
      (BofZ prec emax Hprec Hemax (- 2 ^ p)) = true /\
    Binary.is_finite prec emax
      (BofZ prec emax Hprec Hemax (2 ^ p)) = true.
Proof.
  move => prec emax ? ? p *. split; apply: (fst (snd (BofZ_representable _ _ _ _ _ _))).
  apply integer_representable_opp.
  all: apply: integer_representable_2p => //; lia.
Qed.

Lemma Bcompare_ZofB_range_pow_2:
  ∀ prec emax (Hprec: 0 < prec) (Hemax: prec < emax) p f,
    1 < prec ->
    0 < p -> p < emax ->
    (Binary.Bcompare prec emax
      (BofZ prec emax Hprec Hemax (-Z.pow 2 p)) f = Some Eq \/
     Binary.Bcompare prec emax
      (BofZ prec emax Hprec Hemax (-Z.pow 2 p)) f = Some Lt) ->
    Binary.Bcompare prec emax f
      (BofZ prec emax Hprec Hemax (Z.pow 2 p)) = Some Lt ->
    exists n, ZofB_range prec emax f (-Z.pow 2 p) (Z.pow 2 p - 1) = Some n.
Proof.
  move => prec emax Hprec Hemax p f Hprec' H1 H2 Hlow Hhigh.
  have := (BofZ_pow_2 prec emax Hprec Hemax p Hprec' H1 H2).
  rewrite/Binary.is_finite => [[Hm2p H2p]].
  have Hfinite: Binary.is_finite prec emax f = true.
  { move: Hlow Hhigh; elim f => // s; elim s.
    destruct_match_clear Hm2p => [[]|[]] //.
    destruct_match_clear H2p => [[]|[]] //. }
  have Hrep: integer_representable prec emax (2 ^ p).
  apply: integer_representable_2p; eauto || lia.
  have Hrep' := (integer_representable_opp _ _ _ Hrep).
  move: Hlow Hhigh; rewrite ZofB_range_correct; rewrite !Binary.Bcompare_correct //.
  rewrite (fst (BofZ_representable _ _ _ _ _ Hrep)).
  rewrite (fst (BofZ_representable _ _ _ _ _ Hrep')).
  move => [[Hlow]|[Hlow]] [Hhigh] {Hm2p H2p};
  have Hhigh' := Raux.Rcompare_Lt_inv _ _ Hhigh.
  + have := (Raux.Rcompare_Eq_inv _ _ Hlow) => <-. eexists. apply: if_true.
    repeat (apply: andb_true_intro; split) => //; apply Zaux.Zle_bool_true;
    rewrite Raux.Ztrunc_IZR; lia.
  + have := (Raux.Rcompare_Lt_inv _ _ Hlow) => Hlt. eexists. apply: if_true.
    rewrite -(Raux.Ztrunc_IZR (- 2 ^ p)) -(Raux.Ztrunc_IZR (2 ^ p - 1)).
    repeat (apply: andb_true_intro; split) => //; apply Zaux.Zle_bool_true.
    apply: Raux.Ztrunc_le; lra.
    have := RIneq.Rle_dec (Binary.B2R prec emax f) 0 => [[]] /= H.
    have := Raux.Ztrunc_le _ _ H; rewrite !Raux.Ztrunc_IZR; lia.
    have := RIneq.Rge_le _ _ (RIneq.Rgt_ge _ _ (RIneq.Rnot_le_gt _ _ H)).
    have := Raux.Ztrunc_floor. move => /[apply] ->.
    rewrite Raux.Ztrunc_IZR.
    apply: (fst (Z.le_add_le_sub_r _ _ _)); apply: Zlt_le_succ.
    apply: RIneq.lt_IZR. apply: RIneq.Rle_lt_trans. apply: Raux.Zfloor_lb. exact Hhigh'.
Qed.

Lemma ZofB_range_Bcompare_upper_bound:
  ∀ prec emax (Hprec: 0 < prec) (Hemax: prec < emax) p f n,
    1 < prec ->
    0 < p -> p < emax ->
    ZofB_range prec emax f (-Z.pow 2 p) (Z.pow 2 p - 1) = Some n ->
    Binary.Bcompare prec emax f
      (BofZ prec emax Hprec Hemax (Z.pow 2 p)) = Some Lt.
Proof.
  move => prec emax Hprec Hemax p f n Hprec' H1 H2.
  rewrite ZofB_range_correct => H. do 2 destruct_match_clear H.
  move: H => /andb_prop [] /andb_prop [] Hfinite _.
  have Hrep: integer_representable prec emax (2 ^ p).
  apply: integer_representable_2p; eauto || lia.
  rewrite Binary.Bcompare_correct //.
  apply: fst (snd (BofZ_representable _ _ _ _ _ Hrep)).
  move: (Binary.B2R prec emax f) => x /Zle_bool_imp_le => H; apply: f_equal.
  rewrite (fst (BofZ_representable _ _ _ _ _ Hrep)).
  apply: Raux.Rcompare_Lt. have [] := RIneq.Rle_dec x 0.
  + move: H => /[swap] /Raux.Ztrunc_ceil -> /RIneq.IZR_le.
    have := Raux.Zceil_ub x. move => /RIneq.Rle_trans /[apply]. rewrite RIneq.minus_IZR. lra.
  + move: H => /[swap] /RIneq.Rnot_le_gt/RIneq.Rgt_ge/RIneq.Rge_le.
    move => /Raux.Ztrunc_floor -> /RIneq.IZR_le /(RIneq.Rplus_le_compat_r 1).
    have := Raux.Zfloor_ub x. move => /RIneq.Rlt_le_trans /[apply]. rewrite RIneq.minus_IZR. lra.
Qed.

Lemma Bcompare_ZofB_range_pow_2_inf_prec:
  ∀ prec emax Hprec Hemax p f,
    1 < prec ->
    0 < p -> p < prec ->
    Binary.Bcompare prec emax (BofZ prec emax Hprec Hemax (- 2 ^ p - 1)) f = Some Lt /\
    Binary.Bcompare prec emax f (BofZ prec emax Hprec Hemax (2 ^ p)) = Some Lt <->
    exists n, ZofB_range prec emax f (- 2 ^ p) (2 ^ p - 1) = Some n.
Proof.
  move => prec emax Hprec Hemax p f Hprec' H1 H2.
  have H2' := (Z.lt_trans _ _ _ H2 Hemax).
  have Hrep: integer_representable prec emax (2 ^ p).
  apply: integer_representable_2p; lia.
  have Hrep' := integer_representable_opp _ _ _ Hrep.
  have Hrep1: integer_representable prec emax (- 2 ^ p - 1); [apply: integer_representable_n|].
  { split. have: - 2 ^ prec < - 2 ^ p. apply: (fst (Z.opp_lt_mono _ _)). apply: Z.pow_lt_mono_r. all: lia. }
  have Hm2p1 := fst (snd (BofZ_representable prec emax Hprec Hemax _ Hrep1)).
  have Hm2p := fst (snd (BofZ_representable prec emax Hprec Hemax _ Hrep')).
  have H2p := fst (snd (BofZ_representable prec emax Hprec Hemax _ Hrep)).
  split.
  + move => []. move => Hlow Hhigh.
    have Hfinite: (Binary.is_finite prec emax f = true).
    { move: Hm2p1 H2p; rewrite/Binary.is_finite => Hm2p1 H2p.
      move: Hlow Hhigh. elim f => // s; elim s.
      destruct_match_clear Hm2p1 => //. destruct_match_clear H2p. }
    have [] := RIneq.Rle_dec (Binary.B2R _ _ (BofZ prec emax Hprec Hemax (- 2 ^ p)))
                             (Binary.B2R _ _ f).
    - move/Raux.Rcompare_not_Gt => H.
      apply: (Bcompare_ZofB_range_pow_2 _ _ _ _ p f _ _ _ _ Hhigh); try lia.
      move: H. rewrite !Binary.Bcompare_correct //. elim: Raux.Rcompare; tauto.
    - move/RIneq.Rnot_le_gt/RIneq.Rgt_lt.
      move: Hlow; rewrite !Binary.Bcompare_correct // => [[]].
      move /Raux.Rcompare_Lt_inv. rewrite ZofB_range_correct => Hlow Hhigh'.
      have: (Binary.B2R _ _ f <= 0)%R.
      { apply: RIneq.Rle_trans. apply: RIneq.Rlt_le. exact Hhigh'.
        rewrite (fst (BofZ_representable _ _ _ _ _ Hrep')). apply: RIneq.IZR_le; lia. }
      move => /[dup] /Raux.Ztrunc_ceil -> H.
      have: Raux.Zceil (Binary.B2R prec emax f) = - 2 ^ p.
      { move: Hlow Hhigh'; rewrite !(fst (BofZ_representable _ _ _ _ _ _)) //.
        move => Hlow Hhigh'; apply: Raux.Zceil_imp. lra. }
      move => ->. eexists. apply: if_true. repeat (apply: andb_true_intro; split) => //.
      all: apply: Zle_imp_le_bool; lia.
  + move => [n]. split.
    2: { rewrite (ZofB_range_Bcompare_upper_bound _ _ _ _ p f n); lia || eauto. }
    move: p0; rewrite ZofB_range_correct => H; destruct_match_clear H.
    move: H => /andb_prop [] /andb_prop [] Hfinite.
    rewrite Binary.Bcompare_correct // => Hlow Hhigh.
    rewrite (fst (BofZ_representable _ _ _ _ _ Hrep1)) RIneq.minus_IZR.
    have: Raux.Rcompare (IZR (- 2 ^ p) - IZR 1) (Binary.B2R prec emax f) = Lt; [|move => -> //].
    move: Hlow Hhigh => /Zle_bool_imp_le/Zle_lt_or_eq [] + /Zle_bool_imp_le.
    - move => Hlow Hhigh. apply: Raux.Rcompare_Lt. have [] := RIneq.Rle_dec (Binary.B2R _ _ f) 0.
      all: only 1: (move: Hlow => /[swap] /Raux.Ztrunc_ceil -> /RIneq.IZR_lt Hlow;
                    have := Raux.Zceil_lb (Binary.B2R _ _ f)).
      all: only 2:
          (move: Hlow => /[swap] /RIneq.Rnot_le_gt/RIneq.Rgt_ge/RIneq.Rge_le;
            move => /Raux.Ztrunc_floor -> /RIneq.IZR_lt Hlow;
            have := Raux.Zfloor_lb (Binary.B2R _ _ f)).
      all: only 2: move: (RIneq.Rlt_plus_1 (Binary.B2R _ _ f))
                       => /[swap] /RIneq.Rle_lt_trans /[apply].
      all: move: Hlow => /Raxioms.Rlt_trans /[apply]; lra.
    - move => Hlow Hhigh. apply: Raux.Rcompare_Lt. rewrite Hlow.
      move: (Binary.B2R _ _ f) => x. have [] := RIneq.Rle_dec x 0.
      move: (Raux.Zceil_lb x) => /[swap] /Raux.Ztrunc_ceil ->; lra.
      move: (Raux.Zfloor_lb x) => /[swap].
      move => /RIneq.Rnot_le_gt/RIneq.Rgt_ge/RIneq.Rge_le/Raux.Ztrunc_floor ->; lra.
Qed.

Lemma pow_2_sup_prec_no_Zceil:
  ∀ prec emax (Hprec: 0 < prec) (Hemax: prec < emax) p f,
    prec <= p -> p < emax ->
    Binary.B2R prec emax f <> IZR (- 2 ^ p) ->
    - 2 ^ p <> Raux.Zceil (Binary.B2R prec emax f).
Proof.
  move => prec emax Hprec Hemax p f H1 H2 H.
  elim Hfinite: (Binary.is_finite prec emax f).
  2: { move: H Hfinite; elim: f => // s > _ _ /=; rewrite (Raux.Zceil_imp 0 0 _) /=; lra || lia. }
  have [] := RIneq.Rge_dec (Binary.B2R _ _ f) (IZR (- 2 ^ p)).
  + move: H => /[swap]. rewrite/Rge => [[| ->]].
    move/RIneq.Rgt_lt. have := Raux.Zceil_ub (Binary.B2R _ _ f).
    move/(RIneq.Rlt_le_trans _ _ _) /[apply] /RIneq.lt_IZR. lia.
    rewrite Raux.Zceil_IZR. lra.
  + move/RIneq.Rnot_ge_lt.
    rewrite RIneq.opp_IZR -{1}(RIneq.Ropp_involutive (Binary.B2R _ _ f)).
    move/RIneq.Ropp_lt_cancel. rewrite (Raux.IZR_Zpower Zaux.radix2); [lia|].
    move/(@Ulp.succ_le_lt Zaux.radix2 _ (Binary.fexp_correct prec emax Hprec) _ _
                (Generic_fmt.generic_format_bpow Zaux.radix2 _ p _)).
    have: SpecFloat.fexp prec emax (p + 1) <= p; [|move=>/[swap]/[apply]].
    rewrite/SpecFloat.fexp/SpecFloat.emin; lia.
    have: Generic_fmt.generic_format Zaux.radix2 (SpecFloat.fexp prec emax)
                                     (- Binary.B2R prec emax f); [|move=>/[swap]/[apply]].
    apply: Generic_fmt.generic_format_opp. move: Hfinite H; elim: f => //= s.
    move => _ _; apply: Generic_fmt.generic_format_0.
    move => m e + _ _. rewrite/SpecFloat.bounded => /andb_prop [] + _.
    move/Binary.canonical_canonical_mantissa => /(_ s).
    move/Generic_fmt.generic_format_canonical => //.
    rewrite/Ulp.succ Raux.Rle_bool_true. rewrite -(Raux.IZR_Zpower Zaux.radix2); try lia.
    apply: RIneq.IZR_le => /=; lia.
    rewrite Ulp.ulp_bpow/SpecFloat.fexp/SpecFloat.emin Z.sub_max_distr_r.
    change (3 - emax) with ((2 + 1) - emax). rewrite Z.add_sub_swap Z.add_max_distr_r.
    rewrite Zmax_left. apply: (Zge_trans _ 1); lia.
    move/RIneq.Ropp_le_contravar.
    rewrite RIneq.Ropp_involutive => /(RIneq.Rplus_le_compat_r 1).
    have := Raux.Zceil_lb (Binary.B2R _ _ f) => /RIneq.Rlt_le_trans /[apply].
    rewrite -!(Raux.IZR_Zpower Zaux.radix2) /=; try lia.
    rewrite RIneq.Ropp_plus_distr -!RIneq.opp_IZR -!RIneq.plus_IZR => /RIneq.lt_IZR. lia.
Qed.

Lemma Bcompare_ZofB_range_pow_2_sup_prec:
  ∀ prec emax Hprec Hemax p f,
    1 < prec ->
    0 < p -> p >= prec -> p < emax ->
    (Binary.Bcompare prec emax
      (BofZ prec emax Hprec Hemax (- 2 ^ p)) f = Some Eq \/
     Binary.Bcompare prec emax
      (BofZ prec emax Hprec Hemax (- 2 ^ p)) f = Some Lt) /\
    Binary.Bcompare prec emax f (BofZ prec emax Hprec Hemax (2 ^ p)) = Some Lt <->
    exists n, ZofB_range prec emax f (- 2 ^ p) (2 ^ p - 1) = Some n.
Proof.
  move => prec emax Hprec Hemax p f Hprec' H1 H2 H3. split.
  move => []; apply: Bcompare_ZofB_range_pow_2 => //.
  move => [n H]. split.
  2: { apply: ZofB_range_Bcompare_upper_bound => //. exact H. }
  move: H; rewrite ZofB_range_correct => H. destruct_match_clear H.
  move: H => /andb_prop [] /andb_prop [] Hfinite /Zle_bool_imp_le + /Zle_bool_imp_le.
  have: integer_representable prec emax (2 ^ p).
  apply: integer_representable_2p; lia.
  move/integer_representable_opp => Hrep.
  have [H [Hm2p _]] := BofZ_representable _ _ Hprec Hemax _ Hrep.
  rewrite Binary.Bcompare_correct // H.
  move => /Zle_lt_or_eq [+ | Heq] _; [|rewrite Heq].
  + move: (Binary.B2R _ _ f) => x. have [] := RIneq.Rle_dec 0 x.
    - move/Raux.Ztrunc_floor -> => /RIneq.IZR_lt Hx. right. apply: f_equal. apply: Raux.Rcompare_Lt.
      apply: (RIneq.Rlt_le_trans _ _ _ Hx). exact (Raux.Zfloor_lb x).
    - move /RIneq.Rnot_le_gt/RIneq.Rgt_ge/RIneq.Rge_le/Raux.Ztrunc_ceil => -> Hx.
      have: - 2 ^ p <= Raux.Zceil x - 1. lia.
      have: (IZR (Raux.Zceil x) - 1 < x)%R. have := Raux.Zceil_lb x. lra.
      move => + /RIneq.IZR_le; rewrite RIneq.minus_IZR.
      move => /(RIneq.Rle_lt_trans _ _ _ _) /[apply] /Raux.Rcompare_Lt ->; tauto.
  + have [] := RIneq.Rle_dec (Binary.B2R _ _ f) 0.
    move: Heq => /[swap] /Raux.Ztrunc_ceil ->.
    have [] := RIneq.Req_dec (Binary.B2R _ _ f) (IZR (- 2 ^ p)) => [-> {1}<-|].
    left. apply: f_equal. apply: Raux.Rcompare_Eq => //.
    move/pow_2_sup_prec_no_Zceil /[apply] => [] //; lia.
    move: Heq => /[swap] /RIneq.Rnot_le_gt/RIneq.Rgt_ge/RIneq.Rge_le/Raux.Ztrunc_floor ->.
    have [] := RIneq.Req_dec (Binary.B2R _ _ f) (IZR (Raux.Zfloor (Binary.B2R _ _ f))) => [{3}->|].
    left. apply: f_equal. apply: Raux.Rcompare_Eq => //.
    move => Hneq _. right. apply: f_equal. apply: Raux.Rcompare_Lt.
    have := Raux.Zfloor_lb (Binary.B2R _ _ f). move: Hneq; rewrite/Rle => /[swap] [[|{1}<-]] //.
Qed.

Lemma ZofB_range_low_bound_inf_zero_iff_zero:
  ∀ prec emax (Hprec: 0 < prec) Hemax low high x f,
    low <= 0 ->
    (Binary.Bcompare prec emax (BofZ prec emax Hprec Hemax (-1)) f = Some Lt /\
     ZofB_range prec emax f low high = Some x) <->
    ZofB_range prec emax f 0 high = Some x.
Proof.
  move=> prec emax Hprec Hemax low high x f Hlow. split.
  + move => []. rewrite !ZofB_range_correct.
    elim Hfinite: Binary.is_finite => //=.
    have: integer_representable prec emax (-1) by apply: integer_representable_n => //; lia.
    move/BofZ_representable => [+ [Hfinite1 _]].
    rewrite Binary.Bcompare_correct //= => -> [] /Raux.Rcompare_Lt_inv.
    case: (RIneq.Rle_dec 0 (Binary.B2R _ _ f)).
    - move/[dup]/Raux.Ztrunc_floor -> => /Raux.Zfloor_lub/Zaux.Zle_bool_true -> /=.
      elim: (low <=? _) (Raux.Zfloor _ <=? _) => //.
    - move/RIneq.Rnot_le_gt/RIneq.Rgt_lt/RIneq.Rlt_le/[dup]/Raux.Ztrunc_ceil ->.
      move/(@conj (-1 < Binary.B2R _ _ f)%R) /[apply] /(Raux.Zceil_imp 0) ->.
      elim: (low <=? 0) (0 <=? high) => //=.
  + rewrite!ZofB_range_correct. elim Hfinite: Binary.is_finite => //=.
    elim H: (0 <=? _) => //. move: H => /Zle_bool_imp_le/[dup]/(Z.le_trans low _ _ Hlow).
    have: integer_representable prec emax (-1) by apply: integer_representable_n => //; lia.
    move/BofZ_representable => [+ [Hfinite1 _]] => + /Zaux.Zle_bool_true -> /= + ->.
    rewrite Binary.Bcompare_correct => // -> H. rewrite Raux.Rcompare_Lt => //.
    case: (RIneq.Rle_dec 0%R (Binary.B2R _ _ f)). lra.
    move: H => /[swap] /RIneq.Rnot_le_gt/RIneq.Rgt_lt/RIneq.Rlt_le/[dup]/Raux.Ztrunc_ceil ->.
    have: (IZR (Raux.Zceil (Binary.B2R _ _ f)) - 1 < Binary.B2R _ _ f)%R.
    { have:= Raux.Zceil_lb (Binary.B2R _ _ f). lra. }
    move/(RIneq.Rle_lt_trans (-1)) => H ? /RIneq.IZR_le ?. apply: H. lra.
Qed.


(* Tests on floatting point numbers matches exactly the validity of cast to int *)

Lemma Float_zero_le_imp_m2p_lt:
  ∀ f,
    Binary.is_finite 53 1024 f = true ->
    Float.cmp Clt (BofZ 53 1024 eq_refl eq_refl (-1)) f = true ->
    ∀ n, n < 0 -> integer_representable 53 1024 n ->
    Float.cmp Clt (BofZ 53 1024 eq_refl eq_refl n) f = true.
Proof.
  Transparent Float.cmp Float.compare Float.zero.
  move=> f Hfinite + n H Hrep. rewrite/Float.cmp/Float.compare/Float.zero/cmp_of_comparison.
  have: integer_representable 53 1024 (-1) by apply: integer_representable_n => //.
  move/(BofZ_representable 53 1024 eq_refl eq_refl) => [] // > H1 [Hfinite1 _].
  (do 2 destruct_match_goal_ni) => _ + _.
  rewrite!Binary.Bcompare_correct //. exact (fst (snd (BofZ_representable _ _ _ _ _ Hrep))).
  rewrite H1. move => [] /Raux.Rcompare_Lt_inv H'. rewrite Raux.Rcompare_Lt //.
  rewrite (fst (BofZ_representable _ _ _ _ _ Hrep)).
  apply: (RIneq.Rle_lt_trans _ (-1)) => //. apply: RIneq.IZR_le. lia.
Qed.

Lemma Float32_zero_le_imp_m2p_lt:
  ∀ f,
    Binary.is_finite 24 128 f = true ->
    Float32.cmp Clt (BofZ 24 128 eq_refl eq_refl (-1)) f = true ->
    ∀ n, n < 0 -> integer_representable 24 128 n ->
    Float32.cmp Clt (BofZ 24 128 eq_refl eq_refl n) f = true.
Proof.
  Transparent Float32.cmp Float32.compare Float32.zero.
  move=> f Hfinite + n H Hrep. rewrite/Float32.cmp/Float32.compare/Float32.zero/cmp_of_comparison.
  have: integer_representable 24 128 (-1) by apply: integer_representable_n => //.
  move/(BofZ_representable 24 128 eq_refl eq_refl) => [] // > H1 [Hfinite1 _].
  (do 2 destruct_match_goal_ni) => _ + _.
  rewrite!Binary.Bcompare_correct //. exact (fst (snd (BofZ_representable _ _ _ _ _ Hrep))).
  rewrite H1. move => [] /Raux.Rcompare_Lt_inv H'. rewrite Raux.Rcompare_Lt //.
  rewrite (fst (BofZ_representable _ _ _ _ _ Hrep)).
  apply: (RIneq.Rle_lt_trans _ (-1)) => //. apply: RIneq.IZR_le. lia.
Qed.

Lemma float64_to_int_test:
  forall f,
    Float.cmp Clt (BofZ 53 1024 eq_refl eq_refl (- 2 ^ 31 - 1)) f = true /\
    Float.cmp Clt f (BofZ 53 1024 eq_refl eq_refl (2 ^ 31)) = true <->
    exists n, Float.to_int f = Some n.
Proof.
  Transparent Float.cmp Float.compare Float.to_int.
  move => f. rewrite/Float.cmp/Float.compare/cmp_of_comparison/Float.to_int. split.
  + move => []. do 4 destruct_match_goal; move => _ _.
    apply: (fst (Coqlib_option_map_Some _ _)).
    apply: (fst (Bcompare_ZofB_range_pow_2_inf_prec 53 1024 eq_refl
                                                    eq_refl 31 f _ _ _)).
    all: lia || eauto.
  + move => /(snd (Coqlib_option_map_Some _ _)).
    move/(snd (Bcompare_ZofB_range_pow_2_inf_prec 53 1024 eq_refl
                                                  eq_refl 31 _ _ _ _)) => []; try lia.
    move => -> -> //.
Qed.

Lemma float64_to_int64_test:
  forall f,
    Float.cmp Cle (BofZ 53 1024 eq_refl eq_refl (- 2 ^ 63)) f = true /\
    Float.cmp Clt f (BofZ 53 1024 eq_refl eq_refl (2 ^ 63)) = true <->
    exists n, Float.to_long f = Some n.
Proof.
  Transparent Float.cmp Float.compare Float.to_long.
  move => f. rewrite/Float.cmp/Float.compare/cmp_of_comparison/Float.to_int. split.
  + move => []. do 4 destruct_match_goal; move => _ _;
      apply: (fst (Coqlib_option_map_Some _ _)).
    apply: (fst (Bcompare_ZofB_range_pow_2_sup_prec 53 1024 eq_refl
                                                    eq_refl 63 f _ _ _ _)).
    all: lia || eauto.
    apply: (fst (Bcompare_ZofB_range_pow_2_sup_prec 53 1024 eq_refl
                                                    eq_refl 63 f _ _ _ _)).
    all: lia || eauto.
  + move => /(snd (Coqlib_option_map_Some _ _)).
    move/(snd (Bcompare_ZofB_range_pow_2_sup_prec 53 1024 eq_refl
                                                  eq_refl 63 _ _ _ _ _)) => []; try lia.
    move => [] -> -> //.
Qed.

Lemma float64_to_uint_test:
  forall f,
    Float.cmp Clt (BofZ 53 1024 eq_refl eq_refl (-1)) f = true /\
    Float.cmp Clt f (BofZ 53 1024 eq_refl eq_refl (2 ^ 32)) = true <->
    exists n, Float.to_intu f = Some n.
Proof.
  Transparent Float.cmp Float.compare Float.to_intu.
  move => f. split.
  + move => [] H1 H2. have Hfinite: Binary.is_finite _ _ f; move: H1 H2.
    { elim: f => // [[]] //. }
    move=> /[dup] Hlow_cmp. rewrite/Float.to_intu.
    have: - 2 ^ 32 - 1 < -1 by lia.
    have: integer_representable 53 1024 (- 2 ^ 32 - 1) by apply: integer_representable_n => //.
    move/(Float_zero_le_imp_m2p_lt _ Hfinite) /[apply] /[apply] => Hlow Hhigh.
    apply: (fst (Coqlib_option_map_Some _ _)).
    have:= (fst (Bcompare_ZofB_range_pow_2_inf_prec 53 1024 eq_refl
                                                    eq_refl 32 f _ _ _)) => [[]]; try lia.
    move: Hlow Hhigh; rewrite/Float.cmp/Float.compare/cmp_of_comparison.
    (do 4 destruct_match_goal_ni) => //.
    move: Hlow_cmp => + x. rewrite/Float.zero/Float.cmp/Float.compare/cmp_of_comparison.
    (do 2 destruct_match_goal_ni) => _ H _ *.
    exists x. have H': - 2 ^ 32 <= 0 by lia.
    apply: (fst (ZofB_range_low_bound_inf_zero_iff_zero 53 1024 eq_refl eq_refl (- 2 ^ 32) _ x f H')) => //.
  + move => /(snd (Coqlib_option_map_Some _ _)) [x].
    move/(snd (ZofB_range_low_bound_inf_zero_iff_zero 53 1024 eq_refl eq_refl (- 2 ^ 32) _ _ _ _)) => [] //.
    rewrite/Float.cmp/cmp_of_comparison/Float.compare => -> H.
    have: ∃ z, ZofB_range 53 1024 f (- 2 ^ 32) Int.max_unsigned = Some z; eauto.
    move/(snd (Bcompare_ZofB_range_pow_2_inf_prec 53 1024 eq_refl eq_refl 32 f _ _ _)).
    move=> []; try lia. move=> _ -> //.
Qed.

Lemma float64_to_uint64_test:
  forall f,
    Float.cmp Clt (BofZ 53 1024 eq_refl eq_refl (-1)) f = true /\
    Float.cmp Clt f (BofZ 53 1024 eq_refl eq_refl (2 ^ 64)) = true <->
    exists n, Float.to_longu f = Some n.
Proof.
  Transparent Float.cmp Float.compare Float.to_longu.
  move => f. split.
  + move => [] H1 H2. have Hfinite: Binary.is_finite _ _ f; move: H1 H2.
    { elim: f => // [[]] //. }
    move=> /[dup] Hlow_cmp. rewrite/Float.to_longu.
    have: - 2 ^ 64 - 1 < -1 by lia.
    have: integer_representable 53 1024 (- 2 ^ 64).
    { apply: integer_representable_opp. apply: integer_representable_2p => //. }
    move/(Float_zero_le_imp_m2p_lt _ Hfinite) /[apply] /[apply] => Hlow Hhigh.
    apply: (fst (Coqlib_option_map_Some _ _)).
    have:= (fst (Bcompare_ZofB_range_pow_2_sup_prec 53 1024 eq_refl
                                                    eq_refl 64 f _ _ _ _)) => [[]]; try lia.
    move: Hlow Hhigh; rewrite/Float.cmp/Float.compare/cmp_of_comparison.
    (do 4 destruct_match_goal_ni) => //. eauto.
    move: Hlow_cmp => + x. rewrite/Float.zero/Float.cmp/Float.compare/cmp_of_comparison.
    (do 2 destruct_match_goal_ni) => _ H _ *.
    exists x. have H': - 2 ^ 64 <= 0 by lia.
    apply: (fst (ZofB_range_low_bound_inf_zero_iff_zero 53 1024 eq_refl eq_refl (- 2 ^ 64) _ x f H')) => //.
  + move => /(snd (Coqlib_option_map_Some _ _)) [x].
    move/(snd (ZofB_range_low_bound_inf_zero_iff_zero 53 1024 eq_refl eq_refl (- 2 ^ 64) _ _ _ _)) => [] //.
    rewrite/Float.cmp/cmp_of_comparison/Float.compare => -> H.
    have: ∃ z, ZofB_range 53 1024 f (- 2 ^ 64) Int64.max_unsigned = Some z; eauto.
    move/(snd (Bcompare_ZofB_range_pow_2_sup_prec 53 1024 eq_refl eq_refl 64 f _ _ _ _)).
    move=> []; try lia. move=> _ -> //.
Qed.

Lemma float32_to_int_test:
  forall f,
    Float32.cmp Cle (BofZ 24 128 eq_refl eq_refl (- 2 ^ 31)) f = true /\
    Float32.cmp Clt f (BofZ 24 128 eq_refl eq_refl (2 ^ 31)) = true <->
    exists n, Float32.to_int f = Some n.
Proof.
  Transparent Float32.cmp Float32.compare Float32.to_int.
  move => f. rewrite/Float32.cmp/Float32.compare/cmp_of_comparison/Float32.to_int. split.
  + move => []. do 4 destruct_match_goal; move => _ _;
    apply: (fst (Coqlib_option_map_Some _ _)).
    apply: (fst (Bcompare_ZofB_range_pow_2_sup_prec 24 128 eq_refl
                                                    eq_refl 31 f _ _ _ _)).
    all: lia || eauto.
    apply: (fst (Bcompare_ZofB_range_pow_2_sup_prec 24 128 eq_refl
                                                    eq_refl 31 f _ _ _ _)).
    all: lia || eauto.
  + move => /(snd (Coqlib_option_map_Some _ _)).
    move/(snd (Bcompare_ZofB_range_pow_2_sup_prec 24 128 eq_refl
                                                  eq_refl 31 _ _ _ _ _)) => []; try lia.
    move => [] -> -> //.
Qed.

Lemma float32_to_int64_test:
  forall f,
    Float32.cmp Cle (BofZ 24 128 eq_refl eq_refl (- 2 ^ 63)) f = true /\
    Float32.cmp Clt f (BofZ 24 128 eq_refl eq_refl (2 ^ 63)) = true <->
    exists n, Float32.to_long f = Some n.
Proof.
  Transparent Float32.cmp Float32.compare Float32.to_long.
  move => f. rewrite/Float32.cmp/Float32.compare/cmp_of_comparison/Float32.to_int. split.
  + move => []. do 4 destruct_match_goal; move => _ _;
      apply: (fst (Coqlib_option_map_Some _ _)).
    apply: (fst (Bcompare_ZofB_range_pow_2_sup_prec 24 128 eq_refl
                                                    eq_refl 63 f _ _ _ _)).
    all: lia || eauto.
    apply: (fst (Bcompare_ZofB_range_pow_2_sup_prec 24 128 eq_refl
                                                    eq_refl 63 f _ _ _ _)).
    all: lia || eauto.
  + move => /(snd (Coqlib_option_map_Some _ _)).
    move/(snd (Bcompare_ZofB_range_pow_2_sup_prec 24 128 eq_refl
                                                  eq_refl 63 _ _ _ _ _)) => []; try lia.
    move => [] -> -> //.
Qed.

Lemma float32_to_uint_test:
  forall f,
    Float32.cmp Clt (BofZ 24 128 eq_refl eq_refl (-1)) f = true /\
    Float32.cmp Clt f (BofZ 24 128 eq_refl eq_refl (2 ^ 32)) = true <->
    exists n, Float32.to_intu f = Some n.
Proof.
  Transparent Float32.cmp Float32.compare Float32.to_intu.
  move => f. split.
  + move => [] H1 H2. have Hfinite: Binary.is_finite _ _ f; move: H1 H2.
    { elim: f => // [[]] //. }
    move=> /[dup] Hlow_cmp. rewrite/Float32.to_intu.
    have: - 2 ^ 32 - 1 < -1 by lia.
    have: integer_representable 24 128 (- 2 ^ 32).
    { apply: integer_representable_opp. apply: integer_representable_2p => //. }
    move/(Float32_zero_le_imp_m2p_lt _ Hfinite) /[apply] /[apply] => Hlow Hhigh.
    apply: (fst (Coqlib_option_map_Some _ _)).
    have:= (fst (Bcompare_ZofB_range_pow_2_sup_prec 24 128 eq_refl
                                                    eq_refl 32 f _ _ _ _)) => [[]]; try lia.
    move: Hlow Hhigh; rewrite/Float32.cmp/Float32.compare/cmp_of_comparison.
    (do 4 destruct_match_goal_ni) => //. eauto.
    move: Hlow_cmp => + x. rewrite/Float32.zero/Float32.cmp/Float32.compare/cmp_of_comparison.
    (do 2 destruct_match_goal_ni) => _ H _ *.
    exists x. have H': - 2 ^ 32 <= 0 by lia.
    apply: (fst (ZofB_range_low_bound_inf_zero_iff_zero 24 128 eq_refl eq_refl (- 2 ^ 32) _ x f H')) => //.
  + move => /(snd (Coqlib_option_map_Some _ _)) [x].
    move/(snd (ZofB_range_low_bound_inf_zero_iff_zero 24 128 eq_refl eq_refl (- 2 ^ 32) _ _ _ _)) => [] //.
    rewrite/Float32.cmp/cmp_of_comparison/Float32.compare => -> H.
    have: ∃ z, ZofB_range 24 128 f (- 2 ^ 32) Int.max_unsigned = Some z; eauto.
    move/(snd (Bcompare_ZofB_range_pow_2_sup_prec 24 128 eq_refl eq_refl 32 f _ _ _ _)).
    move=> []; try lia. move=> _ -> //.
Qed.

Lemma float32_to_uint64_test:
  forall f,
    Float32.cmp Clt (BofZ 24 128 eq_refl eq_refl (-1)) f = true /\
    Float32.cmp Clt f (BofZ 24 128 eq_refl eq_refl (2 ^ 64)) = true <->
    exists n, Float32.to_longu f = Some n.
Proof.
  Transparent Float32.cmp Float32.compare Float32.to_longu.
  move => f. split.
  + move => [] H1 H2. have Hfinite: Binary.is_finite _ _ f; move: H1 H2.
    { elim: f => // [[]] //. }
    move=> /[dup] Hlow_cmp. rewrite/Float32.to_intu.
    have: - 2 ^ 64 - 1 < -1 by lia.
    have: integer_representable 24 128 (- 2 ^ 64).
    { apply: integer_representable_opp. apply: integer_representable_2p => //. }
    move/(Float32_zero_le_imp_m2p_lt _ Hfinite) /[apply] /[apply] => Hlow Hhigh.
    apply: (fst (Coqlib_option_map_Some _ _)).
    have:= (fst (Bcompare_ZofB_range_pow_2_sup_prec 24 128 eq_refl
                                                    eq_refl 64 f _ _ _ _)) => [[]]; try lia.
    move: Hlow Hhigh; rewrite/Float32.cmp/Float32.compare/cmp_of_comparison.
    (do 4 destruct_match_goal_ni) => //. eauto.
    move: Hlow_cmp => + x. rewrite/Float32.zero/Float32.cmp/Float32.compare/cmp_of_comparison.
    (do 2 destruct_match_goal_ni) => _ H _ *.
    exists x. have H': - 2 ^ 64 <= 0 by lia.
    apply: (fst (ZofB_range_low_bound_inf_zero_iff_zero 24 128 eq_refl eq_refl (- 2 ^ 64) _ x f H')) => //.
  + move => /(snd (Coqlib_option_map_Some _ _)) [x].
    move/(snd (ZofB_range_low_bound_inf_zero_iff_zero 24 128 eq_refl eq_refl (- 2 ^ 64) _ _ _ _)) => [] //.
    rewrite/Float32.cmp/cmp_of_comparison/Float32.compare => -> H.
    have: ∃ z, ZofB_range 24 128 f (- 2 ^ 64) Int64.max_unsigned = Some z; eauto.
    move/(snd (Bcompare_ZofB_range_pow_2_sup_prec 24 128 eq_refl eq_refl 64 f _ _ _ _)).
    move=> []; try lia. move=> _ -> //.
Qed.

End Validity_Casts_Float_To_Int.
