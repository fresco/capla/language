Require Import ssreflect ssrbool.
Require Import PeanoNat Bool String List Lia.

Import ListNotations.

Require Import BUtils Tactics.
Require Import Errors.

Scheme Equality for list.

Theorem list_reflect {A: Type} (eqA: A -> A -> bool)
                               (Heq: forall x y, reflect (x = y) (eqA x y))
                               (l1 l2: list A):
  reflect (l1 = l2) (list_beq _ eqA l1 l2).
Proof.
  assert (H1: forall x y, eqA x y = true -> x = y) by (move=>>; by case: Heq).
  assert (H2: forall x y, x = y -> eqA x y = true) by (move=>>; by case: Heq).
  case: (list_eq_dec _ eqA H1 H2 l1 l2) => [<-|Hneq].
  - rewrite (internal_list_dec_lb _ _ H2 _ _ eq_refl). by apply: ReflectT.
  - case H: (list_beq A eqA l1 l2).
    + by move/(internal_list_dec_bl _ _ H1) in H.
    + by apply: ReflectF.
Qed.

Section Mem.

Context {A: Type}.
Variable eqA: A -> A -> bool.
Hypothesis Heq: forall x y, reflect (x = y) (eqA x y).

Definition mem (a: A) (l: list A) : bool :=
  existsb (eqA a) l.

Theorem mem_dec_spec:
  forall x l, reflect (In x l) (mem x l).
Proof.
  move=> x. elim/list_rec => /=. by apply: ReflectF.
  move=> a l IH. case: Heq => [->|Hneq]. by (apply: ReflectT; left).
  case: IH => /= H. by (apply: ReflectT; right).
  apply: ReflectF. by move: Hneq => /[swap] - [->|H'].
Qed.

Theorem mem_dec_spec':
  forall x l,
    mem x l = true <-> In x l.
Proof.
  move=> x. elim/list_ind => //= > [IH1 IH2]. apply: conj.
  - case/orP => [/Heq|]; by auto.
  - case=> [->|]. by case (Heq x x). move/IH2 => ->. by case eqA.
Qed.

Theorem mem_spec:
  forall x l, mem x l = true <-> exists y, In y l /\ eqA x y.
Proof.
  move=> x. elim/list_ind => /=. by apply: conj => [|[? []]].
  move=>> [IH1 IH2]. apply: conj.
  - case/orP. eauto. case/IH1 => ? []. eauto.
  - case=>> [[-> ->|]] // Hin H. rewrite IH2. eauto.
    by rewrite orb_true_r.
Qed.

Lemma mem_impl:
  forall l x y,
    mem x l ->
    (forall z, eqA x z -> eqA y z) ->
    mem y l.
Proof.
  move=>> /existsb_exists + H. case=> z [] Hin /H {H} => H.
  apply: (proj2 (existsb_exists _ _)). eauto.
Qed.

Lemma mem_impl_not:
  forall l x y,
    ~~ mem x l ->
    (forall z, ~~ eqA x z -> ~~ eqA y z) ->
    ~~ mem y l.
Proof.
  move=> l x > Hx H. apply: contraT => /negPn.
  move/existsb_exists. case=> z [] Hin.
  move: (H z) => {H} /contraTT/[apply] => H.
  rewrite -(@negbF (mem x l)) => //.
  apply: (proj2 (existsb_exists _ _)). eauto.
Qed.

Lemma mem_In:
  forall l a x,
    ~~ mem a l ->
    In x l ->
    ~~ eqA a x.
Proof. elim/list_ind =>> //= IH >. case/norP => ++ [<-|] // => _. by apply: IH. Qed.

Lemma mem_nth_error:
  forall a l,
    mem a l <->
    exists n b, nth_error l n = Some b /\ eqA a b.
Proof.
  move=> a. elim/list_ind => /=.
  - split => //. case=> - [] > [>] [] //=.
  - move=> b l IH. split.
    + case H: eqA. by move=> _; exists 0, b.
      move: IH. case: mem => //.
      move=> /proj1/[apply]. case=> n [p]. by exists (S n), p.
    + case H: eqA => //. case=> - [] /=.
      * case=> p [[<-]]. by rewrite H.
      * move {H} => n H. rewrite IH. by apply: (ex_intro _ n).
Qed.

End Mem.

Section NoDupKeepFirst.

Context {A : Type}.

Variable eqA: A -> A -> bool.

Hypothesis HeqA: forall x y, reflect (x = y) (eqA x y).

Let decA := fun x y => reflect_dec _ _ (HeqA x y).

Fixpoint nodup_keep_first' (nl l: list A) :=
  match l with
  | [] => []
  | x :: xs => if mem eqA x nl then nodup_keep_first' nl xs
               else x :: nodup_keep_first' (x :: nl) xs
  end.

Definition nodup_keep_first (l: list A) := nodup_keep_first' [] l.

Lemma nodup_app_notin_snd_part (a: A) l1 l2:
  In a l2 ->
  nodup decA (l1 ++ l2) = nodup decA (l1 ++ a :: l2).
Proof.
  induction l1; simpl; intro.
  + now destruct in_dec.
  + rewrite <- IHl1 by exact H. destruct (decA a a0) as [<-|Hneq].
    - destruct in_dec; [|elim n; apply in_or_app; now right].
      destruct in_dec; [|elim n; apply in_or_app; right; now apply in_cons].
      reflexivity.
    - destruct in_dec.
      * destruct in_dec. reflexivity.
        elim n. apply in_app_or in i as []. apply in_or_app; now left.
        apply in_or_app; right; now apply in_cons.
      * destruct in_dec; [|reflexivity].
        elim n. apply in_app_or in i as []. apply in_or_app; now left.
        apply in_or_app; right. now apply in_inv in H0 as [->|].
Qed.

Lemma nodup_keep_first'_app nl l1 l2:
  NoDup nl ->
  nodup_keep_first' nl (l1 ++ l2)
  = nodup_keep_first' nl l1 ++
    nodup_keep_first' (nodup decA (rev l1 ++ nl)) l2.
Proof.
  revert nl. induction l1; simpl; intros. now rewrite -> nodup_fixed_point.
  case (mem_dec_spec _ HeqA) => I.
  - rewrite -> IHl1 by auto. apply app_inv_head_iff.
    change (nodup_keep_first' ?l l2) with ((fun x => nodup_keep_first' x l2) l).
    apply f_equal. rewrite <- app_assoc. simpl.
    now erewrite nodup_app_notin_snd_part.
  - rewrite -> (IHl1 (a :: nl)) by now apply NoDup_cons. now rewrite <- app_assoc.
Qed.

Lemma nodup_keep_first_app l1 l2:
  nodup_keep_first (l1 ++ l2) = nodup_keep_first l1 ++
                                nodup_keep_first' (nodup decA (rev l1)) l2.
Proof. rewrite <- (app_nil_r (rev l1)).
       exact (nodup_keep_first'_app [] _ _ (NoDup_nil _)).
Qed.

Lemma in_nodup_keep_first' x nl l:
  In x (nodup_keep_first' nl l) <-> ~In x nl /\ In x l.
Proof.
  split.
  + revert nl. induction l; simpl; intros. contradiction.
    destruct (mem_dec_spec _ HeqA a nl) as [Hin|Hnotin].
    - apply IHl in H as [Hnl Hl]. split; [|right]; assumption.
    - destruct H as [<-|Hin].
      * split; [|left]; easy.
      * apply IHl in Hin as [Hnl Hl]. apply not_in_cons in Hnl as [_ Hnl].
        split; [|right]; easy.
  + revert nl. induction l; simpl; intros. tauto.
    destruct H as [Hnl [<-|Hl]].
    - case (mem_dec_spec _ HeqA) => I //. exact (in_eq a _).
    - case (mem_dec_spec _ HeqA) => I. now apply IHl.
      destruct (decA x a) as [<-|Hneq]. exact (in_eq x _).
      simpl. right. apply IHl. split; [apply not_in_cons|]; easy.
Qed.

Lemma NoDup_nodup_keep_first' nl l:
  NoDup nl ->
  NoDup (nodup_keep_first' nl l).
Proof.
  revert nl. induction l; simpl; intros. apply NoDup_nil.
  case (mem_dec_spec _ HeqA) => I /=. eauto. apply NoDup_cons; [|apply IHl].
  + apply (not_iff_compat (in_nodup_keep_first' _ _ _)).
    intros [Hnl Hl]. apply not_in_cons in Hnl. easy.
  + apply NoDup_cons; auto.
Qed.

Lemma NoDup_nodup_keep_first l: NoDup (nodup_keep_first l).
Proof. apply NoDup_nodup_keep_first', NoDup_nil. Qed.

Lemma prop_conservation_nodup_keep_first' (P: A -> Prop) nl l:
  Forall P l ->
  Forall P (nodup_keep_first' nl l).
Proof.
  revert nl. induction l; simpl; intros. assumption.
  case (mem_dec_spec _ HeqA) => I /=. apply Forall_inv_tail in H. auto.
  apply Forall_cons_iff in H as [Ha Hl]. apply Forall_cons; auto.
Qed.

Lemma nodup_keep_first'_cons_invariant nl a l:
  In a nl ->
  nodup_keep_first' nl (a :: l) = nodup_keep_first' nl l.
Proof. simpl. by case (mem_dec_spec _ HeqA). Qed.

End NoDupKeepFirst.

Section Prefix.

Context {A: Type}.
Variable eqA: A -> A -> bool.
Hypothesis Heq: forall x y, reflect (x = y) (eqA x y).

Fixpoint prefix (l1 l2: list A) : bool :=
  match l1, l2 with
  | [], _ => true
  | _, [] => false
  | x1 :: l1, x2 :: l2 => eqA x1 x2 && prefix l1 l2
  end.

Theorem prefix_refl (l: list A):
  prefix l l.
Proof. elim/list_ind: l => //= > IH. by case: Heq. Qed.

Theorem prefix_transl (l1 l2 l3: list A):
  prefix l1 l2 -> prefix l2 l3 -> prefix l1 l3.
Proof.
  move: l1 l2 l3. elim/list_ind => // a1 l1 IH [//|a2 l2] [//|a3 l3] /=.
  case: (Heq a1 a2) => [<-|] //=. case: (Heq a1 a3) => //= _.
  by apply: IH.
Qed.

Lemma prefix_firstn (l1 l2: list A):
  prefix l1 l2 ->
  firstn (length l1) l2 = l1.
Proof.
  move: l2; elim/list_ind: l1 => /=.
  - by case.
  - move=> x1 l1 IH [|x2 l2] //=. case/andP. move/Heq => <-. by move/IH ->.
Qed.

Theorem prefix_app' (l1 l2: list A):
  prefix l1 (l1 ++ l2).
Proof. elim/list_ind: l1 => //=. move=>> ->. by case Heq. Qed.

Theorem prefix_app (l l1 l2: list A):
  prefix l1 l2 ->
  prefix (l ++ l1) (l ++ l2).
Proof.
  move: l l2. elim/list_ind: l1 => [|a1 l1 IH] l l2 /=.
  - move=> _. rewrite app_nil_r. by apply: prefix_app'.
  - case l2 => // >. case/andP. move/Heq => <-.
    change (a1 :: ?l) with ([a1] ++ l). rewrite 2!app_assoc.
    by apply IH.
Qed.

Theorem eq_app_case_prefix (l1 l1' l2 l2': list A):
  l1 ++ l2 = l1' ++ l2' ->
  prefix l1 l1' \/ prefix l1' l1.
Proof.
  move: l1 l1' l2 l2'. elim/list_ind => [|a1 l1 IH].
  - move=>> _. by left.
  - case=> [|a1' l1']. move=>> _; by right.
    move=>>. case=> <-. case/IH => H /=.
    + apply: or_introl. by case Heq.
    + apply: or_intror. by case Heq.
Qed.

Theorem prefix_app_cases (l l1 l2: list A):
  prefix l (l1 ++ l2) ->
  prefix l l1 \/ prefix l1 l.
Proof.
  move: l1 l l2. elim/list_ind => [|a1 l1 IH].
  - move=>> _. by right.
  - case=> [|a l]. move=>> _; by left.
    move=>> /=. case/andP. move/Heq => <-. move/IH.
    case=> ->. left. by case Heq. right. by case Heq.
Qed.

Theorem prefix_of_same_list_cases (l1 l2 l: list A):
  prefix l1 l ->
  prefix l2 l ->
  prefix l1 l2 \/ prefix l2 l1.
Proof.
  move: l l1 l2. elim/list_ind => [|a l IH] [|a1 l1] // [|a2 l2] //=.
  by left. by left. by left. by right.
  case: (Heq a1 a) => [->|] //=.
  case: (Heq a2 a) => [->|] //=. case: (Heq a a) => //= _. now apply: IH.
Qed.

Theorem prefix_cut (l1 l: list A):
  prefix l1 l ->
  exists l2, l = l1 ++ l2.
Proof.
  move: l1 l. elim/list_ind => /=. eauto.
  move=> a l IH [|a1 l1] //. case Heq => //= <-. case/IH =>> ->. eauto.
Qed.

Lemma prefix_app_not_eq (l l': list A) (x: A):
  prefix l (l' ++ [x]) ->
  l <> (l' ++ [x]) ->
  prefix l l'.
Proof.
  move: l l'. elim/list_ind => //= a l IH [|a' l'] /=.
  case/andP. case: Heq => // -> {IH}. by case: l.
  case/andP. case: Heq => //= -> _ Hpref Hneq.
  have: l <> l' ++ [x] by (move=> H; by rewrite H in Hneq).
  by apply: IH.
Qed.

Lemma prefix_not_comm (l l': list A):
  prefix l l' ->
  l <> l' ->
  ~~ prefix l' l.
Proof.
  move: l l'. elim/list_ind => /=. by case.
  move=> a l IH. case=> //= a' l'. case: (Heq a a') => //= <-.
  move=> + Hneq. have: l <> l' by congruence.
  move/IH/[apply]. by case: Heq.
Qed.

Lemma prefix_app_left (l1 l2 l: list A):
  prefix (l1 ++ l2) l ->
  prefix l1 l.
Proof.
  move: l. elim/list_ind: l1 => //= a1 l1 IH [|a l] //.
  case: eqA => //=. by apply: IH.
Qed.

End Prefix.

Section Count_Occb.

Context {A: Type}.
Variable eqA: A -> A -> bool.

Fixpoint count_occb (l: list A) (a: A) : nat :=
  match l with
  | [] => 0
  | x :: l =>
      if eqA x a then S (count_occb l a)
      else count_occb l a
  end.

Lemma count_occb_ge_1:
  forall l a,
    1 <= count_occb l a <->
    exists n b, nth_error l n = Some b /\ eqA b a.
Proof.
  elim/list_ind => /=.
  - move=> a. split. lia. by case=> - [] > /= [>] [].
  - move=> a l IH b. split.
    + case H: eqA.
      move=> _. by exists 0, a.
      move: IH => /(_ b). case: count_occb. lia.
      move=>> /proj1/[apply]. case=> n [p]. by exists (S n), p.
    + case H: eqA. lia. case=> - [] /=.
      * case=> p [[<-]]. by rewrite H.
      * move {H} => n H. rewrite IH. by apply: (ex_intro _ n).
Qed.

Lemma count_occb_gt_1:
  forall l a,
    1 < count_occb l a <->
    exists x y p1 p2, x <> y /\
           nth_error l x = Some p1 /\
           nth_error l y = Some p2 /\
           eqA p1 a /\
           eqA p2 a.
Proof.
  elim/list_ind => /=.
  - split. move/Nat.ltb_lt => //. case=> - [] > [>] [>] [>]; easy.
  - move=> a l IH a'. case Ha': eqA; split.
    + case (Nat.ltb_spec0 1 (count_occb l a')).
      move/IH => [x] [y] [p1] [p2] [?] [?] [?] [?] ?.
      move=> _. exists (S x), (S y), p1, p2. eauto 10.
      move=> _ /Nat.lt_succ_r/count_occb_ge_1.
      case=> [n] [p] [] Hn Hp. by exists 0, (S n), a, p.
    + case=> [[|x]] [+] [p1] [p2] /=; [move=> [|y] [Hsep] //=|move=> y] => H.
      apply Nat.lt_succ_r, count_occb_ge_1. exists y, p2; easy.
      apply Nat.lt_succ_r, count_occb_ge_1. exists x, p1; easy.
    + move/IH => [x] [y] [p1] [p2] [] ? ?. exists (S x), (S y), p1, p2 => //. eauto.
    + move=> H. apply: (proj2 (IH _)).
      case: H => - [|x] [[|y]] /= [p1] [p2] [?] [?] [?] [?] ?; try congruence.
      exists x, y, p1, p2. auto.
Qed.

End Count_Occb.

Section Fold_Left2_Recursor.
  Variables (A: Type) (B: Type) (C: Type).
  Variable f : A -> B -> C -> A.

  Fixpoint fold_left2 (l1: list B) (l2: list C) (a0: A) :=
    match l1, l2 with
    | [], [] => Some a0
    | x1 :: l1, x2 :: l2 => fold_left2 l1 l2 (f a0 x1 x2)
    | _, _ => None
    end.

  Lemma fold_left2_app: forall l1 l2 l1' l2' i i',
    fold_left2 l1 l2 i = Some i' ->
    fold_left2 (l1 ++ l1') (l2 ++ l2') i = fold_left2 l1' l2' i'.
  Proof.
    induction l1; destruct l2; try discriminate.
    + intros l1' l2' i i' [= <-]. reflexivity.
    + simpl. intros l1' l2' i i' H.
      now rewrite -> IHl1 with (1 := H).
  Qed.
End Fold_Left2_Recursor.

Arguments fold_left2 {_} {_} {_}.

Section Fold_Left3_Recursor.
  Variables (A B C D: Type).
  Variable f : A -> B -> C -> D -> A.

  Fixpoint fold_left3 (l1: list B) (l2: list C) (l3: list D) (a0: A) :=
    match l1, l2, l3 with
    | [], [], [] => Some a0
    | x1 :: l1, x2 :: l2, x3 :: l3 => fold_left3 l1 l2 l3 (f a0 x1 x2 x3)
    | _, _, _ => None
    end.

  Lemma fold_left3_app: forall l1 l2 l3 l1' l2' l3' i i',
    fold_left3 l1 l2 l3 i = Some i' ->
    fold_left3 (l1 ++ l1') (l2 ++ l2') (l3 ++ l3') i
    = fold_left3 l1' l2' l3' i'.
  Proof.
    induction l1; destruct l2; destruct l3; try discriminate.
    + intros l1' l2' l3' i i' [= <-]. reflexivity.
    + simpl. intros l1' l2' l3' i i' H.
      now rewrite -> IHl1 with (1 := H).
  Qed.
End Fold_Left3_Recursor.

Arguments fold_left3 {_} {_} {_} {_}.

Local Open Scope option_bool_monad_scope.

Section Fold_Right2_Recursor.
  Variables (A: Type) (B: Type) (C: Type).
  Variable f : A -> B -> C -> A.

  Fixpoint fold_right2 (l1: list B) (l2: list C) (a0: A) :=
    match l1, l2 with
    | [], [] => Some a0
    | x1 :: l1, x2 :: l2 =>
      doo r <- fold_right2 l1 l2 a0;
      Some (f r x1 x2)
    | _, _ => None
    end.

  Lemma fold_right2_Some: forall l1 l2 i,
    (exists r, fold_right2 l1 l2 i = Some r) <-> length l1 = length l2.
  Proof.
    induction l1; destruct l2; simpl; intros.
    + split; eauto.
    + split. intros [r H]; discriminate. discriminate.
    + split. intros [r H]; discriminate. discriminate.
    + split.
      - intros [r H]. destruct fold_right2 eqn:Hr; try discriminate.
        revert H; intros [= <-]. apply eq_S, (IHl1 l2 i). eauto.
      - intros H. apply eq_add_S in H. apply (IHl1 l2 i) in H as [r ->].
        simpl. eauto.
  Qed.

  Lemma fold_right2_app: forall l1 l2 l1' l2' i i',
    fold_right2 l1' l2' i = Some i' ->
    fold_right2 (l1 ++ l1') (l2 ++ l2') i = fold_right2 l1 l2 i'.
  Proof.
    induction l1; destruct l2; try discriminate.
    + intros. auto.
    + intros. simpl.
      assert (length l1' = length l2')
        by (apply (fold_right2_Some l1' l2' i); eauto).
      clear H. revert l1' c l2 l2' H0. induction l1'; simpl; intros.
      - reflexivity.
      - destruct l2'. discriminate. simpl in H0. apply eq_add_S in H0.
        destruct l2; simpl.
        * pose proof (IHl1' c0 [] l2' H0). simpl in H. rewrite H. reflexivity.
        * pose proof (IHl1' c1 (l2 ++ [c0]) l2' H0).
          rewrite <- app_assoc in H. simpl in H. rewrite H. reflexivity.
    + intros. simpl ((a :: l1) ++ l1'). simpl ([] ++ l2').
      simpl (fold_right2 (a :: l1) [] i').
      assert (length l1' = length l2')
        by (apply (fold_right2_Some l1' l2' i); eauto).
      clear H IHl1. revert l2' a l1 l1' H0. induction l2'; simpl; intros.
      - reflexivity.
      - destruct l1'. discriminate. simpl in H0. apply eq_add_S in H0.
        destruct l1.
        * pose proof (IHl2' b [] l1' H0). simpl (b :: [] ++ l1') in H.
          simpl ([] ++ b :: l1'). rewrite H. reflexivity.
        * pose proof (IHl2' b0 (l1 ++ [b]) l1' H0).
          rewrite <- app_assoc in H.
          simpl ((b0 :: l1) ++ b :: l1'). simpl (b0 :: l1 ++ [b] ++ l1') in H.
          rewrite H. reflexivity.
    + intros. simpl. rewrite (IHl1 _ _ _ _ _ H). reflexivity.
  Qed.
End Fold_Right2_Recursor.

Arguments fold_right2 {_} {_} {_}.

Section Fold_Right3_Recursor.
  Variables (A B C D: Type).
  Variable f : A -> B -> C -> D -> A.

  Fixpoint fold_right3 (l1: list B) (l2: list C) (l3: list D) (a0: A) :=
    match l1, l2, l3 with
    | [], [], [] => Some a0
    | x1 :: l1, x2 :: l2, x3 :: l3 =>
      doo r <- fold_right3 l1 l2 l3 a0;
      Some (f r x1 x2 x3)
    | _, _, _ => None
    end.

  Lemma fold_right3_Some: forall l1 l2 l3 i,
    (exists r, fold_right3 l1 l2 l3 i = Some r) <-> length l1 = length l2 /\ length l2 = length l3.
  Proof.
    induction l1; destruct l2; destruct l3; simpl; intros.
    + split; eauto.
    + split. intros [r H]; discriminate. case. discriminate.
    + split. intros [r H]; discriminate. case. discriminate.
    + split. intros [r H]; discriminate. case. discriminate.
    + split. intros [r H]; discriminate. case. discriminate.
    + split. intros [r H]; discriminate. case. discriminate.
    + split. intros [r H]; discriminate. case. discriminate.
    + split.
      - intros [r H]. destruct fold_right3 eqn:Hr; try discriminate.
        revert H; intros [= <-]. case (proj1 (IHl1 l2 l3 i)); eauto.
      - intros [H1 H2]. apply eq_add_S in H1, H2.
        case (proj2 (IHl1 l2 l3 i) (conj H1 H2)) as [* ->]; simpl; eauto.
  Qed.

  (* Lemma fold_right3_app: forall l1 l2 l3 l1' l2' l3' i i',
    fold_right3 l1' l2' l3' i = Some i' ->
    fold_right3 (l1 ++ l1') (l2 ++ l2') (l3 ++ l3') i = fold_right3 l1 l2 l3 i'.
  Proof.
    induction l1; destruct l2; try discriminate.
    + intros. auto.
    + intros. simpl.
      assert (length l1' = length l2')
        by (apply (fold_right2_Some l1' l2' i); eauto).
      clear H. revert l1' c l2 l2' H0. induction l1'; simpl; intros.
      - reflexivity.
      - destruct l2'. discriminate. simpl in H0. apply eq_add_S in H0.
        destruct l2; simpl.
        * pose proof (IHl1' c0 [] l2' H0). simpl in H. rewrite H. reflexivity.
        * pose proof (IHl1' c1 (l2 ++ [c0]) l2' H0).
          rewrite <- app_assoc in H. simpl in H. rewrite H. reflexivity.
    + intros. simpl ((a :: l1) ++ l1'). simpl ([] ++ l2').
      simpl (fold_right2 (a :: l1) [] i').
      assert (length l1' = length l2')
        by (apply (fold_right2_Some l1' l2' i); eauto).
      clear H IHl1. revert l2' a l1 l1' H0. induction l2'; simpl; intros.
      - reflexivity.
      - destruct l1'. discriminate. simpl in H0. apply eq_add_S in H0.
        destruct l1.
        * pose proof (IHl2' b [] l1' H0). simpl (b :: [] ++ l1') in H.
          simpl ([] ++ b :: l1'). rewrite H. reflexivity.
        * pose proof (IHl2' b0 (l1 ++ [b]) l1' H0).
          rewrite <- app_assoc in H.
          simpl ((b0 :: l1) ++ b :: l1'). simpl (b0 :: l1 ++ [b] ++ l1') in H.
          rewrite H. reflexivity.
    + intros. simpl. rewrite (IHl1 _ _ _ _ _ H). reflexivity.
  Qed. *)
End Fold_Right3_Recursor.

Arguments fold_right3 {_} {_} {_} {_}.

Section Fold_Right_res_Recursor.
  Variables (A: Type) (B: Type).
  Variable f : A -> B -> res A.

  Local Open Scope error_monad_scope.

  Fixpoint fold_right_res (l: list B) (a0: A) : res A :=
    match l with
    | [] => OK a0
    | x :: l =>
      do r <- fold_right_res l a0;
      f r x
    end.

  Lemma fold_right_res_app:
    forall (l1 l2: list B) a0,
      fold_right_res (l1 ++ l2) a0 =
        do a1 <- fold_right_res l2 a0;
        fold_right_res l1 a1.
  Proof.
    elim/list_ind => //=.
    - move=>>. by case fold_right_res.
    - move=> x1 l1 IH l2 a0. rewrite IH. by case fold_right_res.
  Qed.
End Fold_Right_res_Recursor.

Arguments fold_right_res {_} {_}.

Section Fold_Right2_res_Recursor.
  Variables (A: Type) (B: Type) (C: Type).
  Variable f : A -> B -> C -> res A.

  Local Open Scope error_monad_scope.

  Fixpoint fold_right2_res (l1: list B) (l2: list C) (a0: A) : res A :=
    match l1, l2 with
    | [], [] => OK a0
    | x1 :: l1, x2 :: l2 =>
      do r <- fold_right2_res l1 l2 a0;
      f r x1 x2
    | _, _ => Error [MSG "List lengths are different."]
    end.

  Lemma fold_right2_res_OK:
    forall (l1: list B) (l2: list C) (a0: A) r,
      fold_right2_res l1 l2 a0 = OK r ->
      length l1 = length l2.
  Proof.
    elim/list_ind => /=.
    - by case.
    - move=>> IH [] //= >. case H: fold_right2_res => //=.
      by rewrite (IH _ _ _ H).
  Qed.
End Fold_Right2_res_Recursor.

Arguments fold_right2_res {_} {_} {_}.

Section Fold_Left_res_Recursor.
  Variables (A: Type) (B: Type).
  Variable f : A -> B -> res A.

  Local Open Scope error_monad_scope.

  Fixpoint fold_left_res (a0: A) (l: list B) : res A :=
    match l with
    | [] => OK a0
    | x :: l =>
      do r <- f a0 x;
      fold_left_res r l 
    end.
End Fold_Left_res_Recursor.

Arguments fold_left_res {_} {_}.

Section Fold_Left2_res_Recursor.
  Variables (A: Type) (B: Type) (C: Type).
  Variable f : A -> B -> C -> res A.

  Local Open Scope error_monad_scope.

  Fixpoint fold_left2_res (a0: A) (l1: list B) (l2: list C) : res A :=
    match l1, l2 with
    | [], [] => OK a0
    | x1 :: l1, x2 :: l2 =>
      do r <- f a0 x1 x2;
      fold_left2_res r l1 l2
    | _, _ => Error [MSG "List lengths are different."]
    end.
End Fold_Left2_res_Recursor.

Arguments fold_left2_res {_} {_} {_}.

Section MapOption.

Context {A B: Type}.
Variable f : A -> option B.

Fixpoint map_option (l: list A) : option (list B) :=
  match l with
  | [] => Some []
  | x :: l =>
      doo rl <- map_option l;
      doo rx <- f x;
      Some (rx :: rl)
  end.

End MapOption.

Section Map2.

Context {A B C: Type}.
Variable f : A -> B -> C.

Fixpoint map2 (l1: list A) (l2: list B) : option (list C) :=
  match l1, l2 with
  | [], [] => Some []
  | x1 :: l1, x2 :: l2 =>
    doo r <- map2 l1 l2;
    Some (f x1 x2 :: r)
  | _, _ => None
  end.

Lemma map2_Some:
  forall (l1: list A) (l2: list B),
    (exists (l: list C), map2 l1 l2 = Some l) <-> length l1 = length l2.
Proof.
  elim/list_ind => /=.
  - case=> [|_ >] //=; apply: conj => //. eauto. by case.
  - move=>> IH [|b l2] //=; apply: conj.
    + by case.
    + easy.
    + move: (proj1 (IH l2)). case map2 => /= >.
      * move/(_ (ex_intro _ _ eq_refl)) <- => //.
      * move/[apply] <- => //.
    + move/eq_add_S/(proj2 (IH _)). case=>> -> /=. eauto.
Qed.

End Map2.

Section Filter2.

Context {A B: Type}.
Variable f : A -> B -> bool.

Fixpoint filter2 (la:list A) (lb: list B) : option (list A) :=
  match la, lb with
  | [], [] => Some []
  | a :: la, b :: lb =>
      doo r <- filter2 la lb;
      if f a b then Some (a :: r) else Some r
  | _, _ => None
  end.

Lemma filter2_In:
  forall a la lb l,
    filter2 la lb = Some l ->
    In a l <-> exists b, In (a, b) (combine la lb) /\ f a b = true.
Proof.
  move=> ?. elim/list_ind => [[> [<-]|//]|a la IH lb l] /=.
  { apply: conj => // - [> []] //. }
  case: lb => // b lb. case H: (filter2 la lb) => //=.
  case Hf: (f a b) => - [<-] /=; split.
  - case=> [<-|]. exists b. by split; [left|].
    move/(IH lb _ H). case=> b' [??]. exists b'. by split; [right|].
  - case=> b' [[]]. by case=> <-; left.
    move=> Hin Hf'. right. apply: (proj2 (IH _ _ H)). by exists b'.
  - move/(IH _ _ H). case=> b' [Hin Hf']. by exists b'; split; [right|].
  - case=> b' [[[<- <-]|]]. by rewrite Hf.
    move=> Hc Hf'. apply: (proj2 (IH _ _ H)). by exists b'.
Qed.

End Filter2.

Section SkipN_Lemmas.

Lemma skipn_incl {A: Type}:
  forall n (l l': list A),
    skipn n l = l' ->
    incl l' l.
Proof.
  intros n l. revert n. induction l.
  destruct n; simpl; intros * <-; apply incl_refl.
  destruct n; simpl; intros * <-.
  - apply incl_cons. apply in_eq. apply incl_tl, incl_refl.
  - apply incl_tl, (IHl _ _ eq_refl).
Qed.

Lemma skipn_inv_tl {A: Type}:
  forall n (l l': list A) a,
    skipn n l = a :: l' ->
    skipn (S n) l = l'.
Proof.
  induction n; simpl; intros. rewrite H. reflexivity.
  destruct l. discriminate. apply IHn in H. assumption.
Qed.

Lemma skipn_nth_error {A: Type}:
  forall n (l l': list A) a,
    skipn n l = a :: l' ->
    nth_error l n = Some a.
Proof.
  induction n; simpl; intros. rewrite H. reflexivity.
  destruct l. discriminate. apply IHn in H. assumption.
Qed.

Lemma skipn_nil2 {A: Type}:
  forall n (l: list A),
    skipn n l = [] ->
    n >= length l.
Proof.
  induction n; simpl; intros. rewrite H. simpl. lia.
  destruct l. simpl. lia. specialize IHn with (1 := H). simpl. lia.
Qed.

Lemma skipn_S {A: Type}:
  forall (l: list A) n x l',
    skipn n l = x :: l' ->
    skipn (S n) l = l'.
Proof.
  move=> l n x l' H.
  have Hn: (n < Datatypes.length l)%nat.
  { case (PeanoNat.Nat.ltb_spec0 n (Datatypes.length l)). lia.
    move=> T. exfalso. move: H. rewrite skipn_all2. lia. easy. }
  rewrite -(firstn_skipn n l) H.
  change (?a :: ?b) with ([a] ++ b). rewrite app_assoc skipn_app.
  have L: Datatypes.length (firstn n l ++ [x]) = S n.
  { rewrite app_length firstn_length min_l /=; lia. }
  rewrite L -{1}L. by rewrite skipn_all PeanoNat.Nat.sub_diag.
Qed.

End SkipN_Lemmas.

Section Forallb2.

Variable A B: Type.
Variable f: A -> B -> bool.

Fixpoint forallb2 (l1:list A) (l2: list B) : bool :=
  match l1, l2 with
  | [], [] => true
  | _, [] | [], _ => false
  | x1 :: l1, x2 :: l2 => f x1 x2 && forallb2 l1 l2
  end.

Lemma forallb2_forall:
  forall l1 l2, forallb2 l1 l2 = true <->
             (length l1 = length l2 /\
              forall n x1 x2,
                nth_error l1 n = Some x1 ->
                nth_error l2 n = Some x2 ->
                f x1 x2 = true).
Proof.
  elim=> [|> IH] []; try easy.
  - split=> //= _; split=> // - [] //.
  - move=>> /=; split.
    + case/andP => F /IH [->] H; split=> // - [] //= > [<-] [<-] //.
    + case=> /eq_add_S LEN H; rewrite (H 0 _ _ eq_refl eq_refl) /=.
      apply IH; split=> // n; exact (H (S n)).
Qed.

End Forallb2.

Arguments forallb2 {_} {_}.

Definition list_In_rect {A: Type} (l: list A):
  forall (P: list A -> Type),
         P [] ->
         (forall (x: A) (l0: list A), P l0 -> In x l -> P (x :: l0)) ->
         P l.
  induction l.
  + intros P a0 _. exact a0.
  + intros P a0 H. apply H. apply IHl. exact a0.
    intros * H0 Hin. apply H. exact H0. right. exact Hin.
    left. reflexivity.
Qed.

Lemma list_In_ind {A: Type} (l: list A):
  forall (P : list A -> Prop),
    P [] ->
    (forall (a : A) (l0 : list A), P l0 -> In a l -> P (a :: l0)) ->
    P l.
Proof.
  induction l; simpl; intros P H R. exact H.
  apply R. apply IHl. exact H.
  intros a0 l0 H0 Hin. apply R. exact H0. right. exact Hin.
  left. reflexivity.
Qed.

Lemma forallb_false_exists_first {A: Type} {P: A -> bool}:
  forall l, ~~ forallb P l ->
       exists l1 c l2, l = l1 ++ c :: l2 /\
                  forallb P l1 /\
                  ~~ P c.
Proof.
  elim/list_ind => //=.
  move=> a l IH. case Ha: (P a) => /=.
  - case/IH => l1 [c [l2 [-> [H1 H2]]]]. exists (a :: l1), c, l2 => /=. by rewrite Ha.
  - exists [], a, l. by rewrite Ha.
Qed.

Lemma Forall2_In {A B: Type} (P: A -> B -> Prop):
  forall la lb,
    Forall2 P la lb ->
    forall a, In a la -> exists b, In b lb /\ P a b.
Proof.
  elim/list_ind => //= a la IH [|b lb] T; inv T =>>. case=> [<-|Hin] /=.
  - eauto.
  - have [b' [Hin' H]] := IH _ H4 _ Hin. eauto.
Qed.

Lemma Forall2_nth_error {A B: Type} (P: A -> B -> Prop):
  forall la lb,
    Forall2 P la lb ->
    forall a n, nth_error la n = Some a -> exists b, nth_error lb n = Some b /\ P a b.
Proof.
  elim/list_ind => //=.
  - move=>> T; inv T => ? [] //.
  - move=> a la IH [|b lb] T; inv T => a' [|n] /=.
    + case=> <-. eauto.
    + case/(IH _ H4) => b' [->]. eauto.
Qed.

Lemma Forall2_nth_error' {A B: Type} (P: A -> B -> Prop):
  forall l1 l2 n a b,
    Forall2 P l1 l2 ->
    nth_error l1 n = Some a ->
    nth_error l2 n = Some b ->
    P a b.
Proof.
  elim/list_ind.
  - move=> + [] //.
  - move=>> IH [|??] n > T; inv T. case: n =>> /=. congruence. eauto.
Qed.

Lemma Forall2_forall {A B: Type} (P: A -> B -> Prop):
  forall l1 l2,
    Forall2 P l1 l2 <->
    length l1 = length l2 /\
    forall n a b,
      nth_error l1 n = Some a ->
      nth_error l2 n = Some b ->
      P a b.
Proof.
  move=> l1 l2; split.
  - move=> H. split. apply: Forall2_length H.
    move=>>. apply: Forall2_nth_error' H.
  - elim/list_ind: l1 l2.
    + case=>> [] //.
    + move=>> IH [|??] [] //= /eq_add_S LEN H. econstructor.
      exact (H 0 _ _ eq_refl eq_refl).
      exact (IH _ (conj LEN (fun n => H (S n)))).
Qed.

Inductive Forall3 {A B C : Type} (R : A -> B -> C -> Prop) : list A -> list B -> list C -> Prop :=
| Forall3_nil : Forall3 R [] [] []
| Forall2_cons : forall x y z l l' l'',
  R x y z -> Forall3 R l l' l'' -> Forall3 R (x :: l) (y :: l') (z :: l'').

Lemma In_combine_l {A B: Type}:
  forall (l1: list A) (l2: list B) x,
    length l1 = length l2 ->
    In x l1 ->
    exists y, In (x, y) (combine l1 l2).
Proof.
  elim/list_ind => //= a l1 IH [//|b l2] /= x /eq_add_S LEN.
  case=> [->|Hin]. eauto. case (IH _ _ LEN Hin) =>>. eauto.
Qed.

Lemma In_combine_r {A B: Type}:
  forall (l1: list A) (l2: list B) y,
    length l1 = length l2 ->
    In y l2 ->
    exists x, In (x, y) (combine l1 l2).
Proof.
  move=> /[swap]. elim/list_ind => //= a l1 IH [//|b l2] /= y /eq_add_S LEN.
  case=> [->|Hin]. eauto. case (IH _ _ LEN Hin) =>>. eauto.
Qed.

Lemma combine_map_fst {A B: Type}:
  forall (l1: list A) (l2: list B),
    length l1 = length l2 ->
    map fst (combine l1 l2) = l1.
Proof.
  elim/list_ind => //= a l1 IH [//|b l2] /= /eq_add_S LEN.
  by rewrite (IH _ LEN).
Qed.

Lemma combine_eq_app {A B: Type}:
  forall (l1: list A) (l2: list B) (r1 r2: list (A * B)),
    length l1 = length l2 ->
    combine l1 l2 = r1 ++ r2 ->
    exists l11 l12 l21 l22,
      l1 = l11 ++ l12 /\ l2 = l21 ++ l22 /\
      length l11 = length l21 /\ length l12 = length l22 /\
      combine l11 l21 = r1 /\ combine l12 l22 = r2.
Proof.
  elim/list_ind => /=.
  - case=> //= - [] //= [] //= _ _. by exists [], [], [], [].
  - move=> a1 l1 IH [//|a2 l2] [|x r1] r2 /= /eq_add_S LEN.
    + exists [], (a1 :: l1), [], (a2 :: l2) => /=. eauto 10.
    + case=> <- H. have:= IH _ _ _ LEN H.
      intros (l11 & l12 & l21 & l22 & -> & -> & LEN1 & LEN2 & H1 & H2).
      exists (a1 :: l11), l12, (a2 :: l21), l22. simpl.
      repeat split; congruence.
Qed.

Lemma combine_eq_cons {A B: Type}:
  forall (l1: list A) (l2: list B) (x: A * B) (r: list (A * B)),
    combine l1 l2 = x :: r ->
    exists a1 a2 l1' l2',
      x = (a1, a2) /\ l1 = a1 :: l1' /\ l2 = a2 :: l2' /\
      combine l1' l2' = r.
Proof. case=> [|a1 l1'] //= [|a2 l2'] //= > [<- <-]; eauto 10. Qed.

Lemma combine_app {A B: Type}:
  forall (l11 l12: list A) (l21 l22: list B),
    length l11 = length l21 ->
    combine (l11 ++ l12) (l21 ++ l22) = combine l11 l21 ++ combine l12 l22.
Proof. elim/list_ind => [|> IH] ? [] > //= /eq_add_S /IH -> //. Qed.

Lemma split_eq_app {A B: Type}:
  forall la1 la2 lb1 lb2 (l: list (A * B)),
    length la1 = length lb1 ->
    length la2 = length lb2 ->
    length l = length la1 + length la2 ->
    split l = (la1 ++ la2, lb1 ++ lb2) ->
    exists l1 l2,
      l = l1 ++ l2 /\
      length l1 = length la1 /\ length l2 = length la2 /\
      split l1 = (la1, lb1) /\ split l2 = (la2, lb2).
Proof.
  elim/list_ind => /=.
  - move=> ? [|//] ? l /= _ -> <-. by exists [], l.
  - move=>> IH la2 [//|? lb1] lb2 [//|[a b] l] /=.
    move/eq_add_S => LEN1 LEN2 /eq_add_S LEN.
    case Sp: split. intros [= <- -> <- ->].
    have:= IH _ _ _ _ LEN1 LEN2 LEN Sp.
    intros (l1 & l2 & -> & <- & <- & T1 & T2).
    exists ((a, b) :: l1), l2. repeat split=> //=. by rewrite T1.
Qed.

Lemma split_app {A B: Type}:
  forall (l1 l2: list (A * B)) ll lr,
    split (l1 ++ l2) = (ll, lr) ->
    exists ll1 lr1 ll2 lr2,
      ll = ll1 ++ ll2 /\ lr = lr1 ++ lr2 /\
      split l1 = (ll1, lr1) /\
      split l2 = (ll2, lr2).
Proof.
  elim/list_ind => /=.
  - move=> ? ll lr ->. by exists [], [], ll, lr.
  - move=> [l r] > IH >. case Sp: split => - [<- <-].
    have [> [> [> [> [-> [-> [-> ->]]]]]]]:= IH _ _ _ Sp.
    rewrite !app_comm_cons. repeat eexists.
Qed.
