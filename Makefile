BFRONTENDPROOF=BtoCSharpMinorproof.v NBtoBproof.v Safety.v

BFRONTEND=Tactics.v BUtils.v ListUtils.v PTreeaux.v \
	Types.v BValues.v BPrinters.v Validity.v \
	Ops.v BEnv.v Syntax.v Alias.v Typing.v \
	SemPath.v SemanticsCommon.v ExprSem.v \
	SemanticsBlocking.v Bfacts.v \
	SemanticsNonBlocking.v NBfacts.v \
	FloatCastTests.v \
	BtoCSharpMinor.v NBtoB.v \
	BigStepNBSemantics.v \
	$(BFRONTENDPROOF)

.all: $(BFRONTEND)
	cd ..; make

TEST_DIR=test
BENCH_DIR=benchmarks

ADD_MATRIX=add_matrix.b test_add_matrix.c
GAUSS1=gauss1.b test_gauss1.c
GAUSS2=gauss2.b test_gauss2.c
GCD_POLY=gcd_poly.b test_gcd_poly.c
MUL_MATRIX1=mul_matrix1.b test_mul_matrix.c
MUL_MATRIX2=mul_matrix2.b test_mul_matrix.c
ERATOSTHENE=eratosthene.b test_eratosthene.c
BLOCK_MUL_MATRIX=block_mul_matrix.b test_block_mul_matrix.c

ZDOTU=zdotu.b zdotu_assert.b zdotu_unidimensional.b test_zdotu.c
SAXPY=saxpy.b saxpy_assert.b saxpy_unrolling.b saxpy_unrolling_assert.b test_saxpy.c
SGEMV=sgemv.b sgemv_assert.b test_sgemv.c
DGEMV=dgemv.b dgemv_assert.b test_dgemv.c
DTRSV=dtrsv.b dtrsv_assert.b test_dtrsv.c

TEST_FILES=$(addprefix $(TEST_DIR)/, $(ADD_MATRIX) $(GAUSS1) $(GAUSS2) \
	$(GCD_POLY) $(MUL_MATRIX1) $(MUL_MATRIX2) $(ERATOSTHENE) \
	$(BLOCK_MUL_MATRIX))

BENCH_FILES=$(addprefix $(BENCH_DIR)/, $(ZDOTU) $(SAXPY) \
	$(SGEMV) $(DGEMV) $(DTRSV))

TEST_BIN_DIR=$(TEST_DIR)/output

TEST_CCOMP=../ccomp

.DEFAULT_GOAL := all

$(BENCH_DIR)/zdotu_norestrict.c: $(BENCH_DIR)/zdotu.b
	$(TEST_CCOMP) -dbc -c $< \
	sed 's/restrict//' zdotu.c | sed 's/zdotu/zdotu_norestrict/' > $(BENCH_DIR)/zdotu_norestrict.c

BPARSER=bfrontend/parser/bparser.mly

bfrontend/parser/bparser.ml bfrontend/parser/bparser.mli: bfrontend/parser/bparser.mly
	menhir --table -v --no-stdlib -la 1 --inspection $<

b_benchmarks:
	./benchmarks/benchmark.sh

b_tests: $(TEST_FILES) $(BENCH_FILES) $(TEST_CCOMP)
	mkdir -p $(TEST_BIN_DIR)
	$(TEST_CCOMP) $(addprefix $(TEST_DIR)/, $(ADD_MATRIX)) -o $(TEST_BIN_DIR)/add_matrix
	$(TEST_CCOMP) $(addprefix $(TEST_DIR)/, $(GAUSS1)) -o $(TEST_BIN_DIR)/gauss1
	$(TEST_CCOMP) $(addprefix $(TEST_DIR)/, $(GAUSS2)) -o $(TEST_BIN_DIR)/gauss2
	$(TEST_CCOMP) $(addprefix $(TEST_DIR)/, $(GCD_POLY)) -o $(TEST_BIN_DIR)/gcd_poly
	$(TEST_CCOMP) $(addprefix $(TEST_DIR)/, $(MUL_MATRIX1)) -o $(TEST_BIN_DIR)/mul_matrix1
	$(TEST_CCOMP) $(addprefix $(TEST_DIR)/, $(MUL_MATRIX2)) -o $(TEST_BIN_DIR)/mul_matrix2
	$(TEST_CCOMP) $(addprefix $(TEST_DIR)/, $(ERATOSTHENE)) -o $(TEST_BIN_DIR)/eratosthene
	$(TEST_CCOMP) $(addprefix $(TEST_DIR)/, $(BLOCK_MUL_MATRIX)) -o $(TEST_BIN_DIR)/block_mul_matrix
	$(TEST_CCOMP) $(addprefix $(BENCH_DIR)/, $(ZDOTU)) -o $(TEST_BIN_DIR)/zdotu -lcblas
	$(TEST_CCOMP) $(addprefix $(BENCH_DIR)/, $(SAXPY)) -o $(TEST_BIN_DIR)/saxpy -lcblas
	$(TEST_CCOMP) $(addprefix $(BENCH_DIR)/, $(SGEMV)) -o $(TEST_BIN_DIR)/sgemv -lcblas
	$(TEST_CCOMP) $(addprefix $(BENCH_DIR)/, $(DGEMV)) -o $(TEST_BIN_DIR)/dgemv -lcblas
	$(TEST_CCOMP) $(addprefix $(BENCH_DIR)/, $(DTRSV)) -o $(TEST_BIN_DIR)/dtrsv -lcblas

B_TEST_FILES=$(filter %.b,$(TEST_FILES) $(BENCH_FILES))
TEST_V_FILES=$(patsubst %.b,%.v,$(filter %.b,$(notdir $(B_TEST_FILES))))

$(TEST_V_FILES): $(B_TEST_FILES)
	$(TEST_CCOMP) $^ -dbcoq -c

b_test_coq_output: $(TEST_V_FILES)
	mkdir -p $(TEST_BIN_DIR)
	for f in $^; do mv $$f $(TEST_BIN_DIR)/$$f; done
	rm $(TEST_V_FILES:.v=.o)
	for f in $^; do coqc $(TEST_BIN_DIR)/$$f; done

b_clean-tests:
	rm -r $(TEST_BIN_DIR)

b_clean: .clean-tests
	rm -f *.vos *.vo *.vok
	rm -f *.cmo *.cmi *.cmt *.cmx *.o
	rm -f parser/blexer.ml parser/bparser.ml parser/bparser.mli
	rm -f parser/*.cmx parser/*.cmi parser/*.o parser/*.cmt parser/*.cmo parser/*.cmti
	rm -f parser/bparser.automaton parser/bparser.conflicts
