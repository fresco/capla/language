Require Import Utf8.
Require Import ssreflect ssrfun ssrbool.

Require Import ZArith Lia.
Require Import String Ascii List.

Import ListNotations.

Require Import BValues Ops Types Syntax Typing BUtils PTreeaux Tactics.
Require Import BEnv Validity SemPath.
Require Import SemanticsCommon.
Require SemanticsNonBlocking.
Module NB := SemanticsNonBlocking.
Require Import ExprSem.

Require Import Integers Maps.
Require Smallstep.

Section EVAL_EXPR_FIXPOINT_MATCH.

Local Open Scope outcome_scope.

Variables (e: env) (f: function).

Theorem eval_identlist_fixpoint_match:
  forall lids lv,
    NB.eval_identlist e f lids lv <->
    eval_ident_list e f lids = Val lv.
Proof.
  elim/list_ind.
  - split=>> H. by inv H. case: H => <-. by econstructor.
  - move=>> IH >; split => H.
    + inv H => /=. rewrite H2 H3 /=. by rewrite (proj1 (IH _) H5).
    + move: H => /=. case E: e!_ => //=. case S: (fn_szenv' f)!_ => [[]|] //=.
      all: case I: eval_ident_list =>> //= [<-].
      have:= proj2 (IH _) I. by econstructor.
Qed.

Ltac cbn' := cbn -[get_env_path].

Theorem eval_expr_fixpoint_match_Some:
  forall exp v,
    NB.eval_expr e f exp (Some v) <-> eval_expr e f exp = Val v.
Proof.
  fix IH 1. case.
  - case=> [i l] v. cbn'.
    set F := fix __f _ (l0: list syn_path_elem) := _.
    have H: forall llsz l', NB.eval_path_elem_list e f l llsz (Some l') <->
                            F llsz l = Val l'.
    { elim/list_ind: l => /=.
      - case=>>; split=> [X /ltac:(inv X) //|[<-]]; by econstructor.
      - case=> lidx > IH' [] > /=.
        + split=> // T. by inv T.
        + change (fix eval_expr (exp: expr) := _
                  with eval_syn_path_elem lsz (s: syn_path_elem) := _
                  for eval_expr) with (eval_expr e f).
          set G := fix __f (l: list expr) := _.
          have H: forall lv, NB.eval_exprlist e f lidx (Some lv) <->
                             G lidx = Val lv.
          { elim/list_ind: lidx.
            - move=>>; split=> [T /ltac:(inv T) //|[<-]]; by econstructor.
            - move=>> IHlidx >; split=> [T|].
              + inv T => /=. rewrite (proj1 (IH _ _) H2) (proj1 (IHlidx _) H3) //.
              + move=> /=. case E: eval_expr => //=.
                case El: G =>> //= [<-]. econstructor.
                by apply IH. by apply IHlidx. }
          split=> [T|].
          * inv T. rewrite /= (proj1 (eval_identlist_fixpoint_match _ _) H5) /=.
            rewrite H6 /= (proj1 (H _) H7) /= H8 /=.
            rewrite H9 /= (proj1 (IH' _ _) H10) //.
          * case Elsz: eval_ident_list => //=. case S: natlist_of_Vint64 => //=.
            case Elidx: G => //=. case Bidx: build_index => //=.
            case V: valid_index => //=. case El: F =>> //= [<-].
            move/eval_identlist_fixpoint_match in Elsz.
            move/H in Elidx. move/IH' in El. econstructor; eassumption. }
    split.
    + move=> T. inv T. inv H2. rewrite H6. cbn'.
      rewrite (proj1 (H _ _) H7); cbn'. by rewrite H3 /= H4.
    + case S: (fn_szenv' f)!_ => //; cbn'. case E: F => //; cbn'.
      case P: get_env_path => //=. case Pr: primitive_value =>> // [<-].
      move/H in E. eauto with semanticsnb.
  - case=>>; split=> [T /ltac:(inv T) //|[<-]]; econstructor.
  - move=>>; split=> [T /ltac:(inv T)|]; cbn.
    + rewrite (proj1 (IH _ _) H1) /= H4 //.
    + case E: eval_expr => //=. case S: sem_cast =>> //=.
      move=> [<-]. move/IH in E. econstructor; eassumption.
      by case (_ && _).
  - move=>>; split=> [T /ltac:(inv T)|]; cbn.
    + rewrite (proj1 (IH _ _) H1) /= H4 //.
    + case E: eval_expr => //=; case S: sem_unop =>> //= [<-].
      move/IH in E. econstructor; eassumption.
  - move=>>; split=> [T /ltac:(inv T)|]; cbn.
    + rewrite (proj1 (IH _ _) H2) (proj1 (IH _ _) H5) /= H6 //.
    + case E1: eval_expr => //=; case E2: eval_expr => //=.
      case S: sem_binarith_operation =>> //=.
      move=> [<-]. move/IH in E1. move/IH in E2. econstructor; eassumption.
      by case binarith_operation_allowed.
  - move=>>; split=> [T /ltac:(inv T)|]; cbn.
    + rewrite (proj1 (IH _ _) H2) (proj1 (IH _ _) H5) /= H6 //.
    + case E1: eval_expr => //=; case E2: eval_expr => //=.
      case S: sem_cmp_operation =>> //= [<-].
      move/IH in E1. move/IH in E2. econstructor; eassumption.
  - move=>>; split=> [T /ltac:(inv T)|]; cbn.
    + rewrite (proj1 (IH _ _) H2) (proj1 (IH _ _) H5) /= H6 //.
    + case E1: eval_expr => //=; case E2: eval_expr => //=.
      case S: sem_cmpu_operation =>> //= [<-].
      move/IH in E1. move/IH in E2. econstructor; eassumption.
Qed.

Theorem eval_exprlist_fixpoint_match_Some:
  forall le lv,
    NB.eval_exprlist e f le (Some lv) <-> eval_expr_list e f le = Val lv.
Proof.
  elim/list_ind.
  - move=>>; split=> [T /ltac:(inv T) //|[<-]]; by econstructor.
  - move=>> IH >; split=> [T|].
    + inv T => /=. rewrite (proj1 (eval_expr_fixpoint_match_Some _ _) H2).
      rewrite (proj1 (IH _) H3) //.
    + move=> /=. case E: eval_expr => //=.
      case El: eval_expr_list =>> //= [<-]. econstructor.
      by apply eval_expr_fixpoint_match_Some. by apply IH.
Qed.

Lemma eval_expr_list_eq:
  forall le,
    (eval_expr e f |@ le) = eval_expr_list e f le.
Proof. elim/list_ind => //= > -> //. Qed.

Lemma eval_path_elem_eq:
  forall lsz s,
    eval_path_elem e f lsz s =
    match s with
    | Scell lidx =>
        do lvsz <- eval_ident_list e f lsz;
        do shape <- natlist_of_Vint64 lvsz;
        do lvidx <- eval_expr_list e f lidx;
        do idx <- build_index lvidx shape;
        if valid_index lvidx lvsz then Val (Pcell idx) else Err
    end.
Proof. move=> ? [] //. Qed.

Theorem eval_expr_fixpoint_match_None:
  forall exp,
    NB.eval_expr e f exp None <-> eval_expr e f exp = Err.
Proof.
  fix IH 1. case.
  - case=> [i l]. cbn'.
    set F := fix __f _ (l0: list syn_path_elem) := _.
    have H: forall llsz, NB.eval_path_elem_list e f l llsz None <->
                         F llsz l = Err.
    { elim/list_ind: l => /=.
      - case=>>; split=> [T /ltac:(inv T)|] //.
      - case=> lidx > IH' [] >.
        + split=> // T. by inv T.
        + change (fix __f (l: list expr) : outcome (list value) := _)
            with (eval_expr_list e f).
          have H: NB.eval_exprlist e f lidx None <-> eval_expr_list e f lidx = Err.
          { elim/list_ind: lidx.
            - split=> [T /ltac:(inv T)|] //.
            - move=>> IHlidx >; split=> [T|].
              + inv T => /=. rewrite (proj1 (IH _) H0) //.
                rewrite (proj1 (eval_expr_fixpoint_match_Some _ _) H1) /=.
                by rewrite (proj1 IHlidx H2).
              + move=> /=. case E: eval_expr =>> //=; [|intros _].
                case El: eval_expr_list =>> //= _. move/eval_expr_fixpoint_match_Some in E.
                eapply NB.eval_Econs_None. exact E. by apply IHlidx.
                apply NB.eval_Econs_None_e; by apply IH. }
          split=> [T|].
          * inv T; rewrite (proj1 (eval_identlist_fixpoint_match _ _) H4) /= H5 /=.
            -- rewrite (proj1 (eval_exprlist_fixpoint_match_Some _ _) H6) /=.
               rewrite H7 /= H8 //.
            -- by rewrite (proj1 H H6).
            -- rewrite (proj1 (eval_exprlist_fixpoint_match_Some _ _) H6) /=.
               rewrite H7 /= H8 /=.
               by rewrite (proj1 (IH' _) H9).
          * case Elsz: eval_ident_list => //=; [|intros _].
            move/eval_identlist_fixpoint_match in Elsz.
            case S: natlist_of_Vint64 => //=.
            case Elidx: eval_expr_list => //=; [|intros _].
            move/eval_exprlist_fixpoint_match_Some in Elidx.
            case Bidx: build_index => //=.
            case V: valid_index => //=; [|intros _]. case El: F =>> //= _.
            -- move/IH' in El. eapply NB.eval_Scell_ERR_tl; eassumption.
            -- eapply NB.eval_Scell_ERR; eassumption.
            -- move/H in Elidx. eapply NB.eval_Scell_ERR_idx; eassumption.
            -- by move/eval_ident_list_not_Err in Elsz. }
    split.
    + move=> T. inv T. inv H1. rewrite H3 /=. by rewrite (proj1 (H _) H4).
    + case S: (fn_szenv' f)!_ => //; cbn'. case E: F => //; cbn'.
      case P: get_env_path => //=. by case primitive_value.
      move/H in E. eauto with semanticsnb.
  - case=>>; split=> [T /ltac:(inv T)|] //.
  - move=>>; split=> [T|]; cbn.
    + inv T. rewrite (proj1 (eval_expr_fixpoint_match_Some _ _) H2) /= H5 //=.
      by rewrite H4 (well_typed_value_bool_complete _ _ H3).
      by rewrite (proj1 (IH _) H0).
    + case E: eval_expr =>> //=; [|intros _]. case S: sem_cast =>> //=.
      case C: cast_allowed => //=. case WT: well_typed_value_bool => //= _.
      move/well_typed_value_bool_correct in WT.
      move/eval_expr_fixpoint_match_Some in E. eauto with semanticsnb.
      move/IH in E. eauto with semanticsnb.
  - move=>>; split=> [T /ltac:(inv T)|]; cbn.
    + by rewrite (proj1 (IH _) H0).
    + case E: eval_expr => //=; [|intros _]. case S: sem_unop =>> //= _.
      move/IH in E. econstructor; eassumption.
  - move=>>; split=> [T /ltac:(inv T)|]; cbn.
    + by rewrite (proj1 (IH _) H0).
    + by rewrite (proj1 (eval_expr_fixpoint_match_Some _ _ ) H1) (proj1 (IH _) H4).
    + rewrite (proj1 (eval_expr_fixpoint_match_Some _ _) H3).
      rewrite (proj1 (eval_expr_fixpoint_match_Some _ _) H4) /= H5 H6 //.
    + case E1: eval_expr => //=. move/eval_expr_fixpoint_match_Some in E1.
      case E2: eval_expr => //=. move/eval_expr_fixpoint_match_Some in E2.
      case S: sem_binarith_operation =>> //=.
      case B: binarith_operation_allowed => //=. eauto with semanticsnb.
      move/IH in E2. eauto with semanticsnb.
      move/IH in E1. eauto with semanticsnb.
  - move=>>; split=> [T /ltac:(inv T)|]; cbn.
    + by rewrite (proj1 (IH _) H0).
    + by rewrite (proj1 (eval_expr_fixpoint_match_Some _ _ ) H1) (proj1 (IH _) H4).
    + case E1: eval_expr => //=. move/eval_expr_fixpoint_match_Some in E1.
      case E2: eval_expr => //=. move/eval_expr_fixpoint_match_Some in E2.
      by case sem_cmp_operation.
      move/IH in E2. eauto with semanticsnb.
      move/IH in E1. eauto with semanticsnb.
  - move=>>; split=> [T /ltac:(inv T)|]; cbn.
    + by rewrite (proj1 (IH _) H0).
    + by rewrite (proj1 (eval_expr_fixpoint_match_Some _ _ ) H1) (proj1 (IH _) H4).
    + case E1: eval_expr => //=. move/eval_expr_fixpoint_match_Some in E1.
      case E2: eval_expr => //=. move/eval_expr_fixpoint_match_Some in E2.
      by case sem_cmpu_operation.
      move/IH in E2. eauto with semanticsnb.
      move/IH in E1. eauto with semanticsnb.
Qed.

Theorem eval_exprlist_fixpoint_match_None:
  forall le,
    NB.eval_exprlist e f le None <-> eval_expr_list e f le = Err.
Proof.
  elim/list_ind.
  - split=> [T /ltac:(inv T)|] //.
  - move=>> IH >; split=> [T|].
    + inv T => /=.
      by rewrite (proj1 (eval_expr_fixpoint_match_None _) H0).
      by rewrite (proj1 (eval_expr_fixpoint_match_Some _ _) H1) (proj1 IH H2).
    + move=> /=. case E: eval_expr => //=; [|intros _].
      case El: eval_expr_list =>> //= _.
      * eapply NB.eval_Econs_None. apply eval_expr_fixpoint_match_Some, E.
        by apply IH.
      * eapply NB.eval_Econs_None_e. by apply eval_expr_fixpoint_match_None.
Qed.

Theorem eval_path_fixpoint_match_Some:
  forall p p',
    NB.eval_path e f p (Some p') <-> eval_path e f p = Val p'.
Proof.
  case=> [i l] v. cbn'.
  have H: forall llsz l', NB.eval_path_elem_list e f l llsz (Some l') <->
                          eval_path_elem_list e f llsz l = Val l'.
  { elim/list_ind: l => /=.
    - case=>>; split=> [X /ltac:(inv X) //|[<-]]; by econstructor.
    - case=> lidx > IH' [] > /=.
      + split=> // T. by inv T.
      + cbn. change (fix eval_expr (exp: expr) := _
                     with eval_syn_path_elem lsz (s: syn_path_elem) := _
                     for eval_expr) with (eval_expr e f).
        set G := fix __f (l: list expr) := _.
        have H: forall lv, NB.eval_exprlist e f lidx (Some lv) <->
                            G lidx = Val lv.
        { elim/list_ind: lidx.
          - move=>>; split=> [T /ltac:(inv T) //|[<-]]; by econstructor.
          - move=>> IHlidx >; split=> [T|].
            + inv T => /=. rewrite (proj1 (eval_expr_fixpoint_match_Some _ _) H2).
              rewrite (proj1 (IHlidx _) H3) //.
            + move=> /=. case E: eval_expr => //=.
              case El: G =>> //= [<-]. econstructor.
              by apply eval_expr_fixpoint_match_Some. by apply IHlidx. }
        split=> [T|].
        * inv T. rewrite /= (proj1 (eval_identlist_fixpoint_match _ _) H5) /=.
          rewrite H6 /= (proj1 (H _) H7) /= H8 /=.
          rewrite H9 /= (proj1 (IH' _ _) H10) //.
        * case Elsz: eval_ident_list => //=. case S: natlist_of_Vint64 => //=.
          case Elidx: G => //=. case Bidx: build_index => //=.
          case V: valid_index => //=. case El: eval_path_elem_list =>> //= [<-].
          move/eval_identlist_fixpoint_match in Elsz.
          move/H in Elidx. move/IH' in El. econstructor; eassumption. }
  split.
  + move=> T. inv T. by rewrite H3 /= (proj1 (H _ _) H4).
  + case S: (fn_szenv' f)!_ => //; cbn'. case E: eval_path_elem_list =>> //= [<-].
    move/H in E. eauto with semanticsnb.
Qed.

Theorem eval_path_fixpoint_match_None:
  forall p,
    NB.eval_path e f p None <-> eval_path e f p = Err.
Proof.
  case=> [i l]. cbn'.
  have H: forall llsz, NB.eval_path_elem_list e f l llsz None <->
                       eval_path_elem_list e f llsz l = Err.
  { elim/list_ind: l => /=.
    - case=>>; split=> [T /ltac:(inv T)|] //.
    - case=> lidx > IH' [] >.
      + split=> // T. by inv T.
      + rewrite eval_path_elem_eq.
        have H: NB.eval_exprlist e f lidx None <-> eval_expr_list e f lidx = Err.
        { elim/list_ind: lidx.
          - split=> [T /ltac:(inv T)|] //.
          - move=>> IHlidx >; split=> [T|].
            + inv T => /=. rewrite (proj1 (eval_expr_fixpoint_match_None _) H0) //.
              rewrite (proj1 (eval_expr_fixpoint_match_Some _ _) H1) /=.
              by rewrite (proj1 IHlidx H2).
            + move=> /=. case E: eval_expr =>> //=; [|intros _].
              case El: eval_expr_list =>> //= _. move/eval_expr_fixpoint_match_Some in E.
              eapply NB.eval_Econs_None. exact E. by apply IHlidx.
              apply NB.eval_Econs_None_e; by apply eval_expr_fixpoint_match_None. }
        split=> [T|].
        * inv T; rewrite (proj1 (eval_identlist_fixpoint_match _ _) H4) /= H5 /=.
          -- rewrite (proj1 (eval_exprlist_fixpoint_match_Some _ _) H6) /=.
              rewrite H7 /= H8 //.
          -- by rewrite (proj1 H H6).
          -- rewrite (proj1 (eval_exprlist_fixpoint_match_Some _ _) H6) /=.
              rewrite H7 /= H8 /=.
              by rewrite (proj1 (IH' _) H9).
        * case Elsz: eval_ident_list => //=; [|intros _].
          move/eval_identlist_fixpoint_match in Elsz.
          case S: natlist_of_Vint64 => //=.
          case Elidx: eval_expr_list => //=; [|intros _].
          move/eval_exprlist_fixpoint_match_Some in Elidx.
          case Bidx: build_index => //=.
          case V: valid_index => //=; [|intros _]. case El: eval_path_elem_list =>> //= _.
          -- move/IH' in El. eapply NB.eval_Scell_ERR_tl; eassumption.
          -- eapply NB.eval_Scell_ERR; eassumption.
          -- move/H in Elidx. eapply NB.eval_Scell_ERR_idx; eassumption.
          -- by move/eval_ident_list_not_Err in Elsz. }
  split.
  + move=> T. inv T. rewrite H2 /=. by rewrite (proj1 (H _) H3).
  + case S: (fn_szenv' f)!_ => //; cbn'. case E: eval_path_elem_list =>> //= _.
    move/H in E. eauto with semanticsnb.
Qed.

Theorem eval_full_path_fixpoint_match_Some:
  forall p p',
    NB.eval_full_path e f p (Some p') <-> eval_full_path e f p = Val p'.
Proof.
  move=> p >; split=> H.
  - inversion H; subst.
    move/NB.eval_full_path_eval_path/eval_path_fixpoint_match_Some: H => /=.
    rewrite H1 /=. by case Nat.eqb_spec.
  - case: p H => [i l] /=. case SZ: (fn_szenv' f)!_ => //=.
    case Nat.eqb_spec => // EQ.
    case H: eval_path_elem_list => [l'||] //= [<-].
    have: eval_path e f (i, l) = Val (i, l') by rewrite /= SZ /= H.
    move/eval_path_fixpoint_match_Some => X; inv X.
    econstructor; eassumption || congruence.
Qed.

Theorem eval_full_path_fixpoint_match_None:
  forall p,
    NB.eval_full_path e f p None <-> eval_full_path e f p = Err.
Proof.
  move=> p >; split=> H.
  - inversion H; subst.
    move/NB.eval_full_path_eval_path/eval_path_fixpoint_match_None: H => /=.
    rewrite H0 /=. by case Nat.eqb_spec.
  - case: p H => [i l] /=. case SZ: (fn_szenv' f)!_ => //=.
    case Nat.eqb_spec => // EQ.
    case H: eval_path_elem_list =>> //= _.
    have: eval_path e f (i, l) = Err by rewrite /= SZ /= H.
    move/eval_path_fixpoint_match_None => X; inv X.
    econstructor; eassumption || congruence.
Qed.

Theorem eval_path_list_fixpoint_match_Some:
  forall le lv,
    NB.eval_path_list e f le (Some lv) <-> eval_path_list e f le = Val lv.
Proof.
  elim/list_ind.
  - move=>>; split=> [T /ltac:(inv T) //|[<-]]; by econstructor.
  - move=>> IHlidx >; split=> [T|].
    + inv T => /=. rewrite (proj1 (eval_path_fixpoint_match_Some _ _) H2).
      rewrite (proj1 (IHlidx _) H3) //.
    + move=> /=. case E: eval_path => //=.
      case El: eval_path_list =>> //= [<-]. econstructor.
      by apply eval_path_fixpoint_match_Some. by apply IHlidx.
Qed.

Theorem eval_path_list_fixpoint_match_None:
  forall le,
    NB.eval_path_list e f le None <-> eval_path_list e f le = Err.
Proof.
  elim/list_ind.
  - split=> [T /ltac:(inv T)|] //.
  - move=>> IH >; split=> [T|].
    + inv T => /=.
      by rewrite (proj1 (eval_path_fixpoint_match_None _) H0).
      by rewrite (proj1 (eval_path_fixpoint_match_Some _ _) H1) (proj1 IH H2).
    + move=> /=. case E: eval_path => //=; [|intros _].
      case El: eval_path_list =>> //= _.
      * eapply NB.eval_path_list_Cons_None. apply eval_path_fixpoint_match_Some, E.
        by apply IH.
      * eapply NB.eval_path_list_Cons_None_e. by apply eval_path_fixpoint_match_None.
Qed.

End EVAL_EXPR_FIXPOINT_MATCH.

Lemma build_index'_eq_acc:
  forall lvsz shape lvidx1 lvidx2 n1 n2 idx,
    natlist_of_Vint64 lvsz = Some shape ->
    valid_index lvidx1 lvsz ->
    valid_index lvidx2 lvsz ->
    build_index' lvidx1 shape n1 = Some idx ->
    build_index' lvidx2 shape n2 = Some idx ->
    n1 = n2.
Proof.
  elim/list_ind => /=.
  - move=> ? [|v1 ?] [|v2 ?] > [<-] /=. congruence. by case v2. all: by case v1.
  - move=> [] // sz > IH ? [//|[] // i1 ?] [//|[] // i2 ?] >.
    case S: natlist_of_Vint64 => //= - [<-].
    case/andP => //= B1 V1. case/andP => //= B2 V2.
    move/IH/[apply]/(_ S V1 V2).
    rewrite 2!(PeanoNat.Nat.mul_comm _ (Z.to_nat _)).
    move/Z.ltb_spec0 in B1. move/Z.ltb_spec0 in B2.
    have R1 := proj1 (Int64.unsigned_range i1).
    have R2 := proj1 (Int64.unsigned_range i2).
    have R  := proj1 (Int64.unsigned_range sz).
    have:= proj1 (Znat.Z2Nat.inj_lt _ _ R2 R) B2.
    have:= proj1 (Znat.Z2Nat.inj_lt _ _ R1 R) B1.
    move/PeanoNat.Nat.div_mod_unique/[apply]/[apply]. easy.
Qed.

Lemma build_index_valid_index_injective:
  forall lvidx1 lvidx2 lvsz shape idx,
    valid_index lvidx1 lvsz ->
    valid_index lvidx2 lvsz ->
    natlist_of_Vint64 lvsz = Some shape ->
    build_index lvidx1 shape = Some idx ->
    build_index lvidx2 shape = Some idx ->
    lvidx1 = lvidx2.
Proof.
  move=> lvidx1 lvidx2. rewrite/build_index.
  move: lvidx1 lvidx2 0%nat. elim/list_ind.
  - move=> [|v ?] ? [//|??] > //=. by case v.
  - move=> [] // i1 lvidx1 IH [|[] // i2 lvidx2] ? [//|[] // sz ?] > //=.
    case S: natlist_of_Vint64 => //= ++ [<-].
    case/andP => B1 V1. case/andP => B2 V2. move=> I1 I2.
    have T: i1 = i2.
    { have:= build_index'_eq_acc _ _ _ _ _ _ _ S V1 V2 I1 I2.
      move /Nat.add_cancel_l.
      have R1 := proj1 (Int64.unsigned_range i1).
      have R2 := proj1 (Int64.unsigned_range i2).
      move/(Znat.Z2Nat.inj _ _ R1 R2). case (Int64.eq_dec i1 i2) => //.
      by move/Int64_neq_unsigned. }
    rewrite T. rewrite T in I1.
    by rewrite (IH _ _ _ _ _ V1 V2 S I1 I2).
Qed.

Section RECEPTIVENESS.

Theorem semantics_receptive:
  forall p,
    Smallstep.receptive (SemanticsNonBlocking.semantics p).
Proof.
  move=> p. econstructor.
  - move=>> /=. inv 1; inv 1; try destruct res2 => //; eauto with semanticsnb.
  - inv 1; eauto.
Qed.

End RECEPTIVENESS.

Section DETERMINISM.

Lemma eval_identlist_deterministic e f lids lv1 lv2:
  NB.eval_identlist e f lids lv1 ->
  NB.eval_identlist e f lids lv2 ->
  lv1 = lv2.
Proof. rewrite !eval_identlist_fixpoint_match; congruence. Qed.

Lemma eval_expr_deterministic e f exp v1 v2:
  NB.eval_expr e f exp v1 ->
  NB.eval_expr e f exp v2 ->
  v1 = v2.
Proof.
  case: v1; case: v2 =>> //;
  rewrite !eval_expr_fixpoint_match_Some ?eval_expr_fixpoint_match_None;
  congruence.
Qed.

Lemma eval_exprlist_deterministic e f le lv1 lv2:
  NB.eval_exprlist e f le lv1 ->
  NB.eval_exprlist e f le lv2 ->
  lv1 = lv2.
Proof.
  case: lv1; case: lv2 =>> //;
  rewrite !eval_exprlist_fixpoint_match_Some ?eval_exprlist_fixpoint_match_None;
  congruence.
Qed.

Lemma eval_path_deterministic e f p p'1 p'2:
  NB.eval_path e f p p'1 ->
  NB.eval_path e f p p'2 ->
  p'1 = p'2.
Proof.
  case: p'1; case: p'2 =>> //;
  rewrite !eval_path_fixpoint_match_Some ?eval_path_fixpoint_match_None;
  congruence.
Qed.

Lemma eval_full_path_deterministic e f p p'1 p'2:
  NB.eval_full_path e f p p'1 ->
  NB.eval_full_path e f p p'2 ->
  p'1 = p'2.
Proof.
  case: p'1; case: p'2 =>> //;
  rewrite !eval_full_path_fixpoint_match_Some ?eval_full_path_fixpoint_match_None;
  congruence.
Qed.

Lemma eval_path_list_deterministic e f lp lp'1 lp'2:
  NB.eval_path_list e f lp lp'1 ->
  NB.eval_path_list e f lp lp'2 ->
  lp'1 = lp'2.
Proof.
  case: lp'1; case: lp'2 =>> //;
  rewrite !eval_path_list_fixpoint_match_Some ?eval_path_list_fixpoint_match_None;
  congruence.
Qed.

Ltac deterministic_simplify :=
  repeat match goal with
  | H1: (?a = ?b), H2: (?a' = ?b') |- _ =>
    unify a a';
    rewrite -> H1 in *; clear H1
  | H: ?a = ?b |- _ =>
    revert H; (intros [= ->] || intros [= <-])
  | H1: NB.eval_full_path ?a1 ?b1 ?c1 ?d1,
    H2: NB.eval_full_path ?a2 ?b2 ?c2 ?d2 |- _ =>
    unify a1 a2; unify b1 b2; unify c1 c2;
    let X := fresh in
    pose proof (eval_full_path_deterministic _ _ _ _ _ H1 H2) as X;
    try (discriminate || revert X; intros [= <-]);
    clear H1 H2
  | H1: NB.eval_path ?a1 ?b1 ?c1 ?d1,
    H2: NB.eval_path ?a2 ?b2 ?c2 ?d2 |- _ =>
    unify a1 a2; unify b1 b2; unify c1 c2;
    let X := fresh in
    pose proof (eval_path_deterministic _ _ _ _ _ H1 H2) as X;
    try (discriminate || revert X; intros [= <-]);
    clear H1 H2
  | H1: NB.eval_path_list ?a1 ?b1 ?c1 ?d1,
    H2: NB.eval_path_list ?a2 ?b2 ?c2 ?d2 |- _ =>
    unify a1 a2; unify b1 b2; unify c1 c2;
    let X := fresh in
    pose proof (eval_path_list_deterministic _ _ _ _ _ H1 H2) as X;
    try (discriminate || revert X; intros [= <-]);
    clear H1 H2
  | H1: NB.eval_expr ?a1 ?b1 ?c1 ?d1,
    H2: NB.eval_expr ?a2 ?b2 ?c2 ?d2 |- _ =>
    unify a1 a2; unify b1 b2; unify c1 c2;
    let X := fresh in
    pose proof (eval_expr_deterministic _ _ _ _ _ H1 H2) as X;
    try (discriminate || revert X; intros [= <-]);
    clear H1 H2
  | H1: NB.eval_exprlist ?a1 ?b1 ?c1 ?d1,
    H2: NB.eval_exprlist ?a2 ?b2 ?c2 ?d2 |- _ =>
    unify a1 a2; unify b1 b2; unify c1 c2;
    let X := fresh in
    pose proof (eval_exprlist_deterministic _ _ _ _ _ H1 H2) as X;
    try (discriminate || revert X; intros [= <-]);
    clear H1 H2
  end.

Theorem step_derministic ge:
  ∀ s t t',
    NB.step_stmt ge s t ->
    NB.step_stmt ge s t' ->
    t = t'.
Proof. move=>> H1 H2; inv H1; inv H2 => //; by deterministic_simplify. Qed.

End DETERMINISM.

Section SEM_PRIMITIVE_VALUE.

Lemma sem_cast_primitive_value:
  forall v t1 t2 v',
    sem_cast v t1 t2 = Some v' ->
    primitive_value v = primitive_value v'.
Proof.
  move=> v t1 t2; case: v =>> //=.
  all: case: t1 => [| |_ []|[]|[]|] //=.
  all: case: t2 => [| |? []|[]|[]|] //=.
  all: destruct_match_goal.
  all: try intros [= <-] => //.
  move=>>; case typ_beq => // - [<-] //.
Qed.

Lemma sem_unop_primitive_value:
  forall op k v v',
    sem_unop op k v = Some v' ->
    primitive_value v.
Proof. case=> - [] // [] // ? [] //. Qed.

Lemma sem_binop_arith_primitive_value:
  forall op k v1 v2 v',
    sem_binarith_operation op k v1 v2 = Some v' ->
    primitive_value v1 /\ primitive_value v2.
Proof. case=> - [] // [] // ? [] //. Qed.

Lemma sem_binop_cmp_primitive_value:
  forall op k v1 v2 v',
    sem_cmp_operation op k v1 v2 = Some v' ->
    primitive_value v1 /\ primitive_value v2.
Proof. case=> - [] // [] // ? [] //. Qed.

Lemma sem_binop_cmpu_primitive_value:
  forall op k v1 v2 v',
    sem_cmpu_operation op k v1 v2 = Some v' ->
    primitive_value v1 /\ primitive_value v2.
Proof. case=> - [] // [] // ? [] //. Qed.

End SEM_PRIMITIVE_VALUE.

Section EVAL_EXPR_LIST_TO_INT64_EVAL_EXPR.

Local Open Scope outcome_scope.

Lemma eval_expr_list_to_int64_eval_exprlist e f:
  forall le li,
    eval_expr_list_to_int64 e f le = Val li ->
    NB.eval_exprlist e f le (Some (map Vint64 li)).
Proof.
  elim/list_ind => /=.
  - move=>> [<-]; econstructor.
  - move=>> IH >. case E: eval_expr => [[]| |] //=.
    all: case El: eval_expr_list_to_int64 =>> //= [<-].
    econstructor. by apply eval_expr_fixpoint_match_Some. by apply: IH.
Qed.

Lemma eval_expr_list_to_int64_Err e f:
  forall le,
    eval_expr_list_to_int64 e f le = Err ->
    exists le1 x le2 li1,
      le = le1 ++ x :: le2 /\
      eval_expr_list_to_int64 e f le1 = Val li1 /\
      eval_expr e f x = Err.
Proof.
  elim/list_ind => //= z l. case H: eval_expr => [[| | |i| | |]| |] //=.
  - case eval_expr_list_to_int64 =>> //= /[apply].
    case=> le1 [x] [le2] [li1] [->] [E1 Ex].
    rewrite app_comm_cons. exists (z :: le1), x, le2, (i :: li1).
    repeat split => //=. by rewrite H E1.
  - move=> _ _. by exists [], z, l, [].
Qed.

End EVAL_EXPR_LIST_TO_INT64_EVAL_EXPR.