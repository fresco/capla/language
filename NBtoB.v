Require Import Coq.Lists.List.
Require Import BinNums BinInt.
Require Import Coq.Bool.Bool.
Require Import String.
Require Import Lia.
Require Import POrderedType MSetAVL.

Module PSet := Make (Positive_as_OT).

Import ListNotations.

Require Import ssreflect.


Require Import BValues Types Syntax Typing Ops.
Require Import BUtils PTreeaux Tactics ListUtils.
Require Import BEnv Validity SemanticsCommon.

Require Import Integers Floats Maps Errors Smallstep.
Require Import Globalenvs.
Require Import IEEE754_extra.

Require SemanticsNonBlocking.
Module NB := SemanticsNonBlocking.
Require Import SemanticsBlocking.
Require Import FloatCastTests.
Require Import SemPath.

Local Open Scope option_error_monad_scope.
Local Open Scope error_monad_scope.

Create HintDb nbtob.

Section TestGeneration.

Fixpoint bound_constraints (lidx: list expr) (lsz: list ident) :=
  match lidx, lsz with
  | [], [] => OK []
  | idx :: lidx, sz :: lsz =>
    do r    <- bound_constraints lidx lsz;
    OK ((Ebinop_cmpu Oltu OInt64 idx (Evar sz)) :: r)
  | _, _ => Error [MSG "NB->B: Wrong number of indices."]
  end.

Fixpoint expr_constraints (f: function) (e: expr) : res (list expr) :=
  match e with
  | Econst _ => OK []
  | Ecast e t1 t2 =>
      do c <- expr_constraints f e;
      match t1 with
      | Tfloat F32 =>
        match t2 with
        | Tint _ Signed =>
          OK (c ++
              [Ebinop_cmp Ole OFloat32
                 (Econst (Cfloat32 (BofZ 24 128 eq_refl eq_refl (- 2 ^ 31)))) e;
               Ebinop_cmp Olt OFloat32
                 e (Econst (Cfloat32 (BofZ 24 128 eq_refl eq_refl (2 ^ 31))))])
        | Tint _ Unsigned =>
          OK (c ++
              [Ebinop_cmp Olt OFloat32
                 (Econst (Cfloat32 (BofZ 24 128 eq_refl eq_refl (- 1)))) e;
               Ebinop_cmp Olt OFloat32
                 e (Econst (Cfloat32 (BofZ 24 128 eq_refl eq_refl (2 ^ 32))))])
        | Tint64 Signed =>
          OK (c ++
              [Ebinop_cmp Ole OFloat32
                 (Econst (Cfloat32 (BofZ 24 128 eq_refl eq_refl (- 2 ^ 63)))) e;
               Ebinop_cmp Olt OFloat32
                 e (Econst (Cfloat32 (BofZ 24 128 eq_refl eq_refl (2 ^ 63))))])
        | Tint64 Unsigned =>
          OK (c ++
              [Ebinop_cmp Olt OFloat32
                 (Econst (Cfloat32 (BofZ 24 128 eq_refl eq_refl (- 1)))) e;
              Ebinop_cmp Olt OFloat32
                 e (Econst (Cfloat32 (BofZ 24 128 eq_refl eq_refl (2 ^ 64))))])
        | _ => OK c
        end
      | Tfloat F64 =>
        match t2 with
        | Tint _ Signed =>
          OK (c ++
              [Ebinop_cmp Olt OFloat64
                 (Econst (Cfloat64 (BofZ 53 1024 eq_refl eq_refl (- 2 ^ 31 - 1)))) e;
               Ebinop_cmp Olt OFloat64
                 e (Econst (Cfloat64 (BofZ 53 1024 eq_refl eq_refl (2 ^ 31))))])
        | Tint _ Unsigned =>
          OK (c ++
              [Ebinop_cmp Olt OFloat64
                 (Econst (Cfloat64 (BofZ 53 1024 eq_refl eq_refl (- 1)))) e;
               Ebinop_cmp Olt OFloat64
                 e (Econst (Cfloat64 (BofZ 53 1024 eq_refl eq_refl (2 ^ 32))))])
        | Tint64 Signed =>
          OK (c ++
              [Ebinop_cmp Ole OFloat64
                 (Econst (Cfloat64 (BofZ 53 1024 eq_refl eq_refl (- 2 ^ 63)))) e;
               Ebinop_cmp Olt OFloat64
                 e (Econst (Cfloat64 (BofZ 53 1024 eq_refl eq_refl (2 ^ 63))))])
        | Tint64 Unsigned =>
          OK (c ++
              [Ebinop_cmp Olt OFloat64
                 (Econst (Cfloat64 (BofZ 53 1024 eq_refl eq_refl (- 1)))) e;
               Ebinop_cmp Olt OFloat64
                 e (Econst (Cfloat64 (BofZ 53 1024 eq_refl eq_refl (2 ^ 64))))])
        | _ => OK c
        end
      | _ => OK c
      end
  | Eunop _ _ e => expr_constraints f e
  | Ebinop_arith op k e1 e2 =>
      do c1 <- expr_constraints f e1;
      do c2 <- expr_constraints f e2;
      match k, op with
      | OInt, (Odivu | Omodu) =>
        OK (c1 ++ c2 ++
            [Ebinop_cmp One OInt e2
               (Econst (Cint I32 Unsigned Int.zero))])
      | OInt, (Odivs | Omods) =>
        OK (c1 ++ c2 ++
            [Ebinop_cmp One OInt e2
               (Econst (Cint I32 Unsigned Int.zero));
             Ebinop_arith Oor OInt
               (Ebinop_cmp One OInt e1
                 (Econst (Cint I32 Signed (Int.repr Int.min_signed))))
               (Ebinop_cmp One OInt e2
                 (Econst (Cint I32 Signed Int.mone)))])
      | OInt64, (Odivu | Omodu) =>
        OK (c1 ++ c2 ++
            [Ebinop_cmp One OInt64 e2
               (Econst (Cint64 Unsigned Int64.zero))])
      | OInt64, (Odivs | Omods) =>
        OK (c1 ++ c2 ++
            [Ebinop_cmp One OInt64 e2
               (Econst (Cint64 Unsigned Int64.zero));
             Ebinop_arith Oor OInt
               (Ebinop_cmp One OInt64 e1
                 (Econst (Cint64 Signed (Int64.repr Int64.min_signed))))
               (Ebinop_cmp One OInt64 e2 (Econst (Cint64 Signed Int64.mone)))])
      | _, _ => OK (c1 ++ c2)
      end
  | Ebinop_cmp _ _ e1 e2 | Ebinop_cmpu _ _ e1 e2 =>
      do c1 <- expr_constraints f e1;
      do c2 <- expr_constraints f e2;
      OK (c1 ++ c2)
  | Eacc (id, sp) =>
      do** llsz <- ["NB->B: " ++ (BinaryString.of_pos id) ++ " is not an array."]
                   f.(fn_szenv')!id;
      let syn_path_elem_list_constraints := fix splc llsz sp {struct sp} :=
        match sp, llsz with
        | [], _ => OK []
        | s :: sp, lsz :: llsz =>
            do telt <- syn_path_elem_constraints f lsz s;
            do r    <- splc llsz sp;
            OK (telt ++ r)
        | _, _ => Error [MSG "NB->B: Invalid path."]
        end in
      syn_path_elem_list_constraints llsz sp
  end
with syn_path_elem_constraints f lsz s : res (list expr) :=
  match s with
  | Scell lidx =>
      let lidx_constraints :=
        fix lidx_constraints lidx :=
          match lidx with
          | [] => OK []
          | idx :: lidx =>
            do r    <- lidx_constraints lidx;
            do cidx <- expr_constraints f idx;
            OK (cidx ++ r)
          end in
      do c <- lidx_constraints lidx;
      do b <- bound_constraints lidx lsz;
      OK (c ++ b)
  end.

Definition exprlist_constraints f := fix elc l :=
  match l with
  | [] => OK []
  | e :: l =>
      do lconstrs <- elc l;
      do constrs <- expr_constraints f e;
      OK (constrs ++ lconstrs)
  end.

Definition syn_path_elem_list_constraints f :=
  fix splc llsz sp {struct sp} :=
    match sp, llsz with
    | [], _ => OK []
    | s :: sp, lsz :: llsz =>
        do telt <- syn_path_elem_constraints f lsz s;
        do r    <- splc llsz sp;
        OK (telt ++ r)
    | _, _ => Error [MSG "NB->B: Invalid path."]
    end.

Lemma Scell_constraints:
  forall f lsz lidx,
    syn_path_elem_constraints f lsz (Scell lidx)
    = do c <- exprlist_constraints f lidx;
      do b <- bound_constraints lidx lsz;
      OK (c ++ b).
Proof. reflexivity. Qed.

Lemma Eacc_constraints:
  forall f id sp,
    expr_constraints f (Eacc (id, sp))
    = do** llsz <- ["NB->B: " ++ (BinaryString.of_pos id) ++ " is not an array."]
                   f.(fn_szenv')!id;
      syn_path_elem_list_constraints f llsz sp.
Proof. reflexivity. Qed.

Fixpoint guard_stmt_with_constraints_list (constrs: list expr) (s: stmt)
  : stmt :=
  match constrs with
  | [] => s
  | c :: constrs =>
      Sseq (Sassert c) (guard_stmt_with_constraints_list constrs s)
  end.

Definition guard_stmt_with_expr f e s :=
  do c <- expr_constraints f e;
  OK (guard_stmt_with_constraints_list (nodup_keep_first expr_beq c) s).

Definition call_constraints (caller: function) (callee: fundef) (args: list syn_path) :=
  let aux := fun '(e, i) => do r <- expr_constraints caller e;
                            OK (r ++ [Ebinop_cmp Oeq OInt64 e (Eacc (i, []))]) in
  do tests <- valid_call_tests caller callee args;
  fold_right_res (fun tests x =>
      do r <- aux x;
      OK (r ++ tests)) tests [].

Close Scope option_bool_monad_scope.

End TestGeneration.


Section NonBlockingSemanticsToBlockingSemantics.

Fixpoint transl_stmt (ge: genv) (f: function) (s: stmt) : res stmt :=
  match s with
  | Sskip => OK s
  | Sassign p e =>
      do c  <- expr_constraints f (Eacc p);
      do ce <- expr_constraints f e;
      OK (guard_stmt_with_constraints_list
            (nodup_keep_first expr_beq (ce ++ c))
            (Sassign p e))
  | Salloc i =>
      do* llsz <- (fn_szenv f)!i;
      match llsz with
      | [sz] :: [] =>
        guard_stmt_with_expr f sz (Salloc i)
      | _ => Error [MSG "NB->B: Only single dimensional arrays can be allocated."]
      end
  | Sfree i => OK (Sfree i)
  | Scall _ idf args =>
      match ge!idf with
      | Some f' =>
          do cargs <- exprlist_constraints f (map Eacc args);
          do ccall <- call_constraints f f' args;
          OK (guard_stmt_with_constraints_list (nodup_keep_first expr_beq (cargs ++ ccall)) s)
      | None => Error [MSG "NB->B: Function ";
                       MSG (BinaryString.of_pos idf);
                       MSG " doesn't exists."]
      end
  | Sreturn None => OK s
  | Sreturn (Some e) =>
      do g <- guard_stmt_with_expr f e s; OK g
  | Sseq s1 s2 =>
      do s1 <- transl_stmt ge f s1;
      do s2 <- transl_stmt ge f s2;
      OK (Sseq s1 s2)
  | Sassert cond =>
      guard_stmt_with_expr f cond (Sassert cond)
  | Sifthenelse cond strue sfalse =>
      do strue <- transl_stmt ge f strue;
      do sfalse <- transl_stmt ge f sfalse;
      guard_stmt_with_expr f cond (Sifthenelse cond strue sfalse)
  | Sloop s =>
      do s <- transl_stmt ge f s;
      OK (Sloop s)
  | Sblock s =>
      do s <- transl_stmt ge f s;
      OK (Sblock s)
  | Sexit _ | Serror => OK s
  end.

Definition transl_function (ge: genv) (f: function) : res function :=
  do tfn_body <- transl_stmt ge f f.(fn_body);
  OK (mk_function f.(fn_sig)
                  f.(fn_params)
                  f.(fn_vars)
                  tfn_body
                  f.(fn_tenv)
                  f.(fn_szenv)
                  f.(fn_szenv')
                  f.(fn_penv)
                  f.(fn_nodup_params)
                  f.(fn_disjoint)
                  f.(fn_res_primitive)
                  f.(fn_tenv_sig)
                  f.(fn_tenv_sztype)
                  f.(fn_tenv_vars)
                  f.(fn_szenv_params)
                  f.(fn_szenv'_params)
                  f.(fn_szenv_wf)
                  f.(fn_szenv_tenv)
                  f.(fn_szenv_tenv_str)
                  f.(fn_szenv_szenv')
                  f.(fn_szenv_szenv'_str)
                  f.(fn_szenv_szenv'_inj)
                  f.(fn_szenv_szexprs)
                  f.(fn_szenv'_szvars)
                  f.(fn_szenv'_owned)
                  f.(fn_penv_tenv)
                  f.(fn_penv_szenv')).

Definition transl_fundef (ge: genv) (fd: fundef) : res fundef :=
  match fd with
  | External ef => OK (External ef)
  | Internal f =>
      do tf <- transl_function ge f;
      OK (Internal tf)
  end.

Definition transl_genv (ge: genv) : res genv :=
  map_error (fun _ fd => transl_fundef ge fd) ge.

Definition transl_fundefs (ge: genv) (defs: list (ident * fundef))
    : res (list (ident * fundef)) :=
  list_map_error (fun x => do tfd <- transl_fundef ge (snd x); OK (fst x, tfd)) defs.

Lemma transl_fundefs_eq_map_fst:
  forall ge defs tdefs,
    transl_fundefs ge defs = OK tdefs ->
    map fst defs = map fst tdefs.
Proof.
  induction defs; simpl; intros.
  + revert H; intros [= <-]. reflexivity.
  + destruct a as [id fd].
    cbn in H.
    destruct (transl_fundef ge fd), list_map_error eqn:H0; try discriminate.
    revert H; intros [= <-]. rewrite -> IHdefs with (1 := H0). reflexivity.
Qed.

Lemma transl_fundefs_list_assoc_nodup:
     forall ge defs,
       Coqlib.list_norepet (map fst defs) ->
       match transl_fundefs ge defs with
       | OK tdefs => Coqlib.list_norepet (map fst tdefs)
       | _ => True
       end.
Proof.
  intros.
  destruct (transl_fundefs ge defs) eqn:H0.
  erewrite <- transl_fundefs_eq_map_fst; eassumption.
  easy.
Qed.

Lemma transl_function_same_penv ge f tf:
  transl_function ge f = OK tf ->
  fn_penv f = fn_penv tf.
Proof. intro H. monadInv H. reflexivity. Qed.

Definition transl_program (p: program) : res program.
Proof.
  assert (Hnodup := transl_fundefs_list_assoc_nodup (genv_of_program p) p.(prog_defs) p.(prog_nodup)).
  assert (Hmapfst := transl_fundefs_eq_map_fst (genv_of_program p) (prog_defs p)).
  destruct transl_fundefs.
  apply OK, (mk_program l p.(prog_main)).
  exact Hnodup.
  exact (Error e).
Defined.

End NonBlockingSemanticsToBlockingSemantics.
