Require Import ssreflect ssrbool ssrfun.
Require Import String Coq.Lists.List.
Require Import BinNums BinInt.
Require Import Coq.Bool.Bool.
Require Import Lia.
Require Import POrderedType MSetAVL.

Import ListNotations.

Require Import BValues Types Syntax Typing Ops Tactics.
Require Import BUtils PTreeaux ListUtils.
Require Import BEnv Validity Alias SemanticsCommon.
Require NBfacts ExprSem.
Module ES := ExprSem.

Require Import Integers Floats Maps Errors Smallstep.
Require Import Globalenvs.
Require Import IEEE754_extra.

Require SemanticsNonBlocking.
Module NB := SemanticsNonBlocking.
Require Import SemanticsBlocking.
Require Import FloatCastTests.

Require Import NBtoB.

Local Open Scope error_monad_scope.

Fixpoint transl_cont (ge: genv) (f: option function) (k: NB.cont)
    : res cont :=
  match k with
  | NB.Kstop => OK Kstop
  | NB.Kblock k =>
      do k' <- transl_cont ge f k;
      OK (Kblock k')
  | NB.Kseq s k =>
      match f with
      | Some f' =>
          do  s' <- transl_stmt ge f' s;
          do  k' <- transl_cont ge f k;
          OK (Kseq s' k')
      | None => Error [ MSG "" ]
      end
  | NB.Kreturnto id e' f' marrs k =>
      do k' <- transl_cont ge (Some f') k;
      do tf' <- transl_function ge f';
      OK (Kreturnto id e' tf' marrs k')
  end.

Lemma transl_cont_None_Some ge:
  forall k f tk,
    transl_cont ge None k = OK tk ->
    transl_cont ge (Some f) k = OK tk.
Proof. elim=>> //= IH >; case T: transl_cont =>> //= [<-]. by rewrite (IH _ _ T). Qed.

#[local]
Hint Resolve transl_cont_None_Some : nbtob.

Lemma transl_function_same_sig ge f tf:
  transl_function ge f = OK tf ->
  fn_sig f = fn_sig tf.
Proof. intro H. monadInv H. reflexivity. Qed.

Lemma transl_function_same_tenv ge f tf:
  transl_function ge f = OK tf ->
  fn_tenv f = fn_tenv tf.
Proof. intro H. monadInv H. reflexivity. Qed.

Lemma transl_function_same_szenv ge f tf:
  transl_function ge f = OK tf ->
  fn_szenv f = fn_szenv tf.
Proof. intro H. monadInv H. reflexivity. Qed.

Lemma transl_function_same_szenv' ge f tf:
  transl_function ge f = OK tf ->
  fn_szenv' f = fn_szenv' tf.
Proof. intro H. monadInv H. reflexivity. Qed.

Lemma transl_function_same_params ge f tf:
  transl_function ge f = OK tf ->
  fn_params f = fn_params tf.
Proof. intro H. monadInv H. reflexivity. Qed.

Lemma transl_fundef_same_szenv ge f tf:
  transl_fundef ge f = OK tf ->
  fd_szenv f = fd_szenv tf.
Proof. case: f tf =>> /= H; monadInv H => //. by monadInv EQ. Qed.

Lemma transl_fundef_same_params ge f tf:
  transl_fundef ge f = OK tf ->
  fd_params f = fd_params tf.
Proof. case: f tf =>> /= H; monadInv H => //. by monadInv EQ. Qed.

#[local]
Hint Extern 1 => match goal with
                 |- context[fn_sig _] => (erewrite <- transl_function_same_sig);
                                         [|eassumption]
                 end : nbtob.

#[local]
Hint Extern 1 => match goal with
                 |- context[fn_tenv _] => (erewrite <- transl_function_same_tenv);
                                          [|eassumption]
                 end : nbtob.

#[local]
Hint Extern 1 => match goal with
                 |- context[fn_szenv _] => (erewrite <- transl_function_same_szenv);
                                           [|eassumption]
                 end : nbtob.

#[local]
Hint Extern 1 => match goal with
                |- context[fn_szenv' _] => (erewrite <- transl_function_same_szenv');
                                           [|eassumption]
                end : nbtob.

#[local]
Hint Extern 1 => match goal with
                 |- context[fn_penv _] => (erewrite <- transl_function_same_penv);
                                          [|eassumption]
                 end : nbtob.

#[local]
Hint Extern 1 => match goal with
                 |- context[fn_params _] => (erewrite <- transl_function_same_params);
                                            [|eassumption]
                 end : nbtob.

Theorem identlist_sem_preservation:
  forall ge e f tf lids vlids,
    transl_function ge f = OK tf ->
    NB.eval_identlist e f lids vlids ->
    eval_identlist e tf lids vlids.
Proof. induction lids; intros; inversion H0; eauto with semantics nbtob. Qed.

#[local]
Hint Resolve identlist_sem_preservation : nbtob.

Global Hint Resolve valid_index_product : nbtob.

Theorem transl_expr_sem_preservation ge e f tf exp v
    (Htf:   transl_function ge f = OK tf)
    (Heval: NB.eval_expr e f exp (Some v)):
    eval_expr e tf exp v
with transl_exprlist_sem_preservation ge e f tf le lv
    (Htf:   transl_function ge f = OK tf)
    (Heval: NB.eval_exprlist e f le (Some lv)):
    eval_exprlist e tf le lv
with eval_path_sem_preservation ge e f tf p p'
    (Htf:   transl_function ge f = OK tf)
    (Heval: NB.eval_path e f p (Some p')):
    eval_path e tf p p'
with eval_full_path_sem_preservation ge e f tf p p'
    (Htf:   transl_function ge f = OK tf)
    (Heval: NB.eval_full_path e f p (Some p')):
    eval_full_path e tf p p'
with eval_path_elem_list_sem_preservation ge e f tf synpl llsz sempl
    (Htf:   transl_function ge f = OK tf)
    (Heval: NB.eval_path_elem_list e f synpl llsz (Some sempl)):
    eval_path_elem_list e tf synpl llsz sempl.
Proof.
  - inversion Heval; subst; econstructor; eauto with semantics nbtob semantics.
  - inversion Heval; econstructor; eauto with nbtob semantics.
  - inv Heval; econstructor; eauto with nbtob.
  - inv Heval; econstructor; eauto with nbtob.
  - inv Heval; econstructor; eauto with nbtob.
Qed.

#[local]
Hint Resolve transl_expr_sem_preservation : nbtob.
#[local]
Hint Resolve transl_exprlist_sem_preservation : nbtob.
#[local]
Hint Resolve eval_path_sem_preservation : nbtob.
#[local]
Hint Resolve eval_full_path_sem_preservation : nbtob.
#[local]
Hint Resolve eval_path_elem_list_sem_preservation : nbtob.

Lemma eval_path_list_sem_preservation ge e f tf:
  forall lp lp',
    transl_function ge f = OK tf ->
    NB.eval_path_list e f lp (Some lp') ->
    eval_path_list e tf lp lp'.
Proof.
  move=> ++ TF. elim/list_ind.
  - move=>> T. inv T. econstructor.
  - move=> p lp IH > T. inv T. econstructor; eauto with nbtob.
Qed.

#[local]
Hint Resolve eval_path_list_sem_preservation : nbtob.

Lemma eval_ident_get_env_path:
  forall e i v,
    e!i = Some v ->
    get_env_path e (i, []) = Some v.
Proof. simpl; now intros * ->. Qed.

#[local]
Hint Resolve eval_ident_get_env_path : nbtob.

Lemma eval_identlist_same_value:
  forall e f lids lv1 lv2,
    eval_identlist e f lids lv1 ->
    eval_identlist e f lids lv2 ->
    lv1 = lv2.
Proof.
  induction lids; intros * H1 H2; inversion H1; inversion H2. reflexivity.
  specialize (IHlids _ _ H6 H12). congruence.
Qed.

Lemma eval_expr_same_value:
  forall e f exp v1 v2
    (EV1: eval_expr e f exp v1)
    (EV2: eval_expr e f exp v2),
    v1 = v2.
Proof.
  move=> ??. fix IH 1. case. shelve. all: move=>> EV1 EV2; inv EV1; inv EV2 => //.
  1,2: move: H4 H6; rewrite (IH _ _ _ H3 H5); congruence.
  1-3: move: H6 H9.
  1-3: rewrite (IH _ _ _ H4 H7).
  1-3: rewrite (IH _ _ _ H5 H8); congruence.
  Unshelve.
  move=> [i synpl] > H1 H2; inv H1; inv H2.
  inv H0; inv H1. move: H7 H2 => ->. intros [= <-].
  have: sempl = sempl0; [|congruence].
  { move: synpl llsz sempl sempl0 {H5 H3} H9 H10. elim/list_ind.
    - move=>> H1 H2. inv H1. inv H2. easy.
    - case. move=> lidx synpl IHsynpl > T1 T2. inv T1. inv T2.
      rewrite -(IHsynpl _ _ _ H10 H17).
      move: H2 H12 H5 H14.
      rewrite -(eval_identlist_same_value _ _ _ _ _ H1 H11) => -> [<-].
      have ->: lvidx = lvidx0; [|congruence].
      { move: lidx lvidx lvidx0 H3 H13. elim/list_ind.
        - move=>> T1 T2. inv T1. inv T2. easy.
        - move=>> IHlidx > T1 T2. inv T1. inv T2.
          by rewrite (IH _ _ _ H2 H3) (IHlidx _ _ H5 H9). } }
Qed.

Lemma bound_constraints_verified:
  forall e f lidx lsz lvsz shape lvidx constrs,
    NB.eval_identlist e f lsz lvsz ->
    natlist_of_Vint64 lvsz = Some shape ->
    NB.eval_exprlist e f lidx (Some lvidx) ->
    ExprSem.valid_index lvidx lvsz ->
    bound_constraints lidx lsz = OK constrs ->
    NB.eval_exprlist e f constrs (Some (map (fun _ => Vtrue) constrs)).
Proof.
  induction lidx; simpl; intros *.
  - destruct_match_goal_ni. intros _ _ _ _ _ [= <-]. apply NB.eval_Enil.
  - unfold bind. do 2 destruct_match_goal_ni.
    intros Cbounds -> H; inversion_clear H.
    simpl; unfold option_bind; do 3 destruct_match_goal_ni.
    intros Hlv -> [= <-] H; inversion_clear H.
    simpl. destruct v; try discriminate.
    case/andP => /Z.ltb_spec0 Hbound  Hvalid [<-].
    rewrite map_cons; eauto 10 with semanticsnb nbtob.
Qed.

#[local]
Hint Extern 1 => repeat match goal with
                   | H: _ = _ |- _ => rewrite H
                   | H: is_true _ |- _ => rewrite H
                   end : nbtob.

Lemma eval_expr_Some_constraints_verified:
  forall e f exp v constrs,
    NB.eval_expr e f exp (Some v) ->
    expr_constraints f exp = OK constrs ->
    NB.eval_exprlist e f constrs (Some (map (fun _ => Vtrue) constrs)).
Proof.
  move=> e f. fix IH 1. case. shelve.
  { move=>> _ [<-]. econstructor. }
  { move=> ? t1 t2 > H. inv H. case: t1 H5 => /=.
    1-4,6: elim H: expr_constraints => //=. 3: move=> ??. 4,5: move=> ?.
    1-5: move=> _ [<-]; by apply: (IH _ _ _ H2).
    case; case: t2 => [| |?| |?|?]; elim H: expr_constraints => //=.
    all: try (move=> _; case=> <-; by apply: (IH _ _ _ H2)).
    all: case=> - C [<-]; rewrite map_app; apply: NB.eval_exprlist_app; eauto.
    all: apply: NB.eval_Econs_Some_e; [|apply: NB.eval_Econs_Some_e];
        eauto with semanticsnb.
    all: eapply NB.eval_Binop_cmp; eauto with semanticsnb.
    all: unfold sem_cast in C; destruct_match C; destruct_match_clear C.
    all: unfold sem_cmp_operation, sem_cmp; do 2 apply f_equal.
    all: pattern i0 in C || pattern i in C; apply ex_intro in C.
    apply (proj2 (float32_to_int_test f0)) in C. exact (proj1 C).
    apply (proj2 (float32_to_int_test f0)) in C. exact (proj2 C).
    apply (proj2 (float32_to_uint_test f0)) in C. exact (proj1 C).
    apply (proj2 (float32_to_uint_test f0)) in C. exact (proj2 C).
    apply (proj2 (float32_to_int64_test f0)) in C. exact (proj1 C).
    apply (proj2 (float32_to_int64_test f0)) in C. exact (proj2 C).
    apply (proj2 (float32_to_uint64_test f0)) in C. exact (proj1 C).
    apply (proj2 (float32_to_uint64_test f0)) in C. exact (proj2 C).
    apply (proj2 (float64_to_int_test f0)) in C. exact (proj1 C).
    apply (proj2 (float64_to_int_test f0)) in C. exact (proj2 C).
    apply (proj2 (float64_to_uint_test f0)) in C. exact (proj1 C).
    apply (proj2 (float64_to_uint_test f0)) in C. exact (proj2 C).
    apply (proj2 (float64_to_int64_test f0)) in C. exact (proj1 C).
    apply (proj2 (float64_to_int64_test f0)) in C. exact (proj2 C).
    apply (proj2 (float64_to_uint64_test f0)) in C. exact (proj1 C).
    apply (proj2 (float64_to_uint64_test f0)) in C. exact (proj2 C). }
  { move=>> H. inv H. eauto. }
  { move=> b o > H. inv H => /=.
    elim Hc1: expr_constraints => //=. elim Hc2: expr_constraints => //=.
    case: b H7; case: o => H [<-]; rewrite map_app; eauto 4 with semanticsnb.
    all: rewrite map_app; do 2 (apply: NB.eval_exprlist_app; eauto).
    all: case: v1 H3 H6 H => //; case: v2 => //=.
    all: rewrite/divs/divu/mods/modu/divs64/divu64/mods64/modu64.
    all: move=>>; destruct_match_goal_ni => +++ _.
    all: move=> T H1 H2; apply: NB.eval_Econs_Some_e;
        [|try apply: NB.eval_Econs_Some_e]; eauto with semanticsnb.
    all: move: T; try case/norP => ?; try case/nandP' => [?|[??]].
    all: try (move=> *; eapply NB.eval_Binop_cmp).
    all: try (eapply NB.eval_Binop_arith; eauto with semanticsnb).
    all: repeat (simpl; eauto with semanticsnb nbtob). }
  { move=>> H. inv H => /=.
    elim Hc1: expr_constraints => //=. elim Hc2: expr_constraints => //=.
    move=> [<-]. rewrite map_app. eauto with semanticsnb. }
  { move=>> H. inv H => /=.
    elim Hc1: expr_constraints => //=. elim Hc2: expr_constraints => //=.
    move=> [<-]. rewrite map_app. eauto with semanticsnb. }
  Unshelve.
  move=> [i synpl] ? constrs H. inv H => /= {H3 H4}. inv H2. rewrite H3 {H3}.
  move: synpl llsz sempl constrs H4. elim/list_ind => /=.
  - move=>> _ [<-]. econstructor.
  - move=> [lidx] l IHsynpl llsz sempl constrs H; inv H => /= {shape idx H4 H6}.
    { move: lidx lvidx lsz lvsz constrs H3 H5 H8. elim/list_ind => /=.
      - move=> ? lsz ?? /[swap] H. inv H => /=. case: lsz => // S.
        inv S => /= _. have:= IHsynpl _ _ _ H9.
        case: ((fix splc _ (sp: list _) := _) _ _) => //= >.
        by move/[apply].
      - set lidx_constraints := (fix lidx_constraints (lidx: list expr) := _).
        move=> idx lidx IHlidx ? lsz ? constrs /[swap] H. inv H => /=.
        case: lsz => [|sz lsz S] /=; [|inv S].
        { case: (lidx_constraints lidx) => //=. by case: expr_constraints. }
        case: v H3 => // nidx Hidx; case: v0 H1 => // nsz Hsz.
        case/andP => /Z.ltb_spec0 B V. have:= IHlidx _ _ _ _ H6 H4 V.
        case: (lidx_constraints lidx) => //= Clidx.
        elim HCe: expr_constraints => [Ce|] //=.
        case: (bound_constraints lidx lsz) => //= Blidx.
        case: ((fix splc _ (sp: list _) := _) _ _) => //= > /(_ _ eq_refl).
        rewrite map_app. move/NB.eval_exprlist_app_inv.
        rewrite map_length. case/(_ eq_refl).
        rewrite map_app. move/NB.eval_exprlist_app_inv.
        rewrite map_length. case/(_ eq_refl).
        move=> EClidx EBlidx ER [<-]. rewrite !map_app /=.
        eapply NB.eval_exprlist_app => //.
        eapply NB.eval_exprlist_app. eauto with semanticsnb.
        econstructor ; eauto 6 with semanticsnb nbtob. }
Qed.

#[local]
Hint Resolve eval_expr_Some_constraints_verified : nbtob.

Lemma eval_path_Some_constraints_verified:
  forall e f p p' constrs,
    NB.eval_path e f p (Some p') ->
    expr_constraints f (Eacc p) = OK constrs ->
    NB.eval_exprlist e f constrs (Some (map (fun _ => Vtrue) constrs)).
Proof.
  move=> e f [i synpl] p' constrs T. inv T => /=. rewrite H2 {H2} /=.
  move: synpl llsz sempl constrs H3. elim/list_ind => /=.
  - move=>> _ [<-]. econstructor.
  - move=> [lidx] l IHsynpl llsz sempl constrs H; inv H => /= {shape idx H4 H6}.
    { move: lidx lvidx lsz lvsz constrs H3 H5 H8. elim/list_ind => /=.
      - move=> ? lsz ?? /[swap] H. inv H => /=. case: lsz => // S.
        inv S => /= _. have:= IHsynpl _ _ _ H9.
        case: ((fix splc _ (sp: list _) := _) _ _) => //= >.
        by move/[apply].
      - set lidx_constraints := (fix lidx_constraints (lidx: list expr) := _).
        move=> idx lidx IHlidx ? lsz ? constrs /[swap] H. inv H => /=.
        case: lsz => [|sz lsz S] /=; [|inv S].
        { case: (lidx_constraints lidx) => //=. by case: expr_constraints. }
        case: v H3 => // nidx Hidx; case: v0 H1 => // nsz Hsz.
        case/andP => /Z.ltb_spec0 B V. have:= IHlidx _ _ _ _ H6 H4 V.
        case: (lidx_constraints lidx) => //= Clidx.
        elim HCe: expr_constraints => [Ce|] //=.
        case: (bound_constraints lidx lsz) => //= Blidx.
        case: ((fix splc _ (sp: list _) := _) _ _) => //= > /(_ _ eq_refl).
        rewrite map_app. move/NB.eval_exprlist_app_inv.
        rewrite map_length. case/(_ eq_refl).
        rewrite map_app. move/NB.eval_exprlist_app_inv.
        rewrite map_length. case/(_ eq_refl).
        move=> EClidx EBlidx ER [<-]. rewrite !map_app /=.
        eapply NB.eval_exprlist_app => //.
        eapply NB.eval_exprlist_app. eauto with semanticsnb nbtob.
        econstructor ; eauto 6 with semanticsnb nbtob. }
Qed.

Lemma step_assign_constraints_verified:
  forall ge e e' f k p exp constrs,
    NB.step_stmt ge (NB.State e f (Sassign p exp) k) (NB.State e' f Sskip k) ->
    expr_constraints f (Eacc p) = OK constrs ->
    NB.eval_exprlist e f constrs (Some (map (fun _ => Vtrue) constrs)).
Proof.
  inv 1. inv H6.
  apply: eval_path_Some_constraints_verified; eauto with semanticsnb.
Qed.

Lemma eval_exprlist_Some_constraints_verified:
  forall e f le lv constrs,
    NB.eval_exprlist e f le (Some lv) ->
    exprlist_constraints f le = OK constrs ->
    NB.eval_exprlist e f constrs (Some (map (fun _ => Vtrue) constrs)).
Proof.
  intros e f. induction le; simpl; intros * Heval Hconstrs.
  - revert Hconstrs; intros [= <-].
    inversion_clear Heval. exact (NB.eval_Enil _ _).
  - monadInv Hconstrs. inversion_clear Heval.
    rewrite map_app. apply NB.eval_exprlist_app; eauto.
    apply eval_expr_Some_constraints_verified with (1 := H) (2 := EQ1).
Qed.

#[local]
Hint Resolve eval_exprlist_Some_constraints_verified : nbtob.

Lemma eval_path_list_Some_constraints_verified e f:
  forall lp lp' constrs,
    NB.eval_path_list e f lp (Some lp') ->
    exprlist_constraints f (map Eacc lp)  = OK constrs ->
    NB.eval_exprlist e f constrs (Some (map (fun _ => Vtrue) constrs)).
Proof.
  induction lp; simpl; intros * Heval Hconstrs.
  - revert Hconstrs; intros [= <-].
    inversion_clear Heval. exact (NB.eval_Enil _ _).
  - monadInv Hconstrs. inversion_clear Heval.
    rewrite map_app. apply NB.eval_exprlist_app; eauto.
    apply eval_path_Some_constraints_verified with (1 := H) (2 := EQ1).
Qed.

Lemma invalid_index_constraints_verified:
  forall e f lsz lvsz lidx lvidx constrs,
    NB.eval_identlist e f lsz lvsz ->
    NB.eval_exprlist e f lidx (Some lvidx) ->
    bound_constraints lidx lsz = OK constrs ->
    all_Vint64 lvidx ->
    all_Vint64 lvsz ->
    ~ ExprSem.valid_index lvidx lvsz ->
    exists l1 c l2,
      constrs = l1 ++ c :: l2 /\
      NB.eval_exprlist e f l1 (Some (map (fun _ => Vtrue) l1)) /\
      NB.eval_expr e f c (Some Vfalse).
Proof.
  move=> ??. elim/list_ind.
  + move=> ? lidx >. elim/NB.inv_eval_identlist => // _ _ _.
    case: lidx => // + [<-]. elim/NB.inv_eval_exprlist => // _ _ [<-] //=.
  + move=> ?? IH ? lidx >. elim/NB.inv_eval_identlist => // _ ? v1 ? lvsz.
    intro_ceq=> -> -> Ha Hsz Hl _. case: lidx => // > + /=.
    destruct_match_goal_ni => _. elim Cl: bound_constraints => //= + [<-].
    elim/NB.inv_eval_exprlist => // _ ? v2 ? lvidx. intro_ceq => -> -> ++ <- /=.
    destruct_match_goal_ni => _. move=> Ha1 Hl1 Hlvidx Hlvsz.
    case: Z.ltb_spec0 => /= B.
    * move/(IH _ _ _ _ Hl Hl1 Cl Hlvidx Hlvsz) => [] x1 [] c [] x2 [] -> [].
      move=> Hx1 Hc. rewrite app_comm_cons.
      do 3 apply: ex_intro. do 2 apply: conj => //.
      simpl. econstructor ; eauto 6 with semanticsnb nbtob.
    * move=> _. apply: (ex_intro _ []). do 2 apply: ex_intro.
      do 2 apply: conj => //; eauto 10 with semanticsnb.
      apply: NB.eval_Binop_cmpu; eauto with semanticsnb.
      apply: NB.eval_Access; eauto with semanticsnb.
      simpl. by rewrite Ha. all: simpl; eauto with semanticsnb nbtob.
Qed.

Lemma eval_expr_None_constraints_not_verified:
  forall e f exp constrs,
    NB.eval_expr e f exp None ->
    expr_constraints f exp = OK constrs ->
    exists l1 c l2, constrs = l1 ++ c :: l2 /\
               NB.eval_exprlist e f l1 (Some (map (fun _ => Vtrue) l1)) /\
               NB.eval_expr e f c (Some Vfalse).
Proof.
  move=> e f. fix IH 1. case. shelve.
  { inversion 1. }
  { move=> ? t1 t2 constrs H. inv H => /=.
    - case Hc: expr_constraints => //=.
      inv H4; move: H6 => /=; case: t2 H5 => //= >; repeat destruct_match_goal_ni.
      9: by rewrite/cast_allowed /= => -> //.
      all: move=> H _ _ _ [<-].
      1: have: ~ (exists n, Float32.to_int f0 = Some n) by rewrite H => [[]] //.
      1: move/(proj2 (not_iff_compat (float32_to_int_test _))).
      2: have: ~ (exists n, Float32.to_intu f0 = Some n) by rewrite H => [[]] //.
      2: move/(proj2 (not_iff_compat (float32_to_uint_test _))).
      3: have: ~ (exists n, Float32.to_long f0 = Some n) by rewrite H => [[]] //.
      3: move/(proj2 (not_iff_compat (float32_to_int64_test _))).
      4: have: ~ (exists n, Float32.to_longu f0 = Some n) by rewrite H => [[]] //.
      4: move/(proj2 (not_iff_compat (float32_to_uint64_test _))).
      5: have: ~ (exists n, Float.to_int f0 = Some n) by rewrite H => [[]] //.
      5: move/(proj2 (not_iff_compat (float64_to_int_test _))).
      6: have: ~ (exists n, Float.to_intu f0 = Some n) by rewrite H => [[]] //.
      6: move/(proj2 (not_iff_compat (float64_to_uint_test _))).
      7: have: ~ (exists n, Float.to_long f0 = Some n) by rewrite H => [[]] //.
      7: move/(proj2 (not_iff_compat (float64_to_int64_test _))).
      8: have: ~ (exists n, Float.to_longu f0 = Some n) by rewrite H => [[]] //.
      8: move/(proj2 (not_iff_compat (float64_to_uint64_test _))).
      all: (set t1 := Float32.cmp _ _ _) || set t1 := Float.cmp _ _ _.
      all: (set t2 := Float32.cmp _ _ _) || set t2 := Float.cmp _ _ _.
      all: (elim Hcmp1: t1;
            [ change (?a ++ ?b :: ?c :: ?d) with (a ++ [b] ++ c :: d);
              rewrite app_assoc; elim Hcmp2: t2|]); move {H} => H.
      all: try (apply: False_ind; by apply: H).
      all: have Hc' := eval_expr_Some_constraints_verified _ _ _ _ _ H3 Hc.
      all: do 3 eexists; apply: conj; [move=> //|apply: conj] => //.
      all: try (rewrite map_app; apply: NB.eval_exprlist_app => //;
                apply: NB.eval_Econs_Some_e; [|apply: NB.eval_Enil];
                apply: NB.eval_Binop_cmp; eauto with semanticsnb => /=;
                do 2 apply: f_equal => //).
      all: apply: NB.eval_Binop_cmp; eauto with semanticsnb => /=.
      all: do 2 apply: f_equal => //.
    - case Hc: expr_constraints => //=.
      repeat destruct_match_goal_ni; intro_ceq; intros; subst; eauto.
      all: case: (IH _ _ H1 Hc) => l1 [x [l2 [-> [Hl1 Hx]]]].
      all: rewrite -app_assoc -app_comm_cons; eauto 10. }
  { move=>> H /=. inv H. by apply: IH. }
  { move=> b o > H /=. inv H.
    all: case Hc1: expr_constraints => //=; case Hc2: expr_constraints => //=.
    all: repeat destruct_match_goal_ni.
    all: move=> Hb _ [<-] || move=> _ [<-].
    all: move: Hb => Hb || (try case Hb: b H6 H7 => H6 H7 //).
    all: try (case: (IH _ _ H1 Hc1) => ? [? [? [-> [Hl1 Hx]]]];
              rewrite -app_assoc -app_comm_cons; eauto 7).
    all: try (case: (IH _ _ H5 Hc2) => ? [? [? [-> [Hl1 Hx]]]];
              have Hl1':= eval_expr_Some_constraints_verified _ _ _ _ _ H2 Hc1;
              try rewrite -app_assoc -app_comm_cons;
              rewrite app_assoc; repeat eexists;
                [rewrite map_app|]; eauto with semanticsnb; fail).
    all: have Hc1' := eval_expr_Some_constraints_verified _ _ _ _ _ H4 Hc1.
    all: have Hc2' := eval_expr_Some_constraints_verified _ _ _ _ _ H5 Hc2.
    all: case: v1 H4 H5 H6 H7 => //=; case: v2 => //=.
    all: rewrite/divs/divu/mods/modu/divs64/divu64/mods64/modu64 =>> E1 E2 _.
    all: destruct_match_goal_ni => + _.
    all: case/orb_prop_false_first => [T1|[T1 T2]] || move=> T1.
    all: rewrite app_assoc.
    all: try (repeat eexists; [rewrite map_app|eapply NB.eval_Binop_cmp];
              repeat (simpl; eauto with semanticsnb nbtob); fail).
    all: change (?a :: ?b) with ([a] ++ b); rewrite app_assoc.
    all: repeat eexists; [rewrite !map_app|eapply NB.eval_Binop_arith].
    all: eauto with semanticsnb; simpl; try case/andP: T2 => -> -> //.
    all: apply NB.eval_exprlist_app; eauto with semanticsnb.
    all: apply NB.eval_Econs_Some_e; eauto with semanticsnb.
    all: eapply NB.eval_Binop_cmp; eauto with semanticsnb; simpl; by rewrite T1. }
  all: move=>> H /=.
  all: case Hc1: expr_constraints => //; case Hc2: expr_constraints => //=.
  all: move=> [<-]; inv H.
  1,3: case: (IH _ _ H1 Hc1) => ? [? [? [-> [Hl1 Hx]]]];
       rewrite -app_assoc -app_comm_cons; eauto 7.
  all: case: (IH _ _ H5 Hc2) => ? [? [? [-> [Hl1 Hx]]]];
       have Hc1' := eval_expr_Some_constraints_verified _ _ _ _ _ H2 Hc1;
       rewrite app_assoc; repeat eexists; [rewrite map_app|];
       eauto with semanticsnb.
  Unshelve.
  move=> [i synpl] constrs H. inv H. inv H1 => /=.
  rewrite H2 {H2} /=. set splc := fix splc llsz0 (sp: list syn_path_elem) := _.
  move: synpl llsz constrs H3. elim/list_ind => /=.
  - move=>> H. inv H.
  - move=> [lidx] synpl IHsynpl > H. inv H.
    + rewrite Scell_constraints.
      case Clidx: exprlist_constraints => //=.
      case B: bound_constraints => [bc|] //=.
      case R: splc => [r|] //=.
      move=> [<-].
      have Ha := eval_exprlist_Some_constraints_verified _ _ _ _ _ H4 Clidx.
      have: exists (l1 : list expr) (c : expr) (l2 : list expr),
              bc = l1 ++ c :: l2 /\
              NB.eval_exprlist e f l1 (Some (map (fun=> Vtrue) l1)) /\
              NB.eval_expr e f c (Some Vfalse).
      { have Ilvsz  := natlist_of_Vint64_all_Vint64 _ _ H3.
        have Ilvidx := build_index_all_Vint64 _ _ _ H5.
        move: lidx lvidx lsz lvsz bc H4 H2 H7 Ilvsz Ilvidx B {Clidx shape H3 H5}.
        elim/list_ind => /=.
        - move=> ? [|//] > H H'. inv H. inv H' => //.
        - move=> exp lidx IHlidx ? [//|] sz lsz > H H'. inv H. inv H'.
          move=> /=. case: v H3 => // > Hexp; case: v0 H1 => //= > Hsz.
          case B: bound_constraints => //= +++ [<-].
          case/nandP' => [T|[T V]] H1 H3.
          + move/Z.ltb_spec0 in T.
            rewrite -(app_nil_l (_ :: _)).
            repeat eexists; eauto 7 with semanticsnb nbtob.
          + move/negbTE in V. move/Z.ltb_spec0 in T.
            case (IHlidx _ _ _ _ H4 H6 V H1 H3 B) => ? [? [? [-> [Hl1 Hx]]]].
            rewrite app_comm_cons.
            repeat eexists; simpl; try econstructor; eauto 7 with semanticsnb nbtob. }
      case=> ? [? [? [-> [? ?]]]]. rewrite -2!app_assoc -app_comm_cons app_assoc.
      repeat eexists; [rewrite map_app|]; eauto with semanticsnb.
    + rewrite Scell_constraints. case E: exprlist_constraints => [ec|] //=.
      case: bound_constraints => //= >. case splc => //= > [<-].
      have: exists l1 c l2, ec = l1 ++ c :: l2 /\
                       NB.eval_exprlist e f l1 (Some (map (fun=> Vtrue) l1)) /\
                       NB.eval_expr e f c (Some Vfalse).
      { move: lidx ec H5 E. elim/list_ind.
        - move=>> H. inv H.
        - move=>> IHlidx > H /=. case Clidx: exprlist_constraints => //=.
          case Cexp: expr_constraints => //= > [<-]. inv H.
          + case: (IH _ _ H1 Cexp) => ? [? [? [-> [??]]]].
            rewrite -app_assoc -app_comm_cons. eauto 7.
          + case: (IHlidx _ H5 Clidx) => ? [? [? [-> [??]]]].
            have H := eval_expr_Some_constraints_verified _ _ _ _ _ H4 Cexp.
            rewrite app_assoc.
            repeat eexists; [rewrite map_app|]; eauto with semanticsnb. }
      case=> ? [? [? [-> [? ?]]]]. rewrite -!app_assoc -app_comm_cons.
      repeat eexists; eauto.
    + rewrite Scell_constraints => //= >.
      case Clidx: exprlist_constraints => //=.
      case B: bound_constraints => //=. case R: splc => //= - [<-].
      have HClidx := eval_exprlist_Some_constraints_verified _ _ _ _ _ H4 Clidx.
      have HB := bound_constraints_verified _ _ _ _ _ _ _ _ H2 H3 H4 H6 B.
      case (IHsynpl _ _ H8 R) =>> [> [> [-> [*]]]]. rewrite app_assoc.
      repeat eexists; [rewrite !map_app|]; eauto with semanticsnb.
Qed.

#[local]
Hint Resolve eval_expr_None_constraints_not_verified : nbtob.

Lemma eval_path_None_constraints_not_verified e f:
  forall p constrs,
    NB.eval_path e f p None ->
    expr_constraints f (Eacc p) = OK constrs ->
    exists l1 c l2, constrs = l1 ++ c :: l2 /\
               NB.eval_exprlist e f l1 (Some (map (fun _ => Vtrue) l1)) /\
               NB.eval_expr e f c (Some Vfalse).
Proof.
  move=> [i synpl] constrs H. inv H => /=.
  rewrite H2 {H2} /=. set splc := fix splc llsz0 (sp: list syn_path_elem) := _.
  move: synpl llsz constrs H3. elim/list_ind => /=.
  - move=>> H. inv H.
  - move=> [lidx] synpl IHsynpl > H. inv H.
    + rewrite Scell_constraints.
      case Clidx: exprlist_constraints => //=.
      case B: bound_constraints => [bc|] //=.
      case R: splc => [r|] //=.
      move=> [<-].
      have Ha := eval_exprlist_Some_constraints_verified _ _ _ _ _ H4 Clidx.
      have: exists (l1 : list expr) (c : expr) (l2 : list expr),
              bc = l1 ++ c :: l2 /\
              NB.eval_exprlist e f l1 (Some (map (fun=> Vtrue) l1)) /\
              NB.eval_expr e f c (Some Vfalse).
      { have Ilvsz  := natlist_of_Vint64_all_Vint64 _ _ H3.
        have Ilvidx := build_index_all_Vint64 _ _ _ H5.
        move: lidx lvidx lsz lvsz bc H4 H2 H7 Ilvsz Ilvidx B {Clidx shape H3 H5}.
        elim/list_ind => /=.
        - move=> ? [|//] > H H'. inv H. inv H' => //.
        - move=> exp lidx IHlidx ? [//|] sz lsz > H H'. inv H. inv H'.
          move=> /=. case: v H3 => // > Hexp. case: v0 H1 => //= > Hsz.
          case B: bound_constraints => //= +++ [<-].
          case/nandP' => [T|[T V]] H1 H3.
          + move/Z.ltb_spec0 in T.
            rewrite -(app_nil_l (_ :: _)).
            repeat eexists; eauto 7 with semanticsnb nbtob.
          + move/negbTE in V. move/Z.ltb_spec0 in T.
            case (IHlidx _ _ _ _ H4 H6 V H1 H3 B) => ? [? [? [-> [Hl1 Hx]]]].
            rewrite app_comm_cons.
            repeat eexists; simpl; try econstructor; eauto 7 with semanticsnb nbtob. }
      case=> ? [? [? [-> [? ?]]]]. rewrite -2!app_assoc -app_comm_cons app_assoc.
      repeat eexists; [rewrite map_app|]; eauto with semanticsnb.
    + rewrite Scell_constraints. case E: exprlist_constraints => [ec|] //=.
      case: bound_constraints => //= >. case splc => //= > [<-].
      have: exists l1 c l2, ec = l1 ++ c :: l2 /\
                       NB.eval_exprlist e f l1 (Some (map (fun=> Vtrue) l1)) /\
                       NB.eval_expr e f c (Some Vfalse).
      { move: lidx ec H5 E. elim/list_ind.
        - move=>> H. inv H.
        - move=>> IHlidx > H /=. case Clidx: exprlist_constraints => //=.
          case Cexp: expr_constraints => //= > [<-]. inv H.
          + case: (eval_expr_None_constraints_not_verified _ _ _ _ H1 Cexp)
                => ? [? [? [-> [??]]]].
            rewrite -app_assoc -app_comm_cons. eauto 7.
          + case: (IHlidx _ H5 Clidx) => ? [? [? [-> [??]]]].
            have H := eval_expr_Some_constraints_verified _ _ _ _ _ H4 Cexp.
            rewrite app_assoc.
            repeat eexists; [rewrite map_app|]; eauto with semanticsnb. }
      case=> ? [? [? [-> [? ?]]]]. rewrite -!app_assoc -app_comm_cons.
      repeat eexists; eauto.
    + rewrite Scell_constraints => //= >.
      case Clidx: exprlist_constraints => //=.
      case B: bound_constraints => //=. case R: splc => //= - [<-].
      have HClidx := eval_exprlist_Some_constraints_verified _ _ _ _ _ H4 Clidx.
      have HB := bound_constraints_verified _ _ _ _ _ _ _ _ H2 H3 H4 H6 B.
      case (IHsynpl _ _ H8 R) =>> [> [> [-> [*]]]]. rewrite app_assoc.
      repeat eexists; [rewrite !map_app|]; eauto with semanticsnb.
Qed.

Lemma get_szenv_syn_path_same_domain_and_structure {sze sze'}:
  forall p llsz,
    same_domain sze sze' ->
    same_structure sze sze' ->
    get_szenv_syn_path sze p = Some llsz ->
    exists llsz', get_szenv_syn_path sze' p = Some llsz' /\
                  Forall2 (fun lsz lsz' => length lsz = length lsz') llsz llsz'.
Proof.
  case=> [i l] llsz DOM STR /=. case Hllsz: sze!i => [llsz0|] //=.
  have [llsz0' Hllsz']: exists llsz', sze'!i = Some llsz'.
  { move: Hllsz => /(PTree.elements_correct sze).
    move/(in_map_pair_fst (PTree.elements sze))/DOM.
    case/(in_map_fst_pair (PTree.elements sze')) => llsz'.
    move/(PTree.elements_complete sze') => ->. by exists llsz'. }
  rewrite Hllsz' /=. have:= STR _ _ _ Hllsz Hllsz'.
  elim/list_ind: l llsz0 llsz0' {Hllsz Hllsz'} => /=.
  - move=>> /[swap] - [<-] T. eauto.
  - case=>> _ > IH' [//|??] > T. inv T. by apply: IH'.
Qed.

Lemma valid_call_tests_nth_error':
  forall caller callee args tests,
    valid_call_tests caller callee args = OK tests ->
    forall n sz sz0,
      nth_error tests n = Some (sz, sz0) ->
      exists i j k llsz0 lsz0,
        In i (map fst args) /\
        (fn_szenv' caller)!i = Some llsz0 /\
        nth_error llsz0 j = Some lsz0 /\ nth_error lsz0 k = Some sz0.
Proof.
  move=> caller callee args tests VC n sz sz0 H.
  have:= VC; rewrite/valid_call_tests; case HM: args_map => [M|] //= _.
  have [ap AP] := args_map_args_params _ _ _ HM.
  have:= valid_call_tests_nth_error _ _ _ _ _ _ VC AP HM _ _ _ H.
  intros ([i l] & ? & j & k & _ & _ & _ & l0 & ? & IN & X & _ & _ & _ & J & K & _).
  have [LLSZ0 [X' LEN]]:= get_szenv_syn_path_full _ _ _ _ X => {X}.
  have:= @nth_error_app2 _ LLSZ0 l0 (j + length LLSZ0)%nat.
  rewrite PeanoNat.Nat.add_sub J => Y.
  have [>] := In_nth_error _ _ IN.
  move/(args_params_nth_error_l _ _ _ _ _ _ AP) => /(nth_error_In args).
  move/(in_map_pair_fst args) => IN'.
  repeat eexists. exact IN'. exact X'. apply: Y; lia. exact K.
Qed.

Lemma call_constraints_tests_verified caller e:
  forall (lt : list (expr * SemPath.ident)) (lx : list expr)
         (lx0 : list SemPath.ident) (li1 li2 : list int64)
         (constrs : list expr),
    split lt = (lx, lx0) ->
    eval_expr_list_to_int64 e caller lx = ExprSem.Val li1 ->
    eval_expr_list_to_int64 e caller
      (map (fun x : SemPath.ident => Eacc (x, [])) lx0) = ExprSem.Val li2 ->
    fold_right_res
      (fun acc x =>
        do r <- (fun '(e, i) =>
                  do r <- expr_constraints caller e;
                  OK (r ++ [Ebinop_cmp Oeq OInt64 e (Eacc (i, []))])) x;
                  OK (r ++ acc)) lt [] = OK constrs ->
    forallb (fun '(i, j) => Int64.eq i j) (combine li1 li2) ->
    NB.eval_exprlist e caller constrs (Some (map (fun=> Vtrue) constrs)).
Proof.
  elim/list_ind => /=.
  - move=>> [<- <-] /= _ _ [<-]. econstructor.
  - move=> [y y0] > IH. case Sp: split =>> [<- <-]. cbn -[ES.eval_expr].
    case Ey: ES.eval_expr => [[]| |] //.
    case Ey0: ES.eval_expr => [[]| |] //. simpl.
    case Ely: eval_expr_list_to_int64 => //=.
    case Ely0: eval_expr_list_to_int64 => //=.
    move=> [<-] [<-]. case F: fold_right_res => //=.
    case Cy: expr_constraints =>> //= [<-]. case/andP => EQ H.
    rewrite -app_assoc -app_comm_cons /=.
    have ? := IH _ _ _ _ _ Sp Ely Ely0 F H.
    move/NBfacts.eval_expr_fixpoint_match_Some in Ey.
    have ? := eval_expr_Some_constraints_verified _ _ _ _ _ Ey Cy.
    move/NBfacts.eval_expr_fixpoint_match_Some in Ey0.
    rewrite map_app. apply NB.eval_exprlist_app => //=.
    econstructor => //. eapply NB.eval_Binop_cmp.
    eassumption. eassumption. simpl. by rewrite EQ.
Qed.

Lemma call_constraints_verified:
  forall e caller callee args pargs constrs lvtests,
    NB.eval_path_list e caller args (Some pargs) ->
    call_constraints caller callee args = OK constrs ->
    length args = length (fd_params callee) ->
    valid_call_tests_values caller callee e args = ES.Val lvtests ->
    equal_tests lvtests = true ->
    NB.eval_exprlist e caller constrs (Some (map (fun _ => Vtrue) constrs)).
Proof.
  rewrite/call_constraints.
  move=> e caller callee args0 pargs0 constrs0 lvtests EVp + LEN ETESTS EQ.
  have [pp PP]: exists pp, pargs_params (fd_params callee) pargs0 = Some pp.
  { apply (proj2 (map2_Some _ _ _)). by rewrite -(NB.eval_path_list_length _ _ _ _ EVp). }
  move: ETESTS. rewrite/valid_call_tests_values.
  case Htests: valid_call_tests => [tests|] //=.
  have H := valid_call_tests_nth_error' _ _ _ _ Htests.
  case Sp: split => [lx lx0].
  case Elx0: eval_expr_list_to_int64 => //=.
  case Elx: eval_expr_list_to_int64 =>> //= [T].
  rewrite -T {T} in EQ. move: EQ => /[swap].
  by apply: call_constraints_tests_verified Sp Elx Elx0.
Qed.

Lemma fold_right_res_list_app {A B: Type}
                              (f: B -> res (list A)):
  forall l1 l2 r,
    fold_right_res (fun acc x =>
      do r <- f x;
      OK (r ++ acc)) (l1 ++ l2) [] = OK r ->
    exists r1 r2,
      fold_right_res (fun acc x =>
        do r <- f x;
        OK (r ++ acc)) l1 [] = OK r1 /\
      fold_right_res (fun acc x =>
        do r <- f x;
        OK (r ++ acc)) l2 [] = OK r2 /\
      r = r1 ++ r2.
Proof.
  elim/list_ind => /=.
  - move=>> ->. eauto.
  - move=>> IH >. case H: fold_right_res => //=.
    have [r1 [r2 [-> [-> ->]]]]:= IH _ _ H => {IH H} /=.
    case f =>> //= [<-]. repeat eexists. by rewrite app_assoc.
Qed.

Lemma call_constraints_not_verified:
  forall e caller callee args pargs vargs constrs lvtests,
    NB.eval_path_list e caller args (Some pargs) ->
    get_env_path_list e pargs = Some vargs ->
    well_typed_valuelist (sig_args (fd_sig callee)) vargs ->
    call_constraints caller callee args = OK constrs ->
    length args = length (fd_params callee) ->
    valid_call_tests_values caller callee e args = ES.Val lvtests ->
    ~ equal_tests lvtests ->
    exists l1 c l2,
      constrs = l1 ++ c :: l2 /\
      NB.eval_exprlist e caller l1 (Some (map (fun _ => Vtrue) l1)) /\
      NB.eval_expr e caller c (Some Vfalse).
Proof.
  move=> e caller callee args0 pargs0 vargs0 constrs0 lvtests.
  move=> EVp Ep WT + LEN + EQ.
  cbv delta [call_constraints] beta. set f := fun _ => _. simpl.
  have [pp PP]: exists pp, pargs_params (fd_params callee) pargs0 = Some pp.
  { apply (proj2 (map2_Some _ _ _)). by rewrite -(NB.eval_path_list_length _ _ _ _ EVp). }
  rewrite/valid_call_tests_values. case Htests: valid_call_tests => [tests|] //= /[swap].
  move: EQ => /[swap]. case Sp: split => [lx lx0].
  case Elx0: eval_expr_list_to_int64 => [lnx0| |] //=.
  case Elx: eval_expr_list_to_int64 => [lnx| |] //= [<-].
  move: Htests => /[swap]/negP.
  case/forallb_false_exists_first => l1 [[i1 i2] [l2 [EQ [H1 H2]]]] Htests.
  have LEN': length lnx = length lnx0.
  { rewrite -(eval_expr_list_to_int64_length _ _ _ _ Elx).
    rewrite -(eval_expr_list_to_int64_length _ _ _ _ Elx0) map_length.
    have:= split_length_r tests. have:= split_length_l tests.
    rewrite Sp /= => <- <- //. }
  have:= combine_eq_app _ _ _ _ LEN' EQ => {LEN' EQ}.
  intros (l11 & [|? l12] & l21 & [|? l22] & -> & -> & LEN1 & LEN2 & <- & T) => //.
  case: T LEN2. intros -> -> <- => /= /eq_add_S LEN2.
  move/eval_expr_list_to_int64_eq_app: Elx. intros (lx1 & lx2 & -> & Elx1 & T).
  case: lx2 T Sp => [|x lx2] //=. case Ex: ES.eval_expr => [[]| |] //=.
  case Elx2: eval_expr_list_to_int64 =>> //=. intros [= -> ->].
  move/eval_expr_list_to_int64_eq_app: Elx0 => [>] [>] [] + [].
  move/map_eq_app. intros (lx01 & [|x0 lx02] & -> & <- & <-) => // Elx01.
  cbn -[ES.eval_expr]. case Ex0: ES.eval_expr => [[]| |] // => /=.
  case Elx02: eval_expr_list_to_int64 => //=. intros [= -> ->] Sp.
  have: length tests = (length lx1 + S (length lx2))%nat.
  { have:= split_length_l tests. rewrite Sp /= => <-. by rewrite app_length. }
  have: length lx2 = length lx02.
  { have:= eval_expr_list_to_int64_length _ _ _ _ Elx02. rewrite map_length => ->.
    rewrite (eval_expr_list_to_int64_length _ _ _ _ Elx2) //. }
  move/eq_S. have: length lx1 = length lx01.
  { have:= eval_expr_list_to_int64_length _ _ _ _ Elx01. rewrite map_length => ->.
    rewrite (eval_expr_list_to_int64_length _ _ _ _ Elx1) //. }
  move/split_eq_app: Sp => /[apply]/=/[apply]/[apply].
  intros (lt1 & [|[??] lt2] & -> & LENt1 & LENt2 & Sp1 & Sp2) => //.
  move: Sp2 => /=. case Sp2: split. intros [= -> -> -> ->].
  case/fold_right_res_list_app => c1 [+] [C1] [] /=.
  case C2: fold_right_res => [c2|] //=.
  case Cx: expr_constraints => [cx|] //= + [<-] => _ -> {constrs0}.
  move/NBfacts.eval_expr_fixpoint_match_Some in Ex.
  have Ecx := eval_expr_Some_constraints_verified _ _ _ _ _ Ex Cx.
  have Ec1: NB.eval_exprlist e caller c1 (Some (map (fun=> Vtrue) c1)).
  by apply: call_constraints_tests_verified Sp1 Elx1 Elx01 C1 H1.
  rewrite -app_assoc -app_comm_cons /= app_assoc. repeat eexists.
  rewrite map_app. eauto with semanticsnb.
  move/NBfacts.eval_expr_fixpoint_match_Some in Ex0.
  eapply NB.eval_Binop_cmp. eassumption. eassumption. simpl. by case: Int64.eq H2.
Qed.

Lemma eval_exprlist_cut_on_None e f:
  forall le,
    NB.eval_exprlist e f le None ->
    exists le1 c le2 lv1,
      le = le1 ++ c :: le2 /\
      NB.eval_exprlist e f le1 (Some lv1) /\
      NB.eval_expr e f c None.
Proof.
  elim/list_ind => [|> IH] /= T; inv T.
  - exists []; repeat eexists; eauto with semanticsnb.
  - have [le1 [c [le2 [lv [-> [Hle1 Hc]]]]]] := IH H2.
    rewrite app_comm_cons. repeat eexists; eauto with semanticsnb.
Qed.

Lemma call_constraints_not_verified_eval_Err:
  forall e caller callee args pargs vargs constrs,
    NB.valid_env_szenv e (fn_szenv' caller) (map fst args) ->
    NB.eval_path_list e caller args (Some pargs) ->
    get_env_path_list e pargs = Some vargs ->
    well_typed_valuelist (sig_args (fd_sig callee)) vargs ->
    call_constraints caller callee args = OK constrs ->
    length args = length (fd_params callee) ->
    valid_call_tests_values caller callee e args = ES.Err ->
    exists l1 c l2,
      constrs = l1 ++ c :: l2 /\
               NB.eval_exprlist e caller l1 (Some (map (fun _ => Vtrue) l1)) /\
               NB.eval_expr e caller c (Some Vfalse).
Proof.
  move=> e caller callee args0 pargs0 vargs0 constrs0.
  move=> VENV EVp Ep WT + LEN.
  cbv delta [call_constraints] beta. set f := fun _ => _. simpl.
  have [pp PP]: exists pp, pargs_params (fd_params callee) pargs0 = Some pp.
  { apply (proj2 (map2_Some _ _ _)). by rewrite -(NB.eval_path_list_length _ _ _ _ EVp). }
  rewrite/valid_call_tests_values. case Htests: valid_call_tests => [tests|] //= /[swap].
  case Sp: split => [lx lx0].
  case Elx0: eval_expr_list_to_int64 => [lnx| |] //=.
  case Elx: eval_expr_list_to_int64 => [lnx0| |] //=.
  - move=> _. case/NBfacts.eval_expr_list_to_int64_Err: Elx.
    move=> lx1 [x] [lx2] [lix1] [T] [Elx1 Ex].
    rewrite T {T lx} in Sp.
    have: exists z, nth_error lx0 (length lx1) = Some z.
    { have:= nth_error_Some lx0 (length lx1).
      case (nth_error lx0 (length lx1)). eauto. intro H. exfalso. apply H => //.
      have:= split_length_l tests. have:= split_length_r tests.
      rewrite Sp /= => <- <-. rewrite app_length /=. lia. }
    case=> x0 /(@nth_error_split SemPath.ident) => - [lx01] [lx02] [T] LEN1.
    rewrite T {T lx0} map_app /= in Sp Elx0.
    have:= eval_expr_list_to_int64_app _ _ _ _ _ Elx0 => {Elx0}.
    intros (lix01 & lix02 & -> & Elx01 & Elx02).
    have LEN2: length lx2 = length lx02.
    { have:= split_length_l tests. have:= split_length_r tests. rewrite Sp /=.
      rewrite !app_length /= LEN1. lia. }
    have:= split_eq_app _ _ _ _ _ (eq_sym LEN1) _ _ Sp => /(_ (eq_S _ _ LEN2)).
    have:= split_length_l tests. rewrite Sp /= app_length => <- /(_ eq_refl).
    case=> lt1 [[|[??] lt2]] [->] [LENt1] [LENt2] //= [Sp1] {LEN1 LEN2 Sp}.
    case Sp2: split. intros [= -> -> -> ->].
    case/fold_right_res_list_app => c1 [t] [C1] [/= + ->].
    case fold_right_res => [c2|] //=.
    case Cx: expr_constraints => [cx|] //= [<-].
    move/NBfacts.eval_expr_fixpoint_match_None in Ex.
    have:= eval_expr_None_constraints_not_verified _ _ _ _ Ex Cx.
    intros (cx1 & c & cx2 & -> & Ecx1 & Ec).
    case EQ: (forallb (fun '(i, j) => Int64.eq i j) (combine lix1 lix01)).
    + exists (c1 ++ cx1), c, (cx2 ++ [Ebinop_cmp Oeq OInt64 x (Eacc (x0, []))] ++ c2).
      repeat split => //.
      * by rewrite -!app_assoc -app_comm_cons.
      * rewrite map_app. apply NB.eval_exprlist_app => //.
        by apply: call_constraints_tests_verified Sp1 Elx1 Elx01 C1 EQ.
    + move/negP/negP/forallb_false_exists_first: EQ.
      intros (lt1' & [i0 j0] & lt2' & EQ & H1 & H2).
      have: length lix1 = length lix01.
      { rewrite -(eval_expr_list_to_int64_length _ _ _ _ Elx1).
        have:= split_length_r lt1. rewrite -LENt1 Sp1 /= => <-.
        rewrite -(eval_expr_list_to_int64_length _ _ _ _ Elx01) map_length //. }
      move/combine_eq_app: EQ => /[apply].
      intros (l11 & [|? l12] & l21 & [|? l22] & -> & -> & LEN1 & LEN2 & <- & T) => //.
      revert T; intros [= -> -> <-]. simpl in LEN2.
      move/eval_expr_list_to_int64_eq_app: Elx1.
      intros (lx1' & [|y lx2'] & -> & E1 & E2) => //.
      move: E2 => /=. case Ey: ES.eval_expr => [[]| |] //=.
      case E2: eval_expr_list_to_int64 =>> //=. intros [= -> ->].
      move/eval_expr_list_to_int64_eq_app: Elx01.
      intros (u1 & [|z u2] & U & E1' & E2') => //.
      move/map_eq_app: U. intros (lx01' & [|y0 lx02'] & -> & <- & U) => //.
      revert U; intros [= <- <-].
      move: E2'. cbn -[ES.eval_expr]. case Ey0: ES.eval_expr => [[]| |] // => /=.
      case E2': eval_expr_list_to_int64 =>> //= [T1 T2].
      rewrite T1 T2 {T1 T2} in Ey0 E2'. rewrite app_length in LENt1.
      have: length lx1' = length lx01'.
      { rewrite (eval_expr_list_to_int64_length _ _ _ _ E1) LEN1.
        rewrite -(eval_expr_list_to_int64_length _ _ _ _ E1') map_length //. }
      have: length (y :: lx2') = length (y0 :: lx02').
      { rewrite /= (eval_expr_list_to_int64_length _ _ _ _ E2) LEN2.
        rewrite -(eval_expr_list_to_int64_length _ _ _ _ E2') map_length //. }
      move/split_eq_app: Sp1 => /[apply]/[apply]/(_ LENt1) {LEN1 LEN2 E2 E2' LENt1}.
      intros (lt11 & [|[??] lt12] & -> & _ & _ & Sp11 & Sp12) => //.
      move: Sp12 => /=. case Sp12: split. intros [= -> -> -> ->].
      case/fold_right_res_list_app: C1 => c11 [u] [C11] [/= +] ->.
      case fold_right_res => [c12|] //=.
      case Cy: expr_constraints => [cy|] //= [<-].
      rewrite -3!app_assoc app_assoc /=.
      move/NBfacts.eval_expr_fixpoint_match_Some in Ey.
      move/NBfacts.eval_expr_fixpoint_match_Some in Ey0.
      repeat eexists.
      rewrite map_app. eapply NB.eval_exprlist_app.
      by apply: call_constraints_tests_verified Sp11 E1 E1' C11 H1.
      by apply: eval_expr_Some_constraints_verified Ey Cy.
      econstructor. exact Ey. exact Ey0. simpl. by case: Int64.eq H2.
  - move=> _. move/NBfacts.eval_expr_list_to_int64_Err: Elx0.
    intros (u1 & z & lx02 & li1 & U & E01 & Ex0). move/map_eq_app: U.
    intros (lx011 & [|x0 lx012] & -> & <- & U2) => //.
    move: U2 => /= [T _]. rewrite -T {T} in Ex0.
    have LEN': length lx = length (lx011 ++ x0 :: lx012).
    { have:= split_length_l tests. have:= split_length_r tests.
      rewrite Sp /= => <- -> //. }
    have [x] : exists x, nth_error lx (length lx011) = Some x.
    { case H: nth_error. eauto. exfalso. rewrite app_length /= in LEN'.
      apply nth_error_Some in H => //. lia. }
    move/(nth_error_split lx). intros (lx11 & lx12 & -> & LEN1).
    have: length tests = (length lx11 + length (x :: lx12))%nat.
    { have:= split_length_l tests. rewrite Sp /= => <-. by apply: app_length. }
    have: length (x :: lx12) = length (x0 :: lx012).
    { rewrite !app_length in LEN'. lia. }
    move/split_eq_app: Sp => /(_ LEN1)/[apply]/[apply].
    intros (lt1 & [|[??] lt2] & -> & LEN1' & LEN2' & Sp1 & Sp2) => //.
    move: Sp2 => /=. case split. intros ?? [= -> -> -> ->].
    have: nth_error (lt1 ++ (x, x0) :: lt2) (length lt1) = Some (x, x0).
    { rewrite nth_error_app2. lia. by rewrite PeanoNat.Nat.sub_diag. }
    have:= valid_call_tests_nth_error' _ _ _ _ Htests.
    move/[apply]. intros (i & j & k & llsz & lsz & IN & Hsze & Hllsz & Hlsz).
    have:= VENV _ _ IN Hsze.
    move/Forall_forall => /(_ _ (nth_error_In _ _ Hllsz)).
    move/Forall_forall => /(_ _ (nth_error_In _ _ Hlsz)).
    case=> n T. move: Ex0; cbn.
    case (fn_szenv' caller)!x0 => [[]|] //=; by rewrite T.
Qed.

Theorem transl_cont_destruct_preservation:
  forall ge f k tk,
    transl_cont ge f k = OK tk ->
    transl_cont ge None (NB.destructCont k) = OK (destructCont tk).
Proof.
  induction k; simpl; intros.
  - revert H; intros [= <-]. reflexivity.
  - destruct f, transl_stmt; try discriminate.
    destruct (transl_cont ge (Some f) k); try discriminate.
    revert H; intros [= <-]. simpl. eauto.
  - destruct (transl_cont ge _ k); try discriminate.
    revert H; intros [= <-]; simpl. now apply IHk.
  - destruct (transl_cont _ (Some f0) _), (transl_function _ _); try discriminate.
    revert H; intros [= <-]. reflexivity.
Qed.

#[local]
Hint Resolve transl_cont_destruct_preservation : nbtob.

Lemma eval_ident_list_preservation ge e f tf:
  transl_function ge f = OK tf ->
  ES.eval_ident_list e f = ES.eval_ident_list e tf.
Proof.
  move=> Htf. unfold ES.eval_ident_list.
  by rewrite (transl_function_same_szenv' _ _ _ Htf).
Qed.

Lemma eval_expr_preservation ge e f tf:
  transl_function ge f = OK tf ->
  ES.eval_expr e f = ES.eval_expr e tf.
Proof.
  move=> Htf. unfold ES.eval_expr.
  rewrite (transl_function_same_szenv' _ _ _ Htf).
  by rewrite (eval_ident_list_preservation _ _ _ _ Htf).
Qed.

Lemma eval_expr_list_to_int64_preservation ge e f tf:
  transl_function ge f = OK tf ->
  eval_expr_list_to_int64 e f = eval_expr_list_to_int64 e tf.
Proof.
  move=> Htf. unfold eval_expr_list_to_int64.
  by rewrite (eval_expr_preservation _ _ _ _ Htf).
Qed.

Lemma valid_call_tests_values_preservation:
  forall ge e f fd tf tfd args lv,
    transl_function ge f = OK tf ->
    transl_fundef ge fd = OK tfd ->
    valid_call_tests_values f fd e args = ES.Val lv ->
    valid_call_tests_values tf tfd e args = ES.Val lv.
Proof.
  move=> ???? > /=.
  move=> Htf Htfd.
  rewrite/valid_call_tests_values/valid_call_tests/=.
  rewrite (transl_fundef_same_params _ _ _ Htfd).
  rewrite (transl_fundef_same_szenv _ _ _ Htfd).
  rewrite (transl_function_same_szenv' _ _ _ Htf).
  by rewrite !(eval_expr_list_to_int64_preservation _ _ _ _ Htf).
Qed.

#[local]
Hint Resolve valid_call_tests_values_preservation : nbtob.

Inductive match_states (p: program) : NB.state -> state -> Prop :=
| match_states_State: forall e f s k tf ts tk
    (TRFUNC: transl_function (genv_of_program p) f = OK tf)
    (TRSTMT: transl_stmt (genv_of_program p) f s = OK ts)
    (TRCONT: transl_cont (genv_of_program p) (Some f) k = OK tk),
    match_states p (NB.State e f s k) (State e tf ts tk)
| match_states_Error: forall e f k tf k'
    (TRFUNC: transl_function (genv_of_program p) f = OK tf),
    match_states p (NB.State e f Serror k) (State e tf Serror k')
| match_states_Callstate: forall f args k tf tk
    (TRFD  : transl_function (genv_of_program p) f = OK tf)
    (TRCONT: transl_cont (genv_of_program p) None k = OK tk),
    match_states p (NB.Callstate f args k)
                   (Callstate tf args tk)
| match_states_Returnstate: forall e f v k tf tk
    (TRFUNC: transl_function (genv_of_program p) f = OK tf)
    (TRCONT: transl_cont (genv_of_program p) (Some f) k = OK tk),
    match_states p (NB.Returnstate e f v k) (Returnstate e tf v tk).

#[local]
Hint Constructors match_states : nbtob.

Lemma is_Kreturnto_preservation:
  forall ge fopt k tk,
    transl_cont ge fopt k = OK tk ->
    NB.is_Kreturnto k ->
    is_Kreturnto tk.
Proof.
  destruct k; simpl; intros; try contradiction.
  destruct transl_cont, transl_function; try discriminate.
  revert H; intros [= <-]. easy.
Qed.

Ltac destruct_transl_program p :=
  match goal with
  | H: (transl_program p = OK _) |- _ =>
      unfold transl_program in H; simpl in H; revert H;
      generalize (transl_fundefs_list_assoc_nodup (genv_of_program p) p.(prog_defs) p.(prog_nodup));
      generalize (transl_fundefs_eq_map_fst (genv_of_program p) p.(prog_defs));
      destruct (transl_fundefs (genv_of_program p) p.(prog_defs)) eqn:Hfd;
      intros Hmapfst Hnodup [= <-];
      destruct p as [ prog_defs prog_main prog_nodup ]
  end.

Lemma eval_exprlist_true_nodup_keep_first' e f:
  forall l nl,
    eval_exprlist e f l (map (fun _ => Vtrue) l) ->
    eval_exprlist e f
      (nodup_keep_first' expr_beq nl l)
      (map (fun _ => Vtrue) (nodup_keep_first' expr_beq nl l)).
Proof.
  elim/list_ind => //= exp l IH nl T. inv T.
  case (mem_dec_spec _ expr_reflect) => I. auto.
  apply eval_Econs; auto.
Qed.

Lemma eval_exprlist_true_nodup_keep_first e f l:
  eval_exprlist e f l (map (fun _ => Vtrue) l) ->
  eval_exprlist e f
    (nodup_keep_first expr_beq l)
    (map (fun _ => Vtrue) (nodup_keep_first expr_beq l)).
Proof. apply eval_exprlist_true_nodup_keep_first' with (nl := []). Qed.

Lemma eval_exprlist_true_nodup_keep_first_app:
  forall e f le1 le2,
    eval_exprlist e f le1 (map (fun _ => Vtrue) le1) ->
    eval_exprlist e f le2 (map (fun _ => Vtrue) le2) ->
    eval_exprlist e f (nodup_keep_first expr_beq (le1 ++ le2))
                        (map (fun _ => Vtrue)
                             (nodup_keep_first expr_beq (le1 ++ le2))).
Proof.
  intros * H1 H2. unfold nodup_keep_first in *.
  rewrite (nodup_keep_first'_app _ expr_reflect). exact (NoDup_nil _).
  rewrite map_app.
  apply eval_exprlist_app; now apply eval_exprlist_true_nodup_keep_first'.
Qed.

Lemma nodup_keep_first'_cut_on_false:
  forall e f le1 le2 l c,
    eval_exprlist e f l (map (fun _ => Vtrue) l) ->
    eval_exprlist e f le1 (map (fun _ => Vtrue) le1) ->
    eval_expr e f c Vfalse ->
    exists le,
      nodup_keep_first' expr_beq l (le1 ++ c :: le2) =
      nodup_keep_first' expr_beq l le1 ++ c :: le.
Proof.
  induction le1; simpl; intros.
  + case (mem_dec_spec _ expr_reflect) => [Hin|_].
    - apply In_nth_error in Hin as [n NTH].
      apply eval_exprlist_nth_error with (1 := H) in NTH as [v [Hv1 Hv2]].
      rewrite nth_error_map in Hv1. destruct (nth_error l n); try discriminate.
      revert Hv1; intros [= <-].
      apply eval_expr_same_value with (1 := H1) in Hv2. discriminate.
    - eauto.
  + inversion_clear H0. case (mem_dec_spec _ expr_reflect) => [Hin|_].
    - apply IHle1; eassumption.
    - apply eval_Econs with (1 := H2) in H.
      specialize IHle1 with (le2 := le2) (1 := H) (2 := H3) (3 := H1).
      destruct IHle1 as [le ->]. simpl. eauto.
Qed.

Lemma nodup_keep_first_cut_on_false:
  forall e f le1 le2 c,
    eval_exprlist e f le1 (map (fun _ => Vtrue) le1) ->
    eval_expr e f c Vfalse ->
    exists le,
      nodup_keep_first expr_beq (le1 ++ c :: le2) =
      nodup_keep_first expr_beq le1 ++ c :: le.
Proof.
  unfold nodup_keep_first. intros. eapply nodup_keep_first'_cut_on_false.
  exact (eval_Enil _ _). eassumption. eassumption.
Qed.

Lemma Int64_unsigned_from_Int_unsigned i:
  Int64.unsigned (Int64.repr (Int.unsigned i)) = Int.unsigned i.
Proof.
  pose proof Int64_Int_modulus. rewrite Int64.unsigned_repr.
  unfold Int64.max_unsigned. pose proof (Int.unsigned_range i); lia.
  reflexivity.
Qed.

#[local]
Hint Rewrite Int64_unsigned_from_Int_unsigned : nbtob.

Lemma constraints_verified_skip:
  forall ge e f s k constrs,
    eval_exprlist e f constrs (map (fun _ => Vtrue) constrs) ->
    star step_event ge
      (State e f
        (guard_stmt_with_constraints_list constrs s) k) Events.E0
      (State e f s k).
Proof.
  induction constrs; intro H. apply star_refl.
  inversion_clear H. simpl. eauto 10 using star_step, star_refl with semantics.
Qed.

Lemma constraints_not_verified_error:
  forall ge e f s k lc1 c lc2,
    eval_exprlist e f lc1 (map (fun _ => Vtrue) lc1) ->
    eval_expr e f c Vfalse ->
    exists k',
      plus step_event ge
        (State e f
          (guard_stmt_with_constraints_list (lc1 ++ c :: lc2) s) k) Events.E0
        (State e f Serror k').
Proof.
  induction lc1; simpl; intros * Hc1 Hc.
  + eexists. eapply plus_two. eauto with semantics. split=> //.
    change Serror with (if false then Sskip else Serror). apply step_assert.
    assumption. reflexivity.
  + inversion_clear Hc1. specialize (IHlc1 _ lc2 H0 Hc) as [k' Hstep].
    eexists. eapply plus_left'; eauto with semantics.
    eapply plus_left'; eauto with semantics.
    eapply plus_left'; eauto with semantics.
    reflexivity.
Qed.

Lemma exprlist_constraints_app f:
  forall l1 l2 constrs,
    exprlist_constraints f (l1 ++ l2) = OK constrs ->
    exists lc1 lc2, constrs = lc1 ++ lc2 /\
               exprlist_constraints f l1 = OK lc1 /\
               exprlist_constraints f l2 = OK lc2.
Proof.
  elim/list_ind => /=.
  - move=> ? lc2 H2. exists [], lc2. eauto.
  - move=> e l1 IH >.
    case Cl: exprlist_constraints => //=.
    case Ce: expr_constraints => [ce|] //= - [<-].
    case (IH _ _ Cl) => lc1 [lc2 [-> [-> ->]]] /=.
    rewrite app_assoc. eauto.
Qed.

Theorem transl_stmt_sem_preservation:
  forall p tp s s' ts t,
    transl_program p = OK tp ->
    match_states p s ts ->
    NB.step_event (genv_of_program p) s t s' ->
    exists ts',
      plus step_event (genv_of_program tp) ts t ts' /\ match_states p s' ts'.
Proof.
  intros. destruct s. destruct s.
  (*  Sskip  *)
  - destruct H1. inversion H0. rewrite_equalities.
    inversion H2; rewrite_equalities; simpl in TRFUNC, TRCONT.
    (*  Sskip, Kseq  *)
    + destruct (transl_stmt _ f s) eqn:Ts, (transl_cont _ (Some f) k) eqn:Tk; try discriminate.
      revert TRCONT; intros [= <-].
      eexists. split; [ apply plus_one; split | ]; eauto with nbtob semantics.
    + destruct (transl_cont _ (Some f) k) eqn:Tk; try discriminate.
      revert TRCONT; intros [= <-].
      eexists. split; [ apply plus_one; split | ]; eauto with nbtob semantics.
    (*  Sskip, Kreturnto  *)
    + eexists. split. apply plus_one. split. reflexivity.
      apply SemanticsBlocking.step_skip_returnto. eapply is_Kreturnto_preservation; eassumption.
      eauto with nbtob.
      apply match_states_Returnstate; try assumption.
  (*  Salloc   *)
  - destruct H1. inv H0. inversion H2; subst.
    (* Salloc, normal case *)
    + revert TRSTMT; simpl. rewrite H6 /=. unfold guard_stmt_with_expr.
      destruct (expr_constraints f sz) eqn:Hconstrs; try discriminate. intros [= <-].
      pose proof (eval_expr_Some_constraints_verified _ _ _ _ _ H11 Hconstrs).
      apply transl_exprlist_sem_preservation with (1 := TRFUNC) in H0.
      apply eval_exprlist_true_nodup_keep_first' with (nl := []) in H0.
      eexists. split. eapply plus_right. now apply constraints_verified_skip.
      all: eauto 10 with semantics nbtob.
    (* Salloc, size evaluation raises an error *)
    + revert TRSTMT; simpl. rewrite H6 /=. unfold guard_stmt_with_expr.
      destruct (expr_constraints f sz) eqn:Hconstrs; try discriminate. intros [= <-].
      destruct (eval_expr_None_constraints_not_verified _ _ _ _ H7 Hconstrs)
        as (l1 & c & l2 & -> & Hc1 & Hc).
      apply transl_exprlist_sem_preservation with (1 := TRFUNC) in Hc1.
      apply transl_expr_sem_preservation with (1 := TRFUNC) in Hc.
      destruct (nodup_keep_first_cut_on_false _ _ _ l2 _ Hc1 Hc) as [le ->].
      apply eval_exprlist_true_nodup_keep_first in Hc1.
      destruct constraints_not_verified_error
          with (ge := genv_of_program tp) (s := Salloc i) (k := tk)
               (lc2 := le) (1 := Hc1) (2 := Hc). eauto with nbtob.
    + revert TRSTMT; simpl. rewrite H6 /=. unfold guard_stmt_with_expr.
      destruct (expr_constraints f sz) eqn:Hconstrs; try discriminate. intros [= <-].
      pose proof (eval_expr_Some_constraints_verified _ _ _ _ _ H10 Hconstrs).
      apply transl_exprlist_sem_preservation with (1 := TRFUNC) in H0.
      apply eval_exprlist_true_nodup_keep_first' with (nl := []) in H0.
      eexists. split. eapply plus_right. now apply constraints_verified_skip.
      all: eauto 10 with semantics nbtob.
  (*  Sfree    *)
  - destruct H1. inv H0. inversion H2; subst. revert TRSTMT; intros [= <-].
    eexists; split. apply plus_one. split=> //.
    all: eauto 10 with semantics nbtob.
  (*  Sassign  *)
  - destruct H1. inv H0. inversion H2; subst.
    (*  Sassign, normal case  *)
    + rewrite/transl_stmt in TRSTMT.
      case Hs: (expr_constraints f (Eacc s)) TRSTMT => //.
      case He: (expr_constraints f e0) => //= - [<-].
      have:= step_assign_constraints_verified _ _ _ _ _ _ _ _ H2 Hs.
      have:= eval_expr_Some_constraints_verified e f _ _ _ H8 He.
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC).
      move=> + /(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC).
      move/eval_exprlist_app/[apply]. rewrite -map_app.
      move/eval_exprlist_true_nodup_keep_first.
      eexists. split. eapply plus_right. by apply: constraints_verified_skip.
      all: eauto 8 with semantics nbtob.
    (*  Sassign p exp, p is evaluated to None  *)
    + rewrite/transl_stmt in TRSTMT.
      case Hs: (expr_constraints f (Eacc s)) TRSTMT => //.
      case He: (expr_constraints f e0) => [ce|] //= - [<-].
      have: NB.eval_expr e f (Eacc s) None by (inv H7; eauto with semanticsnb).
      move/(eval_expr_None_constraints_not_verified _ _ _ _) => /(_ _ Hs).
      case=> l1 [c [l2 [-> []]]].
      have:= eval_expr_Some_constraints_verified _ _ _ _ _ H8 He.
      move/NB.eval_exprlist_app/[apply]. rewrite -map_app app_assoc.
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC) => Hc1.
      move/(transl_expr_sem_preservation _ _ _ _ _ _ TRFUNC) => Hc2.
      have [le ->] := nodup_keep_first_cut_on_false _ _ _ l2 _ Hc1 Hc2.
      move/eval_exprlist_true_nodup_keep_first: Hc1.
      move/constraints_not_verified_error.
      case/(_ (genv_of_program tp) (Sassign s e0) tk c le Hc2) =>> T.
      eauto with nbtob.
    (* Sassign p exp, exp is evaluated to None  *)
    + rewrite/transl_stmt in TRSTMT.
      case Hs: (expr_constraints f (Eacc s)) TRSTMT => [cs|] //.
      case He: (expr_constraints f e0) => //= - [<-].
      have:= eval_expr_None_constraints_not_verified _ _ _ _ H7 He.
      case=> l1 [c [l2 [-> []]]]. rewrite -app_assoc -app_comm_cons.
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC) => Hc1.
      move/(transl_expr_sem_preservation _ _ _ _ _ _ TRFUNC) => Hc2.
      have [le ->] := nodup_keep_first_cut_on_false _ _ _ (l2 ++ cs)  _ Hc1 Hc2.
      move/eval_exprlist_true_nodup_keep_first: Hc1.
      move/constraints_not_verified_error.
      case/(_ (genv_of_program tp) (Sassign s e0) tk c le Hc2) =>> T.
      eauto with nbtob.
  (*  Scall internal  *)
  - destruct H1. inversion H0. rewrite_equalities.
    inversion H2; rewrite_equalities; simpl in TRSTMT.
    (*  Scall internal, normal case  *)
    + rewrite H8 in TRSTMT. move: TRSTMT.
      case Cargs: exprlist_constraints => //=.
      case Ccall: call_constraints => //= - [<-].
      destruct_transl_program p; simpl in *. apply PTree.elements_correct in H8.
      have H := proj2 (build_env_correct_empty prog_defs prog_nodup _) H8.
      case (in_list_map_error _ _ _ _ Hfd H) => [y [+ Hy2]].
      case Htfd: transl_fundef => //=; intros [= <-].
      have Hlen: length args = length (fn_params f')
          by now rewrite (proj1 (fn_tenv_sig f')).
      have:= call_constraints_verified _ _ _ _ _ _ _ H9 Ccall Hlen H16 H17.
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC).
      have:= eval_path_list_Some_constraints_verified _ _ _ _ _ H9 Cargs.
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC).
      move/eval_exprlist_app/[apply]. rewrite -map_app.
      move/eval_exprlist_true_nodup_keep_first.
      pose proof Htfd. monadInv Htfd.
      eexists. split. eapply plus_right. now apply constraints_verified_skip.
      split. reflexivity. eapply step_call_Internal. apply PTree.elements_complete.
      apply (proj1 (build_env_correct_empty _ Hnodup _) Hy2).
      all: eauto 3 with semantics nbtob.
      * rewrite -(transl_function_same_penv _ _ _ EQ).
        rewrite -(transl_function_same_penv _ _ _ TRFUNC) //.
      * rewrite -(transl_function_same_tenv _ _ _ EQ).
        rewrite -(transl_function_same_tenv _ _ _ TRFUNC) //.
      * apply match_states_Callstate; try assumption.
        simpl. rewrite -(transl_function_same_penv _ _ _ EQ).
        by rewrite -> TRCONT, TRFUNC.
    (* Scall external, normal case  *)
    + rewrite H8 in TRSTMT. move: TRSTMT. rename ef into f'.
      case Cargs: exprlist_constraints => //=.
      case Ccall: call_constraints => //= - [<-].
      destruct_transl_program p; simpl in *. apply PTree.elements_correct in H8.
      have H := proj2 (build_env_correct_empty prog_defs prog_nodup _) H8.
      case (in_list_map_error _ _ _ _ Hfd H) => [y [+ Hy2]].
      case Htfd: transl_fundef => //=; intros [= <-].
      have Hlen: length args = length (ef_params f')
          by now rewrite (proj1 (ef_tenv_sig f')).
      have:= call_constraints_verified _ _ _ _ _ _ _ H9 Ccall Hlen H15 H16.
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC).
      have:= eval_path_list_Some_constraints_verified _ _ _ _ _ H9 Cargs.
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC).
      move/eval_exprlist_app/[apply]. rewrite -map_app.
      move/eval_exprlist_true_nodup_keep_first.
      revert Htfd; intros [= <-].
      eexists. split. eapply plus_right. now apply constraints_verified_skip.
      split. reflexivity. eapply step_call_External. apply PTree.elements_complete.
      apply (proj1 (build_env_correct_empty _ Hnodup _) Hy2).
      all: eauto 3 with semantics nbtob.
    (* Scall, error during evaluation of parameters  *)
    + rewrite H9 in TRSTMT. move: TRSTMT.
      case Cargs: exprlist_constraints => /=; [|by case fd].
      case (NB.eval_path_list_None_cut _ _ _ H10) => lp1 [x [lp2 [T [[>]]]]] H1 Hx.
      move: Cargs. rewrite T {T}. rewrite map_app /=.
      case/exprlist_constraints_app => lc1 [lc2 [-> [Hc1]]].
      rewrite/exprlist_constraints. fold (exprlist_constraints f).
      case Hc2: exprlist_constraints => [clp2|] //.
      case Cx: expr_constraints => // => - [<-].
      have:= eval_path_list_Some_constraints_verified _ _ _ _ _ H1 Hc1.
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC).
      have:= eval_path_None_constraints_not_verified _ _ _ _ Hx Cx.
      case=> l1 [c [l2 [-> []]]].
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC) => + /[swap].
      move/[swap]/eval_exprlist_app/[apply]. rewrite -map_app.
      move=> + /(transl_expr_sem_preservation _ _ _ _ _ _ TRFUNC) => E1 Ec.
      case Ccall: call_constraints => [cc|] //= - [<-].
      rewrite -3!app_assoc -app_comm_cons app_assoc.
      have [le ->] := nodup_keep_first_cut_on_false _ _ _ (l2 ++ clp2 ++ cc) _ E1 Ec.
      move/eval_exprlist_true_nodup_keep_first: E1.
      move/constraints_not_verified_error.
      case/(_ (genv_of_program tp) (Scall idvar idf (lp1 ++ x :: lp2)) tk c le Ec) =>> T.
      eauto with nbtob.
    (* Scall, error during evaluation of size expressions *)
    + rewrite H8 in TRSTMT. move: TRSTMT.
      case Cargs: exprlist_constraints => [cc|] //=.
      case Ccall: call_constraints => //= - [<-].
      have Hlen: length args = length (fd_params fd)
          by now rewrite (proj1 (fd_tenv_sig fd)).
      specialize call_constraints_not_verified_eval_Err
            with (1 := H16) (2 := H9) (3 := H10) (4 := H18)
                 (5 := Ccall) (6 := Hlen) (7 := H15).
      intros (lt1 & c & lt2 & -> & E1 & Ec). rewrite app_assoc.
      move/eval_path_list_Some_constraints_verified: Cargs => /(_ _ _ H9).
      move/NB.eval_exprlist_app => /(_ _ _ E1). rewrite -map_app.
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC) => E1'.
      move/(transl_expr_sem_preservation _ _ _ _ _ _ TRFUNC) in Ec.
      have [le ->] := nodup_keep_first_cut_on_false _ _ _ lt2 _ E1' Ec.
      move/eval_exprlist_true_nodup_keep_first: E1'.
      move/constraints_not_verified_error.
      case/(_ (genv_of_program tp) (Scall idvar idf args) tk c le Ec) =>> T.
      eauto with nbtob.
    + rewrite H8 in TRSTMT. move: TRSTMT.
      case Cargs: exprlist_constraints => [cc|] //=.
      case Ccall: call_constraints => //= - [<-].
      have Hlen: length args = length (fd_params fd)
          by now rewrite (proj1 (fd_tenv_sig fd)).
      have:= call_constraints_not_verified _ _ _ _ _ _ _ _ H9 H10 H18 Ccall Hlen H15.
      rewrite H16 /= => /(_ notF) [l1 [c [l2 [-> []]]]].
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC).
      have:= eval_path_list_Some_constraints_verified _ _ _ _ _ H9 Cargs.
      move/(transl_exprlist_sem_preservation _ _ _ _ _ _ TRFUNC).
      move/eval_exprlist_app/[apply]. rewrite -map_app app_assoc => E1.
      move/(transl_expr_sem_preservation _ _ _ _ _ _ TRFUNC) => Ec.
      have [le ->] := nodup_keep_first_cut_on_false _ _ _ l2 _ E1 Ec.
      move/eval_exprlist_true_nodup_keep_first: E1.
      move/constraints_not_verified_error.
      case/(_ (genv_of_program tp) (Scall idvar idf args) tk c le Ec).
      eauto with nbtob.
  (*  Sreturn  *)
  - destruct H1. inversion H0. rewrite_equalities.
    inversion H2; rewrite_equalities; try simpl in TRSTMT.
    (*  Sreturn None, normal case  *)
    + eexists; split; [eapply plus_one; split|]; eauto with semantics nbtob.
    (*  Sreturn (Some e), normal case  *)
    + destruct guard_stmt_with_expr eqn:Hg; try discriminate. revert TRSTMT; intros [= <-].
      unfold guard_stmt_with_expr in Hg.
      destruct (expr_constraints f exp) eqn:Hconstrs; try discriminate.
      revert Hg; intros [= <-].
      pose proof (eval_expr_Some_constraints_verified _ _ _ _ _ H7 Hconstrs).
      apply transl_exprlist_sem_preservation with (1 := TRFUNC) in H1.
      apply eval_exprlist_true_nodup_keep_first' with (nl := []) in H1.
      eexists. split. eapply plus_right. now apply constraints_verified_skip.
      all: eauto with semantics nbtob.
    (*  Sreturn exp, exp is evaluated to None  *)
    + destruct guard_stmt_with_expr eqn:Hg; try discriminate. revert TRSTMT; intros [= <-].
      unfold guard_stmt_with_expr in Hg.
      destruct (expr_constraints f exp) eqn:Hconstrs; try discriminate.
      revert Hg; intros [= <-].
      destruct (eval_expr_None_constraints_not_verified _ _ _ _ H7 Hconstrs)
        as (l1 & c & l2 & -> & Hc1 & Hc).
      apply transl_exprlist_sem_preservation with (1 := TRFUNC) in Hc1.
      apply transl_expr_sem_preservation with (1 := TRFUNC) in Hc.
      destruct (nodup_keep_first_cut_on_false _ _ _ l2 _ Hc1 Hc) as [le ->].
      apply eval_exprlist_true_nodup_keep_first in Hc1.
      destruct constraints_not_verified_error
          with (ge := genv_of_program tp) (s := Sreturn (Some exp)) (k := tk)
               (lc2 := le) (1 := Hc1) (2 := Hc). eauto with nbtob.
  (*  Sseq  *)
  - destruct H1. inversion H0. rewrite_equalities.
    inversion H2; rewrite_equalities; simpl in TRSTMT.
    destruct (transl_stmt _ f s0) eqn:Hs0, (transl_stmt _ f s3) eqn:Hs3; try discriminate.
    revert TRSTMT; intros [= <-].
    eexists. split. apply plus_one. split. reflexivity.
    apply SemanticsBlocking.step_seq. apply match_states_State; try assumption.
    simpl. now rewrite -> Hs3, TRCONT.
  (*  Sassert  *)
  - destruct H1. inversion H0. rewrite_equalities.
    inversion H2; rewrite_equalities; simpl in TRSTMT;
    unfold guard_stmt_with_expr in TRSTMT;
    destruct expr_constraints eqn:Hconstrs; try discriminate;
    revert TRSTMT; intros [= <-].
    (* Sassert, normal case  *)
    + pose proof (eval_expr_Some_constraints_verified _ _ _ _ _ H7 Hconstrs).
      apply transl_exprlist_sem_preservation with (1 := TRFUNC) in H1.
      apply eval_exprlist_true_nodup_keep_first in H1.
      eexists. split. eapply plus_right. now apply constraints_verified_skip.
      all: destruct b; eauto with semantics nbtob.
    (* Sassert, condition is evaluated to None  *)
    + destruct (eval_expr_None_constraints_not_verified _ _ _ _ H7 Hconstrs)
            as (l1 & c & l2 & -> & Hc1 & Hc).
      apply transl_exprlist_sem_preservation with (1 := TRFUNC) in Hc1.
      apply transl_expr_sem_preservation with (1 := TRFUNC) in Hc.
      destruct (nodup_keep_first_cut_on_false _ _ _ l2 _ Hc1 Hc) as [le ->].
      apply eval_exprlist_true_nodup_keep_first in Hc1.
      destruct constraints_not_verified_error
          with (ge := genv_of_program tp) (s := Sassert cond) (k := tk)
               (lc2 := le) (1 := Hc1) (2 := Hc). eauto with nbtob.
  (*  Sblock  *)
  - destruct H1. inversion H0. rewrite_equalities.
    inversion H2; rewrite_equalities; simpl in TRSTMT; clear H2.
    destruct (transl_stmt _ f s0) eqn:Hts0; try discriminate.
    revert TRSTMT; intros [= <-].
    eexists; split.
    apply plus_one; split. reflexivity. econstructor.
    apply match_states_State. eassumption. eassumption. simpl; now rewrite TRCONT.
  (*  Sifthenelse  *)
  - destruct H1. inversion H0. rewrite_equalities.
    inversion H2; rewrite_equalities; simpl in TRSTMT;
    destruct (transl_stmt _ f s0) eqn:Hts0; try discriminate;
    destruct (transl_stmt _ f s3) eqn:Hts3; try discriminate;
    unfold guard_stmt_with_expr in TRSTMT; simpl in TRSTMT;
    destruct expr_constraints eqn:Hconstrs; try discriminate;
    revert TRSTMT; intros [= <-].
    (* Sifthenelse, normal case  *)
    + pose proof (eval_expr_Some_constraints_verified _ _ _ _ _ H9 Hconstrs).
      apply transl_exprlist_sem_preservation with (1 := TRFUNC) in H1.
      apply eval_exprlist_true_nodup_keep_first in H1.
      eexists. split. eapply plus_right. now apply constraints_verified_skip.
      all: destruct b; eauto with semantics nbtob.
    (* Sassert, condition is evaluated to None  *)
    + destruct (eval_expr_None_constraints_not_verified _ _ _ _ H9 Hconstrs)
            as (l1 & c & l2 & -> & Hc1 & Hc).
      apply transl_exprlist_sem_preservation with (1 := TRFUNC) in Hc1.
      apply transl_expr_sem_preservation with (1 := TRFUNC) in Hc.
      destruct (nodup_keep_first_cut_on_false _ _ _ l2 _ Hc1 Hc) as [le ->].
      apply eval_exprlist_true_nodup_keep_first in Hc1.
      destruct constraints_not_verified_error
          with (ge := genv_of_program tp) (s := Sifthenelse cond s s1) (k := tk)
               (lc2 := le) (1 := Hc1) (2 := Hc). eauto with nbtob.
  (*  Sloop  *)
  - destruct H1. inversion H0. rewrite_equalities.
    inversion H2; rewrite_equalities; simpl in TRSTMT.
    destruct (transl_stmt _ f s0) eqn:Hs0; try discriminate.
    revert TRSTMT; intros [= <-].
    eexists. split. apply plus_one. split. reflexivity.
    apply SemanticsBlocking.step_loop. apply match_states_State; try eassumption.
    simpl. now rewrite -> Hs0, TRCONT.
  (*  Sexit  *)
  - destruct H1. inversion H0. rewrite_equalities.
    inversion H2; rewrite_equalities; simpl in TRCONT; clear H2.
    destruct (transl_stmt _ f s) eqn:Hs; try discriminate.
    all: destruct (transl_cont _ _ k) eqn:Hk; try discriminate.
    all: revert TRCONT; intros [= <-].
    all: eexists; split; [apply plus_one; split; [reflexivity|]|].
    1: apply step_exit_block_seq.
    2: apply step_exit_block_skip.
    3: apply step_exit_block_stop.
    all: now econstructor.
  (*  Serror  *)
  - destruct H1. inversion H0; rewrite_equalities;
    inversion H2; rewrite_equalities; eauto using plus_one with semantics.
  (*  Callstate  *)
  - destruct H1. inversion H0; rewrite_equalities;
        inversion H2; rewrite_equalities; try discriminate.
    simpl in TRFD. destruct transl_function eqn:TRF; try discriminate.
    revert TRFD; intros [= <-]. pose proof TRF.
    unfold transl_function in TRF. destruct transl_stmt eqn:TRBODY; try discriminate.
    revert TRF; intros [= <-].
    eexists. split. apply plus_one. split. all: eauto with nbtob semantics.
  (*  Returnstate  *)
  - destruct H1. inversion H0; rewrite_equalities.
    inversion H2; rewrite_equalities; simpl in TRCONT.
    destruct transl_stmt in TRCONT; try discriminate.
    all: destruct transl_cont eqn:TRK in TRCONT; try discriminate.
    3: destruct transl_function eqn:TRF in TRCONT; try discriminate.
    all: revert TRCONT; intros [= <-].
    eexists; split; [apply plus_one; split=> //|]; econstructor; eassumption.
    eexists; split; [apply plus_one; split=> //|]; econstructor; eassumption.
    eexists. split. eapply plus_one. split. reflexivity. econstructor => //.
    + rewrite -(transl_function_same_tenv _ _ _ TRF).
      by rewrite -(transl_function_same_tenv _ _ _ TRFUNC).
    + eassumption.
    + eauto with nbtob.
Qed.

Lemma transl_fundefs_genv_public_symbol_add_globals:
  forall p ge ge1 ge2 l id,
    transl_fundefs ge (prog_defs p) = OK l ->
    Genv.public_symbol ge1 id = Genv.public_symbol ge2 id ->
    Genv.genv_public ge1 = Genv.genv_public ge2 ->
    Genv.public_symbol
      (Genv.add_globals ge1
         (SemanticsBlocking.to_AST_globdef l)) id =
    Genv.public_symbol
      (Genv.add_globals ge2
         (to_AST_globdef (prog_defs p))) id.
Proof.
  intro p. induction (prog_defs p); simpl; intros.
  + revert H; intros [= <-]. assumption.
  + destruct a. unfold transl_fundefs in H. simpl in H.
    destruct transl_fundef, list_map_error eqn:Hfundefs in H; try discriminate.
    revert H; intros [= <-].
    apply IHl with (1 := Hfundefs). simpl.
    unfold Genv.add_global. simpl.
    unfold Genv.public_symbol. unfold Genv.find_symbol. simpl.
    unfold Genv.public_symbol in H0. unfold Genv.find_symbol in H0. simpl.
    repeat rewrite PTree.gsspec. destruct (Coqlib.peq id i).
    rewrite H1. all: easy.
Qed.

Theorem transl_program_correct (p: program):
  forall tp,
    transl_program p = OK tp ->
    forward_simulation (SemanticsNonBlocking.semantics p) (SemanticsBlocking.semantics tp).
Proof.
  intros. pose proof H. destruct_transl_program p. rename H into Hp.
  eapply forward_simulation_plus; simpl; intros.
  + pose proof (transl_fundefs_genv_public_symbol_add_globals _ _
              (Genv.empty_genv _ _ (map fst l))
              (Genv.empty_genv _ _ (map fst prog_defs)) l id Hfd).
    simpl in H. simpl in Hmapfst. apply H; rewrite (Hmapfst _ eq_refl); easy.
  + inversion H. rewrite_equalities.
    simpl in H0. pose proof H0. apply PTree.elements_correct in H0.
    apply (proj2 (build_env_correct_empty prog_defs prog_nodup _)) in H0.
    destruct (in_list_map_error _ _ _ _ Hfd H0) as [(prog_main', fd') [H' Hfd']].
    destruct transl_fundef eqn:Htfd; try discriminate. revert H'; intros [= <-].
    simpl in Htfd. destruct transl_function eqn:Htf; try discriminate.
    revert H3 Htfd; intros [= ->] [= <-].
    eexists. split. eapply SemanticsBlocking.initial_state_intro.
    - apply PTree.elements_complete. unfold genv_of_program.
      apply (build_env_correct_empty l); eassumption.
    - unfold transl_function in Htf. destruct transl_stmt; try discriminate.
      revert Htf; intros [= <-]. assumption.
    - apply match_states_Callstate; try easy. simpl. now rewrite -> Htf.
  + inversion H0. rewrite_equalities. inversion_clear H.
    revert TRCONT; intros [= <-]. apply SemanticsBlocking.final_state_intro.
  + eapply transl_stmt_sem_preservation; eassumption.
Qed.
