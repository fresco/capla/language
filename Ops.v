Require Import ssreflect ssrbool.
Require Import BinNums BinInt.
Require Import Coq.Bool.Bool.

Require Import Integers Floats.

Require Import Types BValues.

Inductive unary_operation : Type :=
  | Onotbool : unary_operation
  | Onotint : unary_operation
  | Oneg : unary_operation
  | Oabsfloat : unary_operation.

Inductive operation_kind : Type :=
  | OInt
  | OInt64
  | OFloat32
  | OFloat64.

Inductive binarith_operation : Type :=
  | Oadd
  | Osub
  | Omul
  | Odivs
  | Odivu
  | Omods
  | Omodu
  | Oand
  | Oor
  | Oxor
  | Oshl
  | Oshr
  | Oshru.

Inductive cmp_operation : Type :=
  | Oeq
  | One
  | Olt
  | Ogt
  | Ole
  | Oge.

Inductive cmpu_operation : Type :=
  | Oltu
  | Ogtu
  | Oleu
  | Ogeu.

Scheme Equality for unary_operation.
Scheme Equality for binarith_operation.
Scheme Equality for cmp_operation.
Scheme Equality for cmpu_operation.
Scheme Equality for operation_kind.

Lemma unary_operation_reflect:
  forall o1 o2, reflect (o1 = o2) (unary_operation_beq o1 o2).
Proof. case o1, o2; now (apply ReflectT || apply ReflectF). Qed.

Lemma operation_kind_reflect:
  forall o1 o2, reflect (o1 = o2) (operation_kind_beq o1 o2).
Proof. case o1, o2; now (apply ReflectT || apply ReflectF). Qed.

Lemma binarith_operation_reflect:
  forall o1 o2, reflect (o1 = o2) (binarith_operation_beq o1 o2).
Proof. case o1, o2; now (apply ReflectT || apply ReflectF). Qed.

Lemma cmp_operation_reflect:
  forall o1 o2, reflect (o1 = o2) (cmp_operation_beq o1 o2).
Proof. case o1, o2; now (apply ReflectT || apply ReflectF). Qed.

Lemma cmpu_operation_reflect:
  forall o1 o2, reflect (o1 = o2) (cmpu_operation_beq o1 o2).
Proof. case o1, o2; now (apply ReflectT || apply ReflectF). Qed.

Definition sem_notbool (v: value) : option value :=
  match v with
  | Vbool b => Some (Vbool (negb b))
  | _ => None
  end.

Definition sem_notint (k: operation_kind) (v: value) : option value :=
  match k, v with
  | OInt,   Vint   n => Some (Vint   (Int.not n))
  | OInt64, Vint64 n => Some (Vint64 (Int64.not n))
  | _, _ => None
  end.

Definition sem_neg (k: operation_kind) (v: value) : option value :=
  match k, v with
  | OInt,     Vint     n => Some (Vint     (Int.neg n))
  | OInt64,   Vint64   n => Some (Vint64   (Int64.neg n))
  | OFloat32, Vfloat32 f => Some (Vfloat32 (Float32.neg f))
  | OFloat64, Vfloat64 f => Some (Vfloat64 (Float.neg f))
  | _, _ => None
  end.

Definition sem_absfloat (k: operation_kind) (v: value) : option value :=
  match k, v with
  | OFloat32, Vfloat32 f => Some (Vfloat32 (Float32.abs f))
  | OFloat64, Vfloat64 f => Some (Vfloat64 (Float.abs f))
  | _, _ => None
  end.

Definition sem_unop (op: unary_operation) (k: operation_kind) (v: value) :=
  match op with
  | Onotbool => sem_notbool v
  | Onotint => sem_notint k v
  | Oneg => sem_neg k v
  | Oabsfloat => sem_absfloat k v
  end.

Definition sem_binarith
           (sem_int32: int -> int -> option value)
           (sem_int64: int64 -> int64 -> option value)
           (sem_float32: Floats.float32 -> Floats.float32 -> option value)
           (sem_float64: Floats.float -> Floats.float -> option value)
           (sem_bool: bool -> bool -> option value)
           (k: operation_kind)
           (v1: value) (v2: value) : option value :=
  match k, v1, v2 with
  | OInt, Vint n1, Vint n2 => sem_int32 n1 n2
  | OInt, Vbool b1, Vbool b2 => sem_bool b1 b2
  | OInt64, Vint64 n1, Vint64 n2 => sem_int64 n1 n2
  | OFloat32, Vfloat32 f1, Vfloat32 f2 => sem_float32 f1 f2
  | OFloat64, Vfloat64 f1, Vfloat64 f2 => sem_float64 f1 f2
  | _, _, _ => None
  end.

Definition opf {A B C: Type} (f: A -> B -> C) : A -> B -> option C :=
  fun x y => Some (f x y).

Definition sem_cmp (c: Integers.comparison) (k: operation_kind)
                   (v1: value) (v2: value) : option value :=
  match k, v1, v2 with
  | OInt,     Vint     n1, Vint     n2 => Some (Vbool (Int.cmp c n1 n2))
  | OInt64,   Vint64   n1, Vint64   n2 => Some (Vbool (Int64.cmp c n1 n2))
  | OFloat32, Vfloat32 f1, Vfloat32 f2 => Some (Vbool (Float32.cmp c f1 f2))
  | OFloat64, Vfloat64 f1, Vfloat64 f2 => Some (Vbool (Float.cmp c f1 f2))
  | _, _, _ => None
  end.

Definition sem_cmpu (c: Integers.comparison) (k: operation_kind)
                    (v1: value) (v2: value) : option value :=
  match k, v1, v2 with
  | OInt,   Vint   n1, Vint   n2 => Some (Vbool (Int.cmpu c n1 n2))
  | OInt64, Vint64 n1, Vint64 n2 => Some (Vbool (Int64.cmpu c n1 n2))
  | _, _, _ => None
  end.


Definition opb (f: bool -> bool -> bool) : bool -> bool -> option value :=
  fun x y => Some (Vbool (f x y)).

Definition opi (f: int -> int -> int) : int -> int -> option value :=
  fun x y => Some (Vint (f x y)).

Definition opi64 (f: int64 -> int64 -> int64) : int64 -> int64 -> option value :=
  fun x y => Some (Vint64 (f x y)).

Definition opf32 (f: float32 -> float32 -> float32) : float32 -> float32 -> option value :=
  fun x y => Some (Vfloat32 (f x y)).

Definition opf64 (f: float -> float -> float) : float -> float -> option value :=
  fun x y => Some (Vfloat64 (f x y)).

Definition divu (n1 n2: int) : option value :=
  if Int.eq n2 Int.zero then None
  else Some (Vint (Int.divu n1 n2)).

Definition divs (n1 n2: int) : option value :=
  if Int.eq n2 Int.zero
     || Int.eq n1 (Int.repr Int.min_signed) && Int.eq n2 Int.mone then None
  else Some (Vint (Int.divs n1 n2)).

Definition divu64 (n1 n2: int64) : option value :=
  if Int64.eq n2 Int64.zero then None
  else Some (Vint64 (Int64.divu n1 n2)).

Definition divs64 (n1 n2: int64) : option value :=
  if Int64.eq n2 Int64.zero
     || Int64.eq n1 (Int64.repr Int64.min_signed) && Int64.eq n2 Int64.mone then None
  else Some (Vint64 (Int64.divs n1 n2)).

Definition modu (n1 n2: int) : option value :=
  if Int.eq n2 Int.zero then None
  else Some (Vint (Int.modu n1 n2)).

Definition mods (n1 n2: int) : option value :=
  if Int.eq n2 Int.zero
     || Int.eq n1 (Int.repr Int.min_signed) && Int.eq n2 Int.mone then None
  else Some (Vint (Int.mods n1 n2)).

Definition modu64 (n1 n2: int64) : option value :=
  if Int64.eq n2 Int64.zero then None
  else Some (Vint64 (Int64.modu n1 n2)).

Definition mods64 (n1 n2: int64) : option value :=
  if Int64.eq n2 Int64.zero
     || Int64.eq n1 (Int64.repr Int64.min_signed) && Int64.eq n2 Int64.mone then None
  else Some (Vint64 (Int64.mods n1 n2)).

Definition sem_shift (sem_int32: int -> int -> option value)
                     (sem_int64: int64 -> int -> option value)
                     (k: operation_kind) (v1 v2: value) :=
  match k, v1, v2 with
  | OInt,   Vint   n1, Vint n2 => sem_int32 n1 n2
  | OInt64, Vint64 n1, Vint n2 => sem_int64 n1 n2
  | _, _, _ => None
  end.

Definition shl (n1 n2: int) : option value :=
  Some (Vint (Int.shl n1 (Int.modu n2 Int.iwordsize))).

Definition shl64 (n1: int64) (n2: int) : option value :=
  Some (Vint64 (Int64.shl' n1 (Int.modu n2 Int64.iwordsize'))).

Definition shr (n1 n2: int) : option value :=
  Some (Vint (Int.shr n1 (Int.modu n2 Int.iwordsize))).

Definition shru (n1 n2: int) : option value :=
  Some (Vint (Int.shru n1 (Int.modu n2 Int.iwordsize))).

Definition shr64 (n1: int64) (n2: int) : option value :=
  Some (Vint64 (Int64.shr' n1 (Int.modu n2 Int64.iwordsize'))).

Definition shru64 (n1: int64) (n2: int) : option value :=
  Some (Vint64 (Int64.shru' n1 (Int.modu n2 Int64.iwordsize'))).

Definition sem_None {A B: Type} (_ _: A) : option B := None.

Definition sem_binarith_operation (op: binarith_operation)
           (k: operation_kind) (v1 v2: value) : option value :=
  match op with
  | Oadd =>
    sem_binarith
      (opi Int.add) (opi64 Int64.add) (opf32 Float32.add) (opf64 Float.add) sem_None k v1 v2
  | Osub =>
    sem_binarith
      (opi Int.sub) (opi64 Int64.sub) (opf32 Float32.sub) (opf64 Float.sub) sem_None k v1 v2
  | Omul =>
    sem_binarith
      (opi Int.mul) (opi64 Int64.mul) (opf32 Float32.mul) (opf64 Float.mul) sem_None k v1 v2
  | Odivu =>
    sem_binarith
      divu divu64 (opf32 Float32.div) (opf64 Float.div) sem_None k v1 v2
  | Odivs =>
    sem_binarith
      divs divs64 (opf32 Float32.div) (opf64 Float.div) sem_None k v1 v2
  | Omodu =>
    sem_binarith
      modu modu64 sem_None sem_None sem_None k v1 v2
  | Omods =>
    sem_binarith
      mods mods64 sem_None sem_None sem_None k v1 v2
  | Oand =>
    sem_binarith
      (opi Int.and) (opi64 Int64.and) sem_None sem_None
      (fun b1 b2 => Some (Vbool (andb b1 b2))) k v1 v2
  | Oor =>
    sem_binarith
      (opi Int.or) (opi64 Int64.or) sem_None sem_None
      (fun b1 b2 => Some (Vbool (orb b1 b2))) k v1 v2
  | Oxor =>
    sem_binarith
      (opi Int.xor) (opi64 Int64.xor) sem_None sem_None
      (fun b1 b2 => Some (Vbool (xorb b1 b2))) k v1 v2
  | Oshl => sem_shift shl shl64 k v1 v2
  | Oshr => sem_shift shr shr64 k v1 v2
  | Oshru => sem_shift shru shru64 k v1 v2
  end.

Definition sem_cmp_operation (op: cmp_operation) (k: operation_kind)
           (v1 v2: value) : option value :=
  match op with
  | Oeq => sem_cmp Integers.Ceq k v1 v2
  | One => sem_cmp Integers.Cne k v1 v2
  | Olt => sem_cmp Integers.Clt k v1 v2
  | Ogt => sem_cmp Integers.Cgt k v1 v2
  | Ole => sem_cmp Integers.Cle k v1 v2
  | Oge => sem_cmp Integers.Cge k v1 v2
  end.

Definition sem_cmpu_operation (op: cmpu_operation) (k: operation_kind)
           (v1 v2: value) : option value :=
match op with
| Oltu => sem_cmpu Integers.Clt k v1 v2
| Ogtu => sem_cmpu Integers.Cgt k v1 v2
| Oleu => sem_cmpu Integers.Cle k v1 v2
| Ogeu => sem_cmpu Integers.Cge k v1 v2
end.

Definition cast_int_int (sz: intsize) (sig: signedness) (i: int) : int :=
  match sz, sig with
  | I8,  Signed   => Int.sign_ext 8 i
  | I8,  Unsigned => Int.zero_ext 8 i
  | I16, Signed   => Int.sign_ext 16 i
  | I16, Unsigned => Int.zero_ext 16 i
  | I32, _        => i
  end.

Definition sem_cast (v: value) (tv: typ) (t: typ) : option value :=
  match v, tv, t with
  | Vint n, Tint _ _,        Tbool          => Some (Vbool    (negb (Int.eq n Int.zero)))
  | Vint n, Tint _ _,        Tint   sz  sig => Some (Vint     (cast_int_int sz sig n))
  | Vint n, Tint _ Signed,   Tint64     sig => Some (Vint64   (Int64.repr (Int.signed n)))
  | Vint n, Tint _ Unsigned, Tint64     sig => Some (Vint64   (Int64.repr (Int.unsigned n)))
  | Vint n, Tint _ Signed,   Tfloat F32     => Some (Vfloat32 (Float32.of_int n))
  | Vint n, Tint _ Unsigned, Tfloat F32     => Some (Vfloat32 (Float32.of_intu n))
  | Vint n, Tint _ Signed,   Tfloat F64     => Some (Vfloat64 (Float.of_int n))
  | Vint n, Tint _ Unsigned, Tfloat F64     => Some (Vfloat64 (Float.of_intu n))
  | Vint64 n, Tint64 _, Tbool         => Some (Vbool  (negb (Int64.eq n Int64.zero)))
  | Vint64 n, Tint64 _, Tint64    sig => Some (Vint64 n)
  | Vint64 n, Tint64 _, Tint   sz sig =>
    Some (Vint (cast_int_int sz sig (Int64.loword n)))
  | Vint64 n, Tint64 Signed,   Tfloat F32 => Some (Vfloat32 (Float32.of_long n))
  | Vint64 n, Tint64 Unsigned, Tfloat F32 => Some (Vfloat32 (Float32.of_longu n))
  | Vint64 n, Tint64 Signed,   Tfloat F64 => Some (Vfloat64 (Float.of_long n))
  | Vint64 n, Tint64 Unsigned, Tfloat F64 => Some (Vfloat64 (Float.of_longu n))
  | Vfloat32 f, Tfloat F32, Tint sz Signed =>
    match Float32.to_int f with
    | Some i => Some (Vint (cast_int_int sz Signed i))
    | None => None
    end
  | Vfloat32 f, Tfloat F32, Tint sz Unsigned =>
    match Float32.to_intu f with
    | Some i => Some (Vint (cast_int_int sz Unsigned i))
    | None => None
    end
  | Vfloat32 f, Tfloat F32, Tint64 Signed =>
    match Float32.to_long f with
    | Some i => Some (Vint64 i)
    | None => None
    end
  | Vfloat32 f, Tfloat F32, Tint64 Unsigned =>
    match Float32.to_longu f with
    | Some i => Some (Vint64 i)
    | None => None
    end
  | Vfloat32 f, Tfloat F32, Tfloat F32 => Some v
  | Vfloat32 f, Tfloat F32, Tfloat F64 => Some (Vfloat64 (Float.of_single f))
  | Vfloat64 f, Tfloat F64, Tint sz Signed =>
    match Float.to_int f with
    | Some i => Some (Vint (cast_int_int sz Signed i))
    | None => None
    end
  | Vfloat64 f, Tfloat F64, Tint sz Unigned =>
    match Float.to_intu f with
    | Some i => Some (Vint (cast_int_int sz Unsigned i))
    | None => None
    end
  | Vfloat64 f, Tfloat F64, Tint64 Signed =>
    match Float.to_long f with
    | Some i => Some (Vint64 i)
    | None => None
    end
  | Vfloat64 f, Tfloat F64, Tint64 Unsigned =>
    match Float.to_longu f with
    | Some i => Some (Vint64 i)
    | None => None
    end
  | Vfloat64 f, Tfloat F64, Tfloat F32 => Some (Vfloat32 (Float.to_single f))
  | Vfloat64 f, Tfloat F64, Tfloat F64 => Some v
  | Vundef,   Tvoid, Tvoid => Some Vundef
  | Vbool  b, Tbool, Tbool => Some (Vbool b)
  | Vbool  b, Tbool, Tint sz sg => Some (Vint   (if b then Int.one   else Int.zero))
  | Vbool  b, Tbool, Tint64  sg => Some (Vint64 (if b then Int64.one else Int64.zero))
  | Varr lv, Tarr t1, Tarr t2 =>
      if typ_beq t1 t2 then Some (Varr lv)
      else None
  | _, _, _ => None
  end.

Section CastOpsAllowed.

  Lemma sem_cast_same_type:
    forall v t,
      well_typed_value t v ->
      exists v', sem_cast v t t = Some v'.
  Proof.
    destruct v ; simpl; inversion 1; eauto.
    destruct sig; eauto. destruct sig; eauto.
    rewrite -> typ_dec_lb by reflexivity; simpl; eauto.
  Qed.
  
  Definition cast_allowed t1 t2 :=
    if typ_beq t1 t2 then true
    else
      match t1, t2 with
      | Tbool, Tint _ _ | Tbool, Tint64 _
      | Tint _ _, Tbool | Tint64 _, Tbool
      | Tint _ _, Tint _ _ | Tint _ _, Tint64 _ | Tint _ _, Tfloat _
      | Tint64 _, Tint _ _ | Tint64 _, Tint64 _ | Tint64 _, Tfloat _
      | Tfloat _, Tint _ _ | Tfloat _, Tint64 _ | Tfloat _, Tfloat _ => true
      | _, _ => false
      end.
  
  Definition values_match_operation_kind k v1 v2 :=
    match k, v1, v2 with
    | OInt, Vint _, Vint _
    | OInt64, Vint64 _, Vint64 _
    | OFloat32, Vfloat32 _, Vfloat32 _
    | OFloat64, Vfloat64 _, Vfloat64 _ => true
    | _, _, _ => false
    end.
  
  Definition binarith_operation_allowed op k v1 v2 :=
    match op, k with
    | Omods, OInt | Omods, OInt64 | Omodu, OInt | Omodu, OInt64
    | Oadd, _ | Osub, _ | Omul, _ | Odivs, _ | Odivu, _ =>
      values_match_operation_kind k v1 v2
    | Oand, OInt | Oor, OInt | Oxor, OInt =>
      match v1, v2 with
      | Vbool _, Vbool _ | Vint _, Vint _ => true
      | _, _ => false
      end
    | Oand, OInt64 | Oor, OInt64 | Oxor, OInt64 =>
      match v1, v2 with
      | Vint64 _, Vint64 _ => true
      | _, _ => false
      end
    | Oshl, OInt | Oshr, OInt | Oshru, OInt =>
      values_match_operation_kind k v1 v2
    | Oshl, OInt64 | Oshr, OInt64 | Oshru, OInt64 =>
      match v1, v2 with
      | Vint64 _, Vint _ => true
      | _, _ => false
      end
    | _, _ => false
    end.
  
  Lemma sem_binarith_op_allowed:
    forall op k v1 v2,
      op = Oadd \/ op = Osub \/ op = Omul \/
      op = Oand \/ op = Oor \/ op = Oxor \/
      op = Oshl \/ op = Oshr \/ op = Oshru ->
      binarith_operation_allowed op k v1 v2 ->
      exists v, sem_binarith_operation op k v1 v2 = Some v.
  Proof.
    move=> [] // [] // [|?|?|?|?|?|] // [] //= > + _.
    all: rewrite/opi/opi64/opf32/opf64; eauto.
    all: repeat case=> //.
    all: rewrite/shl/shl64/shr/shr64/shru/shru64; eauto.
  Qed.
  
  Lemma sem_binarith_op_float:
    forall op k v1 v2,
      k = OFloat32 \/ k = OFloat64 ->
      binarith_operation_allowed op k v1 v2 ->
      exists v, sem_binarith_operation op k v1 v2 = Some v.
  Proof.
    move=> [] // [] // [|?|?|?|?|?|] // [] //= > + _.
    all: case=> // _; rewrite/opf32/opf64; eauto.
  Qed.
  
End CastOpsAllowed.