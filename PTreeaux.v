Require Import ssreflect ssrbool ssrfun.
Require Import BinNums BinPos List PArith Lia.

Require Import Maps Errors.

Require BUtils.

Local Open Scope error_monad_scope.

Import ListNotations.

Lemma PTree_keys_set {A: Type} (k: positive) (v: A) (x: positive) (m: PTree.t A):
  In x (map fst (PTree.elements m)) ->
  In x (map fst (PTree.elements (PTree.set k v m))).
Proof.
  intros. apply BUtils.in_map_fst_pair in H. destruct H as [y Hy].
  apply PTree.elements_complete in Hy.
  destruct (Pos.eq_dec x k).
  + apply BUtils.in_map_pair_fst with (y := v).
    revert e; intros [= <-].
    apply PTree.elements_correct.
    rewrite PTree.gsspec. apply Coqlib.peq_true.
  + apply BUtils.in_map_pair_fst with (y := y).
    apply PTree.elements_correct.
    rewrite PTree.gsspec. rewrite Hy. apply Coqlib.peq_false with (1 := n).
Qed.

Lemma PTree_map'_set0 {A B: Type} (i j: positive) (x: A) (f: positive -> A -> B):
  PTree.map' f (PTree.set0 i x) j
  = PTree.set0 i (f (PTree.prev (PTree.prev_append i j)) x).
Proof. revert j. induction i; simpl; intros; try (rewrite IHi); eauto. Qed.

Lemma PTree_map'_set' {A B: Type} (i j: positive) (x: A) (m: PTree.tree' A) (f: positive -> A -> B):
  PTree.map' f (PTree.set' i x m) j
  = PTree.set' i (f (PTree.prev (PTree.prev_append i j)) x) (PTree.map' f m j).
Proof.
  revert j m. induction i; simpl; intros; destruct m; simpl; try eauto;
    try rewrite IHi; try rewrite PTree_map'_set0; reflexivity.
Qed.

Lemma PTree_map_set {A B: Type} (i: positive) (x: A) (m: PTree.t A) (f: positive -> A -> B):
  PTree.map f (PTree.set i x m) = PTree.set i (f i x) (PTree.map f m).
Proof.
  intros. destruct m as [|m]; simpl.
  + rewrite PTree_map'_set0. unfold PTree.set.
    rewrite PTree.prev_append_prev. reflexivity.
  + rewrite PTree_map'_set'. rewrite PTree.prev_append_prev. reflexivity.
Qed.

Fixpoint map_error' {A B} (f: positive -> A -> res B) (m: PTree.tree' A) (i: positive)
  {struct m} : (res (PTree.tree' B)) :=
    match m with
    | PTree.Node001 r =>
        do m' <- map_error' f r (xI i); OK (PTree.Node001 m')
    | PTree.Node010 x =>
        do x' <- f (PTree.prev i) x;
        OK (PTree.Node010 x')
    | PTree.Node011 x r =>
        do m' <- map_error' f r (xI i);
        do x' <- f (PTree.prev i) x;
        OK (PTree.Node011 x' m')
    | PTree.Node100 l =>
        do m' <- map_error' f l (xO i); OK (PTree.Node100 m')
    | PTree.Node101 l r =>
        do m1 <- map_error' f l (xO i);
        do m2 <- map_error' f r (xI i);
        OK (PTree.Node101 m1 m2)
    | PTree.Node110 l x =>
        do m' <- map_error' f l (xO i);
        do x' <- f (PTree.prev i) x;
        OK (PTree.Node110 m' x')
    | PTree.Node111 l x r =>
        do m1 <- map_error' f l (xO i);
        do m2 <- map_error' f r (xI i);
        do x' <- f (PTree.prev i) x;
        OK (PTree.Node111 m1 x' m2)
    end.

Definition map_error {A B} (f: positive -> A -> res B) (m: PTree.t A) : res (PTree.t B) :=
    match m with
    | PTree.Empty => OK PTree.Empty
    | PTree.Nodes m =>
        do m' <- map_error' f m xH; OK (PTree.Nodes m')
    end.

Lemma map_error'_OK_f:
  forall {A B} (f: positive -> A -> res B) (i j : positive)
         (m: PTree.tree' A) (m': PTree.tree' B) (x: A),
    map_error' f m j = OK m' ->
    PTree.get' i m = Some x ->
    exists y, f (PTree.prev (PTree.prev_append i j)) x = OK y.
Proof.
  induction i; intros; destruct m; simpl;
    ((simpl in H; destruct (map_error') eqn:H2, (map_error') eqn:H3 in H)
     || (simpl in H; destruct (map_error') eqn:H2 in H)
         || idtac);
    (discriminate
     || (simpl in H; destruct f eqn:Hf in H; try discriminate)
     || idtac);
    revert H; intros [= <-];
    eauto || revert H0; intros [= <-]; exists b; assumption.
Qed.

Lemma get_map_error'_Some:
  forall {A B} (f: positive -> A -> res B) (i j : positive)
         (m: PTree.tree' A) (m': PTree.tree' B) (x: A) (y: B),
    map_error' f m j = OK m' ->
    PTree.get' i m = Some x ->
    f (PTree.prev (PTree.prev_append i j)) x = OK y ->
    PTree.get' i m' = Some y.
Proof.
  induction i; intros; destruct m; simpl;
    ((simpl in H; destruct (map_error') eqn:H2, (map_error') eqn:H3 in H)
     || (simpl in H; destruct (map_error') eqn:H2 in H)
         || idtac);
    (discriminate
     || (simpl in H; destruct f eqn:Hf in H; try discriminate)
     || idtac);
    revert H; intros [= <-];
    eauto || revert H0 H1 Hf; intros [= ->] [= ->] [= ->]; eauto.
Qed.

Lemma get_map_error'_None:
  forall {A B} (f: positive -> A -> res B) (i j : positive)
         (m: PTree.tree' A) (m': PTree.tree' B),
    map_error' f m j = OK m' ->
    PTree.get' i m = None ->
    PTree.get' i m' = None.
Proof.
  induction i; intros; destruct m; simpl;
  ((simpl in H; destruct (map_error') eqn:H2, (map_error') eqn:H3 in H)
     || (simpl in H; destruct (map_error') eqn:H2 in H)
         || idtac);
    (discriminate
     || (simpl in H; destruct f eqn:Hf in H; try discriminate)
     || idtac);
    revert H; intros [= <-]; eauto.
Qed.

Theorem map_error_OK_f:
  forall {A B} (f: positive -> A -> res B) (i : positive)
         (m: PTree.t A) (m': PTree.t B) (x: A),
    map_error f m = OK m' ->
    PTree.get i m = Some x ->
    exists y, f i x = OK y.
Proof.
  intros; destruct m as [|m]; simpl. revert H; intros [= <-]; discriminate.
  simpl in H. destruct map_error' eqn:H2; try discriminate.
  revert H; intros [= <-]. unfold PTree.get.
  destruct (map_error'_OK_f f i 1 m t x H2 H0) as [y Hy].
  exists y. rewrite PTree.prev_append_prev in Hy. assumption.
Qed.

Theorem get_map_error_Some:
  forall {A B} (f: positive -> A -> res B) (i: positive) (m: PTree.t A) (m': PTree.t B) (x : A) (y : B),
    map_error f m = OK m' ->
    PTree.get i m = Some x ->
    f i x = OK y ->
    PTree.get i m' = Some y.
Proof.
  intros; destruct m as [|m]; simpl. revert H; intros [= <-]; discriminate.
  simpl in H. destruct map_error' eqn:H2; try discriminate.
  revert H; intros [= <-]. unfold PTree.get.
  rewrite (get_map_error'_Some _ i _ _ _ x y H2); try auto.
  repeat f_equal. rewrite PTree.prev_append_prev. eauto.
Qed.

Theorem get_map_error_None:
  forall {A B} (f: positive -> A -> res B) (i: positive) (m: PTree.t A) (m': PTree.t B),
    map_error f m = OK m' ->
    PTree.get i m = None ->
    PTree.get i m' = None.
Proof.
  intros; destruct m as [|m]; simpl. revert H; intros [= <-]; eauto.
  simpl in H. destruct map_error' eqn:H2; try discriminate.
  revert H; intros [= <-]. unfold PTree.get.
  rewrite (get_map_error'_None _ i _ _ _ H2); try auto.
Qed.

Lemma map_error'_set0 {A B: Type} (i j: positive) (x: A) (f: positive -> A -> res B):
  map_error' f (PTree.set0 i x) j =
  do v <- f (PTree.prev (PTree.prev_append i j)) x;
  OK (PTree.set0 i v).
Proof.
  revert j. induction i; simpl; intros; try rewrite IHi; destruct f; reflexivity.
Qed.

Lemma PTree_set_order_independent {A: Type}:
  forall e i1 i2 (x1 x2: A),
    i1 <> i2 ->
    PTree.set i1 x1 (PTree.set i2 x2 e) = PTree.set i2 x2 (PTree.set i1 x1 e).
Proof.
  intros. apply PTree.extensionality; intro.
  repeat rewrite PTree.gsspec.
  destruct (Coqlib.peq i i1), (Coqlib.peq i i2); congruence || reflexivity.
Qed.

Lemma PTree_elements_empty:
  forall A (t: PTree.t A),
    PTree.elements t = [] -> t = PTree.empty A.
Proof.
  intros.
  apply PTree.extensionality_empty. intro.
  apply BUtils.option_None_not_Some. intros y H1.
  apply PTree.elements_correct in H1.
  rewrite H in H1. contradiction.
Qed.

Definition incl {A: Type} (eqA: A -> A -> bool) (s t: PTree.t A) :=
    PTree_Properties.for_all s (fun id x =>
                                  match t!id with
                                  | Some y => eqA x y
                                  | None => false
                                  end).

Lemma incl_refl {A: Type} (eq: A -> A -> bool)
                          (Heq: forall x, eq x x = true):
  forall t, incl eq t t.
Proof.
  move=>>; apply PTree_Properties.for_all_correct => i x ->. by rewrite Heq.
Qed.

Lemma incl_trans {A: Type} (eq: A -> A -> bool)
                           (Heq: forall x y, reflect (x = y) (eq x y)):
  forall t1 t2 t3,
    incl eq t1 t2 -> incl eq t2 t3 -> incl eq t1 t3.
Proof.
  move=> t1 t2 t3 H1 H2. apply PTree_Properties.for_all_correct => i x.
  move/(proj1 (PTree_Properties.for_all_correct _ _) H1).
  case T: t2!i => // /Heq ->.
  exact (proj1 (PTree_Properties.for_all_correct _ _) H2 _ _ T).
Qed.

Lemma PTree_set_ident {A: Type}:
  forall (t: PTree.t A) i x, PTree.set i x t = t -> t!i = Some x.
Proof. move=>> <-. by rewrite PTree.gss. Qed.

Lemma double_incl_eq {A: Type} (eq: A -> A -> bool)
                               (Heq: forall x y, reflect (x = y) (eq x y)):
  forall t t', incl eq t t' -> incl eq t' t -> t = t'.
Proof.
  move=> t t' H1 H2. apply PTree.extensionality => i. case T: t!i => [x|].
  - have:= proj1 (PTree_Properties.for_all_correct _ _) H1 _ _ T.
    case t'!i => // > /Heq <- //.
  - case T': t'!i => //.
    have:= proj1 (PTree_Properties.for_all_correct _ _) H2 _ _ T'.
    by rewrite T.
Qed.

Lemma incl_set {A: Type} (eq: A -> A -> bool)
                         (Heq: forall x y, reflect (x = y) (eq x y)):
  forall (t t': PTree.t A) i x,
    incl eq t t' ->
    incl eq (PTree.set i x t) (PTree.set i x t').
Proof.
  move=> t t' i x I. apply PTree_Properties.for_all_correct => j y.
  case (Pos.eqb_spec j i) => [->|NEQ].
  - rewrite !PTree.gss => - [<-]. by case Heq.
  - rewrite !PTree.gso //.
    by move/(proj1 (PTree_Properties.for_all_correct _ _) I).
Qed.


Lemma incl_remove {A: Type} (eq: A -> A -> bool) (t t': PTree.t A):
  incl eq t t' ->
  forall i, incl eq (PTree.remove i t) (PTree.remove i t').
Proof.
  move=> I i. apply PTree_Properties.for_all_correct => j x H.
  have NEQ: j <> i. { move=> T. move: H. by rewrite T PTree.grs. }
  move: H. rewrite !PTree.gro //.
  by apply (proj1 (PTree_Properties.for_all_correct _ _) I).
Qed.

Definition PTree_union {A: Type} (t1 t2: PTree.t A) :=
  PTree.combine (fun t1 t2 =>
                   match t1, t2 with
                   | Some t1, Some t2 => Some t1
                   | Some t1, None => Some t1
                   | None, Some t2 => Some t2
                   | _, _ => None
                   end) t1 t2.

Definition PTree_inter {A: Type} (t1 t2: PTree.t A) :=
  PTree.combine (fun t1 t2 =>
                   match t1, t2 with
                   | Some t1, Some t2 => Some t1
                   | _, _ => None
                   end) t1 t2.

Fixpoint PTree_list_union {A: Type} (l1 l2: list (PTree.t A)) :=
  match l1, l2 with
  | [], [] => []
  | [], l | l, [] => l
  | e1 :: l1, e2 :: l2 => PTree_union e1 e2 :: PTree_list_union l1 l2
  end.

Lemma incl_union_unit (t1 t1' t2 t2': PTree.t unit):
  incl (fun _ _ => true) t1 t1' ->
  incl (fun _ _ => true) t2 t2' ->
  incl (fun _ _ => true) (PTree_union t1 t2) (PTree_union t1' t2').
Proof.
  move=> I1 I2. apply PTree_Properties.for_all_correct => i x.
  rewrite !PTree.gcombine //. case T1: t1!i => [[]|].
  - case T2: t2!i => [[]|] _.
    all: have:= proj1 (PTree_Properties.for_all_correct _ _) I1 _ _ T1.
    all: case t1'!i => //.
    + by case t2'!i => [[]|].
    + by case t2'!i => [[]|].
  - case T2: t2!i => [[]|] // _.
    have:= proj1 (PTree_Properties.for_all_correct _ _) I2 _ _ T2.
    case t2'!i => [[]|] // _. by case t1'!i => [[]|].
Qed.

Definition incl' {A: Type} (t1 t2: PTree.t A) :=
  forall i x, t1!i = Some x -> t2!i = Some x.

Lemma incl_reflect {A: Type} (eq: A -> A -> bool)
                   (Heq: forall x y, reflect (x = y) (eq x y)):
  forall x y,
    reflect (incl' x y) (incl eq x y).
Proof.
  move=> x y. case H: incl.
  - move/PTree_Properties.for_all_correct in H. apply ReflectT.
    move=>> /H. case y!_ =>> //= /Heq -> //.
  - apply ReflectF => T. have: incl eq x y = true.
    { apply PTree_Properties.for_all_correct.
      move=>> /T ->. by case Heq. }
    by rewrite H.
Qed.

Fixpoint init' {A: Type} (f: positive -> A) (n: nat) (p: positive) (t: PTree.t A) :=
  match n with
  | O => t
  | S n => init' f n (Pos.succ p) (PTree.set p (f p) t)
  end.

Definition init {A: Type} (f: positive -> A) (n: nat) : PTree.t A :=
  init' f n 1%positive (PTree.empty A).

Theorem init'_unchanged {A: Type}:
  forall (f: positive -> A) n p t i,
    (i < p)%positive ->
    (init' f n p t)!i = t!i.
Proof.
  move=> f; elim=> //= n IH > Hi; rewrite IH; [lia|].
  rewrite PTree.gso //; lia.
Qed.

Theorem init'_get {A: Type}:
  forall (f: positive -> A) n p t i,
    (i < n)%nat ->
    (init' f n p t)!(Pos.of_nat (Pos.to_nat p + i))
    = Some (f (Pos.of_nat (Pos.to_nat p + i))).
Proof.
  move=> f; elim=> /=. lia. move=> n IH p t [|i] Hi.
  - rewrite init'_unchanged; [lia|].
    by rewrite PeanoNat.Nat.add_0_r Pos2Nat.id PTree.gss.
  - have -> : (Pos.to_nat p + S i = Pos.to_nat (Pos.succ p) + i)%nat by lia.
    apply: IH; lia.
Qed.

Definition set_list {A: Type} (l: list positive) (x: A) (t: PTree.t A) :=
  fold_left (fun t i => PTree.set i x t) l t.

Lemma gslo {A: Type}:
  forall (t: PTree.t A) (x: A) (i: positive) (l: list positive),
    ~ In i l ->
    (set_list l x t)!i = t!i.
Proof.
  move=> + x i => /[swap]; unfold set_list; elim=> //= i' l IH t.
  case (Pos.eq_dec i i') => E. elim; by left.
  case (in_dec Pos.eq_dec i l) => I. elim; by right.
  move=> _; rewrite IH // PTree.gso //.
Qed.

Lemma gsls {A: Type}:
  forall (t: PTree.t A) (x: A) (i: positive) (l: list positive),
    In i l ->
    (set_list l x t)!i = Some x.
Proof.
  move=> + x i => /[swap]; unfold set_list; elim=> //= i' l IH t [->|].
  case (in_dec Pos.eq_dec i l) => I.
  - by apply: IH.
  - rewrite gslo // PTree.gss //.
  - by apply: IH.
Qed.

Inductive set_list_cases {A: Type} (i: positive) (x: A) (l: list positive)
                            (y: option A) : option A -> Prop :=
| set_list_In: In i l -> set_list_cases i x l y (Some x)
| set_list_NotIn: ~ In i l -> set_list_cases i x l y y.

Lemma case_set_list {A: Type} (t: PTree.t A):
  forall i x l,
    set_list_cases i x l t!i (set_list l x t)!i.
Proof.
  intros; case (in_dec Pos.eq_dec i l) => H.
  - rewrite (gsls _ _ _ _ H); by econstructor.
  - rewrite (gslo _ _ _ _ H); by econstructor.
Qed.

Definition remove_list {A: Type} (l: list positive) (t: PTree.t A) : PTree.t A :=
  fold_left (fun t i => PTree.remove i t) l t.

Lemma grlo {A: Type}:
  forall (t: PTree.t A) (i: positive) (l: list positive),
    ~ In i l ->
    (remove_list l t)!i = t!i.
Proof.
  move=> + i => /[swap]; unfold remove_list; elim=> //= i' l IH t.
  case (Pos.eq_dec i i') => E. elim; by left.
  case (in_dec Pos.eq_dec i l) => I. elim; by right.
  move=> _; rewrite IH // PTree.gro //.
Qed.

Lemma grls {A: Type}:
  forall (t: PTree.t A) (i: positive) (l: list positive),
    In i l ->
    (remove_list l t)!i = None.
Proof.
  move=> + i => /[swap]; unfold remove_list; elim=> //= i' l IH t [->|].
  case (in_dec Pos.eq_dec i l) => I.
  - by apply: IH.
  - rewrite grlo // PTree.grs //.
  - by apply: IH.
Qed.

Inductive remove_list_cases {A: Type} (i: positive) (l: list positive)
                            (x: option A) : option A -> Prop :=
| remove_list_In: In i l -> remove_list_cases i l x None
| remove_list_NotIn: ~ In i l -> remove_list_cases i l x x.

Lemma case_remove_list {A: Type} (t: PTree.t A):
  forall i l,
    remove_list_cases i l t!i (remove_list l t)!i.
Proof.
  intros; case (in_dec Pos.eq_dec i l) => H.
  - rewrite (grls _ _ _ H); by econstructor.
  - rewrite (grlo _ _ _ H); by econstructor.
Qed.

Inductive get_cases {A: Type} (i j: positive) (xi xj: A) : A -> Prop :=
| set_Eq: i = j -> get_cases i j xi xj xi
| set_Neq: i <> j -> get_cases i j xi xj xj.

Lemma case_set {A: Type} (t: PTree.t A):
  forall i j x,
    get_cases i j (Some x) t!j ((PTree.set i x t)!j).
Proof.
  intros; case (Pos.eq_dec i j) => /[dup] H => [->|].
  rewrite PTree.gss; by econstructor.
  rewrite PTree.gso; by [apply not_eq_sym|econstructor].
Qed.

Lemma case_remove {A: Type} (t: PTree.t A):
  forall i j,
    get_cases i j None t!j ((PTree.remove i t)!j).
Proof.
  intros; case (Pos.eq_dec i j) => /[dup] H => [->|].
  rewrite PTree.grs; by econstructor.
  rewrite PTree.gro; by [apply not_eq_sym|econstructor].
Qed.

Lemma remove_remove_list {A: Type}:
  forall (t: PTree.t A) l i,
    PTree.remove i (remove_list l t) = remove_list l (PTree.remove i t).
Proof.
  move=>>; apply PTree.extensionality =>>.
  case case_remove => [<-|] //.
  { case case_remove_list => //; by rewrite PTree.grs. }
  case case_remove_list; case case_remove_list => //.
  move=> _ _ H; apply not_eq_sym in H; by rewrite PTree.gro.
Qed.

Lemma remove_list_app {A: Type}:
  forall l1 l2 (t: PTree.t A),
    remove_list (l1 ++ l2) t = remove_list l1 (remove_list l2 t).
Proof. elim=> // > /= + > => ->; by rewrite remove_remove_list. Qed.

Lemma remove_remove {A: Type}:
  forall (t: PTree.t A) i,
    PTree.remove i (PTree.remove i t) = PTree.remove i t.
Proof.
  move=>>; apply PTree.extensionality =>>; rewrite ?PTree.grspec.
  by case PTree.elt_eq.
Qed.