(** Pretty-printer for our language *)
(* inspired by the Clight pretty-printer *)

open Format
open Camlcoq
open Maps
open Ops
open Types
open Syntax

type assoc = NA | LtoR | RtoL

let precedence = function
  | Eacc _ -> (17, NA)
  | Econst _ -> (17, NA)
  | Eunop _ -> (16, RtoL)
  | Ecast _ -> (15, RtoL)
  | Ebinop_arith ((Oshl|Oshr|Oshru), _, _, _) -> (14, LtoR)
  | Ebinop_arith ((Omul|Odivs|Omods|Odivu|Omodu), _, _, _) -> (13, LtoR)
  | Ebinop_arith ((Oadd|Osub), _, _, _) -> (12, LtoR)
  | Ebinop_cmp ((Olt|Ogt|Ole|Oge), _, _, _) -> (10, LtoR)
  | Ebinop_cmpu ((Oltu|Ogtu|Oleu|Ogeu), _, _, _) -> (10, LtoR)
  | Ebinop_cmp ((Oeq|One), _, _, _) -> (9, LtoR)
  | Ebinop_arith(Oand, _, _, _) -> (8, LtoR)
  | Ebinop_arith(Oxor, _, _, _) -> (7, LtoR)
  | Ebinop_arith(Oor, _, _, _) -> (6, LtoR)

let name_unop = function
  | Oneg -> "-"
  | Onotint -> "~"
  | Onotbool -> "!"
  | Oabsfloat -> "__builtin_fabs"

let name_binop_arith = function
  | Oadd -> "+"
  | Osub -> "-"
  | Omul -> "*"
  | Odivs | Odivu -> "/"
  | Omods | Omodu -> "%"
  | Oshl -> "<<"
  | Oshr | Oshru -> ">>"
  | Oand -> "&"
  | Oor  -> "|"
  | Oxor -> "^"

let name_binop_cmp = function
  | Ole -> "<="
  | Olt -> "<"
  | Oge -> ">="
  | Ogt -> ">"
  | Oeq -> "=="
  | One -> "!="

let name_binop_cmpu = function
  | Oleu -> "<=u"
  | Oltu -> "<u"
  | Ogeu -> ">=u"
  | Ogtu -> ">u"

let name_inttype sz sg =
  match sz, sg with
  | I8, Signed -> "i8"
  | I8, Unsigned -> "u8"
  | I16, Signed -> "i16"
  | I16, Unsigned -> "u16"
  | I32, Signed -> "i32"
  | I32, Unsigned -> "u32"

let rec print_type p = function
  | Tvoid -> fprintf p "void"
  | Tbool -> fprintf p "bool"
  | Tint (sz, sg) -> fprintf p "%s" (name_inttype sz sg)
  | Tint64 Signed -> fprintf p "i64"
  | Tint64 Unsigned -> fprintf p "u64"
  | Tfloat F32 -> fprintf p "f32"
  | Tfloat F64 -> fprintf p "f64"
  | Tarr t -> fprintf p "[%a]" print_type t

let extern_atom a =
  try sprintf "%s%d" (ParserUtils.get_name a) (Camlcoq.P.to_int a)
  with ParserUtils.NotFoundName _ ->
    extern_atom a

let rec print_ident_list p lids =
  match lids with
  | [] -> ()
  | [id] -> fprintf p "%s" (extern_atom id)
  | id :: lids -> fprintf p "%s, " (extern_atom id);
                  print_ident_list p lids

let rec expr p (prec, e) =
  let (prec', assoc) = precedence e in
  let (prec1, prec2) =
    if assoc = LtoR
    then (prec', prec' + 1)
    else (prec' + 1, prec') in
  if prec' < prec
  then fprintf p "@[<hov 2>("
  else fprintf p "@[<hov 2>";
  begin match e with
  | Econst (Cint (I32, Unsigned, n)) ->
      fprintf p "%luu32" (camlint_of_coqint n)
  | Econst (Cint (_, _, n)) ->
      fprintf p "%ldi32" (camlint_of_coqint n)
  | Econst (Cfloat64 f) ->
      fprintf p "%.18gf64" (camlfloat_of_coqfloat f)
  | Econst (Cfloat32 f) ->
      fprintf p "%.18gf32" (camlfloat_of_coqfloat32 f)
  | Econst (Cint64 (Unsigned, n)) ->
      fprintf p "%Luu64" (camlint64_of_coqint n)
  | Econst (Cint64 (_, n)) ->
      fprintf p "%Ldi64" (camlint64_of_coqint n)
  | Econst (Cbool b) ->
      fprintf p "%s" (if b then "true" else "false")
  | Eunop (Oabsfloat, _, a1) ->
      fprintf p "__builtin_fabs(%a)" expr (2, a1)
  | Eunop (op, _, a1) ->
      fprintf p "%s%a" (name_unop op) expr (prec', a1)
  | Ebinop_arith (op, _, a1, a2) ->
      fprintf p "%a@ %s %a"
            expr (prec1, a1) (name_binop_arith op) expr (prec2, a2)
  | Ebinop_cmp (op, _, a1, a2) ->
      fprintf p "%a@ %s %a"
            expr (prec1, a1) (name_binop_cmp op) expr (prec2, a2)
  | Ebinop_cmpu (op, _, a1, a2) ->
      fprintf p "%a@ %s %a"
            expr (prec1, a1) (name_binop_cmpu op) expr (prec2, a2)
  | Ecast (a1, t1, ty) ->
      fprintf p "(%a -> %a) %a" print_type t1 print_type ty expr (prec', a1)
  | Eacc (id, lp) ->
      fprintf p "%s%a" (extern_atom id) syn_path_elem_list (prec', lp)
  end;
  if prec' < prec then fprintf p ")@]" else fprintf p "@]"

and expr_list p (prec, le) =
  match le with
  | [] -> ()
  | e :: [] -> expr p (prec, e)
  | e :: le ->
      expr p (prec, e);
      fprintf p ",@ ";
      expr_list p (prec, le)

and syn_path_elem_list p (prec, lp) =
  match lp with
  | [] -> ()
  | Scell le :: lp ->
      fprintf p "[%a]" expr_list (prec, le);
      syn_path_elem_list p (prec, lp)

let print_expr p e = expr p (0, e)

let print_expr_list p le =
  expr_list p (0, le)

let print_syn_path p (i, lp) =
  fprintf p "%s%a" (extern_atom i) syn_path_elem_list (0, lp)

let rec print_type_with_szvars p (szvars, t) =
  match t with
  | Tarr t ->
    begin match szvars with
    | [] -> fprintf p "[%a]" print_type t
    | l :: szvars -> fprintf p "@[[%a; %a]@]" print_type_with_szvars (szvars, t)
                               print_expr_list l
    end
  | _ -> print_type p t

let rec print_syn_path_list p ls =
  match ls with
  | [] -> ()
  | s :: [] -> print_syn_path p s
  | s :: ls ->
      print_syn_path p s;
      fprintf p ",@ ";
      print_syn_path_list p ls

let rec print_stmt p (blocks, s) =
  match s with
  | Sskip ->
      fprintf p "/*skip*/;"
  | Sassign (s, e) ->
      fprintf p "@[<hv 2>%a =@ %a;@]" print_syn_path s print_expr e
  | Scall (None, idf, args) ->
      fprintf p "@[<hv 2>%s@,(@[<hov 0>%a@]);@]"
                (extern_atom idf)
                print_syn_path_list args
  | Scall (Some idv, idf, args) ->
      fprintf p "@[<hv 2>%s =@ %s@,(@[<hov 0>%a@]);@]"
                (extern_atom idv)
                (extern_atom idf)
                print_syn_path_list args
  | Salloc i -> 
      fprintf p "@[<hv 2>alloc(%s);@]" (extern_atom i)
  | Sfree i -> 
      fprintf p "@[<hv 2>alloc(%s);@]" (extern_atom i)
  | Sseq (Sskip, s2) ->
      print_stmt p (blocks, s2)
  | Sseq (s1, Sskip) ->
      print_stmt p (blocks, s1)
  | Sseq (s1, s2) ->
      fprintf p "%a@ %a" print_stmt (blocks, s1) print_stmt (blocks, s2)
  | Sassert e ->
      fprintf p "@[assert (%a);@]" print_expr e
  | Sifthenelse (e, s1, Sskip) ->
      fprintf p "@[<v 2>if (%a) {@ %a@;<0 -2>}@]"
              print_expr e
              print_stmt (blocks, s1)
  | Sifthenelse (e, Sskip, s2) ->
      fprintf p "@[<v 2>if (! %a) {@ %a@;<0 -2>}@]"
              expr (15, e)
              print_stmt (blocks, s2)
  | Sifthenelse (e, s1, s2) ->
      fprintf p "@[<v 2>if (%a) {@ %a@;<0 -2>} else {@ %a@;<0 -2>}@]"
              print_expr e
              print_stmt (blocks, s1)
              print_stmt (blocks, s2)
  | Sloop s ->
      fprintf p "@[<v 2>while (true) {@ %a@;<0 -2>}@]"
              print_stmt (blocks, s)
  | Sblock s ->
      let blk_id = Camlcoq.fresh_atom () in
      let name = Camlcoq.extern_atom blk_id in
      fprintf p "@[<v 2>%s: {@ %a@;<0 -2>}@]" name print_stmt (blk_id :: blocks, s)
  | Sexit n ->
      begin try
        let name = Camlcoq.extern_atom (List.nth blocks (Camlcoq.Nat.to_int n)) in
        fprintf p "exit %s;" name
      with Failure _ ->
        failwith "exit does not match any block"
      end
  | Sreturn None ->
      fprintf p "return;"
  | Sreturn (Some e) ->
      fprintf p "return %a;" print_expr e
  | Serror ->
      fprintf p "error;"

let rec print_function_parameters p (pe, sze, params, types) =
  match params, types with
  | [], _ | _, [] -> ()
  | [id], [t] ->
    let perm = match PTree.get id pe with
      | Some Mutable -> "mut "
      | Some Owned   -> "own "
      | Some _       -> ""
      | None -> assert false in
    let szvars = match PTree.get id sze with
      | None -> assert false
      | Some l -> l in
    fprintf p "%s: %s%a"
            (extern_atom id) perm print_type_with_szvars (szvars, t)
  | id :: params, t :: types ->
    let perm = match PTree.get id pe with
      | Some Mutable -> "mut "
      | Some Owned   -> "own "
      | Some _       -> ""
      | None -> assert false in
    let szvars = match PTree.get id sze with
      | None -> assert false
      | Some l -> l in
    fprintf p "%s: %s%a, %a"
            (extern_atom id) perm print_type_with_szvars (szvars, t)
            print_function_parameters (pe, sze, params, types)

let print_decl p (id, tenv) =
  match PTree.get id tenv with
  | None -> failwith ("Print error: cannot get type of " ^ (extern_atom id) ^ " from tenv.")
  | Some t ->
    fprintf p "let %s: %a;" (extern_atom id) print_type t 

let print_size_vars_initialization p sze sze' =
  let seen = Hashtbl.create 10 in
  PTree.fold (fun () i llsz ->
    let llsz' = match PTree.get i sze' with Some llsz' -> llsz' | _ -> assert false in
    List.iter2 (List.iter2 (fun szexp szi ->
        if not (Hashtbl.mem seen szi) then begin
          Hashtbl.add seen szi ();
          fprintf p "%a@ " print_stmt ([], Sassign ((szi, []), szexp))
        end
      )) llsz llsz'
  ) sze ()

let print_function p id f =
  fprintf p "fun %s(%a) -> %a@ " (extern_atom id)
            print_function_parameters
            (f.fn_penv, f.fn_szenv, f.fn_params, f.fn_sig.sig_args)
            print_type f.fn_sig.sig_res;
  fprintf p "@[<v 2>{@ ";
  List.iter
    (fun id ->
      fprintf p "%a@ " print_decl (id, f.fn_tenv))
    f.fn_vars;
  print_size_vars_initialization p f.fn_szenv f.fn_szenv';
  print_stmt p ([], f.fn_body);
  fprintf p "@;<0 -2>}@]@ @ "

let rec print_sig_args p args =
  match args with
  | [] -> ()
  | [t] -> fprintf p "%a" print_type t
  | t :: args -> fprintf p "%a, %a" print_type t print_sig_args args

let print_fundef p id fd =
  match fd with
  | External ef ->
    fprintf p "%a %s(%a);@ "
            print_type ef.ef_sig.sig_res
            (extern_atom id)
            print_sig_args ef.ef_sig.sig_args;
  | Internal f -> print_function p id f

let print_program p prog =
  fprintf p "@[<v 0>";
  List.iter (fun (id, fd) -> print_fundef p id fd) prog.prog_defs;
  fprintf p "@]@."

let destination : string option ref = ref None

let nb_destination : string option ref = ref None

let nbut_destination : string option ref = ref None

let print_if_gen prog =
  match !destination with
  | None -> ()
  | Some f ->
      let oc = open_out f in
      print_program (formatter_of_out_channel oc) prog;
      close_out oc

let print_nb_if_gen prog =
  match !nb_destination with
  | None -> ()
  | Some f ->
      let oc = open_out f in
      print_program (formatter_of_out_channel oc) prog;
      close_out oc

let print_nbut_if_gen prog =
  match !nbut_destination with
  | None -> ()
  | Some f ->
      let oc = open_out f in
      print_program (formatter_of_out_channel oc) prog;
      close_out oc

let string_of_type t = Format.asprintf "%a" print_type t
let string_of_expr e = Format.asprintf "%a" print_expr e
let string_of_stmt s = Format.asprintf "%a" print_stmt s

let print_if prog = print_if_gen prog

let print_nb_if prog = print_nb_if_gen prog

let print_nbut_if prog = print_nbut_if_gen prog
