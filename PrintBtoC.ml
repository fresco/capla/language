(** C printer for our language *)
(* inspired by the Clight pretty-printer *)

open Format
open Camlcoq
open Maps
open Ops
open Types
open Syntax

type assoc = NA | LtoR | RtoL

let precedence = function
  | Eacc _ -> (17, NA)
  | Econst _ -> (17, NA)
  | Eunop _ -> (16, RtoL)
  | Ecast _ -> (15, RtoL)
  | Ebinop_arith ((Oshl|Oshr|Oshru), _, _, _) -> (14, LtoR)
  | Ebinop_arith ((Omul|Odivs|Omods|Odivu|Omodu), _, _, _) -> (13, LtoR)
  | Ebinop_arith ((Oadd|Osub), _, _, _) -> (12, LtoR)
  | Ebinop_cmp ((Olt|Ogt|Ole|Oge), _, _, _) -> (10, LtoR)
  | Ebinop_cmpu ((Oltu|Ogtu|Oleu|Ogeu), _, _, _) -> (10, LtoR)
  | Ebinop_cmp ((Oeq|One), _, _, _) -> (9, LtoR)
  | Ebinop_arith(Oand, _, _, _) -> (8, LtoR)
  | Ebinop_arith(Oxor, _, _, _) -> (7, LtoR)
  | Ebinop_arith(Oor, _, _, _) -> (6, LtoR)

let name_unop = function
  | Oneg -> "-"
  | Onotint -> "~"
  | Onotbool -> "!"
  | Oabsfloat -> "__builtin_fabs"

let name_binop_arith = function
  | Oadd -> "+"
  | Osub -> "-"
  | Omul -> "*"
  | Odivs | Odivu -> "/"
  | Omods | Omodu -> "%"
  | Oshl -> "<<"
  | Oshr | Oshru -> ">>"
  | Oand -> "&"
  | Oor  -> "|"
  | Oxor -> "^"

let name_binop_cmp = function
  | Ole -> "<="
  | Olt -> "<"
  | Oge -> ">="
  | Ogt -> ">"
  | Oeq -> "=="
  | One -> "!="

let name_binop_cmpu = function
  | Oleu -> "<="
  | Oltu -> "<"
  | Ogeu -> ">="
  | Ogtu -> ">"

let name_inttype sz sg =
  match sz, sg with
  | I8, Signed -> "int8_t"
  | I8, Unsigned -> "uint8_t"
  | I16, Signed -> "int16_t"
  | I16, Unsigned -> "uint16_t"
  | I32, Signed -> "int32_t"
  | I32, Unsigned -> "uint32_t"

let rec print_type p = function
  | Tvoid -> fprintf p "void"
  | Tbool -> fprintf p "uint8_t"
  | Tint (sz, sg) -> fprintf p "%s" (name_inttype sz sg)
  | Tint64 Signed -> fprintf p "int64_t"
  | Tint64 Unsigned -> fprintf p "uint64_t"
  | Tfloat F32 -> fprintf p "_Float32"
  | Tfloat F64 -> fprintf p "_Float64"
  | Tarr t -> fprintf p "%a*" print_type t

let extern_atom a =
  try
    try sprintf "%s%d" (ParserUtils.get_name a) (Camlcoq.P.to_int a)
    with ParserUtils.NotFoundName _ ->
      Hashtbl.find string_of_atom a
  with Not_found ->
    Printf.sprintf "__temp_%d" (P.to_int a)

let rec print_ident_list p lids =
  match lids with
  | [] -> ()
  | [id] -> fprintf p "%s" (extern_atom id)
  | id :: lids -> fprintf p "%s, " (extern_atom id);
                  print_ident_list p lids

let linear_array_index lsz lx =
  let rec aux e lsz lx =
    match lx, lsz with
    | [], _ -> e
    | x :: lx, sz :: lsz ->
      let e = Ebinop_arith (Oadd, OInt64, Ebinop_arith (Omul, OInt64, e, Eacc (sz, [])), x) in
      aux e lsz lx
    | _ :: _, [] -> failwith "Illegal access to a non-array object." in
  match lx with
  | [] -> Econst (Cint64 (Unsigned, Integers.Int64.zero))
  | idx :: lx -> aux idx (List.tl lsz) lx

let rec expr p (prec, sze, e) =
  let expr p (prec, e) = expr p (prec, sze, e) in
  let (prec', assoc) = precedence e in
  let (prec1, prec2) =
    if assoc = LtoR
    then (prec', prec' + 1)
    else (prec' + 1, prec') in
  if prec' < prec
  then fprintf p "@[<hov 2>("
  else fprintf p "@[<hov 2>";
  begin match e with
  | Econst (Cint (I32, Unsigned, n)) ->
      fprintf p "%lu" (camlint_of_coqint n)
  | Econst (Cint (_, _, n)) ->
      fprintf p "%ld" (camlint_of_coqint n)
  | Econst (Cfloat64 f) ->
      fprintf p "%h" (camlfloat_of_coqfloat f)
  | Econst (Cfloat32 f) ->
      fprintf p "%hf" (camlfloat_of_coqfloat32 f)
  | Econst (Cint64 (Unsigned, n)) ->
      fprintf p "%LuLL" (camlint64_of_coqint n)
  | Econst (Cint64 (_, n)) ->
      fprintf p "%LdLL" (camlint64_of_coqint n)
  | Econst (Cbool b) ->
      fprintf p "%s" (if b then "1" else "0")
  | Eunop (Oabsfloat, _, a1) ->
      fprintf p "__builtin_fabs(%a)" expr (2, a1)
  | Eunop (Oneg, k, a1) ->
      begin match k with
      | OInt -> fprintf p "%s%a" (name_unop Oneg)
                  expr (prec', Ecast (a1, Tint (I32, Signed), Tint (I32, Unsigned)))
      | OInt64 -> fprintf p "%s%a" (name_unop Oneg)
                  expr (prec', Ecast (a1, Tint64 Signed, Tint64 Unsigned))
      | OFloat32 -> fprintf p "%s%a" (name_unop Oneg)
                      expr (prec', Ecast (a1, Tfloat F32, Tfloat F32))
      | OFloat64 -> fprintf p "%s%a" (name_unop Oneg)
                      expr (prec', Ecast (a1, Tfloat F64, Tfloat F64))
      end
  | Eunop (op, _, a1) ->
      fprintf p "%s%a" (name_unop op) expr (prec', a1)
  | Ebinop_arith ((Oshl | Oshr | Oshru) as op, k, a1, a2) ->
      begin match k with
      | OInt ->
        let c1 = match op with
                 | Oshru | Oshl -> "(uint32_t) "
                 | Oshr  -> "(int32_t) "
                 | _ -> assert false in
        fprintf p "%s(%a)@ %s ((%a) & 31)" c1
          expr (prec1, a1) (name_binop_arith op) expr (prec2, a2)
      | OInt64 ->
        let c1 = match op with
                 | Oshru | Oshl -> "(uint64_t) "
                 | Oshr  -> "(int64_t) "
                 | _ -> assert false in
        fprintf p "%s(%a)@ %s ((%a) & 63)" c1
          expr (prec1, a1) (name_binop_arith op) expr (prec2, a2)
      | _ -> ()
      end
  | Ebinop_arith ((Oand | Oxor | Oor) as op, _, a1, a2) ->
      fprintf p "%a@ %s %a" expr (prec1, a1) (name_binop_arith op) expr (prec2, a2)
  | Ebinop_arith ((Oadd | Osub | Omul) as op, k, a1, a2) ->
      begin match k with
      | OInt ->
          fprintf p "(uint32_t) (%a) @ %s (uint32_t) (%a)"
            expr (prec1, a1) (name_binop_arith op) expr (prec2, a2)
      | OInt64 ->
          fprintf p "(uint64_t) (%a)@ %s (uint64_t) (%a)"
            expr (prec1, a1) (name_binop_arith op) expr (prec2, a2)
      | OFloat32 ->
          fprintf p "(_Float32) (%a@ %s %a)"
            expr (prec1, a1) (name_binop_arith op) expr (prec2, a2)
      | OFloat64 ->
          fprintf p "(_Float64) (%a@ %s %a)"
            expr (prec1, a1) (name_binop_arith op) expr (prec2, a2)
      end
  | Ebinop_arith ((Omodu | Omods | Odivu | Odivs) as op, ((OFloat32 | OFloat64) as k), a1, a2) ->
      let c = match k with
              | OFloat32 -> "(_Float32) "
              | OFloat64 -> "(_Float64) "
              | _ -> assert false in
      fprintf p "%s (%a@ %s %a)" c expr (prec1, a1) (name_binop_arith op) expr (prec2, a2)
  | Ebinop_arith ((Omodu | Omods | Odivu | Odivs) as op, ((OInt | OInt64) as k), a1, a2) ->
      let c = match op, k with
              | (Omods | Odivs), OInt -> "(int32_t) "
              | (Omodu | Odivu), OInt -> "(uint32_t) "
              | (Omods | Odivs), OInt64 -> "(int64_t) "
              | (Omodu | Odivu), OInt64 -> "(uint64_t) "
              | _, _ -> assert false in
      fprintf p "%s(%a)@ %s %s(%a)"
              c expr (prec1, a1) (name_binop_arith op) c expr (prec2, a2)
  | Ebinop_cmp (op, _, a1, a2) ->
      fprintf p "%a@ %s %a"
            expr (prec1, a1) (name_binop_cmp op) expr (prec2, a2)
  | Ebinop_cmpu (op, _, a1, a2) ->
      fprintf p "%a@ %s %a"
            expr (prec1, a1) (name_binop_cmpu op) expr (prec2, a2)
  | Ecast (a1, _, ty) ->
      fprintf p "(%a) %a" print_type ty expr (prec', a1)
  | Eacc (id, lp) ->
      match PTree.get id sze with
      | Some llsz ->
        fprintf p "%s%a" (extern_atom id) syn_path_elem_list (prec', sze, llsz, lp)
      | None -> failwith "Undefined variable."
  end;
  if prec' < prec then fprintf p ")@]" else fprintf p "@]"

and expr_list p (prec, sze, le) =
  match le with
  | [] -> ()
  | e :: le ->
      expr p (prec, sze, e);
      fprintf p ",@ ";
      expr_list p (prec, sze, le)

and syn_path_elem_list p (prec, sze, llsz, lp) =
  match lp, llsz with
  | [], _ -> ()
  | Scell le :: lp, lsz :: llsz ->
      let idx = linear_array_index lsz le in
      fprintf p "[%a]" expr (prec, sze, idx);
      syn_path_elem_list p (prec, sze, llsz, lp)
  | _ :: _, [] -> failwith "Illegal access to non-array object."

let print_expr p (sze, e) = expr p (0, sze, e)

let print_expr_list p (sze, le) =
  expr_list p (0, sze, le)

let print_syn_path p (sze, (i, lp)) =
  match PTree.get i sze with
  | Some llsz ->
    fprintf p "%s%a" (extern_atom i) syn_path_elem_list (0, sze, llsz, lp)
  | None -> failwith "Undefined variable."

let rec print_syn_path_list p (sze, ls) =
  match ls with
  | [] -> ()
  | s :: [] -> print_syn_path p (sze, s)
  | s :: ls ->
      print_syn_path p (sze, s);
      fprintf p ",@ ";
      print_syn_path_list p (sze, ls)

let size_product lsz =
  let rec aux lsz sz0 =
    match lsz with
    | [] -> sz0
    | sz :: lsz ->
      let sz0' = Ebinop_arith(Omul, OInt64, sz0, sz) in
      aux lsz sz0' in
  match lsz with
  | [] -> Econst (Cint64 (Unsigned, Integers.Int64.zero))
  | sz :: lsz -> aux lsz sz

type labelKind = Loop | InLoop | Other

let rec no_loop_before blocks n =
  if n <= 0 then true
  else
    match blocks with
    | [] -> true
    | (_, Loop) :: _ -> false
    | (_, (InLoop | Other)) :: blocks -> no_loop_before blocks (n - 1)

let rec print_stmt p (f, blocks, s) =
  let print_stmt p (blocks, s) = print_stmt p (f, blocks, s) in
  match s with
  | Sskip ->
      fprintf p "/*skip*/;"
  | Sassign (s, e) ->
      fprintf p "@[<hv 2>%a =@ %a;@]" print_syn_path (f.fn_szenv', s) print_expr (f.fn_szenv', e)
  | Scall (None, idf, args) ->
      fprintf p "@[<hv 2>%s@,(@[<hov 0>%a@]);@]"
                (extern_atom idf)
                print_syn_path_list (f.fn_szenv', args)
  | Scall (Some idv, idf, args) ->
      fprintf p "@[<hv 2>%s =@ %s@,(@[<hov 0>%a@]);@]"
                (extern_atom idv)
                (extern_atom idf)
                print_syn_path_list (f.fn_szenv', args)
  | Salloc i ->
      begin match PTree.get i f.fn_tenv, PTree.get i f.fn_szenv with
      | Some (Tarr t), Some (lsz :: llsz) ->
        let nmemb = size_product lsz in
        fprintf p "@[<hv 2>%s = calloc(%a, sizeof(%a));@]" (extern_atom i)
                print_expr (f.fn_szenv', nmemb) print_type t
      | Some (Tarr _), _ -> failwith "Dynamically allocated array doesn't have any size variables."
      | _, _ -> failwith "Cannot allocate something which is not an array."
      end
  | Sfree i -> 
      fprintf p "@[<hv 2>free(%s);@]" (extern_atom i)
  | Sseq (Sskip, s2) ->
      print_stmt p (blocks, s2)
  | Sseq (s1, Sskip) ->
      print_stmt p (blocks, s1)
  | Sseq (s1, s2) ->
      fprintf p "%a@ %a" print_stmt (blocks, s1) print_stmt (blocks, s2)
  | Sassert e ->
      fprintf p "@[assert (%a);@]" print_expr (f.fn_szenv', e)
  | Sifthenelse (e, s1, Sskip) ->
      fprintf p "@[<v 2>if (%a) {@ %a@;<0 -2>}@]"
              print_expr (f.fn_szenv', e)
              print_stmt (blocks, s1)
  | Sifthenelse (e, Sskip, s2) ->
      fprintf p "@[<v 2>if (! %a) {@ %a@;<0 -2>}@]"
              expr (15, f.fn_szenv', e)
              print_stmt (blocks, s2)
  | Sifthenelse (e, s1, s2) ->
      fprintf p "@[<v 2>if (%a) {@ %a@;<0 -2>} else {@ %a@;<0 -2>}@]"
              print_expr (f.fn_szenv', e)
              print_stmt (blocks, s1)
              print_stmt (blocks, s2)
  | Sblock (Sloop s) ->
      let blk_id = Camlcoq.fresh_atom () in
      let name = Camlcoq.extern_atom blk_id in
      fprintf p
        "@[<v 2>{@ %a@;<0 -2>}@;%s: ;@]"
        print_stmt ((blk_id, Other) :: blocks, Sloop s) name
  | Sblock s ->
      let blk_id = Camlcoq.fresh_atom () in
      let name = Camlcoq.extern_atom blk_id in
      let k = match blocks with
              | [] -> Other
              | (_, Loop) :: _ -> InLoop
              | _ :: _ -> Other in
      fprintf p "@[<v 2>{@ %a@;<0 -2>}@;%s: ;@]" print_stmt ((blk_id, k) :: blocks, s) name
  | Sloop s ->
      begin match blocks with
      | [] -> failwith "loop outside of a block"
      | (blk_id, _) :: blocks ->
        fprintf p "@[<v 2>while (1) {@ %a@;<0 -2>}@]" print_stmt ((blk_id, Loop) :: blocks, s)
      end
  | Sexit n ->
      begin try
        let n = Camlcoq.Nat.to_int n in
        let blkid, k = List.nth blocks n in
        if no_loop_before blocks n then
          match k with
          | Loop -> fprintf p "break;"
          | InLoop -> fprintf p "continue;"
          | Other -> let name = Camlcoq.extern_atom blkid in
                     fprintf p "goto %s;" name
        else let name = Camlcoq.extern_atom blkid in
             fprintf p "goto %s;" name
      with Failure _ ->
        failwith "exit does not match any block"
      end
  | Sreturn None ->
      fprintf p "return;"
  | Sreturn (Some e) ->
      fprintf p "return %a;" print_expr (f.fn_szenv', e)
  | Serror ->
      fprintf p "error;"

let rec print_function_parameters p (pe, sze, params, types) =
  match params, types with
  | [], _ | _, [] -> ()
  | [id], [t] ->
    let r = match PTree.get id pe with
      | Some (Mutable | Owned) -> "restrict "
      | Some _       -> ""
      | None -> assert false in
    let cst = match PTree.get id pe, PTree.get id sze with
      | Some Shared, Some (_ :: _) -> "const "
      | _ -> "" in
    fprintf p "%s%a %s%s" cst print_type t r (extern_atom id)
  | id :: params, t :: types ->
    let r = match PTree.get id pe with
      | Some (Mutable | Owned) -> "restrict "
      | Some _       -> ""
      | None -> assert false in
    let cst = match PTree.get id pe, PTree.get id sze with
      | Some Shared, Some (_ :: _) -> "const "
      | _ -> "" in
    fprintf p "%s%a %s%s, %a" cst
            print_type t r (extern_atom id)
            print_function_parameters (pe, sze, params, types)

let print_decl p (id, tenv) =
  match PTree.get id tenv with
  | None -> failwith ("Print error: cannot get type of " ^ (extern_atom id) ^ " from tenv.")
  | Some t ->
    fprintf p "%a %s;" print_type t (extern_atom id)

let print_size_vars_initialization p f =
  let seen = Hashtbl.create 10 in
  PTree.fold (fun () i llsz ->
    let llsz' = match PTree.get i f.fn_szenv' with Some llsz' -> llsz' | _ -> assert false in
    List.iter2 (List.iter2 (fun szexp szi ->
        if not (Hashtbl.mem seen szi) then begin
          Hashtbl.add seen szi ();
          fprintf p "%a@ " print_stmt (f, [], Sassign ((szi, []), szexp))
        end
      )) llsz llsz'
  ) f.fn_szenv ()

let print_function p id f =
  fprintf p "%a %s(%a)@ " print_type f.fn_sig.sig_res (extern_atom id)
                          print_function_parameters
                          (f.fn_penv, f.fn_szenv, f.fn_params, f.fn_sig.sig_args);
  fprintf p "@[<v 2>{@ ";
  List.iter
    (fun id ->
      fprintf p "%a@ " print_decl (id, f.fn_tenv))
    f.fn_vars;
  print_size_vars_initialization p f;
  print_stmt p (f, [], f.fn_body);
  fprintf p "@;<0 -2>}@]@ @ "

let rec print_sig_args p args =
  match args with
  | [] -> ()
  | [t] -> fprintf p "%a" print_type t
  | t :: args -> fprintf p "%a, %a" print_type t print_sig_args args

let print_fundef p id fd =
  match fd with
  | External ef -> ()
  | Internal f -> print_function p id f

let header =
{|#include <stdint.h>
#include <stdlib.h>
#include <math.h>
#include <float.h>
#include <assert.h>

#if defined(__HAVE_FLOAT32) && defined(__HAVE_FLOAT64)
#else
static int __f32_test[sizeof(float) == 4 ? 1 : -1];
static int __f64_test[sizeof(double) == 8 ? 1 : -1];
typedef float _Float32;
typedef double _Float64;
#endif
|}

let print_program p prog =
  fprintf p "@[<v 0>";
  fprintf p "%s@ @ " header;
  (* functions declarations *)
  List.iter (fun (id, fd) -> match fd with
    | Internal f ->
      fprintf p "%a %s(%a);@ "
              print_type f.fn_sig.sig_res (extern_atom id)
              print_function_parameters
              (f.fn_penv, f.fn_szenv, f.fn_params, f.fn_sig.sig_args)
    | External ef ->
      fprintf p "extern %a %s(%a);@ "
              print_type ef.ef_sig.sig_res (extern_atom id)
              print_function_parameters
              (ef.ef_penv, ef.ef_szenv, ef.ef_params, ef.ef_sig.sig_args)
  ) prog.prog_defs;
  fprintf p "@ @ ";
  (* print internal functions *)
  List.iter (fun (id, fd) -> print_fundef p id fd) prog.prog_defs;
  fprintf p "@]@."

let destination : string option ref = ref None

let print_if_gen prog =
  match !destination with
  | None -> ()
  | Some f ->
      let oc = open_out f in
      print_program (formatter_of_out_channel oc) prog;
      close_out oc

let print_if prog = print_if_gen prog