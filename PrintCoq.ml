(** Pretty-printer from L1's AST to a Coq Term *)

open Format
open Maps
open Ops
open Types
open Syntax
open Binary (* Flocq *)
open Camlcoq

let extern_atom a =
  try sprintf "%s%d" (ParserUtils.get_name a) (P.to_int a)
  with ParserUtils.NotFoundName _ ->
    extern_atom a

let permission p perm =
  match perm with
  | Shared  -> fprintf p "Shared"
  | Mutable -> fprintf p "Mutable"
  | Owned   -> fprintf p "Owned"

let opkind = function
  | OInt     -> "OInt"
  | OInt64   -> "OInt64"
  | OFloat32 -> "OFloat32"
  | OFloat64 -> "OFloat64"

let unop = function
  | Oneg      -> "Oneg"
  | Onotint   -> "Onotint"
  | Onotbool  -> "Onotbool"
  | Oabsfloat -> "Oabsfloat"
let binop_arith = function
  | Oadd  -> "Oadd"
  | Osub  -> "Osub"
  | Omul  -> "Omul"
  | Odivs -> "Odivs"
  | Odivu -> "Odivu"
  | Omods -> "Omods"
  | Omodu -> "Omodu"
  | Oshl  -> "Oshl"
  | Oshr  -> "Oshr"
  | Oshru -> "Oshru"
  | Oand  -> "Oand"
  | Oor   -> "Oor"
  | Oxor  -> "Oxor"

let binop_cmp = function
  | Ole -> "Ole"
  | Olt -> "Olt"
  | Oge -> "Oge"
  | Ogt -> "Ogt"
  | Oeq -> "Oeq"
  | One -> "One"

let binop_cmpu = function
  | Oleu -> "Oleu"
  | Oltu -> "Oltu"
  | Ogeu -> "Ogeu"
  | Ogtu -> "Ogtu"

let intsize sz =
  match sz with
  | I8  -> "I8"
  | I16 -> "I16"
  | I32 -> "I32"

let signedness sg =
  match sg with
  | Signed   -> "Signed"
  | Unsigned -> "Unsigned"

let int_short sz sg =
  match sz, sg with
  |  I8, Unsigned -> "u8"
  | I16, Unsigned -> "u16"
  | I32, Unsigned -> "u32"
  |  I8, Signed -> "i8"
  | I16, Signed -> "i16"
  | I32, Signed -> "i32"

let rec typ p = function
  | Tvoid -> fprintf p "Tvoid"
  | Tbool -> fprintf p "Tbool"
  | Tint (sz, sg) -> fprintf p "%s" (int_short sz sg)
    (* fprintf p "Tint %s %s" (intsize sz) (signedness sg) *)
  | Tint64 Signed -> fprintf p "i64"
  | Tint64 Unsigned -> fprintf p "u64"
  | Tfloat F32 -> fprintf p "f32"
  | Tfloat F64 -> fprintf p "f64"
  | Tarr (Tarr _ as t) -> fprintf p "Tarr (%a)" typ t
  | Tarr t -> fprintf p "Tarr %a" typ t

let rec list' pp p l =
  match l with
  | [] -> ()
  | e :: [] -> pp p e
  | e :: l -> fprintf p "%a;@ %a" pp e (list' pp) l

let list pp p l =
  fprintf p "@[<hov 1>[%a]@]" (list' pp) l

let coqNat p n =
  fprintf p "(%d)%%nat" (Camlcoq.Nat.to_int n)

let coqZ p z =
  match z with
  | Z.Zneg _ -> fprintf p "(%s)%%Z" (Z.to_string z)
  | _ -> fprintf p "(%s)%%Z" (Z.to_string z)

let coqP p n = fprintf p "%s%%positive" (Z.to_string (Z.Zpos n))

let ident p a =
  let s = try
            try ParserUtils.get_name a
            with ParserUtils.NotFoundName _ ->
              Hashtbl.find string_of_atom a
          with Not_found ->
            Printf.sprintf "__temp_%d" (P.to_int a) in
  fprintf p "S %a \"%s\"" coqP a s

let int p n = fprintf p "(Int.repr %a)" coqZ (Integers.Int.unsigned n)

let int64 p n = fprintf p "(Int64.repr %a)" coqZ (Integers.Int64.unsigned n)

let float prec emax p f =
  match f with
  | B754_zero s -> fprintf p "B754_zero %d%%Z %d%%Z %b" prec emax s
  | B754_infinity s -> fprintf p "B754_infinity %d%%Z %d%%Z %b" prec emax s
  | B754_nan (s, pl) -> fprintf p "B754_nan %d%%Z %d%%Z %b %a eq_refl"
                                  prec emax s coqP pl
  | B754_finite (s, m, e) -> fprintf p "B754_finite %d%%Z %d%%Z %b %a %a eq_refl"
                                       prec emax s coqP m coqZ e

let constant p c =
  match c with
  | Cbool b -> fprintf p "Cbool %b" b
  | Cint (sz, sg, n) -> fprintf p "Cint %s %s %a" (intsize sz) (signedness sg) int n
  | Cint64 (sg, n) -> fprintf p "Cint64 %s %a" (signedness sg) int64 n
  | Cfloat32 f -> fprintf p "Cfloat32 (%a)" (float 24 128) f
  | Cfloat64 f -> fprintf p "Cfloat64 (%a)" (float 53 1024) f

let rec expr p e =
  match e with
  | Econst c -> fprintf p "Econst (%a)" constant c
  | Eunop (op, k, e) -> fprintf p "@[<hov 2>Eunop %s %s (%a)@]" (unop op) (opkind k) expr e
  | Ebinop_arith (op, k, e1, e2) -> fprintf p "@[<hov 2>Ebinop_arith %s %s@ @[(%a)@]@ @[(%a)@]@]"
                                            (binop_arith op) (opkind k)
                                            expr e1 expr e2
  | Ebinop_cmp (op, k, e1, e2)   -> fprintf p "@[<hov 2>Ebinop_cmp %s %s@ @[(%a)@]@ @[(%a)@]@]"
                                            (binop_cmp op) (opkind k)
                                            expr e1 expr e2
  | Ebinop_cmpu (op, k, e1, e2)  -> fprintf p "@[<hov 2>Ebinop_cmpu %s %s@ (%a)@ (%a)@]"
                                            (binop_cmpu op) (opkind k)
                                            expr e1 expr e2
  | Ecast (e, t1, t2) -> fprintf p "@[<hov 2>Ecast (%a) %a %a@]" expr e typ t1 typ t2
  | Eacc x -> fprintf p "Eacc %a" syn_path x

and expr_list p l = list expr p l

and syn_path_elem p e =
  match e with
  | Scell l -> fprintf p "@[Scell %a@]" expr_list l

and syn_path_elem_list p l = list syn_path_elem p l

and syn_path p (i, l) =
  fprintf p "@[<hov 1>(%a,@ %a)@]" ident i syn_path_elem_list l


let syn_path_list p l = list syn_path p l

let rec stmt p s =
  match s with
  | Sskip -> fprintf p "Sskip"
  | Serror -> fprintf p "Serror"
  | Sexit n -> fprintf p "Sexit %a" coqNat n
  | Sreturn None -> fprintf p "Sreturn None"
  | Sreturn (Some e) -> fprintf p "Sreturn (Some (%a))" expr e
  | Sassign (x, e) -> fprintf p "@[<hov 2>Sassign %a@ (%a)@]" syn_path x expr e
  | Scall (None, idf, args) ->
    fprintf p "Scall None (%a) (@[%a@])" ident idf syn_path_list args
  | Scall (Some x, idf, args) ->
    fprintf p "Scall (Some (%a)) (%a) (@[%a@])" ident x ident idf syn_path_list args
  | Salloc i -> fprintf p "Salloc (%a)" ident i
  | Sfree i  -> fprintf p "Sfree (%a)" ident i
  | Sseq (s1, s2) -> fprintf p "@[Sseq (%a) (@,%a)@]" stmt s1 stmt s2
  | Sassert e -> fprintf p "Sassert (%a)" expr e
  | Sifthenelse (c, s1, s2) ->
    fprintf p "@[<hov 2>Sifthenelse@ @[(%a)@ (%a)@ (%a)@]@]"
            expr c stmt s1 stmt s2
  | Sloop s -> fprintf p "@[<hov 2>Sloop (%a)@]" stmt s
  | Sblock s -> fprintf p "@[<hov 2>Sblock (%a)]" stmt s

let ptree' (type a) ?(prim=fun (_: a) -> false) pp =
  let rec aux p (t: a PTree.tree') =
    let pp p e = if prim e then pp p e else fprintf p "(%a)" pp e in
    match t with
    | PTree.Node001 t        -> fprintf p "@[<hov 2>PTree.Node001@ (%a)@]" aux t
    | PTree.Node010 e        -> fprintf p "@[<hov 2>PTree.Node010@ %a@]" pp e
    | PTree.Node011 (e, t)   -> fprintf p "@[<hov 2>PTree.Node011@ %a@ (%a)@]" pp e aux t
    | PTree.Node100 t        -> fprintf p "@[<hov 2>PTree.Node100@ (%a)@]" aux t
    | PTree.Node101 (t1, t2) -> fprintf p "@[<hov 2>PTree.Node101@ (%a)@ (%a)@]"
                                        aux t1 aux t2
    | PTree.Node110 (t, e)   -> fprintf p "@[<hov 2>PTree.Node110@ (%a)@ %a@]" aux t pp e
    | PTree.Node111 (t1, e, t2) -> fprintf p "@[<hov 2>PTree.Node111@ (%a)@ %a@ (%a)@]"
                                          aux t1 pp e aux t2 in
  aux

let ptree (type a) ?(prim=fun (_: a) -> false) pp p t =
  match t with
  | PTree.Empty   -> fprintf p "PTree.Empty"
  | PTree.Nodes t -> fprintf p "PTree.Nodes (@[%a@])" (ptree' ~prim pp) t

let signature p s =
  fprintf p "{| @[sig_args := @[%a@];@ sig_res := %a@] |}"
          (list typ) s.sig_args typ s.sig_res

let prim_typ = function Tarr _ -> false | _ -> true

let ifunction p f =
  fprintf p "@[<hov 2>make_function@;%a@;(%a)@;(%a)@;(%a)@;(%a)@;(%a)@;(%a)@;(%a)@]"
          signature f.fn_sig
          (list ident) f.fn_params
          (list ident) f.fn_vars
          stmt f.fn_body
          (ptree ~prim:prim_typ typ) f.fn_tenv
          (ptree ~prim:(fun _ -> true) (list (list expr))) f.fn_szenv
          (ptree ~prim:(fun _ -> true) (list (list coqP))) f.fn_szenv'
          (ptree ~prim:(fun _ -> true) permission) f.fn_penv

let efunction p ef =
  fprintf p "@[<hov 2>make_external_function@;\"%s\"@;%a@;(%a)@;(%a)@;(%a)@;(%a)@]"
          (camlstring_of_coqstring ef.ef_name)
          signature ef.ef_sig
          (list ident) ef.ef_params
          (ptree ~prim:prim_typ typ) ef.ef_tenv
          (ptree ~prim:(fun _ -> true) (list (list expr))) ef.ef_szenv
          (ptree ~prim:(fun _ -> true) permission) ef.ef_penv

let function_definition p (i, fd) =
  match fd with
  | Internal f ->
    fprintf p "@[<hov 2>Definition %s := extract_res (@,%a@,) eq_refl.@]" (extern_atom i) ifunction f
  | External f ->
    fprintf p "@[<hov 2>Definition %s := extract_res (@,%a@,) eq_refl.@]" (extern_atom i) efunction f

let rec function_definitions p fds =
  match fds with
  | [] -> fprintf p ""
  | fd :: fds -> fprintf p "%a@.@.%a" function_definition fd function_definitions fds

let header = {|Require Import BinNums ZArith String List.
From compcert Require Import Integers Floats Maps.
From compcert Require Import Types Ops Syntax.

From Flocq Require Import Binary.

Import ListNotations.

Definition u8  := Tint I8  Unsigned.
Definition u16 := Tint I16 Unsigned.
Definition u32 := Tint I32 Unsigned.
Definition u64 := Tint64 Unsigned.
Definition i8  := Tint I8  Signed.
Definition i16 := Tint I16 Signed.
Definition i32 := Tint I32 Signed.
Definition i64 := Tint64 Signed.
Definition f32 := Tfloat F32.
Definition f64 := Tfloat F64.

Definition extract_res {A: Type} {x: A} (e: Errors.res A)
                       (H: e = Errors.OK x) : A := x.

Definition S (id: positive) (name: string) := id.
|}

let program p prog =
  let ifundef p (i, fd) = fprintf p "@[<hov 1>(%a, %s)@]" coqP i
                            (match fd with
                             | Internal _ -> "Internal " ^ extern_atom i
                             | External _ -> "External " ^ extern_atom i) in
  fprintf p "%s" header;
  fprintf p "%a@.@.@[<hov 2>Definition program :=@ extract_res (@,make_program@ (%a)@ (%a)@,) eq_refl@].@."
          function_definitions prog.prog_defs
          (list ifundef) prog.prog_defs
          coqP prog.prog_main

let destination : string option ref = ref None

let print_if_gen prog =
  match !destination with
  | None -> ()
  | Some f ->
      let oc = open_out f in
      program (formatter_of_out_channel oc) prog;
      close_out oc

let print_if prog = print_if_gen prog
