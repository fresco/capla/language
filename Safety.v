Require Import Utf8.
From mathcomp Require Import ssreflect ssrbool ssrfun ssrnat.
From mathcomp Require Import ssrZ zify.
(* From mathcomp Require Import ssrint. *)
Set Bullet Behavior "Strict Subproofs".
Unset SsrOldRewriteGoalsOrder.
Unset Strict Implicit.
Unset Printing Implicit Defensive.

Require Import BinNums BinInt BinPos.
Require Import Arith.
Require Import Lia.

Require Import String Ascii List.

Import ListNotations.

Require Import Integers Floats Maps Errors Coqlib.

Require Import BValues Ops Types Syntax Alias Typing.
Require Import BUtils PTreeaux Tactics ListUtils.
Require Import BEnv Validity SemPath.
Require Import SemanticsCommon SemanticsNonBlocking NBfacts.
Require Import Typing.
Require ExprSem.
Module ES := ExprSem.

Declare Scope capla_scope.

(* TODO: move *)
Lemma nth_error_Some {A: Type}:
  forall (l: list A) n,
    nth_error l n <> None <-> (n < length l)%nat.
Proof.
  intros; split.
  - by move/nth_error_Some/ltP.
  - by move/ltP/nth_error_Some.
Qed.

Lemma nth_error_None {A: Type}:
  forall (l: list A) n,
    nth_error l n = None <-> (length l <= n)%nat.
Proof.
  intros; split.
  - by move/nth_error_None/leP.
  - by move/leP/nth_error_None.
Qed.

Lemma nth_error_nth' {A: Type}:
  forall (l: list A) n d,
    (n < length l)%nat ->
    nth_error l n = Some (nth n l d).
Proof. by move=>> /ltP/(@nth_error_nth' A) => <-. Qed.

Inductive wt_value (e: env) (f: function) : typ -> list (list ident) -> value -> Prop :=
| wt_value_undef: wt_value e f Tvoid [] Vundef
| wt_value_bool: forall b, wt_value e f Tbool [] (Vbool b)
| wt_value_int: forall sz sg n, wt_value e f (Tint sz sg) [] (Vint n)
| wt_value_int64: forall sg n, wt_value e f (Tint64 sg) [] (Vint64 n)
| wt_value_float32: forall x, wt_value e f (Tfloat F32) [] (Vfloat32 x)
| wt_value_float64: forall x, wt_value e f (Tfloat F64) [] (Vfloat64 x)
| wt_value_arr: forall t lsz lvsz shape llsz lv vex
  (WTv:   Forall (fun v => wt_value e f t llsz v) lv)
  (WTvex: wt_value e f t llsz vex)
  (EVlsz: eval_identlist e f lsz lvsz)
  (SHAPE: natlist_of_Vint64 lvsz = Some shape)
  (SZLEN: (build_size shape <= length lv)%nat),
  wt_value e f (Tarr t) (lsz :: llsz) (Varr lv).

Definition wt_env (e: env) (f: function) :=
  forall i v,
    e!i = Some v ->
    exists t llsz,
      (fn_tenv f)!i = Some t /\
      (fn_szenv' f)!i = Some llsz /\
      wt_value e f t llsz v.

Definition defined_iset (s: iset) (e: env) :=
  forall i,
    s!i = Some tt ->
    exists v, e!i = Some v.

Notation "F & E |= e '=>' v" :=
  (eval_expr E F e v)
  (at level 70, E at level 70) : capla_scope.

Notation "F & E |= p '=>p' q" :=
  (eval_path E F p q)
  (at level 70, E at level 70) : capla_scope.

Notation "F & E |= e '=>l' v" :=
  (eval_exprlist E F e v)
  (at level 70, E at level 70) : capla_scope.

Notation "F & E |= p '=>lp' q" :=
  (eval_path_list E F p q)
  (at level 70, E at level 70) : capla_scope.

(* Notation "[| x |] ( v )" :=
  (wt_value (fst x) (fst (snd x)) (snd (snd x)) v) (at level 100) : capla_scope. *)

(* Open Scope capla_scope.
Check (∀ e f exp v, e | f |= exp => Some v). *)

Notation "v ∈ ⟦ E , F , t , l ⟧" :=
  (wt_value E F t l v) (at level 100) : capla_scope.

Notation "F |- e ∈ t ¤ n" :=
  (type_expression F e = OK (t, n))
  (at level 70) : capla_scope.

Notation "F |- p '∈p' t ¤ n" :=
  (type_syn_path F p = OK (t, n))
  (at level 70) : capla_scope.

Notation "F |- e '∈l' t ¤ n" :=
  (type_expression_list F e = OK (t, n))
  (at level 70) : capla_scope.

Notation "F |- p '∈lp' t ¤ n" :=
  (type_syn_path_list F p = OK (t, n))
  (at level 70) : capla_scope.

Open Scope capla_scope.

Scheme eval_expr_ind2 := Minimality for eval_expr Sort Prop
  with eval_path_elem_list_ind2 := Minimality for eval_path_elem_list Sort Prop
  with eval_path_ind2 := Minimality for eval_path Sort Prop
  with eval_exprlist_ind2 := Minimality for eval_exprlist Sort Prop.
Combined Scheme eval_expr_path_ind2
  from eval_expr_ind2, eval_path_elem_list_ind2, eval_path_ind2, eval_exprlist_ind2.

Lemma iset_union_get:
  forall n1 n2 i,
    (PTree_union n1 n2)!i = Some tt ->
    n1!i = Some tt \/ n2!i = Some tt.
Proof.
  move=>>; rewrite/PTree_union PTree.gcombine //.
  do 2 case PTree.get => [[]|]; tauto.
Qed.

Lemma defined_iset_union:
  ∀ n1 n2 e,
    defined_iset n1 e ->
    defined_iset n2 e ->
    defined_iset (PTree_union n1 n2) e.
Proof. move=>> H1 H2 > /iset_union_get []; auto. Qed.

Lemma type_syn_path_get_tenv_syn_path f:
  ∀ p t ne,
    f |- p ∈p t ¤ ne ->
    get_tenv_syn_path (fd_tenv f) p = Some t.
Proof.
  case=> i l > /=.
  case (fd_tenv f)!i => t0 //=. case (fd_szenv f)!i => llsz0 //=.
  set llsz0' := match f with Internal f => _ | External _ => _ end.
  clearbody llsz0'.
  case T: type_syn_path_elem_list => [[t ne]|] > //= [<- _].
  elim: l t0 llsz0 llsz0' t ne T => /=.
  - by move=>> _ _ > [<-].
  - move=> [idx] > IH [] // t0 [] // lsz0 llsz0 [|lsz0' llsz0'] >.
    all: case T: type_syn_path_elem => //=.
    all: case Tl: type_syn_path_elem_list => [[]|] > //= [<- _].
    all: by apply: IH Tl.
Qed.

Lemma fd_szenv_tenv_str_get_tenv_syn_path f:
  ∀ p t,
    get_tenv_syn_path (fd_tenv f) p = Some t ->
    ∃ llsz,
      get_szenv_syn_path (fd_szenv f) p = Some llsz /\
      match_type_llsz t llsz.
Proof.
  case=> i l t /=. case Ti: (fd_tenv f)!i => [t0|] //= Tl.
  have [llsz0 Si]:= proj1 (fd_tenv_fd_szenv f i) (ex_intro _ _ Ti).
  rewrite Si.
  have:= fd_szenv_tenv_str f _ _ _ Ti Si.
  elim: l t0 llsz0 Tl {Si Ti} => /=.
  - move=>> [<-]; eauto.
  - move=> [idx] > IH [] //= t0 [] // _ > /IH/[apply] //.
Qed.

Lemma fd_szenv_tenv_str_get_tenv_path f:
  ∀ p t,
    get_tenv_path (fd_tenv f) p = Some t ->
    ∃ llsz,
      get_szenv_path (fd_szenv f) p = Some llsz /\
      match_type_llsz t llsz.
Proof.
  case=> i l t /=. case Ti: (fd_tenv f)!i => [t0|] //= Tl.
  have [llsz0 Si]:= proj1 (fd_tenv_fd_szenv f i) (ex_intro _ _ Ti).
  rewrite Si.
  have:= fd_szenv_tenv_str f _ _ _ Ti Si.
  elim: l t0 llsz0 Tl {Si Ti} => /=.
  - move=>> [<-]; eauto.
  - move=> [idx] > IH [] //= t0 [] // _ > /IH/[apply] //.
Qed.

Lemma fn_szenv_szenv'_str_get_szenv_syn_path f:
  ∀ p llsz,
    get_szenv_syn_path (fn_szenv f) p = Some llsz ->
    ∃ llsz',
      get_szenv_syn_path (fn_szenv' f) p = Some llsz' /\
      Forall2 (λ lsz lsz',
        Datatypes.length lsz = Datatypes.length lsz') llsz llsz'.
Proof.
  case=> i l llsz /=. case Si: (fn_szenv f)!i => [llsz0|] //= Sl.
  have [llsz0' Si']:= proj1 (fn_szenv_fn_szenv' f i) (ex_intro _ _ Si).
  rewrite Si'.
  have:= fn_szenv_szenv'_str f _ _ _ Si Si'.
  elim: l llsz0 llsz0' Sl {Si Si'} => /=.
  - move=>> [<-]; eauto.
  - move=> [idx] > IH [] // > + H; inv H; move/IH: H4 => /[apply] //.
Qed.

Lemma fn_szenv_szenv'_str_get_szenv_path f:
  ∀ p llsz,
    get_szenv_path (fn_szenv f) p = Some llsz ->
    ∃ llsz',
      get_szenv_path (fn_szenv' f) p = Some llsz' /\
      Forall2 (λ lsz lsz',
        Datatypes.length lsz = Datatypes.length lsz') llsz llsz'.
Proof.
  case=> i l llsz /=. case Si: (fn_szenv f)!i => [llsz0|] //= Sl.
  have [llsz0' Si']:= proj1 (fn_szenv_fn_szenv' f i) (ex_intro _ _ Si).
  rewrite Si'.
  have:= fn_szenv_szenv'_str f _ _ _ Si Si'.
  elim: l llsz0 llsz0' Sl {Si Si'} => /=.
  - move=>> [<-]; eauto.
  - move=> [idx] > IH [] // > + H; inv H; move/IH: H4 => /[apply] //.
Qed.

Corollary type_syn_path_get_szenv_syn_path (f: function):
  ∀ p t ne,
    f |- p ∈p t ¤ ne ->
    ∃ l, get_szenv_syn_path (fn_szenv' f) p = Some l.
Proof.
  intros * T.
  have:= type_syn_path_get_tenv_syn_path _ _ _ _ T.
  case/fd_szenv_tenv_str_get_tenv_syn_path =>> /= [+ _].
  case/fn_szenv_szenv'_str_get_szenv_syn_path =>> [-> _] /=; by eexists.
Qed.

Lemma type_syn_path_list_get_szenv_syn_path_list (f: function):
  ∀ lp lt ne,
    f |- lp ∈lp lt ¤ ne ->
    ∃ l, get_szenv_syn_path_list (fn_szenv' f) lp = Some l.
Proof.
  elim=> [|p lp IH] > /=.
  - by exists [].
  - case T: type_syn_path => [[]|] //=; case Tl: type_syn_path_list => [[]|] //= _.
    have [l -> /=]:= IH _ _ Tl.
    have [> ->]:= type_syn_path_get_szenv_syn_path _ _ _ _ T.
    by eexists.
Qed.

Theorem subject_reduction e f:
  wt_env e f ->
  (∀ exp v,
    f & e |= exp => v ->
    match v with
    | Some v =>
      ∀ t ne,
        (f |- exp ∈ t ¤ ne) ->
        v ∈ ⟦ e, f, t, [] ⟧ /\ defined_iset ne e
    | _ => True
    end) /\
  (∀ lsyn llsz0' lsem,
    eval_path_elem_list e f lsyn llsz0' lsem ->
    match lsem with
    | Some lsem =>
      ∀ t0 t ne llsz0 (llsz': list (list ident)) v0 v,
        v0 ∈ ⟦ e, f, t0, llsz0' ⟧ ->
        type_syn_path_elem_list f t0 llsz0 llsz0' lsyn = OK (t, ne) ->
        get_size_syn_path lsyn llsz0' = Some llsz' ->
        get_value_path lsem v0 = Some v ->
        v ∈ ⟦ e, f, t, llsz' ⟧ /\ defined_iset ne e
    | _ => True
    end) /\
  (∀ p p',
    f & e |= p =>p p' ->
    match p' with
    | Some p' =>
      ∀ t ne llsz v,
        f |- p ∈p t ¤ ne ->
        get_szenv_syn_path (fn_szenv' f) p = Some llsz ->
        get_env_path e p' = Some v ->
        v ∈ ⟦ e, f, t, llsz ⟧ /\ defined_iset ne e
    | _ => True
    end) /\
  (∀ le lv,
    f & e |= le =>l lv ->
    match lv with
    | Some lv =>
      ∀ lt ne,
        f |- le ∈l lt ¤ ne ->
        Forall2 (fun t v => v ∈ ⟦ e, f, t, [] ⟧) lt lv /\
        defined_iset ne e
    | _ => True
    end).
Proof.
  move=> WT.
  eapply eval_expr_path_ind2 => //.
  - move=>> E; inv E; split; by [constructor|].
  - move=>> E; inv E; split; by [constructor|].
  - move=>> E; inv E; split; by [constructor|].
  - move=>> E; inv E; split; by [constructor|].
  - move=>> E; inv E; split; by [constructor|].
  - move=> ? t1 t2 > E1 HE1 CAST > T; inv T; move: H0.
    case T1: type_expression => [[]|] > //=.
    move/HE1: T1 CAST {HE1} => [T D]; inv T => //=.
    all: case: t1 => [||?[]|[]|[]|] //; case: t2 => [||?[]|[]|[]|] > //=.
    all: destruct_match_goal => - [<-] [<- <-]; split; by [econstructor|].
  - move=> op k > E1 HE1 OP > T; inv T; move: H0.
    case: op OP; case T1: type_expression => [[]|] > //=.
    all: move/HE1: T1 {HE1} => [T D]; inv T => //=.
    all: case: k => //= - [<-]; try case: sz; try case: sg.
    all: case=> <- <-; split; by [econstructor|].
  - move=> op k > E1 HE1 E2 HE2 OP > T; inv T; move: H0.
    case T1: type_expression => [[]|] //=. case T2: type_expression => [[]|] //=.
    move/HE1: T1 {HE1} => [+ D1]. move/HE2: T2 {HE2} => [+ D2].
    case: op OP; case: k; case typ_reflect => [<-|_] //.
    all: move=> + V2 V1; inv V1 => //; inv V2 => //.
    all: try case: sz; try case sg; try case: sz0; try case sg0.
    all: have D := defined_iset_union _ _ _ D1 D2 => //=.
    all: rewrite/divs/divu/mods/modu/divs64/divu64/mods64/modu64.
    all: destruct_match_goal => - [<-] [<- <-].
    all: split=> //; by [constructor|].
  - move=> op k > E1 HE1 E2 HE2 OP > T; inv T; move: H0.
    case T1: type_expression => [[]|] //=. case T2: type_expression => [[]|] //=.
    move/HE1: T1 {HE1} => [+ D1]. move/HE2: T2 {HE2} => [+ D2].
    case typ_reflect => [<-|] //; case: op OP; case: k.
    all: move=> + V2 V1; inv V1 => //; inv V2 => //; try case: sg => //.
    all: have D := defined_iset_union _ _ _ D1 D2 => //= - [<-] [<- <-].
    all: split=> //; by [constructor|].
  - move=> op k > E1 HE1 E2 HE2 OP > T; inv T; move: H0.
    case T1: type_expression => [[]|] //=. case T2: type_expression => [[]|] //=.
    move/HE1: T1 {HE1} => [+ D1]. move/HE2: T2 {HE2} => [+ D2].
    case typ_reflect => [<-|] //; case: op OP; case: k.
    all: move=> + V2 V1; inv V1 => //; inv V2 => //; try case: sg => //.
    all: have D := defined_iset_union _ _ _ D1 D2 => //= - [<-] [<- <-].
    all: split=> //; by [constructor|].
  - move=> p p' > EP HP EP' PRIM >.
    rewrite type_Eacc; case T: type_syn_path => [[t]|] > //=.
    case PRIM': primitive_type =>> //= [<- <-].
    have: get_szenv_syn_path (fn_szenv' f) p = Some [].
    { have:= type_syn_path_get_tenv_syn_path _ _ _ _ T.
      case/fd_szenv_tenv_str_get_tenv_syn_path => l [].
      case/fn_szenv_szenv'_str_get_szenv_syn_path => l' [].
      case: t PRIM' {T} => // > _; case: l => // + H; by inv H. }
    by move/HP: T EP'; do 2 move/[apply].
  - move=> i > ? /= [<- <-] [<-] [<-]. easy.
  - move=>> Elsz Nlsz Elidx HElidx Bidx VIDX Ellsz HEllsz > V0.
    unfold type_syn_path_elem_list; fold (type_syn_path_elem_list f).
    do 3 destruct_match_goal.
    rewrite type_Scell; case Nat.eqb_spec => // LEN.
    case T: type_expression_list => [[]|] //=.
    case (list_reflect _ typ_reflect) => //= X; rewrite X {X} in T.
    case Tl: type_syn_path_elem_list => [[? ne]|] //= - [<- <-].
    destruct_match_goal. case N: nth_error => //=.
    inv V0. move/Forall_nth: WTv => X; exploit X.
    { apply List.nth_error_Some; by rewrite -> N. }
    rewrite -> (nth_error_nth _ _ Vundef N).
    move/HElidx: T {HElidx Elidx} => [T1 D1].
    move/HEllsz: Tl {HEllsz Ellsz}; do 3 move/[apply]; case=> T2 D2.
    split=> //; apply defined_iset_union=> //.
    elim: EVlsz ne D2 =>> //= H _ _ IH > D; apply IH =>>; case case_set => [<-|]; eauto.
  - move=> i > Si' El Hl > /=.
    case Ti: (fn_tenv f)!i => //=. case Si: (fn_szenv f)!i => //=.
    rewrite Si'. case T: type_syn_path_elem_list => [[]|] > //= [<- <-].
    case Ei: e!i => //=.
    have [> [> [+ []]]]:= WT _ _ Ei; rewrite Ti Si' => - [<-] [<-].
    move/Hl: T {Hl El}; do 3 move/[apply].
    case; split=> // >; case case_set => [<-|]; eauto.
  - move=>> /= [<- <-]; by split.
  - move=>> _ HE _ HEl > /=.
    case T: type_expression => [[]|] //=.
    case Tl: type_expression_list => [[]|] > //= [<- <-].
    case/HE: T => V D. case/HEl: Tl => Vl Dl; split.
    by constructor. by apply defined_iset_union.
Qed.

Corollary subject_reduction_expr e f:
  ∀ exp v t ne,
    wt_env e f ->
    f & e |= exp => Some v ->
    f |- exp ∈ t ¤ ne ->
    v ∈ ⟦ e, f, t, [] ⟧ /\ defined_iset ne e.
Proof. move=>> /subject_reduction/proj1/[apply]; eauto. Qed.

Corollary subject_reduction_path_elem_list e f:
  ∀ lsyn llsz0' lsem t0 t ne llsz0 llsz' v0 v,
    wt_env e f ->
    eval_path_elem_list e f lsyn llsz0' (Some lsem) ->
    v0 ∈ ⟦ e, f, t0, llsz0' ⟧ ->
    type_syn_path_elem_list f t0 llsz0 llsz0' lsyn = OK (t, ne) ->
    get_size_syn_path lsyn llsz0' = Some llsz' ->
    get_value_path lsem v0 = Some v ->
    v ∈ ⟦ e, f, t, llsz' ⟧ /\ defined_iset ne e.
Proof. move=>> /subject_reduction/proj2/proj1/[apply]; eauto. Qed.

Corollary subject_reduction_path e f:
  ∀ p p' t ne llsz v,
    wt_env e f ->
    f & e |= p =>p Some p' ->
    f |- p ∈p t ¤ ne ->
    get_szenv_syn_path (fn_szenv' f) p = Some llsz ->
    get_env_path e p' = Some v ->
    v ∈ ⟦ e, f, t, llsz ⟧ /\ defined_iset ne e.
Proof. move=>> /subject_reduction/proj2/proj2/proj1/[apply]; eauto. Qed.

Corollary subject_reduction_exprlist e f:
  ∀ le lv lt ne,
    wt_env e f ->
    f & e |= le =>l Some lv ->
    f |- le ∈l lt ¤ ne ->
    Forall2 (fun t v => v ∈ ⟦ e, f, t, [] ⟧) lt lv /\
    defined_iset ne e.
Proof. move=>> /subject_reduction/proj2/proj2/proj2/[apply]; eauto. Qed.

Corollary subject_reduction_full_path e f:
  ∀ p p' t ne v,
    wt_env e f ->
    type_write_full_syn_path f p = OK (t, ne) ->
    eval_full_path e f p (Some p') ->
    get_env_path e p' = Some v ->
    v ∈ ⟦ e, f, t, [] ⟧ /\ defined_iset ne e.
Proof.
  case=> i l p' t ne v /= WT.
  case Ti: (fn_tenv f)!i => [t0|] //=.
  case Si: (fn_szenv f)!i => [llsz0|] //=.
  case Nat.eqb_spec => //= LEN.
  set llsz0' := match (fn_szenv' f)!i with Some _ => _ | None => _ end.
  case T: type_syn_path_elem_list => [[? ne']|] > //= H E V.
  exploit (subject_reduction_path _ _ (i, l) p' t (PTree.set i tt ne') [] v WT).
  - by apply: eval_full_path_eval_path.
  - rewrite /= Ti Si /= T /=; move: H; case l => [|_ _] [<-] //.
  - simpl; have [x Si']:= proj1 (fn_szenv_fn_szenv' f _) (ex_intro _ _ Si) => /=.
    have:= fn_szenv_szenv'_str f _ _ _ Si Si'; rewrite Si' {Si Si'} /=.
    elim: l llsz0 x LEN {T H E} => /=.
    + case=> // > _ T; by inv T.
    + move=> [?] > IH [] //= > /eq_add_S LEN T; inv T; by apply: IH LEN H3.
  - assumption.
  - move: H; case l => [|_ _] [_ <-] // [X D]; split=> //.
    move=> j; have:= D j; case case_set => [<-|] // /(_ eq_refl) //.
Qed.

Corollary subject_reduction_path_list e f:
  ∀ lp lp' lt ne lllsz lv,
    wt_env e f ->
    f & e |= lp =>lp Some lp' ->
    f |- lp ∈lp lt ¤ ne ->
    get_szenv_syn_path_list (fn_szenv' f) lp = Some lllsz ->
    get_env_path_list e lp' = Some lv ->
    Forall3 (fun t llsz v => v ∈ ⟦ e, f, t, llsz ⟧) lt lllsz lv /\ defined_iset ne e.
Proof.
  move=> ++++++ WT; elim=> /=.
  - move=>> E [<- <-] [<-]; inv E => /= - [<-]; split; by [constructor|].
  - move=>> IH > E.
    case T: type_syn_path => [[]|] //=.
    case Tl: type_syn_path_list => [[]|] //= - [<- <-].
    case S: get_szenv_syn_path => //=.
    case Sl: get_szenv_syn_path_list =>> //= [<-].
    inv E => /=; case E: get_env_path => //=.
    case El: get_env_path_list =>> //= [<-].
    have [V D]:= subject_reduction_path _ _ _ _ _ _ _ _ WT H2 T S E.
    have [Vl Dl]:= IH _ _ _ _ _ H3 Tl Sl El.
    have:= defined_iset_union _ _ _ D Dl; split=> //; by constructor.
Qed.

Lemma build_index_Some:
  forall lvidx shape,
    all_Vint64 lvidx ->
    length lvidx = length shape ->
    exists idx, build_index lvidx shape = Some idx.
Proof.
  move=> lvidx shape. rewrite/build_index. elim/list_ind: lvidx shape 0%nat => /=.
  - case=>> //=. by eexists.
  - move=> [] // ? lvidx IHlvidx [|? shape] > //= + /eq_add_S. by apply: IHlvidx.
Qed.

Fixpoint size_expr (e: expr) :=
  match e with
  | Econst _ => 1
  | Ecast e _ _ | Eunop _ _ e => S (size_expr e)
  | Ebinop_arith _ _ e1 e2
  | Ebinop_cmp _ _ e1 e2 | Ebinop_cmpu _ _ e1 e2 => size_expr e1 + size_expr e2
  | Eacc (_, ls) => S (fold_right (fun s n => n + size_syn_path_elem s) 0 ls)
  end%nat
with size_syn_path_elem (s: syn_path_elem) :=
  match s with
  | Scell lidx =>
    S (fold_right (fun e n => n + S (size_expr e)) 0 lidx)
  end%nat.

Definition size_path (p: syn_path) :=
  (fold_right (fun s n => n + size_syn_path_elem s) 0 (snd p))%nat.

Definition size_expr_list (le: list expr) :=
  (fold_right (fun e n => n + S (size_expr e)) 0 le)%nat.

Lemma size_expr_pos: ∀ e, (size_expr e >= 1)%nat.
Proof. elim=> //; by [case|move=>> /=; lia]. Qed.

Lemma defined_iset_union_inv:
  ∀ n1 n2 e,
    defined_iset (PTree_union n1 n2) e ->
    defined_iset n1 e /\ defined_iset n2 e.
Proof.
  move=> n1 n2 > H; split=> i; have:= H i; rewrite/PTree_union PTree.gcombine //.
  - move/[swap] => ->; case n2!i => [_|] /(_ eq_refl) //.
  - move/[swap] => ->; case n1!i => [[]|] /(_ eq_refl) //.
Qed.

Lemma incl_union_unit_l {A: Type} (t1 t2: PTree.t A):
  incl (fun _ _ => true) t1 (PTree_union t1 t2).
Proof.
  apply PTree_Properties.for_all_correct=> i x.
  rewrite PTree.gcombine // => ->. by case t2!i.
Qed.

Lemma incl_union_unit_r {A: Type} (t1 t2: PTree.t A):
  incl (fun _ _ => true) t2 (PTree_union t1 t2).
Proof.
  apply PTree_Properties.for_all_correct=> i x.
  rewrite PTree.gcombine // => ->. by case t1!i.
Qed.

Lemma unit_reflect:
  forall (x y: unit), reflect (x = y) ((fun _ _ => true) x y).
Proof. by move=> [] []; apply ReflectT. Qed.

Lemma PTree_union_empty_l {A: Type}:
  forall (t: PTree.t A),
    PTree_union (PTree.empty A) t = t.
Proof.
  move=> t. apply PTree.extensionality => i.
  rewrite PTree.gcombine //=. by case t!i.
Qed.

Lemma PTree_union_empty_r {A: Type}:
  forall (t: PTree.t A),
    PTree_union t (PTree.empty A) = t.
Proof.
  move=> t. apply PTree.extensionality => i.
  rewrite PTree.gcombine //=. by case t!i.
Qed.

Lemma union_idempotent_unit {A: Type}:
  forall (t: PTree.t A), PTree_union t t = t.
Proof.
  move=> t. apply PTree.extensionality =>>.
  rewrite PTree.gcombine //. by case t!_.
Qed.

Lemma incl_union_unit_proj1 (t1 t2 t: PTree.t unit):
    incl (fun _ _ => true) (PTree_union t1 t2) t ->
    incl (fun _ _ => true) t1 t.
Proof.
  move=> H. apply PTree_Properties.for_all_correct => i [] X.
  have: (PTree_union t1 t2)!i = Some tt.
  { rewrite PTree.gcombine // X. by case t2!i. }
  by move/(proj1 (PTree_Properties.for_all_correct _ _) H).
Qed.

Lemma incl_union_unit_proj2 (t1 t2 t: PTree.t unit):
    incl (fun _ _ => true) (PTree_union t1 t2) t ->
    incl (fun _ _ => true) t2 t.
Proof.
  move=> H. apply PTree_Properties.for_all_correct => i [] X.
  have: (PTree_union t1 t2)!i = Some tt.
  { rewrite PTree.gcombine // X. by case t1!i => [[]|]. }
  by move/(proj1 (PTree_Properties.for_all_correct _ _) H).
Qed.

Lemma union_assoc_unit (t1 t2 t3: PTree.t unit):
  PTree_union (PTree_union t1 t2) t3 = PTree_union t1 (PTree_union t2 t3).
Proof.
  apply PTree.extensionality => i. rewrite !PTree.gcombine //.
  case t1!i => [[]|]; case t2!i => [[]|]; case t3!i => [[]|] //.
Qed.

Lemma union_comm_unit (t1 t2: PTree.t unit):
  PTree_union t1 t2 = PTree_union t2 t1.
Proof.
  apply PTree.extensionality => i. rewrite!PTree.gcombine //.
  case t1!i => [[]|]; case t2!i => [[]|] //.
Qed.

Theorem eval_path_get_env_path f e:
  ∀ p p' t ne,
    wt_env e f ->
    f |- p ∈p t ¤ ne ->
    defined_iset ne e ->
    f & e |= p =>p Some p' ->
    ∃ v, get_env_path e p' = Some v.
Proof.
  case=> i l > WT /=.
  case Ti: (fn_tenv f)!i => [t0|] //=; case Si: (fn_szenv f)!i => [llsz|] //=.
  have [llsz' Si']:= proj1 (fn_szenv_fn_szenv' f i) (ex_intro _ _ Si).
  rewrite Si'; case Tl: type_syn_path_elem_list => [[t ne]|] //= [_ <-].
  move/(_ i); rewrite PTree.gss => /(_ eq_refl) [v0 Ei].
  intro H; inv H; move: H3; rewrite Si'; intros [= <-]; rename H4 into El.
  rewrite /= Ei /=.
  have:= WT _ _ Ei; rewrite Ti Si' => - [_] [_] [[<-]] [[<-]].
  elim: l t0 llsz llsz' v0 sempl t ne Tl El {Ei Ti Si Si'}.
  - move=>> _ H; inv H => /=; eauto.
  - case=> lidx l IH [] // t0 [//|lsz llsz] [//|lsz' llsz'] v0 > + H; inv H.
    cbn -[type_syn_path_elem]; rewrite type_Scell.
    case Nat.eqb_spec => [LEN|] //; case Tlidx: type_expression_list => [[]|] //=.
    case (list_reflect _ typ_reflect) => //= T; rewrite T {T} in Tlidx.
    case Tl: type_syn_path_elem_list => [[]|] //= _.
    intro H; inv H.
    have:= eval_identlist_deterministic _ _ _ _ _ H5 EVlsz; intros <-; clear EVlsz.
    move: SHAPE; rewrite H6; intros [= <-].
    have Hidx: (idx < length lv)%coq_nat.
    { have:= valid_index_product _ _ _ _ H6 H8 H9; lia. }
    have:= proj1 (Forall_nth _ _) WTv idx Vundef Hidx.
    have:= proj2 (List.nth_error_Some lv idx) Hidx; case N: nth_error => [v0|] //= _.
    rewrite (nth_error_nth _ _ Vundef N).
    apply: IH; eassumption.
Qed.

Theorem eval_path_list_get_env_path_list e f:
  ∀ lp lt ne lp',
    wt_env e f ->
    f & e |= lp =>lp Some lp' ->
    f |- lp ∈lp lt ¤ ne ->
    defined_iset ne e ->
    ∃ lv, get_env_path_list e lp' = Some lv.
Proof.
  move=> ++++ WT; elim=> [|p lp IH] > /=.
  - inv 1; by exists [].
  - inv 1 => /=.
    case T: type_syn_path => [[]|] //=.
    case Tl: type_syn_path_list => [[]|] > //= [_ <-].
    case/defined_iset_union_inv => D1 D2.
    have [> ->]:= eval_path_get_env_path _ _ _ _ _ _ WT T D1 H3.
    have [> ->]:= IH _ _ _ H4 Tl D2. by eexists.
Qed.

Lemma type_expression_list_length f:
  ∀ le lt ne,
    f |- le ∈l lt ¤ ne ->
    length le = length lt.
Proof.
  elim=> [> [<-] //| > +] //= >; case type_expression => [[]|] > //=.
  case type_expression_list => [[]|] > //= /(_ _ _ eq_refl) -> [<-] //.
Qed.

Theorem progress_expr_path e f:
  wt_env e f ->
  ∀ n,
    (∀ exp t ne,
      (size_expr exp <= n)%nat ->
      f |- exp ∈ t ¤ ne ->
      defined_iset ne e ->
      ∃ r, f & e |= exp => r) /\
    (∀ p t ne,
      (size_path p <= n)%nat ->
      f |- p ∈p t ¤ ne ->
      defined_iset ne e ->
      ∃ r, f & e |= p =>p r) /\
    (∀ le lt ne,
      (size_expr_list le <= n)%nat ->
      f |- le ∈l lt ¤ ne ->
      defined_iset ne e ->
      ∃ r, f & e |= le =>l r).
Proof.
  intro WT. elim.
  { repeat split.
    - move=> exp; have:= size_expr_pos exp; lia.
    - case=> i [|[]]; only 2: (rewrite/size_path /=; lia).
      move=>> /=; case Ti: (fn_tenv _)!i =>> //=; case Si: (fn_szenv _)!i => //=.
      have [> Si']:= proj1 (fn_szenv_fn_szenv' f _) (ex_intro _ _ Si).
      move=> _ [_ <-] /(_ i); rewrite PTree.gss => /(_ eq_refl) [v Ei].
      eexists; econstructor; by [eassumption|econstructor].
    - case=> [|exp]; only 2: (have:= size_expr_pos exp; rewrite/size_expr_list /=; lia).
      intros; eexists; econstructor. }
  { move=> n [IHexp [IHpath IHexprlist]]. repeat split.
    - case.
      + move=> p >; rewrite type_Eacc /= => SZ.
        case T: type_syn_path => [[t]|] //=; case P: primitive_type =>> // [_ <-] D.
        exploit (fun X => IHpath p _ _ X T D).
        { case: p SZ {T} =>>; rewrite/size_path/=; lia. }
        case=> [[]] > Ep.
        * have [v V]:= eval_path_get_env_path _ _ _ _ _ _ WT T D Ep.
          eexists; eapply eval_Access. eassumption. eassumption.
          have:= type_syn_path_get_tenv_syn_path _ _ _ _ T.
          case/fd_szenv_tenv_str_get_tenv_syn_path => llsz [+ _].
          case/fn_szenv_szenv'_str_get_szenv_syn_path => llsz' [Sp _].
          have [+ _]:= subject_reduction_path _ _ _ _ _ _ _ _ WT Ep T Sp V.
          case: t P {T} =>> // _ H; by inv H.
        * exists None; by apply eval_Access_ERR.
      + case=>>; eexists; econstructor.
      + move=> ?? t1 t2 > /= SZ; case T: type_expression => [[t]|] //=.
        have:= IHexp _ _ _ SZ T.
        have:= subject_reduction_expr _ _ _ _ _ _ WT _ T => {T}.
        case: t => [||? []|[]|[]|] //; case: t1 => [||? []|[]|[]|] > //.
        all: case typ_reflect => // -> ++ [_ <-] => + /[apply] => /[swap].
        all: case=> - [] > E T;
              [case/T: (E) => H _; inv H
              |eexists; eapply eval_Cast_ERR_val; eassumption].
        all: try (eexists; eapply eval_Cast; eassumption || reflexivity).
        all: move: E; (set (v := Vfloat32 _) || set (v := Vfloat64 _)) => E.
        all: match goal with
             | |- ∃ _, _ & _ |= Ecast _ ?t1 ?t2 => _ =>
              case S: (sem_cast v t1 t2); eexists;
              [eapply eval_Cast|eapply eval_Cast_ERR];
              unfold v; eassumption || constructor || reflexivity
             end.
      + case=> k > /= SZ; case T: type_expression => [[]|] //=.
        all: have:= IHexp _ _ _ SZ T.
        all: repeat destruct_match_goal.
        all: move=> + [_ <-] => /[apply].
        all: try by case: k Heq.
        all: case=> - [] > E;
              [have [H _]:= subject_reduction_expr _ _ _ _ _ _ WT E T; inv H;
               eexists; eapply eval_Unop; try (eassumption || reflexivity)
               |eexists; apply eval_Unop_ERR; eassumption];
              by case: k Heq.
      + move=> op k e1 e2 > /= SZ;
        case T1: type_expression => [[t1 n1]|] //=;
        case T2: type_expression => [[t2 n2]|] //=.
        case: op; (case typ_reflect; [intros <-|intros NEQ] => //); case: k => //.
        all: repeat destruct_match_goal; intros [= _ <-] D.
        all: have [D1 D2]:= defined_iset_union_inv _ _ _ D.
        all: have S1 := size_expr_pos e1; have S2 := size_expr_pos e2.
        all: exploit (fun X => IHexp _ _ _ X T1 D1); [lia|].
        all: case=> [[]]; [|eexists; apply eval_Binop_arith_ERR_val1; eassumption].
        all: move=> v1 E1; have [V1 _]:= subject_reduction_expr _ _ _ _ _ _ WT E1 T1.
        all: exploit (fun X => IHexp _ _ _ X T2 D2); [lia|].
        all: case=> [[]]; [|eexists; eapply eval_Binop_arith_ERR_val2; eassumption].
        all: move=> v2 E2; have [V2 _]:= subject_reduction_expr _ _ _ _ _ _ WT E2 T2.
        all: match goal with
             | |- ∃ _, _ & _ |= Ebinop_arith ?op ?k _ _ => _ =>
               case S: (sem_binarith_operation op k v1 v2);
               [eexists; eapply eval_Binop_arith; try eassumption
               |eexists; eapply eval_Binop_arith_ERR; by [eassumption|inv V1; inv V2]]
             end.
      + move=> op k e1 e2 > /= SZ;
        case T1: type_expression => [[t1 n1]|] //=;
        case T2: type_expression => [[t2 n2]|] //=.
        case: op; (case typ_reflect; [intros <-|intros NEQ] => //); case: k => //.
        all: repeat destruct_match_goal; intros [= _ <-] D.
        all: have [D1 D2]:= defined_iset_union_inv _ _ _ D.
        all: have S1 := size_expr_pos e1; have S2 := size_expr_pos e2.
        all: exploit (fun X => IHexp _ _ _ X T1 D1); [lia|].
        all: case=> [[]]; [|eexists; apply eval_Binop_cmp_ERR_val1; eassumption].
        all: move=> v1 E1; have [V1 _]:= subject_reduction_expr _ _ _ _ _ _ WT E1 T1.
        all: exploit (fun X => IHexp _ _ _ X T2 D2); [lia|].
        all: case=> [[]]; [|eexists; eapply eval_Binop_cmp_ERR_val2; eassumption].
        all: move=> v2 E2; have [V2 _]:= subject_reduction_expr _ _ _ _ _ _ WT E2 T2.
        all: inv V1; inv V2; eexists; eapply eval_Binop_cmp; by [eassumption|].
      + move=> op k e1 e2 > /= SZ;
        case T1: type_expression => [[t1 n1]|] //=;
        case T2: type_expression => [[t2 n2]|] //=.
        case: op; (case typ_reflect; [intros <-|intros NEQ] => //); case: k => //.
        all: repeat destruct_match_goal; intros [= _ <-] D.
        all: have [D1 D2]:= defined_iset_union_inv _ _ _ D.
        all: have S1 := size_expr_pos e1; have S2 := size_expr_pos e2.
        all: exploit (fun X => IHexp _ _ _ X T1 D1); [lia|].
        all: case=> [[]]; [|eexists; apply eval_Binop_cmpu_ERR_val1; eassumption].
        all: move=> v1 E1; have [V1 _]:= subject_reduction_expr _ _ _ _ _ _ WT E1 T1.
        all: exploit (fun X => IHexp _ _ _ X T2 D2); [lia|].
        all: case=> [[]]; [|eexists; eapply eval_Binop_cmpu_ERR_val2; eassumption].
        all: move=> v2 E2; have [V2 _]:= subject_reduction_expr _ _ _ _ _ _ WT E2 T2.
        all: inv V1; inv V2; eexists; eapply eval_Binop_cmpu; by [eassumption|].
    - case=> i l > /= SZ.
      case Ti: (fn_tenv f)!i => [t0|] //=; case Si: (fn_szenv f)!i => [llsz0|] //=.
      have [llsz0' Si']:= proj1 (fn_szenv_fn_szenv' _ _) (ex_intro _ _ Si).
      rewrite Si'; case T: type_syn_path_elem_list => [[t ne]|] //= [_ <-] D.
      have D': defined_iset ne e.
      { move=> j; have:= D j; case case_set => // _ /(_ eq_refl) //. }
      have: ∃ l', eval_path_elem_list e f l llsz0' l'.
      { move: D => /(_ i); rewrite PTree.gss => /(_ eq_refl) [v0 Ei].
        have:= WT i _ Ei; rewrite Ti Si' => - [_] [_] [[<-]] [[<-]].
        rewrite/size_path/= in SZ.
        have:= fn_szenv_szenv'_str f i _ _ Si Si'.
        elim: l t0 llsz0 llsz0' v0 t ne SZ T D' {Ei Ti Si Si'}.
        + move=>> /= + [_ <-]; exists (Some []); constructor.
        + move=> [lidx] l IH [] // t0 [//|lsz0 ?] [//|lsz0' ?] v0 > SZ +++ H; inv H.
          cbn -[type_syn_path_elem]; rewrite type_Scell.
          case Nat.eqb_spec => // LEN.
          case Tlidx: type_expression_list => [[]|] //=.
          case (list_reflect _ typ_reflect) => //= T; rewrite T {T} in Tlidx.
          case Tl: type_syn_path_elem_list => [[? ne]|] > //= [_ <-].
          case/defined_iset_union_inv => D1 D2 H; inv H.
          have D': defined_iset ne e.
          { move=> j; move/(_ j): D2.
            case case_set_list => // _ /(_ eq_refl) //. }
          move: SZ => /=; set Sl := fold_right _ _ l; set Slidx := fold_right _ _ lidx => SZ.
          have SZlidx: (Slidx <= n)%nat by lia.
          have [[lvidx|] Elidx]:= IHexprlist _ _ _ SZlidx Tlidx D1.
          2: eexists; eapply eval_Scell_ERR_idx; eassumption.
          have [V _]:= subject_reduction_exprlist _ _ _ _ _ _ WT Elidx Tlidx.
          move: LEN; rewrite (eval_exprlist_length _ _ _ _ Elidx) H3.
          rewrite (eval_identlist_length_preservation _ _ _ _ EVlsz).
          rewrite (natlist_of_Vint64_length _ _ SHAPE).
          have: all_Vint64 lvidx.
          { move: V. have:= type_expression_list_length _ _ _ _ Tlidx.
            rewrite repeat_length => <-; rewrite (eval_exprlist_length _ _ _ _ Elidx).
            elim: lvidx {Elidx} => // > IH' H; inv H; inv H4 => /=; by apply IH'. }
          move/build_index_Some => /(_ shape)/[apply] - [idx Bidx].
          case VALID: (ExprSem.valid_index lvidx lvsz).
          2: eexists; eapply eval_Scell_ERR; eassumption.
          have Vidx: (idx < length lv)%coq_nat.
          { have:= valid_index_product _ _ _ _ SHAPE Bidx VALID; lia. }
          have:= proj2 (List.nth_error_Some _ _) Vidx; case N: nth_error => //= _.
          have:= proj1 (Forall_nth _ _) WTv idx Vundef Vidx.
          rewrite (nth_error_nth _ _ _ N) => W.
          exploit (fun X => IH _ _ _ _ _ _ X Tl D' H5 W); [lia|]. case=> [[l'|]].
          2: eexists; eapply eval_Scell_ERR_tl; eassumption.
          eexists; eapply eval_Scell; eassumption. }
      case=> [[l'|]] El'; eexists.
      + eapply eval_path_intro; eassumption.
      + eapply eval_path_ERR; eassumption.
    - case=> [|exp le] >.
      + eexists; econstructor.
      + move=> /= SZ; case T: type_expression => [[]|] //=.
        case Tl: type_expression_list => [[]|] > //= [_ <-] D.
        have [D1 D2]:= defined_iset_union_inv _ _ _ D.
        exploit (fun X => IHexp _ _ _ X T D1); [lia|]. case=> [[v|]].
        2: eexists; eapply eval_Econs_None_e; eassumption.
        exploit (fun X => IHexprlist _ _ _ X Tl D2); [lia|]. case=> [[lv|]].
        2: eexists; eapply eval_Econs_None; eassumption.
        eexists; eapply eval_Econs_Some_e; eassumption. }
Qed.

Corollary progress_expr e f:
  ∀ exp t ne,
    wt_env e f ->
    f |- exp ∈ t ¤ ne ->
    defined_iset ne e ->
    ∃ r, f & e |= exp => r.
Proof. move=> exp > /progress_expr_path/(_ (size_expr exp))/proj1 H; apply: H; lia. Qed.

Corollary progress_path e f:
  ∀ p t ne,
    wt_env e f ->
    f |- p ∈p t ¤ ne ->
    defined_iset ne e ->
    ∃ r, f & e |= p =>p r.
Proof. move=> p > /progress_expr_path/(_ (size_path p))/proj2/proj1 H; apply: H; lia. Qed.

Corollary progress_exprlist e f:
  ∀ le lt ne,
    wt_env e f ->
    f |- le ∈l lt ¤ ne ->
    defined_iset ne e ->
    ∃ r, f & e |= le =>l r.
Proof. move=> le > /progress_expr_path/(_ (size_expr_list le))/proj2/proj2 H; apply: H; lia. Qed.

Corollary progress_pathlist e f:
  ∀ lp lt ne,
    wt_env e f ->
    f |- lp ∈lp lt ¤ ne ->
    defined_iset ne e ->
    ∃ r, f & e |= lp =>lp r.
Proof.
  move=> +++ WT; elim=> [|p lp IH] /=.
  - eexists; constructor.
  - move=>>; case T: type_syn_path => [[]|] //=; case Tl: type_syn_path_list => [[]|] //=.
    case=> _ <- /defined_iset_union_inv [D1 D2].
    have [[p'|] Ep]:= progress_path _ _ _ _ _ WT T D1.
    2: eexists; apply eval_path_list_Cons_None_e; assumption.
    have [[lp'|] Elp] := IH _ _ Tl D2.
    2: eexists; eapply eval_path_list_Cons_None; eassumption.
    eexists; eapply eval_path_list_Cons; eassumption.
Qed.

Corollary progress_full_path e f:
  ∀ p t ne,
    wt_env e f ->
    type_write_full_syn_path f p = OK (t, ne) ->
    defined_iset ne e ->
    ∃ r, eval_full_path e f p r.
Proof.
  case=> i l > WT /=.
  case Ti: (fn_tenv f)!i => //=; case Si: (fn_szenv f)!i => //=.
  case Nat.eqb_spec => // LEN.
  case Tl: type_syn_path_elem_list => [[t ne]|] //=.
  case: l Tl LEN => [|s l] //.
  - case=> <- <- LEN [_ <-] D.
    have [> Si']:= proj1 (fn_szenv_fn_szenv' _ _) (ex_intro _ _ Si).
    have:= fn_szenv_szenv'_str _ _ _ _ Si Si'.
    move/Forall2_length => LEN'.
    eexists; eapply eval_full_path_intro.
    eassumption. by rewrite LEN. econstructor.
  - move=> Tl LEN [_ <-] D.
    exploit (fun X => progress_path _ _ (i, s :: l) t _ WT X D).
    { simpl in Tl; by rewrite/= Ti Si /= Tl. }
    case=> [[]] >.
    + move=> H; inv H.
      have:= fn_szenv_szenv'_str _ _ _ _ Si H3.
      move/Forall2_length => LEN'.
      eexists; eapply eval_full_path_intro.
      eassumption. by rewrite LEN. eassumption.
    + move=> H; inv H.
      have:= fn_szenv_szenv'_str _ _ _ _ Si H2.
      move/Forall2_length => LEN'.
      eexists; eapply eval_full_path_ERR.
      eassumption. by rewrite LEN. assumption.
Qed.

Import ES.
Module NB := SemanticsNonBlocking.

Section STATEMENTS_PROGRESS.

Definition valid_function_call f0 f e0 ap :=
  forall m,
    args_map (map snd ap) (map fst ap) = OK m ->
    forall p i llsz0 llsz j k lsz0 lsz sz0 sz,
      In (p, i) ap ->
      get_szenv_syn_path (fn_szenv' f0) p = Some llsz0 ->
      get_szenv_syn_path (fd_szenv f)  (i, []) = Some llsz ->
      nth_error llsz0 j = Some lsz0 ->
      nth_error llsz  j = Some lsz  ->
      nth_error lsz0  k = Some sz0  ->
      nth_error lsz   k = Some sz   ->
      exists sz' n,
        expr_subst_idents m sz = OK sz' /\
        NB.eval_expr e0 f0 (Eacc (sz0, [])) (Some (Vint64 n)) /\
        eval_expr e0 f0 sz' = Val (Vint64 n).

Local Open Scope error_monad_scope.
Local Open Scope option_error_monad_scope.
Local Open Scope option_bool_monad_scope.

Definition defined_iset' (n: iset) (e: env) : bool :=
  PTree_Properties.for_all n (fun i _ =>
    match e!i with
    | Some v => true
    | None => false
    end).

Theorem defined_iset_dec:
  ∀ n e, reflect (defined_iset n e) (defined_iset' n e).
Proof.
  move=> n e; case D: defined_iset'.
  - apply ReflectT =>>.
    move/(proj1 (PTree_Properties.for_all_correct _ _) D).
    case E: _!_ => //=; eauto.
  - apply ReflectF => H.
    have: defined_iset' n e; [|by rewrite D].
    apply PTree_Properties.for_all_correct => ? [] /H [> ->] //.
Qed.

Fixpoint ex_value e f t llsz : option value :=
  match t, llsz with
  | Tvoid     , [] => Some (Vundef)
  | Tbool     , [] => Some (Vbool false)
  | Tint _ _  , [] => Some (Vint Int.zero)
  | Tint64 _  , [] => Some (Vint64 Int64.zero)
  | Tfloat F32, [] => Some (Vfloat32 Float32.zero)
  | Tfloat F64, [] => Some (Vfloat64 Float.zero)
  | Tarr t, lsz :: llsz =>
    match ES.eval_ident_list e f lsz with
    | Val lvsz =>
      doo shape <- natlist_of_Vint64 lvsz;
      doo v <- ex_value e f t llsz;
      Some (Varr (repeat v (build_size shape)))
    | _ => None
    end
  | _, _ => None
  end.

Lemma cdoo {A B: Type} {x: option A} {f: A -> option B} {t: B}:
  option_bind x f = Some t ->
  ∃ y, x = Some y /\ f y = Some t.
Proof. case x =>> //=; by eexists. Qed.

Lemma cdob {A: Type} {x: option A} {f: A -> bool}:
  option_bool_bind x f = true ->
  ∃ y, x = Some y /\ f y = true.
Proof. case x =>> //=; by eexists. Qed.

Lemma cdo {A B: Type} {x: res A} {f: A -> res B} {t: B}:
  bind x f = OK t ->
  ∃ y, x = OK y /\ f y = OK t.
Proof. case x =>> //=; by eexists. Qed.

Lemma cdos {A B: Type} {m: string} {x: option A} {f: A -> res B} {t: B}:
  option_res_custom_msg_bind m x f = OK t ->
  ∃ y, x = Some y /\ f y = OK t.
Proof. case x =>> //=; by eexists. Qed.

Inductive get_cases {A: Type} (i j: positive) (xi xj: A) : A -> Prop :=
| set_Eq: i = j -> get_cases i j xi xj xi
| set_Neq: i <> j -> get_cases i j xi xj xj.

Inductive ex_value_cases e f t llsz: option value -> Prop :=
| ex_value_OK: ∀ v vex, v ∈ ⟦e, f, t, llsz⟧ -> ex_value_cases e f t llsz (Some vex)
| ex_value_not_OK: (∀ v, ~ (v ∈ ⟦e, f, t, llsz⟧)) -> ex_value_cases e f t llsz None.

Lemma case_ex_value e f:
  ∀ t llsz, ex_value_cases e f t llsz (ex_value e f t llsz).
Proof.
  elim=> [||??|?|[]|> IH] [|lsz llsz] > //=.
  1-13: repeat econstructor; move=>> T; by inv T.
  case EVlsz: eval_ident_list =>>.
  case SHAPE: natlist_of_Vint64 => [shape|] /=.
  case EX: ex_value =>> /=.
  1,2: have:= IH llsz; rewrite EX => T; inv T.
  all: econstructor.
  - instantiate (1 := Varr (repeat v (build_size shape))).
    econstructor.
    apply Forall_forall =>> /(@repeat_spec value) -> //.
    eassumption.
    apply eval_identlist_fixpoint_match; eassumption.
    eassumption.
    by rewrite repeat_length.
  - move=>> T; inv T; by apply: H WTvex.
  - move=>> T; inv T; move: EVlsz SHAPE SHAPE0.
    rewrite (proj1 (eval_identlist_fixpoint_match _ _ _ _) EVlsz0) => - [<-] -> //.
  - move=>> T; inv T; move: EVlsz.
    by rewrite (proj1 (eval_identlist_fixpoint_match _ _ _ _) EVlsz0).
  - move=>> T; inv T; move: EVlsz.
    by rewrite (proj1 (eval_identlist_fixpoint_match _ _ _ _) EVlsz0).
Unshelve.
  exact false. exact Int.zero. exact Int64.zero.
  exact Float32.zero. exact Float.zero.
Qed.

(* Lemma ex_value_wt_value e f:
  ∀ t llsz v, ex_value e f t llsz = Some v -> v ∈ ⟦e, f, t, llsz⟧.
Proof.
  move=>>. case case_ex_value =>> //. econstructor.

  elim=> [||??|?|[]|> IH] [] > //=.
  1-6: case=> <-; econstructor.
  destruct_match_goal. do 2 case/cdoo =>> [?].
  case=> <-; econstructor.
  apply Forall_forall =>> /(@repeat_spec value) ->.
  1,2: apply: IH; eassumption.
  apply eval_identlist_fixpoint_match; eassumption.
  eassumption.
  by rewrite repeat_length.
Qed.

Lemma wt_value_ex_value e f:
  ∀ t llsz v, v ∈ ⟦e, f, t, llsz⟧ -> ∃ v', ex_value e f t llsz = Some v'.
Proof.
  elim=> [||??|?|[]|> IH] [] > T //=; inv T; eauto.
  rewrite (proj1 (eval_identlist_fixpoint_match _ _ _ _) EVlsz) SHAPE /=.
  have [> -> /=]:= IH _ _ WTvex; eauto.
Qed. *)

Fixpoint wt_value' e f t llsz v : bool :=
  match t, llsz, v with
  | Tvoid, [], Vundef
  | Tbool, [], Vbool _
  | Tint _ _, [], Vint _
  | Tint64 _, [], Vint64 _
  | Tfloat F32, [], Vfloat32 _
  | Tfloat F64, [], Vfloat64 _ => true
  | Tarr t, lsz :: llsz, Varr lv =>
    dob vex <- ex_value e f t llsz;
    forallb (wt_value' e f t llsz) lv &&
    match ES.eval_ident_list e f lsz with
    | Val lvsz =>
      dob shape <- natlist_of_Vint64 lvsz;
      (build_size shape <= length lv)%nat
    | _ => false
    end
  | _, _, _ => false
  end.

Theorem wt_value_dec:
  ∀ e f t llsz v, reflect (wt_value e f t llsz v) (wt_value' e f t llsz v).
Proof.
  intros e f; fix IH 1.
  intros; case W: wt_value'; [apply ReflectT|apply ReflectF].
  - case: t W => [||||[]|] >; case: llsz =>> //; case: v =>> //=; try constructor.
    case/cdob =>> [WTvex].
    case/andP => /forallb_forall F.
    case Elsz: eval_ident_list => //=.
    case Nat: natlist_of_Vint64 => //= H.
    move: WTvex; case case_ex_value =>> // WTvex _.
    econstructor.
    + apply Forall_forall =>> /F; by case IH.
    + eassumption.
    + apply eval_identlist_fixpoint_match; eassumption.
    + eassumption.
    + eassumption.
  - intro H; destruct t; inv H; move: W => //=.
    suff: forallb (wt_value' e f t llsz0) lv.
    { move/eval_identlist_fixpoint_match: EVlsz => ->.
      rewrite SHAPE /= SZLEN.
      case case_ex_value =>>.
      by move=> _ ->. by move/(_ _ WTvex). }
    apply forallb_forall =>> /(proj1 (Forall_forall _ _) WTv); by case IH.
Qed.

Definition wt_env' e f :=
  PTree_Properties.for_all e (fun i v =>
    dob t <- (fn_tenv f)!i;
    dob llsz <- (fn_szenv' f)!i;
    wt_value' e f t llsz v).

Lemma wt_env_dec:
  ∀ e f, reflect (wt_env e f) (wt_env' e f).
Proof.
  intros; case H: wt_env'; [apply ReflectT|apply ReflectF].
  - move=>> /(proj1 (PTree_Properties.for_all_correct _ _) H).
    case (fn_tenv f)!_ =>> //=; case (fn_szenv' f)!_ =>> //=.
    move/wt_value_dec; eauto.
  - move=> X; have: wt_env' e f = true; [|by rewrite H].
    apply PTree_Properties.for_all_correct =>> /X.
    case=>> [>] [->] [->] /= /wt_value_dec //.
Qed.

Let Err {A: Type} : res A := Error [MSG ""].

Fixpoint block_height k : nat :=
  match k with
  | Kstop | Kreturnto _ _ _ _ _ => 0
  | Kseq _ k => block_height k
  | Kblock k => S (block_height k)
  end.

Definition wt_value_path e f p v :=
  dob llsz <- get_szenv_path (fn_szenv' f) p;
  dob t <- get_tenv_path (fn_tenv f) p;
  wt_value' e f t llsz v.

Definition wt_value_path_list e f lp lv :=
  forallb2 (wt_value_path e f) lp lv.

Fixpoint same_structure_values v1 v2 :=
  match v1, v2 with
  | Varr lv1, Varr lv2 =>
    forallb2 same_structure_values lv1 lv2
  | _, _ => primitive_value v1 && primitive_value v2
  end.

Definition structure_preserved ecaller ecallee marrs :=
  forallb (fun '(p, j) =>
    dob v <- ecallee!j;
    dob v0 <- get_env_path ecaller p;
    match v, v0 with
    | Varr _, Varr _ => same_structure_values v v0
    | _, _ => false
    end) marrs.

Definition at_least_mutable f (marrs: list (sem_path * ident)) :=
  forallb (fun '((i, _), _) =>
    dob p <- (fn_penv f)!i;
    Mutable <=& p) marrs.

Definition mutable f (marrs: list (sem_path * ident)) :=
  forallb (fun '(_, j) =>
    dob p <- (fn_penv f)!j;
    permission_beq p Mutable) marrs.

Fixpoint valid_cont ge f e (lne_n: list iset) lne_e lne_r k : res iset :=
  match lne_n, lne_e, lne_r with
  | ne_n :: lne_n, ne_e :: lne_e, ne_r :: lne_r =>
    match k with
    | Kstop =>
      match sig_res (fn_sig f) with
      | Tint _ _ => OK ne_r
      | _ => Err
      end
    | Kreturnto x E F m k =>
      do ue0_n' <- valid_cont ge F E lne_n lne_e lne_r k;
      let ue0_n'' := option_get (omap (fun x => PTree.remove x ue0_n') x) ue0_n' in
      if wt_env' E F &&
        structure_preserved E e m &&
        ~~ pargs_params_aliasing (map fst m) m &&
        match_types (fn_tenv F) (fn_tenv f) m &&
        at_least_mutable F m &&
        mutable f m &&
        match x with
        | Some x => dob p <- (fn_penv F)!x; (Mutable <=& p) &&
                    dob t <- (fn_tenv F)!x; typ_beq t (sig_res (fn_sig f))
        | None => true
        end &&
        defined_iset' ue0_n'' E
      then OK ne_r
      else Err
    | Kseq s k =>
      do ne_n' <- valid_cont ge f e (ne_n :: lne_n) (ne_e :: lne_e) (ne_r :: lne_r) k;
      type_statement ge f (block_height k) s ne_n' ne_e ne_r
    | Kblock k =>
      match ne_e with
      | [] => Err
      | (ne_e0 :: ne_e') =>
        do ne'_n <- valid_cont ge f e (ne_n :: lne_n) (ne_e' :: lne_e) (ne_r :: lne_r) k;
        if incl (fun _ _ => true) ne'_n ne_e0 then OK ne'_n
        else Err
      end
    end
  | _, _, _ => Err
  end.

Fixpoint wf_cont out k :=
  match k with
  | Kstop => no_normal_or_exit out
  | Kreturnto _ _ _ _ k => no_normal_or_exit out && wf_cont [Normal] k
  | Kseq s k =>
    let out' := if mem wf_ret_beq Normal out
                then remove wf_ret_eq_dec Normal out ++ possible_results s
                else out in
    wf_cont out' k
  | Kblock k =>
    let out' := map (fun r => match r with
                              | Exit O => Normal
                              | Exit (S n) => Exit n
                              | r => r
                              end) out in
    wf_cont out' k
  end.

Lemma wf_cont_app1:
  ∀ k l1 l2,
    wf_cont (l1 ++ l2) k ->
    wf_cont l1 k /\ wf_cont l2 k.
Proof.
  move=> k l1 l2.
  rewrite -(app_nil_r (l1 ++ l2)) -{2}(app_nil_r l1) -{2}(app_nil_r l2).
  elim: k l1 l2 [] => /=.
  - move=>>; rewrite/no_normal_or_exit ?existsb_app; repeat case/norP.
    split; case norP =>> //; by elim.
  - move=>> IH l1 l2 >.
    case (mem_dec_spec _ wf_ret_reflect) => I.
    + rewrite ?remove_app -app_assoc; case/IH => H1 H2.
      case/in_app: I => [/in_app []|] I.
      * case (mem_dec_spec _ wf_ret_reflect) => I1.
        2: by elim: I1; apply in_or_app; left.
        case (mem_dec_spec _ wf_ret_reflect) => I2.
        by split; rewrite -app_assoc.
        split. by rewrite -app_assoc.
        move: H2 {H1}.
        rewrite notin_remove. by move=> X; elim: I2; apply in_app; left.
        rewrite notin_remove. by move=> X; elim: I2; apply in_app; right.
        by rewrite app_assoc -(app_nil_r ((_ ++ _) ++ _)); case/IH; rewrite app_nil_r.
      * case (mem_dec_spec _ wf_ret_reflect _ (l2 ++ _)) => I2.
        2: by elim: I2; apply in_or_app; left.
        case (mem_dec_spec _ wf_ret_reflect) => I1.
        by split; rewrite -app_assoc.
        split. 2: by rewrite -app_assoc.
        move: H1 {H2}.
        rewrite notin_remove. by move=> X; elim: I1; apply in_app; left.
        rewrite notin_remove. by move=> X; elim: I1; apply in_app; right.
        by rewrite app_assoc -(app_nil_r ((_ ++ _) ++ _)); case/IH; rewrite app_nil_r.
      * case (mem_dec_spec _ wf_ret_reflect).
        2: by elim; apply in_or_app; right.
        case (mem_dec_spec _ wf_ret_reflect).
        2: by elim; apply in_or_app; right.
        by split; rewrite -app_assoc.
    + case (mem_dec_spec _ wf_ret_reflect) => [|_].
      by case/in_app => ??; elim: I; apply in_app; [left; apply in_app; left|right].
      case (mem_dec_spec _ wf_ret_reflect) => [|_].
      by case/in_app => ??; elim: I; apply in_app; [left; apply in_app; right|right].
      by apply: IH.
  - move=>> IH >; rewrite ?map_app; by apply: IH.
  - move=> _ _ _ _ > IH >; rewrite/no_normal_or_exit ?existsb_app.
    case/andP => + ->; repeat case/norP.
    split; case norP =>> //; by elim.
Qed.

Lemma wf_cont_app2:
  ∀ k l1 l2,
    wf_cont l1 k ->
    wf_cont l2 k ->
    wf_cont (l1 ++ l2) k.
Proof.
  move=> k l1 l2.
  rewrite -(app_nil_r (l1 ++ l2)) -{1}(app_nil_r l1) -{1}(app_nil_r l2).
  elim: k l1 l2 [] => /=.
  - move=>>; rewrite/no_normal_or_exit ?existsb_app.
    case/norP => ++ /norP []; by rewrite ?negb_or => -> -> ->.
  - move=>> IH l1 l2 l.
    case (mem_dec_spec _ wf_ret_reflect) => I1;
    case (mem_dec_spec _ wf_ret_reflect) => I2.
    + case (mem_dec_spec _ wf_ret_reflect _ (_ ++ _)); last first.
      { elim; apply in_or_app; case/in_app: I1.
        by left; apply in_app; left. by right. }
      move=> _; rewrite ?remove_app -3!app_assoc; by apply: IH.
    + case (mem_dec_spec _ wf_ret_reflect _ (_ ++ _)); last first.
      { elim; apply in_or_app; case/in_app: I1.
        by left; apply in_app; left. by right. }
      move=> _; rewrite ?remove_app.
      have Il2: ~ In Normal l2 by intro; elim: I2; apply in_app; left.
      have Il:  ~ In Normal l  by intro; elim: I2; apply in_app; right.
      rewrite (notin_remove _ l2) // (notin_remove _ l) //.
      move/[dup]/wf_cont_app1 => [_ WFs].
      rewrite -(app_nil_r (l2 ++ _)); rewrite -(app_nil_r (possible_results _)) in WFs.
      move=> + /IH/(_ WFs).
      rewrite -4!app_assoc app_nil_r; by apply: IH.
    + case (mem_dec_spec _ wf_ret_reflect _ (_ ++ _)); last first.
      { elim; apply in_or_app; case/in_app: I2.
        by left; apply in_app; right. by right. }
      move=> _; rewrite ?remove_app.
      have Il1: ~ In Normal l1 by intro; elim: I1; apply in_app; left.
      have Il:  ~ In Normal l  by intro; elim: I1; apply in_app; right.
      rewrite (notin_remove _ l1) // (notin_remove _ l) //.
      move/[swap]/[dup]/wf_cont_app1 => [_ WFs].
      rewrite -(app_nil_r (l1 ++ _)); rewrite -(app_nil_r (possible_results _)) in WFs.
      move=> + /IH/(_ WFs) => /[swap].
      rewrite -4!app_assoc app_nil_r; by apply: IH.
    + case (mem_dec_spec _ wf_ret_reflect _ (_ ++ _)).
      { repeat case/in_app.
        * intro; elim: I1; apply in_app; by left.
        * intro; elim: I2; apply in_app; by left.
        * intro; elim: I1; apply in_app; by right. }
      move=> _; by apply: IH.
  - move=>> IH >; rewrite ?map_app; by apply: IH.
  - move=> _ _ _ _ > IH >; rewrite/no_normal_or_exit ?existsb_app.
    move=> /andP [] /norP [] +++ /andP [] /norP [].
    by rewrite !negb_or => -> -> -> ->.
Qed.

Definition same_szvars_structure f0 f pp :=
  ∀ p i llsz0 llsz,
    In (p, i) pp ->
    get_szenv_path (fn_szenv' f0) p = Some llsz0 ->
    get_szenv_path (fn_szenv' f)  (i, []) = Some llsz ->
    length llsz0 = length llsz /\
    ∀ n lsz0 lsz, nth_error llsz0 n = Some lsz0 ->
                  nth_error llsz  n = Some lsz  ->
                  length lsz0 = length lsz.

Inductive invariants (p: program) : state -> Prop :=
| inv_Callstate_initial: forall f args
    (TF:     type_function (genv_of_program p) f = OK tt)
    (INIT:   initial_state p (Callstate f args Kstop)),
    invariants p (Callstate f args Kstop)
| inv_Callstate: forall idf f vargs id e0 f0 k args pargs ap pp ne0 ne lne_n lne_e lne_r
    (GENV:    (genv_of_program p)!idf = Some (Internal f))
    (WT0:     wt_env (remove_own_params (own_args (fn_penv f) pp) e0) f0)
    (DEF:     defined_iset (option_get (omap (fun x => PTree.remove x ne0) id) ne0)
                           (remove_own_params (own_args (fn_penv f) pp) e0))
    (WTARGS:  well_typed_valuelist (sig_args (fn_sig f)) vargs)
    (PP:      pargs_params (fn_params f) pargs = Some pp)
    (AP:      args_params (fn_params f) args = Some ap)
    (EARGS:   NB.eval_path_list e0 f0 args (Some pargs))
    (PARGS:   get_env_path_list e0 pargs = Some vargs)
    (VCALL:   valid_function_call f0 (Internal f) e0 ap)
    (ARRS:    ∀ p i, In (p, i) (mut_args (fn_penv f) pp) -> ∃ t, get_tenv_path (fn_tenv f0) p = Some (Tarr t))
    (SZSTR:   same_szvars_structure f0 f pp)
    (MT:      match_types (fn_tenv f0) (fn_tenv f) pp)
    (STR:     wt_value_path_list e0 f0 pargs vargs)
    (NOAm:    ~~ pargs_params_aliasing (map fst (mut_args (fn_penv f) pp)) (mut_args (fn_penv f) pp))
    (NOAo:    ~~ pargs_params_aliasing pargs (mut_args (fn_penv f) pp ++ own_args (fn_penv f) pp))
    (ROOT:    all_root_paths (own_args (fn_penv f) pp))
    (PERM0:   at_least_mutable f0 (mut_args (fn_penv f) pp))
    (TPidv:   match id with
              | Some x => dob p <- (fn_penv f0)!x; (Mutable <=& p) &&
                          dob t <- (fn_tenv f0)!x; typ_beq t (sig_res (fn_sig f))
              | None => true
              end)
    (VK:      valid_cont (genv_of_program p) f0 (remove_own_params (own_args (fn_penv f) pp) e0) lne_n lne_e lne_r k = OK ne0)
    (TS:      type_statement (genv_of_program p) f 0 (fn_body f)
                empty_iset [] empty_iset = OK ne)
    (WF:      wf_cont [Normal] k),
    (* (SZVARS:  all_params_size_variables (fn_params f) (fn_szenv' f) = OK szvars)
    (INCL:    PTree_Properties.for_all ne (fun i _ => mem Pos.eqb i (fn_params f ++ szvars))), *)
    invariants p (Callstate f vargs (Kreturnto id (remove_own_params (own_args (fn_penv f) pp) e0) f0 (mut_args (fn_penv f) pp) k))
| inv_State_error: forall f e k,
    invariants p (State e f Serror k)
| inv_State: forall f e s k ne_n ne_e ne_r lne_n lne_e lne_r ne'_n ne''_n
    (WT:      wt_env e f)
    (VK:      valid_cont (genv_of_program p) f e (ne_n :: lne_n) (ne_e :: lne_e) (ne_r :: lne_r) k = OK ne'_n)
    (TS:      type_statement (genv_of_program p) f (block_height k) s ne'_n ne_e ne_r = OK ne''_n)
    (DEF:     defined_iset ne''_n e)
    (WF:      wf_cont (possible_results s) k),
    invariants p (State e f s k)
| inv_Returnstate: forall f e k v ne_n ne_e ne_r ne'_n
    (WT:      wt_env e f)
    (VK:      valid_cont (genv_of_program p) f e ne_n ne_e ne_r k = OK ne'_n)
    (* (DEF:     defined_iset ne'_n e) *)
    (WF:      wf_cont [Return] k)
    (WTRET:   well_typed_value_bool (sig_res (fn_sig f)) v),
    invariants p (Returnstate e f v k).

(* TODO: move to SemanticsCommon *)
Lemma primitive_type_default_value:
  ∀ t,
    t <> Tvoid ->
    primitive_type t ->
    exists v, default_value t = Some v.
Proof. case=> [| | | |[]|] > //= _ _; eauto. Qed.

Lemma default_value_wt_value e f:
  ∀ t v,
    default_value t = Some v ->
    v ∈ ⟦e, f, t, []⟧.
Proof. case=> [||||[]|] > //= [<-]; econstructor. Qed.

Lemma wt_value_primitive_type_primitive_value e f:
  ∀ t llsz v,
    v ∈ ⟦e, f, t, llsz⟧ ->
    primitive_type t = primitive_value v.
Proof. case; by inv 1. Qed.

Lemma wt_value_well_typed_value e f:
  ∀ t llsz v,
    v ∈ ⟦e, f, t, llsz⟧ ->
    well_typed_value t v.
Proof.
  elim=> [|||||> IH] >; inv 1; econstructor.
  eapply Forall_impl; by [intro; apply IH|eassumption].
Qed.

Lemma wt_value_shrink e f:
  ∀ t llsz v,
    v ∈ ⟦e, f, t, llsz⟧ ->
    (shrink t v) ∈ ⟦e, f, t, llsz⟧.
Proof. case=> [||[][]|||]; inv 1 => /=; econstructor; eassumption. Qed.

Lemma wt_value_assign e f:
  ∀ t llsz v i v',
    v ∈ ⟦e, f, t, llsz⟧ ->
    Forall (Forall (fun j => i <> j)) llsz ->
    v ∈ ⟦PTree.set i v' e, f, t, llsz⟧.
Proof.
  elim=>>. 1-5: inv 1; econstructor.
  (* Array case *)
  move=> IH >; do 2 inv 1; econstructor.
  + apply Forall_forall =>> I; apply: IH => //.
    exact (proj1 (Forall_forall _ _) WTv _ I).
  + apply: IH; eassumption.
  + instantiate (1 := lvsz).
    elim: lsz lvsz EVlsz H3 {H H0 H4 SHAPE} => [|> IH'] >.
    * inv 1; econstructor.
    * do 2 inv 1; econstructor => //.
      rewrite PTree.gso //; by apply not_eq_sym.
      by apply: IH'.
  + eassumption.
  + assumption.
Qed.

Lemma wt_value_remove e f:
  ∀ t llsz v i,
    v ∈ ⟦e, f, t, llsz⟧ ->
    Forall (Forall (fun j => i <> j)) llsz ->
    v ∈ ⟦PTree.remove i e, f, t, llsz⟧.
Proof.
  elim=>>. 1-5: inv 1; econstructor.
  (* Array case *)
  move=> IH >; do 2 inv 1; econstructor.
  + apply Forall_forall =>> I; apply: IH => //.
    exact (proj1 (Forall_forall _ _) WTv _ I).
  + apply: IH; eassumption.
  + instantiate (1 := lvsz).
    elim: lsz lvsz EVlsz H3 {H H0 H4 SHAPE} => [|> IH'] >.
    * inv 1; econstructor.
    * do 2 inv 1; econstructor => //.
      rewrite PTree.gro //; by apply not_eq_sym.
      by apply: IH'.
  + eassumption.
  + assumption.
Qed.

Lemma get_szenv_path_full {A: Type} (sze: PTree.t (list (list A))):
  ∀ i l llsz,
    get_szenv_path sze (i, l) = Some llsz ->
    ∃ llsz0, sze!i = Some (llsz0 ++ llsz) /\ length llsz0 = length l.
Proof.
  move=> i /=. case sze!i => //= /[swap]. elim/list_ind => /=.
  - move=>> [<-]. by exists [].
  - case=> _ /= l IH [] //= >. case/IH =>> [[->] <-]. rewrite app_comm_cons. eauto.
Qed.

Lemma eval_path_get_tenv_path e f:
  ∀ p p',
    f & e |= p =>p Some p' ->
    get_tenv_syn_path (fn_tenv f) p = get_tenv_path (fn_tenv f) p'.
Proof.
  case=> i l [? l'] T; inv T => {H2} /=.
  case (fn_tenv f)!_ => //=.
  elim: l l' llsz H4 => [|> IH] > /= T; inv T => // - [] //=.
  apply: IH; eassumption.
Qed.

Lemma eval_full_path_get_tenv_path e f:
  ∀ p p',
    NB.eval_full_path e f p (Some p') ->
    get_tenv_syn_path (fn_tenv f) p = get_tenv_path (fn_tenv f) p'.
Proof.
  case=> i l [? l'] T; inv T => {H3 H4} /=.
  case (fn_tenv f)!_ => //=.
  elim: l l' llsz H5 => [|> IH] > /= T; inv T => // - [] //=.
  apply: IH; eassumption.
Qed.

Lemma eval_path_get_szenv_path e f:
  ∀ p p',
    f & e |= p =>p Some p' ->
    get_szenv_syn_path (fn_szenv f) p = get_szenv_path (fn_szenv f) p'.
Proof.
  case=> i l [? l'] T; inv T => {H2} /=.
  case (fn_szenv f)!_ => //=.
  elim: l l' llsz H4 => [|> IH] > /= T; inv T => // - [|>] //=.
  apply: IH; eassumption.
Qed.

Lemma eval_path_get_szenv_path' e f:
  ∀ p p',
    f & e |= p =>p Some p' ->
    get_szenv_syn_path (fn_szenv' f) p = get_szenv_path (fn_szenv' f) p'.
Proof.
  case=> i l [? l'] T; inv T => {H2} /=.
  case (fn_szenv' f)!_ => //=.
  elim: l l' llsz H4 => [|> IH] > /= T; inv T => // - [|>] //=.
  apply: IH; eassumption.
Qed.

Lemma mutable_var_not_in_size f:
  ∀ p i llsz,
    get_szenv_path (fn_szenv' f) p = Some llsz ->
    (dob p <- (fn_penv f)!i; Mutable <=& p) ->
    Forall (Forall (fun sz => i <> sz)) llsz.
Proof.
  intros [j l] * SZ P.
  have [llsz0 [Sj LEN]]:= get_szenv_path_full _ _ _ _ SZ.
  have:= fn_penv_szenv' f _ _ Sj.
  case/Forall_app => _ H.
  apply Forall_forall =>> H1; apply Forall_forall =>> H2.
  move/Forall_forall/(_ _ H1)/Forall_forall/(_ _ H2): H => + T.
  rewrite -T; by case: (fn_penv f)!i P => [[]|].
Qed.

Theorem wt_env_assign e f:
  ∀ i l e' t llsz v,
    wt_env e f ->
    get_tenv_path (fn_tenv f) (i, l) = Some t ->
    get_szenv_path (fn_szenv' f) (i, l) = Some llsz ->
    wt_value e f t llsz v ->
    (dob p <- (fn_penv f)!i; Mutable <=& p) ->
    update_env_path' e (i, l) v = Some e' ->
    wt_env e' f.
Proof.
  move=> i l e' t llsz v WT.
  case Pi: (fn_penv f)!i => [perm|] //; cbn -[perm_le] => +++ Hperm => /=.
  case Ti: (fn_tenv f)!i => [t0|] //=.
  case Si: (fn_szenv' f)!i => [llsz0|] //=.
  move=> ++ WTv + j v0'.
  case (Pos.eqb_spec i j) => [<-|NEQ].
  - case: l => [|x s].
    { move=> [EQ1] [EQ2] [<-]; rewrite PTree.gss => - [<-].
      rewrite EQ1 EQ2 in Ti Si.
      repeat eexists; try eassumption.
      apply wt_value_assign => //.
      apply (mutable_var_not_in_size f (i, [])); by [rewrite /= Si|rewrite Pi]. }
    set l := x :: s; clearbody l.
    case Ei: e!i => [v0|] //=; case UPD: update_value =>> //= ++ [<-].
    rewrite PTree.gss => ++ [T]; rewrite T {T} in UPD.
    have:= WT _ _ Ei; rewrite Ti Si => - [_] [_] [[<-]] [[<-]] V0 T0 S0.
    repeat eexists.
    apply wt_value_assign; last first.
    apply (mutable_var_not_in_size f (i, [])); by [rewrite /= Si|rewrite Pi].
    elim: l t0 llsz0 v0 v0' T0 S0 UPD V0 {Ei Ti Si} => /=.
    + move=>> [EQ1] [EQ2] [<-]; by rewrite -EQ1 -EQ2 in WTv.
    + move=> [n] l IH [] // t0 [] // lsz0 llsz0 [] // lv0 > T0 S0.
      case N: nth_error => [v1|] //=.
      case UPD: update_value => [v1'|] //=.
      case R: replace_nth => [lv0'|] //= [<-] H; inv H.
      econstructor; try eassumption.
      * apply Forall_nth => k > Hk.
        case: (Nat.eqb_spec n k) Hk => [<-|NEQ].
        ** rewrite -(replace_nth_length _ _ _ _ R) => Hn.
           have:= proj1 (Forall_nth _ _) WTv0 n v1 Hn.
           rewrite (nth_error_nth _ _ _ N).
           rewrite (nth_error_nth _ _ _ (replace_nth_nth_error_same _ _ _ _ R)).
           by apply: IH.
        ** rewrite -(replace_nth_length _ _ _ _ R) => Hk.
           have:= proj1 (Forall_nth _ _) WTv0 k v1 Hk.
           have:= replace_nth_nth_error_other _ _ _ _ R _ NEQ.
           case N1: (nth_error lv0) => N2; last first.
           { move/nth_error_None: N1; lia. }
           by rewrite (nth_error_nth _ _ _ N1) (nth_error_nth _ _ _ N2).
      * by rewrite -(replace_nth_length _ _ _ _ R).
  - case: l =>> /= _ _.
    2: case Ei: e!i => //=.
    2: set X := match _ with Pcell idx => _ end; case: X =>> //=.
    all: case=> <-; case case_set => // _ Ej.
    all: have [> [llsz' [Tj [Sj WTv']]]]:= WT _ _ Ej.
    all: do 2 eexists; repeat (split; [eassumption|]).
    all: apply wt_value_assign => //.
    all: apply (mutable_var_not_in_size f (j, [])); by [rewrite /= Sj|rewrite Pi].
Qed.

Theorem wt_env_assign_owned_size e f:
  ∀ i llsz j lsz v,
    wt_env e f ->
    (fn_szenv' f)!i = Some llsz ->
    (fn_penv f)!i = Some Owned ->
    In lsz llsz ->
    In j lsz ->
    v ∈ ⟦e, f, Tuint64, []⟧ ->
    ∀ k vk, i <> k ->
            (PTree.set j v e)!k = Some vk ->
            ∃ t llsz,
              (fn_tenv f)!k = Some t /\ (fn_szenv' f)!k = Some llsz /\
              (vk ∈ ⟦(PTree.set j v e), f, t, llsz⟧).
Proof.
  intros * WT Si Pi I1 I2 WTv k vk NEQ.
  case case_set => [<-|NEQ'].
  - case=> <-.
    have:= fn_tenv_sztype f _ _ Si.
    move/Forall_forall/(_ _ I1)/Forall_forall/(_ _ I2) => ->.
    have:= fn_szenv'_szvars f _ _ Si.
    move/Forall_forall/(_ _ I1)/Forall_forall/(_ _ I2).
    case (fn_szenv' f)!j => [[]|] //= _.
    repeat eexists=> //. by inv WTv; econstructor.
  - case/WT => tk [llszk] [Tk] [Sk] WTvk.
    repeat eexists; try eassumption.
    apply wt_value_assign => //.
    apply Forall_forall =>> I1'; apply Forall_forall =>> I2'.
    by apply: (fn_szenv'_owned f) Pi NEQ Si Sk I1 I2 I1' I2'.
Qed.

Lemma valid_cont_assign_non_mutable ge f i v:
  ∀ k e ne_n ne_e ne_r ne_n',
    (fn_penv f)!i <> Some Mutable ->
    valid_cont ge f e ne_n ne_e ne_r k = OK ne_n' ->
    valid_cont ge f (PTree.set i v e) ne_n ne_e ne_r k = OK ne_n'.
Proof.
  elim=> [|?? IH|? IH|????? IH] ? [|??] [|??] [|??] > //=.
  - move=>> Pi; case VK: valid_cont => //=; by rewrite (IH _ _ _ _ _ Pi VK).
  - move=>> Pi; destruct_match_goal.
    by case/cdo =>> [/IH - /(_ Pi) -> /=].
  - move=>> Pi; case valid_cont =>> //=.
    case H: (_ && _) => // E; move: H; repeat case/andP.
    move=> -> H -> -> -> /[dup] M -> -> -> /=.
    set G := structure_preserved _ _ _; have -> //: G = true; rewrite/G.
    apply forallb_forall => - [] > I.
    case case_set => [EQ|_] //=.
    + move/forallb_forall/(_ _ I): M; rewrite -EQ.
      by case: (fn_penv f)!i Pi => [[]|].
    + by move/forallb_forall/(_ _ I): H.
Qed.

Lemma valid_cont_remove_non_mutable ge f i:
  ∀ k e ne_n ne_e ne_r ne_n',
    (fn_penv f)!i <> Some Mutable ->
    valid_cont ge f e ne_n ne_e ne_r k = OK ne_n' ->
    valid_cont ge f (PTree.remove i e) ne_n ne_e ne_r k = OK ne_n'.
Proof.
  elim=> [|?? IH|? IH|????? IH] ? [|??] [|??] [|??] > //=.
  - move=>> Pi; case VK: valid_cont => //=; by rewrite (IH _ _ _ _ _ Pi VK).
  - move=>> Pi; destruct_match_goal.
    by case/cdo =>> [/IH - /(_ Pi) -> /=].
  - move=>> Pi; case valid_cont =>> //=.
    case H: (_ && _) => // E; move: H; repeat case/andP.
    move=> -> H -> -> -> /[dup] M -> -> -> /=.
    set G := structure_preserved _ _ _; have -> //: G = true; rewrite/G.
    apply forallb_forall => - [] > I.
    case case_remove => [EQ|_] //=.
    + move/forallb_forall/(_ _ I): M; rewrite -EQ.
      by case: (fn_penv f)!i Pi => [[]|].
    + by move/forallb_forall/(_ _ I): H.
Qed.

Lemma valid_cont_remove_list_non_mutable ge f:
  ∀ l k e ne_n ne_e ne_r ne_n',
    Forall (fun i => (fn_penv f)!i = Some Owned) l ->
    valid_cont ge f e ne_n ne_e ne_r k = OK ne_n' ->
    valid_cont ge f (remove_list l e) ne_n ne_e ne_r k = OK ne_n'.
Proof.
  elim=>> // IH > T H; inv T; apply: IH => //.
  apply valid_cont_remove_non_mutable=> //. congruence.
Qed.

Lemma valid_cont_assign ge f e i l v e':
  wt_env e f ->
  (dob p <- (fn_penv f)!i; Mutable <=& p) ->
  wt_value_path e f (i, l) v ->
  update_env_path' e (i, l) v = Some e' ->
  primitive_value v ->
  ∀ k ne_n ne_e ne_r ne_n',
    valid_cont ge f e ne_n ne_e ne_r k = OK ne_n' ->
    valid_cont ge f e' ne_n ne_e ne_r k = OK ne_n'.
Proof.
  intros WT Pi WTv UPD PRIM.
  elim=> [|?? IH|? IH|????? IH] [|??] [|??] [|??] > //=.
  - case VK: valid_cont => //=; by rewrite (IH _ _ _ _ VK).
  - destruct_match_goal; by case/cdo =>> [/IH -> /=].
  - case valid_cont =>> //=.
    case H: (_ && _) => // E; move: H; repeat case/andP.
    move=> -> H -> -> -> -> -> -> /=.
    set G := structure_preserved _ _ _; have -> //: G = true; rewrite/G.
    apply forallb_forall => - [[i' l'] j].
    move/forallb_forall: H => /[apply].
    move: UPD => /=.
    case Ej: e!j => [vj|] //=.
    case: l WTv => [|x s]; [|set l := x :: s; clearbody l] => WTv.
    + case=> <-; case: case_set Ej => [<-|?] Ej; [|by rewrite Ej].
      move: WTv; rewrite/wt_value_path => /=.
      have:= WT _ _ Ej.
      case (fn_szenv' f)!i =>> //=; case (fn_tenv f)!i =>> //=.
      case=> _ [_] [[<-]] [[<-]] H1 /wt_value_dec H2.
      (* case _!_ =>> //=; case get_value_path => [[]|] > //=. *)
      by inv H1; inv H2.
    + case Ei: e!i => [v0|] //=; case UPD: update_value => [v0'|] //= [<-].
      case: case_set Ej => [<-|?] //= Ej; [|by rewrite Ej].
      move: Ej; rewrite Ei => - [<-] {vj}.
      move: WTv; rewrite/wt_value_path => /=.
      have:= WT _ _ Ei.
      case (fn_szenv' f )!_ => [llsz0|]  //=; case SZ:  (get_size_path l)  => [llsz|]  //=.
      case (fn_tenv f )!_ => [t0|]  //=; case T:  (get_type_path l)  => [t|]  //=.
      case=> _ [_] [[<-]] [[<-]].
      case _!_ => v1 //=; case P: get_value_path => [v2|] //= W1 W2 H.
      have: ∃ l0, v2 = Varr l0; [|case=> lv2 EQ; move: H; rewrite {1 3}EQ {lv2 EQ} => H].
      { case: v2 H {P} =>>; only 1-6: by case v0. by eexists. }
      have V0: ∃ l0, v0 = Varr l0.
      { case: v0 H {Ei UPD W1} =>> //; by eexists. }
      have: ∃ l0', v0' = Varr l0'; [|case=> lv0' EQ; rewrite {1}EQ {lv0' EQ}].
      { case: V0 W1 UPD =>> -> W1. case: l T SZ W2 => /=.
        * case=> <- [<-]; inv W1 => /wt_value_dec T; inv T => - [<-]; by eexists.
        * case=>> _ _ _; do 3 case/cdoo =>> [_]. case=> <-; by eexists. }
      case: V0 H => _ {1}->.
      elim: l t0 llsz0 v0 v0' v2 T SZ UPD W1 W2 {v1 Ei P} => /=.
      * move=>> [<-] [<-] [<-] H1 /wt_value_dec H2; by inv H1; inv H2.
      * move=> [idx] > IH' [] // t0 [] // lsz0 llsz0 [] // lv0 ? v2 T SZ.
        case N: nth_error => [v1|] //=.
        case UPD: update_value => [v1'|] //=.
        case R: replace_nth => [lv0'|] //= [<-] H1 WTv' /=.
        inv H1 => //.
        have WTv1: v1 ∈ ⟦e, f, t0, llsz0⟧.
        { have:= proj1 (Forall_nth _ _) WTv idx Vundef.
          rewrite (nth_error_nth _ _ _ N) => X; apply: X.
          apply List.nth_error_Some; by rewrite N. }
        case: v2 =>> //=.
        case/forallb2_forall => LEN H. apply forallb2_forall; split.
        { by rewrite -(replace_nth_length _ _ _ _ R). }
        move=> n x1 x2 + /[dup]/H {H} => + H N'.
        case (Nat.eqb_spec idx n) => [EQ|NEQ].
        ** rewrite -EQ (replace_nth_nth_error_same _ _ _ _ R) => - [<-].
           move: N; rewrite EQ => /H; by apply: IH' T SZ UPD WTv1 WTv'.
        ** have:= replace_nth_nth_error_other _ _ _ _ R _ NEQ.
           move=> ->; by apply: H.
Qed.

Lemma type_program_fundef p:
  forall idf fd,
    type_program p = OK tt ->
    (genv_of_program p)!idf = Some fd ->
    type_fundef (genv_of_program p) fd = OK tt.
Proof.
  move=> idf fd TPROG.
  move/(@PTree.elements_correct fundef). rewrite{1}/genv_of_program => H.
  have:= proj2 (build_env_correct_empty _ (prog_nodup p) _) H => {H}.
  move: TPROG => /[swap]. rewrite/type_program.
  elim/list_ind: (prog_defs p) => //= - [idf' fd'] fds IH /=.
  case=> [[_ ->]|] /=; by case type_fundef => [[]|].
Qed.

Lemma type_program_type_size_expressions p:
  forall idf fd,
    type_program p = OK tt ->
    (genv_of_program p)!idf = Some fd ->
    type_size_expressions fd.
Proof.
  move=> idf fd /type_program_fundef/[apply]. rewrite/type_fundef.
  by case type_size_expressions.
Qed.

Lemma check_args_enough_permission:
  forall e f ue pe sze params args pargs pp ue',
    NB.eval_path_list e f args (Some pargs) ->
    pargs_params params pargs = Some pp ->
    check_args f ue pe sze args params = OK ue' ->
    enough_permission (fn_penv f) pe pp.
Proof.
  move=> e f ue pe sze params args pargs pp ue' EV PP.
  have:= proj1 (ListUtils.map2_Some _ _ _) (ex_intro _ pp PP).
  rewrite -(eval_path_list_length _ _ _ _ EV) => LEN.
  rewrite/check_args/args_params.
  have [ap AP] := proj2 (ListUtils.map2_Some pair args params) LEN; rewrite AP.
  case (match _ with [] => OK tt | _ => _ end) => //= _.
  case all_arrays => //=. case check_szvars_structure => //=. case forallb => //=.
  case (~~ (args_aliasing _ _ _)) => //=.
  case H: enough_permission => //= _.
  move: args pargs params ap pp EV AP PP H {LEN}.
  elim/list_ind => /=.
  - move=> ? [|//] > T; inv T => - [<-] [<-] //.
  - move=> [] > IH ? [//|] > T; inv T.
    case AP: ListUtils.map2 => //= - [<-]. case PP: pargs_params => //= - [<-].
    inv H2 => /=. case (fn_penv f)!_ => //= >. case pe!_ => //= >.
    case/andP => -> /=. apply: IH; eassumption.
Qed.

Lemma eval_filter_args_by_perm:
  ∀ e f pe p args pargs params ap pp,
    f & e |= args =>lp (Some pargs) ->
    args_params params args  = Some ap ->
    args_params params pargs = Some pp ->
    f & e |= (map fst (filter_args_by_perm pe p ap)) =>lp
             (Some (map fst (filter_args_by_perm pe p pp))).
Proof.
  move=> e f pe p. elim/list_ind => /=.
  - move=> ? [|//] > T; inv T => /= - [<-] [<-]. econstructor.
  - move=>> IH ? [//|] > T; inv T => /=.
    case AP: args_params => //= - [<-]. case PP: args_params => //= - [<-] /=.
    case pe!_ => /= >. case permission_beq => /=.
    econstructor => //. all: apply: IH; eassumption.
Qed.

Lemma try_eval_eval_expr:
  ∀ e f exp v,
    try_eval exp = Some v ->
    f & e |= exp => (Some v).
Proof.
  move=> e f. elim/expr_ind => //=.
  - case=>> [<-]; econstructor.
  - move=>> IH >. case H: try_eval => //= S.
    have X := IH _ H. econstructor; eassumption.
  - move=>> IH >. case H: try_eval => //= S.
    have X := IH _ H. econstructor; eassumption.
  - move=>> IH1 > IH2 >.
    case H1: try_eval => //=. case H2: try_eval => //= S.
    have X1 := IH1 _ H1. have X2 := IH2 _ H2.
    econstructor; eassumption.
  - move=>> IH1 > IH2 >.
    case H1: try_eval => //=. case H2: try_eval => //= S.
    have X1 := IH1 _ H1. have X2 := IH2 _ H2.
    econstructor; eassumption.
  - move=>> IH1 > IH2 >.
    case H1: try_eval => //=. case H2: try_eval => //= S.
    have X1 := IH1 _ H1. have X2 := IH2 _ H2.
    econstructor; eassumption.
Qed.

Lemma eval_expr_same_result_possible:
  ∀ e f exp1 exp2 v,
    f & e |= exp1 => Some v ->
    f & e |= exp2 => Some v ->
    expr_same_result_possible exp1 exp2.
Proof.
  move=> e f exp1 exp2 v H1 H2. rewrite/expr_same_result_possible.
  case T1: try_eval => //=. case T2: try_eval => //=.
  have X1 := try_eval_eval_expr e f _ _ T1.
  have X2 := try_eval_eval_expr e f _ _ T2.
  have [<-] := (eval_expr_deterministic _ _ _ _ _ H1 X1).
  have [<-] := (eval_expr_deterministic _ _ _ _ _ H2 X2).
  by case value_reflect.
Qed.

Lemma eval_exprlist_same_result_possible:
  ∀ e f lidx1 lidx2 lvidx,
    f & e |= lidx1 =>l Some lvidx ->
    f & e |= lidx2 =>l Some lvidx ->
    ListUtils.prefix expr_same_result_possible lidx1 lidx2
    || ListUtils.prefix expr_same_result_possible lidx2 lidx1.
Proof.
  move=> e f. elim/list_ind => //=.
  move=> exp1 lidx1 IH [//|exp2 lidx2] > T1 T2; inv T1; inv T2 => /=.
  rewrite (eval_expr_same_result_possible _ _ _ _ _ H2 H4) /=.
  rewrite (eval_expr_same_result_possible _ _ _ _ _ H4 H2) /=.
  apply: IH; eassumption.
Qed.

Lemma sem_path_alias_syn_path_alias:
  ∀ e f p1 p2 p'1 p'2,
    f & e |= p1 =>p Some p'1 ->
    f & e |= p2 =>p Some p'2 ->
    sem_path_alias p'1 p'2 ->
    syn_path_alias p1 p2.
Proof.
  move=> e f [i1 l1] [i2 l2] > T1 T2; inv T1; inv T2 => /=.
  move: H2 H4 H3 H5. case Pos.eqb_spec => //= <- -> [<-].
  move: l1 l2 llsz sempl sempl0. elim/list_ind => //=.
  move=> [lidx1] l1 IH [|[lidx2] l2] > T1 T2; inv T1; inv T2 => //=.
  move: H3 H11 H7 H14 H5 H13.
  rewrite -(eval_identlist_deterministic _ _ _ _ _ H2 H10) => /[dup] S -> [<-].
  move=> V1 V2 B1 B2.
  case (PeanoNat.Nat.eqb_spec idx idx0) => [Heq|Hneq].
  - rewrite -Heq internal_nat_dec_lb //= => /(IH _ _ _ _ H8 H15).
    case/orP => [->|->].
    + rewrite -Heq in B2.
      have T := build_index_valid_index_injective _ _ _ _ _ V1 V2 S B1 B2.
      rewrite -T in H12.
      have -> // : ListUtils.prefix expr_same_result_possible lidx1 lidx2
                   || ListUtils.prefix expr_same_result_possible lidx2 lidx1.
      exact (eval_exprlist_same_result_possible _ _ _ _ _ H4 H12).
    + rewrite -Heq in B2.
      have T := build_index_valid_index_injective _ _ _ _ _ V1 V2 S B1 B2.
      rewrite -T in H12.
      have -> : ListUtils.prefix expr_same_result_possible lidx2 lidx1
                || ListUtils.prefix expr_same_result_possible lidx1 lidx2.
      exact (eval_exprlist_same_result_possible _ _ _ _ _ H12 H4).
      by rewrite orbT.
  - case H: internal_nat_beq => //=.
    move/internal_nat_dec_bl in H. by rewrite H in Hneq. move {H}.
    case H: internal_nat_beq => //=.
    move/internal_nat_dec_bl in H. by rewrite H in Hneq.
Qed.

Lemma eval_path_list_Forall2 e f:
  ∀ lp lp',
    f & e |= lp =>lp Some lp' ->
    Forall2 (fun p' p => f & e |= p =>p Some p') lp' lp.
Proof.
  elim/list_ind => /=.
  - move=>> T; inv T. econstructor.
  - move=>> IH > T; inv T. econstructor => //. by apply: IH.
Qed.

Lemma args_aliasing_syn_path_sem_path_correct:
  ∀ e f args pargs ap pp,
    f & e |= args =>lp Some pargs ->
    f & e |= map fst ap =>lp Some (map fst pp) ->
    List.incl (map fst ap) args ->
    List.incl (map fst pp) pargs ->
    args_aliasing sem_path_alias pargs pp ->
    args_aliasing syn_path_alias args  ap.
Proof.
  move=> e f args pargs ap pp EVargs EVp INCLa INCLp.
  set R := fun p' p => NB.eval_path e f p (Some p').
  apply: (args_aliasing_impl sem_path_alias syn_path_alias R); rewrite/R //.
  - move=>>. by apply: (sem_path_alias_syn_path_alias e f).
  - exact (eval_path_list_Forall2 _ _ _ _ EVargs).
  - exact (eval_path_list_Forall2 _ _ _ _ EVp).
Qed.

Lemma eval_path_list_filter_args_by_perm e f pe perm:
  forall args pargs params ap pp,
    NB.eval_path_list e f args (Some pargs) ->
    args_params params args = Some ap ->
    pargs_params params pargs = Some pp ->
    map fst (map fst (filter_args_by_perm pe perm ap))
    = map fst (map fst (filter_args_by_perm pe perm pp)).
Proof.
  elim=> [|> IH] ? [|??] //= > H; inv H.
  - case=> <- [<-] //.
  - inv H3 => /=. case AP: args_params => //=.
    case PP: pargs_params =>> //= [<-] [<-] /=.
    case pe!_ =>> //=. case permission_beq => /=.
    all: by rewrite (IH _ _ _ _ H4 AP PP).
Qed.

Lemma check_args_no_alias:
  forall e f ne pe sze params args pargs pp,
    NB.eval_path_list e f args (Some pargs) ->
    pargs_params params pargs = Some pp ->
    check_args f ne pe sze args params = OK tt ->
    ~~ args_aliasing sem_path_alias pargs (mut_args pe pp ++ own_args pe pp).
Proof.
  move=> e f ne pe sze params args pargs pp EV PP.
  have:= proj1 (ListUtils.map2_Some _ _ _) (ex_intro _ pp PP).
  rewrite -(eval_path_list_length _ _ _ _ EV) => LEN.
  rewrite/check_args/args_params.
  have [ap AP] := proj2 (ListUtils.map2_Some pair args params) LEN; rewrite AP.
  case (match _ with [] => OK tt | _ => _ end) => //= _.
  case all_arrays => //=. case check_szvars_structure => //=.
  case forallb => //=. case H: (~~ (args_aliasing _ _ _)) => //= _.
  case X: (args_aliasing sem_path_alias pargs _) => //.
  have EVmut := eval_filter_args_by_perm _ _ pe Mutable _ _ _ _ _ EV AP PP.
  have EVown := eval_filter_args_by_perm _ _ pe Owned _ _ _ _ _ EV AP PP.
  have EV' := eval_path_list_app _ _ _ _ _ _ EVmut EVown.
  have H': forall pe p ap, List.incl (map fst (filter_args_by_perm pe p ap)) (map fst ap).
  { move=> ??? l >. case/(in_map_fst_pair (filter_args_by_perm _ _ l)) => i.
    by move/filter_args_by_perm_incl/(in_map_pair_fst l). }
  have INCLa: List.incl (map fst (mut_args pe ap ++ own_args pe ap)) args.
  { have I := args_params_list_incl_l _ _ _ AP.
    rewrite map_app. apply: List.incl_app.
    have:= H' _ pe Mutable ap => /incl_tran/(_ I) //.
    have:= H' _ pe Owned ap => /incl_tran/(_ I) //. }
  have INCLb: List.incl (map fst (mut_args pe pp ++ own_args pe pp)) pargs.
  { have I := args_params_list_incl_l _ _ _ PP.
    rewrite map_app. apply: List.incl_app.
    have:= H' _ pe Mutable pp => /incl_tran/(_ I) //.
    have:= H' _ pe Owned pp => /incl_tran/(_ I) //. }
  rewrite -!map_app in EV'.
  erewrite <- args_aliasing_syn_path_sem_path_correct; eassumption.
Qed.

Lemma check_args_all_root_paths:
  ∀ e f ne pe sze params args pargs pp,
    f & e |= args =>lp Some pargs ->
    pargs_params params pargs = Some pp ->
    check_args f ne pe sze args params = OK tt ->
    all_root_paths (own_args pe pp) = true.
Proof.
  move=> e f ne pe sze params args pargs pp EV PP.
  have:= proj1 (ListUtils.map2_Some _ _ _) (ex_intro _ pp PP).
  rewrite -(eval_path_list_length _ _ _ _ EV) => LEN.
  rewrite/check_args/args_params.
  have [ap AP] := proj2 (ListUtils.map2_Some pair args params) LEN; rewrite AP.
  case (match _ with [] => OK tt | _ => _ end) => //= _.
  case all_arrays => //=. case check_szvars_structure => //=.
  case H: forallb => //= _. move: args pargs params ap pp EV AP PP H {LEN}.
  elim/list_ind => /=.
  - move=> ? [|//] > T; inv T => - [<-] [<-] //.
  - move=> [i l] args IH ? [//|p params] > T; inv T => /=.
    case AP: ListUtils.map2 => //= - [<-].
    case PP: pargs_params => //= - [<-] /=.
    case pe!p => [[]|] /=.
    3: case/andP => /[swap].
    all: move/(IH _ _ _ _ H3 AP PP) => // ->.
    inv H2. move: H5. case: l => //= T. by inv T.
Qed.

Lemma check_args_all_arrays:
  ∀ e f ne pe sze params args pargs pp,
    f & e |= args =>lp Some pargs ->
    pargs_params params pargs = Some pp ->
    check_args f ne pe sze args params = OK tt ->
    all_arrays get_tenv_path f (mut_args pe pp) = true.
Proof.
  move=> e f ne pe sze params args pargs pp EV PP.
  have:= proj1 (ListUtils.map2_Some _ _ _) (ex_intro _ pp PP).
  rewrite -(eval_path_list_length _ _ _ _ EV) => LEN.
  rewrite/check_args/args_params.
  have [ap AP] := proj2 (ListUtils.map2_Some pair args params) LEN; rewrite AP.
  case (match _ with [] => OK tt | _ => _ end) => //= _.
  case H: all_arrays => //= _.
  apply forallb_forall => - [p' i].
  case/filter_In => I; case Pi: pe!i => [[]|] //= _.
  case/(In_nth_error pp): I =>> Npp.
  have:= map_nth_error fst _ _ Npp; rewrite /= (args_params_l _ _ _ PP) => Np'.
  have:= map_nth_error snd _ _ Npp; rewrite /= (args_params_r _ _ _ PP) => Ni.
  have [p [Np Ep]]:= eval_path_list_nth_error' _ _ _ _ _ _ EV Np'.
  have I: In (p, i) ap by exact: args_params_nth_error_In (AP) Np Ni.
  have: In (p, i) (mut_args pe ap) by apply filter_In; split=> //; by rewrite Pi.
  move/(proj1 (forallb_forall _ _) H).
  by rewrite (eval_path_get_tenv_path _ _ _ _ Ep).
Qed.

Lemma check_args_own_transfer_used_later:
  ∀ e f ne pe sze params args pargs pp,
    f & e |= args =>lp Some pargs ->
    pargs_params params pargs = Some pp ->
    check_args f ne pe sze args params = OK tt ->
    existsb (fun '(i, _, _) =>
              match ne!i with Some tt => true | _ => false end)
            (own_args pe pp) = false.
Proof.
  move=> e f ne pe sze params args pargs pp EV PP.
  have:= proj1 (ListUtils.map2_Some _ _ _) (ex_intro _ pp PP).
  rewrite -(eval_path_list_length _ _ _ _ EV) => LEN.
  rewrite/check_args/args_params.
  have [ap AP] := proj2 (ListUtils.map2_Some pair args params) LEN; rewrite AP.
  case (match _ with [] => OK tt | _ => _ end) => //= _.
  case all_arrays => //=. case check_szvars_structure => //=.
  case forallb => //=. case args_aliasing => //=.
  case enough_permission => //=.
  case H: existsb => //= _.
  case X: existsb => //=; move/negP: H; elim.
  move/existsb_exists: X => [[[i l] j]] [I H].
  apply existsb_exists.
  case/filter_In: I => /(@In_nth_error (sem_path * ident)) [k] N P.
  have:= map_nth_error fst _ _ N; rewrite (args_params_l _ _ _ PP) /= => Np.
  have:= map_nth_error snd _ _ N; rewrite (args_params_r _ _ _ PP) /= => Nj.
  have [> [+ E]]:= eval_path_list_nth_error' _ _ _ _ _ _ EV Np; inv E.
  rewrite -(args_params_l _ _ _ AP) => Np'.
  have:= nth_error_map fst k ap; rewrite Np'; case N': nth_error => [[]|] //=.
  case=> T; rewrite -T {T} in N'.
  have:= map_nth_error snd _ _ N'; rewrite (args_params_r _ _ _ AP) /= Nj.
  case=> T; rewrite -T {T} in N'.
  move/(@nth_error_In (syn_path * ident)): N' => I'.
  eexists; split.
  apply filter_In; split; eassumption.
  eassumption.
Qed.

Lemma at_least_mutable_mut_arg:
  ∀ f f' pp,
    enough_permission (fn_penv f) (fd_penv f') pp ->
    at_least_mutable f (mut_args (fd_penv f') pp).
Proof.
  intros * H.
  apply forallb_forall => - [[]] >.
  case/filter_In. move/(proj1 (forallb_forall _ _) H).
  case (fn_penv f)!_ => [[]|] > //=.
  by case (fd_penv f')!_ => [[]|].
Qed.

Lemma get_szenv_syn_path_get_szenv_path {A: Type} e f (sze: PTree.t (list (list A))):
  forall p p',
    NB.eval_path e f p (Some p') ->
    get_szenv_syn_path sze p = get_szenv_path sze p'.
Proof.
  case=> [i l] > T; inv T => /=. case sze!i => //=.
  move: l sempl llsz {H2} H3. elim/list_ind => /=.
  - move=>> T; by inv T.
  - move=> [lidx] l IH > T; inv T => /=. case=> //= _.
    by apply: (IH _ llsz).
Qed.

Lemma eval_path_get_szenv_syn_path e f:
  forall p p',
    NB.eval_path e f p (Some p') ->
    exists llsz,
      get_szenv_syn_path (fn_szenv' f) p = Some llsz.
Proof.
  case=> i l p' T; inv T => /=. rewrite H2 {H2} /=.
  elim/list_ind: l sempl llsz H3 => /=.
  - by eexists.
  - move=>> IH > T; inv T. by apply: IH H8.
Qed.

Lemma same_szvars_structure_verified:
  forall e0 f0 f args pargs ap pp,
    NB.eval_path_list e0 f0 args (Some pargs) ->
    args_params (fn_params f) args = Some ap ->
    pargs_params (fn_params f) pargs = Some pp ->
    check_szvars_structure (fn_szenv' f0) (fn_szenv f) ap ->
    same_szvars_structure f0 f pp.
Proof.
  move=> e0 f0 f. elim/list_ind: (fn_params f) => /=.
  - move=> [] //= [] //= > T; inv T => - [<-] [<-] //.
  - move=> i params IH [//|a args] [//|p pargs] > T; inv T => /=.
    case AP: args_params =>> //= [<-]. case PP: pargs_params =>> //= [<-].
    case/andP => + H. rewrite/same_szvars_structure => + > /= [[<- <-]|].
    + rewrite (get_szenv_syn_path_get_szenv_path _ _ (fn_szenv' f0) _ _ H2).
      case get_szenv_path => llsz0 //=. have:= fn_szenv_szenv'_str f i.
      have:= proj1 (fn_szenv_fn_szenv' f i). case (fn_szenv f)!i => l //=.
      case/(_ (ex_intro (fun _ => _) l eq_refl)) =>> -> /= ++ [<-] [<-].
      case/(_ _ _ eq_refl eq_refl)/Forall2_forall => <- X.
      case/forallb2_forall => /[dup] LEN <- T. split=> //. move=> n > N1 N2.
      have [> N3]: ∃ lsz', nth_error l n = Some lsz'.
      { case N: nth_error; eauto. move/nth_error_None: N. rewrite -LEN.
        move/nth_error_None; by rewrite N1. }
      rewrite -(X _ _ _ N3 N2).
      have:= T _ _ _ N1 N3; by case Nat.eqb_spec.
    + move=> _. eapply IH; eassumption.
Qed.

Lemma expr_subst_idents_Some fd M:
  ∀ sz,
    (∀ x : ident, In x (fd_params fd) -> ∃ y : syn_path, M ! x = Some y) ->
    forall_vars (fun x => in_dec Pos.eq_dec x (fd_params fd)) sz ->
    wf_size_expr sz ->
    ∃ szr : expr, expr_subst_idents M sz = OK szr.
Proof.
  move=> + HM. elim/expr_ind => /=.
  - move=> [i []] //= + _. rewrite andbT. case in_dec => // + _.
    case/HM =>> -> /=. eauto.
  - eauto.
  - move=>> IH > /IH/[apply] - [> -> /=]; eauto.
  - move=>> IH > /IH/[apply] - [> -> /=]; eauto.
  - move=>> IH1 > IH2 > /andP [++] /andP [].
    move=> /IH1 + /[swap] => /[apply] - [> -> /=] /IH2/[apply] - [> -> /=]; eauto.
  - move=>> IH1 > IH2 > /andP [++] /andP [].
    move=> /IH1 + /[swap] => /[apply] - [> -> /=] /IH2/[apply] - [> -> /=]; eauto.
  - move=>> IH1 > IH2 > /andP [++] /andP [].
    move=> /IH1 + /[swap] => /[apply] - [> -> /=] /IH2/[apply] - [> -> /=]; eauto.
Qed.

Lemma type_syn_path_list_nth_error f:
  ∀ lp lt ne n p,
    f |- lp ∈lp lt ¤ ne ->
    nth_error lp n = Some p ->
    ∃ t ne', nth_error lt n = Some t /\ f |- p ∈p t ¤ ne' /\ incl (fun=> xpredT) ne' ne.
Proof.
  elim/list_ind => /=.
  - by move=> ++ [].
  - move=> a lp IH lt ? [|n] p.
    + move=> + [<-] /=. case type_syn_path => [[]|] //=.
      case type_syn_path_list => [[]|] //= > [<- <-].
      repeat eexists; by apply incl_union_unit_l.
    + case type_syn_path => [[]|] //= >. move: IH. case type_syn_path_list => [[]|] //= >.
      move/(_ _ _ _ _ eq_refl) => + [<- <-] => /[apply] //.
      case=>> [ne] [->] [->] ?; repeat eexists.
      rewrite -(PTree_union_empty_l ne); by apply incl_union_unit.
Qed.

Lemma type_expression_subst_preservation f0 fd args M:
  ∀ sze exp t ne nesz,
    f0 |- args ∈lp (sig_args (fd_sig fd)) ¤ ne ->
    args_map (fd_params fd) args = OK M ->
    fd |- sze ∈ t ¤ nesz ->
    expr_subst_idents M sze = OK exp ->
    ∃ ne', f0 |- exp ∈ t ¤ ne' /\ incl (fun _ _ => true) ne' ne.
Proof using.
  move=> +++ nargs + TARGS HM. elim/expr_ind.
  - move=> [i []] // exp t. rewrite type_Eacc. case Tp: type_syn_path => [[]|] //=.
    case P: primitive_type =>> //= [T1 T2]; rewrite T1 T2 {T1 T2} in Tp P.
    case Hi: M!i => [q|] //= [<-]. move: Hi.
    case/(args_map_spec' _ _ _ _ _ HM) => k [Nq Ni].
    have [t' [> [Nt' [Tq I]]]]:= type_syn_path_list_nth_error _ _ _ _ _ _ TARGS Nq.
    have T: t = t'.
    { move: Tp => /=. rewrite -(proj2 (fd_tenv_sig fd) _ _ Ni) Nt' /=.
      case (fd_szenv fd)!i => //= _ [] //. }
    rewrite -T in Tq. rewrite type_Eacc Tq /= P //. eauto.
  - move=> c > + [<-]; eexists; split; [eassumption|inv H; case: c H1 =>> _ [_ <-] //].
  - move=>> IH t1 t2 > /=. case T: type_expression => [[t ne]|] //=.
    case S: expr_subst_idents =>> //= + [<-] /=.
    have [> [-> /= I]]:= IH _ _ _ T S.
    case: t {T} =>> //; case: t2 =>> //; case typ_reflect => // _ [<-]; eauto.
  - move=> u k > IH > /=. case T: type_expression => [[t >]|] //=. 2: by case u.
    case S: expr_subst_idents =>> //= + [<-] /=.
    have [> [-> /= I]]:= IH _ _ _ T S.
    case: u {T} => //; case: k => //; case: t => [||[][]||[]|] //= > [<-]; eauto.
  - move=> o k > IH1 > IH2 >; cbn -[type_expression];
    case S1: expr_subst_idents => //. case S2: expr_subst_idents => // + [<-].
    cbn; case T1: type_expression => [[t1 ?]|] //; case T2: type_expression => [[t2 ?]|] //=.
    have [> [-> /= I1]]:= IH1 _ _ _ T1 S1; have [> [-> /= I2]]:= IH2 _ _ _ T2 S2.
    case typ_reflect => [_ {t2 T2}|_]; case: o; case: k => //.
    all: case: t1 {T1} => [||[][]|[]|[]|?] //; try case: t2 {T2} => [||?[]|||] > //.
    all: case=> <- _; eexists; split=> //.
    all: rewrite -(union_idempotent_unit nargs); by apply incl_union_unit.
  - move=> o k > IH1 > IH2 >; cbn -[type_expression];
    case S1: expr_subst_idents => //. case S2: expr_subst_idents => // + [<-].
    cbn; case T1: type_expression => [[t1 ?]|] //; case T2: type_expression => [[t2 ?]|] //=.
    have [> [-> /= I1]]:= IH1 _ _ _ T1 S1; have [> [-> /= I2]]:= IH2 _ _ _ T2 S2.
    case typ_reflect => [_ {t2 T2}|_]; case: o; case: k => //.
    all: case: t1 {T1} => [||[][]|[]|[]|?] //; try case: t2 {T2} => [||?[]|||] > //.
    all: case=> <- _; eexists; split=> //.
    all: rewrite -(union_idempotent_unit nargs); by apply incl_union_unit.
  - move=> o k > IH1 > IH2 >; cbn -[type_expression];
    case S1: expr_subst_idents => //. case S2: expr_subst_idents => // + [<-].
    cbn; case T1: type_expression => [[t1 ?]|] //; case T2: type_expression => [[t2 ?]|] //=.
    have [> [-> /= I1]]:= IH1 _ _ _ T1 S1; have [> [-> /= I2]]:= IH2 _ _ _ T2 S2.
    case typ_reflect => [_ {t2 T2}|_]; case: o; case: k => //.
    all: case: t1 {T1} => [||[][]|[]|[]|?] //; try case: t2 {T2} => [||?[]|||] > //.
    all: case=> <- _; eexists; split=> //.
    all: rewrite -(union_idempotent_unit nargs); by apply incl_union_unit.
Qed.

Lemma eval_expr_subst_defined f0 e0 fd args pargs vargs M:
  ∀ sze exp t ne,
    wt_env e0 f0 ->
    f0 & e0 |= args =>lp Some pargs ->
    get_env_path_list e0 pargs = Some vargs ->
    args_map (fd_params fd) args = OK M ->
    expr_subst_idents M sze = OK exp ->
    f0 |- exp ∈ t ¤ ne ->
    defined_iset ne e0.
Proof using.
  move=> ++++ WT EARGS EPARGS HM; elim=> /=.
  - case=> i [] // >; case Hm: M!i => [arg|] //= [<-].
    rewrite type_Eacc; case Targ: type_syn_path => [[targ narg]|] //=.
    case primitive_type => // - [_ <-].
    move/args_map_spec': HM => /(_ _ _ Hm) [n [Narg Nparam]].
    have [p' [Nparg Earg]]:= eval_path_list_nth_error _ _ _ _ _ _ EARGS Narg.
    have [v [Nvarg Eparg]]:= get_env_path_list_nth_error _ _ _ _ _ EPARGS Nparg.
    have [> Sarg]:= type_syn_path_get_szenv_syn_path _ _ _ _ Targ.
    by apply (subject_reduction_path _ _ _ _ _ _ _ _ WT Earg Targ Sarg Eparg).
  - case=>> [<-] [_ <-] //.
  - move=>> IH t1 t2 >; case/cdo =>> [S] [<-] /=; case/cdo => - [t ne] [T].
    have D := IH _ _ _ S T.
    case: t {T} => //; case: t2 =>> //; case typ_reflect => // _ [_ <-] //.
  - move=> op k > IH >; case/cdo =>> [S] [<-] /=.
    case: op => //; case/cdo => - [t ne] [T].
    all: have D := IH _ _ _ S T.
    all: case: k => //; case: t {T} => [||[][]||[]|] > //= - [_ <-] //.
  - move=> op k > IH1 > IH2 >; case/cdo =>> [S1] /=; case/cdo =>> [S2] [<-] /=.
    case/cdo => - [t1 n1] [T1]; case/cdo => - [t2 n2] [T2].
    have D1 := IH1 _ _ _ S1 T1; have D2 := IH2 _ _ _ S2 T2.
    have D := defined_iset_union _ _ _ D1 D2.
    case typ_reflect => _; case: op => //; case: k => //.
    all: case: t1 {T1} => [||[][]|[]|[]|] > //.
    all: try match goal with |- OK _ = OK _ -> _ => by case=> _ <- end.
    all: case: t2 {T2} => [||?[]|||] > // [_ <-] //.
  - move=> op k > IH1 > IH2 >; case/cdo =>> [S1] /=; case/cdo =>> [S2] [<-] /=.
    case/cdo => - [t1 n1] [T1]; case/cdo => - [t2 n2] [T2].
    have D1 := IH1 _ _ _ S1 T1; have D2 := IH2 _ _ _ S2 T2.
    have D := defined_iset_union _ _ _ D1 D2.
    case typ_reflect => // _; case: k => //; case: t1 {T1} => [||?[]|[]|[]|] //.
    all: case: op => // - [_ <-] //.
  - move=> op k > IH1 > IH2 >; case/cdo =>> [S1] /=; case/cdo =>> [S2] [<-] /=.
    case/cdo => - [t1 n1] [T1]; case/cdo => - [t2 n2] [T2].
    have D1 := IH1 _ _ _ S1 T1; have D2 := IH2 _ _ _ S2 T2.
    have D := defined_iset_union _ _ _ D1 D2.
    case typ_reflect => // _; case: k => //; case: t1 {T1} => [||?[]|[]|[]|] //.
    all: case: op => // - [_ <-] //.
Qed.

Lemma natlist_of_Vint64_In:
  ∀ lv ln v,
    In v lv ->
    natlist_of_Vint64 lv = Some ln ->
    ∃ n, v = Vint64 n.
Proof. elim=> //= - [] > IH > [<-|/IH] //; case natlist_of_Vint64; eauto. Qed.

Lemma wt_value_well_typed_valuelist e f:
  ∀ lt llsz lv,
    Forall3 (fun t llsz v => v ∈ ⟦e, f, t, llsz⟧) lt llsz lv ->
    well_typed_valuelist lt lv.
Proof.
  elim=> [|> IH] > T //=; inv T; econstructor.
  by apply: wt_value_well_typed_value H1.
  by apply: IH H4.
Qed.

Lemma valid_call_tests_value_not_Stuck f0 fd e0:
  forall args pargs vargs ap ne,
    wt_env e0 f0 ->
    f0 & e0 |= args =>lp Some pargs ->
    get_env_path_list e0 pargs = Some vargs ->
    f0 |- args ∈lp sig_args (fd_sig fd) ¤ ne ->
    well_typed_valuelist (sig_args (fd_sig fd)) vargs ->
    args_params (fd_params fd) args = Some ap ->
    check_szvars_structure (fn_szenv' f0) (fd_szenv fd) ap ->
    type_size_expressions fd ->
    valid_call_tests_values f0 fd e0 args <> Stuck.
Proof.
  move=> args pargs vargs ap ne WT EARGS PARGS TARGS WTARGS AP SZSTR TSZEXP.
  rewrite/valid_call_tests_values.
  have [M HM]:= args_params_args_map _ _ _ AP.
  have [t H]: ∃ tests, valid_call_tests f0 fd args = OK tests.
  { unfold valid_call_tests. rewrite HM /= AP /=.
    have HM': ∀ x, In x (fd_params fd) -> exists y, M!x = Some y.
    { move/(args_map_spec _ _ _ (fd_nodup_params fd)) in HM => x.
      case/(In_nth_error (fd_params fd)) =>> /HM. case=>> [->]. eauto. }
    have:= proj2 (fd_tenv_sig fd).
    have:= fd_szenv_params fd _.
    move: AP TARGS SZSTR {EARGS PARGS HM}.
    elim/list_ind: args {1 2 6}(fd_params fd) ap ne (sig_args (fd_sig fd)).
    - case=>> //= [<-] /=; by eexists.
    - move=> [i0 l0] args IH [//|i params] >.
      cbn -[type_syn_path args_params].
      case Tp: type_syn_path => [[t]|] //=.
      case AP: args_params =>> //= [<-] /=.
      case Targs: type_syn_path_list => [[]|] > //= [<- _] /andP [+ SZSTR] X H.
      have [tests0 ->] := IH _ _ _ _ AP Targs SZSTR
        (fun p llsz H1 H2 => X p llsz H1 (or_intror _ H2))
        (fun k => H (S k)) => /=.
      have:= eq_sym (H 0%nat i eq_refl) => /= {H}.
      case/(ex_intro (fun _ => _) t)/(proj1 (fd_tenv_fd_szenv fd i)) => llsz Si.
      rewrite Si. have:= fd_szenv_wf fd _ _ Si.
      move/(_ _ _ Si (or_introl _ eq_refl)): X => {Si}.
      have [llsz0 /= Si0']:= type_syn_path_get_szenv_syn_path _ _ _ _ Tp.
      rewrite Si0' /= {Si0'}.
      elim/list_ind: llsz0 llsz {IH} => /=.
      + case=> //=. eauto.
      + move=> lsz0 llsz0 IH [//|lsz llsz] T; inv T.
        case/andP => W Wl. case/andP => L Ll /=.
        have [> -> /=] := IH _ H2 Wl Ll.
        elim/list_ind: lsz0 lsz H1 W L {IH Wl Ll} => /=.
        * case=> //=. eauto.
        * move=> sz0 lsz0 IH [//|sz lsz] T; inv T.
          case/andP => W Wl LEN /=.
          have [> -> /=] := IH _ H3 Wl LEN.
          have [> -> /=]: exists szr, expr_subst_idents M sz = OK szr; eauto.
          eapply expr_subst_idents_Some; eassumption. }
  rewrite H. have:= valid_call_tests_nth_error _ _ _ _ _ _ H AP HM => {H}.
  elim/list_ind: t => //=.
  move=> [exp sz] t IH H. have:= H 0%nat _ _ eq_refl.
  intros ([i l] & i' & j & k & llsz & lsz & sze & llsz0 & lsz0 &
          Hqi & Si0 & Si' & J1 & K1 & J2 & K2 & Sub).
  case/get_szenv_syn_path_full: Si0 => LLSZ0 [Si0 LEN].
  have T0: f0 |- Eacc (sz, []) ∈ Tuint64 ¤ (PTree.set sz tt empty_iset).
  { have:= fn_tenv_sztype f0 _ _ Si0 => /Forall_app [_].
    move/Forall_forall/(_ _ (nth_error_In _ _ J2)).
    move/Forall_forall/(_ _ (nth_error_In _ _ K2)) => Tsz.
    have [> Ssz]:= proj1 (fd_tenv_fd_szenv f0 _) (ex_intro _ _ Tsz).
    by rewrite /= Tsz Ssz /=. }
  have [v Esz]: ∃ v, f0 & e0 |= Eacc (sz, []) => v.
  { move/(in_map_pair_fst ap): Hqi; rewrite (args_params_l _ _ _ AP).
    case/(eval_path_list_In _ _ _ _ _ EARGS) => p' [+ T]; inv T.
    move: H3; rewrite Si0; intros [= <-].
    case/(get_env_path_list_In _ _ _ _ PARGS) =>> /=.
    case Ei: e0!i => [v0|] //= _.
    have:= WT _ _ Ei; rewrite Si0 => - [t0] [_] [Ti0] [[<-]].
    have: In lsz0 (LLSZ0 ++ llsz0).
    { apply in_or_app; right; by apply: nth_error_In J2. }
    elim: t0 v0 (LLSZ0 ++ llsz0) {Ei Ti0 H4} => [|||||> IH'] > + T; inv T =>> //= [].
    - intros ->; move/eval_identlist_nth_error: EVlsz => /(_ _ _ K2) [> [N E]].
      have [_ [> [_ [Ssz _]]]]:= WT _ _ E.
      eexists; econstructor.
      + econstructor; [eassumption|econstructor].
      + by rewrite/= E.
      + by have [> ->]:= natlist_of_Vint64_In _ _ _ (nth_error_In _ _ N) SHAPE.
    - move: WTvex => /[swap]; by apply: IH'. }
  case Sp: split => [le lx]. cbn -[eval_expr].
  case: v Esz => [v|] Esz.
  - have [T _]:= subject_reduction_expr _ _ _ _ _ _ WT Esz T0; inv T.
    rewrite (proj1 (eval_expr_fixpoint_match_Some _ _ _ _) Esz) /=.
    have:= in_map snd _ _ Hqi => /=. rewrite (args_params_r _ _ _ AP).
    move/forallb_forall: TSZEXP => /[apply]. rewrite Si'.
    move/forallb_forall/(_ _ (nth_error_In _ _ J1)).
    move/forallb_forall/(_ _ (nth_error_In _ _ K1)).
    case Tsze: type_expression => [[[|||[]||]]|] //= _.
    have [ne' [Texp Iexp]]:= type_expression_subst_preservation
                              _ _ _ _ _ _ _ _ _ TARGS HM Tsze Sub.
    have D := eval_expr_subst_defined _ _ _ _ _ _ _ _ _ _ _ WT EARGS PARGS HM Sub Texp.
    have [[v|] Eexp]:= progress_expr _ _ _ _ _ WT Texp D.
    + have [T _]:= subject_reduction_expr _ _ _ _ _ _ WT Eexp Texp.
      rewrite (proj1 (eval_expr_fixpoint_match_Some _ _ _ _) Eexp) /=.
      inv T => /=.
      have:= IH (fun n => H (S n)). rewrite Sp.
      by do 2 case eval_expr_list_to_int64 =>> //.
    + rewrite (proj1 (eval_expr_fixpoint_match_None _ _ _) Eexp) /=.
      have:= IH (fun n => H (S n)). rewrite Sp.
      by case eval_expr_list_to_int64.
  - by rewrite (proj1 (eval_expr_fixpoint_match_None _ _ _) Esz).
Qed.

Lemma count_occ_In {A: Type} (eq_dec: forall x y: A, {x = y} + {x <> y}):
  forall (l: list A) x,
    In x l <-> (count_occ eq_dec l x > 0)%nat.
Proof.
  intros; split.
  - by move/(count_occ_In eq_dec)/ltP.
  - by move/ltP/count_occ_In.
Qed.

Lemma count_occ_2 {A: Type} (dec: forall x y, {x = y} + {x <> y}):
  forall (l: list A) (a: A),
    (count_occ dec l a >= 2)%nat <->
    exists n1 n2, n1 <> n2 /\ nth_error l n1 = Some a /\
                         nth_error l n2 = Some a.
Proof.
  move=> l a. split => H.
  - have H': (count_occ dec l a > 0)%nat by lia.
    have [n1 N1] := In_nth_error _ _ (proj2 (count_occ_In dec l a) H').
    move: H. have [l1 [l2 [T Hn1]]] := nth_error_split _ _ N1.
    rewrite T in N1. rewrite T. rewrite count_occ_elt_eq // => X.
    have X': (count_occ dec (l1 ++ l2) a > 0)%nat by lia.
    have [n2] := In_nth_error _ _ (proj2 (count_occ_In dec _ a) X').
    case (PeanoNat.Nat.le_gt_cases (length l1) n2).
    + move=> /[dup] Hn2 /nth_error_app2 => -> N2.
      exists n1, (S n2). repeat split => //. lia.
      rewrite nth_error_app2. lia. by rewrite PeanoNat.Nat.sub_succ_l.
    + move=> /[dup] Hn2 /nth_error_app1 => -> N2.
      exists n1, n2. repeat split => //. lia. by rewrite nth_error_app1.
  - case: H => n1 [n2 [NEQ [N1 N2]]].
    have [l1 [l2 [T Hn1]]] := nth_error_split _ _ N1.
    rewrite T in N1 N2. rewrite T. rewrite count_occ_elt_eq //.
    unfold ge. have: (0 < count_occ dec (l1 ++ l2) a)%nat => [|//]. move: N2.
    case X: (length l1 <= n2)%nat; move: X.
    + move/leP.
      move=> /[dup] Hn2 /nth_error_app2 => ->.
      case X: (n2 - length l1)%coq_nat => /=. lia.
      move/(nth_error_In l2)/(proj1 (count_occ_In dec _ _)).
      rewrite count_occ_app. lia.
    + move=> ?; have: (n2 < length l1)%nat => [//|]. lia.
      move=> /[dup] Hn2 /ltP/nth_error_app1 => ->.
      move/(nth_error_In l1)/(proj1 (count_occ_In dec _ _)).
      rewrite count_occ_app. lia.
Qed.

Lemma count_occ_incl {A: Type} (dec: forall x y, {x = y} + {x <> y}):
  forall (l l': list A),
    (forall a, count_occ dec l a >= count_occ dec l' a)%nat ->
    List.incl l' l.
Proof.
  move=> l l' H a. move/(proj1 (count_occ_In dec _ _)) => X.
  apply: (proj2 (count_occ_In dec _ _)). have:= H a. lia.
Qed.

Lemma arg_aliasing_restricted {A: Type} (P: A -> A -> bool)
                              (dec: forall x y, {x = y} + {x <> y}):
  forall arg args args',
    (forall a, count_occ dec args a >= count_occ dec args' a)%nat ->
    arg_aliasing P arg args' ->
    arg_aliasing P arg args.
Proof.
  move=> arg args args' CINCL /arg_aliasing_spec.
  have INCL := count_occ_incl dec _ _ CINCL.
  case=> [n1 [n2 [x [y [NEQ [N1 [N2 [P1 P2]]]]]]]].
  move: N1 N2. case (dec x y).
  - move=> <- N1 N2.
    have H := proj2 (count_occ_2 dec _ _)
                    (ex_intro _ n1 (ex_intro _ n2 (conj NEQ (conj N1 N2)))).
    have: (count_occ dec args x >= 2)%nat by have:= CINCL x; lia.
    case/(proj1 (count_occ_2 dec _ _)) => n1' [n2' [NEQ' [N1' N2']]].
    apply arg_aliasing_spec. exists n1', n2', x, x. repeat split => //.
  - move=> H N1 N2.
    have [n1' N1'] := In_nth_error _ _ (INCL _ (nth_error_In _ _ N1)).
    have [n2' N2'] := In_nth_error _ _ (INCL _ (nth_error_In _ _ N2)).
    have H': n1' <> n2'.
    { move=> T. rewrite -T N1' {T} in N2'. by case: N2'. }
    apply: (proj2 (arg_aliasing_spec _ _ _)).
    exists n1', n2', x, y. by repeat split.
Qed.

Lemma args_aliasing_restricted {A: Type} (P: A -> A -> bool)
                               (dec: forall x y, {x = y} + {x <> y}):
  forall args args' pp,
    (forall a, count_occ dec args a >= count_occ dec args' a)%nat ->
    args_aliasing P args' pp ->
    args_aliasing P args pp.
Proof.
  move=> args args' + CINCL. elim/list_ind => //=.
  move=> [p i] pp IH. case/orP.
  - move/(arg_aliasing_restricted P dec _ _ _ CINCL) => -> //.
  - move/IH => ->. by rewrite orbT.
Qed.

Lemma args_aliasing_restricted' {A: Type} (P: A -> A -> bool):
  forall args pp pp',
    List.incl pp' pp ->
    List.incl (map fst pp) args ->
    args_aliasing P args pp' ->
    args_aliasing P args pp.
Proof.
  move=> args pp pp' INCLpp' INCLpp.
  have:= incl_map fst INCLpp' => /incl_tran/(_ INCLpp).
  move/args_aliasing_spec/[apply].
  case=> n1 [n2 [n3 [p1 [p2 [p [id [Hneq [N1 [N2 [N3 [P1 P2]]]]]]]]]]].
  have [n3' N3'] := In_nth_error _ _ (INCLpp' _ (nth_error_In _ _ N3)).
  apply args_aliasing_spec => //.
  exists n1, n2, n3', p1, p2, p, id. repeat split => //.
Qed.

Lemma args_params_mut_own_incl {A: Type}:
  forall pe params (pargs: list A) pp,
    args_params params pargs = Some pp ->
    List.incl (map fst (mut_args pe pp ++ own_args pe pp)) pargs.
Proof.
  intros * PP.
  have:= args_params_list_incl_l _ _ _ PP.
  have:= incl_map fst (incl_app (mut_args_incl pe pp) (own_args_incl pe pp)).
  by move/incl_tran/[apply].
Qed.

Lemma mut_args_fst_count_incl {A: Type} dec pe:
  forall (pargs: list A) params pp,
    args_params params pargs = Some pp ->
    forall a, (count_occ dec pargs a >= count_occ dec (map fst (mut_args pe pp)) a)%nat.
Proof.
  rewrite/mut_args. elim/list_ind => /=.
  - case=> //= > [<-] > //.
  - move=> p pargs IH [//|i params] pp.
    case PP: args_params => //= - [<-] a /=.
    have H := IH _ _ PP. case (dec p a) => [<-|NEQ].
    + case pe!i => [[]|] //=.
      2: case (dec p p) => // _. all: have:= H p; lia.
    + case pe!i => [[]|] //=. by case (dec p a).
Qed.

Lemma no_args_aliasing_restrict_mut {A: Type} {P: A -> A -> bool}
                                    (dec: forall (x y: A), {x = y} + {x <> y}):
  forall pe params pargs pp,
    args_params params pargs = Some pp ->
    ~~ args_aliasing P pargs (mut_args pe pp ++ own_args pe pp) ->
    ~~ args_aliasing P (map fst (mut_args pe pp)) (mut_args pe pp).
Proof.
  intros * => /[dup] PP /(args_params_mut_own_incl pe params pargs pp) I.
  move/negbTE => NOALIAS; move/negP/negP in NOALIAS.
  have NOALIAS' := contra
    (args_aliasing_restricted' _ _ _ _ (incl_appl _ (List.incl_refl _)) I) NOALIAS.
  have CINCLm  := mut_args_fst_count_incl dec pe _ _ _ PP.
  exact (contra (args_aliasing_restricted _ dec _ _ _ CINCLm) NOALIAS').
Qed.

Lemma mut_own_args_fst_count_incl {A: Type} dec pe:
  forall (pargs: list A) params pp,
    args_params params pargs = Some pp ->
    forall a, (count_occ dec pargs a >=
          count_occ dec (map fst (mut_args pe pp ++ own_args pe pp)) a)%nat.
Proof.
  rewrite/mut_args. elim/list_ind => /=.
  - case=> //= > [<-] > //.
  - move=> p pargs IH [//|i params] pp.
    case PP: args_params => //= - [<-] a /=.
    have H := IH _ _ PP. case (dec p a) => [<-|NEQ].
    + case pe!i => [[]|] //=. 3: rewrite map_app /= count_occ_elt_eq // -map_app.
      2: case (dec p p) => // _. all: have:= H p; lia.
    + case pe!i => [[]|] //=. by case (dec p a).
      rewrite map_app /= count_occ_elt_neq // -map_app //.
Qed.

Lemma valid_env_szenv_verified e f:
  ∀ args pargs vargs,
    wt_env e f ->
    f & e |= args =>lp Some pargs ->
    get_env_path_list e pargs = Some vargs ->
    valid_env_szenv e (fn_szenv' f) (map fst args).
Proof.
  move=> +++ WT; rewrite/valid_env_szenv; elim=> [|[i l] > IH] > /= E; inv E => //=.
  case/cdoo => v [Ep']; case/cdoo => lv [Elp'] _.
  move=>> [<-|/ltac: (apply: IH; eassumption)].
  inv H2; rewrite H4 {Elp' H3 H5} => - [<-].
  case/cdoo: Ep' => v0 [Ei] _.
  have:= WT _ _ Ei; rewrite H4 {H4 Ei} => - [t0] [_] [_] [[<-]].
  elim: t0 v0 llsz => [||||[]|> IH'] > T; inv T; econstructor.
  2: apply: IH'; eassumption.
  apply Forall_nth => j d Hj.
  exploit (eval_identlist_nth_error _ _ _ _ EVlsz j).
  { by apply List.nth_error_nth' with (d := d). }
  case=>> [] /(@nth_error_In value)/natlist_of_Vint64_In => /(_ _ SHAPE).
  case=>> ->; eauto.
Qed.

Lemma remove_own_params_remove_list:
  forall l e,
    remove_own_params l e = remove_list (map fst (map fst l)) e.
Proof. elim=> [|[[]]] > //= IH >; by rewrite IH remove_remove_list. Qed.

Lemma wt_env_remove_own_params f:
  forall l e,
    wt_env e f ->
    Forall (fun x => (fn_penv f)!x = Some Owned) (map fst (map fst l)) ->
    wt_env (remove_own_params l e) f.
Proof.
  move=> l e >; rewrite remove_own_params_remove_list.
  move=> H P >; case case_remove_list => // _.
  case/H =>> [>] [->] [/[dup] Si ->] W; repeat eexists.
  elim: l e W P {H} => // - [[x ?] ?] > IH > /= W P; inv P.
  apply: IH H2.
  apply wt_value_remove => //.
  apply Forall_forall =>> I1; apply Forall_forall =>> I2 EQ.
  have:= fn_penv_szenv' f _ _ Si.
  move/Forall_forall/(_ _ I1)/Forall_forall/(_ _ I2).
  by rewrite -EQ H1.
Qed.

Lemma build_env_bound_ident {A: Type}:
  forall (l: list (ident * A)) x,
    In x (map fst l) ->
    exists v, (build_env (PTree.empty A) l)!x = Some v.
Proof.
  move=> l x Hin.
  have:= or_introl (In x (map fst (PTree.elements (PTree.empty A)))) Hin.
  move: l x (PTree.empty A) {Hin}. elim/list_ind => /=.
  - move=> x t [//|] /(in_map_fst_pair (PTree.elements t)) [v].
    move/(PTree.elements_complete t). by exists v.
  - move=> [i v] l IH x t /=. case (Pos.eqb_spec i x) => [<-|].
    2: move=> NEQ [[//|]|].
    + move=> _. apply IH. right.
      have:= @PTree.elements_correct _ (PTree.set i v t) i v.
      rewrite PTree.gss => /(_ eq_refl). by apply: in_map_pair_fst.
    + move=> Hin. apply IH. by left.
    + move=> Hin. apply IH. right.
      case/(in_map_fst_pair (PTree.elements t)): Hin => v'.
      move/(PTree.elements_complete t) => Hx.
      have:= @PTree.elements_correct _ (PTree.set i v t) x v'.
      rewrite PTree.gso. by apply not_eq_sym. move/(_ Hx) => Hin.
      by apply (in_map_pair_fst _ _ v').
Qed.

Lemma external_call_update_env_Some:
  forall ef e lp pp lv vmarrs,
    pargs_params (ef_params ef) lp = Some pp ->
    get_env_path_list e lp = Some lv ->
    length (mut_args (ef_penv ef) pp) = length vmarrs ->
    all_root_paths (own_args (ef_penv ef) pp) ->
    ~~ args_aliasing sem_path_alias (map fst (mut_args (ef_penv ef) pp ++
                                     own_args (ef_penv ef) pp))
                                    (mut_args (ef_penv ef) pp) ->
    exists e', update_env (build_env PTree.Empty
                            (combine (map snd (mut_args (ef_penv ef) pp)) vmarrs))
                     (remove_own_params (own_args (ef_penv ef) pp) e)
                     (mut_args (ef_penv ef) pp) = Some e'.
Proof.
  move=> ef e lp0 pp lv vmarrs. set pe := ef_penv ef => PP Elp LEN ROOT NOALIAS.
  have dec := fun x y => Bool.reflect_dec _ _ (eqSemPath x y).
  have AMM: ¬ pargs_params_aliasing (map fst (mut_args pe pp)) (mut_args pe pp).
  { move/negP in NOALIAS. move=> ALIAS. apply: NOALIAS.
    apply (args_aliasing_restricted _ dec _ (map fst (mut_args pe pp))) => //.
    move=>>. rewrite map_app count_occ_app. lia. }
  have AMO: ¬ pargs_params_aliasing (map fst (own_args pe pp)) (mut_args pe pp).
  { move/negP in NOALIAS. move=> ALIAS. apply: NOALIAS.
    apply (args_aliasing_restricted _ dec _ (map fst (own_args pe pp))) => //.
    move=>>. rewrite map_app count_occ_app. lia. }
  apply update_env_Some => //. move: PP Elp NOALIAS. set OWN := own_args pe pp.
  have:= List.incl_refl (mut_args pe pp). set MUT := {2 6} (mut_args pe pp).
  move: {1 2}lp0 (ef_params ef) {1 2 3 4 5}pp lv. elim/list_ind => /=.
  - case=>> // _ [<-] //.
  - move=> p lp IH [//|x params] >. case PP: pargs_params => [pp'|] //=.
    case Ep: get_env_path => //=. case Elp: get_env_path_list => //=.
    rewrite/mut_args => /[swap]. move=> [<-] + _ => /=.
    case Hpe: pe!_ => [[]|] //= INCL.
    + by move/(IH _ _ _ INCL PP Elp).
    + rewrite sem_path_alias_refl.
      have [IN INCL'] := List.incl_cons_inv INCL.
      case/norP => A1 /negP A2. move=> q j. case.
      * case=> <- <-. split.
        ** apply build_env_bound_ident. rewrite ListUtils.combine_map_fst.
           by rewrite map_length. exact (in_map_pair_snd _ _ _ IN).
        ** case: {1 2 3}p A1 Ep => i l /= /negP A. rewrite remove_own_params_other.
           { move=> i' l' j' Hin T. rewrite T in Hin. apply: A.
             apply ListUtils.mem_spec. exists (i, l'). split.
             rewrite map_app. apply in_or_app. right. by apply (in_map_pair_fst _ _ j').
             have:= proj1 (forallb_forall _ _) ROOT _ Hin. case l' => //= _.
             rewrite Pos.eqb_refl /=. by rewrite orbT. }
           eauto.
      * have A2': ~~ args_aliasing sem_path_alias (map fst (mut_args pe pp' ++ OWN))
                                                  (mut_args pe pp').
        { case H: args_aliasing => //=. elim: A2.
          apply (args_aliasing_restricted _ dec _ (map fst (mut_args pe pp' ++ OWN))).
          move=>> /=. rewrite/mut_args. case dec => _; lia. easy. }
        by apply (IH _ _ _ INCL' PP Elp A2').
    + by move/(IH _ _ _ INCL PP Elp).
    + by move/(IH _ _ _ INCL PP Elp).
Qed.

Lemma update_env_primitive_value_not_prefix_no_alias:
  ∀ e m e0 e' p v,
    update_env e e0 m = Some e' ->
    (∀ q j, sem_path_prefix q p -> ~ In (q, j) m) ->
    get_env_path e0 p = Some v ->
    primitive_value v ->
    ~~ mem sem_path_alias p (map fst m).
Proof.
  move=> e +++ p v +++ PRIM; elim=> [|[[i l] j] m IH] e0 e' //= + P E.
  case/cdoo => v1 [Ej]; case E0i: _!i => [v0|] //=.
  case UPDV: update_value => [v0'|] //= UPDE.
  have NOALIAS: ~~ sem_path_alias p (i, l).
  { eapply introN. exact idP.
    case: p E P {IH} => i0 l0 E P.
    rewrite/sem_path_alias; case/andP => /Pos.eqb_spec; intros ->.
    case/orP; last first.
    { have:= P (i, l) j _ (or_introl _ eq_refl) => /=; by rewrite Pos.eqb_refl /=. }
    case/(prefix_cut _ sem_path_elem_reflect) => l' EQ.
    move: P; case/update_value_get_value_path': UPDV =>>; rewrite EQ {EQ}.
    case/get_value_path_app =>>.
    move: E; rewrite /= E0i /= => -> [[<-]].
    case: l' => [|[]] /=. 2: by case: v PRIM.
    rewrite app_nil_r; move=> _ /(_ (i, l0) j _ (or_introl _ eq_refl)) => X.
    apply: X => /=; by rewrite Pos.eqb_refl (prefix_refl _ sem_path_elem_reflect). }
  exploit (IH _ _ UPDE).
  { move=>> /P X I; apply: X; right; eassumption. }
  { apply (update_env_spec_1 e [(i, l, j)] e0) => //; last first.
    { by rewrite /= Ej E0i /= UPDV. }
    case/mem_spec => _ /= [[] // <-].
    eapply elimN; by [exact idP|]. }
  move/negPf => ->; by rewrite orbF.
Qed.

Lemma same_structure_values_refl:
  ∀ v, same_structure_values v v.
Proof.
  elim/value_ind' =>> //= IH.
  apply forallb2_forall; split=> // > /[dup] N -> [<-].
  exact: proj1 (Forall_forall _ _) IH _ (nth_error_In _ _ N).
Qed.

Lemma same_structure_values_trans:
  ∀ v1 v2 v3,
    same_structure_values v1 v2 ->
    same_structure_values v2 v3 ->
    same_structure_values v1 v3.
Proof.
  elim/value_ind'=> [|?|?|?|?|?|l1 IH] // [] // l2 [] //= l3.
  case/forallb2_forall => L1 H1. case/forallb2_forall => L2 H2.
  apply forallb2_forall; split.
  { by rewrite -L2. }
  move=> n > N1 N3.
  have [> N2]: ∃ v, nth_error l2 n = Some v.
  { case N: nth_error; eauto.
    move/nth_error_None: N; rewrite -L1.
    move/nth_error_None; by rewrite N1. }
  have:= H2 _ _ _ N2 N3; have:= H1 _ _ _ N1 N2.
  apply (proj1 (Forall_forall _ _) IH).
  exact (nth_error_In _ _ N1).
Qed.

Lemma same_structure_values_comm:
  ∀ v1 v2,
    same_structure_values v1 v2 = same_structure_values v2 v1.
Proof.
  elim/value_ind' => [|?|?|?|?|?|lv1 IH] [] //= lv2.
  elim: lv1 lv2 IH => [|> IH] [] //= > T; inv T; by rewrite H1 (IH _ H2).
Qed.

Lemma valid_cont_return ge f0:
  ∀ e e0 m e' k ne_n ne_e ne_r ne'_n,
    ~ pargs_params_aliasing (map fst m) m ->
    update_env e e0 m = Some e' ->
    structure_preserved e0 e m ->
    at_least_mutable f0 m ->
    valid_cont ge f0 e0 ne_n ne_e ne_r k = OK ne'_n ->
    valid_cont ge f0 e' ne_n ne_e ne_r k = OK ne'_n.
Proof.
  move=> e e0 m e' +++++ NOALIAS UPDE STR0 MMUT.
  elim=> [|?? IH|? IH|????? IH] [|??] [|??] [|??] > //=.
  - case/cdo =>> [/IH ->] //.
  - destruct_match_goal; by case/cdo =>> [/IH ->].
  - case/cdo =>> [->] /=.
    case E: (_ && _) => // - [<-]; move: E.
    repeat case/andP. move=> -> STR -> -> -> -> -> -> /=.
    set X := structure_preserved _ _ _.
    suff: X by move=> ->.
    rewrite/X {X}.
    apply forallb_forall => - [p j].
    move/forallb_forall: STR => /[apply].
    case/cdob => v [Ei].
    have [v' Ei']:= update_env_spec_3'' _ _ _ _ _ _ Ei UPDE; rewrite Ei' /=.
    case I: (mem sem_path_prefix (j, []) (map fst m)); last first.
    { have H: ∀ q, ~ (In q (map fst m) /\ sem_path_prefix (j, []) q).
      { move=>> H.
        have:= proj2 (mem_spec sem_path_prefix _ _) (ex_intro _ _ H).
        by rewrite I. }
      move/negP in I.
      have:= update_env_spec_1 _ _ _ _ (j, []) v _ I UPDE => /=.
      rewrite Ei Ei' /= => - /(_ eq_refl) [->] //. }
    case (get_env_path _ p) => //= v0.
    have: ∀ q x, sem_path_prefix q (j, []) -> q <> (j, []) -> ~ In (q, x) m.
    { case=> ? ls > /=; case/andP => /Pos.eqb_spec ->; by case: ls. }
    have: get_env_path e' (j, []) = Some v' by rewrite /= Ei'.
    have: get_env_path e0 (j, []) = Some v by rewrite /= Ei.
    move=> E0 E' PREC H.
    have X: ∃ lv, v = Varr lv; [|case: (X) H => _ {1}-> H].
    { case: v H {Ei E0} =>> //; by eexists. }
    have: ∃ lv, v0 = Varr lv; [|move: H => /[swap] - [_ {1 3}->] H].
    { case: v0 H =>> //; by eexists. }
    have: ∃ lv, v' = Varr lv; [|case=> _ {1}->].
    { case: X E0 =>> -> E0.
      case (mem_dec_spec _ eqSemPath (j, []) (map fst m)).
      + case/(in_map_fst_pair m) => x J.
        move/forallb_forall: STR0 => /(_ _ J).
        rewrite E0 /=; case Ej: e!x =>> //=.
        have:= update_env_spec_2 _ _ _ _ NOALIAS UPDE (j, []) x _ J Ej.
        rewrite E' => - [<-]; case v' => //; by eexists.
      + move=> J. have SUB: ~~ mem sub_path (j, []) (map fst m).
        { case negP => //; elim=> /mem_spec [[? l'] [J']] /=.
          case: Pos.eqb_spec J' => //= ->; by case: l'. }
        have [>]:= update_env_spec_3'bis _ _ _ _ (j, []) _ UPDE SUB E0.
        rewrite E' => - [[->] _]; by eexists. }
    elim/value_ind': v v' [] v0 I E0 E' PREC H {Ei Ei' IH X} => [|b|n|n|f|f|lv IH].
    1-6: move=> v' ls >.
    1-6: case (mem_dec_spec _ eqSemPath (j, ls) (map fst m)) => I PREF V + PREC;
         [ (* Paths in (map fst m) point to an array value
              (stated in structure_preserved). *)
           case/(@in_map_fst_pair sem_path): I => x I;
           move/forallb_forall: STR0 => /(_ _ I);
           case/cdob => vj [Ej]; rewrite V /=; by case: vj {Ej}
         | (* (i, l) is not in (map fst m), the value is primitive and
               an invariants states that (i, l) is a prefix for some
               path in (map fst m). This is absurd because update_env
               could not return Some in this case. *)
           set d := (X in _ = Some X) in V;
           suff: get_env_path e' (j, ls) = Some d; [move=> -> [<-] //|];
           apply (update_env_spec_1 _ _ _ _ (j, ls)) with (3 := UPDE) => //;
           apply: elimN; [exact idP|];
           apply: update_env_primitive_value_not_prefix_no_alias UPDE _ V eq_refl;
           move=>> X J; apply: PREC X _ _; [|eassumption];
           move=> E; apply: I; move/(in_map fst): J => /=; by rewrite E].
    move=> v' ls v0.
    case (mem_dec_spec _ eqSemPath (j, ls) (map fst m)) => I PREF V + PREC.
    + case/(@in_map_fst_pair sem_path): I => x I.
      move/forallb_forall: STR0 => /(_ _ I).
      rewrite V; case Ex: e!x => [[]|] //.
      rewrite (update_env_spec_2 _ _ _ _ NOALIAS UPDE _ _ _ I Ex) => + [<-].
      by apply same_structure_values_trans.
    + case: v0 => // ls0 P /= H.
      have SUB: ~~ mem sub_path (j, ls) (map fst m).
      { case negP => //; elim=> /mem_spec [[i' l']] [/[swap]] /=.
        case Pos.eqb_spec => //= -> {i'}.
        case (list_reflect _ sem_path_elem_reflect ls l') => [<- //|?] X.
        case/(in_map_fst_pair m) =>>; apply: PREC => /=.
        by rewrite Pos.eqb_refl. congruence. }
      have:= update_env_spec_3'bis _ _ _ _ (j, ls) _ UPDE SUB V.
      rewrite P => - [>] [+ LEN']; intros [= ->] => /=.
      case/forallb2_forall: H => LEN H.
      apply forallb2_forall; split. { congruence. }
      move=> n v1 v2 N1 N2.
      have Hn: (n < length lv)%coq_nat.
      { rewrite LEN; apply List.nth_error_Some; by rewrite N2. }
      have P': get_env_path e0 (j, ls ++ [Pcell n]) = Some (nth n lv Vundef).
      { move: V => /=; case e0!_ =>> //= V; apply get_value_path_app.
        eexists; split. eassumption. by rewrite /= (List.nth_error_nth' _ Vundef). }
      case PREF': (mem sem_path_prefix (j, ls ++ [Pcell n]) (map fst m)).
      * apply (proj1 (Forall_nth _ _) IH n Vundef Hn _ (ls ++ [Pcell n])) => //.
        { move: P => /=; case e'!_ =>> //= P; apply get_value_path_app.
          eexists; split. eassumption. by rewrite /= N1. }
        { case=> ? ls' x /=; case: Pos.eqb_spec => //= -> + NEQ.
          move/(prefix_app_not_eq _ sem_path_elem_reflect) => - X.
          exploit X => {X}. { intro S; elim: NEQ; by rewrite S. }
          case (list_reflect _ sem_path_elem_reflect ls' ls) => [-> _|NEQ' X].
          { by move/(in_map_pair_fst m). }
          { apply: PREC. by rewrite /= Pos.eqb_refl X. congruence. } }
        { move: N2; have:= List.nth_error_nth' lv Vundef Hn; by apply: H. }
      * move/negP/negP in PREF'.
        have X:= update_env_spec_1 _ _ _ _ (j, ls ++ [Pcell n]) _ P' _ UPDE.
        exploit X => {X}.
        { case/mem_spec => [[>]] [] /=.
          case Pos.eqb_spec => //= <- J; case/orP.
          ** move=> X; move/negP/mem_spec: PREF'; elim.
             eexists; split. eassumption. by rewrite /= Pos.eqb_refl X.
          ** move=> X; move/negP/mem_spec: SUB; elim.
             eexists; split. eassumption. rewrite /= Pos.eqb_refl.
             rewrite (prefix_app_not_eq _ sem_path_elem_reflect _ _ (Pcell n)) //.
             move=> E; rewrite E {E} in J.
             move/negP/mem_spec: PREF'; elim.
             eexists; split. eassumption.
             by rewrite /= Pos.eqb_refl (prefix_refl _ sem_path_elem_reflect). }
        move: P => /=; case e'!_ =>> //= + /get_value_path_app => -> [_] [[<-]].
        rewrite /= N1 => - [->]; apply (H n) => //. by apply List.nth_error_nth'.
Qed.

Lemma defined_iset_remove_own_params:
  ∀ f' ne'_n e pp,
    defined_iset ne'_n e ->
    existsb (λ '(i, _, _),
      match ne'_n ! i with
      | Some tt => true
      | None => false
      end) (own_args (fd_penv (Internal f')) pp) = false ->
    defined_iset ne'_n (remove_own_params (own_args (fn_penv f') pp) e).
Proof.
  intros * D H =>> /[dup] Hi /D [v E]; exists v.
  rewrite remove_own_params_remove_list.
  case case_remove_list => //.
  case/(in_map_fst_pair (map fst (own_args (fn_penv f') pp))) =>>.
  case/(in_map_fst_pair (own_args (fn_penv f') pp)) =>>.
  case/(In_nth _ _ (1%positive, [], 1%positive)) =>> [Hn N].
  have:= existsb_nth _ _ (1%positive, [], 1%positive) Hn H.
  by rewrite N Hi.
Qed.

Lemma defined_iset_return ne e1:
  ∀ m e e',
    defined_iset ne e ->
    update_env e1 e m = Some e' ->
    defined_iset ne e'.
Proof.
  elim=> [|[[]] > IH] > H /=.
  - by case=> <-.
  - case/cdoo =>> [_] //=; case/cdoo =>> [] //=.
    do 2 case/cdoo =>> [_] //=. case=> <-. apply: IH.
    move=>> /H; case case_set => //=; eauto.
Qed.



Axiom external_call_valid_updated_env:
  forall f e ef pargs vargs vmarrs r pp e',
    wt_env e f ->
    pargs_params (ef_params ef) pargs = Some pp ->
    external_call ef (combine pargs vargs) = (vmarrs, r) ->
    let e1 := build_env PTree.Empty
                (combine (map snd (mut_args (ef_penv ef) pp))
                vmarrs) in
    update_env e1 e (mut_args (ef_penv ef) pp) = Some e' ->
    wt_env e' f /\
    structure_preserved e e1 (mut_args (ef_penv ef) pp).
Open Scope outcome_scope.

Ltac cbn' := cbn -[get_env_path].

Lemma eval_expr_preserved_by_call e0 f0 f:
  forall args pargs vargs pp M e sz szr v,
    args_map (fn_params f) args = OK M ->
    SemanticsNonBlocking.eval_path_list e0 f0 args (Some pargs) ->
    get_env_path_list e0 pargs = Some vargs ->
    build_func_env (PTree.empty value) (fn_params f) vargs = Some e ->
    pargs_params (fn_params f) pargs = Some pp ->
    same_szvars_structure f0 f pp ->
    (* wf_size_expr sz -> *)
    expr_subst_idents M sz = OK szr ->
    eval_expr e0 f0 szr = Val v ->
    eval_expr e f sz = Val v.
Proof.
  move=> args pargs vargs pp M e sz szr n.
  move/args_map_spec' => HM EARGS PARGS He PP SZSTR.
  elim/expr_ind: sz szr n => /=.
  - move=> [i []] //= >. case Hi: M!i => [[i0 l0]|] //= [<-]; cbn'.
    change (fix eval_expr (exp: expr) := _
            with eval_syn_path_elem lsz (s: syn_path_elem) := _
            for eval_syn_path_elem) with (ES.eval_path_elem e0 f0).
    change (fix __f l1 (m: list syn_path_elem) := _) with (eval_path_elem_list e0 f0).
    case Si0: (fn_szenv' f0)!i0 => [|] //. have [n [Ni0 Ni]] := HM _ _ Hi.
    have [LEN] := fn_tenv_sig f => /(_ n i Ni).
    case N: nth_error => [t|]; cbn' => /(@eq_sym (option typ)).
    2: { move: N => /nth_error_None; rewrite -LEN => /nth_error_None.
         by rewrite Ni. }
    have [p [Np Ep]]:= eval_path_list_nth_error _ _ _ _ _ _ EARGS Ni0.
    have:= args_params_nth_error' _ _ _ _ PP Np Ni => /(nth_error_In pp) Hpi.
    have:= get_szenv_syn_path_get_szenv_path _ _ (fn_szenv' f0) _ _ Ep.
    move/(@eq_sym (option (list (list ident)))) => Sp.
    move/(ex_intro (fun _ => _) t)/fn_tenv_fn_szenv' => [llsz Si].
    have [llsz0 Ep'] := eval_path_get_szenv_syn_path _ _ _ _ Ep.
    have:= proj1 (eval_path_fixpoint_match_Some _ _ _ _) Ep; cbn'. rewrite Si0; cbn'.
    inv Ep. move: H2; rewrite Si0 => - [T]; rewrite T in Si0. rewrite !T {T}.
    case El0: eval_path_elem_list => [l0'| |] //; cbn' => - [T].
    rewrite T in El0. rewrite T Si {T}; cbn'.
    have -> : match llsz with | [] | _ => Val [] end = Val [] by case llsz.
    case E0: get_env_path => [v|] //=. have -> //: e!i = Some v.
    apply (build_func_env_spec _ _ _ _ (fn_nodup_params f) He _ _ _ Ni).
    have [v0 [->]]:= get_env_path_list_nth_error _ _ _ _ _ PARGS Np. by rewrite E0.
  - move=>> [<-] //.
  - move=>> IH >. case S: expr_subst_idents =>> //= [<-]; cbn.
    case E: eval_expr => //=. by rewrite (IH _ _ S E).
  - move=>> IH >. case S: expr_subst_idents =>> //= [<-]; cbn.
    case E: eval_expr => //=. by rewrite (IH _ _ S E).
  - move=>> IH1 > IH2 >. case S1: expr_subst_idents =>> //=.
    case S2: expr_subst_idents =>> //= [<-]; cbn.
    case E1: eval_expr => //=. case E2: eval_expr => //=.
    by rewrite (IH1 _ _ S1 E1) (IH2 _ _ S2 E2).
  - move=>> IH1 > IH2 >. case S1: expr_subst_idents =>> //=.
    case S2: expr_subst_idents =>> //= [<-]; cbn.
    case E1: eval_expr => //=. case E2: eval_expr => //=.
    by rewrite (IH1 _ _ S1 E1) (IH2 _ _ S2 E2).
  - move=>> IH1 > IH2 >. case S1: expr_subst_idents =>> //=.
    case S2: expr_subst_idents =>> //= [<-]; cbn.
    case E1: eval_expr => //=. case E2: eval_expr => //=.
    by rewrite (IH1 _ _ S1 E1) (IH2 _ _ S2 E2).
Qed.

Lemma wt_value_path_list_type_syn_path_list e (f: function):
  wt_env e f ->
  ∀ args pargs targs ne sargs vargs,
    f |- args ∈lp targs ¤ ne ->
    get_szenv_syn_path_list (fn_szenv' f) args = Some sargs ->
    f & e |= args =>lp Some pargs ->
    get_env_path_list e pargs = Some vargs ->
    wt_value_path_list e f pargs vargs.
Proof.
  intros WT; elim.
  - by move=>> ++ T; inv T => /= ++ [<-].
  - move=>> IH > ++ T; inv T => /=.
    case T: type_syn_path => [[]|] //=; case Tl: type_syn_path_list => [[]|] > //= _.
    case S: get_szenv_syn_path => //=; case Sl: get_szenv_syn_path_list =>> //= _.
    case E: get_env_path => //=; case El: get_env_path_list =>> //= [<-].
    have [V _]:= subject_reduction_path _ _ _ _ _ _ _ _ WT H2 T S E.
    have:= type_syn_path_get_tenv_syn_path _ _ _ _ T.
    rewrite (eval_path_get_tenv_path _ _ _ _ H2) => Tp'.
    rewrite (eval_path_get_szenv_path' _ _ _ _ H2) in S.
    rewrite (IH  _ _ _ _ _ Tl Sl H3 El) /wt_value_path S Tp' /=.
    by move/wt_value_dec: V => ->.
Qed.

Lemma match_types_verified f f' e:
  ∀ args pargs ne pp,
    f & e |= args =>lp Some pargs ->
    f |- args ∈lp sig_args (fd_sig f') ¤ ne ->
    pargs_params (fd_params f') pargs = Some pp ->
    match_types (fn_tenv f) (fd_tenv f') pp.
Proof.
  intros * Eargs Targs PP.
  apply forallb_forall => - [p' i].
  case/(In_nth_error pp) => k N.
  have:= nth_error_map fst k pp; rewrite (args_params_l _ _ _ PP) N /= => Np'.
  have:= nth_error_map snd k pp; rewrite (args_params_r _ _ _ PP) N /= => Ni.
  have [p [Np Ep]]:= eval_path_list_nth_error' _ _ _ _ _ _ Eargs Np'.
  have [t [> [Sp [Tp Ip]]]]:= type_syn_path_list_nth_error _ _ _ _ _ _ Targs Np.
  have:= type_syn_path_get_tenv_syn_path _ _ _ _ Tp.
  rewrite (eval_path_get_tenv_path _ _ _ _ Ep) => ->.
  have:= proj2 (fd_tenv_sig f') k i Ni; rewrite Sp => <- /=; by case typ_reflect.
Qed.

Lemma valid_call_tests_fold_right2_res_lsz_is_app M:
  forall (lsz: list expr) (lsz0: list ident) t0 t,
    fold_right2_res (λ tests sz sz0,
      bind (expr_subst_idents M sz) (fun e => OK ((e, sz0) :: tests))
    ) lsz lsz0 t0 = OK t ->
    exists t1, t = t1 ++ t0.
Proof.
  elim/list_ind => /=.
  - case=>> //= [->]. by exists [].
  - move=>> IH [] // >. case X: fold_right2_res =>> //=.
    case E: expr_subst_idents =>> //= [<-].
    have [> ->] := IH _ _ _ X. rewrite app_comm_cons. by eexists.
Qed.

Lemma valid_call_tests_fold_right2_res_llsz_is_app M:
  forall (llsz: list (list expr)) (llsz0: list (list ident)) t0 t,
    fold_right2_res (λ tests lsz lsz0,
      fold_right2_res (λ tests sz sz0,
        bind (expr_subst_idents M sz) (fun e => OK ((e, sz0) :: tests))
      ) lsz lsz0 tests
    ) llsz llsz0 t0 = OK t ->
    exists t1, t = t1 ++ t0.
Proof.
  elim/list_ind => /=.
  - case=>> //= [->]. by exists [].
  - move=>> IH [] // >. case X: fold_right2_res =>> //=.
    case/valid_call_tests_fold_right2_res_lsz_is_app =>> ->.
    have [> ->]:= IH _ _ _ X. rewrite app_assoc. by eexists.
Qed.

Lemma valid_function_call_verified:
  forall e0 f0 fd args vtests ap,
    args_params (fd_params fd) args = Some ap ->
    valid_env_szenv e0 (fn_szenv' f0) (map fst args) ->
    valid_call_tests_values f0 fd e0 args = Val vtests ->
    equal_tests vtests ->
    valid_function_call f0 fd e0 ap.
Proof.
  move=> e0 f0 fd args vtests ap AP VENV.
  rewrite/valid_call_tests_values. case V: valid_call_tests => [tests|] //=.
  move: V. rewrite/valid_call_tests. case HM: args_map => [M|] //=.
  have HM' := args_map_spec _ _ _ (fd_nodup_params fd) HM.
  rewrite AP /= => VC Etests EQ.
  move=> M' + p i llsz0 llsz j k lsz0 lsz sz0 sz.
  rewrite (args_params_r _ _ _ AP) (args_params_l _ _ _ AP) HM => - [<-].
  move=> Hpi Sp /= + Hllsz0 Hllsz Hlsz0 Hlsz.
  case Si: (fd_szenv fd)!i => [x|] //= - [T]. rewrite T {x T} in Si.
  have [ni Npi] := In_nth_error _ _ Hpi.
  have Ni := args_params_nth_error_r _ _ _ _ _ _ AP Npi.
  have Np := args_params_nth_error_l _ _ _ _ _ _ AP Npi.
  have Hi := nth_error_In _ _ Ni.
  have:= fd_szenv_params fd _ _ Si Hi.
  have Illsz := nth_error_In _ _ Hllsz.
  have Ilsz  := nth_error_In _ _ Hlsz.
  move/Forall_forall/(_ _ Illsz)/Forall_forall/(_ _ Ilsz) {Illsz Ilsz} => F.
  have [a [Mi Na]] := HM' _ _ Ni.
  move: Na; rewrite Np => - [T]. rewrite -T {a T} in Mi.
  move: Ni Np AP VC Etests EQ {HM HM' VENV Npi}.
  elim/list_ind: (fd_params fd) args ni ap tests vtests {Hpi}.
  - move=> ? [] //.
  - move=> i' params IH [//|a' args] ni ? tests vtests /=.
    case AP: args_params =>> //= ++ [<-] /=.
    case VC: valid_call_tests_rec => [t0|] //=. case: ni => [|ni] /=.
    { move=> [->] [->]. rewrite Si Sp /=.
      elim/list_ind: llsz llsz0 j t0 tests vtests Hllsz Hllsz0 {VC Si Sp IH}.
      - move=> ? [] //.
      - move=> lsz' llsz IH [//|lsz0' llsz0] j t0 t vt /=.
        case X: fold_right2_res => [t1|] //=. case: j => [|j] /=.
        { move=> [->] [->].
          elim/list_ind: lsz lsz0 k t1 t vt Hlsz Hlsz0 {X IH}.
          - move=> ? [] //.
          - move=> sz' lsz IH [//|sz0' lsz0] k t1 t vt /=.
            case X: fold_right2_res => [t2|] //= N1 N2.
            case SE: expr_subst_idents =>> //= [<-] /=.
            case Sp: split. cbn -[eval_expr].
            case E0: eval_expr => [[]| |] //=.
            case El0: eval_expr_list_to_int64 =>> //=.
            case E: eval_expr => [[]| |] //=.
            case El: eval_expr_list_to_int64 =>> //= [<-] /=.
            case/andP => /Int64.same_if_eq T EQ. rewrite T {T} in E.
            case: k N1 N2 => [|k] /=.
            + move=> [T1] [T2]. rewrite T1 T2 {T1 T2} in E0 SE. rewrite SE.
              move/NBfacts.eval_expr_fixpoint_match_Some in E0. eauto 10.
            + move=> N1 N2. have:= IH _ _ _ _ _ N1 N2 X.
              rewrite Sp El El0 /= => /(_ _ eq_refl EQ) //. }
        { move=> N1 N2 X'.
          have [t2 ->] := valid_call_tests_fold_right2_res_lsz_is_app _ _ _ _ _ X'.
          case Sp: split. have:= split_app _ _ _ _ Sp => {Sp}.
          intros (? & ? & ? & ? & -> & -> & Sp1 & Sp2).
          rewrite map_app. case El0: eval_expr_list_to_int64 => //=.
          have:= eval_expr_list_to_int64_app _ _ _ _ _ El0.
          intros (? & ? & -> & El01 & El02) => {El0}.
          case El: eval_expr_list_to_int64 =>> //= [<-].
          have:= eval_expr_list_to_int64_app _ _ _ _ _ El.
          intros (? & ? & -> & El1 & El2) => {El}. rewrite combine_app.
          { rewrite -(eval_expr_list_to_int64_length _ _ _ _ El01).
            rewrite -(eval_expr_list_to_int64_length _ _ _ _ El1) map_length.
            have:= split_length_l t2. have:= split_length_r t2.
            rewrite Sp1 /= => -> -> //. }
          rewrite/equal_tests. rewrite forallb_app. case/andP => _ EQ.
          have:= IH _ _ _ _ _ N1 N2 X. rewrite Sp2 El02 El2 /=.
          by move/(_ _ eq_refl EQ). } }
    { move=> N1 N2. case (fd_szenv fd)!i' => //= llsz'.
      case get_szenv_syn_path => //= llsz0' X {Sp}.
      have [t1 ->]:= valid_call_tests_fold_right2_res_llsz_is_app _ _ _ _ _ X.
      case Sp: split. have:= split_app _ _ _ _ Sp => {Sp}.
      intros (? & ? & ? & ? & -> & -> & Sp1 & Sp2).
      rewrite map_app. case El0: eval_expr_list_to_int64 => //=.
      have:= eval_expr_list_to_int64_app _ _ _ _ _ El0.
      intros (? & ? & -> & El01 & El02) => {El0}.
      case El: eval_expr_list_to_int64 =>> //= [<-].
      have:= eval_expr_list_to_int64_app _ _ _ _ _ El.
      intros (? & ? & -> & El1 & El2) => {El}. rewrite combine_app.
      { rewrite -(eval_expr_list_to_int64_length _ _ _ _ El01).
        rewrite -(eval_expr_list_to_int64_length _ _ _ _ El1) map_length.
        have:= split_length_l t1. have:= split_length_r t1.
        rewrite Sp1 /= => -> -> //. }
      rewrite/equal_tests. rewrite forallb_app. case/andP => _ EQ.
      have:= IH _ _ _ _ _ N1 N2 AP VC. rewrite Sp2 El02 El2 /=.
      by move/(_ _ eq_refl EQ). }
Qed.

Lemma subject_reduction_expr_wf_size_expr e (f: function):
  ∀ exp t ne v,
    wf_size_expr exp ->
    (∀ i v t, e!i = Some v ->
              primitive_value v ->
              (fn_tenv f)!i = Some t ->
              v ∈ ⟦e, f, t, []⟧) ->
    f |- exp ∈ t ¤ ne ->
    f & e |= exp => Some v ->
    v ∈ ⟦e, f, t, []⟧.
Proof using.
  elim.
  - case=> i [] //= > _ WT; case/cdos =>> [Ti]; case/cdos =>> [Si].
    case P: primitive_type =>> // [<- _] T; inv T; inv H1; inv H6.
    case/cdoo: H2 =>> [Ei]; intros [= <-].
    exact: WT Ei H3 Ti.
  - case=>> _ _ /= [<- _] T; inv T; econstructor.
  - move=>> IH ? t1 > /= WF WT; case/cdo => - [[] >] [Texp] //.
    all: case: t1 => //=; case typ_reflect => // -> > [<- _] T; inv T.
    all: have T := IH _ _ _ WF WT Texp H1; inv T.
    all: move: H4 => /=; repeat destruct_match_goal; case=> <-; econstructor.
  - case=> - [] > IH > /= WF WT; case/cdo => - [[] >] [Texp] //.
    all: repeat destruct_match_goal; case=> <- _ T; inv T.
    all: have T := IH _ _ _ WF WT Texp H1; inv T.
    all: move: H4 => //= [<-]; econstructor.
  - case=>> IH1 > IH2 > /= /andP [WF1 WF2] WT.
    all: case/cdo => [[] >] [Texp1] //; case/cdo => [[] >] [Texp2] //=.
    all: repeat destruct_match_goal; case=> <- _ T; inv T.
    all: have T1 := IH1 _ _ _ WF1 WT Texp1 H2; inv T1.
    all: have T2 := IH2 _ _ _ WF2 WT Texp2 H5; inv T2 => //.
    all: move: H6 => /=.
    all: rewrite/divs/divu/mods/modu/divs64/divu64/mods64/modu64.
    all: repeat destruct_match_goal; case=> <-; econstructor.
  - case=>> IH1 > IH2 > /= /andP [WF1 WF2] WT.
    all: case/cdo => [[] >] [Texp1] //; case/cdo => [[] >] [Texp2] //=.
    all: repeat destruct_match_goal; case=> <- _ T; inv T.
    all: have T1 := IH1 _ _ _ WF1 WT Texp1 H2; inv T1.
    all: have T2 := IH2 _ _ _ WF2 WT Texp2 H5; inv T2 => //.
    all: move: H6 => /= [<-]; econstructor.
  - case=>> IH1 > IH2 > /= /andP [WF1 WF2] WT.
    all: case/cdo => [[] >] [Texp1] //; case/cdo => [[] >] [Texp2] //=.
    all: repeat destruct_match_goal; case=> <- _ T; inv T.
    all: have T1 := IH1 _ _ _ WF1 WT Texp1 H2; inv T1.
    all: have T2 := IH2 _ _ _ WF2 WT Texp2 H5; inv T2 => //.
    all: move: H6 => /= [<-]; econstructor.
Qed.

Lemma wt_env_function_call e0 f0 f:
  ∀ args pargs ap pp vargs e1 e2,
    NB.eval_path_list e0 f0 args (Some pargs) ->
    get_env_path_list e0 pargs = Some vargs ->
    wt_value_path_list e0 f0 pargs vargs ->
    type_size_expressions (Internal f) ->
    args_params (fn_params f) args = Some ap ->
    pargs_params (fn_params f) pargs = Some pp ->
    match_types (fn_tenv f0) (fn_tenv f) pp ->
    same_szvars_structure f0 f pp ->
    valid_function_call f0 f e0 ap ->
    build_func_env (PTree.empty value) (fn_params f) vargs = Some e1 ->
    instantiate_size_vars e1 f = OK e2 ->
    wt_env e2 f.
Proof.
  intros * EARGS EPARGS WT0 TSZ AP PP MT SZSTR VCALL B I i v.
  have NOREPET:= fn_nodup_params f.
  case/(proj1 (instantiate_size_vars_spec2 _ _ _ I _)).
  - case/(build_func_env_spec' _ _ _ _ _ _ B) => // - [k [Ni Nv]].
    have [t [Nt Ti]]: ∃ t, nth_error (sig_args (fn_sig f)) k = Some t
                          /\ (fn_tenv f)!i = Some t.
    { have [LEN /(_ _ _ Ni)]:= fn_tenv_sig f.
      case N: nth_error; eauto.
      move/nth_error_None: N; rewrite -LEN; move/nth_error_None; by rewrite Ni. }
    have [llsz  Si ]:= proj1 (fn_tenv_fn_szenv f _) (ex_intro _ _ Ti).
    have [llsz' Si']:= proj1 (fn_szenv_fn_szenv' f _) (ex_intro _ _ Si).
    repeat eexists; try eassumption.
    have [p0' [Np0' Ep0']]:= get_env_path_list_nth_error' _ _ _ _ _ EPARGS Nv.
    have [p0  [Np0  Ep0]]:= eval_path_list_nth_error' _ _ _ _ _ _ EARGS Np0'.
    case/forallb2_forall: WT0 => LEN /(_ _ _ _ Np0' Nv).
    rewrite/wt_value_path; case/cdob => llsz0 [Sp0']; case/cdob => t0 [Tp0] WT0.
    have IN: In (p0, i) ap by apply (args_params_nth_error_In _ _ _ k _ _ AP).
    have IN': In (p0', i) pp by apply (args_params_nth_error_In _ _ _ k _ _ PP).
    move/forallb_forall: MT => /(_ (p0', i) IN').
    rewrite Ti Tp0 /=; move/typ_reflect; intros ->.
    have:= SZSTR _ _ _ llsz' IN' Sp0'; rewrite /= Si' => /(_ eq_refl) [].
    have [M HM] := args_params_args_map _ _ _ AP.
    move: VCALL; rewrite/valid_function_call.
    rewrite (args_params_l _ _ _ AP) (args_params_r _ _ _ AP).
    have Sp0: get_szenv_syn_path (fn_szenv' f0) p0 = Some llsz0
      by rewrite (get_szenv_syn_path_get_szenv_path _ _ _ _ _ Ep0).
    move/(_ _ HM _ _ _ _ _ _ _ _ _ _ IN Sp0); rewrite /= Si => /(_ _ _ _ _ _ _ _ eq_refl).
    have:= instantiate_size_vars_spec1 _ _ _ I _ _ _ _ _ _ _ _ _ (nth_error_In _ _ Ni) Si Si'.
    have:= fn_szenv'_szvars f _ _ Si'.
    have:= fn_szenv_szenv'_str f _ _ _ Si Si'.
    clear i k p0 IN IN' Tp0 Ti Ep0 Ep0' Np0 Np0' LEN Sp0 Sp0' Si Si' Nt Ni Nv NOREPET.
    elim: t llsz0 llsz llsz' v WT0 => [||??|?|[]|t IH].
    1-6: move=> [] // ? [] // [] > //; econstructor.
    move=> [//|lsz0 llsz0] [//|lsz llsz]. by move=>> _ T; inv T.
    move=> [//|lsz' llsz'] [] // lv /=.
    case E: ex_value => //=; case/andP => WTlv.
    case Elsz0: eval_ident_list => [lvsz0||] //=.
    case Nlsz0: natlist_of_Vint64 => [shape|] //= SIZE.
    move=> T1 T2 EV VCALL /eq_add_S LEN STR; inv T1; inv T2.
    case: case_ex_value E =>> // /wt_value_dec E _.
    have EV':= EV 0%nat _ _ _ _ _ eq_refl eq_refl.
    have STR':= STR 0%nat _ _ eq_refl eq_refl.
    have VCALL':= VCALL 0%nat _ _ _ _ _ eq_refl eq_refl.
    have Elsz': eval_ident_list e2 f lsz' = Val lvsz0.
    { elim: lsz lsz0 lsz' lvsz0 STR' H2 Elsz0 H1 EV' VCALL' {EV STR VCALL IH Nlsz0}.
      - by move=> [|??] [].
      - move=> sz lsz IH [|sz0 lsz0] [|sz' lsz'] > //=.
        move=> /eq_add_S X1 /eq_add_S X2 + T; inv T.
        case Esz0: e0!sz0 => [v0|] //=.
        case Ssz0: (fn_szenv' f0)!sz0 => [[]|] //=;
          case Elsz0: eval_ident_list =>> //= [<-] EV VCALL.
        have [n [Esz Esz']]:= EV 0%nat _ _ eq_refl eq_refl.
        have Elsz':= IH _ _ _ X1 X2 Elsz0 H2 (fun n => EV (S n)) (fun n => VCALL (S n)).
        rewrite Esz' Elsz' /=.
        case: (fn_szenv' f)!sz' H1 => [[]|] //= _; do 2 f_equal.
        have [szr [n' [SUB [Esz0' ESUB]]]]:= VCALL 0%nat _ _ eq_refl eq_refl.
        move/eval_expr_fixpoint_match_Some: Esz0'.
        rewrite /ExprSem.eval_expr /= Esz0 Ssz0 /=; case primitive_value => // - [->].
        suff: eval_expr e1 f sz = Val (Vint64 n').
        { rewrite Esz; by case=> <-. }
        exact: eval_expr_preserved_by_call HM EARGS EPARGS B PP SZSTR SUB ESUB. }
    econstructor.
    + apply Forall_forall =>>; move/forallb_forall: WTlv => /[apply] H.
      apply: IH H _ _ (fun n => EV (S n)) (fun n => VCALL (S n)) _ (fun n => STR (S n)); eassumption.
    + apply: IH E _ _ (fun n => EV (S n)) (fun n => VCALL (S n)) _ (fun n => STR (S n)); eassumption.
    + apply eval_identlist_fixpoint_match; eassumption.
    + eassumption.
    + eassumption.
  - intros (j & ? & ? & ? & ? & ? & ? & szexp & IN & S & S' & N1 & N1' & N2 & N2' & EV).
    have:= fn_tenv_sztype f _ _ S'.
    move/Forall_forall => /(_ _ (nth_error_In _ _ N1')).
    move/Forall_forall => /(_ _ (nth_error_In _ _ N2')) => Ti.
    have:= fn_szenv'_szvars f _ _ S'.
    move/Forall_forall => /(_ _ (nth_error_In _ _ N1')).
    move/Forall_forall => /(_ _ (nth_error_In _ _ N2')).
    case Si': (fn_szenv' f)!i => [[]|] //= _.
    repeat eexists; try eassumption.
    move/forallb_forall: TSZ => /(_ _ IN) /=; rewrite S.
    move/forallb_forall => /(_ _ (nth_error_In _ _ N1)).
    move/forallb_forall => /(_ _ (nth_error_In _ _ N2)).
    case Tsz: type_expression => [[[] // []]|] // DEF.
    move/eval_expr_fixpoint_match_Some in EV.
    have:= fn_szenv_wf f _ _ _ S S'.
    case/forallb2_forall => _ /(_ _ _ _ N1' N1).
    case/forallb2_forall => _ /(_ _ _ _ N2' N2).
    move/wf_size_expr'_wf_size_expr => WF.
    suff WT: ∀ i v t, e1!i = Some v ->
                      primitive_value v ->
                      (fn_tenv f)!i = Some t ->
                      v ∈ ⟦e1, f, t, []⟧.
    { have:= subject_reduction_expr_wf_size_expr _ _ _ _ _ _ WF WT Tsz EV.
      inversion 1; econstructor. }
    move=>> /(build_func_env_spec' _ _ _ _ _ _ B) [//|[n [Ni Nv]]] + {Ti} - Ti.
    have:= nth_error_map snd n pp; rewrite (args_params_r _ _ _ PP) Ni.
    case Npp: nth_error => [[]|] > //= [T]; rewrite -T {T} in Npp.
    have Np:= args_params_nth_error_l _ _ _ _ _ _ PP Npp.
    case/forallb2_forall: WT0 => _ /(_ _ _ _ Np Nv).
    move/forallb_forall: MT => /(_ _ (nth_error_In _ _ Npp)).
    rewrite/wt_value_path Ti /=; case get_tenv_path =>> //= /typ_reflect <-.
    case get_szenv_path =>> //= /wt_value_dec H; inv H => //; econstructor.
Qed.

Lemma all_params_size_variables_correct sze':
  ∀ params szvars i,
    all_params_size_variables params sze' = OK szvars ->
    In i szvars ->
    ∃ i0 llsz0 lsz0,
      In i0 params /\
      sze'!i0 = Some llsz0 /\
      In lsz0 llsz0 /\
      In i lsz0.
Proof.
  rewrite/all_params_size_variables.
  elim=> [|p params IH] /=.
  - by move=>> [<-].
  - move=> ? i /cdo [szvars] [H] /cdos [llsz] [Sp] [<-].
    case (mem_dec_spec _ Pos.eqb_spec i szvars).
    + move/IH: H => /(_ i)/[apply].
      case=>> [>] [>] [?]; eauto 10.
    + move=> I1 I2; exists p, llsz; rewrite Sp.
      elim: llsz szvars I1 I2 {IH H Sp} => [|lsz llsz IH] > //= I.
      case/in_app; eauto 10.
      case/(IH _ I) =>> [_] [_] []; eauto 10.
Qed.

Theorem progress_Callstate:
  ∀ p f vargs k,
    type_program p = OK tt ->
    invariants p (Callstate f vargs k) ->
    ∃ t, step_stmt (genv_of_program p) (Callstate f vargs k) t /\
         invariants p t.
Proof.
  intros p f vargs k TP INV; inv INV.
  - inv INIT. have:= proj1 (fn_tenv_sig f). rewrite H2 /=.
    move/length_zero_iff_nil => T.
    eexists. split. eapply step_callstate_Internal. by rewrite T.
    unfold instantiate_size_vars. by rewrite T.
    move: TF. rewrite/type_function.
    case TS: type_statement => [ne|] //=.
    rewrite T /=.
    case INCL: incl => //.
    case WF: well_formed_function => //= _.
    eapply inv_State with (ne_n := PTree.empty unit) (lne_n := []) (lne_e := []) (lne_r := []) => //=.
    + by rewrite H2.
    + exact: TS.
    + move/PTree_Properties.for_all_correct: INCL => /=.
      by move=> + > => /[apply].
  - rename f1 into f0.
    have LEN := well_typed_valuelist_length _ _ WTARGS.
    rewrite -(proj1 (fn_tenv_sig f)) in LEN.
    have [e1 He1]: ∃ e, build_func_env (PTree.empty _) (fn_params f) vargs = Some e.
    { elim/list_ind: vargs (fn_params _) LEN {VK WTARGS PARGS STR} => /=.
      - case=> //=; eauto.
      - move=> v vargs IH [//|>] /= /eq_add_S /IH [> ->]; eauto. }
    have [e2 He2]: ∃ e, instantiate_size_vars e1 f = OK e.
    { move: VCALL. unfold instantiate_size_vars, valid_function_call.
      have [M HM] := args_params_args_map _ _ _ AP.
      specialize eval_expr_preserved_by_call
            with (1 := HM) (2 := EARGS) (3 := PARGS) (4 := He1) (5 := PP)
                 (6 := SZSTR) as EP.
      move: HM. rewrite (args_params_l _ _ _ AP) (args_params_r _ _ _ AP).
      move=> /[swap]/[apply].
      have: ∀ i, In i (fn_params f) -> ∃ llsz, (fn_szenv f)!i = Some llsz.
      { move=>> /(In_nth_error (fn_params f)) [>] /[dup] N.
        have [LEN']:= fn_tenv_sig f => /[apply].
        case N': nth_error => [>|] Ti.
        - apply (fn_tenv_fn_szenv f); rewrite -Ti; by eexists.
        - move/nth_error_None: N'; rewrite -LEN'; move/nth_error_None; by rewrite N. }
      move: AP PP EARGS STR SZSTR {PARGS WTARGS VK LEN WT0 DEF ARRS MT NOAm NOAo ROOT PERM0}.
      have:= eq_refl vargs; rewrite -{2}(app_nil_l vargs).
      elim: (fn_params f) args pargs {2 3}vargs [] ap pp {2}e1.
      - case=>> //= _ [<-]; by eexists.
      - move=> i params IH [|a args] [|q pargs] vargs' vargs0 ?? e2 //= V.
        case AP: args_params => [ap|] //= [<-].
        case PP: pargs_params => [pp|] //= [<-].
        move=> E; inv E. destruct vargs' => //; case/andP => WTv WTvargs.
        move=> SZSTR SZ SUBST.
        have SZSTR':= fun p i llsz0 llsz I => SZSTR p i llsz0 llsz (or_intror _ I).
        have SZ':= fun i I => SZ i (or_intror _ I).
        have SUBST':= fun p i llsz' llsz j k lsz' lsz sz' sz I =>
          SUBST p i llsz' llsz j k lsz' lsz sz' sz (or_intror _ I).
        have [llsz Si]:= SZ _ (or_introl _ eq_refl).
        have [llsz' Si']:= proj1 (fn_szenv_fn_szenv' f _) (ex_intro _ _ Si).
        rewrite Si Si' /=.
        have:= IH args pargs vargs' (vargs0 ++ [v]) ap pp _ _ AP PP H4 WTvargs SZSTR' SZ' SUBST'.
        rewrite -app_assoc /= => /(_ _ eq_refl).
        set F := fold_left2_res _ _ _ _.
        suff: ∃ r, F = OK r. { case=>> -> /=; eauto. }
        rewrite/F {F SUBST' SZ' SZSTR' SZ H4 WTvargs AP PP IH}.
        have [llsz0 /[dup] Sp]:= eval_path_get_szenv_syn_path _ _ _ _ H2.
        rewrite (get_szenv_syn_path_get_szenv_path _ _ _ _ _ H2) => Sp'.
        have:= SZSTR _ _ _ _ (or_introl _ eq_refl) Sp' => /= {SZSTR}.
        rewrite Si' /= => /(_ _ eq_refl) [LEN STR].
        move/(_ _ _ _ _ _ _ _ _ _ _ (or_introl _ eq_refl) Sp): SUBST.
        rewrite Si /= => /(_ _ _ _ _ _ _ _ eq_refl).
        have:= fn_szenv_wf f _ _ _ Si Si'.
        have:= fn_szenv_szenv'_str f _ _ _ Si Si' => {Si Si'}.
        elim: llsz llsz' llsz0 e2 LEN STR {Sp Sp'} => /=.
        + case=>> //=; eauto.
        + move=> lsz llsz IH [//|lsz' llsz'] [|lsz0 llsz0] e2 // ++ T; inv T => /=.
          move/eq_add_S => LEN STR; case/andP => WFlsz WFllsz H.
          have:= IH _ _ _ LEN
                  (fun n lsz1 lsz2 => STR (S n) lsz1 lsz2)
                  H5 WFllsz
                  (fun n k l l0 i e => H (S n) k l l0 i e).
          set F := fold_left2_res _ e2 lsz lsz'.
          suff: ∃ r, F = OK r. { case=>> -> /=; eauto. }
          rewrite/F {F H5 IH WFllsz LEN}.
          move/(_ 0%nat _ _ eq_refl eq_refl) in STR.
          move/(_ 0%nat _ _ _ _ _ eq_refl eq_refl) in H.
          elim: lsz lsz' lsz0 e2 H3 STR WFlsz H => /=.
          * case=>> //=; eauto.
          * move=> sz lsz IH [//|sz' lsz'] [|sz0 lsz0] e2 //=.
            move=> /eq_add_S LEN /eq_add_S LEN'; case/andP => WFsz WFlsz H.
            have [szr [n [SUB [Esz0 Eszr]]]]:= H 0%nat _ _ eq_refl eq_refl.
            rewrite (EP _ _ _ SUB Eszr) /=.
            apply: IH LEN LEN' WFlsz (fun n i e => H (S n) i e). }
    have:= type_program_fundef _ _ _ TP GENV.
    rewrite/type_fundef; case TSZ: type_size_expressions => //.
    rewrite/type_function TS /=.
    case/cdo => szvars [SZVARS].
    case INCLne: incl => //=; case WFF: well_formed_function => // _.
    have WT:= wt_env_function_call _ _ _ _ _ _ _ _ _ _ EARGS PARGS STR TSZ AP PP MT SZSTR VCALL He1 He2.
    eexists; split; [econstructor; eassumption|].
    eapply inv_State with (ne_n := PTree.empty unit).
    + eassumption.
    + have MT': match_types (fn_tenv f0) (fn_tenv f) (mut_args (fn_penv f) pp).
      { apply forallb_forall => - [q i] /mut_args_incl.
        by move/(proj1 (forallb_forall _ _) MT). }
      simpl; rewrite -> VK; rewrite /= NOAm PERM0 MT'.
      move: TPidv => /= ->.
      move/wt_env_dec: WT0 => ->.
      move/defined_iset_dec: DEF => ->.
      suff: structure_preserved (remove_own_params (own_args (fn_penv f) pp) e0) e2 (mut_args (fn_penv f) pp).
      { move=> -> /=.
        suff: mutable f (mut_args (fn_penv f) pp). { by move=> ->. }
        apply forallb_forall => - [] > /filter_In [_]; by case (fn_penv f)!_ => [[]|]. }
      apply forallb_forall => - [q j] I.
      case/(mut_args_incl (fn_penv f) pp)/(In_nth_error pp): (I) => n Npp.
      have Nq:= args_params_nth_error_l _ _ _ _ _ _ PP Npp.
      have Nj:= args_params_nth_error_r _ _ _ _ _ _ PP Npp.
      have [v [Nv Eq]]:= get_env_path_list_nth_error _ _ _ _ _ PARGS Nq.
      have E1j:= build_func_env_spec _ _ _ _ (fn_nodup_params f) He1 _ _ _ Nj Nv.
      case E2j: e2!j; last first.
      { have:= proj2 (instantiate_size_vars_spec2 _ _ _ He2 _) E2j.
        by rewrite E1j. }
      case (proj1 (instantiate_size_vars_spec2 _ _ _ He2 _) _ E2j); last first.
      { intros (? & ? & ? & ? & ? & ? & ? & ? & I' & S & S' & N1 & N1' & N2 & N2' & E).
        have:= fn_penv_szenv' f _ _ S'.
        move/Forall_forall/(_ _ (nth_error_In _ _ N1')).
        move/Forall_forall/(_ _ (nth_error_In _ _ N2')).
        by case/filter_In: I => _ /[swap] ->. }
      have Eq': get_env_path (remove_own_params (own_args (fn_penv f) pp) e0) q = Some v.
      { move: Npp Nq I Eq; case q => i l Npp Nq /filter_In [_] Pj /=.
        case E0i: e0!i =>> //=.
        rewrite remove_own_params_remove_list; case case_remove_list.
        * case/(@in_map_fst_pair ident (list sem_path_elem)) =>>.
          case/(@in_map_fst_pair sem_path ident) => j' /[dup] I'.
          case/filter_In => /(In_nth_error pp) [k N'].
          case (Pos.eqb_spec j j') => [<-|NEQ].
          { move: Pj; by case (fn_penv f)!j => [[]|]. }
          have NEQ': n <> k.
          { move=> EQ; move: N'; rewrite -EQ Npp; by case. }
          have INCL': List.incl (map fst (mut_args (fn_penv f) pp ++ own_args (fn_penv f) pp)) pargs.
          { move=>>; rewrite map_app; case/(@in_app_or sem_path).
            all: case/(@in_map_fst_pair sem_path ident) =>>.
            move/mut_args_incl/(@in_map_pair_fst sem_path ident).
            by apply (args_params_list_incl_l _ _ _ PP).
            move/own_args_incl/(@in_map_pair_fst sem_path ident).
            by apply (args_params_list_incl_l _ _ _ PP). }
          suff: pargs_params_aliasing pargs (mut_args (fn_penv f) pp ++ own_args (fn_penv f) pp).
          { by move: NOAo => /[swap] ->. }
          apply (pargs_params_aliasing_spec _ _ INCL').
          have Nq':= args_params_nth_error_l _ _ _ _ _ _ PP N'.
          have [> Nm]: ∃ n', nth_error (mut_args (fn_penv f) pp) n' = Some (i, l, j).
          { apply In_nth_error, filter_In; split=> //.
            exact (nth_error_In _ _ Npp). }
          repeat eexists. exact NEQ'.
          eassumption. eassumption.
          rewrite nth_error_app1.
          { apply List.nth_error_Some; by rewrite -> Nm. }
          { eassumption. }
          exact: sem_path_alias_refl.
          move/forallb_forall: ROOT => /(_ _ I').
          destruct_match_goal => _ /=; by rewrite Pos.eqb_refl orbT.
        * move=> _; by rewrite E0i. }
      rewrite E1j Eq' => - [<-] /=; rewrite same_structure_values_refl.
      have [> Tq]:= ARRS _ _ I.
      case/forallb2_forall: STR => _ /(_ _ _ _ Nq Nv).
      rewrite/wt_value_path Tq; case get_szenv_path =>> // /wt_value_dec T; by inv T.
    + exact TS.
    + move=>> /(proj1 (PTree_Properties.for_all_correct _ _) INCLne).
      case case_set_list => // /in_app [].
      * move=> /(In_nth_error (fn_params f)) [n Ni] _.
        have:= nth_error_map snd n pp; rewrite (args_params_r _ _ _ PP) Ni.
        case Npp: nth_error => [[]|] > //= [T]; rewrite -T {T} in Npp.
        have Nq:= args_params_nth_error_l _ _ _ _ _ _ PP Npp.
        have [v [Nv Eq]]:= get_env_path_list_nth_error _ _ _ _ _ PARGS Nq.
        have E1j:= build_func_env_spec _ _ _ _ (fn_nodup_params f) He1 _ _ _ Ni Nv.
        case E2j: e2!_ =>>; eauto.
        have:= proj2 (instantiate_size_vars_spec2 _ _ _ He2 _) E2j; by rewrite E1j.
      * move/(all_params_size_variables_correct _ _ _ _ SZVARS).
        case=>> [>] [>] [Hparams] [SZE'] [].
        case/(@In_nth_error (list ident)) => j Nj'.
        case/(@In_nth_error ident) => k Nk'.
        have [llsz SZE]:= proj2 (fn_szenv_fn_szenv' f _) (ex_intro _ _ SZE').
        have:= fn_szenv_szenv'_str f _ _ _ SZE SZE'.
        case/Forall2_forall => L1.
        have [lsz Nj]: ∃ lsz, nth_error llsz j = Some lsz.
        { case H: nth_error; eauto.
          move/nth_error_None: H; rewrite L1.
          move/nth_error_None; by rewrite Nj'. }
        move/(_ _ _ _ Nj Nj') => L2.
        have [sz Nk]: ∃ sz, nth_error lsz k = Some sz.
        { case H: nth_error; eauto.
          move/nth_error_None: H; rewrite L2.
          move/nth_error_None; by rewrite Nk'. }
        have:= instantiate_size_vars_spec1 _ _ _ He2
                _ _ _ _ _ _ _ _ _ Hparams SZE SZE' Nj Nj' Nk Nk'.
        case=>> [_]; eexists; eassumption.
    + by move: WFF; rewrite/well_formed_function/well_formed /= => ->.
Qed.

Lemma eval_identlist_Shared_update_env f0 e0 e m e':
  update_env e e0 m = Some e' ->
  at_least_mutable f0 m ->
  ∀ l lv,
    Forall (fun sz => (fn_penv f0)!sz = Some Shared) l ->
    eval_identlist e0 f0 l lv ->
    eval_identlist e' f0 l lv.
Proof.
  move=> UPDE MUT; elim.
  - move=>> _ T; inv T; constructor.
  - move=> i > IH > S T; inv S; inv T.
    case (mem_dec_spec _ Pos.eqb_spec i (map fst (map fst m))) => I.
    + case/(in_map_fst_pair (map fst m)): I =>>.
      case/(in_map_fst_pair m) =>>.
      move/(proj1 (forallb_forall _ _) MUT); by rewrite H1.
    + have NOALIAS: ~ mem sem_path_alias (i, []) (map fst m).
      { case/mem_spec => - [] > [].
        move/(in_map_pair_fst (map fst m)) => /= + /andP [] /Pos.eqb_spec EQ.
        by rewrite -EQ. }
      have:= update_env_spec_1 _ _ _ _ _ _ _ NOALIAS UPDE.
      rewrite /= H3 => /(_ _ eq_refl).
      case E'i: e'!i =>> //= [V]; rewrite V {V} in E'i.
      econstructor => //; by apply: IH.
Qed.

Lemma ex_value_return f0 e0 e m e':
  update_env e e0 m = Some e' ->
  at_least_mutable f0 m ->
  ∀ v t i llsz0 llsz,
    (fn_szenv' f0)!i = Some (llsz0 ++ llsz) ->
    v  ∈ ⟦e0, f0, t, llsz ⟧ ->
    (* v' ∈ ⟦e , f , t, llsz'⟧ -> *)
    ∃ v', v' ∈ ⟦e', f0, t, llsz ⟧.
Proof.
  move=> UPDE MUT v t i llsz0 llsz Si.
  have:= fn_penv_szenv' f0 _ _ Si.
  case/Forall_app => _.
  elim: t v llsz {i Si} => [||??|?|[]|].
  1-6: move=> v > _ T; exists v; inv T; constructor.
  move=>> IH > S T; inv S; inv T.
  have EVlsz':= eval_identlist_Shared_update_env _ _ _ _ _ UPDE MUT _ _ H EVlsz.
  have [vex' WTvex']:= IH _ _ H0 WTvex.
  exists (Varr (repeat vex' (build_size shape))).
  econstructor; try eassumption.
  2: by rewrite repeat_length.
  by apply Forall_forall =>> /(@repeat_spec value) ->.
Qed.

Lemma wt_value_return f0 e0 e m e':
  update_env e e0 m = Some e' ->
  at_least_mutable f0 m ->
  ∀ v v' t i llsz,
    (fn_szenv' f0)!i = Some llsz ->
    same_structure_values v v' ->
    v  ∈ ⟦e0, f0, t, llsz ⟧ ->
    well_typed_value t v' ->
    v' ∈ ⟦e', f0, t, llsz ⟧.
Proof.
  move=> UPDE MUT v v' t i llsz Si.
  have:= fn_penv_szenv' f0 _ _ Si.
  move: Si; rewrite -{1}(app_nil_l llsz).
  elim: t v v' llsz [] => [||??|?|[]|].
  1-6: move=>> _ _ _ T1 T2; inv T1; inv T2; econstructor.
  move=>> IH > Si S STR T1 T2; inv S; inv T2; inv T1.
  simpl in STR; case/forallb2_forall: STR => LEN STR.
  have EVlsz':= eval_identlist_Shared_update_env _ _ _ _ _ UPDE MUT _ _ H EVlsz.
  change (?a :: ?b) with ([a] ++ b) in Si; rewrite app_assoc in Si.
  have [> WTvex']:= ex_value_return _ _ _ _ _ UPDE MUT _ _ _ _ _ Si WTvex.
  eapply wt_value_arr; try eassumption.
  2: by rewrite -LEN.
  apply Forall_forall => v /(In_nth_error lv) [k Nv].
  have Hk: (k < length lv)%coq_nat.
  { apply List.nth_error_Some; by rewrite Nv. }
  have Hk0: (k < length lv0)%coq_nat by rewrite LEN.
  have WT0:= proj1 (Forall_nth _ _) WTv _ Vundef Hk0.
  have Nv0:= List.nth_error_nth' _ Vundef Hk0.
  have:= proj1 (Forall_nth _ _) H2 _ Vundef Hk;
    rewrite (nth_error_nth _ _ _ Nv) => WT.
  exact: IH Si H0 (STR _ _ _ Nv0 Nv) WT0 WT.
Qed.

Lemma update_value_same_structure_values:
  ∀ l v0 v1 v v0',
    get_value_path l v0 = Some v1 ->
    update_value v0 v l = Some v0' ->
    same_structure_values v v1 ->
    same_structure_values v0 v0'.
Proof.
  elim=> [|[idx] l IH].
  - move=>> [<-] [<-]; by rewrite same_structure_values_comm.
  - case=>> //=.
    case N: nth_error => //=; case UPD: update_value => //=.
    case R: replace_nth => //= H [<-].
    move/(IH _ _ _ _ H UPD) => X.
    apply forallb2_forall; split.
    { exact: replace_nth_length R. }
    move=> n >  N1 N2; case (Nat.eqb_spec idx n); [intros [= <-]|intro NEQ].
    + move: N2; rewrite (replace_nth_nth_error_same _ _ _ _ R) => - [<-].
      move: N1; rewrite N => - [<-] //.
    + move: N2; rewrite (replace_nth_nth_error_other _ _ _ _ R _ NEQ).
      rewrite N1 => - [<-]; exact: same_structure_values_refl.
Qed.

Corollary update_env_path_same_structure_values e:
  ∀ i l v v' e' v0 v0',
    get_env_path e (i, l) = Some v ->
    update_env_path e (i, l) v' = Some e' ->
    same_structure_values v v' ->
    e!i = Some v0 ->
    e'!i = Some v0' ->
    same_structure_values v0 v0'.
Proof.
  move=> i l v v' e' v0 v0' /= +++ Ei.
  rewrite Ei /=; case UPD: update_value =>> //= El [<-].
  rewrite PTree.gss same_structure_values_comm => + [<-].
  by apply: update_value_same_structure_values El UPD.
Qed.

Lemma same_structure_values_get_value_path:
  ∀ v0 v0' l v,
    same_structure_values v0 v0' ->
    get_value_path l v0 = Some v ->
    ∃ v', get_value_path l v0' = Some v' /\
          same_structure_values v v'.
Proof.
  move=> v0 v0' l; elim: l v0 v0' => [|[idx] l IH].
  - move=>> + [<-] /=; eauto.
  - case=> // lv0 [] //= lv0' >.
    case/forallb2_forall => LEN STR.
    case N: nth_error => //=; case N': nth_error => //=; last first.
    { move/nth_error_None: N'; rewrite -LEN.
      move/nth_error_None; by rewrite N. }
    exact: IH (STR _ _ _ N N').
Qed.

Lemma update_env_path_structure_preserved e e1:
  ∀ p v v' e' m,
    get_env_path e p = Some v ->
    update_env_path e p v' = Some e' ->
    same_structure_values v v' ->
    structure_preserved e  e1 m ->
    structure_preserved e' e1 m.
Proof.
  case=> i l v v' ? m Ep UPD STR STRe.
  have STR':= update_env_path_same_structure_values _ _ _ _ _ _ _ _ Ep UPD STR.
  apply forallb_forall => - [[i' l'] j].
  move/forallb_forall: STRe => /[apply].
  case: e1!j UPD STR' => [v0|] //=.
  case Ei: e!i =>> //=; case update_value =>> //= [<-].
  move/(_ _ _ eq_refl (PTree.gss _ _ _)) => STR'.
  case case_set => [<-|NEQ] //=; rewrite Ei /=.
  case El': get_value_path => [v1|] //=.
  have [v1' [-> /=]]:= same_structure_values_get_value_path _ _ _ _ STR' El'.
  case: v0 =>> //; case v1 =>> //; case v1' =>> // /[swap].
  by apply: same_structure_values_trans.
Qed.

Lemma update_env_same_structure_values e:
  ∀ m e0 e0' i v v',
    structure_preserved e0 e m ->
    update_env e e0 m = Some e0' ->
    e0!i = Some v  ->
    e0'!i = Some v' ->
    same_structure_values v v'.
Proof.
  elim=> [|[[i l] j] m IH] e0 e0' k vk vk'.
  - move=> _ [<-] -> [<-]; exact: same_structure_values_refl.
  - cbn -[get_env_path update_env_path]; case/andP => + STR.
    case Ej: e!j => [v|] //. case Ep: get_env_path => [v1|] //.
    cbn -[update_env_path].
    case UPD: update_env_path => [e0''|] //= V UPDE E0k.
    have V': same_structure_values v1 v.
    { move: V; case v => //; case v1 => // >; by rewrite same_structure_values_comm. }
    case (Pos.eqb_spec k i); [intros [= ->]|intro NEQ].
    + have STR':= update_env_path_structure_preserved _ _ _ _ _ _ _ Ep UPD V' STR.
      move=> E0'i.
      have [vk'' E0''i]:= update_env_spec_3 _ _ _ _ _ _ E0'i UPDE.
      have:= IH _ _ i _ _ STR' UPDE E0''i E0'i.
      have:= update_env_path_same_structure_values _ _ _ _ _ _ _ _ Ep UPD V' E0k E0''i.
      by apply: same_structure_values_trans.
    + have STR':= update_env_path_structure_preserved _ _ _ _ _ _ _ Ep UPD V' STR.
      move=> E0'k.
      have [vk'' E0''k]:= update_env_spec_3 _ _ _ _ _ _ E0'k UPDE.
      have:= IH _ _ k _ _ STR' UPDE E0''k E0'k.
      move: UPD E0''k => /=; case e0!i =>> //=; case update_value =>> //= [<-].
      rewrite PTree.gso // E0k => - [<-] //.
Qed.

Lemma well_typed_value_change_type:
  ∀ t t' v v',
    well_typed_value t v ->
    well_typed_value t v' ->
    well_typed_value t' v ->
    same_structure_values v v' ->
    well_typed_value t' v'.
Proof.
  elim=> [|||||> IH] > T1 T2 H STR; inv T1; inv T2; inv H; constructor.
  apply Forall_forall =>> /[dup] I /(In_nth_error lv0) [k N].
  move: STR => /= /forallb2_forall [LEN].
  have [> N']: ∃ v, nth_error lv k = Some v.
  { case N': nth_error; eauto.
    move/nth_error_None: N'; rewrite LEN.
    move/nth_error_None; by rewrite N. }
  have I':= nth_error_In _ _ N'.
  move/Forall_forall/(_ _ I'): H1 => T1.
  move/Forall_forall/(_ _ I): H2 => T2.
  move/Forall_forall/(_ _ I'): H4 => H.
  move/(_ _ _ _ N' N) => STR.
  by apply: IH T1 T2 H STR.
Qed.

Lemma update_value_type_preserved:
  ∀ l v0 v v' v0' t0 t,
    get_value_path l v0 = Some v ->
    well_typed_value t v ->
    well_typed_value t v' ->
    same_structure_values v v' ->
    update_value v0 v' l = Some v0' ->
    well_typed_value t0 v0 ->
    well_typed_value t0 v0'.
Proof.
  elim=> [|[idx] l IH].
  - move=>> [<-] ++ /[swap] - [<-] /[swap].
    by apply: well_typed_value_change_type.
  - case=>> //=; case N: nth_error => //=; case UPD: update_value => //=.
    case R: replace_nth =>> //= Vl WTv WTv' STR [<-] T; inv T.
    constructor; apply Forall_forall =>> /[dup] I /(@In_nth_error value) [k N'].
    case: (Nat.eqb_spec idx k) N' => [<-|NEQ].
    + rewrite (replace_nth_nth_error_same _ _ _ _ R) => - [<-].
      move/Forall_forall: H1 => /(_ _ (nth_error_In _ _ N)).
      by apply: IH Vl WTv WTv' STR UPD.
    + rewrite (replace_nth_nth_error_other _ _ _ _ R _ NEQ).
      move/(@nth_error_In value).
      by move/Forall_forall: H1 => /[apply].
Qed.

Lemma update_env_path_type_preserved e:
  ∀ p v v' t e',
    get_env_path e p = Some v ->
    well_typed_value t v ->
    well_typed_value t v' ->
    same_structure_values v v' ->
    update_env_path e p v' = Some e' ->
    ∀ i t0 v0 v0',
      e!i = Some v0 ->
      e'!i = Some v0' ->
      well_typed_value t0 v0 ->
      well_typed_value t0 v0'.
Proof.
  case=> i l > /=.
  case Ei: e!i =>> //= El WTv WTv' STR.
  case UPD: update_value =>> //= [<-] i' > /=.
  case case_set => [<-|NEQ] //=.
  - rewrite Ei /= => - [<-] [<-].
    by apply: update_value_type_preserved El WTv WTv' STR UPD.
  - by move=> -> [<-].
Qed.

(* Lemma update_env_path_get_env_path'' e:
  ∀ p q v0 v v' e',
    get_env_path e p = Some v0 ->
    get_env_path e q = Some v ->
    update_env_path e q v' = Some e' ->
    same_structure_values v v' ->
    ∃ v0', get_env_path e' p = Some v0'.
Proof.
  intros [i l] [i' l'] * Ep Eq UPD STR.
  have:= update_env_path_same_structure_values _ _ _ _ _ _ _ _ Eq UPD STR.
  case (Pos.eqb_spec i i') => [<-|NEQ].
  - move: Ep UPD {Eq} => /=; case e!i =>> //=.

   case e'!i => //=. *)


Lemma update_env_type_preserved f0 f e:
  ∀ m e0 e0',
    structure_preserved e0 e m ->
    (∀ i t v, e0!i = Some v -> (fn_tenv f0)!i = Some t -> well_typed_value t v) ->
    wt_env e f ->
    match_types (fn_tenv f0) (fn_tenv f) m ->
    update_env e e0 m = Some e0' ->
    (∀ i t v, e0'!i = Some v -> (fn_tenv f0)!i = Some t -> well_typed_value t v).
Proof.
  move=> +++++ WT; elim=> [|[[i l] j] m IH] e0 >.
  - by move=> ++ _ [<-].
  - cbn -[update_env_path get_tenv_path get_env_path]; move=>>.
    case Ej: e!j => [v'|] //; case Ep: get_env_path => [v|] // /andP [STR STRM].
    move=> H /andP [+ MT]; simpl in STR.
    case Tp: get_tenv_path => [t|] //; case Tj: (fn_tenv f)!j => //.
    move/typ_reflect => T; rewrite -T {T} in Tj; cbn -[update_env_path].
    case UPD: update_env_path =>> //= UPDE.
    have STR': same_structure_values v v'.
    { move: STR; case v' => //; case v =>> //.
      by rewrite same_structure_values_comm. }
    apply: IH _ _ MT UPDE.
    { exact: update_env_path_structure_preserved Ep UPD STR' STRM. }
    move=> i' >.
    case (Pos.eqb_spec i' i) => [->|NEQ].
    + have:= WT _ _ Ej; rewrite Tj => - [_ [? [[<-] [_]]]].
      move/wt_value_well_typed_value => WTv' E'i Ti.
      have WTv: well_typed_value t v.
      { move: Ep Tp (H i) => /=; case e0!i => [v0|] //=; case (fn_tenv f0)!i => [t0|] //=.
        move=> ++ /(_ _ _ eq_refl eq_refl).
        elim: (l) t0 v0 => [|[] > IH] > /=.
        - by move=> [<-] [<-].
        - repeat destruct_match_goal; case/cdoo =>> [N] El Tl X; inv X.
          move/Forall_forall: H2 => /(_ _ (nth_error_In _ _ N)).
          by apply: IH El Tl. }
      have U:= update_env_path_type_preserved _ _ _ _ _ _ Ep WTv WTv' STR' UPD.
      move: Ep => /=; case Ei: e0!i =>> //= _.
      by apply: U Ei E'i (H _ _ _ Ei Ti).
    + move: UPD => /=; case e0!i =>> //=; case update_value =>> //= [<-].
      rewrite PTree.gso //; by apply: H.
Qed.

Lemma wt_env_return f0 f e0 e:
  ∀ m e',
    wt_env e0 f0 ->
    wt_env e f ->
    ¬ pargs_params_aliasing (map fst m) m ->
    match_types (fn_tenv f0) (fn_tenv f) m ->
    at_least_mutable f0 m ->
    structure_preserved e0 e m ->
    update_env e e0 m = Some e' ->
    wt_env e' f0.
Proof.
  intros * WT0 WT NOALIAS MT MUT STR UPDE i v0' E'i.
  case (mem_dec_spec _ Pos.eqb_spec i (map fst (map fst m))) => I; last first.
  { have H: ¬ mem sem_path_alias (i, []) (map fst m).
    { case/mem_spec => - [i' l'] [] /= + /andP [/Pos.eqb_spec] EQ.
      move/(in_map_pair_fst (map fst m)); by rewrite -EQ. }
    have [v0 E0i]:= update_env_spec_3 _ _ _ _ _ _ E'i UPDE.
    have [t0 [llsz0 [-> [/[dup] Si -> Hv0]]]]:= WT0 _ _ E0i; repeat eexists.
    have:= update_env_spec_1 _ _ _ _ (i, []) v0 _ H UPDE.
    rewrite /= E0i E'i /= => /(_ eq_refl) [->].
    apply: (wt_value_return _ _ _ _ _ UPDE MUT v0 v0 _ _ _ Si _ Hv0).
    exact: same_structure_values_refl.
    exact: wt_value_well_typed_value Hv0. }
  have [v0 E0i]:= update_env_spec_3 _ _ _ _ _ _ E'i UPDE.
  have [t0 [llsz0 [/[dup] Ti -> [/[dup] Si -> Hv0]]]]:= WT0 _ _ E0i; repeat eexists.
  have V:= update_env_same_structure_values _ _ _ _ _ _ _ STR UPDE E0i E'i.
  case/(in_map_fst_pair (map fst m)): I => l.
  case/(in_map_fst_pair m) => j I.
  move/forallb_forall/(_ _ I): (STR).
  case Ej: e!j => [v|] //=.
  rewrite E0i /=; case El: get_value_path => [v1|] //= X.
  have {X} - STR': same_structure_values v v1.
  { move: X; case v => //; case v1 => //. }
  have [t [llsz [Tj [Sj Hv]]]]:= WT _ _ Ej.
  move/wt_value_well_typed_value in Hv.
  move/forallb_forall/(_ _ I): (MT); rewrite Tj; cbn -[get_tenv_path].
  case Tp: get_tenv_path => [t'|] //= /typ_reflect T; rewrite T {t' T} in Tp.
  have Hv0': well_typed_value t0 v0'.
  { apply: update_env_type_preserved STR _ WT MT UPDE _ _ _ E'i Ti.
    move=>> /WT0 /[swap] -> [_ [> [[<-] [_]]]].
    by apply: wt_value_well_typed_value. }
  by apply: wt_value_return UPDE MUT _ _ _ _ _ Si V Hv0 Hv0'.
Qed.

Theorem progress_Returnstate:
  ∀ p e f v k,
    type_program p = OK tt ->
    invariants p (Returnstate e f v k) ->
    (∀ r, ~ final_state (Returnstate e f v k) r) ->
    ∃ t, step_stmt (genv_of_program p) (Returnstate e f v k) t /\
         invariants p t.
Proof.
  move=> p e f v k TP INV. inv INV.
  case: k ne_n ne_e ne_r WTRET WF VK
    => [|??|?|idv e0 f0 marrs k] [|ne_n lne_n] [|ne_e lne_e] [|ne_r lne_r] //= WTv WF.
  - case: (sig_res (fn_sig f)) WTv => > //=.
    case: v => // n _ _ /(_ n); by elim.
  - case VK: valid_cont => //= _ _.
    eexists; split.
    by apply step_returnstate_seq.
    econstructor; eassumption.
  - case: ne_e => // >; case/cdo =>> [VK] _.
    eexists; split.
    by apply step_returnstate_block.
    econstructor; eassumption.
  - case VK: valid_cont => //=.
    case H: (_ && _) => //=; intros [= <-].
    move: H; (repeat case/andP) => WT0 STR NOALIAS MT MUT0 MUT TPidv DEF0 _.
    have MV: marrs_varr e0 e marrs.
    { apply forallb_forall => - [q i].
      move/(proj1 (forallb_forall _ _) STR).
      case e!i => [[]|] //=; case get_env_path => [[]|] //. }
    have [e' Hupde]: exists e', update_env e e0 marrs = Some e'.
    { apply update_env_Some; [by apply: negP|].
      move=> q i Hin; have:= proj1 (forallb_forall _ _) MV _ Hin.
      case get_env_path => [[]|] //= >; case e!i => [[]|] //=; eauto. }
    eexists; split.
    apply step_returnstate => //.
    eassumption.
    rewrite -(primitive_type_primitive_value _ _ WTv); exact: (fn_res_primitive f).
    move: VK; case: lne_n =>>; case: lne_e =>>; case: lne_r =>>; (try by case k) => VK.
    move/wt_env_dec in WT0. move/negP in NOALIAS.
    have WT':= wt_env_return _ _ _ _ _ _ WT0 WT NOALIAS MT MUT0 STR Hupde.
    have Hidv: match idv with
               | Some idv =>
                 ∃ p, (fn_penv f0)!idv = Some p /\ (Mutable <=& p) /\
                     (fn_tenv f0)!idv = Some (sig_res (fn_sig f)) /\
                     (fn_szenv' f0)!idv = Some []
               | None => True
               end.
    { case: idv TPidv {DEF0} => [idv|] //=.
      case Pidv: (fn_penv f0)!idv => [perm|] //= /andP [Hperm].
      case Tidv: (fn_tenv f0)!idv => [t|] //= /typ_reflect T.
      eexists; repeat split=> //. by rewrite T.
      have [l Sidv]:= proj1 (fn_tenv_fn_szenv f0 _) (ex_intro _ _ Tidv).
      have [l' Sidv']:= proj1 (fn_tenv_fn_szenv' f0 _) (ex_intro _ _ Tidv).
      have:= fn_szenv_tenv_str f0 _ _ _ Tidv Sidv.
      have:= fn_szenv_szenv'_str f0 _ _ _ Sidv Sidv'.
      have:= fn_res_primitive f.
      rewrite T; case sig_res => //; case l =>> // + X; by inv X. }
    clear TPidv.
    econstructor.
    + case: idv Hidv {DEF0} => [idv|] //= [>] [Pidv] [PERM] [Tidv] Sidv.
      apply: (wt_env_assign _ _ idv [] _ _ _ v WT') => /=.
      * by rewrite Tidv.
      * by rewrite Sidv.
      * move: WTv (fn_res_primitive f).
        case sig_res => [||||[]|] //; case v =>> //=; constructor.
      * by rewrite Pidv.
      * reflexivity.
    + have VK':= valid_cont_return _ _ _ _ _ _ _ _ _ _ _ NOALIAS Hupde STR MUT0 VK.
      case: idv Hidv {DEF0} => [idv [>] [Pidv] [PERM] [Tidv] Sidv|_] /=.
      * apply: (valid_cont_assign _ _ _ idv [] _ _ WT' _ _ eq_refl _ _ _ _ _ _ VK').
        by rewrite Pidv.
        by rewrite/wt_value_path /= Sidv Tidv /=;
           have:= fn_res_primitive f; case: v WTv =>>; case: sig_res.
        by have:= fn_res_primitive f; case: v WTv =>>; case: sig_res.
      * exact: VK'.
    + reflexivity.
    + move/defined_iset_dec in DEF0.
      have DEF':= defined_iset_return _ _ _ _ _ DEF0 Hupde.
      case: idv Hidv DEF' {DEF0} => [idv [>] [Pidv] [PERM] [Tidv] Sidv|_] //= DEF.
      by move=> j; have:= DEF j; case case_set; eauto; case case_remove.
    + assumption.
Qed.

Definition PTree_minus {A: Type} (t1 t2: PTree.t A) :=
  PTree.combine (fun x1 x2 =>
    match x1, x2 with
    | Some _, Some _ => None
    | Some _, None => x1
    | None, _ => None
    end) t1 t2.

Definition PTree_minus_comp {A: Type} (t1 t2: PTree.t A) :=
  PTree.combine (fun x1 x2 =>
    match x1, x2 with
    | Some _, None => None
    | Some _, Some _ => x1
    | None, _ => None
    end) t1 t2.

Lemma PTree_minus_empty_l {A: Type}:
  ∀ t, PTree_minus (PTree.empty A) t = PTree.empty A.
Proof.
  move=>>; apply PTree.extensionality =>>.
  by rewrite/PTree_minus PTree.gcombine.
Qed.

Lemma PTree_minus_empty_r {A: Type}:
  ∀ t, PTree_minus t (PTree.empty A) = t.
Proof.
  move=>>; apply PTree.extensionality =>>.
  by rewrite/PTree_minus PTree.gcombine //=; case (_!_).
Qed.

Lemma minus_set_unit:
  ∀ n1 n2 i,
    PTree_minus n1 (PTree.set i tt n2) = PTree_minus (PTree.remove i n1) n2.
Proof.
  move=>>; apply PTree.extensionality =>>.
  rewrite/PTree_minus ?PTree.gcombine //.
  case H1: _!_ => /=; case case_remove => //; try rewrite H1 //.
  all: by case case_set.
Qed.

Lemma union_set_unit:
  ∀ n1 n2 i,
    PTree_union n1 (PTree.set i tt n2) = PTree_union (PTree.set i tt n1) n2.
Proof.
  move=>>; apply PTree.extensionality =>>.
  rewrite/PTree_union ?PTree.gcombine //.
  case H1: _!_ => [[]|] /=; do 2 case case_set => //; try rewrite H1 //.
  all: by case _!_.
Qed.

Lemma set_comm_unit:
  ∀ t i j,
    PTree.set i tt (PTree.set j tt t) = PTree.set j tt (PTree.set i tt t).
Proof.
  move=>>; apply PTree.extensionality =>>.
  by repeat case case_set.
Qed.

Lemma PTree_remove_comm {A: Type}:
  ∀ (t: PTree.t A) i j,
    PTree.remove i (PTree.remove j t) = PTree.remove j (PTree.remove i t).
Proof.
  move=>>; apply PTree.extensionality =>>.
  by repeat case case_remove.
Qed.

Lemma PTree_union_minus_distr_l {A: Type}:
  ∀ (t1 t2 t3: PTree.t A),
    PTree_minus (PTree_union t1 t2) t3 = PTree_union (PTree_minus t1 t3) (PTree_minus t2 t3).
Proof.
  intros; apply PTree.extensionality =>>.
  rewrite/PTree_union/PTree_minus ?PTree.gcombine //.
  by repeat case _!_ =>>.
Qed.

Lemma PTree_minus_union_r {A: Type}:
  ∀ (t1 t2 t3: PTree.t A),
    PTree_minus t1 (PTree_union t2 t3) = PTree_minus (PTree_minus t1 t2) t3.
Proof.
  intros; apply PTree.extensionality =>>.
  rewrite/PTree_union/PTree_minus ?PTree.gcombine //.
  by repeat case _!_ =>>.
Qed.

Lemma minus_inter_union_minus:
  ∀ (t a b: iset),
    PTree_minus t (PTree_inter a b) = PTree_union (PTree_minus t a) (PTree_minus t b).
Proof.
  move=>>; apply PTree.extensionality =>>.
  rewrite/PTree_union/PTree_inter/PTree_minus ?PTree.gcombine //.
  by repeat case _!_ => [[]|].
Qed.

Lemma union_minus_eq:
  ∀ (t1 t2: iset),
    PTree_union (PTree_minus t1 t2) t1 = t1.
Proof.
  intros; apply PTree.extensionality =>>.
  rewrite/PTree_union/PTree_minus ?PTree.gcombine //.
  by repeat case _!_ =>>.
Qed.

Ltac simplify_sets :=
  repeat match goal with
    | |- context c [PTree_minus ?a ?b] =>
          let t := type of a in
          (unify a (PTree.empty _ : t); rewrite (PTree_minus_empty_l b)) ||
          (unify b (PTree.empty _ : t); rewrite (PTree_minus_empty_r a))
    | |- context c [PTree_union ?a ?b] =>
          let t := type of a in
          (unify a (PTree.empty _ : t); rewrite (PTree_union_empty_l b)) ||
          (unify b (PTree.empty _ : t); rewrite (PTree_union_empty_r a))
    | |- context c [PTree_minus (PTree_minus ?a ?b) ?c] =>
          rewrite -(PTree_minus_union_r a b c)
    | |- context c [PTree_union ?a ?b = PTree_union ?c ?d] =>
          unify b d; rewrite (union_comm_unit a b) (union_comm_unit c d); f_equal
    | |- context c [PTree_union ?a (PTree_union ?b ?c)] =>
          rewrite -(union_assoc_unit a b c)
    | |- context c [PTree_minus (PTree_union ?a ?b) ?c] =>
          rewrite (PTree_union_minus_distr_l a b c)
    | |- context c [PTree_minus ?a (PTree.set ?i _ ?b)] =>
          rewrite (minus_set_unit a b i)
    | |- context [PTree_minus ?a (PTree_inter ?b ?c)] =>
          rewrite (minus_inter_union_minus a b c)
    | |- context [PTree_union (PTree_minus ?b ?c) ?a] =>
          unify a b; rewrite (union_minus_eq a c)
    | |- context [PTree_union ?a (PTree_minus ?b ?c)] =>
          unify a b; rewrite (union_comm_unit a (PTree_minus b c)) (union_minus_eq a c)
    | |- context c [PTree_union ?a ?b = PTree_union ?c ?d] =>
          unify a c; f_equal
    end.

Fixpoint PTree_union_list {A: Type} (l: list (PTree.t A)) :=
  match l with
  | [] => PTree.empty A
  | x :: l => PTree_union x (PTree_union_list l)
  end.

Lemma PTree_union_to_list_of_unions {A: Type}:
  ∀ (t1 t2: PTree.t A),
    PTree_union t1 t2 = PTree_union_list [t1; t2].
Proof. by move=>> /=; simplify_sets. Qed.

Lemma union_union_list:
  ∀ (l1 l2: list iset),
    PTree_union (PTree_union_list l1) (PTree_union_list l2)
    = PTree_union_list (l1 ++ l2).
Proof.
  elim=> //.
  - move=> l2; cbn; by rewrite PTree_union_empty_l.
  - move=>> IH > /=; by rewrite union_assoc_unit IH.
Qed.

Corollary union_list_union_list_1:
  ∀ (l1 l2: list iset),
    PTree_union_list ((PTree_union_list l1) :: l2)
    = PTree_union_list (l1 ++ l2).
Proof. by cbn=>> ; rewrite union_union_list. Qed.

Corollary union_list_union_list_2:
  ∀ (l1 l2: list iset) t,
    PTree_union_list (t :: (PTree_union_list l1) :: l2)
    = PTree_union_list (t :: l1 ++ l2).
Proof. by cbn=>>; rewrite union_union_list. Qed.

Corollary union_list_union_list_3:
  ∀ (l1 l2: list iset) t1 t2,
    PTree_union_list (t1 :: t2 :: (PTree_union_list l1) :: l2)
    = PTree_union_list (t1 :: t2 :: l1 ++ l2).
Proof. by cbn=>>; rewrite union_union_list. Qed.

Corollary union_list_union_list_4:
  ∀ (l1 l2: list iset) t1 t2 t3,
    PTree_union_list (t1 :: t2 :: t3 :: (PTree_union_list l1) :: l2)
    = PTree_union_list (t1 :: t2 :: t3 :: l1 ++ l2).
Proof. by cbn=>>; rewrite union_union_list. Qed.

Lemma PTree_eq_dec {A: Type}:
  (∀ (x y: A), {x = y} + {x <> y}) ->
  ∀ (x y: PTree.t A), {x = y} + {x <> y}.
Proof.
  intros H *; case X: (PTree.beq H x y).
  - left; apply PTree.extensionality => i.
    move/PTree.beq_correct: X => /(_ i).
    do 2 case _!i => //.
    move=>>; case H => // + _ => -> //.
  - right; intros <-.
    case/PTree_Properties.beq_false: X =>>.
    case _!_ => // >; by case H.
Qed.

Lemma unit_eq_dec:
  ∀ (x y: unit), {x = y} + {x <> y}.
Proof. case=> - []; by left. Qed.

Lemma union_list_In:
  ∀ (l: list iset) a,
    In a l ->
    PTree_union_list l = PTree_union a (PTree_union_list l).
Proof.
  elim=> a > //= IH > [->|].
  - by rewrite -union_assoc_unit union_idempotent_unit.
  - move/IH => ->.
    rewrite -2!union_assoc_unit {2}(union_comm_unit a _).
    rewrite -union_assoc_unit union_idempotent_unit.
    by rewrite (union_comm_unit a).
Qed.

Lemma union_list_nodup:
  ∀ (l1: list iset),
    PTree_union_list l1 =
    PTree_union_list (nodup (PTree_eq_dec unit_eq_dec) l1).
Proof.
  elim=> //= > IH; case in_dec.
  - rewrite IH.
    move/(nodup_In (PTree_eq_dec unit_eq_dec)).
    by move/union_list_In => {2}->.
  - by rewrite IH.
Qed.

Lemma union_list_In_remove:
  ∀ (l: list iset) t,
    In t l ->
    PTree_union_list l = PTree_union t (PTree_union_list (remove (PTree_eq_dec unit_eq_dec) t l)).
Proof.
  elim=> [|a l] //= IH t.
  case (in_dec (PTree_eq_dec unit_eq_dec) t l) => I.
  - case=> [->|_].
    + case PTree_eq_dec => // _.
      by rewrite (IH _ I) -union_assoc_unit union_idempotent_unit.
    + case PTree_eq_dec => // [<-|NEQ].
      by rewrite (IH _ I) -union_assoc_unit union_idempotent_unit.
      by rewrite (IH _ I) /= -?union_assoc_unit (union_comm_unit t a).
  - case=> [->|_] //.
    case PTree_eq_dec => // _; by rewrite notin_remove.
Qed.

Lemma union_list_In_both:
  ∀ t (l1 l2: list iset),
    In t l1 -> In t l2 ->
    PTree_union_list (remove (PTree_eq_dec unit_eq_dec) t l1)
      = PTree_union_list (remove (PTree_eq_dec unit_eq_dec) t l2) ->
    PTree_union_list l1 = PTree_union_list l2.
Proof.
  move=>> I1 I2.
  by rewrite (union_list_In_remove _ _ I1) (union_list_In_remove _ _ I2) => ->.
Qed.

Lemma union_list_move_to_hd:
  ∀ t (l l': list iset),
    PTree_union_list (l ++ t :: l') = PTree_union t (PTree_union_list (l ++ l')).
Proof.
  move=> t;
  elim=>> //= IH t'; by rewrite IH // -?union_assoc_unit (union_comm_unit _ t).
Qed.

Lemma union_list_In_both':
  ∀ t (l1 l1' l2 l2': list iset),
    PTree_union_list (l1 ++ l1') = PTree_union_list (l2 ++ l2') ->
    PTree_union_list (l1 ++ t :: l1') = PTree_union_list (l2 ++ t :: l2').
Proof. move=>>; by rewrite ?union_list_move_to_hd => ->. Qed.

Lemma union_list_minus_eq_1:
  ∀ (t1 t2: iset) l1 l2 l3,
    PTree_union_list (l1 ++ t1 :: l2 ++ (PTree_minus t1 t2) :: l3)
    = PTree_union_list (l1 ++ t1 :: l2 ++ l3).
Proof.
  move=>>.
  rewrite 2!union_list_move_to_hd.
  rewrite !app_assoc union_list_move_to_hd -union_assoc_unit.
  by rewrite (union_comm_unit _ (PTree_minus _ _)) union_minus_eq.
Qed.

Lemma union_list_minus_eq_2:
  ∀ (t1 t2: iset) l1 l2 l3,
    PTree_union_list (l1 ++ (PTree_minus t1 t2) :: l2 ++ t1 :: l3)
    = PTree_union_list (l1 ++ t1 :: l2 ++ l3).
Proof.
  move=>>.
  rewrite 2!union_list_move_to_hd.
  rewrite !app_assoc union_list_move_to_hd -union_assoc_unit.
  by rewrite union_minus_eq.
Qed.

Ltac cut_at' n l :=
  rewrite -(firstn_skipn n l);
  tryif is_var l then unfold l
  else idtac;
  cbn [firstn skipn].

Ltac cut_at n l :=
  match type of n with
  | nat => cut_at' n l
  | Z =>
    let n := eval compute in (Z.to_nat n) in
    cut_at' n l
  end.

Ltac listify :=
  rewrite ?PTree_union_to_list_of_unions;
  rewrite ?union_list_union_list_1 ?union_list_union_list_2
          ?union_list_union_list_3 ?union_list_union_list_4; cbn [app].

Fixpoint res (b: list bool) (lne r a: list iset) :=
  match lne, b, r, a with
  | [], [], [], [] => empty_iset
  | ne :: lne, b :: lb, r :: lr, a :: la =>
    PTree_union (PTree_union_list [PTree_minus (if b then ne else empty_iset) r; a])
                (res lb lne lr la)
  | [], b :: lb, r :: lr, a :: la => PTree_union a (res lb lne lr la)
  | _, _, _, _ => empty_iset
  end.

Fixpoint merge {A B C: Type} (f: A -> B -> C)
               (l1: list A) (l2: list B) (da: A) (db: B) : list C :=
  match l1 with
  | [] => map (fun x2 => f da x2) l2
  | x1 :: l1' =>
    match l2 with
    | [] => map (fun x1 => f x1 db) l1
    | x2 :: l2' => f x1 x2 :: merge f l1' l2' da db
    end
  end.

Lemma merge_empty_1 {A B C: Type} (f: A -> B -> C):
  ∀ l2 da db,
    merge f [] l2 da db = map (fun x2 => f da x2) l2.
Proof. easy. Qed.

Lemma merge_empty_2 {A B C: Type} (f: A -> B -> C):
  ∀ l1 da db,
    merge f l1 [] da db = map (fun x1 => f x1 db) l1.
Proof. by case. Qed.

Fixpoint merge3 {A B C D: Type} (f: A -> B -> C -> D)
               (l1: list A) (l2: list B) (l3: list C)
               (da: A) (db: B) (dc: C) : list D :=
  match l1 with
  | [] => merge (fun x2 x3 => f da x2 x3) l2 l3 db dc
  | x1 :: l1' =>
    match l2 with
    | [] => merge (fun x1 x3 => f x1 db x3) l1 l3 da dc
    | x2 :: l2' =>
      match l3 with
      | [] => merge (fun x1 x2 => f x1 x2 dc) l1 l2 da db
      | x3 :: l3' =>
        f x1 x2 x3 :: merge3 f l1' l2' l3' da db dc
      end
    end
  end.

Lemma merge3_empty_1 {A B C D: Type} (f: A -> B -> C -> D):
  ∀ l2 l3 da db dc,
    merge3 f [] l2 l3 da db dc = merge (fun x2 x3 => f da x2 x3) l2 l3 db dc.
Proof. easy. Qed.

Lemma merge3_empty_2 {A B C D: Type} (f: A -> B -> C -> D):
  ∀ l1 l3 da db dc,
    merge3 f l1 [] l3 da db dc = merge (fun x1 x3 => f x1 db x3) l1 l3 da dc.
Proof. by case. Qed.

Lemma merge3_empty_3 {A B C D: Type} (f: A -> B -> C -> D):
  ∀ l1 l2 da db dc,
    merge3 f l1 l2 [] da db dc = merge (fun x1 x2 => f x1 x2 dc) l1 l2 da db.
Proof. by case=> [|??] [|??]. Qed.

Fixpoint merge4 {A B C D E: Type} (f: A -> B -> C -> D -> E)
               (l1: list A) (l2: list B) (l3: list C) (l4: list D)
               (da: A) (db: B) (dc: C) (dd: D) : list E :=
  match l1 with
  | [] => merge3 (fun x2 x3 x4 => f da x2 x3 x4) l2 l3 l4 db dc dd
  | x1 :: l1' =>
    match l2 with
    | [] => merge3 (fun x1 x3 x4 => f x1 db x3 x4) l1 l3 l4 da dc dd
    | x2 :: l2' =>
      match l3 with
      | [] => merge3 (fun x1 x2 x4 => f x1 x2 dc x4) l1 l2 l4 da db dd
      | x3 :: l3' =>
        match l4 with
        | [] => merge3 (fun x1 x2 x3 => f x1 x2 x3 dd) l1 l2 l3 da db dc
        | x4 :: l4' =>
          f x1 x2 x3 x4 :: merge4 f l1' l2' l3' l4' da db dc dd
        end
      end
    end
  end.

Lemma merge4_empty_1 {A B C D E: Type} (f: A -> B -> C -> D -> E):
  ∀ l2 l3 l4 da db dc dd,
    merge4 f [] l2 l3 l4 da db dc dd = merge3 (fun x2 x3 x4 => f da x2 x3 x4) l2 l3 l4 db dc dd.
Proof. easy. Qed.

Lemma merge4_empty_2 {A B C D E: Type} (f: A -> B -> C -> D -> E):
  ∀ l1 l3 l4 da db dc dd,
    merge4 f l1 [] l3 l4 da db dc dd = merge3 (fun x1 x3 x4 => f x1 db x3 x4) l1 l3 l4 da dc dd.
Proof. by case. Qed.

Lemma merge4_empty_3 {A B C D E: Type} (f: A -> B -> C -> D -> E):
  ∀ l1 l2 l4 da db dc dd,
    merge4 f l1 l2 [] l4 da db dc dd = merge3 (fun x1 x2 x4 => f x1 x2 dc x4) l1 l2 l4 da db dd.
Proof. by case=> [|??] [|??]. Qed.

Lemma merge4_empty_4 {A B C D E: Type} (f: A -> B -> C -> D -> E):
  ∀ l1 l2 l3 da db dc dd,
    merge4 f l1 l2 l3 [] da db dc dd = merge3 (fun x1 x2 x3 => f x1 x2 x3 dd) l1 l2 l3 da db dc.
Proof. by case=> [|??] [|??] [|??]. Qed.

Lemma merge_length {A B C: Type} (f: A -> B -> C):
  ∀ l1 l2 da db,
    length (merge f l1 l2 da db) = max (length l1) (length l2).
Proof.
  elim.
  - move=> l2 > /=; case l2 =>> //=; by rewrite map_length.
  - move=>> IH [] > /=.
    all: by [rewrite map_length|rewrite IH].
Qed.

Lemma merge3_length {A B C D: Type} (f: A -> B -> C -> D):
  ∀ l1 l2 l3 da db dc,
    length (merge3 f l1 l2 l3 da db dc) = max (length l1) (max (length l2) (length l3)).
Proof.
  elim.
  - move=> l2 l3 > /=; case l2 =>> //=; case l3 =>> //=.
    all: by [rewrite map_length|rewrite merge_length].
  - move=>> IH [|??] [|??] > /=.
    all: by [rewrite map_length|rewrite merge_length|rewrite IH].
Qed.

Lemma merge4_length {A B C D E: Type} (f: A -> B -> C -> D -> E):
  ∀ l1 l2 l3 l4 da db dc dd,
    length (merge4 f l1 l2 l3 l4 da db dc dd)
    = max (length l1) (max (length l2) (max (length l3) (length l4))).
Proof.
  elim.
  - move=> l2 l3 l4 > /=; case l2 =>> //=; case l3 =>> //=; case l4 =>> //=.
    all: by [rewrite map_length|rewrite merge_length|rewrite merge3_length].
  - move=>> IH [|??] [|??] [|??] > /=.
    all: by [rewrite map_length|rewrite merge_length|rewrite merge3_length|rewrite IH].
Qed.

Ltac get_index k l y f :=
  match l with
  | [] => fail
  | ?x :: ?l =>
    tryif unify x y then f k
    else get_index (S k) l y f
  end.

Ltac match_in_both_union_list x f :=
  match goal with
  | |- PTree_union_list ?L1 = PTree_union_list ?L2 =>
    get_index 0%nat L1 x ltac:(fun k1 =>
      get_index 0%nat L2 x ltac:(fun k2 =>
        f L1 L2 k1 k2
      )
    )
  end.

Ltac remove_in_both_union_list x :=
  match_in_both_union_list x ltac:(fun L1 L2 k1 k2 =>
    cut_at k1 L1; cut_at k2 L2;
    apply union_list_In_both'; cbn [app]
  ).

Ltac remove_useless_minus_union_list x y :=
  match goal with
  | |- context [PTree_union_list ?L1] =>
    get_index 0%nat L1 x ltac:(fun k1 =>
      get_index 0%nat L1 (PTree_minus x y) ltac:(fun k2 =>
        match eval compute in (Nat.ltb k1 k2) with
        | true  =>
          let k2 := eval compute in (Nat.sub k2 (S k1)) in
          let L2 := eval cbn in (skipn (S k1) L1) in
          cut_at k1 L1;
          cut_at k2 L2;
          rewrite union_list_minus_eq_1
        | false =>
          let k1 := eval compute in (Nat.sub k1 (S k2)) in
          let L2 := eval cbn in (skipn (S k2) L1) in
          cut_at k2 L1;
          cut_at k1 L2;
          rewrite union_list_minus_eq_2
        end; rewrite ?app_nil_r; cbn [app]
      )
    )
  end.

Ltac simplify_merge :=
  repeat rewrite ?merge_empty_1 ?merge_empty_2
                 ?merge3_empty_1 ?merge3_empty_2 ?merge3_empty_3
                 ?merge4_empty_1 ?merge4_empty_2 ?merge4_empty_3 ?merge4_empty_4.

Lemma type_statement_remove_add ge f:
  ∀ s d ne_r,
    ∃ lb lr la,
      length lb = length lr /\ length lb = length la /\
      ∀ ne_n ne_e ne'_n,
        type_statement ge f d s ne_n ne_e ne_r = OK ne'_n ->
        ne'_n = res lb (ne_n :: ne_e) lr la.
Proof.
  elim.
  (* Sskip *)
  - move=>>; exists [true], [empty_iset], [empty_iset]; do 2 split=> //; move=> ? ne_e' > [<-] /=.
    by case: ne_e' => [|_ _]; rewrite PTree_minus_empty_r ?PTree_union_empty_r.
  (* Salloc *)
  - move=> i >; case Si': (fn_szenv' f)!i => [[|[|sz]]|].
    1,2,4: exists [], [], []; do 2 split=> //; move=>> /=.
    1,2,3: do 2 case/cdos =>> [_]; rewrite Si' //=; case/cdos =>> [_].
    1,2: by repeat destruct_match_goal.
    have [l Si]:= proj2 (fn_szenv_fn_szenv' f _) (ex_intro _ _ Si').
    have H:= fn_szenv_szenv'_str f _ _ _ Si Si'; inv H; destruct x as [|sze] => //.
    case Texp: (type_expression f sze) => [[? ne]|].
    2: exists [], [], []; do 2 split=> //; move=>> /=.
    2: case/cdos =>> [_]; rewrite Si Si' /=; case/cdos =>> [_].
    2: repeat destruct_match_goal.
    2: by rewrite Texp.
    exists [true], [PTree.set i tt (PTree.set sz tt empty_iset)], [ne];
      do 2 split=> //; move=> ? ne_e' > /=.
    case/cdos =>> [_]; rewrite Si Si' /=; case/cdos =>> [_].
    repeat destruct_match_goal.
    rewrite Texp /=; repeat destruct_match_goal; case=> <-; case: ne_e' => [|_ _].
    all: by simplify_sets; rewrite PTree_remove_comm union_comm_unit.
  (* Sfree *)
  - move=> i >; case Si': (fn_szenv' f)!i => [[|[|sz]]|].
    1,2,4: exists [], [], []; do 2 split=> //; move=>> /=.
    1,2,3: do 2 case/cdos =>> [_]; rewrite Si' //=; case/cdos =>> [_].
    1,2: case/cdo =>> [_]; by repeat destruct_match_goal.
    exists [true], [empty_iset], [PTree.set i tt (PTree.set sz tt empty_iset)];
      do 2 split=> //; move=> ? ne_e' >; cbn.
    do 2 case/cdos =>> [_]; rewrite Si'; cbn; case/cdos =>> [_]; case/cdo => - [] [_].
    repeat destruct_match_goal; case=> <-; case: ne_e' => [|_ _].
    all: by simplify_sets; rewrite ?union_set_unit set_comm_unit; simplify_sets.
  (* Sassign *)
  - case=> i l exp >.
    case Tp: (type_write_full_syn_path f (i, l)) => [[? ne1]|].
    case Texp: (type_expression f exp) => [[? ne2]|].
    2,3: exists [], [], []; do 2 split=> //; move=>>; rewrite/type_statement.
    2,3: by rewrite Tp //=; case/cdo =>> [_]; rewrite Texp.
    exists [true],
           [match l with [] => PTree.set i tt empty_iset | _ => empty_iset end],
           [PTree_union ne1 ne2];
      do 2 split=> //; move=> ? ne_e' >; rewrite/type_statement.
    rewrite Tp Texp /=; case/cdo => _ [_].
    repeat destruct_match_goal; case=> <-.
    simplify_sets; case l => [|_ _] //; case: ne_e' => [|_ _].
    all: simplify_sets.
    all: by rewrite (union_comm_unit (PTree_union _ _)) union_assoc_unit.
  (* Scall *)
  - case=> [i|] ? args > /=.
    all: case Targs: (type_syn_path_list f args) => [[? ne]|].
    1: exists [true], [PTree.set i tt empty_iset], [ne]; do 2 split=> //; move=> ? ne_e' >.
    3: exists [true], [empty_iset], [ne]; do 2 split=> //; move=> ? ne_e' >.
    2,4: exists [], [], []; do 2 split=> //; move=>>.
    1,2: case/cdo =>> [_].
    all: case/cdos =>> [_] //=.
    1: case/cdos =>> [_].
    all: case/cdo =>> [_]; repeat destruct_match_goal; case=> <-.
    all: simplify_sets; case: ne_e' => [|_ _]; simplify_sets.
    all: by rewrite union_comm_unit.
  (* Sreturn *)
  - case=> [exp|] ? ne_r /=.
    1: case (type_expression f exp) => [[? ne]|] /=.
    2: by exists [], [], [] =>>.
    1: exists [false], [empty_iset], [PTree_union ne ne_r]; do 2 split=> //; move=> ? ne_e' >.
    2: exists [false], [empty_iset], [ne_r]; do 2 split=> //; move=> ? ne_e' >.
    all: destruct_match_goal => - [<-].
    all: cbn -[PTree_union PTree_minus]; case: ne_e' => [|_ _].
    all: by simplify_sets.
  (* Sseq *)
  - move=> s1 IH1 s2 IH2 d ne_r /=.
    have [lb2 [lr2 [la2 [L2 [L2' H2]]]]]:= IH2 d ne_r.
    have [lb1 [lr1 [la1 [L1 [L1' H1]]]]]:= IH1 d ne_r.
    move: L1 L1' L2 L2' H1 H2 {IH1 IH2}.
    case: lb1 => [|b1 lb1]; case: lb2 => [|b2 lb2] //.
    all: case: lr1 => [|r1 lr1]; case: lr2 => [|r2 lr2] //.
    all: case: la1 => [|a1 la1]; case: la2 => [|a2 la2] //.
    + exists [], [], []; do 2 split=> //; move=> ? ne_e >.
      case T2: type_statement => //= /H1 ->; move/H2: T2 => -> //.
    + exists [], [], []; do 2 split=> //; move=> ? ne_e >.
      case T2: type_statement => //= /H1 ->; move/H2: T2 => -> //.
    + exists (false :: lb1), (empty_iset :: lr1), (a1 :: la1).
      do 2 split=> //; move=> ? ne_e >.
      case T2: type_statement => // /H1 ->; move/H2: T2 => ->.
      cbn -[PTree_union PTree_minus]; rewrite if_same.
      by rewrite !PTree_minus_empty_l.
    + move=> /= /eq_add_S L1 /eq_add_S L1' /eq_add_S L2 /eq_add_S L2' H1 H2.
      exists (b1 && b2 :: merge (fun x1 x2 => (b1 && x2) || x1) lb1 lb2 false false).
      exists (PTree_union r1 r2 ::
              merge4 (fun b1i b2i x1 x2 =>
                        if b1 && b2i && b1i then PTree_inter (PTree_union x2 r1) x1
                        else if b1 && b2i   then PTree_union x2 r1
                        else if b1i         then x1
                        else empty_iset)
                lb1 lb2 lr1 lr2 false false empty_iset empty_iset).
      exists (PTree_union (if b1 then PTree_minus a2 r1 else empty_iset) a1 ::
              merge (fun x1 x2 =>
                        PTree_union
                          (PTree_union (if b1 then PTree_minus x2 r1 else empty_iset) a1)
                          x1)
                la1 la2 empty_iset empty_iset).
      split.
      { rewrite /= merge4_length merge_length -L1 -L2; lia. }
      split.
      { rewrite /= merge_length merge_length -L1' -L2'; lia. }
      move=> ? ne_e > /=.
      case T2: type_statement => //= /H1 ->; move/H2: T2 => -> {H1 H2}.
      move: L1 L1' L2 L2'.
      have X: ∀ (l: list iset), match l with [] | _ => empty_iset end = empty_iset by case.
      elim: lb1 lr1 la1 lb2 lr2 la2 ne_e => [|b1' lb1' IH1] [|r1' lr1'] // [|a1' la1'] //.
      * elim=> [|b2' lb2' IH2] [|r2' lr2'] // [|a2' la2'] // ne_e.
        ** move=> /= _ _ _ _; rewrite X.
           case b1; case b2; cbn -[PTree_union PTree_minus]; simplify_sets => //.
           by rewrite union_comm_unit.
        ** move=> /= _ _ /eq_add_S L2 /eq_add_S L2'.
           have:= IH2 lr2' la2' _ eq_refl eq_refl L2 L2'.
           case: ne_e {IH2} => [/(_ [])|ne_e lne_e /(_ lne_e)] //.
           all: case b1; case b2; case b2'; cbn -[PTree_union PTree_minus].
           all: shelve.
      * case=> [|b2' lb2'] [|r2' lr2'] // [|a2' la2'] // ne_e.
        ** cbn; move=> /eq_add_S L1 /eq_add_S L1' _ _; rewrite X.
           have:= IH1 lr1' la1' [] [] [] _ L1 L1' eq_refl eq_refl.
           case: ne_e {IH1} => [/(_ [])|ne_e lne_e /(_ lne_e)] //.
           all: case b1; case b2; case b1'; cbn -[PTree_union PTree_minus].
           all: shelve.
        ** cbn; move=> /eq_add_S L1 /eq_add_S L1' /eq_add_S L2 /eq_add_S L2'.
           have:= IH1 lr1' la1' lb2' lr2' la2' _ L1 L1' L2 L2'.
           case: ne_e {IH1} => [/(_ [])|ne_e lne_e /(_ lne_e)] //.
           all: case b1; case b2; case b1'; case b2'; cbn -[PTree_union PTree_minus].
           all: shelve.
        Unshelve.
        all: rewrite ?X; simplify_sets; listify => H; rewrite ?orbT ?orb_false_r.
        all: try remove_in_both_union_list (PTree_minus a2' r1).
        all: try remove_in_both_union_list (PTree_minus ne_e r1').
        all: try remove_in_both_union_list (PTree_minus ne_e (PTree_union_list [r2'; r1])).
        all: try remove_in_both_union_list a1'.
        all: rewrite {1}H; simplify_merge.
        all: (apply (union_list_In_both a1); [simpl; tauto|simpl;tauto|]).
        all: simplify_sets.
        all: simpl; by repeat case PTree_eq_dec => //.
  (* Sassert *)
  - move=> exp >.
    case Texp: (type_expression f exp) => [[[||??|?|?|?] ne]|].
    2: exists [true], [empty_iset], [ne].
    1,3-7: exists [], [], [].
    all: repeat split=> //=; rewrite Texp //= => ? ne_e > [<-].
    by case: ne_e =>>; simplify_sets.
  (* Sblock *)
  - move=> s IH d ne_r.
    have [lb [lr [la [+ [++]]]]]:= IH (S d) ne_r.
    case: lb => [|b [|b' lb']]; case: lr => [|r [|r' lr']] //; case: la => [|a [|a' la']] //.
    all: move=> /= L L' H.
    1: exists [], [], [].
    2: exists [b], [r], [a].
    3: exists ((b || b') :: lb'),
              ((if b && b' then PTree_inter r r'
                else if b  then r
                else if b' then r'
                else empty_iset) :: lr'),
              (PTree_union a a' :: la').
    all: repeat split => //=; try lia.
    all: move=> ne_n ne_e > /H; case: ne_e =>>; simplify_sets => //.
    all: listify => ->; case b; case b'; cbn; simplify_sets.
    1,3: by rewrite (union_comm_unit _ a) (union_assoc_unit a _ _) (union_comm_unit a _).
    all: by rewrite union_comm_unit.
  (* Sifthenelse *)
  - move=> exp s1 IH1 s2 IH2 d ne_r.
    have [lb1 [lr1 [la1 [L1 [L1' H1]]]]]:= IH1 d ne_r.
    have [lb2 [lr2 [la2 [L2 [L2' H2]]]]]:= IH2 d ne_r.
    simpl; case (type_expression f exp) => [[[] ne]|] > /=.
    1,3-7: exists [], [], []; by repeat split=> //.
    move: L1 L1' L2 L2' H1 H2.
    have X: ∀ (l: list iset), match l with [] | _ => empty_iset end = empty_iset by case.
    case: lb1 => [|b1 lb1]; case: lb2 => [|b2 lb2] //.
    all: case: lr1 => [|r1 lr1]; case: lr2 => [|r2 lr2] //.
    all: case: la1 => [|a1 la1]; case: la2 => [|a2 la2] //.
    + exists [false], [empty_iset], [ne]; repeat split=> //.
      move=>>; case/cdo =>> [/H1] ->; case/cdo =>> [/H2] -> [<-]; cbn; rewrite X; by simplify_sets.
    + exists (b2 :: lb2), (r2 :: lr2), ((PTree_union ne a2) :: la2); repeat split=> //.
      move=>>; case/cdo =>> [/H1] ->; case/cdo =>> [/H2] ->.
      simplify_sets => - [<-]; cbn; simplify_sets; by rewrite union_comm_unit.
    + exists (b1 :: lb1), (r1 :: lr1), ((PTree_union ne a1) :: la1); repeat split=> //.
      move=>>; case/cdo =>> [/H1] ->; case/cdo =>> [/H2] ->.
      simplify_sets => - [<-]; cbn; simplify_sets; by rewrite union_comm_unit.
    + move=> /= /eq_add_S L1 /eq_add_S L1' /eq_add_S L2 /eq_add_S L2' H1 H2.
      exists (b1 || b2 :: merge (fun b1 b2 => b1 || b2) lb1 lb2 false false),
             ((if b1 && b2 then PTree_inter r1 r2
               else if b1  then r1
               else if b2  then r2
               else empty_iset) ::
              merge4 (fun b1 b2 x1 x2 =>
                        if b1 && b2 then PTree_inter x1 x2
                        else if b1  then x1
                        else if b2  then x2
                        else empty_iset)
                     lb1 lb2 lr1 lr2 false false empty_iset empty_iset),
             (PTree_union ne (PTree_union a1 a2) ::
              merge (fun x1 x2 => PTree_union x1 x2)
                    la1 la2 empty_iset empty_iset).
      split.
      { rewrite /= merge4_length merge_length -L1 -L2; lia. }
      split.
      { rewrite /= ?merge_length -L1' -L2'; lia. }
      move=> ne_n ne_e >.
      case/cdo => ne1 [T1]; case/cdo => ne2 [T2] [<-].
      move/H1: T1 => ->; move/H2: T2 => -> {H1 H2 IH1 IH2}.
      move: L1 L1' L2 L2'.
      elim: lb1 lr1 la1 lb2 lr2 la2 ne_e => [|b1' lb1' IH1] [|r1' lr1'] // [|a1' la1'] //.
      * elim=> [|b2' lb2' IH2] [|r2' lr2'] // [|a2' la2'] // ne_e.
        ** move=> /= _ _ _ _; rewrite X.
           case b1; case b2; cbn -[PTree_union PTree_minus]; simplify_sets => //.
           listify; by remove_in_both_union_list ne; remove_in_both_union_list a1.
           listify; by remove_in_both_union_list ne.
           listify; by remove_in_both_union_list (PTree_minus ne_n r2).
        ** cbn [length] => _ _ /eq_add_S L2 /eq_add_S L2'.
           have:= IH2 lr2' la2' _ eq_refl eq_refl L2 L2'.
           case: ne_e {IH2} => [/(_ [])|ne_e lne_e /(_ lne_e)] //.
           all: case b1; case b2; case b2'; cbn -[PTree_union PTree_minus].
           all: shelve.
      * case=> [|b2' lb2'] [|r2' lr2'] // [|a2' la2'] // ne_e.
        ** cbn; move=> /eq_add_S L1 /eq_add_S L1' _ _; rewrite X.
           have:= IH1 lr1' la1' [] [] [] _ L1 L1' eq_refl eq_refl.
           case: ne_e {IH1} => [/(_ [])|ne_e lne_e /(_ lne_e)] //.
           all: case b1; case b2; case b1'; cbn -[PTree_union PTree_minus].
           all: shelve.
        ** cbn; move=> /eq_add_S L1 /eq_add_S L1' /eq_add_S L2 /eq_add_S L2'.
           have:= IH1 lr1' la1' lb2' lr2' la2' _ L1 L1' L2 L2'.
           case: ne_e {IH1} => [/(_ [])|ne_e lne_e /(_ lne_e)] //.
           all: case b1; case b2; case b1'; case b2'; cbn -[PTree_union PTree_minus].
           all: shelve.
        Unshelve.
        all: rewrite ?X; simplify_sets; listify => H; rewrite ?orbT ?orb_false_r.
        all: try remove_in_both_union_list a1'.
        all: try remove_in_both_union_list a2'.
        all: try remove_in_both_union_list (PTree_minus ne_e r1').
        all: try remove_in_both_union_list (PTree_minus ne_e r2').
        all: rewrite {1}H; simplify_merge.
        all: simplify_sets.
        all: simpl; by repeat case PTree_eq_dec => //.
  (* Sloop *)
  - move=> s IH d ne_r /=.
    have [lb [lr [la [+ [++]]]]]:= IH d ne_r.
    case: lb lr la => [|b lb] [|r lr] // [|a la] //=.
    + move=> _ _ H.
      exists [], [], []; repeat split=> //.
      move=> ne_n ne_e >.
      case/cdo =>> [T1]; case/cdo =>> [T2] [<-].
      move/H: T2 => -> //.
    + move=> /eq_add_S L /eq_add_S L' H.
      exists (false :: lb), (r :: lr), (a :: la).
      split. { by rewrite /= L.  }
      split. { by rewrite /= L'. }
      move=> ne_n ne_e >.
      case/cdo =>> [T1]; case/cdo =>> [T2] [<-].
      move/H: T2 => ->; move/H: T1 => -> {H}.
      case b; cbn.
      * rewrite ?PTree_union_minus_distr_l; listify; simplify_sets.
        remove_useless_minus_union_list (res lb ne_e lr la) r.
        simplify_sets.
        by remove_useless_minus_union_list a r.
      * reflexivity.
  (* Sexit *)
  - move=> n d >.
    exists (repeat false (S n) ++ [true]), (repeat empty_iset (S (S n))), (repeat empty_iset (S (S n))).
    split. { rewrite app_length ?repeat_length /=; lia. }
    split. { rewrite app_length ?repeat_length /=; lia. }
    move=> ? ne_e >; rewrite/type_statement; case Nat.ltb => // - [<-]; cbn.
    simplify_sets.
    elim: n ne_e => [|n IH] [|? ne_e] //; cbn.
    + case ne_e; by simplify_sets.
    + move/(_ []): IH => /= <-; by case n.
    + move/(_ ne_e): IH; cbn => <-; by simplify_sets.
  (* Serror *)
  - exists [], [], []; repeat split=> //; move=>> [<-] //.
Qed.

Corollary type_statement_idempotent ge f:
  ∀ s d ne_e ne_r ne'_n ne''_n,
    type_statement ge f d s empty_iset ne_e ne_r = OK ne'_n ->
    type_statement ge f d s ne'_n ne_e ne_r = OK ne''_n ->
    ne'_n = ne''_n.
Proof.
  intros *.
  have [lb [lr [la [+ [++]]]]]:= type_statement_remove_add ge f s d ne_r.
  case: lb lr la => [|b lb] [|r lr] // [|a la] //=.
  - move=> _ _ H /H -> /H -> //.
  - move=> /eq_add_S L /eq_add_S L' H.
    move/H => -> /H -> {H}.
    case b; cbn.
    + rewrite ?PTree_union_minus_distr_l; listify; simplify_sets.
      remove_useless_minus_union_list (res lb ne_e lr la) r.
      simplify_sets.
      by remove_useless_minus_union_list a r.
    + reflexivity.
Qed.

Theorem progress_State (p: program):
  let ge := genv_of_program p in
  type_program p = OK tt ->
  forall e f s k,
    invariants p (State e f s k) ->
    exists t, NB.step_stmt ge (State e f s k) t /\
              invariants p t.
Proof.
  set ge := genv_of_program _ => /=. intros TPROG * INV.
  destruct s; inv INV.
  (* Sskip *)
  - case: TS WF VK => ->; case: k => //=.
    + move=>> WF; case VK: valid_cont =>> //= TS.
      eexists; split; [apply step_skip|]; econstructor; eassumption.
    + move=> k WF; case: ne_e => // >; case/cdo =>> [VK]; case incl => //; intros [= <-].
      eexists; split; [apply step_skip_block|]; econstructor; by [eassumption|].
  (* Salloc *)
  - move: TS => /=.
    case Ti: (fn_tenv f)!i => [t|] //=.
    case Si: (fn_szenv f)!i => [llsz|] //=.
    case Si': (fn_szenv' f)!i => [llsz'|] //=.
    case Pi: (fn_penv f)!i => [perm|] //=.
    case: t Ti => //= t Ti.
    case: llsz Si => [|[|szei []][]] //= Si.
    case: llsz' Si' => [|[|szi []][]] //= Si'.
    case: perm Pi => //= Pi.
    case Texp: type_expression => [[? ne]|] //=.
    case typ_reflect => // T; rewrite T {T} in Texp.
    case andP => //= - [PRIM] /typ_reflect VOID; intros [= <-].
    case/defined_iset_union_inv: DEF => D1 D2.
    have [[vsz|] Eexp]:= progress_expr _ _ _ _ _ WT Texp D1.
    (* Error: size is not a 64-bit integer *)
    2: eexists; split; [eapply step_alloc_ERR_sz|econstructor]; eassumption.
    have [T _] := subject_reduction_expr _ _ _ _ _ _ WT Eexp Texp; inv T.
    case (Z.le_gt_cases (Int64.unsigned n * typ_sizeof t) (Int64.max_unsigned)).
    (* Error: size is too large *)
    2: eexists; split; [eapply step_alloc_ERR_overflow|econstructor]; by [eassumption|lia].
    (* OK *)
    have [vdef INIT]:= primitive_type_default_value _ VOID PRIM.
    intro BOUND; eexists; split; [eapply step_alloc|econstructor]; try eassumption.
    + move=> j vj; case case_set => [<-|NEQ].
      * case=> <-; repeat eexists; try eassumption.
        econstructor.
        ** apply Forall_forall =>> /(@repeat_spec value) ->.
           by apply: default_value_wt_value INIT.
        ** by apply: default_value_wt_value INIT.
        ** have: szi <> i.
           { have:= mutable_var_not_in_size f (i, []) i _ => /=.
             rewrite Si' Pi /= => /(_ _ eq_refl eq_refl).
             move/(@Forall_inv (list ident))/(@Forall_inv ident).
             by apply not_eq_sym. }
           econstructor.
           by rewrite PTree.gso // PTree.gss.
           have:= fn_szenv'_szvars f _ _ Si'.
           move/(@Forall_inv (list ident))/(@Forall_inv ident);
             by case (fn_szenv' f)!szi => [[]|].
           econstructor.
        ** reflexivity.
        ** rewrite repeat_length /build_size /=; lia.
      * move/wt_env_assign_owned_size.
        move/(_ _ _ _ _ WT Si' Pi (in_eq _ _) (in_eq _ _) (wt_value_int64 _ _ _ n) NEQ).
        case=> tj [llszj] [Tj] [Sj] WTvj.
        repeat eexists; try eassumption.
        apply wt_value_assign => //.
        apply (mutable_var_not_in_size f (j, [])); by [rewrite /= Sj|rewrite Pi].
    + apply valid_cont_assign_non_mutable. by rewrite Pi.
      apply valid_cont_assign_non_mutable.
      { have:= fn_penv_szenv' f _ _ Si'.
        by move/(@Forall_inv (list ident))/(@Forall_inv ident) => ->. }
      eassumption.
    + reflexivity.
    + intro j; do 2 (case case_set => [<-|?]; eauto).
      have:= D2 j. do 2 case case_remove => //.
  (* Sfree *)
  - move: TS => /=.
    case Ti: (fn_tenv f)!i => [t|] //=.
    case Si: (fn_szenv f)!i => [llsz|] //=.
    case Si': (fn_szenv' f)!i => [llsz'|] //=.
    case Pi: (fn_penv f)!i => [perm|] //=.
    case Ui: ne'_n!i => //=.
    case SZ: forallb => //=.
    case: t Ti => //= t Ti.
    case: llsz Si => [|[|szei []][]] //= Si.
    case: llsz' SZ Si' => [|[|szi []][]] // SZ /= Si'.
    case: perm Pi => //= Pi.
    case PRIM: primitive_type => //; intros [= <-].
    have [> Ei]: ∃ lv, e!i = Some (Varr lv).
    { have:= DEF i; rewrite PTree.gss => - /(_ eq_refl) [> Ei].
      have:= WT _ _ Ei; rewrite Ti Si' => - [_] [_] [[<-]] [[<-]].
      inv 1; eauto. }
    eexists; split. econstructor; eassumption.
    econstructor.
    + move=> j >; do 2 case case_remove => //; intros ? NEQ.
      case/WT =>> [>] [Tj] [Sj] WTv.
      repeat eexists; try eassumption.
      apply wt_value_remove; last first.
      { apply Forall_forall =>> I1; apply Forall_forall =>> I2.
        have:= fn_penv_szenv' f _ _ Sj.
        move/Forall_forall/(_ _ I1)/Forall_forall/(_ _ I2) => + EQ.
        by rewrite -EQ Pi. }
      apply wt_value_remove => //.
      { apply Forall_forall =>> I1; apply Forall_forall =>> I2.
        exact: (fn_szenv'_owned f) Pi NEQ Si' Sj (in_eq _ _) (in_eq _ _) I1 I2. }
    + apply valid_cont_remove_non_mutable. by rewrite Pi.
      apply valid_cont_remove_non_mutable.
      { have:= fn_penv_szenv' f _ _ Si'.
        by move/(@Forall_inv (list ident))/(@Forall_inv ident) => ->. }
      eassumption.
    + reflexivity.
    + move=> j; have:= DEF j.
      case case_set => [<-|?]. by rewrite Ui.
      case case_set => [EQ|?].
      { move: SZ => /= ++ N; by rewrite EQ N. }
      do 2 case case_remove => //.
    + assumption.
  (* Sassign *)
  - case: s WF TS => i l WF.
    cbn -[type_write_full_syn_path check_writable].
    case TPATH: type_write_full_syn_path => [[t ne]|] //=.
    case Pi: (fn_penv f)!i => [perm|] //=.
    case X: (match perm with Shared => Error _ | _ => OK tt end) => //=.
    have Hperm: Mutable <=& perm by case: perm X {Pi}. clear X.
    case Texp: type_expression => [[texp neexp]|] //=.
    case PRIM: primitive_type => //=.
    case typ_reflect => //= T; intros [= <-]; rewrite T {T} in Texp PRIM.
    case/defined_iset_union_inv: DEF => D1 D2.
    case/defined_iset_union_inv: D1 => D1 Dexp.
    have [p' Ep]:= progress_full_path _ _ _ _ _ WT TPATH D1.
    move: (TPATH) => /=.
    case Ti: (fn_tenv f)!i => [ti|] //=.
    case Si: (fn_szenv f)!i => [llsz|] //=.
    case Nat.eqb_spec => //= LEN.
    case: l D2 Ep WF LEN TPATH => [|x s]; [|set l := x :: s; clearbody l].
    all: move=> D2 Ep WF LEN TPATH.
    all: have [[vexp|] Eexp]:= progress_expr _ _ _ _ _ WT Texp Dexp.
    2,4: repeat eexists; by [eapply step_assign_ERR_val|econstructor].
    all: have [llsz' Si'] := proj1 (fn_szenv_fn_szenv' f _) (ex_intro _ _ Si).
    all: rewrite Si'; case TPATH': type_syn_path_elem_list => [[? ne0]|] //=.
    all: have LEN': length llsz = length llsz'
          by exact (Forall2_length (fn_szenv_szenv'_str f _ _ _ Si Si')).
    all: have [WTvexp _]:= subject_reduction_expr _ _ _ _ _ _ WT Eexp Texp.
    all: have PRIMvexp := wt_value_primitive_type_primitive_value _ _ _ _ _ WTvexp;
          rewrite PRIM in PRIMvexp; symmetry in PRIMvexp.
    all: have WTvexp' := wt_value_well_typed_value _ _ _ _ _ WTvexp.
    all: move/wt_value_shrink in WTvexp.
    all: have MUTi: Coqlib.option_map (perm_le Mutable) (fn_penv f)!i = Some true
          by rewrite /= Pi; by case: perm Hperm {Pi}.
    all: case: p' Ep => [p'|] Ep.
    (* An error occured during path evaluation. *)
    2,4: repeat eexists; [eapply step_assign_ERR|econstructor]; eassumption.
    (* The path was evaluated without errors. *)
    (* Empty path, possibly initializing variable. *)
    + move: TPATH' => /[swap] /= -> [T _]; rewrite T {ti T} in Ti.
      have: get_tenv_path (fn_tenv f) (i, []) = Some t by rewrite /= Ti.
      have: llsz' = []; [|intros [= ->]].
      { move: (eq_sym LEN'); rewrite -LEN => /length_zero_iff_nil //. }
      inversion Ep; subst; inv H4.
      repeat eexists. eapply step_assign; by [eassumption|].
      econstructor.
      * eapply (wt_env_assign e _ i [] _ t _ (shrink t vexp)) => //.
        by rewrite /= Si'. assumption. by rewrite Pi.
      * apply (valid_cont_assign ge _ _ i [] (shrink t vexp) _ WT) => //.
        by rewrite Pi.
        rewrite/wt_value_path /= Si' Ti /=; by move/wt_value_dec: WTvexp.
        by apply primitive_value_shrink. eassumption.
      * reflexivity.
      * move=> j; have:= D2 j; case case_remove => //; case case_set => //; eauto.
      * assumption.
    (* Non-empty path, which has already been initialized. *)
    + case=> [T1 T2]; rewrite T1 -T2 in TPATH' TPATH D1.
      have TPATH'': f |- (i, l) ∈p t ¤ (PTree.set i tt ne0).
      { apply type_write_full_syn_path_type_syn_path; try eassumption.
        by rewrite PTree.gss. }
      have: get_tenv_syn_path (fd_tenv f) (i, l) = Some t.
      { eapply type_syn_path_get_tenv_syn_path; eassumption. }
      have:= eval_full_path_eval_path _ _ _ _ Ep.
      move/eval_path_get_env_path => /(_ _ _ WT TPATH'' D1) [v0 Ep'].
      have [e']:= get_env_path_update_env_path _ _ _ (shrink t vexp) Ep'.
      move/update_env_path_update_env_path' => UPD.
      rewrite (eval_full_path_get_tenv_path _ _ _ _ Ep) => Tp'.
      repeat eexists. eapply step_assign; eassumption.
      inv Ep.
      have [llsz1 [+ MTSZ]]:= fd_szenv_tenv_str_get_tenv_path f _ _ Tp'.
      case/fn_szenv_szenv'_str_get_szenv_path => llsz1' [Sp' MSZ].
      have: llsz1' = []; [|intros ->].
      { move: PRIM MTSZ MSZ; case t =>> // _; case llsz1 => // _ T; by inv T. }
      econstructor.
      * eapply (wt_env_assign e _ i sempl e' t _ (shrink t vexp)) => //.
        eassumption. assumption. by rewrite Pi.
      * apply (valid_cont_assign ge _ _ i sempl (shrink t vexp) _ WT) => //.
        by rewrite Pi.
        rewrite/wt_value_path Sp' Tp' /=; by move/wt_value_dec: WTvexp.
        by apply primitive_value_shrink. eassumption.
      * reflexivity.
      * move=> j /(D2 j); move: UPD => /=; case sempl => [[<-]|] >.
        ** case case_set => //; eauto.
        ** case e!i =>> //; cbn -[update_value].
           case update_value =>> //= [<-]; case case_set => //; eauto.
      * assumption.
  (* Scall *)
  - simpl in WF. rename i into idf. rename l into args.
    move: TS; rewrite/type_statement.
    case: o => [idv|]; [case PERM: check_writable => [[]|] //=|].
    all: case Gidf: (genv_of_program p)!idf => [f'|] //=.
    all: case Targs: type_syn_path_list => [[t ne]|] //=.
    case Tidv: (fn_tenv f)!idv => //=.
    all: case A: check_args => [[]|] //=.
    case: typ_reflect Tidv => // -> Tidv.
    have Tres: get_tenv_path (fn_tenv f) (idv, []) = (Some (sig_res (fd_sig f')))
                by rewrite /= Tidv.
    all: case (list_reflect _ typ_reflect) => // T; rewrite T {t T} in Targs.
    all: intros [= <-].
    all: case/defined_iset_union_inv: DEF => D1 D2.
    all: have [[pargs|] Eargs]:= progress_pathlist _ _ _ _ _ WT Targs D1.
    2,4: repeat eexists; [eapply step_call_ERR_args|econstructor]; eassumption.
    all: have [> Sargs]:= type_syn_path_list_get_szenv_syn_path_list _ _ _ _ Targs.
    all: have [vargs Epargs]:= eval_path_list_get_env_path_list _ _ _ _ _ _ WT Eargs Targs D1.
    all: have [WTargs _]:= subject_reduction_path_list _ _ _ _ _ _ _ _ WT Eargs Targs Sargs Epargs.
    all: have WTargs':= wt_value_well_typed_valuelist _ _ _ _ _ WTargs.
    all: have:= type_program_fundef _ _ _ TPROG Gidf;
         rewrite/type_fundef; case TSZ: type_size_expressions => // TF.
    all: move: (A); rewrite/check_args.
    all: case AP: args_params => [ap|] //=.
    all: case (match own_args _ _ with [] => OK tt | _ => _ end) => [[]|] //=.
    all: case all_arrays => //=.
    all: case SZSTR: check_szvars_structure => //=.
    all: case ROOTOWN: forallb => //=.
    all: case args_aliasing => //=; case enough_permission =>> //=.
    all: case U: existsb => //= _.
    all: have [pp PP]: exists pp, pargs_params (fd_params f') pargs = Some pp
          by apply ListUtils.map2_Some;
             rewrite -(eval_path_list_length _ _ _ _ Eargs);
             exact (proj1 (ListUtils.map2_Some _ _ _) (ex_intro _ _ AP)).
    all: have Pargs := check_args_enough_permission _ _ _ _ _ _ _ _ _ _ Eargs PP A.
    all: have NOALIAS := check_args_no_alias _ _ _ _ _ _ _ _ _ Eargs PP A.
    all: have ROOT := check_args_all_root_paths _ _ _ _ _ _ _ _ _ Eargs PP A.
    all: have ALLARRS := check_args_all_arrays _ _ _ _ _ _ _ _ _ Eargs PP A.
    all: have OWNne' := check_args_own_transfer_used_later _ _ _ _ _ _ _ _ _ Eargs PP A.
    all: have:= valid_call_tests_value_not_Stuck _ _ _ _ _ _ _ _
                  WT Eargs Epargs Targs WTargs' AP SZSTR TSZ.
    all: have LENargs: Datatypes.length args = Datatypes.length (sig_args (fd_sig f'))
          by rewrite (proj1 (ListUtils.map2_Some _ _ _) (ex_intro _ _ AP));
             exact (proj1 (fd_tenv_sig f')).
    all: have X := args_params_mut_own_incl (fd_penv f') _ _ _ PP.
    all: have sem_path_dec := fun x y => Bool.reflect_dec _ _ (eqSemPath x y).
    all: have NOALIASm := no_args_aliasing_restrict_mut sem_path_dec _ _ _ _ PP NOALIAS.
    all: move/negbTE: (NOALIAS) => NOALIAS'.
    all: set pe := fd_penv f'.
    all: have NOALIAS'' :=
      contra (args_aliasing_restricted' _ _ _ _ (incl_appl _ (List.incl_refl _)) X) NOALIAS.
    all: have CINCLmo := mut_own_args_fst_count_incl sem_path_dec pe _ _ _ PP.
    all: have NOALIASmo := contra (args_aliasing_restricted _ sem_path_dec
                                     _ _ _ CINCLmo) NOALIAS''.
    all: case Vtests: valid_call_tests_values => [vtests||] //= _;
          [case EQtests: (equal_tests vtests)|].
    (* Progress when size tests evaluation raises an error. *)
    all: have VENV:= valid_env_szenv_verified _ _ _ _ _ WT Eargs Epargs.
    3,6: eexists; split; [eapply step_call_ERR_tests|econstructor]; eassumption || reflexivity.
    (* Progress when size tests are not valid. *)
    2,4: eexists; split; [eapply step_call_ERR_invalid|econstructor]; eassumption || reflexivity.
    all: have OWN: Forall (fun i => (fn_penv f)!i = Some Owned) (map fst (map fst (own_args pe pp)))
          by apply Forall_forall =>>;
             case/(in_map_fst_pair (map fst (own_args pe pp))) =>>;
             case/(in_map_fst_pair (own_args pe pp)) =>> /filter_In [I];
             move/forallb_forall: Pargs => /(_ _ I); rewrite/pe/=;
             case (fn_penv f)!_ => [[]|] //=; by case (fd_penv f')!_ => [[]|].
    all: have WTr:= wt_env_remove_own_params _ (own_args (fd_penv f') pp) _ WT OWN.
    all: have fstOWN := eval_path_list_filter_args_by_perm _ _ pe Owned _ _ _ _ _
                          Eargs AP PP.
    all: destruct f' as [f'|f']; [shelve|]. (* Shelve cases for internal function calls *)
    (* External function calls. *)
    all: have Hf' := external_functions_properties f'.
    all: case EC: (external_call f' (combine pargs vargs)) => [vmarrs r].
    all: have WTres := ec_well_typed_res f' _ Hf' _ _ _ EC.
    all: have PRIM := ef_res_primitive f'.
    all: move/well_typed_value_bool_complete in WTres.
    all: have PRIMvexp := primitive_type_primitive_value _ _ WTres;
          rewrite PRIM in PRIMvexp; symmetry in PRIMvexp.
    all: have:= ec_mut_args_preserved f' _ Hf' _ _ _ _ _ EC.
    all: have LEN := get_env_path_list_length _ _ _ Epargs.
    all: rewrite (ListUtils.combine_map_fst _ _ LEN) => /(_ _ _ PP eq_refl) LEN'.
    all: have [e' UPDe]:= external_call_update_env_Some _ _ _ _ _ _
                            PP Epargs (eq_sym LEN') ROOT NOALIASmo.
    all: specialize external_call_valid_updated_env
            with (1 := WTr) (2 := PP) (3 := EC) (4 := UPDe)
              as [WT' STR].
    all: have HMUT := at_least_mutable_mut_arg _ _ _ Pargs.
    have Sidv: (fn_szenv' f)!idv = Some [].
    { have [l Sidv]:= proj1 (fn_tenv_fn_szenv f _) (ex_intro _ _ Tidv).
      have [l' Sidv']:= proj1 (fn_tenv_fn_szenv' f _) (ex_intro _ _ Tidv).
      have:= fn_szenv_szenv'_str f _ _ _ Sidv Sidv'; rewrite Sidv'.
      have:= fn_szenv_tenv_str f _ _ _ Tidv Sidv.
      case: sig_res PRIM =>> // _; case: l {Sidv} => // _ T; by inv T. }
    all: eexists; split;
          [ eapply step_call_External; eassumption || easy
          | econstructor; try (eassumption || easy)].
    all: move/well_typed_value_bool_correct in WTres.
    + apply (wt_env_assign _ _ idv [] _ (sig_res (ef_sig f')) [] r WT') => //.
      by rewrite /= Sidv.
      inv WTres => //; econstructor.
      move: PERM => /=; by case (fn_penv f)!idv => [[]|].
    + apply (valid_cont_assign _ _ e' idv [] r) => //.
      { move: PERM => /=; by case (fn_penv f)!idv => [[]|]. }
      { rewrite /wt_value_path /= Sidv Tidv /=; inv WTres => //; econstructor. }
      eapply valid_cont_return.
      { move/negP in NOALIASm; eassumption. }
      { eassumption. }
      { eassumption. }
      { eassumption. }
      suff H: valid_cont ge f (remove_own_params (own_args (ef_penv f') pp) e)
                         (ne_n :: lne_n) (ne_e :: lne_e) (ne_r :: lne_r) k = OK ne'_n;
              first by eassumption.
      rewrite remove_own_params_remove_list.
      elim: (map fst _) {1 2}e OWN VK => //= > IH > T VK; inv T; apply: IH => //.
      apply valid_cont_remove_non_mutable => //; congruence.
    + have D2': defined_iset (PTree.remove idv ne'_n)
                  (remove_list (map fst (map fst (own_args (ef_penv f') pp))) e).
      { move=> j; case case_remove_list => //; last by move=> _ /D2.
        case/(@in_map_fst_pair positive) =>>.
        case/(@in_map_fst_pair sem_path) =>>.
        case/(@In_nth _ _ _ (1%positive, [], 1%positive)) =>> [N] H.
        move/(existsb_nth _ (own_args (ef_penv f') pp) _ N): OWNne'.
        move/(_ (1%positive, [], 1%positive)); rewrite H.
        case case_remove => //; by case ne'_n!j => [[]|]. }
      move/defined_iset_return: (UPDe).
      rewrite remove_own_params_remove_list => /(_ _ D2') => D2''.
      move=> j /=; have:= D2'' j; case case_remove; case case_set => //; eauto.
    + eapply valid_cont_return; try eassumption.
      { move/negP in NOALIASm; eassumption. }
      suff H: valid_cont ge f (remove_own_params (own_args (ef_penv f') pp) e)
                         (ne_n :: lne_n) (ne_e :: lne_e) (ne_r :: lne_r) k = OK ne'_n;
              first by eassumption.
      rewrite remove_own_params_remove_list.
      elim: (map fst _) {1 2}e OWN VK => //= > IH > T VK; inv T; apply: IH => //.
      apply valid_cont_remove_non_mutable => //; congruence.
    + have D2': defined_iset ne'_n
                  (remove_list (map fst (map fst (own_args (ef_penv f') pp))) e).
      { move=> j; case case_remove_list => //; last by move=> _ /D2.
        case/(@in_map_fst_pair positive) =>>.
        case/(@in_map_fst_pair sem_path) =>>.
        case/(@In_nth _ _ _ (1%positive, [], 1%positive)) =>> [N] H.
        move/(existsb_nth _ (own_args (ef_penv f') pp) _ N): OWNne'.
        move/(_ (1%positive, [], 1%positive)); rewrite H.
        by case ne'_n!j => [[]|]. }
      move/defined_iset_return: (UPDe).
      rewrite remove_own_params_remove_list => /(_ _ D2') //.
    Unshelve.
    all: have MT := match_types_verified f f' e _ _ _ _ Eargs Targs PP.
    all: have VCALL := valid_function_call_verified _ _ _ _ _ _ AP VENV Vtests EQtests.
    all: have SZSTR':= same_szvars_structure_verified _ _ _ _ _ _ _ Eargs AP PP SZSTR.
    all: have VK' := valid_cont_remove_list_non_mutable _ _ _ _ _ _ _ _ _ OWN VK;
                     rewrite -remove_own_params_remove_list in VK'.
    all: move: TF; rewrite/type_function; case TBODY: type_statement => [ne1_n|] //=;
         case/cdo =>> [_]; case incl => //;
         case WFF: well_formed_function => // _.
    all: have WTpvargs:= wt_value_path_list_type_syn_path_list _ _ WT _ _ _ _ _ _ Targs Sargs Eargs Epargs.
    all: have HMUT := at_least_mutable_mut_arg _ _ _ Pargs.
    all: eexists; split;
          [ eapply step_call_Internal; eassumption || reflexivity
          | eapply inv_Callstate with (ne0 := ne'_n); try eassumption ].
    1,4: apply defined_iset_remove_own_params => //=.
    { case E: existsb => //.
      case/existsb_exists: E => - [[]] > [H]; case case_remove => // _.
      case/(In_nth _ _ (1%positive, [], 1%positive)): H =>> [H H'].
      have:= existsb_nth _ _ (1%positive, [], 1%positive) H OWNne'.
      by rewrite H' => ->. }
    4: reflexivity.
    2: move: PERM => /=; case: (fn_penv f)!idv => [[]|] //= _;
         rewrite Tidv /=; by case typ_reflect.
    all: move=>> /(proj1 (forallb_forall _ _) ALLARRS) /=;
         case get_tenv_path => [[]|] //; eauto.
  (* Sreturn *)
  - case: o WF TS => [exp|] /= WF.
    case Texp: type_expression => [[t neexp]|] //=.
    all: case typ_reflect => // R; intros [= <-].
    1: rewrite -R in Texp.
    have [DEFexp {DEF} - DEF]:= defined_iset_union_inv _ _ _ DEF.
    have [[v|] Eexp]:= progress_expr _ _ _ _ _ WT Texp DEFexp.
    (* Progress when returned expression evaluates to an error. *)
    2: eexists; split; [apply step_return_ERR; eassumption|econstructor].
    (* Normal cases *)
    (* return expr *)
    + have [WTv _]:= subject_reduction_expr _ _ _ _ _ _ WT Eexp Texp.
      have WTv' := wt_value_well_typed_value _ _ _ _ _ WTv.
      eexists; split; [apply step_return_Some; try eassumption|].
      rewrite -(primitive_type_primitive_value (sig_res (fn_sig f)) _).
      by apply well_typed_value_bool_complete.
      exact: (fn_res_primitive f).
      econstructor; try eassumption.
      by apply well_typed_value_bool_complete.
    (* return nothing *)
    + eexists; split; [apply step_return_None; try eassumption|].
      rewrite R; constructor.
      econstructor; try eassumption.
      by rewrite R.
  (* Sseq *)
  - move: TS => /=; case/cdo =>> [T2] T1.
    eexists; split; econstructor.
    + exact: WT.
    + simpl; rewrite -> VK => /=; exact: T2.
    + exact: T1.
    + exact: DEF.
    + by move: WF => /=; case: (mem_dec_spec _ wf_ret_reflect); case in_dec.
  (* Sassert *)
  - move: TS => /=; case/cdo => - [[]] > [Texp] //; intros [= <-].
    case/defined_iset_union_inv: DEF => D1 D2.
    have [[v|] Eexp]:= progress_expr _ _ _ _ _ WT Texp D2.
    (* The expression in assert raises an error during its evaluation *)
    2: eexists; split; by [eapply step_assert_ERR|constructor].
    (* Normal case *)
    have [H _]:= subject_reduction_expr _ _ _ _ _ _ WT Eexp Texp; inv H.
    case: b Eexp => Eexp.
    + eexists; split. eapply step_assert; eassumption.
      econstructor; reflexivity || eassumption.
    + eexists; split. eapply step_assert; eassumption.
      apply inv_State_error.
  (* Sblock *)
  - simpl in TS.
    eexists; split. constructor.
    eapply inv_State with (ne_e := ne'_n :: ne_e); try eassumption.
    simpl; rewrite -> VK => /=; by rewrite incl_refl.
  (* Sifthenelse *)
  - move: TS => /=; case/cdo => - [[]] > [Texp] //.
    case/cdo =>> [T1]; case/cdo =>> [T2]; intros [= <-].
    case/defined_iset_union_inv: DEF => Dexp.
    case/defined_iset_union_inv => D1 D2.
    move: WF => /= /wf_cont_app1 [WF1 WF2].
    have [[v|] Eexp]:= progress_expr _ _ _ _ _ WT Texp Dexp.
    (* The expression in assert raises an error during its evaluation *)
    2: eexists; split; by [eapply step_assert_ERR|constructor].
    (* Normal case *)
    have [H _]:= subject_reduction_expr _ _ _ _ _ _ WT Eexp Texp; inv H.
    case: b Eexp => Eexp.
    + eexists; split. eapply step_ifthenelse; eassumption.
      econstructor; eassumption.
    + eexists; split. eapply step_ifthenelse; eassumption.
      econstructor; eassumption.
  (* Sloop *)
  - move: TS => /=; case/cdo =>> [TS1]; case/cdo =>> [TS2]; intros [= <-].
    eexists; split. econstructor.
    have T:= type_statement_idempotent _ _ _ _ _ _ _ _ TS1 TS2; rewrite -T {T} in TS2 DEF.
    econstructor.
    + exact WT.
    + by simpl; rewrite -> VK => /=; rewrite TS1 /= TS2.
    + exact TS2.
    + exact DEF.
    + move: WF => /=; case (mem_dec_spec _ wf_ret_reflect) => I /=.
      * move=> WF; by apply wf_cont_app2.
      * by rewrite /= notin_remove.
  (* Sexit *)
  - destruct k => //; [|destruct n, ne_e => //]; move: VK => /=.
    + case VK: valid_cont => //= TS'.
      eexists; split; econstructor; eassumption.
    + move: TS => /=; intros [= <-]; case/cdo =>> [VK].
      case (incl_reflect _ unit_reflect) => // I; intros [= ->].
      eexists; split; econstructor; try (eassumption || reflexivity).
      move=>> /I; by apply: DEF.
    + move: TS => /=; case Nat.ltb_spec => // Hn; intros [= <-].
      case/cdo =>> [VK]; destruct_match_goal; intros [= ->].
      eexists; split; econstructor; try eassumption.
      simpl; case Nat.ltb_spec => //; lia.
  (* Serror *)
  - eexists; split; [constructor|apply inv_State_error].
  - eexists; split; [constructor|apply inv_State_error].
Qed.

Theorem progress p:
  type_program p = OK tt ->
  ∀ s,
    invariants p s ->
    (∀ r, ~ final_state s r) ->
    ∃ t, step_stmt (genv_of_program p) s t.
Proof.
  move=> TPROG [].
  - move=>> /(progress_State _ TPROG) [> [+ _]]; eexists; eassumption.
  - move=>> /(progress_Callstate _ _ _ _ TPROG) [> [+ _]]; eexists; eassumption.
  - move=>> /(progress_Returnstate _ _ _ _ _ TPROG)/[apply] - [> [+ _]]; eexists; eassumption.
Qed.

Theorem states_subject_reduction p:
  type_program p = OK tt ->
  ∀ s t,
    invariants p s ->
    step_stmt (genv_of_program p) s t ->
    invariants p t.
Proof.
  move=> TPROG [].
  - move=>> /(progress_State _ TPROG) [> [+ I]].
    by move/step_derministic/[apply] <-.
  - move=>> /(progress_Callstate _ _ _ _ TPROG) [> [+ I]].
    by move/step_derministic/[apply] <-.
  - move=>> /(progress_Returnstate _ _ _ _ _ TPROG) H /[dup] T.
    exploit H. { by move=>> X; inv X; inv T. }
    case=>> [+ I].
    by move/step_derministic/[apply] <-.
Qed.

End STATEMENTS_PROGRESS.

Theorem safety p:
  type_program p = OK tt ->
  ∀ s,
    (∀ r, ~ final_state s r) ->
    invariants p s ->
    ∃ t, step_stmt (genv_of_program p) s t /\ invariants p t.
Proof.
  move=> TPROG s H I. have [t S] := progress _ TPROG _ I H.
  exists t. split=> //. by apply: states_subject_reduction I S.
Qed.

Theorem initial_invariants p:
  type_program p = OK tt ->
  forall s,
    initial_state p s ->
    invariants p s.
Proof.
  move=> TPROG > I. inversion I; subst. apply: inv_Callstate_initial I.
  have:= type_program_fundef _ _ _ TPROG H. rewrite/type_fundef.
  by case type_size_expressions.
Qed.