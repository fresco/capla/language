Require Import ssreflect ssrbool.
Require Import PeanoNat BinPos Bool List.

Require Import BUtils ListUtils.

Definition ident := positive.
Definition ident_eq : ident -> ident -> bool := Pos.eqb.

Inductive sem_path_elem : Type :=
| Pcell: nat -> sem_path_elem.

Scheme Equality for sem_path_elem.

Definition sem_path : Type := ident * list sem_path_elem.

Theorem sem_path_elem_reflect (x y: sem_path_elem):
  reflect (x = y) (sem_path_elem_beq x y).
Proof.
  case: x. case: y =>> /=. case Nat.eqb_spec.
  - move=> ->. by apply: ReflectT.
  - move=> H. apply: ReflectF. by case.
Qed.

Lemma sem_path_elem_beq_comm (a b: sem_path_elem):
  sem_path_elem_beq a b = sem_path_elem_beq b a.
Proof. do 2 case sem_path_elem_reflect; intros; auto. exfalso; auto. Qed.

Definition sem_path_prefix (p1 p2: sem_path) :=
  let (id1, pl1) := p1 in
  let (id2, pl2) := p2 in
  Pos.eqb id1 id2 && prefix sem_path_elem_beq pl1 pl2.

Definition sub_path (p1 p2: sem_path) := sem_path_prefix p2 p1.

Lemma sub_path_refl: forall p, sub_path p p.
Proof.
  case=>> /=. rewrite Pos.eqb_refl prefix_refl => //.
  apply: sem_path_elem_reflect.
Qed.

Lemma sub_path_app:
  forall p id pl1 pl2,
    sub_path p (id, pl1 ++ pl2) ->
    sub_path p (id, pl1).
Proof.
  move=> [_+] _ l + /= /andP [->] /=. elim/list_ind: l => //= > IH.
  move=> [|x pl1] pl2 //. case/andP => -> /=. by apply: IH.
Qed.

Corollary sub_path_app':
  forall p id pl1 pl2,
    ~~ sub_path p (id, pl1) ->
    ~~ sub_path p (id, pl1 ++ pl2).
Proof. move=>>. apply: contra. by apply: sub_path_app. Qed.

Definition sem_path_alias (p1 p2: sem_path) :=
  let (id1, pl1) := p1 in
  let (id2, pl2) := p2 in
  Pos.eqb id1 id2 &&
  (prefix sem_path_elem_beq pl1 pl2 || prefix sem_path_elem_beq pl2 pl1).

Lemma sem_path_alias_comm (p1 p2: sem_path):
  sem_path_alias p1 p2 = sem_path_alias p2 p1.
Proof.
  move: p1 p2 => [id1 pl1] [id2 pl2] => /=.
  case Pos.eqb_spec => [->|].
  - rewrite Pos.eqb_refl => /=. move: pl1 pl2.
    elim/list_ind => /=. by case.
    move=> a1 l1 IH [|a2 l2] //=.
    rewrite sem_path_elem_beq_comm.
    by rewrite sem_path_elem_beq_comm -2!andb_orb_distrib_r IH.
  - by case Pos.eqb_spec => [->|].
Qed.

Definition sem_path_beq (p1 p2 : sem_path) : bool :=
  let (id1, pl1) := p1 in
  let (id2, pl2) := p2 in
  Pos.eqb id1 id2 && list_beq _ sem_path_elem_beq pl1 pl2.

Lemma eqSemPath: forall p1 p2, reflect (p1 = p2) (sem_path_beq p1 p2).
Proof.
  case=> [id1 l1]. case=> [id2 l2]. rewrite/sem_path_beq.
  case: (Pos.eqb_spec id1 id2) => [<-|Hneq] /=.
  - case: (list_reflect _ sem_path_elem_reflect) => [<-|Hneq].
    + by apply: ReflectT.
    + apply: ReflectF. by case.
  - apply: ReflectF. by case.
Qed.

Lemma sem_path_beq_true:
  forall x y, x = y <-> sem_path_beq x y = true.
Proof. intros. now case (eqSemPath x y). Qed.

Lemma sem_path_beq_false:
  forall x y, x <> y <-> sem_path_beq x y = false.
Proof. intros. now case (eqSemPath x y). Qed.

Lemma sub_path_path_beq1:
  forall p1 p2,
    sem_path_beq p1 p2 ->
    sub_path p1 p2.
Proof. move=>>. case: eqSemPath => [<-|] // _. by apply: sub_path_refl. Qed.

Lemma sub_path_path_beq2:
  forall p1 p2,
    sem_path_beq p1 p2 ->
    sub_path p2 p1.
Proof. move=>>. case: eqSemPath => [<-|] // _. by apply: sub_path_refl. Qed.

Lemma sub_path_prefix:
  forall id pl pl' p,
    prefix sem_path_elem_beq pl' pl ->
    ~~ sub_path (id, pl) p ->
    ~~ sub_path (id, pl') p.
Proof.
  move=> id /[swap]. elim/list_ind => /=.
  - move=> pl [id' pl'] /= _. case/nandP'. by case: Pos.eqb_spec.
    case=> /Pos.eqb_spec ->. rewrite Pos.eqb_refl. by case: pl'.
  - move=> x' pl' IH [] //= x pl [id1 pl1]. case/andP.
    move/sem_path_elem_reflect -> => Hpref /=.
    case: pl1 => //= y pl1. case: sem_path_elem_beq => //=.
    by apply: (IH pl (id1, pl1) Hpref).
Qed.

Definition sem_path_alias_list_incl (l1 l2: list sem_path) :=
  forall x n, count_occb sem_path_alias l1 x = n ->
         count_occb sem_path_alias l2 x >= n.

Lemma sem_path_alias_list_incl_refl:
  forall l, sem_path_alias_list_incl l l.
Proof. by move=>> ->. Qed.
