Require Import Coq.Lists.List.
Require Import BinNums BinInt.

Import ListNotations.

Require Import Types Ops BValues Syntax Typing BEnv.
Require Import SemPath Alias SemanticsCommon.
Require Import BUtils.

Require Import Integers Floats Maps Smallstep.
Require Import Errors Globalenvs PTreeaux.
Require AST Events.
Module CAST := AST.

Section SEMANTICS_BLOCKING.

Variable ge: genv.

Section EXPR.

Variable e: env.
Variable f: function.

Inductive eval_identlist: list ident -> list value -> Prop :=
| eval_ident_Enil: eval_identlist [] []
| eval_ident_Econs: forall id v lids lv,
  e!id = Some v ->
  f.(fn_szenv')!id = Some [] ->
  eval_identlist lids lv ->
  eval_identlist (id :: lids) (v :: lv).

Inductive eval_expr: expr -> value -> Prop :=
| eval_Const_int: forall sz sig n,
  eval_expr (Econst (Cint sz sig n)) (Vint n)
| eval_Const_int64: forall sig n,
  eval_expr (Econst (Cint64 sig n)) (Vint64 n)
| eval_Const_float32: forall f,
  eval_expr (Econst (Cfloat32 f)) (Vfloat32 f)
| eval_Const_float64: forall f,
  eval_expr (Econst (Cfloat64 f)) (Vfloat64 f)
| eval_Const_bool: forall b,
  eval_expr (Econst (Cbool b)) (Vbool b)
| eval_Cast: forall exp t1 t2 v1 v,
  eval_expr exp v1 ->
  sem_cast v1 t1 t2 = Some v ->
  eval_expr (Ecast exp t1 t2) v
| eval_Unop: forall op k e1 v1 v,
  eval_expr e1 v1 ->
  sem_unop op k v1 = Some v ->
  eval_expr (Eunop op k e1) v
| eval_Binop_arith: forall op k e1 e2 v1 v2 v,
  eval_expr e1 v1 ->
  eval_expr e2 v2 ->
  sem_binarith_operation op k v1 v2 = Some v ->
  eval_expr (Ebinop_arith op k e1 e2) v
| eval_Binop_cmp: forall op k e1 e2 v1 v2 v,
  eval_expr e1 v1 ->
  eval_expr e2 v2 ->
  sem_cmp_operation op k v1 v2 = Some v ->
  eval_expr (Ebinop_cmp op k e1 e2) v
| eval_Binop_cmpu: forall op k e1 e2 v1 v2 v,
  eval_expr e1 v1 ->
  eval_expr e2 v2 ->
  sem_cmpu_operation op k v1 v2 = Some v ->
  eval_expr (Ebinop_cmpu op k e1 e2) v
| eval_Access: forall p p' v,
  eval_path p p' ->
  get_env_path e p' = Some v ->
  primitive_value v = true ->
  eval_expr (Eacc p) v

with eval_path_elem_list: list syn_path_elem ->
                          list (list ident)  ->
                          list sem_path_elem -> Prop :=
| eval_path_elem_list_Nil: forall llsz,
  eval_path_elem_list [] llsz []
| eval_Scell: forall lidx synpl lsz llsz lvsz shape lvidx idx sempl,
  eval_identlist lsz lvsz ->
  natlist_of_Vint64 lvsz = Some shape ->
  eval_exprlist lidx lvidx ->
  build_index lvidx shape = Some idx ->
  (idx < build_size shape)%nat ->
  eval_path_elem_list synpl llsz sempl ->
  eval_path_elem_list (Scell lidx :: synpl) (lsz :: llsz) (Pcell idx :: sempl)

with eval_path: syn_path -> sem_path -> Prop :=
| eval_path_intro: forall id synpl llsz sempl,
  f.(fn_szenv')!id = Some llsz ->
  eval_path_elem_list synpl llsz sempl ->
  eval_path (id, synpl) (id, sempl)

with eval_full_path: syn_path -> sem_path -> Prop :=
| eval_full_path_intro: forall id synpl llsz sempl,
  f.(fn_szenv')!id = Some llsz ->
  length synpl = length llsz ->
  eval_path_elem_list synpl llsz sempl ->
  eval_full_path (id, synpl) (id, sempl)

with eval_exprlist: list expr -> list value -> Prop :=
| eval_Enil: eval_exprlist [] []
| eval_Econs: forall e v le lv,
  eval_expr e v ->
  eval_exprlist le lv ->
  eval_exprlist (e :: le) (v :: lv).

Lemma eval_identlist_nth_error:
  forall lids lv,
    eval_identlist lids lv ->
    forall n id,
      nth_error lids n = Some id ->
      exists v, nth_error lv n = Some v /\ e!id = Some v.
Proof.
  induction lids; simpl; intros. now destruct n.
  inversion_clear H. destruct n.
  + revert H0; intros [= <-]. eexists. split; simpl; eauto.
  + simpl; eauto.
Qed.

Lemma eval_full_path_eval_path:
  forall synp semp,
    eval_full_path synp semp ->
    eval_path synp semp.
Proof. intros * H. inversion_clear H. eapply eval_path_intro; eassumption. Qed.

Lemma eval_exprlist_nth_error:
  forall lexp lv,
    eval_exprlist lexp lv ->
    forall n exp,
      nth_error lexp n = Some exp ->
      exists v, nth_error lv n = Some v /\ eval_expr exp v.
Proof.
  induction lexp; simpl; intros. now destruct n.
  inversion_clear H. destruct n.
  + revert H0; intros [= <-]. eexists. split; simpl; eauto.
  + simpl; eauto.
Qed.

Lemma eval_identlist_length:
  forall lids lv,
    eval_identlist lids lv -> length lids = length lv.
Proof.
  induction lids, lv.
  + easy.
  + intro. inversion H.
  + intro. inversion H.
  + intro. inversion_clear H. simpl; apply eq_S. apply (IHlids _ H2).
Qed.

Lemma eval_exprlist_length:
  forall le lv,
    eval_exprlist le lv -> length le = length lv.
Proof.
  induction le, lv.
  + easy.
  + intro. inversion H.
  + intro. inversion H.
  + intro. inversion_clear H. simpl; apply eq_S. apply (IHle _ H1).
Qed.

Lemma eval_exprlist_app:
  forall le1 le2 lv1 lv2,
    eval_exprlist le1 lv1 ->
    eval_exprlist le2 lv2 ->
    eval_exprlist (le1 ++ le2) (lv1 ++ lv2).
Proof.
  intros. revert lv1 H. induction le1.
  + intros. inversion_clear H. apply H0.
  + intros. inversion_clear H. simpl.
    specialize (IHle1 _ H2). apply eval_Econs; assumption.
Qed.

Lemma eval_exprlist_rev:
  forall le lv,
    eval_exprlist le lv ->
    eval_exprlist (rev le) (rev lv).
Proof.
  induction le.
  + intros * H. inversion_clear H. exact eval_Enil.
  + intros * H. inversion_clear H. simpl.
    apply eval_exprlist_app. auto.
    apply eval_Econs. assumption. exact eval_Enil.
Qed.

Inductive eval_path_list: list syn_path -> list sem_path -> Prop :=
| eval_path_list_Nil: eval_path_list [] []
| eval_path_list_Cons: forall p lp p' lp',
    eval_path p p' ->
    eval_path_list lp lp' ->
    eval_path_list (p :: lp) (p' :: lp').

End EXPR.

Derive Inversion inv_eval_expr
  with (forall e f exp v, eval_expr e f exp v) Sort Prop.
Derive Inversion inv_eval_exprlist
  with (forall e f le lv, eval_exprlist e f le lv) Sort Prop.
Derive Inversion inv_eval_path
  with (forall e f p p', eval_path e f p p') Sort Prop.
Derive Inversion inv_eval_path_elem_list
  with (forall e f synpl llsz sempl, eval_path_elem_list e f synpl llsz sempl) Sort Prop.

Section STATEMENT.

Inductive cont : Type :=
| Kstop: cont
| Kseq: stmt -> cont -> cont
| Kblock: cont -> cont
| Kreturnto: option ident ->
             env ->
             function ->
             list (sem_path * ident) ->
             cont -> cont.

Inductive state : Type :=
| State
    (e: env)
    (f: function)
    (s: stmt)
    (k: cont) : state
| Callstate
    (fd:   function)
    (args: list value)
    (k:    cont) : state
| Returnstate
    (e:   env)
    (f:   function)
    (res: value)
    (k:   cont) : state.

Fixpoint destructCont (k: cont) : cont :=
  match k with
  | Kseq _ k | Kblock k => destructCont k
  | _ => k
  end.

Definition is_Kreturnto (k: cont) :=
  match k with
  | Kreturnto _ _ _ _ _ => True
  | _ => False
  end.

Definition is_Kreturnto_or_Kstop k :=
  match k with
  | Kstop | Kreturnto _ _ _ _ _ => True
  | _ => False
  end.

(*
Definition match_szenv (e: env) (f: function) :=
  forall i llsz llsz',
    (fn_szenv  f)!i = Some llsz ->
    (fn_szenv' f)!i = Some llsz' ->
    Forall2 (Forall2 (fun sz sz' =>
              exists n, eval_expr e f sz               (Vint64 n) /\
                        eval_expr e f (Eacc (sz', [])) (Vint64 n)
            )) llsz llsz'. *)

Inductive step_stmt: state -> state -> Prop :=
(* Sskip *)
| step_skip: forall e f s k,
  step_stmt (State e f Sskip (Kseq s k)) (State e f s k)
| step_skip_block: forall e f k,
  step_stmt (State e f Sskip (Kblock k)) (State e f Sskip k)
| step_skip_returnto: forall e f k,
  is_Kreturnto k ->
  well_typed_value f.(fn_sig).(sig_res) Vundef ->
  step_stmt (State e f Sskip k) (Returnstate e f Vundef k)
(* Sassign *)
| step_assign: forall e f k p p' t exp v e',
  eval_full_path e f p p' ->
  get_tenv_path f.(fn_tenv) p' = Some t ->
  eval_expr e f exp v ->
  primitive_value v = true ->
  option_map (perm_le Mutable) (fn_penv f)!(fst p) = Some true ->
  well_typed_value t v ->
  update_env_path' e p' (shrink t v) = Some e' ->
  step_stmt (State e  f (Sassign p exp) k) (State e' f Sskip k)
(* Salloc *)
| step_alloc: forall e f k i t sz isz v n,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  default_value t = Some v ->
  eval_expr e f sz (Vint64 n) ->
  Int64.unsigned n * typ_sizeof t <= Int64.max_unsigned ->
  step_stmt
    (State e f (Salloc i) k)
    (State (PTree.set i (Varr (repeat v (Z.to_nat (Int64.unsigned n))))
            (PTree.set isz (Vint64 n) e))
           f Sskip k)
| step_alloc_ERR_overflow: forall e f k i t sz isz n,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  eval_expr e f sz (Vint64 n) ->
  Int64.unsigned n * typ_sizeof t > Int64.max_unsigned ->
  step_stmt (State e f (Salloc i) k) (State e f Serror k)
(* Sfree *)
| step_free: forall e f k i t sz isz lv,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  e!i = Some (Varr lv) ->
  step_stmt
    (State e f (Sfree i) k)
    (State (PTree.remove i (PTree.remove isz e)) f Sskip k)
(* Scall *)
| step_call_Internal: forall e f k idvar idf args pargs vargs pp mut shr own f',
  ge!idf = Some (Internal f') ->
  eval_path_list e f args pargs ->
  get_env_path_list e pargs = Some vargs ->
  enough_permission (fn_penv f) (fn_penv f') pp = true ->
  pargs_params (fn_params f') pargs = Some pp ->
  (* List.incl pp (mut ++ shr ++ own) -> *)
  mut_args    (fn_penv f') pp = mut ->
  shr_args    (fn_penv f') pp = shr ->
  own_args    (fn_penv f') pp = own ->
  length args = length f'.(fn_sig).(sig_args) ->
  match_types (fn_tenv f) (fn_tenv f') pp = true ->
  well_typed_valuelist f'.(fn_sig).(sig_args) vargs ->
  all_root_paths own = true ->
  pargs_params_aliasing pargs (mut ++ own) = false ->
  step_stmt (State e f (Scall idvar idf args) k)
            (Callstate f' vargs
               (Kreturnto idvar (remove_own_params own e) f mut k))
| step_call_External: forall e f k idvar idf ef args pargs vargs pp mut shr own vmarrs r e',
  ge!idf = Some (External ef) ->
  eval_path_list e f args pargs ->
  get_env_path_list e pargs = Some vargs ->
  pargs_params (ef_params ef) pargs = Some pp ->
  (* List.incl pp (mut ++ shr ++ own) -> *)
  mut_args (ef_penv ef) pp = mut ->
  shr_args (ef_penv ef) pp = shr ->
  own_args (ef_penv ef) pp = own ->
  length args = length (ef_sig ef).(sig_args) ->
  well_typed_valuelist (ef_sig ef).(sig_args) vargs ->
  all_root_paths own = true ->
  pargs_params_aliasing pargs (mut ++ own) = false ->
  external_call ef (combine pargs vargs) = (vmarrs, r) ->
  (idvar <> None -> primitive_value r = true) ->
  update_env (build_env PTree.Empty (combine (map snd mut) vmarrs))
             (remove_own_params own e) mut = Some e' ->
  step_stmt (State e f (Scall idvar idf args) k)
            (State (set_optenv e' idvar r) f Sskip k)
(* Callstate *)
| step_callstate_Internal: forall f vargs k e e',
  build_func_env (PTree.empty value) f.(fn_params) vargs = Some e ->
  instantiate_size_vars e f = OK e' ->
  step_stmt (Callstate f vargs k) (State e' f f.(fn_body) k)
(* Sreturn *)
| step_return_None: forall e f k,
  well_typed_value f.(fn_sig).(sig_res) Vundef ->
  step_stmt (State e f (Sreturn None) k)
            (Returnstate e f Vundef k)
| step_return_Some: forall e f k exp v,
  eval_expr e f exp v ->
  well_typed_value f.(fn_sig).(sig_res) v ->
  step_stmt (State e f (Sreturn (Some exp)) k)
            (Returnstate e f v k)
(* Returnstate *)
| step_returnstate_seq: forall e f v s k,
  step_stmt (Returnstate e f v (Kseq s k)) (Returnstate e f v k)
| step_returnstate_block: forall e f v k,
  step_stmt (Returnstate e f v (Kblock k)) (Returnstate e f v k)
| step_returnstate: forall ecallee callee v optid e f marrs k e',
  marrs_varr e ecallee marrs = true ->
  match_types (fn_tenv f) (fn_tenv callee) marrs = true ->
  update_env ecallee e marrs = Some e' ->
  primitive_value v = true ->
  step_stmt (Returnstate ecallee callee v (Kreturnto optid e f marrs k))
            (State (set_optenv e' optid v) f Sskip k)
(* Sseq *)
| step_seq: forall e f k s1 s2,
  step_stmt (State e f (Sseq s1 s2) k) (State e f s1 (Kseq s2 k))
(* Sassert *)
| step_assert: forall e f k cond b,
  eval_expr e f cond (Vbool b) ->
  step_stmt (State e f (Sassert cond) k)
            (State e f (if b then Sskip else Serror) k)
(* Sifthenelse *)
| step_ifthenelse: forall e f k cond b s1 s2,
  eval_expr e f cond (Vbool b) ->
  step_stmt (State e f (Sifthenelse cond s1 s2) k) (State e f (if b then s1 else s2) k)
(* Sloop *)
| step_loop: forall e f s k,
  step_stmt (State e f (Sloop s) k) (State e f s (Kseq (Sloop s) k))
(* Sblock *)
| step_block: forall e f s k,
  step_stmt (State e f (Sblock s) k) (State e f s (Kblock k))
(* Sexit *)
| step_exit_block_seq: forall e f n s k,
  step_stmt (State e f (Sexit n) (Kseq s k)) (State e f (Sexit n) k)
| step_exit_block_skip: forall e f n k,
  step_stmt (State e f (Sexit (S n)) (Kblock k)) (State e f (Sexit n) k)
| step_exit_block_stop: forall e f k,
  step_stmt (State e f (Sexit O) (Kblock k)) (State e f Sskip k)
(* Serror *)
| step_error: forall e f k,
  step_stmt (State e f Serror k) (State e f Serror k).

End STATEMENT.

Inductive initial_state (p: program) : state -> Prop :=
| initial_state_intro: forall f,
  (genv_of_program p) ! (prog_main p) = Some (Internal f) ->
  f.(fn_sig) = {| sig_args := []; sig_res := Tint I32 Signed |} ->
  initial_state p (Callstate f [] Kstop).

Inductive final_state : state -> int -> Prop :=
| final_state_intro: forall e f i,
  final_state (Returnstate e f (Vint i) Kstop) i.

End SEMANTICS_BLOCKING.

Section COMPCERT_SEMANTICS.

Definition to_AST_globdef (l: list (ident * fundef)) : list (ident * CAST.globdef fundef typ):=
  map (fun x => (fst x, CAST.Gfun (snd x))) l.

Definition to_genv (prog_defs: list (ident * fundef)) : Genv.t fundef typ :=
  Genv.add_globals (Genv.empty_genv fundef typ (map fst prog_defs)) (to_AST_globdef prog_defs).

Definition step_event ge s t s' := t = Events.E0 /\ step_stmt ge s s'.

Lemma step_stmt_step_event:
  forall ge st1 st2,
    step_stmt ge st1 st2 ->
    step_event ge st1 Events.E0 st2.
Proof. intros. split; easy. Qed.

Definition semantics p :=
  Semantics_gen step_event (initial_state p) final_state (genv_of_program p) (to_genv p.(prog_defs)).

End COMPCERT_SEMANTICS.

Create HintDb semantics.
Global Hint Constructors eval_expr eval_exprlist eval_identlist : semantics.
Global Hint Constructors eval_path eval_full_path eval_path_elem_list : semantics.
Global Hint Constructors eval_path_list : semantics.
Global Hint Constructors step_stmt : semantics.
Global Hint Resolve eval_exprlist_app : semanticsnb.
Global Hint Resolve step_stmt_step_event : semantics.
