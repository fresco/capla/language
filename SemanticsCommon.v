Require Import ssreflect ssrbool ssrfun.
Require Import String List ZArith Lia.
Require Import Coq.Bool.Bool.

Require Import Types Syntax BValues BEnv Alias Typing.
Require Import SemPath Ops ExprSem.
Require Import BUtils Tactics ListUtils.
Require PTreeaux.

Require Import Integers Maps Errors Events.
Require Floats.

Import ListNotations.

Definition default_value t :=
  match t with
  | Tbool => Some (Vbool false)
  | Tint _ _ => Some (Vint Int.zero)
  | Tint64 _ => Some (Vint64 Int64.zero)
  | Tfloat F32 => Some (Vfloat32 Floats.Float32.zero)
  | Tfloat F64 => Some (Vfloat64 Floats.Float.zero)
  | _ => None
  end.

Lemma default_value_well_typed_value:
  forall t v,
    default_value t = Some v ->
    well_typed_value t v.
Proof. case=> [| | | |[]|] //= > [<-]; econstructor. Qed.

Local Open Scope option_bool_monad_scope.

Fixpoint get_type_path (pl: list sem_path_elem) (t: typ) : option typ :=
  match pl with
  | [] => Some t
  | Pcell idx :: pl =>
      match t with
      | Tarr t => get_type_path pl t
      | _ => None
      end
  end.

Definition get_tenv_path (te: tenv) (p: sem_path) :=
  let (id, pl) := p in
  option_bind te!id (get_type_path pl).

Fixpoint get_size_path {A: Type}
                       (pl: list sem_path_elem) (llsz: list (list A))
                       : option (list (list A)) :=
  match pl with
  | [] => Some llsz
  | Pcell idx :: pl =>
      match llsz with
      | [] => None
      | _ :: llsz => get_size_path pl llsz
      end
  end.

Definition get_szenv_path {A: Type}
                          (sze: PTree.t (list (list A))) (p: sem_path) :=
  let (id, pl) := p in
  option_bind sze!id (get_size_path pl).

Lemma get_type_path_app:
  forall l l' t0 t,
    get_type_path (l ++ l') t0 = Some t <->
    exists t', get_type_path l t0 = Some t' /\ get_type_path l' t' = Some t.
Proof.
  elim/list_ind => [|a l IH] l' t0 t /=.
  - split. eauto. case=> t' [[->]] //.
  - case: a => _. case: t0 => // *; apply: conj => // - [> []] //.
Qed.

Lemma get_tenv_path_app_eq:
  forall te te' i1 l1 i2 l2 l t,
    get_tenv_path te (i1, l1) = get_tenv_path te' (i2, l2) ->
    get_tenv_path te (i1, l1 ++ l) = Some t ->
    get_tenv_path te' (i2, l2 ++ l) = Some t.
Proof.
  move=> te te' i1 l1 i2 l2 l t /=.
  case: te!i1 => //= t1. case: te'!i2 => /= [t2|] /[swap]/get_type_path_app.
  all: case=> t' [-> //] *. apply: (proj2 (get_type_path_app _ _ _ _)). eauto.
Qed.

Lemma get_value_path_app:
  forall l l' v0 v,
    get_value_path (l ++ l') v0 = Some v <->
    exists v', get_value_path l v0 = Some v' /\ get_value_path l' v' = Some v.
Proof.
  elim/list_ind => [|a l IH] l' v0 v /=.
  - split. eauto. case=> t' [[->]] //.
  - case: a => idx. case: v0 => // *; apply: conj => //.
    1-6: case=>> [] //.
    all: case: nth_error =>> //=. 3: case=>> [] //.
    all: by move/IH.
Qed.

Lemma get_env_path_list_In e:
  forall lp lv p,
    get_env_path_list e lp = Some lv ->
    In p lp ->
    exists v, get_env_path e p = Some v.
Proof.
  elim/list_ind => //= p' lp + lv p.
  case Hp': (get_env_path e p') => //=.
  case (get_env_path_list e lp) =>> //= /(_ _ _ eq_refl) IH _.
  case=> [<-|]; eauto.
Qed.

Lemma get_env_path_list_nth_error e:
  forall lp lv k p,
    get_env_path_list e lp = Some lv ->
    nth_error lp k = Some p ->
    exists v, nth_error lv k = Some v /\ get_env_path e p = Some v.
Proof.
  elim/list_ind => /=.
  - move=> ? k > [<-]. by case k.
  - move=> p' lp + lv k p.
    case Hp': (get_env_path e p') => //=.
    case (get_env_path_list e lp) =>> //= /(_ _ _ _ eq_refl) IH [<-].
    case k => /=. case=> <-. eauto. eauto.
Qed.

Lemma get_env_path_list_nth_error' e:
  forall lp lv k v,
    get_env_path_list e lp = Some lv ->
    nth_error lv k = Some v ->
    exists p, nth_error lp k = Some p /\ get_env_path e p = Some v.
Proof.
  elim/list_ind => /=.
  - move=> ? k > [<-]. by case k.
  - move=> p' lp + lv k p.
    case Hp': (get_env_path e p') => //=.
    case (get_env_path_list e lp) =>> //= /(_ _ _ _ eq_refl) IH [<-].
    case k => /=. case=> <-. eauto. eauto.
Qed.

Lemma get_env_path_list_length e:
  forall lp lv,
    get_env_path_list e lp = Some lv ->
    length lp = length lv.
Proof.
  elim/list_ind => /=. by case.
  move=>> IH >. case get_env_path => //= >. case H: get_env_path_list => //=.
  move=> [<-] /=. by rewrite (IH _ H).
Qed.

Definition pargs_params (params: list ident) (pargs: list sem_path) :=
  args_params params pargs.

Lemma sem_path_alias_refl: forall p, sem_path_alias p p.
Proof.
  case=> [id pl] /=. rewrite Pos.eqb_refl => /=.
  by rewrite (prefix_refl _ sem_path_elem_reflect).
Qed.

Definition sem_path_ident_beq (p1 p2: sem_path * ident) :=
  let (p1, id1) := p1 in
  let (p2, id2) := p2 in
  sem_path_beq p1 p2 && Pos.eqb id1 id2.

Lemma sem_path_ident_reflect:
  forall (x y: sem_path * ident), reflect (x = y) (sem_path_ident_beq x y).
Proof.
  move=> [??] [??] /=. case: eqSemPath => [<-|Hpath].
  case: Pos.eqb_spec => [<-|Hpos].
  - by apply: ReflectT.
  - apply: ReflectF. by case.
  - apply: ReflectF. by case.
Qed.

Definition pargs_params_aliasing (args: list sem_path)
                                 (pp: list (sem_path * ident)) : bool :=
  args_aliasing sem_path_alias args pp.

Lemma pargs_params_aliasing_spec (args: list sem_path)
                                 (pp: list (sem_path * ident)):
  incl (map fst pp) args ->
  pargs_params_aliasing args pp = true <->
  exists x y z p1 p2 p id, x <> y /\
                      nth_error args x = Some p1 /\
                      nth_error args y = Some p2 /\
                      nth_error pp z = Some (p, id) /\
                      sem_path_alias p p1 /\ sem_path_alias p p2.
Proof. exact (args_aliasing_spec sem_path_alias args pp). Qed.

Lemma pargs_params_aliasing_spec':
  forall params pargs pp pe,
    pargs_params params pargs = Some pp ->
    pargs_params_aliasing pargs (mut_args pe pp ++ own_args pe pp) = false ->
    forall p p' i i',
      In (p, i) (mut_args pe pp ++ own_args pe pp) ->
      In (p', i') (mut_args pe pp ++ shr_args pe pp ++ own_args pe pp) ->
      (p, i) <> (p', i') ->
      ~~ sem_path_alias p p'.
Proof.
  move=> params pargs pp pe.
  set m := mut_args pe pp. set s := shr_args pe pp. set o := own_args pe pp.
  move=> PP NOALIAS.
  have M: pargs_params_aliasing pargs (m ++ o) <> true by now rewrite NOALIAS.
  have I: List.incl (map fst (m ++ o)) pargs.
  { move=> x Hin. apply: (args_params_list_incl_l _ _ _ PP).
    move: Hin. rewrite map_app => /(in_app_or (map fst m)).
    case=> /(in_map_fst_pair _ x) [?].
    move/mut_args_incl. by apply: in_map_pair_fst.
    move/own_args_incl. by apply: in_map_pair_fst. }
  have H := proj1 (not_iff_compat (args_aliasing_spec sem_path_alias pargs (m ++ o) I)) M.
  move=> q q' i i' I1 I2 N. case: negP => // T. elim: T => ALIAS. apply: H.
  have:= I1 => /(In_nth_error (m ++ o)) [n1' Hnth1'].
  have: In (q, i) pp => [|/(In_nth_error pp) [n1 Hnth1]].
  { case: (in_app_or _ _ _ I1).
    by apply: mut_args_incl. by apply: own_args_incl. }
  have: In (q', i') pp => [|/(In_nth_error pp) [n2 Hnth2]].
  { case: (in_app_or _ _ _ I2). by apply: mut_args_incl.
    case/(in_app_or s o). by apply: shr_args_incl. by apply: own_args_incl. }
  have: n1 <> n2 by congruence. exists n1, n2, n1', q, q', q, i.
  have T := args_params_nth_error_l _ _ _ _ _ _ PP.
  move: Hnth1 Hnth2 => /T + /T {T}. by rewrite sem_path_alias_refl.
Qed.

Definition all_root_paths (pp: list (sem_path * ident)) :=
  forallb (fun '(_, l, _) => match l with [] => true | _ => false end) pp.

Lemma all_root_paths_spec:
  forall pp, all_root_paths pp = true <->
        forall i l j, In (i, l, j) pp ->
                 l = [].
Proof.
  elim/list_ind => //=. move=> [[i l] j] pp IH. apply: conj.
  - case l => //= /IH + > [[_ <- _] //|] => /[apply] //.
  - case l => [H|] /=.
    apply: (proj2 IH) =>>. by apply: (fun x => H _ _ _ (or_intror _ x)).
    move=>> /(_ _ _ _ (or_introl _ eq_refl)) //.
Qed.

Lemma Coqlib_list_norepet_exists {A: Type} (eqb: A -> A -> bool)
                                           (Heq: forall x y, reflect (x = y) (eqb x y)):
  forall (l: list A),
    ~ Coqlib.list_norepet l ->
    exists i j x,
      i <> j /\
      nth_error l i = Some x /\
      nth_error l j = Some x.
Proof.
  have Heq_dec := (fun x y => reflect_dec _ _ (Heq x y)).
  elim/list_ind => /=.
  - move=> H. exfalso. apply: H. apply: Coqlib.list_norepet_nil.
  - move=> a l IH H. case: (in_dec Heq_dec a l).
    + move=> Hin. have [i Hi] := In_nth_error _ _ Hin. by exists (S i), 0, a.
    + case: (Coqlib.list_norepet_dec Heq_dec l) => Hnorepet Hnotin.
      * exfalso. apply: H. by apply: Coqlib.list_norepet_cons.
      * move: Hnorepet => /IH {H}. case=> i [j] [x] [Hsep H].
        exists (S i), (S j), x. apply: conj. lia. easy.
Qed.

Lemma pargs_params_not_in_params:
  forall params (pargs: list sem_path) pp id,
    ~ In id params ->
    pargs_params params pargs = Some pp ->
    ~ In id (map snd pp).
Proof.
  elim/list_ind => /=.
  - by case=> // - [] // > _ [<-].
  - move=> p params IH. case=> [|a pargs] pp id => //.
    case (Pos.eq_dec p id). move=> -> H _. exfalso. apply H. by left.
    case (in_dec Pos.eq_dec id params). move=> ? _ H. exfalso. apply: H. by right.
    move/(IH pargs) => /=. case: pargs_params =>> //.
    move/(_ _ eq_refl) => ++ _ [<-] /=. by move=> NotIn Neq [].
Qed.

Lemma mut_args_no_repet_params_snd:
  forall params (pargs: list sem_path) pp,
    Coqlib.list_norepet params ->
    pargs_params params pargs = Some pp ->
    Coqlib.list_norepet (map snd pp).
Proof.
  elim/list_ind => /=.
  - case=> //. by case.
  - move=> p params IH. case=> // a pargs > H.
    case (Coqlib_list_norepet_cons_inv _ _ H) => {H}.
    move=> /(pargs_params_not_in_params _ pargs) + /(IH pargs) /=.
    case: pargs_params =>> //. move=> /(_ _ eq_refl) NotIn /(_ _ eq_refl) NoRepet.
    case=> <-. apply: Coqlib.list_norepet_cons => //.
Qed.

Lemma mut_args_no_repet_params_snd':
  forall params (pargs: list sem_path) pp,
    Coqlib.list_norepet params ->
    pargs_params params pargs = Some pp ->
    forall p q i j, In (p, i) pp -> In (q, j) pp -> (p, i) <> (q, j) -> i <> j.
Proof.
  elim/list_ind => /=.
  - case=> //. by case.
  - move=> ? params IH. case=> // a pargs > H.
    case (Coqlib_list_norepet_cons_inv _ _ H) => {H}.
    move=> /(pargs_params_not_in_params _ pargs) + /(IH pargs) /=.
    case: pargs_params =>> //. move=> /(_ _ eq_refl) NotIn /(_ _ eq_refl) NoRepet.
    case=> <-. move=> p q i j /= H1 H2.
    move: H1 H2 NotIn => [[-> ->]|H1] /= [// [_ ->]|].
    + move=> H1. have:= in_map_pair_snd _ _ _ H1 => H1' H2 _ H.
      by rewrite H in H2.
    + have:= in_map_pair_snd _ _ _ H1 => H1' H2 _ H.
      by rewrite H in H1'.
    + move=> H2 _. by apply: NoRepet.
Qed.

Lemma pargs_params_no_repet_params:
  forall params (pargs: list sem_path) pp,
    Coqlib.list_norepet params ->
    pargs_params params pargs = Some pp ->
    Coqlib.list_norepet pp.
Proof.
  elim/list_ind => /=.
  - case=> //. case=> // _ _. by apply: Coqlib.list_norepet_nil.
  - move=> p params IH. case=> // a pargs > H.
    case (Coqlib_list_norepet_cons_inv _ _ H) => {H}.
    move=> /(pargs_params_not_in_params _ pargs) + /(IH pargs) /=.
    case: pargs_params =>> //. move=> /(_ _ eq_refl) NotIn /(_ _ eq_refl) NoRepet.
    case=> <-. apply: Coqlib.list_norepet_cons => //.
    move=> H. apply: NotIn. exact: (in_map_pair_snd _ _ _ H).
Qed.

Lemma pargs_params_aliasing_norepet_fst:
  forall pp,
    ~~ pargs_params_aliasing (map fst pp) pp ->
    Coqlib.list_norepet (map fst pp).
Proof.
  move=> pp Hpp. apply: Decidable.not_not.
  rewrite/Decidable.decidable.
  case: (Coqlib.list_norepet_dec
           (fun x y => reflect_dec _ _ (eqSemPath x y))
           (map fst pp)).
  by left. by right.
  move: Hpp => /negP. apply: contra_not => /Coqlib_list_norepet_exists.
  move/(_ sem_path_beq eqSemPath). case=> i [j] [x] [Hsep [Hi Hj]].
  apply: (proj2 (args_aliasing_spec sem_path_alias _ _ (incl_refl _))).
  exists i, j, i, x, x, x. have:= nth_error_map fst i pp. rewrite Hi Hj.
  case: nth_error => //= - [>] [<-]. rewrite sem_path_alias_refl. eauto 10.
Qed.

Lemma mut_args_aliasing_norepet:
  forall pp,
    ~~ pargs_params_aliasing (map fst pp) pp ->
    Coqlib.list_norepet pp.
Proof.
  move=> pp /pargs_params_aliasing_norepet_fst.
  elim/list_ind: pp => /= [_|]. by apply: Coqlib.list_norepet_nil.
  move=> [p id] /= > IH H. have:= Coqlib_list_norepet_cons_inv _ _ H.
  case {H} => NotIn /IH H. apply: Coqlib.list_norepet_cons => //.
  move=> Hin. apply: NotIn. exact: (in_map_pair_fst _ _ _ Hin).
Qed.

Lemma sem_path_alias_not_beq_app:
  forall id1 pl1 id2 pl2 pl pl',
    ~~ sem_path_alias (id1, pl1) (id2, pl2) ->
    ~~ sem_path_beq (id1, pl1 ++ pl) (id2, pl2 ++ pl').
Proof.
  move=> ? pl1 ? pl2 >. case/nandP' => /=. by move/negPf ->.
  case=> -> /=. case/norP. move: pl2. elim/list_ind: pl1 => //.
  move=> a pl1 IH [|b pl2] //=. rewrite sem_path_elem_beq_comm.
  case: sem_path_elem_beq => //=. by apply: IH.
Qed.

Definition remove_own_params (own: list (sem_path * ident)) (e: env) : env :=
  fold_right (fun '((i, _), _) => PTree.remove i) e own.

Lemma remove_own_params_own:
  forall own e i l j,
    In ((i, l), j) own ->
    (remove_own_params own e)!i = None.
Proof.
  induction own; simpl; intros. contradiction.
  case H as [-> | Hin]. exact: PTree.grs.
  case a => [[]] i' _ _. case: (Pos.eqb_spec i i') => [<-|].
  exact: PTree.grs. move/PTree.gro => ->. by apply: (IHown e i l j).
Qed.

Lemma remove_own_params_other:
  forall own e i,
    (forall i' l' j, In ((i', l'), j) own -> i' <> i) ->
    (remove_own_params own e)!i = e!i.
Proof.
  induction own; simpl; intros. reflexivity.
  destruct a as [[i' l'] j']. move: H. case: (Pos.eqb_spec i i') => [<-|Hneq].
  - move/(_ _ _ _ (or_introl _ eq_refl)) => //=.
  - move=> H. rewrite PTree.gro //. apply: IHown.
    move=>> Hin. exact: (H _ _ _ (or_intror _ Hin)).
Qed.

Definition marrs_varr (ecaller ecallee: env) (marrs: list (sem_path * ident)) :=
  forallb (fun '(p, id) => match get_env_path ecaller p with
                           | Some (Varr _) =>
                               match ecallee!id with
                               | Some (Varr _) => true
                               | _ => false
                               end
                           | _ => false
                           end) marrs.

Definition match_types (tecaller tecallee: tenv) (pp: list (sem_path * ident)) :=
  forallb (fun '(p, id) => Option.apply
                             (fun t1 => Option.apply (typ_beq t1) false tecallee!id)
                             false (get_tenv_path tecaller p)) pp.

Fixpoint update_value (v v': value) (p: list sem_path_elem) : option value :=
  match p, v with
  | [], _ => Some v'
  | Pcell idx :: p, Varr lv =>
      doo v   <- nth_error lv idx;
      doo v'  <- update_value v v' p;
      doo lv' <- replace_nth lv idx v';
      Some (Varr lv')
  | Pcell idx :: p, _ => None
  end.

Definition update_env_path e (p: sem_path) v :=
  let (i, l) := p in
  doo v' <- e!i;
  doo v' <- update_value v' v l;
  Some (PTree.set i v' e).

Definition update_env_path' e (p: sem_path) v :=
  let (i, l) := p in
  match l with
  | [] => Some (PTree.set i v e)
  | _  => update_env_path e (i, l) v
  end.

Lemma update_env_path'_already_defined:
  forall e i l v v',
    e!i = Some v ->
    update_env_path' e (i, l) v' = update_env_path e (i, l) v'.
Proof. move=> e i [] //= > -> //. Qed.

Lemma update_env_path_update_env_path':
  forall e p v e',
    update_env_path e p v = Some e' ->
    update_env_path' e p v = Some e'.
Proof.
  move=> e [i l] v e' /=. case e!i => //= >.
  case Hupdv: update_value => //= - [<-].
  case: l Hupdv => //=. by case=> <-.
Qed.

Fixpoint update_env (ecallee: env) (e: env)
                    (mutarrs: list (sem_path * ident)) : option env :=
  match mutarrs with
  | [] => Some e
  | ((id, pl), idcallee) :: mutarrs =>
      doo v' <- ecallee!idcallee;
      doo e' <- update_env_path e (id, pl) v';
      update_env ecallee e' mutarrs
  end.

Local Close Scope option_bool_monad_scope.

Lemma update_value_get_value_path:
  forall pl v v' r,
    update_value v v' pl = Some r ->
    get_value_path pl r = Some v'.
Proof.
  induction pl; simpl; intros. easy.
  destruct a, v; try discriminate. unfold option_bind in H.
  do 3 destruct_match H. revert H; intros [= <-].
  apply replace_nth_nth_error_same in Heq1. rewrite Heq1. eauto.
Qed.

Lemma update_value_get_value_path':
  forall pl v0 v v0',
    update_value v0 v pl = Some v0' ->
    exists v', get_value_path pl v0 = Some v'.
Proof.
  elim/list_ind => /=. eauto.
  move=> [idx] pl IH [] // lv v v0'.
  case: nth_error =>> //=. case Hupdv: update_value => //= _.
  by apply: (IH _ _ _ Hupdv).
Qed.

Lemma update_value_get_value_path_Varr:
  forall pl pl' v0 v v0' lv,
    update_value v0 v pl = Some v0' ->
    pl <> pl' ->
    ~~ prefix sem_path_elem_beq pl pl' ->
    get_value_path pl' v0' = Some (Varr lv) ->
    exists lv', get_value_path pl' v0 = Some (Varr lv').
Proof.
  move=> pl pl' v0 v v0' lv Hupdv.
  have [v'] := update_value_get_value_path' _ _ _ _ Hupdv.
  move: pl' pl v0 v v0' lv Hupdv. elim/list_ind => /=.
  - move=> [|[idx] pl] //=. case => //. eauto.
  - move=> [idx'] pl' IH [|[idx] pl] //= [] // lv0 v [] // lv0' lv.
    case: (Nat.eqb_spec idx idx') => [<-|Hneq].
    + rewrite (internal_nat_dec_lb idx _ eq_refl) /=.
      case: (nth_error lv0 idx) =>> //=.
      case Hupdv: update_value => //=. case Hrep: replace_nth => //=.
      case=> <-. rewrite (replace_nth_nth_error_same _ _ _ _ Hrep) /=.
      move=> + Hneq. have: pl <> pl' by congruence.
      move/[swap]. move: Hupdv. apply: IH.
    + case: (nth_error lv0 idx) =>> //=.
      case Hupdv: update_value => //=. case Hrep: replace_nth => //=.
      case=> <-. rewrite (replace_nth_nth_error_other _ _ _ _ Hrep _ Hneq).
      case: (nth_error lv0 idx') => //=. eauto.
Qed.

Lemma update_value_get_value_path_Varr':
  forall pl pl' v0 v v0' lv,
    update_value v0 v pl = Some v0' ->
    pl <> pl' ->
    ~~ prefix sem_path_elem_beq pl pl' ->
    get_value_path pl' v0 = Some (Varr lv) ->
    exists lv', get_value_path pl' v0' = Some (Varr lv') /\
                length lv = length lv'.
Proof.
  move=> pl pl' v0 v v0' lv Hupdv.
  have:= update_value_get_value_path _ _ _ _ Hupdv.
  move: pl' pl v0 v v0' lv Hupdv. elim/list_ind => /=.
  - move=> [|[idx] pl] //=. case =>> //.
    case nth_error =>> //=. case update_value =>> //=.
    case R: replace_nth =>> //= [<-]; eexists; split=> //.
    rewrite -(replace_nth_length _ _ _ _ R). congruence.
  - move=> [idx'] pl' IH [|[idx] pl] //= [] // lv0 v [] // lv0' lv.
    case: (Nat.eqb_spec idx idx') => [<-|Hneq].
    + rewrite (internal_nat_dec_lb idx _ eq_refl) /=.
      case: (nth_error lv0 idx) =>> //=.
      case Hupdv: update_value => //=. case Hrep: replace_nth => //=.
      case=> <-. rewrite (replace_nth_nth_error_same _ _ _ _ Hrep) /=.
      move=> + Hneq. have: pl <> pl' by congruence.
      move/[swap]. move: Hupdv. apply: IH.
    + case: (nth_error lv0 idx) =>> //=.
      case Hupdv: update_value => //=. case Hrep: replace_nth => //=.
      case=> <-. rewrite (replace_nth_nth_error_other _ _ _ _ Hrep _ Hneq).
      case: (nth_error lv0 idx') => //=. eauto.
Qed.

Lemma update_env_path_get_env_path:
  forall e p v e',
    update_env_path e p v = Some e' ->
    get_env_path e' p = Some v.
Proof.
  move=> e [i l] > /=. case e!i => //= >.
  case Hupdv: update_value => //= > [<-]. rewrite PTree.gss /=.
  exact: (update_value_get_value_path _ _ _ _ Hupdv).
Qed.

Lemma update_env_path_get_env_path':
  forall e i l v e',
    l <> [] ->
    update_env_path e (i, l) v = Some e' ->
    exists v', get_env_path e (i, l) = Some v'.
Proof.
  move=> e i l > /=. case l => //.
  case e!i => // > _. cbn -[update_value get_value_path].
  case Hupdv: update_value => // > _.
  exact: (update_value_get_value_path' _ _ _ _ Hupdv).
Qed.

Lemma update_env_path'_get_env_path:
  forall e p v e',
    update_env_path' e p v = Some e' ->
    get_env_path e' p = Some v.
Proof.
  move=> e [i l] > /=. case l.
  - case=> <-. by rewrite PTree.gss.
  - case e!i => // >. cbn -[update_value get_value_path].
    case Hupdv: update_value => // > [<-]. rewrite PTree.gss.
    exact: (update_value_get_value_path _ _ _ _ Hupdv).
Qed.

Lemma update_env_path'_get_env_path':
  forall e i l v e',
    l <> [] ->
    update_env_path' e (i, l) v = Some e' ->
    exists v', get_env_path e (i, l) = Some v'.
Proof.
  move=> e i l > /=. case l => //.
  case e!i => // > _. cbn -[update_value get_value_path].
  case Hupdv: update_value => // > _.
  exact: (update_value_get_value_path' _ _ _ _ Hupdv).
Qed.

Lemma get_env_path_update_env_path:
  forall e p v v',
    get_env_path e p = Some v ->
    exists e', update_env_path e p v' = Some e'.
Proof.
  move=> e [i l] v v' => /=. case e!i => //=.
  elim/list_ind: l => //=. eauto.
  move=> s l IH v0. case: s => [idx]. case: v0 => // lv.
  case Hnth: nth_error => [v0'|] //= /IH [>]. case l => [/=|>].
  - case (nth_error_replace_nth _ _ _ v' Hnth) =>> -> /=. eauto.
  - case: update_value => //= x.
    case (nth_error_replace_nth _ _ _ x Hnth) =>> -> /=. eauto.
Qed.

Lemma update_value_get_value_path_no_alias:
  forall l l' v0 v v0',
    update_value v0 v l = Some v0' ->
    ~~ prefix sem_path_elem_beq l l' ->
    ~~ prefix sem_path_elem_beq l' l ->
    get_value_path l' v0 = get_value_path l' v0'.
Proof.
  elim=> [|[idx] > IH] [|[?]?] // [] // > //=.
  case N1: nth_error =>> //=.
  case UPD: update_value =>> //=.
  case R: replace_nth =>> //= [<-].
  case Nat.eqb_spec => [<-|NEQ] /=.
  - case Nat.eqb_spec => //= _.
    rewrite (replace_nth_nth_error_same _ _ _ _ R) N1 /=.
    exact: IH UPD.
  - by rewrite (replace_nth_nth_error_other _ _ _ _ R).
Qed.

Corollary update_env_path_get_env_path_no_alias:
  forall p p' e v e',
    update_env_path e p v = Some e' ->
    ~~ sem_path_alias p p' ->
    get_env_path e p' = get_env_path e' p'.
Proof.
  move=> [i l] [i' l'] > /=.
  case Ei: _!_ =>> //=; case UPDV: update_value =>> //= [<-].
  case/nandP => [/Pos.eqb_spec NEQ|/norP [P1 P2]].
  - by case PTreeaux.case_set.
  - case PTreeaux.case_set => // <-; rewrite Ei {Ei} /=.
    exact: update_value_get_value_path_no_alias UPDV P1 P2.
Qed.

Lemma update_env_In e0:
  forall m e e' p j,
    ~ pargs_params_aliasing (map fst m) m ->
    In (p, j) m ->
    update_env e0 e m = Some e' ->
    exists vp vj, get_env_path e p = Some vp /\ e0!j = Some vj.
Proof.
  intros * NOALIAS I UPDE.
  have [> Ej]: exists vj, e0!j = Some vj.
  { move/(in_map_pair_snd m): I UPDE.
    elim: m e e' {NOALIAS} => //= - [[i l] j'] > IH ?? /=.
    case (Pos.eqb_spec j' j) => [-> _|NEQ].
    - case Ei: _!_ => //; by eexists.
    - case=> // I. case Ej: _!_ => //=.
      case Ei: _!_ =>> //=; case UPD: update_value =>> //=.
      by apply: IH. }
  have [> Ep]: exists vp, get_env_path e p = Some vp.
  { move/(in_map_pair_fst m): I UPDE.
    have:= List.incl_refl (map fst m).
    elim: {2 3 5 6}m e e' NOALIAS => // - [[i l] j'] > IH e1 e1' +++ UPD.
    case (eqSemPath (i, l) p) => [<- _ _ _|NEQ] /=.
    - move: UPD => /=; case _!_ =>> //=.
      case _!_ =>> //=; case UPD: update_value =>> //=.
      case/update_value_get_value_path': UPD =>> ->; by eexists.
    - case/negP/norP => N /negP NOALIAS INCL [] // I.
      move: UPD; cbn -[update_env_path].
      rewrite [update_env_path]lock.
      case Ej': _!_ => [v|] //=.
      unlock.
      case UPD: update_env_path =>> //=.
      case/(@List.incl_cons_inv sem_path): INCL => I' INCL.
      case/(IH _ _ NOALIAS INCL I) => v1'.
      have H: ~~ sem_path_alias (i, l) p.
      { case negP => //; elim=> H.
        move/negP in N; apply: N; apply arg_aliasing_spec.
        case/INCL/(In_nth_error (map fst m)): I => k1 N1.
        case/(In_nth_error (map fst m)): I' => k2 N2.
        exists k1, k2, p, (i, l); repeat split=> //.
        by move=> T; move: N2 N1; rewrite T => -> [].
        by apply: sem_path_alias_refl. }
      move/update_env_path_get_env_path_no_alias: UPD H => /[apply] ->.
      by eexists; eassumption. }
  by repeat eexists; eassumption.
Qed.

Theorem update_env_spec_1:
  forall ecallee marrs e e' p v,
    get_env_path e p = Some v ->
    ~ mem sem_path_alias p (map fst marrs) ->
    update_env ecallee e marrs = Some e' ->
    get_env_path e' p = Some v.
Proof.
  move=> ecallee. elim/list_ind. by move=>> ++ [<-].
  move=> [[id' pl'] idcallee] marrs IH e e' [id pl] v /=.
  case Hsep: ((id =? id')%positive && _) => //. case/nandP': Hsep.
  - move/negP => Hneq. case: ecallee!idcallee => //= >. case: e!id' => //= >.
    case: update_value => //= > + /IH/[apply] /= => + /(_ v).
    rewrite PTree.gso. move: Hneq; by case: (Pos.eqb_spec id id'). by auto.
  - case=> /Pos.eqb_spec <-. case/norP.
    case: e!id => //= vid. case: ecallee!idcallee => //= v' Hp1 Hp2 Hpl + H.
    have [vid' Hupdv]: exists v, update_value vid v' pl' = Some v.
    by move: H; case: update_value => //=; eauto.
    move: H; rewrite Hupdv => //= /IH/[apply] /= H. apply: H.
    rewrite PTree.gss => /=. move: pl pl' vid vid' Hp1 Hp2 Hupdv Hpl {IH}.
    elim/list_ind => // e1 pl IH [|e2 pl'] vid vid' //=.
    case: e1 => i1. case: e2 => i2. case: vid => // lv. case/nandP'.
    + move/sem_path_elem_reflect => Hsep. have {Hsep}: i2 <> i1 by auto.
      move=> Hsep _. case: nth_error => //= >. case Hnth: nth_error => //=.
      case: update_value => //= >. case Hrep: replace_nth => //= > [<-].
      rewrite (replace_nth_nth_error_other _ _ _ _ Hrep _ Hsep).
      by rewrite Hnth.
    + case. case/sem_path_elem_reflect => ->.
      case: sem_path_elem_reflect => //= _. case: nth_error => //= vid.
      case Hupdv: update_value => //= >. case Hrep: replace_nth => //= ++ [<-].
      rewrite (replace_nth_nth_error_same _ _ _ _ Hrep) => /=. by eauto.
Qed.

Lemma pargs_params_aliasing_cons:
  forall pp l p,
    incl (map fst pp) l ->
    pargs_params_aliasing l pp ->
    pargs_params_aliasing (p :: l) pp.
Proof.
  move=>> /[dup] Hincl /args_aliasing_spec/[apply].
  case=> x [y] [z] [p1] [p2] [p] [id] [Hsep] [Hx] [Hy] [Hz] [Hp1] Hp2.
  apply: (proj2 (args_aliasing_spec _ _ _ _)).
  - by apply: incl_tl.
  - exists (S x), (S y), z, p1, p2, p, id => /=. auto 10.
Qed.

Corollary no_pargs_params_aliasing_cons:
  forall pp l p,
    incl (map fst pp) l ->
    ~ pargs_params_aliasing (p :: l) pp ->
    ~ pargs_params_aliasing l pp.
Proof. by move=> ?? p /(pargs_params_aliasing_cons _ _ p)/contra_not. Qed.

Theorem update_env_spec_2:
  forall ecallee pp e e',
    ~ pargs_params_aliasing (map fst pp) pp ->
    update_env ecallee e pp = Some e' ->
    forall p idcallee v,
      In (p, idcallee) pp ->
      ecallee!idcallee = Some v ->
      get_env_path e' p = Some v.
Proof.
  move=> ecallee. elim/list_ind => // - [[id pl] idcallee] marrs IH e e' /=.
  rewrite/pargs_params_aliasing /=.
  case: Pos.eqb_spec => // _. rewrite (prefix_refl _ sem_path_elem_reflect).
  case H: mem => //= Hnoalias. move: H => /negP H.
  case Hcallee: ecallee!idcallee => //=. case He_id: e!id => //=.
  case Hupdv: update_value => //= Hupde > [[<- <-]|].
  - rewrite Hcallee => <-.
    apply: update_env_spec_1 _ _ _ _ _ _ _ H Hupde => /=.
    rewrite PTree.gss => /=. apply: update_value_get_value_path. exact: Hupdv.
  - apply: (IH _ _ _ Hupde). move=> Hnoalias'. apply: Hnoalias.
    by apply: pargs_params_aliasing_cons.
Qed.

Lemma update_env_spec_2':
  forall ecallee pp e e',
    ~ pargs_params_aliasing (map fst pp) pp ->
    update_env ecallee e pp = Some e' ->
    forall i l i' l',
      In (i, l, i') pp ->
      get_env_path ecallee (i', l') = None ->
      get_env_path e' (i, l ++ l') = None.
Proof.
  move=> ecallee. elim/list_ind => // - [[id pl] idcallee] marrs IH e e'.
  move=> /[dup] NOALIAS + /[dup] Hupde => /=.
  rewrite/pargs_params_aliasing /=.
  rewrite Pos.eqb_refl (prefix_refl _ sem_path_elem_reflect).
  case H: mem => //= Hnoalias. move: H => /negP H.
  case Hcallee: ecallee!idcallee => //=. case He_id: e!id => //=.
  case Hupdv: update_value => //= Hupde'. move=>> [[<- <- <-]|Hin].
  - rewrite Hcallee /= => N.
    have:= update_env_spec_2 _ _ _ _ NOALIAS Hupde (id, pl) idcallee _ (in_eq _ _) Hcallee.
    move=> /=. case: e'!id => //= > Hpl.
    case T: get_value_path => //. case/get_value_path_app: T.
    rewrite Hpl =>> [[<-]]. by rewrite N.
  - apply: (IH _ _ _ Hupde') => // T.
    apply: Hnoalias. by apply: pargs_params_aliasing_cons.
Qed.

Theorem update_env_spec_3:
  forall ecallee marrs e e' i v,
    e'!i = Some v ->
    update_env ecallee e marrs = Some e' ->
    exists v', e!i = Some v'.
Proof.
  move=> ecallee. elim/list_ind => /=. move=>> + [->]. eauto.
  move=> [[i l] idcallee] marrs IH e e' i' v.
  case Hcallee: ecallee!idcallee => //=.
  case He_i: e!i => //=. case Hupdv: update_value => //=.
  move/IH/[apply]. case: (Pos.eqb_spec i' i) => [->|Hneq]. eauto.
  by rewrite PTree.gso.
Qed.

Fixpoint unchanged (i: ident) (l: list sem_path_elem)
                   (marrs: list (sem_path * ident)) (v v': value)
                   {struct v} : Prop :=
  if ~~ (mem sub_path (i, l) (map fst marrs)) then
    match v, v' with
    | Varr lv, Varr lv' =>
        let f := fix f j lv lv' :=
            match lv, lv' with
            | [], [] => True
            | _, [] | [], _ => False
            | v :: lv, v' :: lv' =>
                unchanged i (l ++ [Pcell j]) marrs v v' /\
                f (S j) lv lv'
            end in
        f 0 lv lv'
    | _, _ => v = v'
    end
  else True.

Theorem unchanged_Varr:
  forall marrs lv lv' i l,
    ~~ mem sub_path (i, l) (map fst marrs) ->
    unchanged i l marrs (Varr lv) (Varr lv') ->
    length lv = length lv' /\
    forall j v v',
      nth_error lv  j = Some v ->
      nth_error lv' j = Some v' ->
      unchanged i (l ++ [Pcell j]) marrs v v'.
Proof.
  move=> marrs lv lv' i l /=. move=> -> H. apply: conj.
  - move: lv lv' 0 H. elim/list_ind => [|> IH] [] // > [_] /IH /=. lia.
  - move=> j. rewrite -{3}(Nat.add_0_l j).
    move: lv lv' 0 l H j. elim/list_ind => [|v lv IH] [|v' lv'] // n l.
    + move=> _. by case.
    + case=> Hv /IH Hlv. case=> /=.
      move=>> [<-] [<-]. by rewrite Nat.add_0_r.
      move=> idx > H1 H2. have:= Hlv idx _ _ H1 H2. by rewrite Nat.add_succ_comm.
Qed.

Lemma unchanged_eq:
  forall marrs v i l, unchanged i l marrs v v.
Proof.
  move=> marrs. elim/value_ind' => /=.
  1-6: move=>>; by case: mem.
  move=> lv IH >. case: mem => //=.
  move: lv 0 IH. elim/list_ind => //= v lv IHlv n.
  case/Forall_cons_iff => Hv Hlv. apply: conj => //.
  by apply: IHlv.
Qed.

Lemma unchanged_trans:
  forall marrs v v' v'' i l,
    unchanged i l marrs v v' ->
    unchanged i l marrs v' v'' ->
    unchanged i l marrs v v''.
Proof.
  move=> marrs. elim/value_ind' => /=.
  1-6: move=>>; case Hmem: mem => //=.
  1-6: move=> <- /=; rewrite Hmem //.
  move=> lv IH v' v'' i l. case Hmem: mem => //=.
  case: v' => //= lv'. rewrite Hmem /=. case: v'' => //= lv''.
  move: lv' lv'' 0 IH.
  elim/list_ind: lv => [|v lv IH] [|v' lv'] // [|v'' lv''] //.
  move=> n. case/Forall_cons_iff => H Hlv.
  case=> H1 H1'. case=> H2 H2'. apply: conj.
  exact: (H _ _ _ _ H1 H2). exact: (IH _ _ _ Hlv H1' H2').
Qed.

Lemma unchanged_cons:
  forall marrs x v v' i l,
    unchanged i l marrs v v' ->
    unchanged i l (x :: marrs) v v'.
Proof.
  move=> marrs x. elim/value_ind' => /=.
  1-6: move=>>; case: mem => /=; by [rewrite orb_true_r|case: sub_path].
  move=> lv IH v' i l. case: mem => /=.
  - by rewrite orb_true_r.
  - case: sub_path => //=. case: v' => //=. move: lv 0 IH.
    elim/list_ind => //= v lv IHlv n. case/Forall_cons_iff => H Hlv.
    case=> // v' lv'. case=> H1 H1'. apply: conj.
    by apply: H. by apply: IHlv.
Qed.

Theorem update_value_unchanged:
  forall marrs v0 v0' v i l l' ,
    In (i, l ++ l') (map fst marrs) ->
    update_value v0 v l' = Some v0' ->
    unchanged i l marrs v0 v0'.
Proof.
  move=> marrs. elim/value_ind' => [|b|n|n|f|f|lv IH] v0' v i l [|[idx] l'] //=.
  1-7: rewrite app_nil_r.
  1-7: have: sub_path (i, l) (i, l)
    by rewrite/sub_path /= Pos.eqb_refl (prefix_refl _ sem_path_elem_reflect).
  1-7: move: {2 3}(i, l) => p Hsub Hin.
  1-7: have:= ex_intro (fun _ => _) p (conj Hin Hsub).
  1-7: move/mem_spec -> => //.
  case Hnth: nth_error => //=. case Hupdv: update_value => //=.
  case Hrep: replace_nth => [lv'|] //= + [<-].
  case Hmem: mem => //= Hin.
  move: Hin IH Hrep Hnth. have: 0 + idx = idx by lia.
  move: lv {1 4 5}idx lv' 0. elim/list_ind => [|a lv IHlv] [|idx0] //=.
  - move=>>. rewrite Nat.add_0_r => -> Hin + [<-] [H]. rewrite -H {H} in Hupdv.
    change (Pcell idx :: l') with ([Pcell idx] ++ l') in Hin.
    rewrite app_assoc in Hin.
    case/Forall_cons_iff. move/(_ _ _ _ _ _ Hin Hupdv) => H Hlv.
    apply: conj => //. move {Hlv IHlv Hupdv Hin H v}.
    move: lv idx. elim/list_ind => // v lv IH idx.
    apply: conj. by apply: unchanged_eq. by apply: IH.
  - move=> lv' n Hn Hin. case/Forall_cons_iff.
    change (Pcell idx :: l') with ([Pcell idx] ++ l') in Hin.
    move=> H Hlv. case Hrep: replace_nth => //= - [<-] Hnth.
    apply: conj. by apply: unchanged_eq.
    apply: (IHlv idx0 _ _ _ Hin Hlv Hrep Hnth). lia.
Qed.

Theorem update_env_unchanged:
  forall ecallee marrs e e',
    update_env ecallee e marrs = Some e' ->
    forall i v v',
      e!i = Some v ->
      e'!i = Some v' ->
      unchanged i [] marrs v v'.
Proof.
  move=> ecallee marrs e e' Hupde i v v'.
  move: marrs e e' v v' Hupde.
  elim/list_ind => /= [|[[i' l'] i''] marrs IH] e e' v v'.
  - move=> [->] -> [<-]. by apply: unchanged_eq.
  - case: ecallee!i'' => [v''|] //=.
    case He_i': e!i' => //=. case Hupdv: update_value => [v1|] //=.
    move:He_i'. case: (Pos.eqb_spec i i') => [<-|Hneq].
    + move=> -> /IH + /[swap]. rewrite PTree.gss. move/(_ _ _ eq_refl)/[apply].
      move=> H [<-]. apply: (unchanged_trans _ _ v1).
      * apply: (update_value_unchanged _ _ _ _ i _ l' _ Hupdv). by left.
      * by apply: unchanged_cons.
    + move=> _. move/IH. rewrite PTree.gso. exact: Hneq.
      move/[apply]/[apply]. by apply: unchanged_cons.
Qed.

Fixpoint unchanged' (i: ident) (l: list sem_path_elem)
                    (marrs: list (sem_path * ident)) (v v': value)
                    {struct v} : Prop :=
  match v, v' with
  | Varr lv, Varr lv' =>
      if ~~ (mem sub_path (i, l) (map fst marrs)) then
        let f := fix f j lv lv' :=
            match lv, lv' with
            | [], [] => True
            | _, [] | [], _ => False
            | v :: lv, v' :: lv' =>
                unchanged' i (l ++ [Pcell j]) marrs v v' /\
                  f (S j) lv lv'
            end in
        f 0 lv lv'
      else True
  | _, _ => v = v'
  end.

Theorem unchanged_unchanged':
  forall marrs v v' i l,
    ~~ mem sub_path (i, l) (map fst marrs) ->
    (forall l', In (i, l ++ l') (map fst marrs) ->
           exists lv lv', get_value_path l' v  = Some (Varr lv) /\
                     get_value_path l' v' = Some (Varr lv')) ->
    unchanged i l marrs v v' ->
    unchanged' i l marrs v v'.
Proof.
  move=> marrs v v' i l Hsub.
  have: forall l', prefix sem_path_elem_beq l' l ->
              l <> l' ->
              ~ In (i, l') (map fst marrs).
  { move=> l' Hpref Hneq Hin. move/negP in Hsub.
    have Hsub': sub_path (i, l) (i, l') by rewrite /= Pos.eqb_refl Hpref.
    have: mem sub_path (i, l) (map fst marrs) => //.
    apply mem_spec. by exists (i, l'). }
  move: v v' l {Hsub}.
  elim/value_ind' => [|b|n|n|f|f|lv IH] /= v' l Hpref.
  all: case Hmem: mem => //=.
  1-7: move: Hmem => /mem_spec [[i' l'] []] /=.
  1-7: case: Pos.eqb_spec => //= ->.
  1-7: case: (list_reflect _ sem_path_elem_reflect l l') => [->|Hneq].
  all: try (move=> + _ => /[swap] /(_ []);
            rewrite app_nil_r => /[apply] - [? [? []]] // _ [->] //).
  all: try by move /Hpref/[apply]/(_ Hneq).
  case: v' => //= lv' HVarr.
  have:
    forall j l', In (i, (l ++ [Pcell (0 + j)]) ++ l') (map fst marrs) ->
            exists x x', get_value_path ([Pcell j] ++ l') (Varr lv) = Some (Varr x) /\
                    get_value_path ([Pcell j] ++ l') (Varr lv') = Some (Varr x').
  { move=> j l'. rewrite Nat.add_0_l -app_assoc. apply: HVarr. }
  move {HVarr}. move: lv lv' 0 IH. elim/list_ind => //= v lv IH [|v' lv'] // n.
  case/Forall_cons_iff => H Hlv HVarr [H1 H1']. apply: conj.
  - apply: H.
    + move=> l' Hpref' Hneq. have:= not_eq_sym Hneq => {Hneq}.
      move/(prefix_app_not_eq _ sem_path_elem_reflect _ _ _ Hpref') => Hpref''.
      case: (list_reflect _ sem_path_elem_reflect l l') => [<-|Hneq].
      * move=> Hin. move/negP in Hmem. apply: Hmem.
        apply mem_spec. exists (i, l). apply: conj => //=.
        by rewrite Pos.eqb_refl (prefix_refl _ sem_path_elem_reflect).
      * by apply: Hpref.
    + move=> l'. have:= HVarr 0 l'. rewrite Nat.add_0_r. eauto.
    + exact: H1.
  - apply: IH. exact: Hlv.
    move=> j. have:= HVarr (S j). by rewrite Nat.add_succ_comm.
    exact: H1'.
Qed.

Theorem unchanged'_unchanged:
  forall marrs v v' i l,
    unchanged' i l marrs v v' ->
    unchanged i l marrs v v'.
Proof.
  move=> marrs. elim/value_ind' => [|b|n|n|f|f|lv IH] /= v' i l.
  1-6: by case: mem.
  case: v' => //=. case: mem => //= lv'.
  move: lv lv' 0 IH. elim/list_ind => //= v lv IH [|v' lv'] //= n.
  case/Forall_cons_iff => H Hlv [H1 H1']. apply: conj.
  by apply: H. by apply: IH.
Qed.

Theorem unchanged'_Varr:
  forall marrs lv lv' i l,
    ~~ mem sub_path (i, l) (map fst marrs) ->
    unchanged' i l marrs (Varr lv) (Varr lv') ->
    length lv = length lv' /\
    forall j v v',
      nth_error lv  j = Some v ->
      nth_error lv' j = Some v' ->
      unchanged' i (l ++ [Pcell j]) marrs v v'.
Proof.
  move=> marrs lv lv' i l /=. move=> -> H. apply: conj.
  - move: lv lv' 0 H. elim/list_ind => [|> IH] [] // > [_] /IH /=. lia.
  - move=> j. rewrite -{3}(Nat.add_0_l j).
    move: lv lv' 0 l H j. elim/list_ind => [|v lv IH] [|v' lv'] // n l.
    + move=> _. by case.
    + case=> Hv /IH Hlv. case=> /=.
      move=>> [<-] [<-]. by rewrite Nat.add_0_r.
      move=> idx > H1 H2. have:= Hlv idx _ _ H1 H2. by rewrite Nat.add_succ_comm.
Qed.

Theorem update_env_spec_3':
  forall ecallee marrs e e' p lv,
    update_env ecallee e marrs = Some e' ->
    ~~ mem sub_path p (map fst marrs) ->
    get_env_path e' p = Some (Varr lv) ->
    exists lv', get_env_path e p = Some (Varr lv').
Proof.
  move=> ecallee. elim/list_ind => /=. move=>> [->]. eauto.
  move=> [[id pl] idcallee] l IH e e' [id' pl'] lv /=.
  move/[swap]. case/norP.
  case Hcallee: ecallee!idcallee => //=.
  case He_id: e!id => //=. case Hupdv: update_value => //=.
  case: (Pos.eqb_spec id id') => [<-|Hneq] /=.
  - move=> Hpref /IH/[apply]/[apply]. case=> lv' /=.
    rewrite PTree.gss /= He_id /=.
    have Hneq: pl <> pl'.
    { move=> H. rewrite H in Hpref.
      by rewrite (prefix_refl _ sem_path_elem_reflect) in Hpref. }
    apply: (update_value_get_value_path_Varr _ _ _ _ _ _ Hupdv Hneq Hpref).
  - move=> _. move=> /IH/[apply]/[apply]. case=> lv' /=.
    rewrite PTree.gso /=. lia. case: e!id' => //=. eauto.
Qed.

Theorem update_env_spec_3'bis:
  forall ecallee marrs e e' p lv,
    update_env ecallee e marrs = Some e' ->
    ~~ mem sub_path p (map fst marrs) ->
    get_env_path e p = Some (Varr lv) ->
    exists lv', get_env_path e' p = Some (Varr lv') /\
                length lv = length lv'.
Proof.
  move=> ecallee. elim/list_ind => /=. move=>> [->]. eauto.
  move=> [[id pl] idcallee] l IH e e' [id' pl'] lv /=.
  move/[swap]. case/norP.
  case Hcallee: ecallee!idcallee => //=.
  case He_id: e!id => //=. case Hupdv: update_value => //=.
  case Pos.eqb_spec => [<-|Hneq] /=.
  - move=> Hpref /IH/[apply] X E.
    have Hneq: pl <> pl'.
    { move=> H. rewrite H in Hpref.
      by rewrite (prefix_refl _ sem_path_elem_reflect) in Hpref. }
    rewrite He_id /= in E.
    have [> [E' ->]]:= update_value_get_value_path_Varr' _ _ _ _ _ _ Hupdv Hneq Hpref E.
    apply: X => /=; rewrite PTree.gss /=; eassumption.
  - move=> _ /IH/[apply] X E; apply: X.
    rewrite /= PTree.gso. lia. eassumption.
Qed.

Theorem update_env_spec_3'':
  forall ecallee marrs e e' i v,
    e!i = Some v ->
    update_env ecallee e marrs = Some e' ->
    exists v', e'!i = Some v'.
Proof.
  move=> ecallee. elim/list_ind => /=. move=>> + [<-]. eauto.
  move=> [[i l] idcallee] marrs IH e e' i' v.
  case Hcallee: ecallee!idcallee => //=.
  case He_i: e!i => //=. case Hupdv: update_value => [v'|] //=.
  case: (Pos.eqb_spec i' i) => [->|Hneq] He.
  - apply: (IH _ _ _ v'). by rewrite PTree.gss.
  - apply: (IH _ _ _ v). by rewrite PTree.gso.
Qed.

Lemma update_env_Some:
  forall marrs ecallee e,
    ~ pargs_params_aliasing (map fst marrs) marrs ->
    (forall p i, In (p, i) marrs ->
            (exists v, ecallee!i = Some v) /\ (exists v, get_env_path e p = Some v)) ->
    exists e', update_env ecallee e marrs = Some e'.
Proof.
  elim/list_ind => [/=|]. eauto.
  move=> [[i l] j] marrs IH ecallee e Halias H. cbn -[update_env_path].
  have [[v' ->] [v]] := H (i, l) j (or_introl _ eq_refl).
  move/get_env_path_update_env_path => /(_ v'). cbn -[update_env_path].
  move: Halias => /=. rewrite/pargs_params_aliasing /=.
  rewrite Pos.eqb_refl (ListUtils.prefix_refl _ sem_path_elem_reflect) /=.
  move/negP. case/norP => S /negP T.
  case=>> /[dup] He -> /=. apply: IH.
  - by apply: (no_pargs_params_aliasing_cons _ _ (i, l)).
  - move=> [i' l'] j' Hin. have [H1 [x Hx]] := H _ _ (or_intror _ Hin) => {H}.
    split => //. move/(in_map_pair_fst marrs): Hin.
    move/(mem_In _ _ _ _ S) => /=. move: He. case Pos.eqb_spec => [->|Hneq] /=.
    + move: Hx => /=. case e!i' => //= v0. case Hupdv: update_value => [v0'|] //=.
      move=> + [<-]. rewrite PTree.gss /=.
      move: l l' v0 v0' Hupdv {S T}. elim/list_ind => //=.
      move=> [idx] l IH [//|[idx'] l'] [] // lv >.
      case Hnth: nth_error => //=. case Hupdv: update_value => //= >.
      case Hrep: replace_nth => //= - [<-].
      case (PeanoNat.Nat.eqb_spec idx idx') => [<-|Hneq].
      * rewrite (internal_nat_dec_lb _ _ eq_refl) /= Hnth /=.
        rewrite (replace_nth_nth_error_same _ _ _ _ Hrep) /=.
        by apply: IH.
      * case T: internal_nat_beq => /=.
        by rewrite (internal_nat_dec_bl _ _ T) in Hneq. move {T}.
        case T: internal_nat_beq => /=.
        by rewrite (internal_nat_dec_bl _ _ T) in Hneq. move {T}.
        rewrite (replace_nth_nth_error_other _ _ _ _ Hrep) //. eauto.
    + case e!i => //= >. case update_value => //= > [<-].
      rewrite PTree.gso. by apply: not_eq_sym. eauto.
Qed.

Lemma build_index'_lt:
  forall lidx lsz n idx,
    Forall (fun x => x > 0) lsz ->
    build_index' lidx lsz n = Some idx ->
    n <= idx.
Proof.
  induction lidx; simpl; intros.
  + destruct lsz; revert H0; now intros [= <-].
  + destruct a; try discriminate.
    destruct lsz; try discriminate. inversion_clear H.
    specialize IHlidx with (1 := H2) (2 := H0).
    assert (n * n0 <= idx) by lia. nia.
Qed.

Lemma build_index_map:
  forall li lsz,
    length li = length lsz ->
    exists idx, build_index (map Vint64 li) lsz = Some idx.
Proof.
  unfold build_index. intro li. generalize 0%nat. induction li; simpl; intros.
  all: destruct lsz; try discriminate. eauto.
  simpl in H. apply eq_add_S in H. now apply IHli.
Qed.

Lemma natlist_of_Vint64_map:
  forall li, natlist_of_Vint64 (map (fun i => Vint64 i) li) = Some (map Z.to_nat (map Int64.unsigned li)).
Proof.
  induction li; simpl. eauto.
  destruct natlist_of_Vint64; simpl; try discriminate.
  injection IHli. now intros ->.
Qed.

Lemma natlist_of_Vint64_length:
  forall lv shape,
    natlist_of_Vint64 lv = Some shape ->
    length lv = length shape.
Proof.
  induction lv; simpl; intros *. now intros [= <-].
  destruct a; try discriminate. destruct natlist_of_Vint64; try discriminate.
  simpl. intros [= <-]. now rewrite (IHlv l).
Qed.

Definition build_size (lsz: list nat) : nat := fold_left Nat.mul lsz 1.

Lemma valid_index_product:
  forall lvidx lvsz shape idx,
    natlist_of_Vint64 lvsz = Some shape ->
    build_index lvidx shape = Some idx ->
    valid_index lvidx lvsz = true ->
    (idx < build_size shape)%nat.
Proof.
  intro lvidx. unfold build_index, build_size.
  specialize PeanoNat.Nat.lt_0_1.
  generalize 1%nat as sz0. generalize 0%nat as idx0.
  induction lvidx; simpl; intros * INIT * H IDX VALID.
  + destruct lvsz; try discriminate. revert H IDX; intros [= <-] [= <-]. auto.
  + repeat destruct_match_noeq VALID. destruct shape; try discriminate.
    apply andb_prop in VALID as [BOUND VALID]. simpl in H, IDX.
    destruct natlist_of_Vint64 eqn:H'; try discriminate.
    injection H; clear H; intros -> <-. simpl.
    apply IHlvidx with (2 := H') (3 := IDX) (4 := VALID).
    pose proof (Int64.unsigned_range i).
    set (x := Z.to_nat (Int64.unsigned i)) in *.
    set (t := Z.to_nat (Int64.unsigned i0)).
    apply PeanoNat.Nat.lt_le_trans with (m := (idx0 * t + t)%nat). lia.
    apply PeanoNat.Nat.le_trans with (m := ((idx0 + 1) * t)%nat). lia. nia.
Qed.

Fixpoint all_Vint64 (lv: list value) : Prop :=
  match lv with
  | [] => True
  | Vint64 n :: lv => all_Vint64 lv
  | _ :: lv => False
  end.

Lemma natlist_of_Vint64_all_Vint64:
  forall lv s,
    natlist_of_Vint64 lv = Some s ->
    all_Vint64 lv.
Proof.
  elim/list_ind => //=.
  move=> [] // n lv + >. case natlist_of_Vint64 =>> //=. by move/(_ _ eq_refl).
Qed.

Lemma build_index_all_Vint64:
  forall lvidx lsz idx,
    build_index lvidx lsz = Some idx ->
    all_Vint64 lvidx.
Proof.
  rewrite/build_index => lvidx lsz.
  elim/list_ind: lvidx lsz 0%nat => //=.
  move=> [] // n lvidx + [//|sz lsz] >. by move/[apply].
Qed.

Lemma all_Vint64_map:
  forall li, all_Vint64 (map (fun i => Vint64 i) li).
Proof. induction li; simpl; eauto. Qed.

Definition mut_array_content (te: tenv) (id: ident) : option typ :=
  match te!id with
  | Some (Tarr t) => Some t
  | _ => None
  end.

Lemma sem_cast_primitive_value:
  forall v t1 t2 v',
    sem_cast v t1 t2 = Some v' ->
    primitive_value v = primitive_value v'.
Proof.
  move=> v t1 t2; case: v =>> //=.
  all: case: t1 => [| |_ []|[]|[]|] //=.
  all: case: t2 => [| |? []|[]|[]|] //=.
  all: destruct_match_goal.
  all: try intros [= <-] => //.
  move=>>; case typ_beq => // - [<-] //.
Qed.

Lemma sem_unop_primitive_value:
  forall op k v v',
    sem_unop op k v = Some v' ->
    primitive_value v.
Proof. case=> - [] // [] // ? [] //. Qed.

Lemma sem_binop_arith_primitive_value:
  forall op k v1 v2 v',
    sem_binarith_operation op k v1 v2 = Some v' ->
    primitive_value v1 /\ primitive_value v2.
Proof. case=> - [] // [] // ? [] //. Qed.

Lemma sem_binop_cmp_primitive_value:
  forall op k v1 v2 v',
    sem_cmp_operation op k v1 v2 = Some v' ->
    primitive_value v1 /\ primitive_value v2.
Proof. case=> - [] // [] // ? [] //. Qed.

Lemma sem_binop_cmpu_primitive_value:
  forall op k v1 v2 v',
    sem_cmpu_operation op k v1 v2 = Some v' ->
    primitive_value v1 /\ primitive_value v2.
Proof. case=> - [] // [] // ? [] //. Qed.

Section SIZE_EXPR_ASSIGNMENTS.

Local Open Scope option_error_monad_scope.

Definition instantiate_size_vars (e: env) (f: function) : res env :=
  fold_left_res (fun e' i =>
    do* llsz  <- (fn_szenv f)!i;
    do* llsz' <- (fn_szenv' f)!i;
    fold_left2_res (fun e' lsz lsz' =>
        fold_left2_res (fun e' exp i =>
                    match eval_expr e f exp with
                    | Val (Vint64 n) => OK (PTree.set i (Vint64 n) e')
                    | _ => Error [MSG "Size expression is not evaluated to an u64."]
                    end) e' lsz lsz'
    ) e' llsz llsz'
  ) e (fn_params f).

(* Definition match_szenv (e: env) (f: function) :=
  forall i llsz llsz',
    (fn_szenv  f)!i = Some llsz ->
    (fn_szenv' f)!i = Some llsz' ->
    Forall2 (Forall2 (fun sz sz' =>
              exists n, eval_expr e f sz               = Val (Vint64 n) /\
                        eval_expr e f (Eacc (sz', [])) = Val (Vint64 n)
            )) llsz llsz'. *)

Definition instantiate_size_vars_spec1 f:
  forall e e',
    instantiate_size_vars e f = OK e' ->
    forall i llsz llsz' j lsz lsz' k sz sz',
      In i (fn_params f) ->
      (fn_szenv f)!i = Some llsz ->
      (fn_szenv' f)!i = Some llsz' ->
      nth_error llsz j = Some lsz ->
      nth_error llsz' j = Some lsz' ->
      nth_error lsz k = Some sz ->
      nth_error lsz' k = Some sz' ->
      exists n, eval_expr e f sz = Val (Vint64 n) /\
                e'!sz' = Some (Vint64 n).
Proof.
  rewrite/instantiate_size_vars.
  move=> e0 ++ i llsz llsz' j lsz lsz' k sz sz' T Hsze Hsze' Nllsz Nllsz' Nlsz Nlsz'.
  elim/list_ind: (fn_params f) T {2}e0 => //=.
  move=> p params IH + e e'.
  case Hllsz: (fn_szenv f)!p => [llsz1|] //=.
  case Hllsz': (fn_szenv' f)!p => [llsz1'|] //=.
  case X: (fold_left2_res _ e llsz1 llsz1') => [e1|] //=.
  case (Pos.eqb_spec p i) => [T|].
  2: { move=> NEQ [] // T. by apply: IH. }
  move=> _ U {IH}.
  have: exists n, eval_expr e0 f sz = Val (Vint64 n) /\
                  e1!sz' = Some (Vint64 n).
  { move: Hllsz Hllsz' X {U}. rewrite T Hsze Hsze' => - [<-] [<-].
    have:= fn_szenv_szenv'_inj f _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
      Hsze Hsze Hsze' Hsze' Nllsz _ Nllsz' _ Nlsz'.
    elim/list_ind: llsz llsz' j e e1 Nllsz Nllsz' {Hsze Hsze'} => /=.
    - case=> // - [] //.
    - move=> lsz1 llsz IH [//|lsz1' llsz'] j e e1 ++ H /=.
      case X: (fold_left2_res _ e lsz1 lsz1') => [e3|] //=. case: j => [|j] /=.
      2: { move/IH/[apply] => /(_ _ _ (fun x => H (S x))) IH'. apply: IH'. }
      move=> [T1] [T2] U. rewrite T1 T2 {T1 T2} in X H.
      have: exists n, eval_expr e0 f sz = Val (Vint64 n) /\
                      e3!sz' = Some (Vint64 n).
      { move: X {U}. have:= H 0 _ _ _ eq_refl eq_refl. rewrite Nlsz.
        elim/list_ind: lsz lsz' k e e3 Nlsz' {IH H Nlsz} => /=.
        + case=> // - [] //.
        + move=> sz1 lsz IH [//|sz1' lsz'] k e e3 + H.
          case E1: eval_expr => [[| | |n| | |]| |] //=. case: k => [|k] /=.
          2: { move/IH/[apply] => /(_ (fun x => H (S x))) IH'. apply: IH'. }
          case=> [U]. rewrite U in H. rewrite U {U}. have:= H 0 eq_refl => /=.
          case=> [U]. rewrite -U {U} in E1.
          have:= fun x => H (S x) => /=.
          have:= PTree.gss sz' (Vint64 n) e.
          elim/list_ind: lsz lsz' (PTree.set sz' _ _) e3 {IH H} => /=.
          * case=>> // + _ [<-]. eauto.
          * move=> sz2 lsz IH [//|sz2' lsz'] e5 e3 E H.
            case E2: eval_expr => [[]| |] //=.
            apply: IH. 2: exact (fun x => H (S x)).
            case: (Pos.eqb_spec sz' sz2') H => [<-|NEQ].
            ** move/(_ 0 eq_refl) => /=. rewrite PTree.gss. congruence.
            ** by rewrite PTree.gso. }
      case=> n [E1 E2]. move: U. have:= fun x => H (S x) => {H} /=.
      elim/list_ind: llsz llsz' e3 e1 E2 {X IH} => /=.
      + case=>> // + _ [<-] => ->.
      rewrite E1.  eauto.
      + move=> lsz2 llsz IH [//|lsz2' llsz'] e3 e1 E H.
        case X: (fold_left2_res _ e3 lsz2 lsz2') => [e4|] //=.
        apply: IH. 2: exact (fun x => H (S x)).
        move: X. have:= H 0 _ _ _ eq_refl eq_refl.
        elim/list_ind: lsz2 lsz2' e3 e4 E {H} => /=.
        * case=>> // + _ [<-] //.
        * move=> sz2 lsz2 IH [//|sz2' lsz2'] e3 e4 E H.
          case E2: eval_expr => [[]| |] //=.
          apply: IH. 2: exact (fun x => H (S x)).
          case: (Pos.eqb_spec sz' sz2') H => [<-|NEQ].
          ** move/(_ 0 eq_refl) => /=. rewrite PTree.gss. congruence.
          ** by rewrite PTree.gso. }
  case=> n [E1 E2].
  elim/list_ind: params e1 e' E2 U {X} => /=.
  - move=>> /[swap] - [<-] ->. eauto.
  - move=>> IH e1 e' E.
    case Hllsz1: (fn_szenv f)!_ => [llsz2|] //=.
    case Hllsz1': (fn_szenv' f)!_ => [llsz2'|] //=.
    case X: (fold_left2_res _ e1 _ _) => [e2|] //=.
    apply: IH. move: X.
    have:= fn_szenv_szenv'_inj f _ _ _ _ _ _ _ _ _ _ _ _ _ _ _
             Hsze Hllsz1 Hsze' Hllsz1' Nllsz _ Nllsz' _ Nlsz'.
    elim/list_ind: llsz2 llsz2' e1 e2 E {Hllsz1 Hllsz1'} => /=.
    + case=>> // + _ [<-] //.
    + move=> lsz2 llsz2 IH [//|lsz2' llsz2'] e1 e2 E H.
      case X: (fold_left2_res _ e1 lsz2 lsz2') => [e3|] //=.
      apply: IH. 2: exact (fun x => H (S x)). move: X.
      have:= H 0 _ _ _ eq_refl eq_refl.
      elim/list_ind: lsz2 lsz2' e1 e3 E {H} => /=.
      * case=>> // + _ [<-] //.
      * move=> sz2 lsz2 IH [//|sz2' lsz2'] e1 e3 E H.
        case E2: eval_expr => [[]| |] //=.
        apply: IH. 2: exact (fun x => H (S x)).
        case: (Pos.eqb_spec sz' sz2') H => [<-|NEQ].
        ** move/(_ 0 eq_refl) => /=. rewrite PTree.gss. congruence.
        ** by rewrite PTree.gso.
Qed.

Definition instantiate_size_vars_spec2 f:
  forall e e',
    instantiate_size_vars e f = OK e' ->
    forall i,
      (forall v, 
        e'!i = Some v ->
        e!i = Some v \/
        (exists i' j k llsz llsz' lsz lsz' sz,
            In i' (fn_params f) /\
            (fn_szenv f)!i' = Some llsz /\
            (fn_szenv' f)!i' = Some llsz' /\
            nth_error llsz j = Some lsz /\
            nth_error llsz' j = Some lsz' /\
            nth_error lsz k = Some sz /\
            nth_error lsz' k = Some i /\
            eval_expr e f sz = Some v)) /\
      (e'!i = None -> e!i = None).
Proof.
  move=> e0 e' H. pattern e0 at 1 3, e', (fn_params f). set G := (fun e0 e1 => _).
  move: H; rewrite/instantiate_size_vars.
  have:= skipn_O (fn_params f).
  elim/list_ind: {2 3 4}(fn_params f) 0%nat {2 3}e0 e' => /=.
  - move=>> _ [<-] >. split=> //. by left.
  - move=> p params IH n e e' Sparams.
    case Hllsz: (fn_szenv f)!p => [llsz|] //=.
    case Hllsz': (fn_szenv' f)!p => [llsz'|] //=.
    case X: (fold_left2_res _ e llsz llsz') => [e1|] //= U.
    have: G e e1 (p :: params).
    { move: Hllsz Hllsz' X. rewrite -{1}(app_nil_l llsz) -{1}(app_nil_l llsz').
      have: length ([]: list (list expr)) = length ([]: list (list ident)) by easy.
      elim/list_ind: llsz llsz' e e1 [] [] {U IH}.
      - case=> //= > _ _ _ [<-]. split=> //. by left.
      - move=> lsz llsz IH [//|lsz' llsz'] e e1 llsz0' llsz0 LEN Hllsz Hllsz' /=.
        case X: (fold_left2_res _ e lsz lsz') => [e2|] //= U.
        have H: G e e2 (p :: params).
        { move: Hllsz Hllsz' X. rewrite -{1}(app_nil_l lsz) -{1}(app_nil_l lsz').
          have: length ([]: list expr) = length ([]: list ident) by easy.
          elim/list_ind: lsz lsz' e e2 [] [] {U IH}.
          - case=> //= > _ _ _ [<-]. split=> //. by left.
          - move=> sz lsz IH [//|sz' lsz'] e e2 lsz0' lsz0 LEN' Hllsz Hllsz' /=.
            have:= Hllsz'. have:= Hllsz.
            change (?a :: ?b) with ([a] ++ b) at 2 4. rewrite !app_assoc.
            case Esz: eval_expr => [[]| |] //=. move/IH/[apply]/[apply].
            rewrite !app_length /= LEN' /G => /(_ eq_refl) => H i. split.
            + move=> v He. case (proj1 (H i) v He) => {He}.
              case (Pos.eqb_spec i sz') => [->|NEQ].
              * rewrite PTree.gss => - [<-]. right.
                exists p, (length llsz0), (length lsz0). repeat eexists.
                by left. exact Hllsz. exact Hllsz'.
                rewrite nth_error_app2 // Nat.sub_diag //.
                rewrite nth_error_app2; [lia|]; rewrite LEN Nat.sub_diag //.
                rewrite nth_error_app2 // Nat.sub_diag //.
                rewrite nth_error_app2; [lia|]; rewrite LEN' Nat.sub_diag //.
                exact Esz.
              * rewrite PTree.gso //. by left.
              * by right.
            + move/(proj2 (H i)). rewrite PTree.gsspec. by case Coqlib.peq. }
        move: Hllsz Hllsz' U. change (?a :: ?b) with ([a] ++ b). rewrite !app_assoc.
        move/IH/[apply]/[apply]. rewrite !app_length /= LEN => /(_ eq_refl) H' i; split.
        + move=> v He. case (proj1 (H' i) v He) => [He'|]; [|by right].
          case (proj1 (H i) v He'); [by left|by right].
        + by move/(proj2 (H' i))/(proj2 (H i)). }
    have:= skipn_S _ _ _ _ Sparams. move/IH: U => /[apply] H' H i; split.
    + move=> v He. case (proj1 (H' i) v He) => [He'|].
      2: { intros (? & ? & ? & ? & ? & ? & ? & ? & ? & ?). right.
           do 9 eexists. simpl; right; eassumption. eassumption. }
      case (proj1 (H i) v He'); [by left|by right].
    + by move/(proj2 (H' i))/(proj2 (H i)).
Qed.

Lemma instantiate_size_vars_incl f:
  forall args e e',
    build_func_env (PTree.empty value) (fn_params f) args = Some e ->
    instantiate_size_vars e f = OK e' ->
    PTreeaux.incl' e e'.
Proof.
  move=> args e e' /build_func_env_spec' H.
  move/instantiate_size_vars_spec2 => H' i v He.
  case He': e'!i => [v'|] //=.
  2: { move/(proj2 (H' _)) in He'. by rewrite He' in He. }
  case/(proj1 (H' _)): He' => {H'}.
  - by rewrite He.
  - intros (? & ? & ? & ? & ? & ? & ? & ? & _ & _ & Hsze & _ & N1 & _ & N2 & _).
    have:= fn_szenv'_params f _ _ Hsze.
    have N1' := nth_error_In _ _ N1. have N2' := nth_error_In _ _ N2.
    move/Forall_forall => /(_ _ N1'). move/Forall_forall => /(_ _ N2').
    have [//|[n [H1 H2]]] := H _ _ He => {H He}.
    by have:= nth_error_In _ _ H1.
Qed.

End SIZE_EXPR_ASSIGNMENTS.

Section EXTERNAL_FUNCTIONS.

Definition extcall_sem : Type :=
  list (sem_path * value) -> list value * value.

Parameter external_functions_sem: String.string -> signature -> penv -> extcall_sem.

Definition external_call (ef: external_function) : extcall_sem :=
  external_functions_sem (ef_name ef) (ef_sig ef) (ef_penv ef).

Record extcall_properties (ef: external_function) (sem: extcall_sem) : Prop :=
  mk_extcall_properties {
      (* The return value of an external call must agree with its signature. *)
      ec_well_typed_res: forall vargs vmarrs r,
        sem vargs = (vmarrs, r) ->
        well_typed_value (sig_res (ef_sig ef)) r;
      (* The return value of an external call must be in the bounds defined by
         the return type. *)
      (* ec_shrunk_res: forall vargs vmarrs r,
        sem vargs = (vmarrs, r) ->
        r = shrink (sig_res (ef_sig ef)) r; *)
      (* External calls cannot free mutable arguments. *)
      ec_mut_args_preserved: forall vargs vmarrs r pp marrs,
        sem vargs = (vmarrs, r) ->
        pargs_params (ef_params ef) (map fst vargs) = Some pp ->
        mut_args (ef_penv ef) pp = marrs ->
        length vmarrs = length marrs;
      (* External calls can only modify arrays *)
      ec_only_arrays_modified: forall vargs vmarrs r,
        sem vargs = (vmarrs, r) ->
        Forall (fun v => match v with Varr _ => True | _ => False end) vmarrs;
      (* External calls must preserve types of the values in memory. *)
      ec_well_typed_mut_args: forall vargs vmarrs r pp marrs,
        sem vargs = (vmarrs, r) ->
        pargs_params (ef_params ef) (map fst vargs) = Some pp ->
        mut_args (ef_penv ef) pp = marrs ->
        Forall2 (fun '(p, i) v =>
                   well_typed_value (nth (Pos.to_nat i - 1) (sig_args (ef_sig ef)) Tvoid)
                                    v) marrs vmarrs
      }.

Axiom external_functions_properties:
  forall ef, extcall_properties ef (external_call ef).

End EXTERNAL_FUNCTIONS.