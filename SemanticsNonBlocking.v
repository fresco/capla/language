Require Import ssreflect ssrbool ssrfun.
Require Import Coq.Lists.List.
Require Import BinNums BinInt Lia.
Require Import Bool.

Import ListNotations.

Require Import Types Ops BValues Syntax Typing BEnv Validity.
Require Import SemPath Alias SemanticsCommon ExprSem.
Require Import Tactics BUtils PTreeaux.

Require Import Integers Floats Maps Smallstep.
Require Import Errors Globalenvs.
Require AST Events.
Module CAST := AST.

Section SEMANTICS_NON_BLOCKING.

Variable ge: genv.

Section EXPR.

Variable e: env.
Variable f: function.

Inductive eval_identlist: list ident -> list value -> Prop :=
| eval_ident_Enil: eval_identlist [] []
| eval_ident_Econs: forall id v lids lv,
  e!id = Some v ->
  f.(fn_szenv')!id = Some [] ->
  eval_identlist lids lv ->
  eval_identlist (id :: lids) (v :: lv).

Inductive eval_expr: expr -> option value -> Prop :=
| eval_Const_int: forall sz sig n,
  eval_expr (Econst (Cint sz sig n)) (Some (Vint n))
| eval_Const_int64: forall sig n,
  eval_expr (Econst (Cint64 sig n)) (Some (Vint64 n))
| eval_Const_float32: forall f,
  eval_expr (Econst (Cfloat32 f)) (Some (Vfloat32 f))
| eval_Const_float64: forall f,
  eval_expr (Econst (Cfloat64 f)) (Some (Vfloat64 f))
| eval_Const_bool: forall b,
  eval_expr (Econst (Cbool b)) (Some (Vbool b))
| eval_Cast: forall exp t1 t2 v1 v,
  eval_expr exp (Some v1) ->
  sem_cast v1 t1 t2 = Some v ->
  eval_expr (Ecast exp t1 t2) (Some v)
| eval_Cast_ERR: forall exp t1 t2 v1,
  eval_expr exp (Some v1) ->
  well_typed_value t1 v1 ->
  cast_allowed t1 t2 ->
  sem_cast v1 t1 t2 = None ->
  eval_expr (Ecast exp t1 t2) None
| eval_Cast_ERR_val: forall exp t1 t2,
  eval_expr exp None ->
  eval_expr (Ecast exp t1 t2) None
| eval_Unop: forall op k e1 v1 v,
  eval_expr e1 (Some v1) ->
  sem_unop op k v1 = Some v ->
  eval_expr (Eunop op k e1) (Some v)
| eval_Unop_ERR: forall op k e1,
  eval_expr e1 None ->
  eval_expr (Eunop op k e1) None
| eval_Binop_arith: forall op k e1 e2 v1 v2 v,
  eval_expr e1 (Some v1) ->
  eval_expr e2 (Some v2) ->
  sem_binarith_operation op k v1 v2 = Some v ->
  eval_expr (Ebinop_arith op k e1 e2) (Some v)
| eval_Binop_arith_ERR_val1: forall op k e1 e2,
  eval_expr e1 None ->
  eval_expr (Ebinop_arith op k e1 e2) None
| eval_Binop_arith_ERR_val2: forall op k e1 e2 v,
  eval_expr e1 (Some v) ->
  eval_expr e2 None ->
  eval_expr (Ebinop_arith op k e1 e2) None
| eval_Binop_arith_ERR: forall op k e1 e2 v1 v2,
  eval_expr e1 (Some v1) ->
  eval_expr e2 (Some v2) ->
  binarith_operation_allowed op k v1 v2 ->
  sem_binarith_operation op k v1 v2 = None ->
  eval_expr (Ebinop_arith op k e1 e2) None
| eval_Binop_cmp: forall op k e1 e2 v1 v2 v,
  eval_expr e1 (Some v1) ->
  eval_expr e2 (Some v2) ->
  sem_cmp_operation op k v1 v2 = Some v ->
  eval_expr (Ebinop_cmp op k e1 e2) (Some v)
| eval_Binop_cmp_ERR_val1: forall op k e1 e2,
  eval_expr e1 None ->
  eval_expr (Ebinop_cmp op k e1 e2) None
| eval_Binop_cmp_ERR_val2: forall op k e1 e2 v,
  eval_expr e1 (Some v) ->
  eval_expr e2 None ->
  eval_expr (Ebinop_cmp op k e1 e2) None
| eval_Binop_cmpu: forall op k e1 e2 v1 v2 v,
  eval_expr e1 (Some v1) ->
  eval_expr e2 (Some v2) ->
  sem_cmpu_operation op k v1 v2 = Some v ->
  eval_expr (Ebinop_cmpu op k e1 e2) (Some v)
| eval_Binop_cmpu_ERR_val1: forall op k e1 e2,
  eval_expr e1 None ->
  eval_expr (Ebinop_cmpu op k e1 e2) None
| eval_Binop_cmpu_ERR_val2: forall op k e1 e2 v,
  eval_expr e1 (Some v) ->
  eval_expr e2 None ->
  eval_expr (Ebinop_cmpu op k e1 e2) None
| eval_Access: forall p p' v,
  eval_path p (Some p') ->
  get_env_path e p' = Some v ->
  primitive_value v = true ->
  eval_expr (Eacc p) (Some v)
| eval_Access_ERR: forall p,
  eval_path p None ->
  eval_expr (Eacc p) None

with eval_path_elem_list: list syn_path_elem ->
                          list (list ident) ->
                          option (list sem_path_elem) -> Prop :=
| eval_path_elem_list_Nil: forall llsz,
  eval_path_elem_list [] llsz (Some [])
| eval_Scell: forall lidx synpl lsz llsz lvsz shape lvidx idx sempl,
  eval_identlist lsz lvsz ->
  natlist_of_Vint64 lvsz = Some shape ->
  eval_exprlist lidx (Some lvidx) ->
  build_index lvidx shape = Some idx ->
  valid_index lvidx lvsz = true ->
  eval_path_elem_list synpl llsz (Some sempl) ->
  eval_path_elem_list (Scell lidx :: synpl) (lsz :: llsz) (Some (Pcell idx :: sempl))
| eval_Scell_ERR: forall lidx synpl lsz llsz lvsz shape lvidx idx,
  eval_identlist lsz lvsz ->
  natlist_of_Vint64 lvsz = Some shape ->
  eval_exprlist lidx (Some lvidx) ->
  build_index lvidx shape = Some idx ->
  valid_index lvidx lvsz = false ->
  eval_path_elem_list (Scell lidx :: synpl) (lsz :: llsz) None
| eval_Scell_ERR_idx: forall lidx synpl lsz llsz lvsz shape,
  eval_identlist lsz lvsz ->
  natlist_of_Vint64 lvsz = Some shape ->
  eval_exprlist lidx None ->
  eval_path_elem_list (Scell lidx :: synpl) (lsz :: llsz) None
| eval_Scell_ERR_tl: forall lidx synpl lsz llsz lvsz shape lvidx idx,
  eval_identlist lsz lvsz ->
  natlist_of_Vint64 lvsz = Some shape ->
  eval_exprlist lidx (Some lvidx) ->
  build_index lvidx shape = Some idx ->
  valid_index lvidx lvsz = true ->
  eval_path_elem_list synpl llsz None ->
  eval_path_elem_list (Scell lidx :: synpl) (lsz :: llsz) None


with eval_path: syn_path -> option sem_path -> Prop :=
| eval_path_intro: forall id synpl llsz sempl,
  f.(fn_szenv')!id = Some llsz ->
  eval_path_elem_list synpl llsz (Some sempl) ->
  eval_path (id, synpl) (Some (id, sempl))
| eval_path_ERR: forall id synpl llsz,
  f.(fn_szenv')!id = Some llsz ->
  eval_path_elem_list synpl llsz None ->
  eval_path (id, synpl) None

with eval_full_path: syn_path -> option sem_path -> Prop :=
| eval_full_path_intro: forall id synpl llsz sempl,
  f.(fn_szenv')!id = Some llsz ->
  length synpl = length llsz ->
  eval_path_elem_list synpl llsz (Some sempl) ->
  eval_full_path (id, synpl) (Some (id, sempl))
| eval_full_path_ERR: forall id synpl llsz,
  f.(fn_szenv')!id = Some llsz ->
  length synpl = length llsz ->
  eval_path_elem_list synpl llsz None ->
  eval_full_path (id, synpl) None

with eval_exprlist: list expr -> option (list value) -> Prop :=
| eval_Enil: eval_exprlist [] (Some [])
| eval_Econs_Some_e: forall e v le lv,
  eval_expr e (Some v) ->
  eval_exprlist le (Some lv) ->
  eval_exprlist (e :: le) (Some (v :: lv))
| eval_Econs_None_e: forall e le,
  eval_expr e None ->
  eval_exprlist (e :: le) None
| eval_Econs_None: forall e v le,
  eval_expr e (Some v) ->
  eval_exprlist le None ->
  eval_exprlist (e :: le) None.

Lemma eval_path_elem_list_length:
  forall synpl llsz sempl,
    eval_path_elem_list synpl llsz (Some sempl) ->
    length synpl = length sempl.
Proof. elim=> [|> IH] > H; inv H => //=; by rewrite (IH llsz sempl). Qed.

Lemma eval_full_path_eval_path:
  forall synp semp,
    eval_full_path synp semp ->
    eval_path synp semp.
Proof. intros * H. inversion_clear H; econstructor; eassumption. Qed.

Lemma eval_identlist_nth_error:
  forall lids lv,
    eval_identlist lids lv ->
    forall n id,
      nth_error lids n = Some id ->
      exists v, nth_error lv n = Some v /\ e!id = Some v.
Proof.
  induction lids; simpl; intros. now destruct n.
  inversion_clear H. destruct n.
  + revert H0; intros [= <-]. eexists. split; simpl; eauto.
  + simpl; eauto.
Qed.

Lemma eval_exprlist_nth_error:
  forall lexp lv,
    eval_exprlist lexp (Some lv) ->
    forall n exp,
      nth_error lexp n = Some exp ->
      exists v, nth_error lv n = Some v /\ eval_expr exp (Some v).
Proof.
  induction lexp; simpl; intros. now destruct n.
  inversion_clear H. destruct n.
  + revert H0; intros [= <-]. eexists. split; simpl; eauto.
  + simpl; eauto.
Qed.

Lemma eval_exprlist_length:
  forall le lv,
    eval_exprlist le (Some lv) -> length le = length lv.
Proof.
  induction le, lv.
  + easy.
  + intro. inversion H.
  + intro. inversion H.
  + intro. inversion_clear H. simpl; apply eq_S. apply (IHle _ H1).
Qed.

Lemma eval_exprlist_app:
  forall le1 le2 lv1 lv2,
    eval_exprlist le1 (Some lv1) ->
    eval_exprlist le2 (Some lv2) ->
    eval_exprlist (le1 ++ le2) (Some (lv1 ++ lv2)).
Proof.
  intros. revert lv1 H. induction le1.
  + intros. inversion_clear H. apply H0.
  + intros. inversion_clear H. simpl.
    specialize (IHle1 _ H2). apply eval_Econs_Some_e; assumption.
Qed.

Lemma eval_exprlist_app_inv:
  forall le1 le2 lv1 lv2,
    length le1 = length lv1 ->
    eval_exprlist (le1 ++ le2) (Some (lv1 ++ lv2)) ->
    eval_exprlist le1 (Some lv1) /\ eval_exprlist le2 (Some lv2).
Proof.
  intros *. revert lv1. induction le1.
  + intros. simpl in H. apply eq_sym, length_zero_iff_nil in H as ->.
    split. exact eval_Enil. exact H0.
  + intros. destruct lv1. discriminate. simpl in H, H0.
    inversion_clear H0. apply eq_add_S in H.
    specialize (IHle1 _ H H2) as [Hle1 Hle2].
    split. apply eval_Econs_Some_e; assumption. assumption.
Qed.

Lemma eval_exprlist_rev:
  forall le lv,
    eval_exprlist le (Some lv) ->
    eval_exprlist (rev le) (Some (rev lv)).
Proof.
  induction le.
  + intros * H. inversion_clear H. exact eval_Enil.
  + intros * H. inversion_clear H. simpl.
    apply eval_exprlist_app. auto.
    apply eval_Econs_Some_e. assumption. exact eval_Enil.
Qed.

Lemma eval_path_fst:
  forall p p',
    eval_path p (Some p') ->
    fst p = fst p'.
Proof. intros * H. now inv H. Qed.

Inductive eval_path_list: list syn_path -> option (list sem_path) -> Prop :=
| eval_path_list_Nil: eval_path_list [] (Some [])
| eval_path_list_Cons: forall p lp p' lp',
    eval_path p (Some p') ->
    eval_path_list lp (Some lp') ->
    eval_path_list (p :: lp) (Some (p' :: lp'))
| eval_path_list_Cons_None_e: forall p lp,
    eval_path p None ->
    eval_path_list (p :: lp) None
| eval_path_list_Cons_None: forall p p' lp,
    eval_path p (Some p') ->
    eval_path_list lp None ->
    eval_path_list (p :: lp) None.

Lemma eval_path_list_map_fst:
  forall lp lp',
    eval_path_list lp (Some lp') ->
    map fst lp = map fst lp'.
Proof.
  induction lp; simpl; intros * H; inv H. reflexivity.
  now rewrite (eval_path_fst _ _ H3) (IHlp _ H4).
Qed.

Lemma eval_path_list_length:
  forall args pargs,
    eval_path_list args (Some pargs) ->
    length args = length pargs.
Proof.
  elim/list_ind.
  - move=>> T. by inv T.
  - move=>> IH > T. inv T => /=. by rewrite (IH _ H3).
Qed.

Lemma eval_path_list_None_cut:
  forall lp,
    eval_path_list lp None ->
    exists lp1 p lp2, lp = lp1 ++ p :: lp2 /\
                 (exists lp1', eval_path_list lp1 (Some lp1')) /\
                 eval_path p None.
Proof.
  elim/list_ind.
  - move=> T. inv T.
  - move=> p lp IH T. inv T.
    + exists [], p, lp. repeat split => //. exists []. econstructor.
    + case (IH H2) => lp1 [x [lp2 [-> [[lp1']]]]] *. rewrite app_comm_cons.
      exists (p :: lp1), x, lp2. repeat split => //.
      exists (p' :: lp1'). by econstructor.
Qed.

Lemma eval_path_list_In:
  forall lp lp' p,
    eval_path_list lp (Some lp') ->
    In p lp ->
    exists p', In p' lp' /\ eval_path p (Some p').
Proof.
  elim/list_ind => // q lp IH > T. inv T => /=. case=> [<-|].
  - exact (ex_intro _ _ (conj (or_introl _ eq_refl) H2)).
  - case/(IH _ _ H3) =>> [] Hin H.
    exact (ex_intro _ _ (conj (or_intror _ Hin) H)).
Qed.

Lemma eval_path_list_In':
  forall lp lp' p',
    eval_path_list lp (Some lp') ->
    In p' lp' ->
    exists p, In p lp /\ eval_path p (Some p').
Proof.
  elim/list_ind.
  - move=>> T; by inv T.
  - move=> q lp IH > T; inv T => /=. case=> [<-|].
    + exact (ex_intro _ _ (conj (or_introl _ eq_refl) H2)).
    + case/(IH _ _ H3) =>> [] Hin H.
      exact (ex_intro _ _ (conj (or_intror _ Hin) H)).
Qed.

Lemma eval_path_list_nth_error:
  forall lp lp' p n,
    eval_path_list lp (Some lp') ->
    nth_error lp n = Some p ->
    exists p', nth_error lp' n = Some p' /\ eval_path p (Some p').
Proof.
  elim/list_ind.
  - by move=> ++ [].
  - move=>> IH ?? [] > T; inv T => /=.
    + case=> <-. exact (ex_intro _ _ (conj eq_refl H2)).
    + case/(IH _ _ _ H3) =>> H. exact (ex_intro _ _ H).
Qed.

Lemma eval_path_list_nth_error':
  forall lp lp' p' n,
    eval_path_list lp (Some lp') ->
    nth_error lp' n = Some p' ->
    exists p, nth_error lp n = Some p /\ eval_path p (Some p').
Proof.
  elim/list_ind.
  - move=> ?? n T; inv T. by case n.
  - move=>> IH ?? [] > T; inv T => /=.
    + case=> <-. exact (ex_intro _ _ (conj eq_refl H2)).
    + case/(IH _ _ _ H3) =>> H. exact (ex_intro _ _ H).
Qed.

Lemma eval_path_list_app:
  forall lp1 lp1' lp2 lp2',
    eval_path_list lp1 (Some lp1') ->
    eval_path_list lp2 (Some lp2') ->
    eval_path_list (lp1 ++ lp2) (Some (lp1' ++ lp2')).
Proof.
  elim/list_ind.
  - move=>> T; inv T => //.
  - move=>> IH > T; inv T => /=; econstructor => //. by apply: IH.
Qed.

End EXPR.

Derive Inversion inv_eval_identlist
  with (forall e f le lv, eval_identlist e f le lv) Sort Prop.
Derive Inversion inv_eval_expr
  with (forall e f exp v, eval_expr e f exp v) Sort Prop.
Derive Inversion inv_eval_exprlist
  with (forall e f le lv, eval_exprlist e f le lv) Sort Prop.

Section STATEMENT.

Inductive cont : Type :=
| Kstop: cont
| Kseq: stmt -> cont -> cont
| Kblock: cont -> cont
| Kreturnto: option ident ->
             env ->
             function ->
             list (sem_path * ident) ->
             cont -> cont.

Inductive state : Type :=
| State
    (e: env)
    (f: function)
    (s: stmt)
    (k: cont) : state
| Callstate
    (fd:   function)
    (args: list value)
    (k:    cont) : state
| Returnstate
    (e:   env)
    (f:   function)
    (res: value)
    (k:   cont) : state.

Fixpoint destructCont (k: cont) : cont :=
  match k with
  | Kseq _ k | Kblock k => destructCont k
  | _ => k
  end.

Definition is_Kreturnto (k: cont) :=
  match k with
  | Kreturnto _ _ _ _ _ => True
  | _ => False
  end.

Definition is_Kreturnto_or_Kstop k :=
  match k with
  | Kstop | Kreturnto _ _ _ _ _ => true
  | _ => false
  end.

Definition match_lsz e f lsz lsz' :=
  Forall2 (fun sz sz' =>
            exists n, eval_expr e f sz               (Some (Vint64 n)) /\
                      eval_expr e f (Eacc (sz', [])) (Some (Vint64 n))) lsz lsz'.

Definition match_llsz e f llsz llsz' :=
  Forall2 (match_lsz e f) llsz llsz'.

(* Definition match_szenv (e: env) (f: function) :=
  forall i llsz llsz',
    (fn_szenv  f)!i = Some llsz ->
    (fn_szenv' f)!i = Some llsz' ->
    match_llsz e f llsz llsz'. *)

Definition valid_env_szenv (e: env) (sze: szenvi) (lids: list ident) :=
  forall id llsz,
    In id lids ->
    sze!id = Some llsz ->
    Forall (Forall (fun sz => exists n, e!sz = Some (Vint64 n))) llsz.

Inductive step_stmt: state -> state -> Prop :=
(* Sskip *)
| step_skip: forall e f s k,
  step_stmt (State e f Sskip (Kseq s k)) (State e f s k)
| step_skip_block: forall e f k,
  step_stmt (State e f Sskip (Kblock k)) (State e f Sskip k)
| step_skip_returnto: forall e f k,
  is_Kreturnto k ->
  well_typed_value f.(fn_sig).(sig_res) Vundef ->
  step_stmt (State e f Sskip k)
            (Returnstate e f Vundef k)
(* Sassign *)
| step_assign: forall e f k p p' t exp v e',
  eval_full_path e f p (Some p') ->
  get_tenv_path f.(fn_tenv) p' = Some t ->
  eval_expr e f exp (Some v) ->
  primitive_value v = true ->
  option_map (perm_le Mutable) (fn_penv f)!(fst p) = Some true ->
  well_typed_value t v ->
  update_env_path' e p' (shrink t v) = Some e' ->
  step_stmt (State e  f (Sassign p exp) k)
            (State e' f Sskip k)
| step_assign_ERR: forall e f k p exp v,
  eval_full_path e f p None ->
  eval_expr e f exp (Some v) ->
  primitive_value v = true ->
  option_map (perm_le Mutable) (fn_penv f)!(fst p) = Some true ->
  step_stmt (State e f (Sassign p exp) k)
            (State e f Serror k)
| step_assign_ERR_val: forall e f k p exp,
  eval_expr e f exp None ->
  step_stmt (State e f (Sassign p exp) k)
            (State e f Serror k)
(* Salloc *)
| step_alloc: forall e f k i t sz isz v n,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  default_value t = Some v ->
  eval_expr e f sz (Some (Vint64 n)) ->
  Int64.unsigned n * typ_sizeof t <= Int64.max_unsigned ->
  step_stmt
    (State e f (Salloc i) k)
    (State (PTree.set i (Varr (repeat v (Z.to_nat (Int64.unsigned n))))
            (PTree.set isz (Vint64 n) e))
           f Sskip k)
| step_alloc_ERR_sz: forall e f k i sz,
  (fn_szenv f)!i = Some [[sz]] ->
  eval_expr e f sz None ->
  step_stmt (State e f (Salloc i) k) (State e f Serror k)
| step_alloc_ERR_overflow: forall e f k i t sz isz n,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  eval_expr e f sz (Some (Vint64 n)) ->
  Int64.unsigned n * typ_sizeof t > Int64.max_unsigned ->
  step_stmt (State e f (Salloc i) k) (State e f Serror k)
(* Sfree *)
| step_free: forall e f k i t sz isz lv,
  (fn_tenv f)!i = Some (Tarr t) ->
  (fn_szenv f)!i = Some [[sz]] ->
  (fn_szenv' f)!i = Some [[isz]] ->
  (fn_penv f)!i = Some Owned ->
  e!i = Some (Varr lv) ->
  step_stmt
    (State e f (Sfree i) k)
    (State (PTree.remove i (PTree.remove isz e)) f Sskip k)
(* Scall *)
| step_call_Internal: forall e f k idvar idf args pargs vargs pp mut shr own f' lvtests,
  ge!idf = Some (Internal f') ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  enough_permission (fn_penv f) (fn_penv f') pp = true ->
  pargs_params (fn_params f') pargs = Some pp ->
  mut_args (fn_penv f') pp = mut ->
  shr_args (fn_penv f') pp = shr ->
  own_args (fn_penv f') pp = own ->
  valid_call_tests_values f (Internal f') e args = Val lvtests ->
  equal_tests lvtests = true ->
  length args = length f'.(fn_sig).(sig_args) ->
  match_types (fn_tenv f) (fn_tenv f') pp = true ->
  well_typed_valuelist f'.(fn_sig).(sig_args) vargs ->
  all_root_paths own = true ->
  pargs_params_aliasing pargs (mut ++ own) = false ->
  step_stmt (State e f (Scall idvar idf args) k)
            (Callstate f' vargs
                       (Kreturnto idvar (remove_own_params own e) f mut k))
| step_call_External: forall e f k idvar idf ef args pargs vargs pp mut shr own vmarrs r e' lvtests,
  ge!idf = Some (External ef) ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  pargs_params (ef_params ef) pargs = Some pp ->
  mut_args (ef_penv ef) pp = mut ->
  shr_args (ef_penv ef) pp = shr ->
  own_args (ef_penv ef) pp = own ->
  valid_call_tests_values f (External ef) e args = Val lvtests ->
  equal_tests lvtests = true ->
  length args = length (ef_sig ef).(sig_args) ->
  well_typed_valuelist (ef_sig ef).(sig_args) vargs ->
  all_root_paths own = true ->
  pargs_params_aliasing pargs (mut ++ own) = false ->
  external_call ef (combine pargs vargs) = (vmarrs, r) ->
  (idvar <> None -> primitive_value r = true) ->
  update_env (build_env PTree.Empty (combine (map snd mut) vmarrs))
             (remove_own_params own e) mut = Some e' ->
  step_stmt (State e f (Scall idvar idf args) k)
            (State (set_optenv e' idvar r) f Sskip k)
| step_call_ERR_args: forall e f k idvar idf fd args,
  ge!idf = Some fd ->
  eval_path_list e f args None ->
  step_stmt (State e f (Scall idvar idf args) k)
            (State e f Serror k)
| step_call_ERR_tests: forall e f k idvar idf fd args pargs vargs pp marrs oarrs,
  ge!idf = Some fd ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  pargs_params (fd_params fd) pargs = Some pp ->
  mut_args (fd_penv fd) pp = marrs ->
  own_args (fd_penv fd) pp = oarrs ->
  valid_call_tests_values f fd e args = Err ->
  valid_env_szenv e (fn_szenv' f) (map fst args) ->
  length args = length (fd_sig fd).(sig_args) ->
  well_typed_valuelist (fd_sig fd).(sig_args) vargs ->
  pargs_params_aliasing pargs (marrs ++ oarrs) = false ->
  step_stmt (State e f (Scall idvar idf args) k)
            (State e f Serror k)
| step_call_ERR_invalid: forall e f k idvar idf fd args pargs vargs pp marrs oarrs lvtests,
  ge!idf = Some fd ->
  eval_path_list e f args (Some pargs) ->
  get_env_path_list e pargs = Some vargs ->
  pargs_params (fd_params fd) pargs = Some pp ->
  mut_args (fd_penv fd) pp = marrs ->
  own_args (fd_penv fd) pp = oarrs ->
  valid_call_tests_values f fd e args = Val lvtests ->
  equal_tests lvtests = false ->
  length args = length (fd_sig fd).(sig_args) ->
  well_typed_valuelist (fd_sig fd).(sig_args) vargs ->
  pargs_params_aliasing pargs (marrs ++ oarrs) = false ->
  step_stmt (State e f (Scall idvar idf args) k)
            (State e f Serror k)
(* Callstate *)
| step_callstate_Internal: forall f vargs k e e',
  build_func_env (PTree.empty value) f.(fn_params) vargs = Some e ->
  instantiate_size_vars e f = OK e' ->
  step_stmt (Callstate f vargs k)(State e' f f.(fn_body) k)
(* Sreturn *)
| step_return_None: forall e f k,
  well_typed_value f.(fn_sig).(sig_res) Vundef ->
  step_stmt (State e f (Sreturn None) k)
            (Returnstate e f Vundef k)
| step_return_Some: forall e f k exp v,
  eval_expr e f exp (Some v) ->
  well_typed_value f.(fn_sig).(sig_res) v ->
  primitive_value v = true ->
  step_stmt (State e f (Sreturn (Some exp)) k)
            (Returnstate e f v k)
| step_return_ERR: forall e f k exp,
  eval_expr e f exp None ->
  step_stmt (State e f (Sreturn (Some exp)) k)
            (State e f Serror k)
(* Returnstate *)
| step_returnstate_seq: forall e f v s k,
  step_stmt (Returnstate e f v (Kseq s k)) (Returnstate e f v k)
| step_returnstate_block: forall e f v k,
  step_stmt (Returnstate e f v (Kblock k)) (Returnstate e f v k)
| step_returnstate: forall ecallee callee v optid e f marrs k e',
  marrs_varr e ecallee marrs = true ->
  match_types (fn_tenv f) (fn_tenv callee) marrs = true ->
  update_env ecallee e marrs = Some e' ->
  primitive_value v = true ->
  step_stmt (Returnstate ecallee callee v (Kreturnto optid e f marrs k))
            (State (set_optenv e' optid v) f Sskip k)
(* Sseq *)
| step_seq: forall e f k s1 s2,
  step_stmt (State e f (Sseq s1 s2) k)
            (State e f s1 (Kseq s2 k))
(* Sassert *)
| step_assert: forall e f k cond b,
  eval_expr e f cond (Some (Vbool b)) ->
  step_stmt (State e f (Sassert cond) k)
            (State e f (if b then Sskip else Serror) k)
| step_assert_ERR: forall e f k cond,
  eval_expr e f cond None ->
  step_stmt (State e f (Sassert cond) k)
            (State e f Serror k)
(* Sifthenelse *)
| step_ifthenelse: forall e f k cond b s1 s2,
  eval_expr e f cond (Some (Vbool b)) ->
  step_stmt (State e f (Sifthenelse cond s1 s2) k)
            (State e f (if b then s1 else s2) k)
| step_ifthenelse_ERR: forall e f k cond s1 s2,
  eval_expr e f cond None ->
  step_stmt (State e f (Sifthenelse cond s1 s2) k)
            (State e f Serror k)
(* Sloop *)
| step_loop: forall e f s k,
  step_stmt (State e f (Sloop s) k)
            (State e f s (Kseq (Sloop s) k))
(* Sblock *)
| step_block: forall e f s k,
  step_stmt (State e f (Sblock s) k) (State e f s (Kblock k))
(* Sexit *)
| step_exit_block_seq: forall e f n s k,
  step_stmt (State e f (Sexit n) (Kseq s k)) (State e f (Sexit n) k)
| step_exit_block_skip: forall e f n k,
  step_stmt (State e f (Sexit (S n)) (Kblock k)) (State e f (Sexit n) k)
| step_exit_block_stop: forall e f k,
  step_stmt (State e f (Sexit O) (Kblock k)) (State e f Sskip k)
(* Serror *)
| step_error: forall e f k,
  step_stmt (State e f Serror k) (State e f Serror k).

End STATEMENT.

Inductive initial_state (p: program) : state -> Prop :=
| initial_state_intro: forall f,
  (genv_of_program p) ! (prog_main p) = Some (Internal f) ->
  f.(fn_sig) = {| sig_args := []; sig_res := Tint I32 Signed |} ->
  initial_state p (Callstate f [] Kstop).

Inductive final_state : state -> int -> Prop :=
| final_state_intro: forall e f i,
  final_state (Returnstate e f (Vint i) Kstop) i.

End SEMANTICS_NON_BLOCKING.

Section COMPCERT_SEMANTICS.

Definition to_AST_globdef (l: list (ident * fundef)) : list (ident * CAST.globdef fundef typ):=
  map (fun x => (fst x, CAST.Gfun (snd x))) l.

Definition to_genv (prog_defs: list (ident * fundef)) : Genv.t fundef typ :=
  Genv.add_globals (Genv.empty_genv fundef typ (map fst prog_defs)) (to_AST_globdef prog_defs).

Definition step_event ge s t s' := t = Events.E0 /\ step_stmt ge s s'.

Lemma step_stmt_step_event:
  forall ge st1 st2,
    step_stmt ge st1 st2 ->
    step_event ge st1 Events.E0 st2.
Proof. intros. split; easy. Qed.

Definition semantics p :=
  Semantics_gen step_event (initial_state p) final_state (genv_of_program p) (to_genv p.(prog_defs)).

End COMPCERT_SEMANTICS.

Section USEFUL_LEMMAS.

Lemma eval_identlist_length_preservation:
  forall e f le lv,
    eval_identlist e f le lv -> length le = length lv.
Proof.
  induction le, lv; try easy.
  intro. inversion_clear H. simpl; apply eq_S. apply (IHle _ H2).
Qed.

Lemma Vbool_Int64_ltu_true:
  forall i1 i2,
    Int64.unsigned i1 < Int64.unsigned i2 ->
    Some (Vbool (Int64.ltu i1 i2)) = Some Vtrue.
Proof. intros. unfold Int64.ltu. now rewrite Coqlib.zlt_true. Qed.

Lemma Vbool_Int64_ltu_false:
  forall i1 i2,
    ~ Int64.unsigned i1 < Int64.unsigned i2 ->
    Some (Vbool (Int64.ltu i1 i2)) = Some Vfalse.
Proof. intros. unfold Int64.ltu. now rewrite Coqlib.zlt_false. Qed.

Lemma Vbool_Int64_eq_true:
  forall i1 i2,
    Int64.unsigned i1 = Int64.unsigned i2 ->
    Some (Vbool (Int64.eq i1 i2)) = Some Vtrue.
Proof. intros. unfold Int64.eq. now rewrite -> H, Coqlib.zeq_true. Qed.

Lemma Vbool_Int64_eq_false:
  forall i1 i2,
    Int64.unsigned i1 <> Int64.unsigned i2 ->
    Some (Vbool (Int64.eq i1 i2)) = Some Vfalse.
Proof. intros. unfold Int64.eq. now rewrite Coqlib.zeq_false. Qed.

End USEFUL_LEMMAS.

Create HintDb semanticsnb.
Global Hint Constructors eval_expr eval_exprlist eval_identlist : semanticsnb.
Global Hint Constructors eval_path eval_full_path eval_path_elem_list : semanticsnb.
Global Hint Constructors eval_path_list : semanticsnb.
Global Hint Resolve eval_identlist_length_preservation : semanticsnb.
Global Hint Resolve eval_path_list_length : semanticsnb.
Global Hint Resolve eq_refl : semanticsnb.
Global Hint Constructors step_stmt : semanticsnb.
Global Hint Resolve eval_exprlist_app : semanticsnb.
Global Hint Unfold sem_cmpu_operation : semanticsnb.
Global Hint Resolve step_stmt_step_event : semanticsnb.

Global Hint Resolve Vbool_Int64_ltu_true Vbool_Int64_ltu_false : semanticsnb.
Global Hint Resolve Vbool_Int64_eq_true : semanticsnb.
Global Hint Resolve Vbool_Int64_eq_false : semanticsnb.
