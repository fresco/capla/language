Require Import ssreflect ssrbool ssrfun.

Require Import BinNums BinInt String List.

Require Import Integers Floats Maps Coqlib.
Require Import Errors.

Import ListNotations.

Require Import Types Ops BValues.
Require Import ListUtils BUtils Tactics SemPath.

Inductive permission := Shared | Mutable | Owned.

Definition perm_le (p q: permission) :=
  match p, q with
  | Shared, _ => true
  | Mutable, Mutable | Mutable, Owned => true
  | Owned, Owned => true
  | _, _ => false
  end.

Lemma perm_le_trans:
  forall p q r, perm_le p q = true -> perm_le q r = true -> perm_le p r = true.
Proof. now intros [] [] []. Qed.

Lemma perm_le_refl:
  forall p, perm_le p p = true.
Proof. now intros []. Qed.

Lemma perm_le_ge:
  forall p p', perm_le p p' = true -> perm_le p' p = true -> p = p'.
Proof. now intros [] []. Qed.

Notation "A '<=&' B" := (perm_le A B) (at level 70, no associativity).

Scheme Equality for permission.

Lemma perm_reflect:
  forall p p', reflect (p = p') (permission_beq p p').
Proof. intros [] []; now (apply ReflectT || apply ReflectF). Qed.

Inductive constant : Type :=
| Cbool: bool -> constant
| Cint: intsize -> signedness -> int -> constant
| Cint64: signedness -> int64 -> constant
| Cfloat32: float32 -> constant
| Cfloat64: float -> constant.

Lemma int_reflect: forall x y, reflect (x = y) (Int.eq x y).
Proof.
  intros. case (Int.eq_dec x y) as [<-|Hneq].
  rewrite Int.eq_true. now apply ReflectT.
  rewrite -> Int.eq_false with (1 := Hneq). now apply ReflectF.
Qed.

Lemma int64_reflect: forall x y, reflect (x = y) (Int64.eq x y).
Proof.
  intros. case (Int64.eq_dec x y) as [<-|Hneq].
  rewrite Int64.eq_true. now apply ReflectT.
  rewrite -> Int64.eq_false with (1 := Hneq). now apply ReflectF.
Qed.

Definition float32_beq x y :=
  match Float32.eq_dec x y with
  | left _ => true
  | right _ => false
  end.

Lemma float32_reflect: forall x y, reflect (x = y) (float32_beq x y).
Proof.
  intros. unfold float32_beq. case (Float32.eq_dec x y).
  apply ReflectT. apply ReflectF.
Qed.

Definition float64_beq x y :=
  match Float.eq_dec x y with
  | left _ => true
  | right _ => false
  end.

Lemma float64_reflect: forall x y, reflect (x = y) (float64_beq x y).
Proof.
  intros. unfold float64_beq. case (Float.eq_dec x y).
  apply ReflectT. apply ReflectF.
Qed.

Definition constant_beq (c1 c2: constant) :=
  match c1, c2 with
  | Cbool b1, Cbool b2 => eqb b1 b2
  | Cint sz1 sg1 n1, Cint sz2 sg2 n2 =>
      intsize_beq sz1 sz2 && signedness_beq sg1 sg2 && Int.eq n1 n2
  | Cint64 sg1 n1, Cint64 sg2 n2 =>
      signedness_beq sg1 sg2 && Int64.eq n1 n2
  | Cfloat32 f1, Cfloat32 f2 => float32_beq f1 f2
  | Cfloat64 f1, Cfloat64 f2 => float64_beq f1 f2
  | _, _ => false
  end.

Lemma constant_reflect: forall c1 c2, reflect (c1 = c2) (constant_beq c1 c2).
Proof.
  case c1, c2; simpl; try now apply ReflectF.
  - case b, b0; simpl; now (apply ReflectF || apply ReflectT).
  - case i, i1, s, s0; case (int_reflect i0 i2) as [<-|Hneq]; simpl.
    all: try (now apply ReflectF || now apply ReflectT).
    all: apply ReflectF; now injection.
  - case s, s0; case (int64_reflect i i0) as [<-|Hneq]; simpl.
    all: try (now apply ReflectF || now apply ReflectT).
    all: apply ReflectF; now injection.
  - case (float32_reflect f f0) as [<-|Hneq].
    now apply ReflectT. apply ReflectF; now injection.
  - case (float64_reflect f f0) as [<-|Hneq].
    now apply ReflectT. apply ReflectF; now injection.
Qed.

Inductive expr : Type :=
| Eacc: (ident * list syn_path_elem) -> expr
| Econst: constant -> expr
| Ecast: expr -> typ -> typ -> expr
| Eunop: unary_operation -> operation_kind -> expr -> expr
| Ebinop_arith: binarith_operation -> operation_kind -> expr -> expr -> expr
| Ebinop_cmp: cmp_operation -> operation_kind -> expr -> expr -> expr
| Ebinop_cmpu: cmpu_operation -> operation_kind -> expr -> expr -> expr

with syn_path_elem :=
| Scell: list expr -> syn_path_elem.

Fixpoint expr_beq (e1 e2: expr) :=
  match e1, e2 with
  | Eacc (id1, l1), Eacc (id2, l2) =>
      ident_eq id1 id2 && list_beq _ syn_path_elem_beq l1 l2
  | Econst c1, Econst c2 => constant_beq c1 c2
  | Ecast e1 t1 t1', Ecast e2 t2 t2' => expr_beq e1 e2 && typ_beq t1 t2 && typ_beq t1' t2'
  | Eunop op1 k1 e1, Eunop op2 k2 e2 =>
      unary_operation_beq op1 op2 && operation_kind_beq k1 k2 && expr_beq e1 e2
  | Ebinop_arith op1 k1 e11 e12, Ebinop_arith op2 k2 e21 e22 =>
      binarith_operation_beq op1 op2 && operation_kind_beq k1 k2 &&
      expr_beq e11 e21 && expr_beq e12 e22
  | Ebinop_cmp op1 k1 e11 e12, Ebinop_cmp op2 k2 e21 e22 =>
      cmp_operation_beq op1 op2 && operation_kind_beq k1 k2 &&
      expr_beq e11 e21 && expr_beq e12 e22
  | Ebinop_cmpu op1 k1 e11 e12, Ebinop_cmpu op2 k2 e21 e22 =>
      cmpu_operation_beq op1 op2 && operation_kind_beq k1 k2 &&
      expr_beq e11 e21 && expr_beq e12 e22
  | _, _ => false
  end
with syn_path_elem_beq (e1 e2: syn_path_elem) :=
  match e1, e2 with
  | Scell l1, Scell l2 => list_beq _ expr_beq l1 l2
  end.

Theorem expr_reflect (e1 e2: expr):
  reflect (e1 = e2) (expr_beq e1 e2).
Proof.
  revert e1 e2. fix IH 1. case e1, e2; simpl.
  all: try case p as [id1 l1]; try now apply ReflectF.
  - case p0 as [id2 l2]. unfold ident_eq.
    case (Pos.eqb_spec id1 id2) as [<-|Hneq]. 2: apply ReflectF; congruence.
    simpl. revert l1 l2. induction l1; intros []; try easy; simpl.
    now apply ReflectT. now apply ReflectF. now apply ReflectF.
    assert (reflect (a = s) (syn_path_elem_beq a s)) as Has.
    { clear l l1 IHl1. case a, s. revert l l0. simpl.
      induction l; intros []; simpl.
      now apply ReflectT. now apply ReflectF. now apply ReflectF.
      case (IH a e) as [<-|Hneq].
      case (IHl l0) as [[= <-]|]. now apply ReflectT.
      all: apply ReflectF; congruence. }
    case Has as [<-|Hneq].
    case (IHl1 l) as [[= <-]|Hneq]. now apply ReflectT.
    all: apply ReflectF; congruence.
  - case (constant_reflect c c0) as [<-|Hneq].
    now apply ReflectT. apply ReflectF; congruence.
  - case (IH e1 e2) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (typ_reflect t t1) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (typ_reflect t0 t2) as [<-|Hneq]. 2: apply ReflectF; congruence.
    now apply ReflectT.
  - case (unary_operation_reflect u u0) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (operation_kind_reflect o o0) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (IH e1 e2) as [<-|Hneq]. 2: apply ReflectF; congruence.
    now apply ReflectT.
  - case (binarith_operation_reflect b b0) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (operation_kind_reflect o o0) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (IH e1_1 e2_1) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (IH e1_2 e2_2) as [<-|Hneq]. 2: apply ReflectF; congruence.
    now apply ReflectT.
  - case (cmp_operation_reflect c c0) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (operation_kind_reflect o o0) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (IH e1_1 e2_1) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (IH e1_2 e2_2) as [<-|Hneq]. 2: apply ReflectF; congruence.
    now apply ReflectT.
  - case (cmpu_operation_reflect c c0) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (operation_kind_reflect o o0) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (IH e1_1 e2_1) as [<-|Hneq]. 2: apply ReflectF; congruence.
    case (IH e1_2 e2_2) as [<-|Hneq]. 2: apply ReflectF; congruence.
    now apply ReflectT.
Qed.

Theorem syn_path_elem_reflect (e1 e2: syn_path_elem):
  reflect (e1 = e2) (syn_path_elem_beq e1 e2).
Proof.
  case e1, e2. simpl. case (list_reflect _ expr_reflect l l0) as [<-|Hneq].
  now apply ReflectT. apply ReflectF. congruence.
Qed.

Definition syn_path : Type := ident * list syn_path_elem.

Definition Evar i := Eacc (i, []).

Lemma expr_eq_dec (x y: expr) : {x = y} + {x <> y}.
Proof. exact (reflect_dec _ _ (expr_reflect x y)). Qed.

Inductive stmt : Type :=
| Sskip: stmt
| Salloc: ident -> stmt
| Sfree: ident -> stmt
| Sassign: syn_path -> expr -> stmt
| Scall: option ident -> ident -> list syn_path -> stmt
| Sreturn: option expr -> stmt
| Sseq: stmt -> stmt -> stmt
| Sassert: expr -> stmt
| Sblock: stmt -> stmt
| Sifthenelse: expr -> stmt -> stmt -> stmt
| Sloop: stmt -> stmt
| Sexit: nat -> stmt
| Serror.

Definition Swhile (cond: expr) (s: stmt) :=
  Sblock (Sloop (Sblock (Sifthenelse cond s (Sexit 1)))).

Local Open Scope error_monad_scope.

Definition Sfor_step (i ihigh istep: ident) (low high step: expr)
                     (decr: bool) (t: typ) (s: stmt) :=
  do r <- match t with
          | Tint _ Signed   => OK (OInt,
                                   Ebinop_cmp  (if decr then Ogt  else Olt ) OInt)
          | Tint _ Unsigned => OK (OInt,
                                   Ebinop_cmpu (if decr then Ogtu else Oltu) OInt)
          | Tint64 Signed   => OK (OInt64,
                                   Ebinop_cmp  (if decr then Ogt  else Olt ) OInt64)
          | Tint64 Unsigned => OK (OInt64,
                                   Ebinop_cmpu (if decr then Ogtu else Oltu) OInt64)
          | _ => Error [MSG "Invalid type for variable in for."]
          end;
  let (k, f) := r in
  let Sassign i := Sassign (i, []) in
  OK (Sseq (Sassign i low)
       (Sseq (Sassign ihigh high)
             (Sseq (Sassign istep step)
                (Swhile
                   (f (Evar i) (Evar ihigh))
                   (Sseq s (Sassign i (Ebinop_arith (if decr then Osub else Oadd)
                                         k (Evar i) (Evar istep)))))))).

Definition Sfor (i ihigh: ident) (low high: expr)
                (decr: bool) (t: typ) (s: stmt) :=
  do r <- match t with
          | Tint _ Signed   => OK (OInt,
                                   Cint I32 Signed Int.one,
                                   Ebinop_cmp  (if decr then Ogt  else Olt ) OInt)
          | Tint _ Unsigned => OK (OInt,
                                   Cint I32 Unsigned Int.one,
                                   Ebinop_cmpu (if decr then Ogtu else Oltu) OInt)
          | Tint64 Signed   => OK (OInt64,
                                   Cint64 Signed Int64.one,
                                   Ebinop_cmp  (if decr then Ogt  else Olt ) OInt64)
          | Tint64 Unsigned => OK (OInt64,
                                   Cint64 Unsigned Int64.one,
                                   Ebinop_cmpu (if decr then Ogtu else Oltu) OInt64)
          | _ => Error [MSG "Invalid type for variable in for."]
          end;
  let '(k, c, f) := r in
  let Sassign i := Sassign (i, []) in
  OK (Sseq (Sassign i low)
       (Sseq (Sassign ihigh high)
         (Swhile
           (f (Evar i) (Evar ihigh))
           (Sseq s (Sassign i (Ebinop_arith (if decr then Osub else Oadd)
                                            k (Evar i) (Econst c))))))).

Definition penv   := PTree.t permission.
Definition szenvi := PTree.t (list (list ident)).   (* array size variables environment *)
Definition szenve := PTree.t (list (list expr)).

Definition valid_tenv_sig (te: tenv) (params: list ident) (sig_args: list typ) :=
  length params = length sig_args /\
  forall i var,
    nth_error params i = Some var ->
    nth_error sig_args i = te!var.

Definition valid_tenv_vars (te: tenv) (vars: list ident) :=
  forall i,
    In i vars <->
    exists t, te!i = Some t.

Definition same_domain {A B: Type} (t1: PTree.t A) (t2: PTree.t B) :=
  forall id,
    In id (map fst (PTree.elements t1))
    <-> In id (map fst (PTree.elements t2)).

(* size variables are unsigned 64-bits integers *)
Definition valid_tenv_sztype (te: tenv) (sze: szenvi) :=
  forall id llsz,
    sze!id = Some llsz ->
    Forall (Forall (fun sz => te!sz = Some Tuint64)) llsz.

Fixpoint forall_vars (f: ident -> bool) (e: expr) :=
  match e with
  | Econst _ => true
  | Eacc (y, l) =>
    f y && forallb (fun x => forall_vars_path_elem f x) l
  | Ecast e _ _ | Eunop _ _ e => forall_vars f e
  | Ebinop_arith _ _ e1 e2 | Ebinop_cmp _ _ e1 e2 | Ebinop_cmpu _ _ e1 e2 =>
    forall_vars f e1 && forall_vars f e2
  end
with forall_vars_path_elem f (e: syn_path_elem) :=
  match e with
  | Scell le => forallb (fun e => forall_vars f e) le
  end.

(* size variables are shared, i.e. not writable *)
Definition valid_penv_szenvi (pe: penv) (sze: szenvi) :=
  forall id llsz,
    sze!id = Some llsz ->
    Forall (Forall (fun sz => pe!sz = Some Shared)) llsz.

Definition valid_szenve_params (sze: szenve) (params: list ident) :=
  forall id llsz,
    sze!id = Some llsz ->
    In id params ->
    Forall (Forall (fun sz => forall_vars (fun x => in_dec Pos.eq_dec x params) sz)) llsz.

(* size variable have a empty size liste *)
Definition valid_szenvi_szvars (sze: szenvi) :=
  forall id llsz,
    sze!id = Some llsz ->
    Forall (Forall (fun sz => match sze!sz with
                              | Some [] => True
                              | _       => False
                              end)) llsz.

(* variables in size expressions have a empty size liste *)
Definition valid_szenve_szexprs (sze: szenve) :=
  forall id llsz,
    sze!id = Some llsz ->
    Forall (Forall (fun sz => forall_vars (fun x => match sze!x with
                                                    | Some [] => true
                                                    | _       => false
                                                    end) sz)) llsz.

Definition same_structure (sze: szenve) (sze': szenvi) :=
  forall i llsz llsz',
    sze!i = Some llsz ->
    sze'!i = Some llsz' ->
    Forall2 (fun lsz lsz' => length lsz = length lsz') llsz llsz'.

Definition valid_szenvi_params (sze: szenvi) (params: list ident) :=
  forall i llsz,
    sze!i = Some llsz ->
    Forall (Forall (fun sz => ~ In sz params)) llsz.

Fixpoint wf_size_expr exp :=
  match exp with
  | Eacc (i, []) => true
  | Eacc _ => false
  | Econst _ => true
  | Ecast e _ _ | Eunop _ _ e => wf_size_expr e
  | Ebinop_arith _ _ e1 e2 | Ebinop_cmp _ _ e1 e2 | Ebinop_cmpu _ _ e1 e2 =>
    wf_size_expr e1 && wf_size_expr e2
  end.

Fixpoint wf_size_expr' isz exp :=
  match exp with
  | Eacc (i, []) => forall_vars (fun i => negb (Pos.eqb i isz)) exp
  | Eacc _ => false
  | Econst _ => true
  | Ecast e _ _ | Eunop _ _ e => wf_size_expr' isz e
  | Ebinop_arith _ _ e1 e2 | Ebinop_cmp _ _ e1 e2 | Ebinop_cmpu _ _ e1 e2 =>
    wf_size_expr' isz e1 && wf_size_expr' isz e2
  end.

Definition wf_szenv (sze: szenve) :=
  forall i llsz,
    sze!i = Some llsz ->
    forallb (forallb wf_size_expr) llsz.

Definition wf_szenv' (sze: szenve) (sze': szenvi) :=
  forall i llsz llsz',
    sze!i = Some llsz ->
    sze'!i = Some llsz' ->
    forallb2 (forallb2 wf_size_expr') llsz' llsz.

Fixpoint match_type_llsz (t: typ) (llsz: list (list expr)) :=
  match t, llsz with
  | Tarr t, _ :: llsz => match_type_llsz t llsz
  | Tarr _, [] => false
  | _, [] => true
  | _, _ => false
  end.

Definition valid_szenve_tenv_str (sze: szenve) (te: tenv) :=
  forall i t llsz,
    te!i = Some t ->
    sze!i = Some llsz ->
    match_type_llsz t llsz.

Definition valid_szenv_szenv'_injective (sze: szenve) (sze': szenvi) :=
  forall i i' llsz1 llsz2 llsz1' llsz2' j j' lsz1 lsz2 lsz1' lsz2' k k' x,
    sze!i = Some llsz1 ->
    sze!i' = Some llsz2 ->
    sze'!i = Some llsz1' ->
    sze'!i' = Some llsz2' ->
    nth_error llsz1 j = Some lsz1 ->
    nth_error llsz2 j' = Some lsz2 ->
    nth_error llsz1' j = Some lsz1' ->
    nth_error llsz2' j' = Some lsz2' ->
    nth_error lsz1' k = Some x ->
    nth_error lsz2' k' = Some x ->
    nth_error lsz1 k = nth_error lsz2 k'.

Definition owned_szvars_separated (sze: szenvi) (pe: penv) :=
  forall i i' llsz1 llsz2 lsz1 lsz2 x x',
    pe!i = Some Owned ->
    i <> i' ->
    sze!i = Some llsz1 ->
    sze!i' = Some llsz2 ->
    In lsz1 llsz1 ->
    In x lsz1 ->
    In lsz2 llsz2 ->
    In x' lsz2 ->
    x <> x'.

Record function : Type := mk_function {
    fn_sig: signature;
    fn_params: list ident;
    fn_vars: list ident;
    fn_body: stmt;
    fn_tenv: tenv;
    fn_szenv: szenve;
    fn_szenv': szenvi;
    fn_penv: penv;
    fn_nodup_params: Coqlib.list_norepet fn_params;
    fn_disjoint: Coqlib.list_disjoint fn_params fn_vars;
    fn_res_primitive: primitive_type (sig_res fn_sig);
    fn_tenv_sig: valid_tenv_sig fn_tenv fn_params fn_sig.(sig_args);
    fn_tenv_sztype: valid_tenv_sztype fn_tenv fn_szenv';
    fn_tenv_vars: valid_tenv_vars fn_tenv (fn_params ++ fn_vars);
    fn_szenv_params: valid_szenve_params fn_szenv fn_params;
    fn_szenv'_params: valid_szenvi_params fn_szenv' fn_params;
    fn_szenv_wf: wf_szenv' fn_szenv fn_szenv';
    fn_szenv_tenv: same_domain fn_szenv fn_tenv;
    fn_szenv_tenv_str: valid_szenve_tenv_str fn_szenv fn_tenv;
    fn_szenv_szenv': same_domain fn_szenv fn_szenv';
    fn_szenv_szenv'_str: same_structure fn_szenv fn_szenv';
    fn_szenv_szenv'_inj: valid_szenv_szenv'_injective fn_szenv fn_szenv';
    fn_szenv_szexprs: valid_szenve_szexprs fn_szenv;
    fn_szenv'_szvars: valid_szenvi_szvars fn_szenv';
    fn_szenv'_owned: owned_szvars_separated fn_szenv' fn_penv;
    fn_penv_tenv: same_domain fn_penv fn_tenv;
    fn_penv_szenv': valid_penv_szenvi fn_penv fn_szenv';
  }.

Record external_function : Type := mk_ef {
  ef_name: string;
  ef_sig:  signature;
  ef_params: list ident;
  ef_tenv: tenv;
  ef_szenv: szenve;
  ef_penv: penv;
  ef_nodup_params: Coqlib.list_norepet ef_params;
  ef_res_primitive: primitive_type (sig_res ef_sig);
  ef_tenv_sig: valid_tenv_sig ef_tenv ef_params ef_sig.(sig_args);
  ef_tenv_vars: valid_tenv_vars ef_tenv ef_params;
  ef_szenv_params: valid_szenve_params ef_szenv ef_params;
  ef_szenv_wf: wf_szenv ef_szenv;
  ef_szenv_tenv: same_domain ef_szenv ef_tenv;
  ef_szenv_tenv_str: valid_szenve_tenv_str ef_szenv ef_tenv;
  ef_szenv_szexprs: valid_szenve_szexprs ef_szenv;
  ef_penv_tenv: same_domain ef_penv ef_tenv;
}.

Inductive fundef : Type :=
| Internal: function -> fundef
| External: external_function -> fundef.

Record program : Type :=
  mk_program {
      prog_defs: list (ident * fundef);
      prog_main: ident;
      prog_nodup: Coqlib.list_norepet (map fst prog_defs);
    }.

Definition fundef_nb_params (fd: fundef): nat :=
  match fd with
  | Internal f => length f.(fn_params)
  | External ef => length ef.(ef_params)
  end.

Definition fd_common_field {A: Type}
                           (ef_f: external_function -> A) (fn_f: function -> A)
                           (fd: fundef) : A :=
  match fd with
  | Internal f  => fn_f f
  | External ef => ef_f ef
  end.

Definition fd_sig := fd_common_field ef_sig fn_sig.
Definition fd_params := fd_common_field ef_params fn_params.
Definition fd_tenv := fd_common_field ef_tenv fn_tenv.
Definition fd_penv := fd_common_field ef_penv fn_penv.
Definition fd_szenv := fd_common_field ef_szenv fn_szenv.

Corollary fd_nodup_params:
  forall fd, Coqlib.list_norepet (fd_params fd).
Proof. case=>>. apply fn_nodup_params. apply ef_nodup_params. Qed.

Corollary fd_res_primitive:
  forall fd, primitive_type (sig_res (fd_sig fd)).
Proof. case=>>. apply fn_res_primitive. apply ef_res_primitive. Qed.

Corollary fd_tenv_sig:
  forall fd, valid_tenv_sig (fd_tenv fd) (fd_params fd) (sig_args (fd_sig fd)).
Proof. case=>>. apply fn_tenv_sig. apply ef_tenv_sig. Qed.

Corollary fd_tenv_vars:
  forall fd, valid_tenv_vars (fd_tenv fd) (match fd with
                                           | Internal f => fn_params f ++ fn_vars f
                                           | External f => ef_params f
                                           end).
Proof. case=>>. apply fn_tenv_vars. apply ef_tenv_vars. Qed.

Corollary fd_szenv_params:
  forall fd, valid_szenve_params (fd_szenv fd) (fd_params fd).
Proof. case=>>. apply fn_szenv_params. apply ef_szenv_params. Qed.

Corollary fd_szenv_tenv:
  forall fd, same_domain (fd_szenv fd) (fd_tenv fd).
Proof. case=>>. apply fn_szenv_tenv. apply ef_szenv_tenv. Qed.

Corollary fd_szenv_szexprs:
  forall fd, valid_szenve_szexprs (fd_szenv fd).
Proof. case=>>. apply fn_szenv_szexprs. apply ef_szenv_szexprs. Qed.

Corollary fd_penv_tenv:
  forall fd, same_domain (fd_penv fd) (fd_tenv fd).
Proof. case=>>. apply fn_penv_tenv. apply ef_penv_tenv. Qed.

Lemma wf_size_expr'_wf_size_expr:
  forall i e,
    wf_size_expr' i e -> wf_size_expr e.
Proof.
  move=> i. elim/expr_ind => //.
  - case=> ? [] //.
  - move=>> IH1 > IH2 /=. case/andP => /IH1 -> /IH2 -> //.
  - move=>> IH1 > IH2 /=. case/andP => /IH1 -> /IH2 -> //.
  - move=>> IH1 > IH2 /=. case/andP => /IH1 -> /IH2 -> //.
Qed.


Lemma fd_tenv_fd_szenv f i:
  (exists t, (fd_tenv f)!i = Some t) <-> (exists llsz, (fd_szenv f)!i = Some llsz).
Proof.
  have:= fd_szenv_tenv f => /(_ i) H. split.
  - case=>> /(@PTree.elements_correct typ)/(@in_map_pair_fst ident typ)/H.
    case/(@in_map_fst_pair ident (list (list expr))) => llsz.
    move/(@PTree.elements_complete (list (list expr))). by exists llsz.
  - case=>> /(@PTree.elements_correct (list (list expr))).
    move/(@in_map_pair_fst ident (list (list expr)))/H.
    case/(@in_map_fst_pair ident typ) => t.
    move/(@PTree.elements_complete typ). by exists t.
Qed.

Lemma fn_tenv_fn_szenv f i:
  (exists t, (fn_tenv f)!i = Some t) <-> (exists llsz, (fn_szenv f)!i = Some llsz).
Proof. exact: (fd_tenv_fd_szenv (Internal f)). Qed.

Lemma fn_szenv_fn_szenv' f i:
  (exists l, (fn_szenv f)!i = Some l) <-> (exists l, (fn_szenv' f)!i = Some l).
Proof.
  have:= fn_szenv_szenv' f => /(_ i) H. split.
  - case=>> /(@PTree.elements_correct (list (list expr))).
    move/(@in_map_pair_fst ident (list (list expr)))/H.
    case/(@in_map_fst_pair ident (list (list ident))) => l.
    move/(@PTree.elements_complete (list (list ident))). by exists l.
  - case=>> /(@PTree.elements_correct (list (list ident))).
    move/(@in_map_pair_fst ident (list (list ident)))/H.
    case/(@in_map_fst_pair ident (list (list expr))) => l.
    move/(@PTree.elements_complete (list (list expr))). by exists l.
Qed.

Corollary fn_tenv_fn_szenv' f i:
  (exists t, (fn_tenv f)!i = Some t) <-> (exists l, (fn_szenv' f)!i = Some l).
Proof.
  eapply iff_trans.
  exact (fn_tenv_fn_szenv f i). exact (fn_szenv_fn_szenv' f i).
Qed.

Corollary ef_tenv_ef_szenv ef i:
  (exists t, (ef_tenv ef)!i = Some t) <-> (exists l, (ef_szenv ef)!i = Some l).
Proof.
  split; case=>>.
  - move/(PTree.elements_correct (ef_tenv ef)).
    move/(@in_map_pair_fst _ _ _ _ _)/(ef_szenv_tenv ef).
    case/(@in_map_fst_pair _ _ _ _) =>>.
    move/(PTree.elements_complete (ef_szenv ef)); eauto.
  - move/(PTree.elements_correct (ef_szenv ef)).
    move/(@in_map_pair_fst _ _ _ _ _)/(ef_szenv_tenv ef).
    case/(@in_map_fst_pair _ _ _ _) =>>.
    move/(PTree.elements_complete (ef_tenv ef)); eauto.
Qed.

Lemma fd_tenv_fd_penv f i:
  (exists t, (fd_tenv f)!i = Some t) <-> (exists llsz, (fd_penv f)!i = Some llsz).
Proof.
  have:= fd_penv_tenv f => /(_ i) H. split.
  - case=>> /(@PTree.elements_correct typ)/(@in_map_pair_fst ident typ)/H.
    case/(@in_map_fst_pair ident permission) => p.
    move/(@PTree.elements_complete permission). by exists p.
  - case=>> /(@PTree.elements_correct permission).
    move/(@in_map_pair_fst ident permission)/H.
    case/(@in_map_fst_pair ident typ) => t.
    move/(@PTree.elements_complete typ). by exists t.
Qed.

Corollary fd_szenv_wf:
  forall fd, wf_szenv (fd_szenv fd).
Proof. case=>>.
  - move=> H. have:= proj1 (fn_szenv_fn_szenv' _ _) (ex_intro _ _ H).
    case=> ll H'. apply forallb_forall =>> I1. apply forallb_forall =>> I2.
    have:= fn_szenv_wf _ _ _ _ H H'.
    case/forallb2_forall => LEN.
    case/(@In_nth_error (list expr)): I1 => n1 N1.
    have [l N1']: exists l, nth_error ll n1 = Some l.
    { case N: nth_error; eauto.
      move/nth_error_None: N; rewrite LEN.
      move/nth_error_None; by rewrite N1. }
    move/(_ _ _ _ N1' N1).
    case/forallb2_forall => LEN'.
    case/(@In_nth_error expr): I2 => n2 N2.
    have [> N2']: exists x, nth_error l n2 = Some x.
    { case N: nth_error; eauto.
      move/nth_error_None: N; rewrite LEN'.
      move/nth_error_None; by rewrite N2. }
    move/(_ _ _ _ N2' N2).
    by apply: wf_size_expr'_wf_size_expr.
  - apply ef_szenv_wf.
Qed.

Corollary fd_szenv_tenv_str:
  forall fd, valid_szenve_tenv_str (fd_szenv fd) (fd_tenv fd).
Proof. case=>>. apply fn_szenv_tenv_str. apply ef_szenv_tenv_str. Qed.

Section SAFE_CONSTRUCTORS.

Definition valid_tenv_sig_check te params sig_args :=
  forallb2 (fun p targ => match te!p with
                          | None => false
                          | Some t => typ_beq t targ
                          end) params sig_args.

Lemma valid_tenv_sig_check_correct {te params sig_args}:
  valid_tenv_sig_check te params sig_args = true ->
  valid_tenv_sig te params sig_args.
Proof.
  revert params sig_args.
  induction params; intros []; try discriminate.
  + unfold valid_tenv_sig; simpl. intro; split; [reflexivity|].
    intros [] *; discriminate.
  + simpl. destruct te!a eqn:Ha; [|discriminate]. intro H.
    apply andb_prop in H as [Ht H]. apply IHparams in H.
    unfold valid_tenv_sig in *. destruct H as [Hlen H].
    split; simpl. auto. intros [] *; simpl.
    intros [= <-]. rewrite Ha. now rewrite (typ_dec_bl _ _ Ht). eauto.
Qed.

Definition valid_tenv_sztype_check te sze :=
  PTree_Properties.for_all sze (fun id lsz =>
    forallb (forallb (fun sz => match te!sz with
                       | Some (Tint64 Unsigned) => true
                       | _ => false
                       end)) lsz).

Lemma valid_tenv_sztype_check_correct {te sze}:
  valid_tenv_sztype_check te sze = true ->
  valid_tenv_sztype te sze.
Proof.
  intros * H id llsz Hsze.
  apply Forall_forall. intros lsz Hlsz.
  apply Forall_forall. intros sz Hsz.
  apply PTree_Properties.for_all_correct with (2 := Hsze) in H.
  apply forallb_forall with (2 := Hlsz), forallb_forall with (2 := Hsz) in H.
  now do 3 destruct_match H.
Qed.

Definition valid_tenv_vars_check (te: tenv) (vars: list ident) :=
  forallb (fun id => match te!id with None => false | Some _ => true end) vars &&
  PTree_Properties.for_all te (fun id _ => mem Pos.eqb id vars).

Lemma valid_tenv_vars_check_correct {te vars}:
  valid_tenv_vars_check te vars = true ->
  valid_tenv_vars te vars.
Proof.
  case/andP => H1 H2 i. split.
  - intros Hin. apply forallb_forall with (2 := Hin) in H1.
    destruct_match H1; eauto.
  - case=> t Ht. apply PTree_Properties.for_all_correct with (2 := Ht) in H2.
    by move/(mem_dec_spec _ Pos.eqb_spec): H2.
Qed.

Definition valid_szenve_params_check sze params :=
  PTree_Properties.for_all sze (fun id llsz =>
    negb (in_dec Pos.eq_dec id params) ||
    forallb (forallb (fun sz => forall_vars (fun x => in_dec Pos.eq_dec x params) sz)) llsz).

Lemma valid_szenve_params_check_correct {sze params}:
  valid_szenve_params_check sze params = true ->
  valid_szenve_params sze params.
Proof.
  intros * H id llsz Hsze Hin.
  apply PTree_Properties.for_all_correct with (2 := Hsze) in H.
  apply orb_prop in H. destruct H. now case in_dec in H.
  apply Forall_forall. intros lsz Hlsz.
  apply Forall_forall. intros sz Hsz.
  now apply forallb_forall with (2 := Hlsz), forallb_forall with (2 := Hsz) in H.
Qed.

Definition same_domain_check {A B: Type} (t1: PTree.t A) (t2: PTree.t B) :=
  forallb (fun id => match t1!id with Some _ => true | _ => false end)
    (map fst (PTree.elements t2)) &&
  forallb (fun id => match t2!id with Some _ => true | _ => false end)
    (map fst (PTree.elements t1)).

Lemma same_domain_check_correct {A B: Type} {t1: PTree.t A} {t2: PTree.t B}:
  same_domain_check t1 t2 = true ->
  same_domain t1 t2.
Proof.
  intros * H. apply andb_prop in H as [H1 H2]. intro id. split; intro H.
  - apply forallb_forall with (2 := H) in H2. destruct_match H2.
    now apply PTree.elements_correct, in_map_pair_fst in Heq.
  - apply forallb_forall with (2 := H) in H1. destruct_match H1.
    now apply PTree.elements_correct, in_map_pair_fst in Heq.
Qed.

Definition valid_szenve_tenv_str_check (sze: szenve) (te: tenv) :=
  PTree_Properties.for_all te (fun id t =>
    match sze!id with
    | Some llsz => match_type_llsz t llsz
    | None => true
    end
  ).

Lemma valid_szenve_tenv_str_check_correct {sze te}:
  valid_szenve_tenv_str_check sze te ->
  valid_szenve_tenv_str sze te.
Proof. move/PTree_Properties.for_all_correct => H i t lls /H/[swap] => -> //. Qed.

Definition valid_penv_szenvi_check (pe: penv) (sze: szenvi) :=
  PTree_Properties.for_all sze (fun id llsz =>
      forallb (forallb (fun sz =>
        match pe!sz as rw' return pe!sz = rw' -> _ with
        | Some Shared => fun _ => true
        | _ => fun _ => false
        end eq_refl)) llsz).

Lemma valid_penv_szenvi_check_correct {pe sze}:
  valid_penv_szenvi_check pe sze = true ->
  valid_penv_szenvi pe sze.
Proof.
  intros * H. intros id llsz Hsze.
  apply PTree_Properties.for_all_correct with (2 := Hsze) in H.
  apply Forall_forall. intros lsz Hlsz.
  apply Forall_forall. intros sz Hsz.
  apply forallb_forall with (2 := Hlsz), forallb_forall with (2 := Hsz) in H.
  destruct pe!sz; [|discriminate]. now destruct p.
Qed.

Definition valid_szenvi_szvars_check (sze: szenvi) :=
  PTree_Properties.for_all sze (fun id llsz =>
      forallb (forallb (fun sz =>
        match sze!sz with
        | Some [] => true
        | _       => false
        end)) llsz).

Lemma valid_szenvi_szvars_check_correct {sze}:
  valid_szenvi_szvars_check sze = true ->
  valid_szenvi_szvars sze.
Proof.
  intros * H. intros id llsz Hsze.
  apply PTree_Properties.for_all_correct with (2 := Hsze) in H.
  apply Forall_forall. intros lsz Hlsz.
  apply Forall_forall. intros sz Hsz.
  apply forallb_forall with (2 := Hlsz), forallb_forall with (2 := Hsz) in H.
  now destruct sze!sz as [[]|].
Qed.

Definition valid_szenve_szexprs_check (sze: szenve) :=
  PTree_Properties.for_all sze (fun id llsz =>
      forallb (forallb
        (forall_vars (fun x => match sze!x with
                               | Some [] => true
                               | _       => false
                               end))) llsz).

Lemma valid_szenve_szexprs_check_correct {sze}:
  valid_szenve_szexprs_check sze = true ->
  valid_szenve_szexprs sze.
Proof.
  intros * H. intros id llsz Hsze.
  apply PTree_Properties.for_all_correct with (2 := Hsze) in H.
  apply Forall_forall. intros lsz Hlsz.
  apply Forall_forall. intros sz Hsz.
  now apply forallb_forall with (2 := Hlsz), forallb_forall with (2 := Hsz) in H.
Qed.

Definition same_structure_check (sze: szenve) (sze': szenvi) :=
  PTree_Properties.for_all sze (fun i llsz =>
      match sze'!i with
      | Some llsz' => forallb2 (fun lsz lsz' => (length lsz =? length lsz')%nat) llsz llsz'
      | None => true
      end).

Lemma same_structure_check_correct {sze sze'}:
  same_structure_check sze sze' ->
  same_structure sze sze'.
Proof.
  intros H i * Hllsz Hllsz'.
  apply PTree_Properties.for_all_correct with (2 := Hllsz) in H.
  rewrite Hllsz' in H. clear Hllsz Hllsz'. revert llsz llsz' H.
  induction llsz; simpl.
  - case; eauto; discriminate.
  - case. discriminate. intros *. case/andP => LEN. move/IHllsz.
    move/PeanoNat.Nat.eqb_spec in LEN. by econstructor.
Qed.

Definition valid_szenvi_params_check (sze: szenvi) (params: list ident) :=
  PTree_Properties.for_all sze (fun i llsz =>
    forallb (forallb (fun sz => ~~ in_dec Pos.eq_dec sz params)) llsz
  ).

Lemma valid_szenvi_params_check_correct {sze params}:
  valid_szenvi_params_check sze params ->
  valid_szenvi_params sze params.
Proof.
  intros H i * Hllsz.
  apply PTree_Properties.for_all_correct with (2 := Hllsz) in H.
  apply Forall_forall. intros * H1. apply forallb_forall with (2 := H1) in H.
  apply Forall_forall. intros * H2. apply forallb_forall with (2 := H2) in H.
  revert H. now case in_dec.
Qed.

Definition valid_szenv_szenv'_injective_check (sze: szenve) (sze': szenvi) :=
  PTree_Properties.for_all sze (fun i llsz1 =>
    PTree_Properties.for_all sze (fun i' llsz2 =>
      match sze'!i, sze'!i' with
      | Some llsz1', Some llsz2' =>
        forallb2 (forallb2 (fun sz1 sz1' =>
          forallb2 (forallb2 (fun sz2 sz2' =>
            if Pos.eqb sz1' sz2' then expr_beq sz1 sz2
            else true
          )) llsz2 llsz2'
        )) llsz1 llsz1'
      | _, _ => true
      end
    )
  ).

Lemma valid_szenv_szenv'_injective_check_correct {sze sze'}:
  valid_szenv_szenv'_injective_check sze sze' ->
  valid_szenv_szenv'_injective sze sze'.
Proof.
  move=> + ?? llsz1 llsz2 llsz1' llsz2' j j' lsz1 lsz2 lsz1' lsz2' k k' >.
  move=> + H1 H2 H1' H2' J1 J2 J1' J2' K1' K2'.
  move/PTree_Properties.for_all_correct => /(_ _ _ H1).
  move/PTree_Properties.for_all_correct => /(_ _ _ H2).
  rewrite H1' H2'.
  case/forallb2_forall => LEN1.
  move/(_ _ _ _ J1 J1').
  case/forallb2_forall => LEN1'.
  have [> K1]: (exists sz1, nth_error lsz1 k = Some sz1).
  { case N: nth_error; eauto.
    move/nth_error_None: N; rewrite LEN1'.
    move/nth_error_None; by rewrite K1'. }
  move/(_ _ _ _ K1 K1').
  case/forallb2_forall => LEN2.
  move/(_ _ _ _ J2 J2').
  case/forallb2_forall => LEN2'.
  have [> K2]: (exists sz2, nth_error lsz2 k' = Some sz2).
  { case N: nth_error; eauto.
    move/nth_error_None: N; rewrite LEN2'.
    move/nth_error_None; by rewrite K2'. }
  move/(_ _ _ _ K2 K2').
  rewrite Pos.eqb_refl K1 K2; move/expr_reflect => -> //.
Qed.

Definition wf_szenv_check (sze: szenve) :=
  PTree_Properties.for_all sze (fun _ llsz =>
    forallb (forallb wf_size_expr) llsz).

Definition wf_szenv'_check (sze: szenve) (szi: szenvi) :=
  PTree_Properties.for_all sze (fun i llsz =>
    match szi!i with
    | Some llsz' => forallb2 (forallb2 wf_size_expr') llsz' llsz
    | None => false
    end).

Lemma wf_szenv_check_correct {sze}:
  wf_szenv_check sze ->
  wf_szenv sze.
Proof. by move/PTree_Properties.for_all_correct. Qed.

Lemma wf_szenv'_check_correct {sze szi}:
  wf_szenv'_check sze szi ->
  wf_szenv' sze szi.
Proof.
  move/PTree_Properties.for_all_correct => + > Si Si'.
  move/(_ _ _ Si). by rewrite Si'.
Qed.

Definition owned_szvars_separated_check (sze: szenvi) (pe: penv) :=
  PTree_Properties.for_all pe (fun i perm =>
    match perm with
    | Owned =>
      PTree_Properties.for_all sze (fun i' llsz' =>
        Pos.eqb i i' ||
        match sze!i with
        | None => true
        | Some llsz =>
          forallb (forallb (fun sz1 =>
              forallb (forallb (fun sz2 => negb (Pos.eqb sz1 sz2))) llsz'
          )) llsz
        end)
    | _ => true
    end).

Lemma owned_szvars_separated_check_correct {sze pe}:
  owned_szvars_separated_check sze pe ->
  owned_szvars_separated sze pe.
Proof.
  move/PTree_Properties.for_all_correct => + ? i' > => /[apply].
  move/PTree_Properties.for_all_correct/(_ i').
  case Pos.eqb_spec => // _ + _ Si => /[apply] /=. rewrite Si.
  do 4 move/forallb_forall/[apply]. by move/Pos.eqb_spec.
Qed.

Declare Scope syntax_scope.
Local Open Scope syntax_scope.

Definition check_bind {A: Type} (msg: string) (b: bool) : (b = true -> res A) -> res A :=
  match b with
  | true  => fun f => f eq_refl
  | false => fun _ => Error [ MSG msg ]
  end.

Notation "'let*' X := [ M ] A 'in' B" :=
  (check_bind M A (fun X => B))
    (at level 200, X name, M at level 100, A at level 100, B at level 200)
    : syntax_scope.

Definition make_function sig params vars body te sze sze' pe :=
  match Coqlib.list_norepet_dec Pos.eq_dec params with
  | left Hparams_norepet =>
    match Coqlib.list_disjoint_dec Pos.eq_dec params vars with
    | left Hdisjoint =>
      let* Vte_sig       := ["Type environment doesn't match signature."]
                            valid_tenv_sig_check te params (sig_args sig) in
      let* Vte_sztype    := ["A size variable is not of type u64."]
                            valid_tenv_sztype_check te sze' in
      let* Vte_vars      := ["Type environment's domain is not correct."]
                            valid_tenv_vars_check te (params ++ vars) in
      let* Vsze_params   := ["An array in parameters have one of its size variable" ++
                             " which is not in parameters."]
                            valid_szenve_params_check sze params in
      let* Vsze'_params  := ["A parameter is defined in size variables environment."]
                            valid_szenvi_params_check sze' params in
      let* Vwf_sze       := ["A size expression is not well-formed."]
                            wf_szenv'_check sze sze' in
      let* Vsze_te       := ["Size variable environment and type environment" ++
                             " don't have the same domain."]
                            same_domain_check sze te in
      let* Vsze_te_str   := ["Size environment and type environment don't have the " ++
                             "same structure"]
                            valid_szenve_tenv_str_check sze te in
      let* Vpe_te        := ["Type environment and permission environment don't" ++
                             " have the same domain."]
                            same_domain_check pe te in
      let* Vpe_sze'      := ["Some size variables are not read-only."]
                            valid_penv_szenvi_check pe sze' in
      let* Vsze_sze'     := ["Size variable environment and size expressions envrionment \
                              don't have the same domain."]
                            same_domain_check sze sze' in
      let* Vsze_sze'_str := ["Size variable environment and size expressions envrionment \
                             don't have the same structure."]
                            same_structure_check sze sze' in
      let* Vsze_sze'_inj := ["A size variable is associated to two different \
                             size expressions."]
                            valid_szenv_szenv'_injective_check sze sze' in
      let* Vsze_szvars   := ["Some variables in size expressions are not defined in size environment."]
                            valid_szenve_szexprs_check sze in
      let* Vsze_szvars'  := ["Some size variables are not defined in size environment."]
                            valid_szenvi_szvars_check sze' in
      let* Vsze_owned    := ["Size variables of owned arrays cannot be shared with other arrays."]
                            owned_szvars_separated_check sze' pe in
      let* res_prim      := ["Return type is not primitive."]
                            primitive_type (sig_res sig) in
      OK (mk_function sig params vars body te sze sze' pe
                      Hparams_norepet Hdisjoint
                      res_prim
                      (valid_tenv_sig_check_correct Vte_sig)
                      (valid_tenv_sztype_check_correct Vte_sztype)
                      (valid_tenv_vars_check_correct Vte_vars)
                      (valid_szenve_params_check_correct Vsze_params)
                      (valid_szenvi_params_check_correct Vsze'_params)
                      (wf_szenv'_check_correct Vwf_sze)
                      (same_domain_check_correct Vsze_te)
                      (valid_szenve_tenv_str_check_correct Vsze_te_str)
                      (same_domain_check_correct Vsze_sze')
                      (same_structure_check_correct Vsze_sze'_str)
                      (valid_szenv_szenv'_injective_check_correct Vsze_sze'_inj)
                      (valid_szenve_szexprs_check_correct Vsze_szvars)
                      (valid_szenvi_szvars_check_correct Vsze_szvars')
                      (owned_szvars_separated_check_correct Vsze_owned)
                      (same_domain_check_correct Vpe_te)
                      (valid_penv_szenvi_check_correct Vpe_sze'))
    | _ => Error [MSG "A local variable is aleady defined in parameters."]
    end
  | _ => Error [MSG "Two parameters have the same name."]
  end.

Definition make_external_function name sig params te sze pe :=
  match Coqlib.list_norepet_dec Pos.eq_dec params with
  | left Hparams_norepet =>
    let* Vte_sig     := ["Type environment doesn't match signature."]
                        valid_tenv_sig_check te params (sig_args sig) in
    let* Vte_params  := ["Type environment's domain is not correct."]
                        valid_tenv_vars_check te params in
    let* Vsze_params := ["An array in parameters have one of its size variable" ++
                         " which is not in parameters."]
                        valid_szenve_params_check sze params in
    let* Vwf_sze     := ["A size expression is not well-formed."]
                        wf_szenv_check sze in
    let* Vsze_te     := ["Size variable environment and type environment" ++
                         " don't have the same domain."]
                        same_domain_check sze te in
    let* Vsze_te_str := ["Size environment and type environment don't have the " ++
                         "same structure"]
                        valid_szenve_tenv_str_check sze te in
    let* Vpe_te      := ["Type environment and permission environment don't" ++
                         " have the same domain."]
                        same_domain_check pe te in
    let* Vsze_szvars := ["Some size variables are not defined in size environment."]
                        valid_szenve_szexprs_check sze in
    let* res_prim    := ["Return type is not primitive."]
                        primitive_type (sig_res sig) in
    OK (mk_ef name sig params te sze pe
                    Hparams_norepet
                    res_prim
                    (valid_tenv_sig_check_correct Vte_sig)
                    (valid_tenv_vars_check_correct Vte_params)
                    (valid_szenve_params_check_correct Vsze_params)
                    (wf_szenv_check_correct Vwf_sze)
                    (same_domain_check_correct Vsze_te)
                    (valid_szenve_tenv_str_check_correct Vsze_te_str)
                    (valid_szenve_szexprs_check_correct Vsze_szvars)
                    (same_domain_check_correct Vpe_te))
  | _ => Error [MSG "Two parameters have the same name."]
  end.

Definition make_program fds id_main :=
  match Coqlib.list_norepet_dec Pos.eq_dec (map fst fds) with
  | left Hnorepet =>
      OK (mk_program fds id_main Hnorepet)
  | _ => Error [MSG "Two functions have the same name."]
  end.

End SAFE_CONSTRUCTORS.

Lemma all_params_in_penv:
  forall (fd: fundef) i,
    In i (fd_params fd) ->
    exists p, (fd_penv fd)!i = Some p.
Proof.
  move=> f i. have [] := fd_tenv_sig f.
  move: (fd_params f) (sig_args _).
  elim/list_ind => //=.
  move=> i' params IH [//|t sig] /= /eq_add_S LEN H. case=> [<-|].
  - have:= eq_sym (H 0%nat i' eq_refl) => /=.
    move/(PTree.elements_correct (fd_tenv f)).
    move/(in_map_pair_fst (PTree.elements (fd_tenv f))).
    move/(proj2 (fd_penv_tenv f i')).
    case/(in_map_fst_pair (PTree.elements (fd_penv f))) => p.
    move/(PTree.elements_complete (fd_penv f)). eauto.
  - by move/(IH _ LEN (fun i => H (S i))).
Qed.

Fixpoint get_type_syn_path (l: list syn_path_elem) (t: typ) : option typ :=
  match l with
  | [] => Some t
  | Scell _ :: l =>
      match t with
      | Tarr t => get_type_syn_path l t
      | _ => None
      end
  end.

Definition get_tenv_syn_path (te: tenv) (p: syn_path) : option typ :=
  let (i, l) := p in
  match te!i with
  | Some t => get_type_syn_path l t
  | _ => None
  end.

Fixpoint get_size_syn_path {A: Type}
                           (pl: list syn_path_elem) (llsz: list (list A))
                           : option (list (list A)) :=
  match pl with
  | [] => Some llsz
  | Scell idx :: pl =>
      match llsz with
      | [] => None
      | _ :: llsz => get_size_syn_path pl llsz
      end
  end.

Local Open Scope option_bool_monad_scope.

Definition get_szenv_syn_path {A: Type} (sze: PTree.t (list (list A))) (p: syn_path) :=
  let (id, pl) := p in
  doo llsz <- sze!id; get_size_syn_path pl llsz.

Fixpoint get_szenv_syn_path_list {A: Type} (sze: PTree.t (list (list A))) (lp : list syn_path) :=
  match lp with
  | [] => Some []
  | p :: lp =>
    doo t  <- get_szenv_syn_path sze p;
    doo lt <- get_szenv_syn_path_list sze lp;
    Some (t :: lt)
  end.

Local Close Scope option_bool_monad_scope.