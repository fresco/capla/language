Require Import Errors.

Ltac rewrite_one_equality :=
  let s H c := is_constructor c; injection H; clear H; intros; subst in
  multimatch goal with
  | H: _ = ?n |- _ =>
    try (is_var n; revert H; intros [= <-])
  | H: ?n = _ |- _ =>
    try (is_var n; revert H; intros [= ->])
  | H: _ = Some ?n |- _ =>
    try (is_var n; injection H; clear H; intros [= <-])
  | H: Some ?n = _ |- _ =>
    try (is_var n; injection H; clear H; intros [= ->])
  | H: _ = OK ?n |- _ =>
    try (is_var n; injection H; clear H; intros [= <-])
  | H: OK ?n = _ |- _ =>
    try (is_var n; injection H; clear H; intros [= ->])
  | H: ?c _ = ?c _ |- _ => s H c
  | H: ?c _ _ = ?c _ _ |- _ => s H c
  | H: ?c _ _ _ = ?c _ _ _ |- _ => s H c
  | H: ?c _ _ _ _ = ?c _ _ _ _ |- _ => s H c
  | H: ?c _ _ _ _ _ = ?c _ _ _ _ _ |- _ => s H c
  | H: ?c _ _ _ _ _ _ = ?c _ _ _ _ _ _ |- _ => s H c
  | H: ?c _ _ _ _ _ _ _ = ?c _ _ _ _ _ _ _ |- _ => s H c
  | H: ?c _ _ _ _ _ _ _ _ = ?c _ _ _ _ _ _ _ _ |- _ => s H c
  | H: ?c _ _ _ _ _ _ _ _ _ = ?c _ _ _ _ _ _ _ _ _ |- _ => s H c
  | H: ?c _ _ _ _ _ _ _ _ _ _ = ?c _ _ _ _ _ _ _ _ _ _ |- _ => s H c
  end.

Ltac rewrite_equalities :=
  repeat (rewrite_one_equality).

Ltac intro_ceq' k :=
  let X := fresh in
  let s T c := is_constructor c; intro X; injection X; clear X; intro_ceq' k in
  match goal with
  | |- (?n = ?m) -> ?T => is_var n; intro X; intro_ceq' k; revert X
  | |- (?C _ = ?C _) -> ?T => s T C
  | |- (?C _ _ = ?C _ _) -> ?T => s T C
  | |- (?C _ _ _ = ?C _ _ _) -> ?T => s T C
  | |- (?C _ _ _ _ = ?C _ _ _ _) -> ?T => s T C
  | |- (?C _ _ _ _ _ = ?C _ _ _ _ _) -> ?T => s T C
  | |- (?C _ _ _ _ _ _ = ?C _ _ _ _ _ _) -> ?T => s T C
  | |- ?H -> ?T => intro X; intro_ceq' ltac:(fun Y => revert X; k Y)
  | |- _ => k ltac:(idtac)
  end.

Ltac intro_ceq := intro_ceq' ltac:(fun Y => idtac).

Ltac destruct_match T :=
  match type of T with
  | (match ?c with _ => _ end = _) =>
      let id := fresh "Heq" in destruct c eqn:id;
      try discriminate
  | (match ?c with _ => _ end) =>
      let id := fresh "Heq" in destruct c eqn:id;
      try contradiction
  | _ => idtac
  end.

Ltac destruct_match_noeq T :=
  match type of T with
  | (match ?c with _ => _ end = _) =>
      destruct c; try discriminate
  | (match ?c with _ => _ end) =>
      destruct c; try contradiction
  | _ => idtac
  end.

Ltac destruct_match_clear T :=
  match type of T with
  | (match ?c with _ => _ end = _) =>
      let id := fresh in destruct c eqn:id;
      try discriminate; clear T; rename id into T
  | (match ?c with _ => _ end) =>
      let id := fresh in destruct c eqn:id;
      try contradiction; clear T; rename id into T
  | _ => idtac
  end.

Ltac destruct_match_term T :=
  match type of T with
  | (match ?c with _ => _ end = _) =>
      let id := fresh in destruct c eqn:id;
      try easy
  | (match ?c with _ => _ end) =>
      let id := fresh in destruct c eqn:id;
      try easy
  | _ => idtac "Type of term" T "is not a match"
  end.

Ltac destruct_match_type T :=
  match T with
  | (match ?c with _ => _ end = _) -> _ =>
      let id := fresh "Heq" in destruct c eqn:id;
      try discriminate
  | (match ?c with _ => _ end) -> _ =>
      let id := fresh "Heq" in destruct c eqn:id;
      try contradiction
  | _ -> ?T => destruct_match_type T
  | _ => idtac
  end.

Ltac destruct_match_type_nointro T :=
  match T with
  | (match ?c with _ => _ end = _) -> _ =>
      let id := fresh "Heq" in destruct c eqn:id;
      try discriminate; revert id
  | (match ?c with _ => _ end) -> _ =>
      let id := fresh "Heq" in destruct c eqn:id;
      try contradiction; revert id
  | _ -> ?T => destruct_match_type_nointro T
  | _ => idtac
  end.

Ltac destruct_match_goal :=
  match goal with
  | |- ?T => destruct_match_type T
  | _ => idtac
  end.

Ltac destruct_match_goal_ni :=
  match goal with
  | |- ?T => destruct_match_type_nointro T
  | _ => idtac
  end.


Tactic Notation "inv" hyp(H) := inversion H; subst; clear H.
Tactic Notation "inv" integer(n) := inversion n; subst.