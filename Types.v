Require Import Coq.Bool.Bool List Lia.

Require Import ZArith.

Require Import Archi Maps.

Inductive intsize : Type := I8 | I16 | I32.

Scheme Equality for intsize.

Definition max_intsize s1 s2 :=
  match s1, s2 with
  | I8, _ => s2
  | I16, I8 => s1
  | I16, _ => s2
  | I32, I8 | I32, I16 => s1
  | I32, _ => s2
  end.

Inductive floatsize : Type := F32 | F64.

Definition eq_floatsize s1 s2 :=
  match s1, s2 with
  | F32, F32 => true
  | F64, F64 => true
  | _, _ => false
  end.

Scheme Equality for floatsize.

Inductive signedness : Type := Signed | Unsigned.

Scheme Equality for signedness.

Definition max_signedness s1 s2 :=
  match s1, s2 with
  | Signed, Signed => Signed
  | _, _ => Unsigned
  end.

Definition eq_signedness s1 s2 :=
  match s1, s2 with
  | Unsigned, Unsigned => true
  | Signed, Signed => true
  | _, _ => false
  end.

Inductive typ : Type :=
| Tvoid
| Tbool
| Tint: intsize -> signedness -> typ
| Tint64: signedness -> typ
| Tfloat: floatsize -> typ
| Tarr: typ -> typ.

Definition list_eq_dec' := list_eq_dec.

Scheme Equality for bool.
Scheme Equality for positive.
Scheme Equality for list.

Fixpoint typ_beq (t1 t2: typ): bool :=
  match t1, t2 with
  | Tvoid, Tvoid => true
  | Tbool, Tbool => true
  | Tint sz1 sg1, Tint sz2 sg2 => intsize_beq sz1 sz2 && signedness_beq sg1 sg2
  | Tint64   sg1, Tint64 sg2   => signedness_beq sg1 sg2
  | Tfloat   sz1, Tfloat sz2   => floatsize_beq sz1 sz2
  | Tarr      t1, Tarr    t2   => typ_beq t1 t2
  | _, _ => false
  end.

Theorem typ_reflect: forall t1 t2, reflect (t1 = t2) (typ_beq t1 t2).
Proof.
  induction t1; intro; case t2; intros; simpl; try now apply ReflectF.
  - now apply ReflectT.
  - now apply ReflectT.
  - case i, i0, s, s0; now (apply ReflectF || apply ReflectT).
  - case s, s0; now (apply ReflectF || apply ReflectT).
  - case f, f0; now (apply ReflectF || apply ReflectT).
  - case (IHt1 t) as [<-|Hneq]; simpl.
    now apply ReflectT. apply ReflectF; now injection.
Qed.

Corollary typ_dec_bl: forall x y: typ, typ_beq x y = true -> x = y.
Proof. intros. now destruct (typ_reflect x y). Qed.

Corollary typ_dec_lb: forall x y: typ, x = y -> typ_beq x y = true.
Proof. intros. now destruct (typ_reflect x y). Qed.

Definition Tuint64 := Tint64 Unsigned.

Definition typ_sig (t: typ) : option signedness :=
  match t with
  | Tint _ sig => Some sig
  | _ => None
  end.

Definition tenv := PTree.t typ.

Definition typ_sizeof (t: typ) : Z :=
  match t with
  | Tvoid | Tbool => 1
  | Tint    I8 _ => 1
  | Tint   I16 _ => 2
  | Tint   I32 _ => 4
  | Tint64     _ => 8
  | Tfloat F32   => 4
  | Tfloat F64   => 8
  | Tarr       _ => if Archi.ptr64 then 8 else 4
  end.

Lemma typ_sizeof_bounds (t: typ):
  (1 <= typ_sizeof t <= 8)%Z.
Proof.
  destruct t; simpl; try destruct i; try destruct f; try destruct ptr64; lia.
Qed.

Record signature : Type := mk_signature {
  sig_args: list typ;
  sig_res:  typ
}.

Definition primitive_type (t: typ) :=
  match t with
  | Tarr _ => false
  | _ => true
  end.
