Require Import ssreflect ssrbool ssrfun.
Require Import String List.
Require Import BinNums BinInt.
Require Import Coq.Bool.Bool.
Require BinaryString.

Require Import Types Syntax Ops BValues BEnv.
Require Import SemPath Alias BUtils ListUtils PTreeaux.
Require Import BPrinters.



Require Import Integers Maps Errors.

Coercion function_to_fundef := Internal.
Coercion external_function_to_fundef := External.

Import ListNotations.

Local Open Scope bool_scope.

Definition iset := PTree.t unit.
Definition empty_iset := PTree.empty unit.

Section Messages.

Definition UndefVariable                := msg "Typing: Undefined variable.".
Definition InvalidCastInputType         := msg "Typing: Inferred type for cast is not valid.".
Definition IncompatibleTypesCast        := msg "Typing: Incompatible types in cast.".
Definition WrongTypeBooleanNot          := msg "Typing: Boolean not operator can only be applied booleans.".
Definition WrongKindIntegerNot          := msg "Typing: Inferred operation kind is not valid for integer not operation.".
Definition WrongTypeIntegerNot          := msg "Typing: Integer not operator can only be applied on integers.".
Definition WrongTypeNegation            := msg "Typing: Negation operator can only be applied on integers of floats.".
Definition WrongTypeAbsoluteValue       := msg "Typing: Absolute value is applied on something else than a float.".
Definition WrongSignDivision            := msg "Typing: Division must be of the sign of its operands.".
Definition WrongTypeModulo              := msg "Typing: Modulo operator can only be applied on integers.".
Definition WrongTypeBitwiseOps          := msg "Typing: |, & and ^ are only defined on integers and booleans.".
Definition WrongTypeShift               := msg "Typing: Shifts are only defined on integers.".
Definition WrongOffsetTypeShift         := msg "Typing: Shift offset must be of type u8, u16 or u32.".
Definition InferenceErrorShift          := msg "Typing: inference error for shifts.".
Definition WrongTypeBinaryOperator      := msg "Typing: Binary arithmetic operators can only be applied on operands of integer or float type.".
Definition ErrorDifferentTypesBinOp     := msg "Typing: Binary arithmetic operator can only be applied on operands of the same type.".
Definition WrongTypeSignedCmp           := msg "Typing: Signed comparison can only be applied on signed integers or floats.".
Definition ErrorDifferentTypesCmpOp     := msg "Typing: Comparison operation can only be applied on integers or floats of the same type.".
Definition WrongTypeUnsignedCmp         := msg "Typing: Unsigned comparison can only be applied on unsigned integers or floats.".
Definition UndefVariableWithName        i : string := "Typing: Undefined variable " ++ (BPrinters.string_of_var_ident i) ++ ".".
Definition NoSizeVariablesWithName      i : string := "Typing: No size variables for " ++ (BPrinters.string_of_var_ident i) ++ ".".
Definition WrongPathLength              := msg "Typing: Mismatch between size variables and path.".
Definition ReadingNonPrimitive          := msg "Typing: Reading a non-primitive type.".
Definition WrongTypeIndex               := msg "Typing: Type of index is not u64.".
Definition WrongNbOfIndices             := msg "Typing: Incorrect number of indices.".
Definition FullPathNeeded               := msg "Typing: Path length and number of size variables don't match.".
Definition UnsupportedOwnershipTransfer := msg "Typing: Owned arrays arguments are not currently supported.".
Definition OnlyArrayMutBorrow           := msg "Typing: Only arrays can be borrowed as mutable.".
Definition OwnershipTransferNeededLater := msg "Typing: An ownership transfered array is needed after function call.".
Definition InvalidPermission            := msg "Typing: An argument permission is lower than the needed permission.".
Definition PossibleAlias                := msg "Typing: A mutable path is possibly aliased in arguments.".
Definition NonEmptyTransferedPath       := msg "Typing: Transfered paths must be empty.".
Definition MismatchArrayStructure       := msg "Typing: Size variables structure do not match.".
Definition WrongNbOfArguments           := msg "Typing: Too many or not enough arguments.".
Definition WritingNotAllowed            := msg "Typing: Writing is not allowed.".
Definition NoPermission                 := msg "Typing: Variable permission is not defined.".
Definition InvalidWritingType           := msg "Typing: Type of variable and expression don't match.".
Definition WritingNonPrimitive          := msg "Typing: Writing non-primitive type.".
Definition WrongContentTypeAlloc        := msg "Typing: Dynamically allocated array must contain primitive objects.".
Definition WrongTypeSizeExpr            := msg "Typing: Size expression is not of type u64.".
Definition OnlyArrayAlloc               := msg "Typing: Only owned array can be dynamically allocated.".
Definition OnlyPrimArrayAlloc           := msg "Typing: Only primitive arrays can be dynamically allocated.".
Definition WrongTypeAlloc               := msg "Typing: Destination of dynamic allocation is not an array.".
Definition WrongContentTypeFree         := msg "Typing: Only arrays containing primitive object can be freed.".
Definition OnlyArrayFree                := msg "Typing: Only owned arrays can be freed.".
Definition OnlyPrimArrayFree            := msg "Typing: Only primitive arrays can be freed.".
Definition FreeUsedLater                := msg "A size variable of the freed array is used later.".
Definition UndefFunction                : string := "Typing: Function doesn't exists.".
Definition WrongTypeArguments           := msg "Typing: Types of arguments and parameters don't match.".
Definition WrongTypeReturnVar           := msg "Typing: Type of variable and function return type don't match.".
Definition NonVoidReturnType            := msg "Typing: Return type of function is not void.".
Definition WrongTypeReturn              := msg "Typing: Return type and expression's type don't match.".
Definition WrongTypeAssert              := msg "Typing: Asserted condition is not of type bool.".
Definition WrongTypeConditional         := msg "Typing: Conditional expression is not of type bool.".
Definition InvalidExit                  := msg "Typing: Invalid exit instruction, depth is too large.".
Definition MissingReturn                := msg "Typing: There is a missing return in a branch of the program.".
Definition VarUsedBeforeInit            := msg "Typing: A variable is used before initialization.".

Opaque UndefVariable
  InvalidCastInputType
  IncompatibleTypesCast
  WrongTypeBooleanNot
  WrongKindIntegerNot
  WrongTypeIntegerNot
  WrongTypeNegation
  WrongTypeAbsoluteValue
  WrongSignDivision
  WrongTypeModulo
  WrongTypeBitwiseOps
  WrongTypeShift
  WrongOffsetTypeShift
  InferenceErrorShift
  WrongTypeBinaryOperator
  ErrorDifferentTypesBinOp
  WrongTypeSignedCmp
  ErrorDifferentTypesCmpOp
  WrongTypeUnsignedCmp
  UndefVariableWithName
  NoSizeVariablesWithName
  WrongPathLength
  ReadingNonPrimitive
  WrongTypeIndex
  WrongNbOfIndices
  FullPathNeeded
  UnsupportedOwnershipTransfer
  OnlyArrayMutBorrow
  OwnershipTransferNeededLater
  InvalidPermission
  PossibleAlias
  NonEmptyTransferedPath
  MismatchArrayStructure
  WrongNbOfArguments
  WritingNotAllowed
  NoPermission
  InvalidWritingType
  WritingNonPrimitive
  WrongContentTypeAlloc
  WrongTypeSizeExpr
  OnlyArrayAlloc
  OnlyPrimArrayAlloc
  WrongTypeAlloc
  WrongContentTypeFree
  OnlyArrayFree
  OnlyPrimArrayFree
  FreeUsedLater
  UndefFunction
  WrongTypeArguments
  WrongTypeReturnVar
  NonVoidReturnType
  WrongTypeReturn
  WrongTypeAssert
  WrongTypeConditional
  InvalidExit
  MissingReturn
  VarUsedBeforeInit.

End Messages.

Notation "A @ i" := (match A!i with
                     | None => OK tt
                     | Some _ => Error UndefVariable
                     end) (at level 1).

Section TypeExpression.

Definition typ_corresp_opkind_unop (t: typ) (k: operation_kind) :=
  match k, t with
  | OInt, Tint _ _   => true
  | OInt64, Tint64 _ => true
  | OFloat32, Tfloat F32 => true
  | OFloat64, Tfloat F64 => true
  | _, _ => false
  end.

Local Open Scope error_monad_scope.
Local Open Scope option_error_monad_scope.
Local Open Scope option_bool_monad_scope.

Let empty := PTree.empty unit.

Fixpoint type_expression (f: fundef) (e: expr) {struct e} : res (typ * iset) :=
  match e with
  | Econst (Cint sz sig n) => OK (Tint sz sig, empty)
  | Econst (Cint64 sig n) => OK (Tint64 sig, empty)
  | Econst (Cfloat32 _) => OK (Tfloat F32, empty)
  | Econst (Cfloat64 _) => OK (Tfloat F64, empty)
  | Econst (Cbool _) => OK (Tbool, empty)
  | Ecast exp t1 t =>
    dop (texp, ne) <- type_expression f exp;
    match texp, t with
    | (Tint _ _ | Tint64 _ | Tfloat _),
      (Tint _ _ | Tint64 _ | Tfloat _) =>
      if typ_beq t1 texp then OK (t, ne)
      else Error InvalidCastInputType
    | Tbool, (Tint _ _ | Tint64 _ | Tbool) | (Tint _ _ | Tint64 _), Tbool =>
      if typ_beq t1 texp then OK (t, ne)
      else Error InvalidCastInputType
    | _, _ => Error IncompatibleTypesCast
    end
  | Eunop Onotbool _ e =>
    dop (texp, ne) <- type_expression f e;
    match texp with
    | Tbool => OK (texp, ne)
    | _ => Error WrongTypeBooleanNot
    end
  | Eunop Onotint k e =>
    dop (texp, ne) <- type_expression f e;
    match k, texp with
    | OInt, Tint I32 Unsigned => OK (Tint I32 Unsigned, ne)
    | OInt, Tint _ _ => OK (Tint I32 Signed, ne)
    | OInt64, Tint64 sg => OK (Tint64 sg, ne)
    | _, Tint64 _ | _, Tint _ _ =>
      Error WrongKindIntegerNot
    | _, _ =>
      Error WrongTypeIntegerNot
    end
  | Eunop Oneg k e =>
    dop (texp, ne) <- type_expression f e;
    if typ_corresp_opkind_unop texp k then
      match texp with
      | Tint I32 Unsigned => OK (Tint I32 Unsigned, ne)
      | Tint _ _ => OK (Tint I32 Signed, ne)
      | Tint64 sg => OK (Tint64 sg, ne)
      | _ => OK (texp, ne)
      end
    else Error WrongTypeNegation
  | Eunop Oabsfloat k e =>
    dop (texp, ne) <- type_expression f e;
    match texp, k with
    | Tfloat F32, OFloat32 => OK (Tfloat F32, ne)
    | Tfloat F64, OFloat64 => OK (Tfloat F64, ne)
    | _, _ => Error WrongTypeAbsoluteValue
    end
  | Ebinop_arith op k e1 e2 =>
    dop (te1, ne1) <- type_expression f e1;
    dop (te2, ne2) <- type_expression f e2;
    let ne := PTree_union ne1 ne2 in
    if typ_beq te1 te2 then
      match op, k, te1 with
      | (Omods | Odivs), OInt, Tint _ Signed => OK (Tint I32 Signed, ne)
      | (Omods | Odivs), OInt64, Tint64 Signed => OK (Tint64 Signed, ne)
      | (Omodu | Odivu), OInt, Tint I32 Unsigned => OK (Tint I32 Unsigned, ne)
      | (Omodu | Odivu), OInt, Tint _ Unsigned => OK (Tint I32 Signed, ne)
      | (Omodu | Odivu), OInt64, Tint64 Unsigned => OK (Tint64 Unsigned, ne)
      | (Odivu | Odivs), (OInt | OInt64), _ =>
        Error WrongSignDivision
      | (Omodu | Omods), _, _ =>
        Error WrongTypeModulo
      | (Oand | Oor | Oxor), (OFloat32 | OFloat64), _ =>
        Error WrongTypeBitwiseOps
      | (Oshl | Oshr | Oshru), (OFloat32 | OFloat64), _ =>
        Error WrongTypeShift
      | (Oshl | Oshr), OInt, Tint (I8|I16) Unsigned => OK (Tint I32 Signed, ne)
      | (Oshl | Oshru), OInt, Tint I32 Unsigned => OK (Tint I32 Unsigned, ne)
      | (Oshl | Oshr | Oshru), OInt, Tint _ Signed
      | (Oshl | Oshr | Oshru), OInt64, Tint64 _ =>
        Error WrongOffsetTypeShift
      | (Oshl | Oshr | Oshru), _, _ =>
        Error InferenceErrorShift
      | _, OInt, Tint (I8|I16) _ => OK (Tint I32 Signed, ne)
      | _, OInt, Tint I32 sg => OK (Tint I32 sg, ne)
      | _, OInt64, Tint64 sg => OK (Tint64 sg, ne)
      | _, OFloat32, Tfloat F32 => OK (Tfloat F32, ne)
      | _, OFloat64, Tfloat F64 => OK (Tfloat F64, ne)
      | _, _, _ =>
        Error WrongTypeBinaryOperator
      end
    else
      match op, k, te1, te2 with
      | (Oshl | Oshr | Oshru), (OFloat32 | OFloat64), _, _ =>
        Error WrongTypeShift
      | (Oshl |  Oshr),   OInt,  Tint (I8|I16)        _, Tint _ Unsigned => OK (Tint I32 Signed, ne)
      | (Oshl |  Oshr),   OInt,  Tint      I32   Signed, Tint _ Unsigned => OK (Tint I32 Signed, ne)
      | (Oshl | Oshru),   OInt,  Tint      I32 Unsigned, Tint _ Unsigned => OK (Tint I32 Unsigned, ne)
      | (Oshl |  Oshr), OInt64,  Tint64          Signed, Tint _ Unsigned => OK (Tint64 Signed, ne)
      | (Oshl | Oshru), OInt64,  Tint64        Unsigned, Tint _ Unsigned => OK (Tint64 Unsigned, ne)
      | (Oshl | Oshr | Oshru), (OInt | OInt64), _, (Tint _ Signed | Tint64 _) =>
        Error WrongOffsetTypeShift
      | (Oshl | Oshr | Oshru), _, _, _ =>
        Error InferenceErrorShift
      | _, _, _, _ =>
        Error ErrorDifferentTypesBinOp
      end
  | Ebinop_cmp op k e1 e2 =>
    dop (te1, ne1) <- type_expression f e1;
    dop (te2, ne2) <- type_expression f e2;
    let ne := PTree_union ne1 ne2 in
    if typ_beq te1 te2 then
      match k, te1 with
      | OInt, Tint _ Signed
      | OInt64, Tint64 Signed
      | OFloat32, Tfloat F32
      | OFloat64, Tfloat F64 => OK (Tbool, ne)
      | OInt, Tint _ Unsigned
      | OInt64, Tint64 Unsigned =>
        match op with
        | Oeq | One => OK (Tbool, ne)
        | _ =>
          Error WrongTypeSignedCmp
        end
      | _, _ =>
        Error WrongTypeSignedCmp
      end
    else
      Error ErrorDifferentTypesCmpOp
  | Ebinop_cmpu _ k e1 e2 =>
    dop (te1, ne1) <- type_expression f e1;
    dop (te2, ne2) <- type_expression f e2;
    let ne := PTree_union ne1 ne2 in
    if typ_beq te1 te2 then
      match k, te1 with
      |   OInt, Tint _ Unsigned => OK (Tbool, ne)
      | OInt64, Tint64 Unsigned => OK (Tbool, ne)
      |      _,               _ =>
        Error WrongTypeUnsignedCmp
      end
    else
      Error ErrorDifferentTypesCmpOp
  | Eacc p =>
    let (i, sp) := p in
    do** t    <- [UndefVariableWithName i] (fd_tenv f)!i;
    do** llsz <- [NoSizeVariablesWithName i] (fd_szenv f)!i;
    let llsz' := match f with
                 | Internal f => match (fn_szenv' f)!i with
                                 | Some llsz' => llsz'
                                 | None => []
                                 end
                 | _ => []
                 end in
    let type_syn_path_elem_list := fix g t llsz llsz' sp {struct sp} :=
        match sp, t, llsz, llsz' with
        | [], _, _, _ => OK (t, empty)
        | e :: sp, Tarr t, lsz :: llsz, [] =>
            do ne1 <- type_syn_path_elem f lsz e;
            dop (t, ne2) <- g t llsz [] sp;
            OK (t, PTree_union ne1 ne2)
        | e :: sp, Tarr t, lsz :: llsz, lsz' :: llsz' =>
            do ne1 <- type_syn_path_elem f lsz e;
            dop (t, ne2) <- g t llsz llsz' sp;
            let ne2' := set_list lsz' tt ne2 in
            OK (t, PTree_union ne1 ne2')
        | _, _, _, _ => Error WrongPathLength
        end in
    dop (t, ne) <- type_syn_path_elem_list t llsz llsz' sp;
    if primitive_type t then OK (t, PTree.set i tt ne)
    else Error ReadingNonPrimitive
  end
with type_syn_path_elem (f: fundef) (lsz: list expr)
                        (s: syn_path_elem) {struct s} :=
  match s with
  | Scell lidx =>
      if Nat.eqb (Datatypes.length lidx) (Datatypes.length lsz) then
        let type_expr_list := fix type_expr_list lidx : res (list typ * iset) :=
          match lidx with
          | [] => OK ([], empty)
          | idx :: lidx =>
              dop (t,  ne1) <- type_expression f idx;
              dop (lt, ne2) <- type_expr_list lidx;
              OK (t :: lt, PTree_union ne1 ne2)
          end in
        dop (lt, ne) <- type_expr_list lidx;
        if list_beq _ typ_beq lt (repeat Tuint64 (length lt)) then OK ne
        else Error WrongTypeIndex
      else Error WrongNbOfIndices
  end.

Definition type_syn_path_elem_list (f: fundef) :=
  fix type_syn_path_elem_list (t: typ)
      (llsz: list (list expr)) (llsz': list (list ident))
      (sp: list syn_path_elem) {struct sp} :=
  match sp, t, llsz, llsz' with
  | [], _, _, _ => OK (t, empty)
  | e :: sp, Tarr t, lsz :: llsz, [] =>
      do ne1 <- type_syn_path_elem f lsz e;
      dop (t, ne2) <- type_syn_path_elem_list t llsz [] sp;
      OK (t, PTree_union ne1 ne2)
  | e :: sp, Tarr t, lsz :: llsz, lsz' :: llsz' =>
      do ne1 <- type_syn_path_elem f lsz e;
      dop (t, ne2) <- type_syn_path_elem_list t llsz llsz' sp;
      let ne2' := set_list lsz' tt ne2 in
      OK (t, PTree_union ne1 ne2')
  | _, _, _, _ => Error WrongPathLength
  end.

(* Definition test :=
  Ebinop_arith Oadd OInt
    (Eacc (1%positive, [Scell [Eunop Oneg OInt64 (Eacc (2%positive, []));
                               Ecast (Eacc (3%positive, [])) (Tfloat F32) (Tint64 Unsigned)];
                        Scell [Eacc (5%positive, [])]]))
    (Eacc (4%positive, [])).

Definition f := Eval cbv in make_function
  {| sig_args := []; sig_res := Tvoid |}
  [] [1; 2; 3; 4; 5; 6; 7]%positive
  Sskip
  (PTree.set 1 (Tarr (Tarr (Tint I32 Signed)))
    (PTree.set 2 (Tint64 Unsigned)
      (PTree.set 3 (Tfloat F32)
        (PTree.set 4 (Tint I32 Signed)
          (PTree.set 5 (Tint64 Unsigned)
            (PTree.set 6 (Tint64 Unsigned)
              (PTree.set 7 (Tint64 Unsigned) (PTree.empty _))))))))
  (PTree.set 1 [[Eacc (6, []); Eacc (6, [])]; [Eacc (6, [])]]
    (PTree.set 2 []
      (PTree.set 3 []
        (PTree.set 4 []
          (PTree.set 5 []
            (PTree.set 6 []
              (PTree.set 7 [] (PTree.empty _))))))))%positive
  (PTree.set 1 [[7; 7]; [7]]
    (PTree.set 2 []
      (PTree.set 3 []
        (PTree.set 4 []
          (PTree.set 5 []
            (PTree.set 6 []
              (PTree.set 7 [] (PTree.empty _))))))))%positive
  (PTree.set 1 Shared
    (PTree.set 2 Shared
      (PTree.set 3 Shared
        (PTree.set 4 Shared
          (PTree.set 5 Shared
            (PTree.set 6 Shared
              (PTree.set 7 Shared (PTree.empty _)))))))).

Definition get_res {A: Type} (r: res A) {x: A} (eq: r = OK x) : A := x.

Definition f' := get_res f eq_refl.

Definition r :=
  get_res (type_expression (Internal f') test) eq_refl.
Definition t := Eval cbv in fst r.
Definition ne := Eval cbv in snd r.

Compute (PTree.get 6 ne). *)

Definition type_syn_path (f: fundef) (p: syn_path) : res (typ * iset) :=
  let (i, sp) := p in
  do** t    <- [UndefVariableWithName i] (fd_tenv f)!i;
  do** llsz <- [NoSizeVariablesWithName i] (fd_szenv f)!i;
  let llsz' := match f with
                | Internal f => match (fn_szenv' f)!i with
                                | Some llsz' => llsz'
                                | None => []
                                end
                | _ => []
                end in
  dop (t, ne) <- type_syn_path_elem_list f t llsz llsz' sp;
  OK (t, PTree.set i tt ne).

Definition type_write_full_syn_path (f: fundef) (p: syn_path) : res (typ * iset) :=
  let (i, sp) := p in
  do** t    <- [UndefVariableWithName i] (fd_tenv f)!i;
  do** llsz <- [NoSizeVariablesWithName i] (fd_szenv f)!i;
  let llsz' := match f with
                | Internal f => match (fn_szenv' f)!i with
                                | Some llsz' => llsz'
                                | None => []
                                end
                | _ => []
                end in
  if Nat.eqb (length sp) (length llsz) then
    dop (t, ne) <- type_syn_path_elem_list f t llsz llsz' sp;
    match sp with
    | [] => OK (t, ne)
    | _  => OK (t, PTree.set i tt ne)
    end
  else Error FullPathNeeded.

Lemma type_write_full_syn_path_type_syn_path:
  forall f ne i l t,
    type_write_full_syn_path f (i, l) = OK (t, ne) ->
    ne!i = Some tt ->
    type_syn_path f (i, l) = OK (t, ne).
Proof.
  intros * => + T. rewrite/type_write_full_syn_path/type_syn_path.
  case (fd_tenv f)!i =>> //. case (fd_szenv f)!i =>> //=.
  case Nat.eqb => //.
  case type_syn_path_elem_list => [[]|] > //=.
  case: l =>> // [-> ->]; by rewrite PTree.gsident.
Qed.

Fixpoint type_syn_path_list (f: fundef) (lp: list syn_path) : res (list typ * iset) :=
  match lp with
  | [] => OK ([], empty)
  | p :: lp =>
      do r  <- type_syn_path f p;
      do lr <- type_syn_path_list f lp;
      let (t, ne)   := r in
      let (lt, ne') := lr in
      OK (t :: lt, PTree_union ne ne')
  end.

Lemma type_syn_path_list_length f:
  forall lp lt ne,
    type_syn_path_list f lp = OK (lt, ne) ->
    length lp = length lt.
Proof.
  elim=> [> [<- _]| > IH] > //=.
  case type_syn_path => [[]|] > //=.
  case T: type_syn_path_list => [[]|] > //= [<- _] /=; by rewrite (IH _ _ T).
Qed.

Definition type_expression_list (f: fundef) :=
  fix type_expression_list (le: list expr) : res (list typ * iset) :=
    match le with
    | [] => OK ([], empty)
    | e :: le =>
        dop (t,  ne)  <- type_expression f e;
        dop (lt, ne') <- type_expression_list le;
        OK (t :: lt, PTree_union ne ne')
    end.

Lemma type_Eacc:
  forall f p,
    type_expression f (Eacc p)
    = dop (t, ne) <- type_syn_path f p;
      if primitive_type t then OK (t, ne)
      else Error ReadingNonPrimitive.
Proof.
  rewrite/type_syn_path/type_syn_path_elem_list/= => ? [i l].
  case (fd_tenv _)!i =>> //. case (fd_szenv _)!i =>> //=.
  set g := fix g _ _ _ (sp: list syn_path_elem) := _.
  by case (g _ _ _) => [[]|].
Qed.

Lemma type_Scell:
  forall f lsz lidx,
    type_syn_path_elem f lsz (Scell lidx) =
    if Nat.eqb (Datatypes.length lidx) (Datatypes.length lsz) then
      dop (lt, ne) <- type_expression_list f lidx;
      if list_beq _ typ_beq lt (repeat Tuint64 (length lt)) then OK ne
      else Error WrongTypeIndex
    else Error WrongNbOfIndices.
Proof. reflexivity. Qed.

End TypeExpression.

Section ArgsChecking.

Local Open Scope option_bool_monad_scope.

Fixpoint try_eval (e: expr) : option value :=
  match e with
  | Eacc _ => None
  | Econst (Cbool b)    => Some (Vbool b)
  | Econst (Cint _ _ n) => Some (Vint n)
  | Econst (Cint64 _ n) => Some (Vint64 n)
  | Econst (Cfloat32 f) => Some (Vfloat32 f)
  | Econst (Cfloat64 f) => Some (Vfloat64 f)
  | Ecast  exp t1 t2 =>
      doo v <- try_eval exp;
      sem_cast v t1 t2
  | Eunop op k exp =>
      doo v <- try_eval exp;
      sem_unop op k v
  | Ebinop_arith op k exp1 exp2 =>
      doo v1 <- try_eval exp1;
      doo v2 <- try_eval exp2;
      sem_binarith_operation op k v1 v2
  | Ebinop_cmp op k exp1 exp2 =>
      doo v1 <- try_eval exp1;
      doo v2 <- try_eval exp2;
      sem_cmp_operation op k v1 v2
  | Ebinop_cmpu op k exp1 exp2 =>
      doo v1 <- try_eval exp1;
      doo v2 <- try_eval exp2;
      sem_cmpu_operation op k v1 v2
  end.

Definition expr_same_result_possible (e1 e2: expr) :=
  match try_eval e1, try_eval e2 with
  | Some v1, Some v2 => value_beq v1 v2
  | _, _ => true
  end.

Lemma expr_same_result_possible_refl:
  forall e, expr_same_result_possible e e.
Proof.
  rewrite/expr_same_result_possible => e. case try_eval => // v.
  by case value_reflect.
Qed.

Definition syn_path_elem_alias_possible (e1 e2: syn_path_elem) :=
  match e1, e2 with
  | Scell le1, Scell le2 =>
      prefix expr_same_result_possible le1 le2 ||
      prefix expr_same_result_possible le2 le1
  end.

Definition syn_path_alias (p1 p2: syn_path) :=
  let (id1, pl1) := p1 in
  let (id2, pl2) := p2 in
  Pos.eqb id1 id2 &&
  (prefix syn_path_elem_alias_possible pl1 pl2 ||
   prefix syn_path_elem_alias_possible pl2 pl1).

Lemma syn_path_alias_comm:
  forall p1 p2, syn_path_alias p1 p2 = syn_path_alias p2 p1.
Proof.
  move=> [i1 l1] [i2 l2] /=.
  case Pos.eqb_spec => //= [<-|Hneq].
  - rewrite Pos.eqb_refl /=. by rewrite orb_comm.
  - rewrite (proj2 (Pos.eqb_neq _ _)) //. by apply not_eq_sym.
Qed.

Lemma syn_path_alias_refl:
  forall p, syn_path_alias p p.
Proof.
  move=> [i l] /=. rewrite Pos.eqb_refl /=. rewrite orb_diag.
  elim/list_ind: l => //= - [lidx] l ->. rewrite andbT /= orb_diag.
  elim/list_ind: lidx => //= e lidx ->. rewrite andbT.
  by apply: expr_same_result_possible_refl.
Qed.

Definition enough_permission {A: Type} (pe0 pe: penv)
                             (pp: list ((ident * A) * ident)) :=
  forallb (fun '((i0, _), i) =>
             match pe0!i0, pe!i with
             | Some p1, Some p2 => p2 <=& p1
             | _, _ => false
             end) pp.

Definition all_arrays {A: Type} (type: tenv -> A -> option typ)
                      (f: fundef) (pp: list (A * ident)) : bool :=
  forallb (fun '(p, _) =>
    match type (fd_tenv f) p with
    | Some (Tarr _) => true
    | _ => false
    end) pp.

Definition check_szvars_structure (sze0: szenvi) (sze: szenve)
                                  (ap: list (syn_path * ident)) : bool :=
  forallb (fun '(p, i) =>
    dob llsz0 <- get_szenv_syn_path sze0 p;
    dob llsz  <- sze!i;
    forallb2 (fun lsz0 lsz => Nat.eqb (length lsz0) (length lsz)) llsz0 llsz
  ) ap.

Local Open Scope error_monad_scope.

Definition check_args (f0: function) (ne: iset) (pe: penv) (sze: szenve)
                      (args: list syn_path) (params: list ident) : res unit :=
  match args_params params args with
  | Some pp =>
      let marrs := mut_args pe pp in
      let oarrs := own_args pe pp in
      do x <- match oarrs with
              | [] => OK tt
              | _ => Error UnsupportedOwnershipTransfer
              end;
      do tt <- if all_arrays get_tenv_syn_path f0 marrs then OK tt
               else Error OnlyArrayMutBorrow;
      if check_szvars_structure (fn_szenv' f0) sze pp then
        if forallb (fun '((_, l), _) => list_beq _ syn_path_elem_beq l []) oarrs then
          if negb (args_aliasing syn_path_alias args (marrs ++ oarrs)) then
            if enough_permission (fn_penv f0) pe pp then
              if existsb (fun '(i, _, _) =>
                            match ne!i with Some tt => true | _ => false end)
                         (own_args pe pp) then
                Error OwnershipTransferNeededLater
              else OK tt
            else
              Error InvalidPermission
          else
            Error PossibleAlias
        else
          Error NonEmptyTransferedPath
      else
        Error MismatchArrayStructure
  | None => Error WrongNbOfArguments
  end.

End ArgsChecking.

Section WellFormedness.

Inductive wf_ret := Normal | Exit (n: nat) | Return.

Scheme Equality for wf_ret.

Lemma wf_ret_reflect:
  forall x y, reflect (x = y) (wf_ret_beq x y).
Proof.
  move=>>; case H: wf_ret_beq.
  - apply ReflectT; by move/internal_wf_ret_dec_bl: H.
  - apply ReflectF; move/internal_wf_ret_dec_lb; by rewrite H.
Qed.

Fixpoint possible_results (s: stmt) :=
  match s with
  | Sskip | Sassign _ _ | Scall _ _ _ | Sassert _ => [Normal]
  | Salloc _ | Sfree _ => [Normal]
  | Sexit n => [Exit n]
  | Sreturn _ | Serror => [Return]
  | Sseq s1 s2 =>
    let p := possible_results s1 in
    if in_dec wf_ret_eq_dec Normal p
    then (remove wf_ret_eq_dec Normal p) ++ possible_results s2
    else p
  | Sifthenelse _ s1 s2 =>
    possible_results s1 ++ possible_results s2
  | Sloop s => remove wf_ret_eq_dec Normal (possible_results s)
  | Sblock s =>
    map (fun r => match r with
                  | Exit O => Normal
                  | Exit (S n) => Exit n
                  | r => r
                  end) (possible_results s)
  end.

Definition no_normal_or_exit out :=
  negb (existsb (fun r => match r with Normal | Exit _ => true | _ => false end) out).

Definition well_formed s : bool := no_normal_or_exit (possible_results s).

Definition well_formed_function (f: function) := well_formed (fn_body f).

End WellFormedness.

Section TypeStatements.

Let empty := empty_iset.

Definition copy_ptree_at {A: Type} (i: positive) (t t': PTree.t A) :=
  match t!i with
  | Some v => PTree.set i v t'
  | None   => t'
  end.

Definition check_writable (pe: penv) (p: ident * list syn_path_elem) :=
  let (i, pl) := p in
  match pe!i with
  | Some Owned | Some Mutable  => OK tt
  | Some _ => Error WritingNotAllowed
  | None   => Error NoPermission
  end.

Local Open Scope error_monad_scope.
Local Open Scope option_error_monad_scope.

Fixpoint type_statement (ge: genv) (f: function) (d: nat) (s: stmt)
                        (ne_n: iset) (ne_e: list iset) (ne_r: iset) :=
  match s with
  | Sskip => OK ne_n
  | Sassign ((i, pl) as p) exp =>
    dop (t', ne1) <- type_write_full_syn_path f p;
    do tt <- check_writable f.(fn_penv) (i, pl);
    dop (t,  ne2)  <- type_expression f exp;
    if primitive_type t then
      if typ_beq t t' then
        let ne := PTree_union ne1 ne2 in
        let ne_n := match pl with [] => PTree.remove i ne_n | _ => ne_n end in
        OK (PTree_union ne ne_n)
      else Error InvalidWritingType
    else Error WritingNonPrimitive
  | Salloc i =>
    do** t  <- [UndefVariableWithName i] f.(fn_tenv)!i;
    do*  l  <- f.(fn_szenv)!i;
    do*  l' <- f.(fn_szenv')!i;
    do*  p  <- f.(fn_penv)!i;
    match t, l, l', p with
    | Tarr t, [[sze]], [[isz]], Owned =>
      dop (tsze, ne) <- type_expression f sze;
      if typ_beq tsze Tuint64 then
        if primitive_type t && negb (typ_beq t Tvoid) then
          OK (PTree_union ne (PTree.remove i (PTree.remove isz ne_n)))
        else
          Error WrongContentTypeAlloc
      else Error WrongTypeSizeExpr
    | Tarr _, [[_]], [[_]], _ => Error OnlyArrayAlloc
    | Tarr _, _, _, _ => Error OnlyPrimArrayAlloc
    | _, _, _, _ => Error WrongTypeAlloc
    end
  | Sfree i =>
    do** t <- [UndefVariableWithName i] f.(fn_tenv)!i;
    do*  l  <- f.(fn_szenv)!i;
    do*  l' <- f.(fn_szenv')!i;
    do*  p <- f.(fn_penv)!i;
    do  tt <- ne_n@i;
    if (forallb (forallb (fun sz => match ne_n!sz with None => true | _ => false end))) l' then
      match t, l, l', p with
      | Tarr t, [[_]], [[isz]], Owned =>
        if primitive_type t then
          OK (PTree.set i tt (PTree.set isz tt ne_n))
        else
          Error WrongContentTypeFree
      | Tarr _, [[_]], [[_]], _ => Error OnlyArrayFree
      | Tarr _, _, _, _ => Error OnlyPrimArrayFree
      | _, _, _, _ => Error OnlyArrayFree
      end
    else
      Error FreeUsedLater
  | Scall None idf args =>
    do** fd <- [UndefFunction] ge!idf;
    dop (tl, ne) <- type_syn_path_list f args;
    do tt <- check_args f ne_n (fd_penv fd) (fd_szenv fd) args (fd_params fd);
    if list_beq _ typ_beq tl (sig_args (fd_sig fd)) then
      OK (PTree_union ne ne_n)
    else Error WrongTypeArguments
  | Scall (Some idvar) idf args =>
    do tt <- check_writable f.(fn_penv) (idvar, []);
    do** fd <- [UndefFunction] ge!idf;
    dop (tl, ne) <- type_syn_path_list f args;
    do** tvar <- [UndefVariableWithName idvar] f.(fn_tenv)!idvar;
    do tt <- check_args f ne_n (fd_penv fd) (fd_szenv fd) args (fd_params fd);
    if typ_beq tvar (fd_sig fd).(sig_res) then
      if list_beq _ typ_beq tl (fd_sig fd).(sig_args) then
        OK (PTree_union ne (PTree.remove idvar ne_n))
      else Error WrongTypeArguments
    else Error WrongTypeReturnVar
  | Sreturn None =>
    if typ_beq f.(fn_sig).(sig_res) Tvoid then OK ne_r
    else Error NonVoidReturnType
  | Sreturn (Some exp) =>
    dop (texp, ne) <- type_expression f exp;
    if typ_beq f.(fn_sig).(sig_res) texp then OK (PTree_union ne ne_r)
    else Error WrongTypeReturn
  | Sseq s1 s2 =>
    do ne_n' <- type_statement ge f d s2 ne_n ne_e ne_r;
    type_statement ge f d s1 ne_n' ne_e ne_r
  | Sassert c =>
    dop (tc, ne) <- type_expression f c;
    match tc with
    | Tbool => OK (PTree_union ne_n ne)
    | _ => Error WrongTypeAssert
    end
  | Sifthenelse c s1 s2 =>
    dop (tc, ne) <- type_expression f c;
    match tc with
    | Tbool =>
      do ne1 <- type_statement ge f d s1 ne_n ne_e ne_r;
      do ne2 <- type_statement ge f d s2 ne_n ne_e ne_r;
      OK (PTree_union ne (PTree_union ne1 ne2))
    | _ => Error WrongTypeConditional
    end
  | Sloop s =>
    do ne1 <- type_statement ge f d s empty ne_e ne_r;
    (* We type s two times, to ensure that no necessary variable is freed
       while the loop continues. *)
    do ne2 <- type_statement ge f d s ne1 ne_e ne_r;
    OK ne2
  | Sblock s => type_statement ge f (S d) s ne_n (ne_n :: ne_e) ne_r
  | Sexit n =>
    if Nat.ltb n d then OK (nth n ne_e empty)
    else Error InvalidExit
  | Serror => OK empty
  end.

End TypeStatements.

Section TypeProgram.

Local Open Scope error_monad_scope.

Definition build_needed_env (l: list ident) :=
  set_list l tt (PTree.empty unit).

Lemma build_needed_env_spec:
  forall l i, (build_needed_env l)!i = Some tt <-> In i l.
Proof. move=> l i; rewrite/build_needed_env; by case case_set_list. Qed.

Definition define_szenvi_vars (ne: iset) (sze: szenvi) (vars: list ident): iset :=
  fold_left (fun ne i =>
    match sze!i with
    | Some llsz =>
      fold_left (fun ne lsz =>
        fold_left (fun ne sz => PTree.remove sz ne) lsz ne
      ) llsz ne
    | None => ne
    end) vars ne.

Lemma define_szenvi_vars_spec sze:
  forall vars ne,
    (forall i llsz lsz sz,
       In i vars ->
       sze!i = Some llsz ->
       In lsz llsz ->
       In sz lsz ->
       (define_szenvi_vars ne sze vars)!sz = None) /\
    (forall i, ne!i = None -> (define_szenvi_vars ne sze vars)!i = None).
Proof.
  elim/list_ind => //= i vars IH ne. split.
  - move=> j llsz lsz sz. case=> [<- ->|]. 2: by apply (fst (IH _)).
    move=> Hllsz Hlsz. apply: (snd (IH _)). move: Hllsz Hlsz.
    set F := fold_left _ _ _.
    apply: (@proj1 _ (forall i, ne!i = None -> F!i = None)). rewrite/F {F}.
    elim/list_ind: llsz ne {IH} => //= lsz' llsz IH ne. split.
    + case=> [-> Hlsz|]. 2: by apply (fst (IH _)).
      apply: (snd (IH _)). move: Hlsz.
      set F := fold_left _ _ _.
      apply: (@proj1 _ (forall i, ne!i = None -> F!i = None)). rewrite/F {F}.
      elim/list_ind: lsz ne {IH} => //= sz' lsz IH ne. split.
      * case=> [->|]. 2: by apply (fst (IH _)).
        apply: (snd (IH _)). exact: PTree.grs.
      * move=> k H. apply: (snd (IH _)).
        rewrite PTree.grspec H. by case PTree.elt_eq.
    + move=> k H. apply: (snd (IH _)).
      elim/list_ind: lsz' ne H {IH} => //= sz' lsz' IH ne H. apply: IH.
      rewrite PTree.grspec H. by case PTree.elt_eq.
  - move=> k H. apply: (snd (IH _)). case sze!_ => // llsz.
    elim/list_ind: llsz ne H {IH} => //= lsz llsz IH ne H. apply: IH.
    elim/list_ind: lsz ne H => //= sz lsz IH ne H. apply: IH.
    rewrite PTree.grspec H. by case PTree.elt_eq.
Qed.

Lemma define_szenvi_vars_spec' sze:
  forall vars ne i,
    (define_szenvi_vars ne sze vars)!i = None ->
    ne!i = None \/
    exists j llsz lsz,
      In j vars /\
      sze!j = Some llsz /\
      In lsz llsz /\
      In i lsz.
Proof.
  elim/list_ind => //=.
  - move=>> ->. by left.
  - move=> i vars IH ne j. case Si: sze!i => [llsz|].
    2: { case/IH. by left. intros (? & ? & ? & ? & ? & ? & ?).
         right. repeat eexists. right. all: eassumption. }
    case/IH.
    2: { intros (? & ? & ? & ? & ? & ? & ?).
         right. repeat eexists. right. all: eassumption. }
    move: Si. rewrite -{1}(app_nil_l llsz).
    elim/list_ind: llsz [] ne {IH} => //=.
    + move=>> _ ->. by left.
    + move=> lsz llsz IH llsz0 ne /[dup] Si.
      change (?a :: ?b) with ([a] ++ b).
      rewrite app_assoc. move/IH/[apply]. case.
      2: { intros (? & ? & ? & ? & ? & ? & ?).
           right. repeat eexists. all: eassumption. }
      move: Si. rewrite -{1}(app_nil_l lsz). elim/list_ind: lsz [] ne {IH} => //=.
      * move=>> _ ->. by left.
      * move=> sz lsz IH lsz0 ne /[dup] Si.
        change (?a :: ?b) with ([a] ++ b) at 2.
        rewrite app_assoc. move/IH/[apply]. case.
        2: { intros (? & ? & ? & ? & ? & ? & ?).
             right. repeat eexists. all: eassumption. }
        case (Pos.eqb_spec j sz) => [T|NEQ]. rewrite -T {T} in Si.
        ** move=> _. right. repeat eexists.
           by left. exact Si. by apply in_elt. by apply in_elt.
        ** rewrite PTree.gro //. by left.
Qed.

Definition new_fundef_ne (fd: fundef) :=
  match fd with
  | Internal f => build_needed_env (fn_vars f)
  | External _ => PTree.empty unit
  end.

Definition type_size_expressions fd :=
  let ne := new_fundef_ne fd in
  let vars := match fd with
              | Internal f => fn_vars f
              | External _ => []
              end in
  forallb (fun i =>
    match (fd_szenv fd)!i with
    | Some llsz =>
        forallb (forallb (fun sz => match type_expression fd sz with
                                    | OK (Tint64 Unsigned, ne) =>
                                      forallb (fun i => match ne!i with
                                                        | Some tt => false
                                                        | None => true
                                                        end) vars
                                    | _ => false
                                    end)) llsz
    | _ => true
    end) (fd_params fd).

Local Open Scope option_error_monad_scope.

Definition all_params_size_variables (params: list ident) (sze: szenvi) :=
  fold_right_res (fun l i =>
    do* llsz <- sze!i;
    OK (fold_right (@app _) l llsz)
  ) params [].

Definition type_function (ge: genv) (f: function) :=
  do ne <- type_statement ge f 0 (fn_body f) empty_iset [] empty_iset;
  do szvars <- all_params_size_variables (fn_params f) (fn_szenv' f);
  let nep := set_list (fn_params f ++ szvars) tt empty_iset in
  if incl (fun _ _ => true) ne nep then
    if well_formed_function f then OK tt
    else Error MissingReturn
  else Error VarUsedBeforeInit.

Fixpoint type_genv' (ge: genv) (fds: list (ident * fundef)) :=
  match fds with
  | [] => OK tt
  | (_, Internal f) :: fds =>
      do tf <- type_function ge f;
      type_genv' ge fds
  | (_, External _) :: fds => type_genv' ge fds
  end.

Definition type_fundef (ge: genv) (fd: fundef) :=
  if type_size_expressions fd then
    match fd with
    | Internal f => type_function ge f
    | External _ => OK tt
    end
  else Error WrongTypeSizeExpr.

Fixpoint type_fundefs (ge: genv) (fds: list (ident * fundef)) :=
  match fds with
  | [] => OK tt
  | fd :: fds =>
    do r <- type_fundef ge (snd fd);
    type_fundefs ge fds
  end.

Definition type_program (p: program) :=
  type_fundefs (genv_of_program p) (prog_defs p).

End TypeProgram.
