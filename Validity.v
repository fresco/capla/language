Require Import ssreflect ssrbool ssrfun.
Require Import String Coq.Lists.List.
Require Import BinNums BinInt.
Require Import Lia.

Import ListNotations.

Require Import Types Ops BValues SemPath Syntax BEnv Alias ExprSem.
Require Import BUtils ListUtils.

Require Import Integers Floats Maps Errors.
Require AST Events.

Section EVAL_SIZE_EXPR.

Variable e: env.
Variable f: function.

Local Open Scope outcome_scope.

Fixpoint eval_expr_list_to_int64 (le: list expr) : outcome (list int64) :=
  match le with
  | [] => Some []
  | exp :: le => do v  <- eval_expr e f exp;
                 match v with
                 | Vint64 i =>
                    do li <- eval_expr_list_to_int64 le;
                    Val (i :: li)
                 | _ => Stuck
                 end
  end.

Lemma eval_expr_list_to_int64_eq_app:
  forall li1 li2 le,
    eval_expr_list_to_int64 le = Val (li1 ++ li2) ->
    exists le1 le2,
      le = le1 ++ le2 /\
      eval_expr_list_to_int64 le1 = Val li1 /\
      eval_expr_list_to_int64 le2 = Val li2.
Proof.
  elim/list_ind => /=.
  - move=>> H. exists []; by eexists.
  - move=>> IH ? [] //= x >. case X: eval_expr => [[]| |] //=.
    all: case H: eval_expr_list_to_int64 =>> //= [<- T].
    rewrite T {T} in H. have [le1 [le2 [-> [H1 H2]]]] := IH _ _ H.
    exists (x :: le1), le2. repeat split => //=. by rewrite X H1.
Qed.

Lemma eval_expr_list_to_int64_app:
  forall le1 le2 li,
    eval_expr_list_to_int64 (le1 ++ le2) = Val li ->
    exists li1 li2,
      li = li1 ++ li2 /\
      eval_expr_list_to_int64 le1 = Val li1 /\
      eval_expr_list_to_int64 le2 = Val li2.
Proof.
  elim/list_ind => /=.
  - move=> ? li ->. by exists [], li.
  - move=>> IH le2 li. case E: eval_expr => [[| | |i| | |]| |] //=.
    case El: eval_expr_list_to_int64 =>> //= [<-].
    case (IH _ _ El) => li1 [li2] [->] [->] ->.
    by exists (i :: li1), li2.
Qed.

Lemma eval_expr_list_to_int64_length:
  forall le lv,
    eval_expr_list_to_int64 le = Val lv ->
    length le = length lv.
Proof.
  elim/list_ind => [[]|>] //=. case eval_expr => [[]| |] //= >.
  case eval_expr_list_to_int64 =>> //= /(_ _ eq_refl) -> > [<-] //.
Qed.

End EVAL_SIZE_EXPR.

Section VALIDITY_OF_FUNCTION_CALL.

Local Open Scope option_error_monad_scope.
Local Open Scope option_bool_monad_scope.
Local Open Scope error_monad_scope.

Fixpoint args_map (params: list ident) (args: list syn_path)
                : res (PTree.t syn_path) :=
match params, args with
| [], [] => OK (PTree.empty syn_path)
| p :: params, a :: args =>
  do m <- args_map params args;
  OK (PTree.set p a m)
| _, [] => Error [MSG "NB->B: Not enough arguments."]
| [], _ => Error [MSG "NB->B: Too many arguments."]
end.

Lemma args_params_args_map:
  forall params args ap,
    args_params params args = Some ap ->
    exists m, args_map params args = OK m.
Proof.
  elim/list_ind => /=.
  - case=>> //= _. by eexists.
  - move=>> IH [] > //=. case AP: args_params =>> //= _.
    case (IH _ _ AP) =>> -> /=. eauto.
Qed.

Lemma args_map_args_params:
  forall params args m,
    args_map params args = OK m ->
    exists ap, args_params params args = Some ap.
Proof.
  elim/list_ind => /=.
  - case=>> //= _. by eexists.
  - move=>> IH [] > //=. case HM: args_map =>> //= _.
    case (IH _ _ HM) =>> -> /=. eauto.
Qed.

Lemma args_map_spec:
  forall params args m,
    Coqlib.list_norepet params ->
    args_map params args = OK m ->
    forall i x,
      nth_error params i = Some x ->
      exists y, m!x = Some y /\ nth_error args i = Some y.
Proof.
  induction params; simpl; intros * Hnorepet Hargs i x Hnth. now destruct i.
  destruct args. discriminate.
  destruct args_map eqn:Hargs_map; try discriminate.
  revert Hargs; intros [= <-]. destruct i; simpl in *.
  revert Hnth; intros [= <-]. rewrite PTree.gss. eauto.
  inversion_clear Hnorepet. destruct (Pos.eq_dec a x) as [->|Hneq].
  apply nth_error_In in Hnth. contradiction.
  destruct IHparams with (1 := H0) (2 := Hargs_map) (3 := Hnth)
                      as [y [Hmap Hargs]].
  rewrite PTree.gso; eauto.
Qed.

Lemma args_map_spec':
  forall params args m i p,
    args_map params args = OK m ->
    m!i = Some p ->
    exists n, nth_error args n = Some p /\ nth_error params n = Some i.
Proof.
  elim/list_ind => /=.
  - case=> //= > [<-] //.
  - move=> p params IH [|a args] // ? i >.
    move: (IH args). case args_map => //= > {IH} - IH [<-].
    case (Pos.eqb_spec i p) => [->|Hneq].
    rewrite PTree.gss => - [<-]. by exists 0%nat.
    rewrite PTree.gso => //. case/(IH _ _ _ eq_refl) => n. by exists (S n).
Qed.

Lemma get_szenv_syn_path_full {A: Type} (sze: PTree.t (list (list A))):
  forall i l llsz,
    get_szenv_syn_path sze (i, l) = Some llsz ->
    exists llsz0, sze!i = Some (llsz0 ++ llsz) /\ length llsz0 = length l.
Proof.
  move=> i /=. case sze!i => //= /[swap]. elim/list_ind => /=.
  - move=>> [<-]. by exists [].
  - case=> _ /= l IH [] //= >. case/IH =>> [[->] <-]. rewrite app_comm_cons. eauto.
Qed.

Fixpoint expr_subst_idents (m: PTree.t syn_path) (e: expr) : res expr :=
  match e with
  | Eacc (i, []) => do* p <- m!i; OK (Eacc p)
  | Eacc _ => Error [MSG "Only variables are allowed in size expressions."]
  | Econst c => OK (Econst c)
  | Ecast e t1 t2 => do e <- expr_subst_idents m e; OK (Ecast e t1 t2)
  | Eunop op k e => do e <- expr_subst_idents m e; OK (Eunop op k e)
  | Ebinop_arith op k e1 e2 => do e1 <- expr_subst_idents m e1;
                               do e2 <- expr_subst_idents m e2;
                               OK (Ebinop_arith op k e1 e2)
  | Ebinop_cmp op k e1 e2 => do e1 <- expr_subst_idents m e1;
                             do e2 <- expr_subst_idents m e2;
                             OK (Ebinop_cmp op k e1 e2)
  | Ebinop_cmpu op k e1 e2 => do e1 <- expr_subst_idents m e1;
                              do e2 <- expr_subst_idents m e2;
                              OK (Ebinop_cmpu op k e1 e2)
  end.

Fixpoint valid_call_tests_rec (szei_caller: szenvi)
                              (sze_callee: szenve)
                              (m: PTree.t syn_path)
                              (pp: (list (syn_path * ident)))
                              : res (list (expr * ident)) :=
  match pp with
  | [] => OK []
  | (p, i) :: pp =>
      do  tests  <- valid_call_tests_rec szei_caller sze_callee m pp;
      do* llsz   <- get_szenv_syn_path sze_callee (i, []);
      do* llsz0  <- get_szenv_syn_path szei_caller p;
      fold_right2_res (fun tests lsz lsz0 =>
          fold_right2_res (fun tests sz sz0 =>
                    do e <- expr_subst_idents m sz;
                    OK ((e, sz0) :: tests)
                  ) lsz lsz0 tests
          ) llsz llsz0 tests
  end.

Definition valid_call_tests (caller: function) (callee: fundef)
                            (args: list syn_path) :=
  do m <- args_map (fd_params callee) args;
  do** pp <- ["NB->B: Wrong number of arguments."]
               args_params (fd_params callee) args;
  valid_call_tests_rec (fn_szenv' caller) (fd_szenv callee) m pp.

Lemma valid_call_tests_nth_error:
  forall caller callee args tests ap M,
    valid_call_tests caller callee args = OK tests ->
    args_params (fd_params callee) args = Some ap ->
    args_map (fd_params callee) args = OK M ->
    forall n sz sz0,
      nth_error tests n = Some (sz, sz0) ->
      exists q i j k llsz lsz sz' llsz0 lsz0,
        In (q, i) ap /\
        get_szenv_syn_path (fn_szenv' caller) q = Some llsz0 /\
        (fd_szenv callee)!i = Some llsz /\
        nth_error llsz  j = Some lsz  /\ nth_error lsz  k = Some sz'  /\
        nth_error llsz0 j = Some lsz0 /\ nth_error lsz0 k = Some sz0 /\
        expr_subst_idents M sz' = OK sz.
Proof.
  rewrite/valid_call_tests.
  move=> caller callee args0 tests ap M + AP Hmap. rewrite Hmap.
  have:= skipn_O args0. have:= skipn_O (fd_params callee).
  move: {2 3}(fd_params callee) 0%nat {2 3}args0 tests.
  elim/list_ind => //=.
  - move=> ? [] //= > _ _ [<-] /=. by case.
  - move=> p params IH n [|a args] //= tests Sparams Sargs.
    have nth_p := skipn_nth_error _ _ _ _ Sparams.
    have nth_a := skipn_nth_error _ _ _ _ Sargs.
    have {Sparams} - Sparams := skipn_S _ _ _ _ Sparams.
    have {Sargs}   - Sargs   := skipn_S _ _ _ _ Sargs.
    move: {IH} (IH (S n) args). case args_params => //= >.
    case valid_call_tests_rec => [tests0|] //= >.
    move/(_ _ Sparams Sargs eq_refl) => IH.
    case Hllsz:  (fd_szenv callee)!p => [llsz|] //=. case: a nth_a => [i l] nth_a.
    case Hllsz0: (get_szenv_syn_path (fn_szenv' caller) (i, l)) => [llsz0|] //= F.
    move: F. have:= skipn_O llsz0. have:= skipn_O llsz. move: tests.
    elim/list_ind: {2 3}llsz 0%nat {2 3}llsz0.
    + move=> ? [|//] > //= _ _ [<-] //.
    + move=> lsz llsz' IH' k [//|lsz0 llsz0'] tests X1 X2 /= {IH}.
      have K1 := skipn_nth_error _ _ _ _ X1. have K2 := skipn_nth_error _ _ _ _ X2.
      have X1' := skipn_S _ _ _ _ X1 => {X1}. have X2' := skipn_S _ _ _ _ X2 => {X2}.
      case H: (fold_right2_res _ llsz' llsz0' _) => [tests1|] //=.
      have IH := IH' _ _ _ X1' X2' H => {H IH' X1' X2'}. move=> H.
      have:= skipn_O lsz0. have:= skipn_O lsz.
      elim/list_ind: {1 3}lsz 0%nat {1 3}lsz0 tests H.
      * move=> ? [|//] > [<-] _ //.
      * move=> sz lsz' IH' j [//|sz0 lsz0'] tests2 /= + X1 X2.
        have J1 := skipn_nth_error _ _ _ _ X1. have J2 := skipn_nth_error _ _ _ _ X2.
        have X1' := skipn_S _ _ _ _ X1 => {X1}. have X2' := skipn_S _ _ _ _ X2 => {X2}.
        have:= IH' _ _ _ _ X1' X2' => {IH'}. case fold_right2_res => //= >.
        move/(_ _ eq_refl) => H. case S: expr_subst_idents =>> //= [<-].
        case=>> //=. 2: by apply: H. case=> <- <-.
        have IN := nth_error_In _ _ (args_params_nth_error' _ _ _ _ AP nth_a nth_p).
        repeat eexists. exact IN.
        exact Hllsz0. exact Hllsz. exact K1. exact J1. exact K2. exact J2. exact S.
Qed.

Fixpoint int64_of_Vint64 (lv: list value) : option (list int64) :=
  match lv with
  | [] => Some []
  | Vint64 i :: lv =>
    doo li <- int64_of_Vint64 lv;
    Some (i :: li)
  | _ => None
  end.

Lemma int64_of_Vint64_length:
  forall lv li,
    int64_of_Vint64 lv = Some li ->
    length lv = length li.
Proof.
  elim/list_ind => [[]|[] >] //=.
  case int64_of_Vint64 =>> //= /(_ _ eq_refl) => -> > [<-] //.
Qed.

Lemma int64_of_Vint64_eq_app:
  forall li1 li2 lv,
    int64_of_Vint64 lv = Some (li1 ++ li2) ->
    exists lv1 lv2,
      lv = lv1 ++ lv2 /\
      int64_of_Vint64 lv1 = Some li1 /\
      int64_of_Vint64 lv2 = Some li2.
Proof.
  elim/list_ind => /=.
  - move=>> H. exists []; by eexists.
  - move=>> IH ? [|[]] //= i >. case H: int64_of_Vint64 =>> //= [<- T].
    rewrite T {T} in H. have [lv1 [lv2 [-> [H1 H2]]]] := IH _ _ H.
    exists (Vint64 i :: lv1), lv2. repeat split => //=. by rewrite H1.
Qed.

Local Open Scope outcome_scope.

Definition valid_call_tests_values (caller: function) (callee: fundef)
                                   (e: env) (args: list syn_path) :=
  match valid_call_tests caller callee args with
  | OK tests =>
    let (le, lx) := split tests in
    do lnx <- eval_expr_list_to_int64 e caller (map (fun x => Eacc (x, [])) lx);
    do lne <- eval_expr_list_to_int64 e caller le;
    Val (combine lne lnx)
  | Error _ => Stuck
  end.

Definition equal_tests (l: list (int64 * int64)) :=
  forallb (fun '(i1, i2) => Int64.eq i1 i2) l.

End VALIDITY_OF_FUNCTION_CALL.
