#!/bin/bash

SCRIPT_DIR=$( cd -- "$( dirname -- "${BASH_SOURCE[0]}" )" &> /dev/null && pwd )

cd $SCRIPT_DIR

CCOMP="$SCRIPT_DIR/../../ccomp"
GCC="gcc -ftree-vectorize"
CLANG="clang -ftree-vectorize"
ARGS="-O2 -lcblas64"

rm -rf /tmp/benchmarks/*
mkdir -p /tmp/benchmarks
cp *.c /tmp/benchmarks/
cp *.b /tmp/benchmarks/
cd /tmp/benchmarks

for f in *.b; do
  $CCOMP -dbc $f -S
done

sed 's/restrict//' zdotu.c | sed 's/zdotu/zdotu_norestrict/' > zdotu_norestrict.c

ZDOTU="zdotu.b zdotu_unidimensional.b zdotu_assert.b test_zdotu.c"
ZDOTUC="${ZDOTU//.b/.c} zdotu_norestrict.c"

$GCC   $ZDOTUC $ARGS -o test_zdotu_gcc
$CLANG $ZDOTUC $ARGS -o test_zdotu_clang
$CCOMP $ZDOTU  $ARGS -o test_zdotu_ccomp

SAXPY="saxpy.b saxpy_assert.b saxpy_unrolling.b saxpy_unrolling_assert.b test_saxpy.c"
SAXPYC="${SAXPY//.b/.c}"

$GCC   $SAXPYC $ARGS -o test_saxpy_gcc
$CLANG $SAXPYC $ARGS -o test_saxpy_clang
$CCOMP $SAXPY  $ARGS -o test_saxpy_ccomp

SGEMV="sgemv.b sgemv_assert.b test_sgemv.c"
SGEMVC="${SGEMV//.b/.c}"

$GCC   $SGEMVC $ARGS -o test_sgemv_gcc
$CLANG $SGEMVC $ARGS -o test_sgemv_clang
$CCOMP $SGEMV  $ARGS -o test_sgemv_ccomp

DGEMV="dgemv.b dgemv_assert.b test_dgemv.c"
DGEMVC="${DGEMV//.b/.c}"

$GCC   $DGEMVC $ARGS -o test_dgemv_gcc
$CLANG $DGEMVC $ARGS -o test_dgemv_clang
$CCOMP $DGEMV  $ARGS -o test_dgemv_ccomp

DTRSV="dtrsv.b dtrsv_assert.b test_dtrsv.c"
DTRSVC="${DTRSV//.b/.c}"

$GCC   -DT="CblasNoTrans" $DTRSVC $ARGS -o test_dtrsv_n_gcc
$CLANG -DT="CblasNoTrans" $DTRSVC $ARGS -o test_dtrsv_n_clang
$CCOMP -DT="CblasNoTrans" $DTRSV  $ARGS -o test_dtrsv_n_ccomp

$GCC   -DT="CblasTrans" $DTRSVC $ARGS -o test_dtrsv_t_gcc
$CLANG -DT="CblasTrans" $DTRSVC $ARGS -o test_dtrsv_t_clang
$CCOMP -DT="CblasTrans" $DTRSV  $ARGS -o test_dtrsv_t_ccomp

echo "------------------- ZDOTU -------------------"

./test_zdotu_gcc
./test_zdotu_clang
./test_zdotu_ccomp

echo "------------------- SAXPY -------------------"

./test_saxpy_gcc
./test_saxpy_clang
./test_saxpy_ccomp

echo "------------------- SGEMV -------------------"

./test_sgemv_gcc
./test_sgemv_clang
./test_sgemv_ccomp

echo "------------------- DGEMV -------------------"

./test_dgemv_gcc
./test_dgemv_clang
./test_dgemv_ccomp

echo "----------------- DTRSV (N) -----------------"

./test_dtrsv_n_gcc
./test_dtrsv_n_clang
./test_dtrsv_n_ccomp

echo "----------------- DTRSV (T) -----------------"

./test_dtrsv_t_gcc
./test_dtrsv_t_clang
./test_dtrsv_t_ccomp

