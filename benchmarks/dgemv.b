fun b_dgemv(
  trans: [u8; 1],
  m n: i32,
  alpha: f64,
  a:     [f64; (u64) n, (u64) lda], lda: i32,
  x:     [f64; (u64) (1 + (m - 1) * incx)], incx: i32,
  beta: f64,
  y: mut [f64; (u64) (1 + (n - 1) * incy)], incy: i32)
{
  assert (trans[0] == 78 || trans[0] == 84 || trans[0] == 67);
  assert (trans[0] != 78); // cannot implement N operation now
  assert (m >= 0);
  assert (n >= 0);
  assert (lda >= m);
  assert (incx >= 0);
  assert (incy >= 0);

  let lenx: i32 = 0; let leny: i32 = 0;
  let kx: i32 = 0; let ky: i32 = 0;
  let temp: f64 = 0.;
  let jx: i32 = 0; let jy: i32 = 0;
  let ix: i32 = 0; let iy: i32 = 0;

  if (m == 0 || n == 0 || alpha == 0. || beta == 1.) return;

  if (trans[0] == 78) { // == 'N'
    lenx = n;
    leny = m;
  } else {
    lenx = m;
    leny = n;
  }

  if (incx > 0) { kx = 0; }
  else { kx = - (lenx - 1) * incx; }

  if (incy > 0) { ky = 0; }
  else { ky = - (leny - 1) * incy; }

  if (beta != 1.) {
    if (incy == 1) {
      if beta == 0. {
        for i: u32 = 0 .. (u32) leny {
          y[i] = 0.;
        }
      } else {
        for i: u32 = 0 .. (u32) leny {
          y[i] = beta * y[i];
        }
      }
    } else {
      iy = ky;
      if beta == 0. {
        for i: i32 = 0 .. leny {
          y[iy] = 0.;
          iy = iy + incy;
        }
      } else {
        for i: i32 = 0 .. leny {
          y[iy] = beta * y[iy];
          iy = iy + incy;
        }
      }
    }
  }
  if (alpha == 0.) return;
  jy = ky;
  if (incx == 1) {
    for j: u32 = 0 .. (u32) n {
      temp = 0.;
      for i: u32 = 0 .. (u32) m {
        temp = temp + a[j, i] * x[i];
      }
      y[jy] = y[jy] + alpha * temp;
      jy = jy + incy;
    }
  } else {
    for j: u32 = 0 .. (u32) n {
      temp = 0.;
      ix = kx;
      for i: u32 = 0 .. (u32) m {
        temp = temp + a[j, i] * x[ix];
        ix = ix + incx;
      }
      y[jy] = y[jy] + alpha * temp;
      jy = jy + incy;
    }
  }

  return;
}