fun b_dtrsv_assert(
  uplo trans diag: u8,
  n: i32,
  a:     [f64; (u64) n, (u64) lda], lda: i32,
  x: mut [f64; (u64) (1 + (n - 1) * incx)], incx: i32)
{
  assert (uplo == 85 || uplo == 76);
  assert (trans == 78 || trans == 84 || trans == 67);
  assert (diag == 85 || diag == 78);
  assert (n >= 0);
  assert (lda >= n);
  assert (incx != 0);

  if (n == 0) return;

  let nounit: bool = diag == 78;

  let kx: i32 = 0, jx: i32 = 0, ix: i32 = 0;
  let temp: f64 = 0.;
  let j: u64 = 0, i: u64 = 0;

  if incx <= 0 { kx = - (n - 1) * incx; }
  else if incx != 1 { kx = 0; }

  if trans == 78 { // trans == 'N'
    if uplo == 85 { // uplo == 'U'
      if incx == 1 {
        assert ((u64) (1 + (n - 1) * incx) == (u64) n);
        for jp: u64 = 0 .. (u64) n {
          j = (u64) (n - 1) - jp;
          assert (j < (u64) lda);
          if x[j] != 0. {
            if nounit { x[j] = x[j] / a[j, j]; }
            temp = x[j];
            for ip: u64 = 0 .. j {
              i = (j - 1) - ip;
              x[i] = x[i] - temp * a[j, i];
            }
          }
        }
      } else {
        jx = kx + (n - 1) * incx;
        for jp: u64 = 0 .. (u64) n {
          j = (u64) (n - 1) - jp;
          assert (j < (u64) lda);
          if x[jx] != 0. {
            if nounit { x[jx] = x[jx] / a[j, j]; }
            temp = x[jx];
            ix = jx;
            for ip: u64 = 0 .. j {
              i = (j - 1) - ip;
              ix = ix - incx;
              x[ix] = x[ix] - temp * a[j, i];
            }
          }
          jx = jx - incx;
        }
      }
    } else {
      if incx == 1 {
        assert ((u64) (1 + (n - 1) * incx) == (u64) n);
        for j: u64 = 0 .. (u64) n {
          assert (j < (u64) lda);
          if x[j] != 0. {
            if nounit { x[j] = x[j] / a[j, j]; }
            temp = x[j];
            for i: u64 = (j + 1) .. (u64) n {
              x[i] = x[i] - temp * a[j, i];
            }
          }
        }
      } else {
        jx = kx;
        for j: u64 = 0 .. (u64) n {
          assert (j < (u64) lda);
          if x[jx] != 0. {
            if nounit { x[jx] = x[jx] / a[j, j]; }
            temp = x[jx];
            ix = jx;
            for i: u64 = (j + 1) .. (u64) n {
              ix = ix + incx;
              x[ix] = x[ix] - temp * a[j, i];
            }
          }
          jx = jx + incx;
        }
      }
    }
  } else {
    if uplo == 85 { // uplo == 'U'
      if incx == 1 {
        assert ((u64) (1 + (n - 1) * incx) == (u64) n);
        for j: u64 = 0 .. (u64) n {
          assert (j < (u64) lda);
          temp = x[j];
          for i: u64 = 0 .. j {
            temp = temp - a[j, i] * x[i];
          }
          if nounit { temp = temp / a[j, j]; }
          x[j] = temp;
        }
      } else {
        jx = kx;
        for j: u64 = 0 .. (u64) n {
          assert (j < (u64) lda);
          temp = x[jx];
          ix = kx;
          for i: u64 = 0 .. j {
            temp = temp - a[j, i] * x[ix];
            ix = ix + incx;
          }
          if nounit { temp = temp / a[j, j]; }
          x[jx] = temp;
          jx = jx + incx;
        }
      }
    } else {
      if incx == 1 {
        assert ((u64) (1 + (n - 1) * incx) == (u64) n);
        for jp: u64 = 0 .. (u64) n {
          j = (u64) (n - 1) - j;
          assert (j < (u64) lda);
          temp = x[j];
          for decr i: u64 = (u64) (n - 1) .. j {
            assert (i < (u64) n);
            temp = temp - a[j, i] * x[i];
          }
          if nounit { temp = temp / a[j, j]; }
          x[j] = temp;
        }
      } else {
        kx = kx + (n - 1) * incx;
        jx = kx;
        for jp: u64 = 0 .. (u64) n {
          j = (u64) (n - 1) - jp;
          assert (j < (u64) lda);
          temp = x[jx];
          ix = kx;
          for decr i: u64 = (u64) (n - 1) .. j {
            temp = temp - a[j, i] * x[ix];
            ix = ix - incx;
          }
          if nounit { temp = temp / a[j, j]; }
          x[jx] = temp;
          jx = jx - incx;
        }
      }
    }
  }
}