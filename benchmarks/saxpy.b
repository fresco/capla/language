fun b_saxpy(
  n: i32, sa: f32,
  sx:     [f32; (u64) (1 + (n - 1) * incx)], incx: i32,
  sy: mut [f32; (u64) (1 + (n - 1) * incy)], incy: i32)
{
  if n <= 0 return;
  if sa == 0.0 return;
  if incx == 1 && incy == 1 {
    for i: i32 = 0 .. n {
      sy[i] = sy[i] + sa * sx[i];
    }
  } else {
    let ix: i32 = 0;
    let iy: i32 = 0;
    if incx < 0 { ix = (-n+1)*incx; }
    if incy < 0 { iy = (-n+1)*incy; }
    for i: i32 = 0 .. n {
      sy[iy] = sy[iy] + sa * sx[ix];
      ix = ix + incx;
      iy = iy + incy;
    }
  }
  return;
}