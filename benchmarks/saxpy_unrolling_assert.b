fun b_saxpy_unrolling_assert(
  n: i32, sa: f32,
  sx:     [f32; (u64) (1 + (n - 1) * incx)], incx: i32,
  sy: mut [f32; (u64) (1 + (n - 1) * incy)], incy: i32)
{
  if n <= 0 return;
  if sa == 0.0 return;
  if incx == 1 && incy == 1 {
    assert (1 + (n - 1) * incx == n);
    assert (1 + (n - 1) * incy == n);
    let m: i32 = n % 4;
    if m != 0 {
      for i: i32 = 0 .. m {
        sy[i] = sy[i] + sa * sx[i];
      }
    }
    if n < 4 return;
    for i: i32 = m .. n step 4 {
      sy[i] = sy[i] + sa * sx[i];
      sy[i+1] = sy[i+1] + sa * sx[i+1];
      sy[i+2] = sy[i+2] + sa * sx[i+2];
      sy[i+3] = sy[i+3] + sa * sx[i+3];
    }
  } else {
    let ix: i32 = 0;
    let iy: i32 = 0;
    if incx < 0 { ix = (-n+1)*incx; }
    if incy < 0 { iy = (-n+1)*incy; }
    for i: i32 = 0 .. n {
      sy[iy] = sy[iy] + sa * sx[ix];
      ix = ix + incx;
      iy = iy + incy;
    }
  }
  return;
}