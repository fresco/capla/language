#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>
#include <cblas_64.h>

#define u64 unsigned long long int
#define i32 signed int
#define MOD 100

#if defined(__clang__)
#define COMP "Clang"
#elif defined(__GNUC__)
#define COMP "GCC"
#else
#define COMP "CompCert"
#endif

extern void b_dgemv(char*, i32, i32, double, double*, i32, double*, i32, double, double*, i32);
extern void b_dgemv_assert(char*, i32, i32, double, double*, i32, double*, i32, double, double*, i32);

void print_int(u64 i) { printf("%llu\n", i); }

double* rand_vector(CBLAS_INT N) {
  double* x = malloc(sizeof(double) * N);
  for (CBLAS_INT i = 0; i < N; i++) {
    x[i] = rand() % MOD;
  }
  return x;
}

double* rand_matrix(CBLAS_INT M, CBLAS_INT N) {
  double* x = malloc(sizeof(double) * M * N);
  for (CBLAS_INT i = 0; i < M; i++)
    for (CBLAS_INT j = 0; j < N; j++)
      x[i * N + j] = rand() % MOD;
  return x;
}

void print_vector(CBLAS_INT N, double* x) {
  printf("[ ");
  for (CBLAS_INT i = 0; i < N; i++)
    printf("%g ", x[i]);
  printf("]");
}

#define ITER 1000000
#define CHECK(N, res, res2)                     \
  for (CBLAS_INT i = 0; i < N; i++) {           \
    if (res[i] != res2[i]) {                    \
      printf("Error: Results are different!");  \
      printf(" res[%d] = %g ", i, res[i]);      \
      printf("res2[%d] = %g ", i, res2[i]);     \
      printf("\n");                             \
      exit(1);                                  \
    }                                           \
  }                                             \

int main() {
  srand(time(NULL));

  CBLAS_INT M = 50;
  CBLAS_INT N = 60;
  clock_t t1, t2;
  double  alpha = (rand() % MOD) + 2;
  double  beta  = (rand() % MOD) + 2;
  double* a = rand_matrix(M, N);
  double* x = rand_vector(M);
  double* y = rand_vector(N);
  double* res = malloc(sizeof(double) * N);
  double* res2 = malloc(sizeof(double) * N);

  /* BLAS */

  memcpy(res, y, sizeof(double) * N);
  t1 = clock();
  for (int i = 0; i < ITER; i++) {
    cblas_dgemv_64(CblasColMajor, CblasTrans, M, N, alpha, a, M, x, 1, beta, res, 1);
  }
  t2 = clock();
  double ref_time = (double)(t2 - t1) / (double)CLOCKS_PER_SEC;

  /* ORIGINAL */

  memcpy(res2, y, sizeof(double) * N);
  t1 = clock();
  for (int i = 0; i < ITER; i++) {
    b_dgemv("T", M, N, alpha, a, M, x, 1, beta, res2, 1);
  }
  t2 = clock();
  printf("%8s -- Original:\t\t", COMP);
  CHECK(N, res, res2);
  printf("%.2lf\n", (double)(t2 - t1) / (double)CLOCKS_PER_SEC / ref_time);

  /* ASSERTIONS ADDED */

  memcpy(res2, y, sizeof(double) * N);
  t1 = clock();
  for (int i = 0; i < ITER; i++) {
    b_dgemv_assert("T", M, N, alpha, a, M, x, 1, beta, res2, 1);
  }
  t2 = clock();
  printf("%8s -- Assertions added:\t", COMP);
  CHECK(N, res, res2);
  printf("%.2lf\n", (double)(t2 - t1) / (double)CLOCKS_PER_SEC / ref_time);

  return 0;
}
