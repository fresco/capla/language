#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cblas_64.h>

#define u64 unsigned long long int
#define i32 signed int
#define MOD 100

#if defined(__clang__)
#define COMP "Clang"
#elif defined(__GNUC__)
#define COMP "GCC"
#else
#define COMP "CompCert"
#endif

// #define CBLAS_INT int64_t

extern void b_zdotu(i32, void*, i32, void*, i32, void*);
extern void b_zdotu_assert(i32, void*, i32, void*, i32, void*);
extern void b_zdotu_uni(i32, void*, i32, void*, i32, void*);
extern void b_zdotu_norestrict(i32, void*, i32, void*, i32, void*);

void print_int(u64 i) { printf("%llu\n", i); }

struct cplx {
  double x; double y;
};

typedef struct cplx cplx;

cplx* rand_vector(CBLAS_INT N) {
  cplx* x = malloc(sizeof(cplx) * N);
  for (CBLAS_INT i = 0; i < N; i++) {
    x[i].x = rand() % MOD;
    x[i].y = rand() % MOD;
  }
  return x;
}

cplx** vector_cplx_to_ptr(CBLAS_INT N, cplx* x) {
  cplx** y = malloc(sizeof(cplx*) * N);
  for (CBLAS_INT i = 0; i < N; i++) {
    y[i] = malloc(sizeof(cplx));
    y[i]->x = x[i].x;
    y[i]->y = x[i].y;
  }
  return y;
}

void print_vector(CBLAS_INT N, cplx* x) {
  printf("[ ");
  for (CBLAS_INT i = 0; i < N; i++)
    printf("(%g, %g) ", x[i].x, x[i].y);
  printf("]");
}

#define ITER 4000000
#define CHECK(res, res2)                     \
  if (res.x != res2.x || res.y != res2.y) {  \
    printf("Error: Results are different!"); \
    printf("(%g, %g)\n", res.x, res.y);      \
    printf("(%g, %g)\n", res2.x, res2.y);    \
    exit(1);                                 \
  }

int main() {
  srand(time(NULL));

  CBLAS_INT N = 900;
  clock_t t1, t2;
  cplx* zx = rand_vector(N);
  cplx* zy = rand_vector(N);
  cplx res; res.x = 0.0; res.y = 0.0;
  cplx res2; res2.x = 0.0; res2.y = 0.0;

  /* BLAS */

  t1 = clock();
  for (int i = 0; i < ITER; i++)
    cblas_zdotu_sub_64(N, zx, 1, zy, 1, &res);
  t2 = clock();
  double ref_time = (double)(t2 - t1) / (double)CLOCKS_PER_SEC;
  // printf("%8s -- BLAS:\t\t", COMP);
  // printf("%f s\n", ref_time);

  /* ORIGINAL */

  t1 = clock();
  for (int i = 0; i < ITER; i++)
    b_zdotu(N, zx, 1, zy, 1, &res2);
  t2 = clock();
  printf("%8s -- Original:\t\t", COMP);
  CHECK(res, res2);
  printf("%.2lf\n", (double)(t2 - t1) / (double)CLOCKS_PER_SEC / ref_time);

  /* RESTRICT REMOVED */

  #if defined(__clang__) || defined(__GNUC__)
  t1 = clock();
  for (int i = 0; i < ITER; i++)
    b_zdotu_norestrict(N, zx, 1, zy, 1, &res2);
  t2 = clock();
  printf("%8s -- No Restrict:\t", COMP);
  CHECK(res, res2);
  printf("%.2lf\n", (double)(t2 - t1) / (double)CLOCKS_PER_SEC / ref_time);
  #endif

  /* UNIDIMENSIONAL */

  t1 = clock();
  for (int i = 0; i < ITER; i++)
    b_zdotu_uni(N, zx, 1, zy, 1, &res2);
  t2 = clock();
  printf("%8s -- Unidimensional:\t", COMP);
  CHECK(res, res2);
  printf("%.2lf\n", (double)(t2 - t1) / (double)CLOCKS_PER_SEC / ref_time);

  /* ASSERTIONS ADDED */

  t1 = clock();
  for (int i = 0; i < ITER; i++)
    b_zdotu_assert(N, zx, 1, zy, 1, &res2);
  t2 = clock();
  printf("%8s -- Assertions added:\t", COMP);
  CHECK(res, res2);
  printf("%.2lf\n", (double)(t2 - t1) / (double)CLOCKS_PER_SEC / ref_time);

  return 0;
}
