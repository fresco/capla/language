fun b_zdotu_uni(n: i32, zx: [f64; (u64) ((1 + (n - 1) * incx) * 2)], incx: i32,
                      zy: [f64; (u64) ((1 + (n - 1) * incy) * 2)], incy: i32,
          res: mut [f64; 2]) {
  res[0] = 0.; res[1] = 0.;
  if n <= 0 return;

  if incx == 1 && incy == 1 {
    // assert (1 + (n - 1) * incx == n);
    // assert (1 + (n - 1) * incy == n);
    for i: i32 = 0 .. (2 * n) step 2 {
      res[0] = res[0] + (zx[i] * zy[i] - zx[i + 1] * zy[i + 1]);
      res[1] = res[1] + (zx[i + 1] * zy[i] + zx[i] * zy[i + 1]);
    }
  } else {
    let ix: i32 = 0;
    let iy: i32 = 0;
    if incx < 0 { ix = (-n+1)*2*incx; }
    if incy < 0 { iy = (-n+1)*2*incy; }

    for i: i32 = 0 .. n {
      res[0] = res[0] + (zx[ix] * zy[iy] - zx[ix + 1] * zy[iy + 1]);
      res[1] = res[1] + (zx[ix + 1] * zy[iy] + zx[ix] * zy[iy + 1]);
      ix = ix + 2 * incx;
      iy = iy + 2 * incy;
    }
  }
}