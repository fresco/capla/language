#!/bin/bash

declare -a groups
declare -a sums
declare -a names

names[0]='Syntax and types'
names[1]='Common semantics definitions and proofs'
names[2]='Semantics of L1'
names[3]='Semantics of L2'
names[4]='Type checking'
names[5]='Type safety'
names[6]='Compilation from L1 to L2'
names[7]='Compilation from L1 to L2 proof'
names[8]='Compilation from L2 to C#minor'
names[9]='Compilation from L2 to C#minor proof'
names[10]='Miscellaneous'

groups[0]='Syntax;Types'
groups[1]='BValues;Ops;SemanticsCommon;SemPath;BEnv;Alias;Validity'
groups[2]='SemanticsNonBlocking;NBfacts;ExprSem'
groups[3]='SemanticsBlocking;Bfacts'
groups[4]='Typing'
groups[5]='Safety'
groups[6]='NBtoB'
groups[7]='NBtoBproof;FloatCastTests'
groups[8]='BtoCSharpMinor'
groups[9]='BtoCSharpMinorproof'
groups[10]='BUtils;ListUtils;Tactics;PTreeaux;BPrinters'

IFS=";"
i=0

total_spec=0
total_proof=0

for group in "${groups[@]}"
do
    read -r -a arr <<< "${group}"
    sum_spec=0
    sum_proof=0

    for file in "${arr[@]}"
    do
        lc=$(coqwc "${file}.v" | tail -n 1 \
                | sed "s/[^0-9 ]*//g" \
                | sed "s/\(^\ \+\)\|\(\ \+$\)//g" | sed "s/\ \+/;/g")
        read -r -a cols <<< ${lc}
        sum_spec=$((sum_spec+cols[0]))
        sum_proof=$((sum_proof+cols[1]))
    done
    sums[$i]="${sum_spec};${sum_proof}"
    total_spec=$((total_spec+sum_spec))
    total_proof=$((total_proof+sum_proof))
    i=$((i+1))
done

printf "%-45s %9s %9s\n" "" "Code/Spec" "Proofs"

lc=$(ocamlwc parser/*.ml parser/*.mll parser/*.mly 2> /dev/null | tail -n 1 \
        | sed "s/[^0-9 ]*//g" \
        | sed "s/\(^\ \+\)\|\(\ \+$\)//g" | sed "s/\ \+/;/g")
read -r -a cols <<< ${lc}
printf "%-45s %9s\n" "Parser, typing and simplifications (OCaml):" ${cols[0]}
total_spec=$((total_spec+cols[0]))

j=0
while (( j < i )) do
    read -r -a cols <<< "${sums[j]}"
    printf "%-45s %9s %9s\n" "${names[j]}:" "${cols[0]}" "${cols[1]}"
    j=$((j+1))
done

printf "%-45s %9s %9s\n" "Total" "${total_spec}" "${total_proof}"

