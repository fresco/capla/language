#!/bin/sh

pandoc -s                                           \
       --syntax-definition misc/capla-syntax.xml    \
       --syntax-definition misc/AST.xml             \
       --highlight-style misc/highlight-style.theme \
       --template misc/template.html                \
       --metadata title="The Capla language"        \
       -f markdown -t html5 README.md -o public/index.html
