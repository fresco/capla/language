open Syntax
open Bast
open Iast
open TranslConsts
open TranslToNB
open AAST
open Berror
open Maps
open BValues
open Diagnostics

module CInt = Integers.Int
module CInt64 = Integers.Int64

type ident = Bast.ident

type penv = Syntax.permission PTree.t

exception WrongNbOfArgs

let rec mut_args (args: liexpr list) (params: ident list)
                   (perms : permission list)
                   : (loc * isyn_path) list =
  match args, params, perms with
  | [], [], _ -> []
  | _, [], _ | [], _, _ -> raise WrongNbOfArgs
  | _, _, [] -> assert false
  | (l, Iacc (i, ls)) :: args, p :: params, Mutable :: perms ->
     (l, (i, ls)) :: mut_args args params perms
  | _ :: args, _ :: params, _ :: perms ->
     mut_args args params perms

let rec try_eval ((l, e): liexpr) : value option =
  match e with
  | Iacc _ -> None
  | Iconst_int (_, I32 (_, n)) -> Some (Vint n)
  | Iconst_int (_, I64 (_, n)) -> Some (Vint64 n)
  | Iconst_float (_, F32 f) -> Some (Vfloat32 f)
  | Iconst_float (_, F64 f) -> Some (Vfloat64 f)
  | Iconst_bool b -> Some (Vbool b)
  | Icast (e, Ttyp t1, Ttyp t2) ->
     begin match try_eval e with
     | Some v -> Ops.sem_cast v t1 t2
     | None -> None
     end
  | Icast _ -> assert false
  | Iunop (op, e, Ttyp t) ->
    let op = transl_unop op in
    let k  = kind_of_typ t in
    begin match try_eval e with
    | Some v -> Ops.sem_unop op k v
    | None -> None
    end
  | Iunop _ -> assert false
  | Ibinop (op, e1, e2, Ttyp (Types.Tint (_, sg) as t))
  | Ibinop (op, e1, e2, Ttyp (Types.Tint64 sg as t)) ->
     begin match try_eval e1, try_eval e2 with
     | None, _ | _, None -> None
     | Some v1, Some v2 ->
        match op with
        | Oband | Obor -> None
        | Oadd | Osub | Omul | Odiv | Omod | Oshl | Oshr | Oand | Oor | Oxor ->
           let op = transl_binop_arith op sg in
           let k  = kind_of_typ t in
           Ops.sem_binarith_operation op k v1 v2
        | Oeq | One | Olt | Ogt | Ole | Oge ->
           match sg with
           | Types.Signed ->
              let op = transl_binop_cmp op in
              let k  = kind_of_typ t in
              Ops.sem_cmp_operation op k v1 v2
           | Types.Unsigned ->
              let op = transl_binop_cmpu op in
              let k  = kind_of_typ t in
              Ops.sem_cmpu_operation op k v1 v2
     end
  | Ibinop (op, e1, e2, Ttyp (Types.Tfloat _ as t)) ->
     begin match try_eval e1, try_eval e2 with
     | None, _ | _, None -> None
     | Some v1, Some v2 ->
        match op with
        | Oband | Obor -> None
        | Oadd | Osub | Omul | Odiv | Omod | Oshl | Oshr | Oand | Oor | Oxor ->
           let op = transl_binop_arith op Types.Signed in
           let k  = kind_of_typ t in
           Ops.sem_binarith_operation op k v1 v2
        | Oeq | One | Olt | Ogt | Ole | Oge ->
           let op = transl_binop_cmp op in
           let k  = kind_of_typ t in
           Ops.sem_cmp_operation op k v1 v2
     end
  | Ibinop (op, e1, e2, Ttyp Types.Tbool) ->
     begin match try_eval e1, try_eval e2 with
     | None, _ | _, None -> None
     | Some v1, Some v2 ->
        match op with
        | Oband ->
           begin match v1, v2 with
           | Vbool b1, Vbool b2 -> Some (Vbool (b1 && b2))
           | _, _ -> None
           end
        | Obor ->
           begin match v1, v2 with
           | Vbool b1, Vbool b2 -> Some (Vbool (b1 && b2))
           | _, _ -> None
           end
        | _ -> None
     end
  | Ibinop _ -> assert false
  | Iecall _ -> None

let expr_same_result_possible e1 e2 =
  match try_eval e1, try_eval e2 with
  | Some v1, Some v2 -> value_beq v1 v2
  | _, _ -> true

let syn_path_elem_alias_possible e1 e2 =
  match e1, e2 with
  | Icell le1, Icell le2 ->
    List.for_all2 expr_same_result_possible le1 le2

let syn_path_alias (i1, ls1) (i2, ls2) =
  Camlcoq.P.eq i1 i2 &&
    (ListUtils.prefix syn_path_elem_alias_possible ls1 ls2
    || ListUtils.prefix syn_path_elem_alias_possible ls2 ls1)

let check_arg_aliasing args (l, (i, ls)) =
  match List.filter (fun (_, (i', ls')) -> syn_path_alias (i, ls) (i', ls')) args with
  | (l1, (i1, ls1)) :: (l2, (i2, ls2)) :: _ ->
    let (l', i', ls') = if (i1, ls1) = (i, ls) then (l2, i2, ls2)
                        else (l1, i1, ls1) in
    error_loc l' (Aliasing (l, (i, ls), (i', ls')))
  | _ -> ()

let check_mut_args_aliasing args marrs =
  List.iter (check_arg_aliasing args) marrs

let check_aliasing (pe: penv) (args: liexpr list) (params: ident list) =
  let args = List.filter_map (function (l, Iacc (i, ls)) -> Some (l, (i, ls))
                                     | _ -> None) args in
  let marrs = ListUtils.filter2 (fun a p -> match PTree.get p pe with
                                            | Some Mutable | Some Owned -> true
                                            | _ -> false) args params
              |> Option.get in
  check_mut_args_aliasing args marrs

let check_invalid_ownership_transfer (pe: penv) (args: liexpr list)
                                     (params: ident list) =
  List.iter2 (fun a p -> match a with
                         | (_, Iacc (i, [])) -> ()
                         | (l, Iacc (i,  _)) ->
                            begin match PTree.get p pe with
                            | Some Owned ->
                               error_loc l PartialOwnershipTransfer
                            | _ -> ()
                            end
                         | _ -> ()) args params

let params_penv g idf =
  match PTree.get idf g with
  | None -> assert false
  | Some (IInternal f)  -> f.ifparams, f.ifpenv
  | Some (IExternal ef) -> ef.ieparams, ef.iepenv

let rec arrays_in_expr (g: ifundef PTree.t) ((l, e) : liexpr)
                       : lisyn_path list * lisyn_path list =
  match e with
  | Iconst_int _ | Iconst_float _ | Iconst_bool _ -> ([], [])
  | Icast (e, _, _) -> arrays_in_expr g e
  | Iacc (_, ls) -> arrays_in_syn_path_elem_list g ls
  | Iunop (_, e, _) -> arrays_in_expr g e
  | Ibinop (_, e1, e2, _) ->
    let a1, m1 = arrays_in_expr g e1 in
    let a2, m2 = arrays_in_expr g e2 in
    a1 @ a2, m1 @ m2
  | Iecall (idf, args) ->
    let params, pe = params_penv g idf in
    let args = List.filter_map (function (l, Iacc (i, ls)) -> Some (l, (i, ls))
                                     | _ -> None) args in
    let marrs = ListUtils.filter2 (fun a p -> match PTree.get p pe with
                                            | Some Mutable | Some Owned -> true
                                            | _ -> false) args params
                |> Option.get in
    args, marrs

and arrays_in_expr_list g le =
  List.fold_right (fun e (arrs, marrs) ->
      let a, m = arrays_in_expr g e in
      a @ arrs, m @ marrs) le ([], [])

and arrays_in_syn_path_elem g e =
  match e with
  | Icell lidx -> arrays_in_expr_list g lidx

and arrays_in_syn_path_elem_list g (ls: isyn_path_elem list) =
  List.fold_right (fun e (arrs, marrs) ->
      let a, m = arrays_in_syn_path_elem g e in
      a @ arrs, m @ marrs) ls ([], [])

let check_undefined_evaluation_order g l le =
  let arrs, marrs = arrays_in_expr_list g le in
  try check_mut_args_aliasing arrs marrs
  with Abort -> fatal_error (file_loc l.loc_file)
                            "There is aliasing between an array passed \
                             to a function as mutable and another path. \
                             The evaluation order is undefined."

let rec check_expr (g: ifundef PTree.t) ((l, e) : liexpr) =
  match e with
  | Iconst_int _ | Iconst_float _ | Iconst_bool _ -> ()
  | Icast (e, _, _) -> check_expr g e
  | Iacc (_, ls) -> check_syn_path_elem_list g ls
  | Iunop (_, e, _) -> check_expr g e
  | Ibinop (_, e1, e2, _) -> check_expr g e1; check_expr g e2
  | Iecall (idf, args) ->
    let params, pe = params_penv g idf in
    check_invalid_ownership_transfer pe args params;
    check_aliasing pe args params

and check_syn_path_elem g = function
  | Icell lidx -> List.iter (check_expr g) lidx

and check_syn_path_elem_list g ls = List.iter (check_syn_path_elem g) ls

let rec check_stmt (g: ifundef PTree.t) ((l, s) : listmt) =
  match s with
  | Iassign ((i, ls), e) ->
    check_expr g (l, Iacc (i, ls));
    check_expr g e;
    check_undefined_evaluation_order g l [(l, Iacc (i, ls)); e]
  | Icall (_, _, args) ->
    check_undefined_evaluation_order g l args
  | Iseq (s1, s2) -> check_stmt g s1; check_stmt g s2
  | Iifthenelse (c, s1, s2) ->
    check_expr g c;
    check_undefined_evaluation_order g l [c];
    check_stmt g s1; check_stmt g s2
  | Ifor (_, _, _, lo, hi, s) ->
    check_expr g lo; check_expr g hi;
    check_undefined_evaluation_order g l [lo; hi];
    check_stmt g s
  | Ifor_step (_, _, _, lo, hi, step, s) ->
    check_expr g lo; check_expr g hi; check_expr g step;
    check_undefined_evaluation_order g l [lo; hi; step];
    check_stmt g s
  | Iwhile (c, s) -> check_expr g c; check_stmt g s
  | Iloop s -> check_stmt g s
  | Ireturn None -> ()
  | Ireturn (Some e) ->
    check_expr g e;
    check_undefined_evaluation_order g l [e]
  | Iblock (_, s) ->
    check_stmt g s
  | _ -> ()

let check_function g f = check_stmt g f.ifbody

let check_fundef g = function
  | IInternal f -> check_function g f
  | IExternal _ -> ()

let genv_of_program p =
  List.fold_left (fun ge (id, fd) -> PTree.set id fd ge) PTree.empty p.ifundefs

let check_program p =
  let g = genv_of_program p in
  List.iter (fun (_, fd) -> check_fundef g fd) p.ifundefs
