module type OrderedType = sig
	type t
	val compare : t -> t -> int
end

module Make (X : OrderedType) : sig
	type t
	type elt = X.t
	val create : unit -> t
	val add : t -> elt -> unit
	val find : t -> elt -> elt
 	val union : t -> elt -> elt -> unit
  val iter : (elt -> unit) -> t -> unit
end = struct
	type elt = X.t
	type t = (elt, elt) Hashtbl.t
	
	let create () = Hashtbl.create 100
	
	let add t x =
		if not (Hashtbl.mem t x) then
			Hashtbl.add t x x
	
	let rec find t x =
		let y = Hashtbl.find t x in
		if x = y then x
		else begin
			let p = find t y in
			Hashtbl.replace t x p;
			p
		end
	
	let union t x y =
		let px = find t x in
		let py = find t y in
		let c  = X.compare px py in
		if c = 0 then ()
		else if c < 0 then
			Hashtbl.replace t y x
		else
			Hashtbl.replace t x y

 let iter f t =
   Hashtbl.iter (fun k _ -> f k) t
end
