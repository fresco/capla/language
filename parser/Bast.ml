open Berror
open Format
open ParserUtils

type ident = SemPath.ident
type tnames_tbl = (ident, string) Hashtbl.t

exception SyntaxError of string

type unop =
| Onotint | Onotbool | Oneg

type binop =
| Oadd | Osub  | Omul | Odiv | Omod
| Oeq  | One   | Olt  | Ole  | Ogt  | Oge
| Oor  | Oxor  | Oand
| Obor | Oband
| Oshl | Oshr

type float_info = {
  sign:    bool;
  intpart: string;
  decpart: string;
  exp:     string
}

type expr =
| Econst_bool        of bool
| Econst_int         of string
| Econst_annot_int   of Types.intsize * Types.signedness * string
| Econst_annot_int64 of Types.signedness * string
| Econst_float       of float_info
| Econst_annot_float of Types.floatsize * float_info
| Eacc               of ident * syn_path_elem list
| Ecast              of lexpr * Types.typ
| Eunop              of unop * lexpr
| Ebinop             of binop * lexpr * lexpr
| Ecall              of ident * lexpr list
and syn_path_elem =
| Scell of lexpr list
and lexpr = loc * expr

(* used in parser *)
type init = Expr of lexpr | Alloc

type syn_path = ident * syn_path_elem list

type stmt =
| Sskip
| Sdecl       of ident * Types.typ option * lexpr list list
| Sassign     of (ident * syn_path_elem list) * lexpr
| Salloc      of ident
| Sfree       of ident
| Sassert     of lexpr
| Scall       of ident option * ident * lexpr list
| Sreturn     of lexpr option
| Sseq        of lstmt * lstmt
| Sifthenelse of lexpr * lstmt * lstmt
| Sfor        of ident * bool * Types.typ option * lexpr * lexpr * lstmt
| Sfor_step   of ident * bool * Types.typ option * lexpr * lexpr * lexpr * lstmt
| Swhile      of lexpr * lstmt
| Sloop       of lstmt
| Sblock      of ident * lstmt
| Scontinue   of ident option
| Sbreak      of ident option
| Sexit       of ident
| Serror
and lstmt = loc * stmt

type fct = {
  floc:    loc;
  fret:    Types.typ;
  fparams: (ident * Types.typ) list;
  fbody:   lstmt;
  fszvars: (ident * lexpr list list) list;
  fperms:  (ident * Syntax.permission) list
}

type ef = {
  eloc:    loc;
  eret:    Types.typ;
  eparams: (ident * Types.typ) list;
  eszvars: (ident * lexpr list list) list;
  eperms:  (ident * Syntax.permission) list;
}

type fundef =
| Internal of fct
| External of ef

type program = {
  ploc:      loc;
  main:      ident;
  fundefs:   (ident * fundef) list
}

let floatsize_of_string = function
  | "32" -> Types.F32
  | "64" -> Types.F64
  | s    -> failwith (Format.sprintf "%s is not a valid float size." s)

let signedness_of_string = function
  | "u" -> Types.Unsigned
  | "i" -> Types.Signed
  | s   -> failwith (Format.sprintf "%s is not a valid signedness." s)

(* let while_to_loop l c s =
  (l, Sloop (
    (l, Sifthenelse (c, s, (l, Sbreak)))
  )) *)

let name_unop = function
  | Oneg -> "-"
  | Onotint -> "~"
  | Onotbool -> "!"

let name_binop = function
  | Oadd -> "+"
  | Osub -> "-"
  | Omul -> "*"
  | Odiv -> "/"
  | Omod -> "%"
  | Oor -> "|"
  | Oand -> "&"
  | Oxor -> "^"
  | Ole -> "<="
  | Olt -> "<"
  | Oge -> ">="
  | Ogt -> ">"
  | Oeq -> "=="
  | One -> "!="
  | Oshl -> "<<"
  | Oshr -> ">>"
  | Oband -> "&&"
  | Obor  -> "||"

let rec pp_ident_list p = function
  | [] -> ()
  | id :: lids -> fprintf p "%s %a" (get_name id) pp_ident_list lids

let rec pp_expr p = function
| Econst_int s | Econst_annot_int (_, _, s)
| Econst_annot_int64 (_, s) -> fprintf p "%s" s
| Econst_float f | Econst_annot_float (_, f) ->
  fprintf p "%s%s.%se%s" (if f.sign then "-" else "")
                 f.intpart f.decpart f.exp
| Econst_bool b -> fprintf p "%s" (if b then "true" else "false")
| Eacc (i, ls)  -> fprintf p "%s%a" (get_name i) pp_syn_path_elem_list ls
| Ecast (e, t)  -> fprintf p "(%a)%a" PrintB.print_type t pp_lexpr e
| Eunop (op, e) -> fprintf p "%s%a" (name_unop op) pp_lexpr e
| Ebinop (op, e1, e2)   -> fprintf p "(%a%s%a)"
                                   pp_lexpr e1 (name_binop op) pp_lexpr e2
| Ecall (idf, args) ->
  fprintf p "%s(%a)" (Camlcoq.extern_atom idf)
          pp_lexprlist (true, args)
and pp_syn_path_elem_list p ls =
  match ls with
  | [] -> ()
  | Scell le :: ls ->
    fprintf p "[%a]" pp_lexprlist (true, le);
    pp_syn_path_elem_list p ls
and pp_lexprlist p (first, l) =
  match l with
  | [] -> ()
  | e :: l -> if not first then fprintf p ",@ ";
              pp_lexpr p e;
              pp_lexprlist p (false, l)
and pp_lexpr p (_, e) = pp_expr p e

let rec pp_expr_list' p (first, l) =
  match l with
  | [] -> ()
  | e :: l -> if not first then fprintf p ",@ ";
              pp_expr p e;
              pp_expr_list' p (false, l)

let pp_expr_list p l = pp_expr_list' p (true, l)
