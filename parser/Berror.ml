type loc = {
  loc_lnum: int;
  loc_cnum: int;
  loc_clen: int;
  loc_file: string
}

let filename = ref ""

let set_filename s = filename := s

let get_filename () = !filename

let loc_of_lexing_pos p1 p2 =
  { loc_lnum = p1.Lexing.pos_lnum;
    loc_cnum = p1.Lexing.pos_cnum - p1.Lexing.pos_bol;
    loc_clen = p2.Lexing.pos_cnum - p1.Lexing.pos_cnum;
    loc_file = !filename }
