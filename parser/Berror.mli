type loc = {
  loc_lnum: int;
  loc_cnum: int;
  loc_clen: int;
  loc_file: string
}

val set_filename : string -> unit
val get_filename : unit -> string

val loc_of_lexing_pos : Lexing.position -> Lexing.position -> loc