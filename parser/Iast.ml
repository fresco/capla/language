open Bast
open Berror
open Types
open Format
open ParserUtils

open Maps
open Diagnostics

type ityp =
  | Tvar of int
  | Ttyp of Types.typ

module type ASTParam = sig
  type int
  type float

  val string_of_int: int -> string
  val string_of_float: float -> string
end

module AST(P: ASTParam) = struct
  type iexpr =
  | Iconst_int   of ityp * P.int
  | Iconst_float of ityp * P.float
  | Iconst_bool  of bool
  | Iacc   of ident * isyn_path_elem list
  | Icast  of liexpr * ityp * ityp
  | Iunop  of Bast.unop * liexpr * ityp
  | Ibinop of Bast.binop * liexpr * liexpr * ityp
  | Iecall of ident * liexpr list
  and liexpr = loc * iexpr
  and isyn_path_elem =
  | Icell of liexpr list

  type isyn_path = ident * isyn_path_elem list

  type lisyn_path = loc * isyn_path

  type istmt =
  | Iskip
  | Idecl       of ident * Types.typ
  | Iassign     of isyn_path * liexpr
  | Iassert     of liexpr
  | Ialloc      of ident
  | Ifree       of ident
  | Icall       of ident option * ident * liexpr list
  | Ireturn     of liexpr option
  | Iseq        of listmt * listmt
  | Iifthenelse of liexpr * listmt * listmt
  | Ifor        of ident * bool * ityp * liexpr * liexpr * listmt
  | Ifor_step   of ident * bool * ityp * liexpr * liexpr * liexpr * listmt
  | Iwhile      of liexpr * listmt
  | Iloop       of listmt
  | Iblock      of ident * listmt
  | Iexit       of ident
  | Ierror
  and listmt = loc * istmt

  type ifunction = {
      ifloc:    loc;
      ifparams: ident list;
      ifsg:     Types.signature;
      iftenv:   Types.typ PTree.t;
      ifbody:   listmt;
      ifszenv:  (liexpr list list) PTree.t;
      ifpenv:   Syntax.permission PTree.t
    }

  type efunction = {
      ieloc: loc;
      ieparams: ident list;
      iesg:     Types.signature;
      ietenv:   Types.typ PTree.t;
      ieszenv:  (liexpr list list) PTree.t;
      iepenv:   Syntax.permission PTree.t
  }

  type ifundef = IInternal of ifunction
               | IExternal of efunction

  type iprogram = {
      iloc:     loc;
      ifundefs: (ident * ifundef) list;
      imain:    ident
    }

  let pp_ityp p = function
  | Ttyp t -> fprintf p "@[%a@]" PrintB.print_type t
  | Tvar x -> fprintf p "@['t%d@]" x

  let rec pp_iexpr p = function
  | Iconst_int (_, n) -> fprintf p "%s" (P.string_of_int n)
  | Iconst_bool b -> fprintf p "%s" (if b then "true" else "false")
  | Iconst_float (_, f) -> fprintf p "%s" (P.string_of_float f)
  | Iacc (i, ls) -> fprintf p "@[%s%a@]" (get_name i) pp_isyn_path_elem_list ls
  | Icast ((_, e), t1, t2) -> fprintf p "@[(%a->%a)%a@]" pp_ityp t1 pp_ityp t2 pp_iexpr e
  | Iunop (op, (_, e), _) -> fprintf p "@[%s%a@]" (name_unop op)
                                              pp_iexpr e
  | Ibinop (op, (_, e1), (_, e2), _) ->
    fprintf p "@[(%a@,%s%a)@]" pp_iexpr e1 (name_binop op) pp_iexpr e2
  | Iecall (idf, args) ->
    fprintf p "@[%s(%a)@]" (Camlcoq.extern_atom idf) pp_liexpr_list args
  and pp_isyn_path_elem_list p = function
  | [] -> ()
  | Icell le :: ls -> fprintf p "[%a]" pp_liexpr_list le;
                      pp_isyn_path_elem_list p ls
  and pp_liexpr_list p = function
  | [] -> ()
  | (_, e) :: [] -> pp_iexpr p e
  | (_, e) :: l -> fprintf p "@[%a, @]%a" pp_iexpr e pp_liexpr_list l

  let rec pp_iexpr_list p = function
  | [] -> ()
  | e :: [] -> pp_iexpr p e
  | e :: l -> fprintf p "@[%a, @]%a" pp_iexpr e pp_iexpr_list l

  let pp_isyn_path p (i, ls) =
    fprintf p "%s%a" (get_name i) pp_isyn_path_elem_list ls

  let rec pp_size_list p = function
  | [] -> ()
  | lsz :: [] -> fprintf p "@[[_; %a]@]" pp_lexprlist (true, lsz)
  | lsz :: llsz -> fprintf p "@[[%a; %a]@]" pp_size_list llsz pp_lexprlist (true, lsz)
    

  type shape =
  | Sarr   of int           (* depth *)
  | Sint   of intsize list
  | Sint64 of intsize list
  | Sfloat

  type expected =
  | Exact of typ
  | Shape of shape

  let integer_shape = Shape (Sint64 [I8; I16; I32])

  type type_error =
  | FunNotExists of string
  | VarNotExists of string
  | VarNotInit of string
  | Expected of typ * expected
  | ExprNotOfType of iexpr * typ
  | InvalidApp of string * typ
  | NotInferrableVar of string
  | NotInferrable of iexpr
  | NotInferrableL of iexpr list
  | WrongNbOfArguments of int * int
  | PartialOwnershipTransfer
  | Aliasing of loc * isyn_path * isyn_path
  | FreeVarInLoopContinue
  | NonPrimitiveType of typ
  | IncompatibleSizes of (lexpr list list) * (lexpr list list)
  | ArrayAlreadyAllocated of string
  | WritingNotAllowed of string
  | InvalidAccessToPathInTvar
  | ArrayTvar
  | OwnershipTransferUsedLater of string * loc
  | UnknownBlock of string
  | InvalidPermission of Syntax.permission * Syntax.permission

  let str_intsize = function
  | I8  -> "8"
  | I16 -> "16"
  | I32 -> "32"

  let rec pp_intsize_list p = function
  | [] -> ()
  | sz :: [] -> fprintf p "%s" (str_intsize sz)
  | sz :: l  -> fprintf p "%s, %a" (str_intsize sz) pp_intsize_list l

  let pp_shape p = function
  | Sarr d    -> fprintf p "an array of depth %d" d
  | Sint l    -> fprintf p "a %a bits integer" pp_intsize_list l
  | Sint64 [] -> fprintf p "a 64 bits integer"
  | Sint64 l  -> fprintf p "a %a, 64 bits integer" pp_intsize_list l
  | Sfloat    -> fprintf p "a floatting point number"

  let pp_expected p = function
  | Exact t -> fprintf p "%a" PrintB.print_type t
  | Shape s -> fprintf p "%a" pp_shape s

  let pp_permission p = function
  | Syntax.Shared -> fprintf p "shared"
  | Syntax.Mutable -> fprintf p "mutable"
  | Syntax.Owned -> fprintf p "owned"

  let rec input_line_nth c n =
    try
      let s = input_line c in
      if n <= 1 then s else input_line_nth c (n - 1)
    with End_of_file -> failwith ("Cannot read line " ^ string_of_int n ^ " of file.")

  let rec pp_n_times p (s, n) =
    if n = 0 then ()
    else fprintf p "%s%a" s pp_n_times (s, n - 1)

  let split_and_count s =
    let l = String.split_on_char ' ' s in
    let i = ref 0 in
    let b = ref false in
    !i, String.concat " "
          (List.filter (fun s -> if not !b && String.length s = 0 then (incr i; false)
                                 else (b := true; true)) l)

  let pp_error_line p l =
    let c = open_in l.loc_file in
    let line = input_line_nth c l.loc_lnum in
    let i, line = split_and_count line in
    fprintf p "%s@.%a%a@." line pp_n_times (" ", l.loc_cnum - i)
                                pp_n_times ("^", min (String.length line - (l.loc_cnum - i))
                                                     l.loc_clen);
    close_in c

  let error_loc l e =
    let f = file_loc l.loc_file in
    match e with
    | VarNotExists id ->
      fatal_error f "@[line %d, column %d@]: @,Variable %s does not exists.@.@.%a"
                    l.loc_lnum l.loc_cnum id pp_error_line l
    | VarNotInit id ->
      fatal_error f "@[line %d, column %d@]: @,Cannot access to variable %s. It may not be initialized or it has been freed.@.@.%a"
                    l.loc_lnum l.loc_cnum id pp_error_line l
    | FunNotExists id ->
      fatal_error f "@[line %d, column %d@]: @,Function %s does not exists.@.@.%a"
                    l.loc_lnum l.loc_cnum id pp_error_line l
    | Expected (t, exp) ->
      fatal_error f "@[line %d, column %d@]: @,Expected %a, got %a.@.@.%a"
                    l.loc_lnum l.loc_cnum
                    pp_expected exp PrintB.print_type t pp_error_line l
    | ExprNotOfType (e, t) ->
      fatal_error f "@[line %d, column %d@]: @,Expression %a is expected to be of type %a.@.@.%a"
                    l.loc_lnum l.loc_cnum
                    pp_iexpr e PrintB.print_type t pp_error_line l
    | InvalidApp (op, t) ->
      fatal_error f "@[line %d, column %d@]: @,Invalid application of %s on type %a.@.@.%a"
                    l.loc_lnum l.loc_cnum op PrintB.print_type t pp_error_line l
    | NotInferrableVar i ->
      fatal_error f "@[line %d, column %d@]: @,Cannot infer type of variable %s.@.@.%a"
                    l.loc_lnum l.loc_cnum i pp_error_line l
    | NotInferrable e ->
      fatal_error f "@[line %d, column %d@]: @,Cannot infer type of %a.@.@.%a"
                    l.loc_lnum l.loc_cnum pp_iexpr e pp_error_line l
    | NotInferrableL le ->
      fatal_error f "@[line %d, column %d@]: @,Cannot infer type of %a.@.@.%a"
                    l.loc_lnum l.loc_cnum pp_iexpr_list le pp_error_line l
    | WrongNbOfArguments (exp, found) ->
      let msg = if exp > found then "Too few arguments."
                else "Too much arguments." in
      fatal_error f "@[line %d, column %d@]: @,%s Expected %d, found %d.@.@.%a"
                  l.loc_lnum l.loc_cnum msg exp found pp_error_line l
    | PartialOwnershipTransfer ->
      fatal_error f "@[line %d, column %d@]: @,Transferring a sub array is forbidden.@.@.%a"
                  l.loc_lnum l.loc_cnum pp_error_line l
    | Aliasing (l', (i1, ls1), (i2, ls2)) ->
      let p1, p2 = if List.length ls1 < List.length ls2
                   then (i1, ls1), (i2, ls2)
                   else (i2, ls2), (i1, ls1) in
      fatal_error f "@[line %d, column %d@]: @,Potential aliasing between two arguments in function call.@.@.%a@.@.\
                     @[%a is a potential prefix of %a@]"
                  l.loc_lnum l.loc_cnum pp_error_line l pp_isyn_path p1 pp_isyn_path p2
    | FreeVarInLoopContinue ->
      fatal_error f "@[line %d, column %d@]: @,A freed variable may be used in next loop turn.@.@.%a"
                  l.loc_lnum l.loc_cnum pp_error_line l
    | NonPrimitiveType t ->
      fatal_error f "@[line %d, column %d@]: @,Non-primitive type %a.@.@.%a"
                  l.loc_lnum l.loc_cnum PrintB.print_type t pp_error_line l
    | IncompatibleSizes (lf, lexp) ->
      fatal_error f "@[line %d, column %d@]: @,Incompatible sizes. Expected %a, found %a.@.@.%a"
                  l.loc_lnum l.loc_cnum
                  pp_size_list lexp pp_size_list lf
                  pp_error_line l
    | ArrayAlreadyAllocated i ->
      fatal_error f "@[line %d, column %d@]: @,Array %s already allocated.@.@.%a"
                  l.loc_lnum l.loc_cnum i pp_error_line l
    | WritingNotAllowed i ->
      fatal_error f "@[line %d, column %d@]: @,Writing to %s is not allowed.@.@.%a"
                  l.loc_lnum l.loc_cnum i pp_error_line l
    | InvalidAccessToPathInTvar ->
      fatal_error f "@[line %d, column %d@]: @,Non-empty path access to unknown type.@.@.%a"
                  l.loc_lnum l.loc_cnum pp_error_line l
    | ArrayTvar ->
      fatal_error f "@[line %d, column %d@]: @,Array's type cannot be inferred.@.@.%a"
                  l.loc_lnum l.loc_cnum pp_error_line l
    | OwnershipTransferUsedLater (i, loc) ->
      fatal_error f "@[line %d, column %d@]: @,Invalid ownership transfer.@.@.%a@.@.Variable is used at line %d, column %d:@.@.%a"
                  l.loc_lnum l.loc_cnum pp_error_line l
                  loc.loc_lnum loc.loc_cnum pp_error_line loc
    | UnknownBlock blkid ->
      fatal_error f "@[line %d, column %d@]: @,Unknown block %s.@.@.%a"
                  l.loc_lnum l.loc_cnum blkid pp_error_line l
    | InvalidPermission (p1, p2) ->
      fatal_error f "@[line %d, column %d@]: @,Invalid permission. Expected %a, got %a.@.@.%a"
                  l.loc_lnum l.loc_cnum
                  pp_permission p1 pp_permission p2
                  pp_error_line l

end
