open TranslConsts.AAST
open Maps
open Berror

type sigenv = Types.signature PTree.t
type tenv = Types.typ PTree.t
type ident = Bast.ident

exception UnknownFunction of (loc * ident)

module IdSet = Set.Make(Camlcoq.P)

let rec get_type_syn_path l t0 d t ls =
  match ls, t with
  | [], _ -> t
  | Icell _ :: ls, Types.Tarr t -> get_type_syn_path l t0 (d + 1) t ls
  | _, _ ->
    error_loc l (Expected (t0, Shape (Sarr (d + 1))))

let get_type (te: tenv) l (i, ls) =
  match PTree.get i te with
  | Some t -> get_type_syn_path l t 0 t ls
  | None -> error_loc l (VarNotExists (ParserUtils.get_name i))

let exprlist_to_variable_assignments (te: tenv) le lt (k: tenv -> liexpr list -> tenv * listmt) =
  List.fold_left2 (fun k (l, e) t ->
      match e with
      | Iacc (i, ls) when not (Types.primitive_type (get_type te l (i, ls))) -> 
        fun te args -> k te ((l, e) :: args)
      | _ -> let i = ParserUtils.fresh_ident "_tmp" in
             fun te args -> let te', s = k (PTree.set i t te) ((l, Iacc (i, [])) :: args) in
                            te',
                            (l, Iseq ((l, Iseq ((l, Idecl (i, t)),
                                                (l, Iassign ((i, []), (l, e))))), s)))
                  k le lt te []

let (let+) (te, s1) s2 =
  te, s2 s1

let rec simplify_expr (sige: sigenv) (te: tenv) (l, e) (k: tenv -> liexpr -> tenv * listmt) =
  let simplify_expr = simplify_expr sige in
  match e with
  | Iconst_int _ | Iconst_bool _ | Iconst_float _ -> k te (l, e)
  | Ibinop (Bast.Oband, e1, e2, _) ->
    let te', s = simplify_expr te e2 k in
    simplify_expr te' e1 (fun te' r -> let+ x = k te' (l, Iconst_bool false) in l, Iifthenelse(r, s, x))
  | Ibinop (Bast.Obor, e1, e2, _) ->
    let te', s = simplify_expr te e2 k in
    simplify_expr te' e1 (fun te' r -> let+ x = k te' (l, Iconst_bool true) in l, Iifthenelse(r, x, s))
  | Icast (e, t1, t2) ->
    simplify_expr te e (fun te' r -> k te' (l, Icast (r, t1, t2)))
  | Iacc (id, ls) ->
    simplify_syn_path_elem_list sige te ls (fun te' r -> k te' (l, Iacc (id, r)))
  | Iunop (op, e, t) ->
    simplify_expr te e (fun te' r -> k te' (l, Iunop (op, r, t)))
  | Ibinop (op, e1, e2, t) ->
    simplify_expr te  e1 (fun te1 r1 ->
    simplify_expr te1 e2 (fun te2 r2 ->
      k te2 (l, Ibinop (op, r1, r2, t))
    ))
  | Iecall (idf, args) ->
    match PTree.get idf sige with
    | Some s ->
      simplify_expr_list sige te args (fun te' rargs ->
        let i = ParserUtils.fresh_ident "_tmp_res" in
        let te' = PTree.set i s.Types.sig_res te' in
        exprlist_to_variable_assignments te' rargs s.Types.sig_args (fun te' args ->
          let+ x = k te' (l, Iacc (i, [])) in
          (l, Iseq ((l, Iseq ((l, Idecl (i, s.Types.sig_res)),
                              (l, Icall (Some i, idf, args)))),
                    x))))
    | None -> raise (UnknownFunction (l, idf))

and simplify_syn_path_elem_list sige te ls (k: tenv -> isyn_path_elem list -> tenv * listmt) =
  match ls with
  | [] -> k te []
  | Icell le :: ls ->
    simplify_expr_list sige te le (fun te le ->
    simplify_syn_path_elem_list sige te ls (fun te ls ->
        k te (Icell le :: ls)))

and simplify_expr_list sige te l (k: tenv -> liexpr list -> tenv * listmt) =
  match l with
  | [] -> k te []
  | e :: le ->
    simplify_expr sige te e (fun te e ->
    simplify_expr_list sige te le (fun te le ->
      k te (e :: le)))

let rec expr_can_be_simplified (l, e) =
  match e with
  | Ibinop (Bast.Oband, _, _, _)
  | Ibinop (Bast.Obor, _, _, _) -> true
  | Iunop  (_, e, _)       -> expr_can_be_simplified e
  | Icast  (e, _, _)       -> expr_can_be_simplified e
  | Iacc   (_, ls)         -> syn_path_elem_list_has_lazy_bool_ops ls
  | Ibinop (_, e1, e2, _)  -> expr_can_be_simplified e1 ||
                              expr_can_be_simplified e2
  | Iecall (_, args)       -> true
  | _ -> false
and syn_path_elem_list_has_lazy_bool_ops ls =
  match ls with
  | [] -> false
  | Icell le :: ls ->
    expr_list_can_be_simplified le || syn_path_elem_list_has_lazy_bool_ops ls
and expr_list_can_be_simplified l =
  match l with
  | [] -> false
  | e :: le -> expr_can_be_simplified e || expr_list_can_be_simplified le

let rec simplify_stmt sige te (l, s) =
  let simplify_stmt = simplify_stmt sige in
  match s with
  | Iassign ((i, ls), e) ->
    simplify_expr sige te e (fun te e ->
    simplify_syn_path_elem_list sige te ls (fun te ls ->
      te, (l, Iassign ((i, ls), e))))
  | Icall (idv, idf, args) ->
    begin match PTree.get idf sige with
    | Some s ->
      simplify_expr_list sige te args (fun te rargs ->
      exprlist_to_variable_assignments te rargs s.Types.sig_args (fun te args ->
        te, (l, Icall (idv, idf, args))))
    | None -> raise (UnknownFunction (l, idf))
    end
  | Ireturn None -> te, (l, Ireturn None)
  | Ireturn (Some e) ->
    simplify_expr sige te e (fun te e -> te, (l, Ireturn (Some e)))
  | Iseq (s1, s2) ->
    let te, s1' = simplify_stmt te s1 in
    let te, s2' = simplify_stmt te s2 in
    te, (l, Iseq (s1', s2'))
  | Iifthenelse (c, strue, sfalse) ->
    let tmp = ParserUtils.fresh_ident "__tmp" in
    let u   = ref false in
    let te, s1' = simplify_stmt te strue  in
    let te, s2' = simplify_stmt te sfalse in
    let+ s =
      simplify_expr sige te c (fun te c ->
          te, (l, (Iseq ((l, Idecl (tmp, Types.Tbool)),
                    match c with
                    | (_, Iconst_bool true)  -> (l, Iassign ((tmp, []), (l, Iconst_bool true)))
                    | (_, Iconst_bool false) -> (l, Iassign ((tmp, []), (l, Iconst_bool false)))
                    | _ ->
                      u := true;
                      (l, Iifthenelse (c,
                            (l, Iassign ((tmp, []), (l, Iconst_bool true))),
                            (l, Iassign ((tmp, []), (l, Iconst_bool false))))))))) in
    (if !u then (l, Iseq (s, (l, Iifthenelse ((l, Iacc (tmp, [])), s1', s2'))))
     else s)
  | Iblock (blkid, (l1, s)) ->
    begin match s with
    | Iwhile (c, (l2, Iblock (blkid_in, s))) ->
      if expr_can_be_simplified c then
        let s = (l, Iblock (blkid, (l1, Iloop ((l2, Iblock (blkid_in,
                    (l, Iifthenelse (c, s, (l, Iexit blkid))))))))) in
        simplify_stmt te s
      else
        let+ s' = simplify_stmt te s in
        (l, Iblock (blkid, (l1, Iwhile (c, (l2, Iblock (blkid_in, s'))))))
    | Iwhile _ ->
      failwith "loop without inner block"
    | _ ->
      let te, s' = simplify_stmt te (l1, s) in
      (te, (l, Iblock (blkid, s')))
    end
  | Ifor (id, d, t, lo, hi, s) ->
    let te, s' = simplify_stmt te s in
    simplify_expr sige te lo (fun te lo ->
    simplify_expr sige te hi (fun te hi ->
      te, (l, Ifor (id, d, t, lo, hi, s'))))
  | Ifor_step (id, d, t, lo, hi, step, s) ->
    let te, s' = simplify_stmt te s in
    simplify_expr sige te lo (fun te lo ->
    simplify_expr sige te hi (fun te hi ->
    simplify_expr sige te step (fun te step ->
      te, (l, Ifor_step (id, d, t, lo, hi, step, s')))))
  | Iloop s ->
    let+ s' = simplify_stmt te s in
    (l, Iloop s')
  | Iassert e ->
    simplify_expr sige te e (fun te e ->
      te, (l, Iassert e))
  | Iwhile _ -> failwith "loop outside of a block"
  | s -> te, (l, s)

let simplify_function sige f =
  { f with ifbody = simplify_stmt sige f.iftenv f.ifbody |> snd }

let simplify_fundef sige = function
  | IInternal f -> IInternal (simplify_function sige f)
  | ef -> ef

let fd_sig (fd: ifundef) =
  match fd with
  | IInternal f -> f.ifsg
  | IExternal ef -> ef.iesg

let make_sigenv (lfd: (ident * ifundef) list) =
  List.fold_left (fun e (id, fd) ->
      let s = fd_sig fd in
      PTree.set id s e) PTree.Empty lfd

let simplify_program p =
  let sige = make_sigenv p.ifundefs in
  { p with ifundefs = List.map (fun (id, fd) ->
                          (id, simplify_fundef sige fd)) p.ifundefs }
