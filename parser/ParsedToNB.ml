open Syntax

let transl_program (p: Bast.program) : program =
  let ip = TypeInference.infer_program p in
  let ap = TranslConsts.transl_program ip in
  Aliasing.check_program ap;
  let ap' = Isimplify.simplify_program ap in
  TranslToNB.transl_program ap'
