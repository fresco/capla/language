let helper_functions () = [
  Camlcoq.intern_string "__compcert_i64_dtos",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_dtos",
    { AST.sig_args = [AST.Tfloat];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_dtou",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_dtou", 
    { AST.sig_args = [AST.Tfloat];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_stod",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_stod", 
    { AST.sig_args = [AST.Tlong];
      AST.sig_res = AST.Tret (AST.Tfloat);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_utod",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_utod", 
    { AST.sig_args = [AST.Tlong];
      AST.sig_res = AST.Tret (AST.Tfloat);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_stof",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_stof", 
    { AST.sig_args = [AST.Tlong];
      AST.sig_res = AST.Tret (AST.Tsingle);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_utof",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_utof", 
    { AST.sig_args = [AST.Tlong];
      AST.sig_res = AST.Tret (AST.Tsingle);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_ftos",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_ftos",
    { AST.sig_args = [AST.Tsingle];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_ftou",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_ftou",
    { AST.sig_args = [AST.Tsingle];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_sdiv",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_sdiv",
    { AST.sig_args = [AST.Tlong; AST.Tlong];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_udiv",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_udiv", 
    { AST.sig_args = [AST.Tlong; AST.Tlong];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_smod",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_smod", 
    { AST.sig_args = [AST.Tlong; AST.Tlong];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_umod",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_umod", 
    { AST.sig_args = [AST.Tlong; AST.Tlong];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_shl",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_shl", 
    { AST.sig_args = [AST.Tlong; AST.Tint];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_shr",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_shr", 
    { AST.sig_args = [AST.Tlong; AST.Tint];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_sar",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_sar", 
    { AST.sig_args = [AST.Tlong; AST.Tint];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_smulh",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_smulh", 
    { AST.sig_args = [AST.Tlong; AST.Tlong];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "__compcert_i64_umulh",
    AST.External (AST.EF_runtime (Camlcoq.coqstring_of_camlstring "__compcert_i64_umulh", 
    { AST.sig_args = [AST.Tlong; AST.Tlong];
      AST.sig_res = AST.Tret (AST.Tlong);
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "abort",
    AST.External (AST.EF_external (Camlcoq.coqstring_of_camlstring "abort",
    { AST.sig_args = [];
      AST.sig_res = AST.Tvoid;
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "calloc",
    AST.External (AST.EF_external (Camlcoq.coqstring_of_camlstring "calloc",
    { AST.sig_args = [AST.Tlong; AST.Tlong];
      AST.sig_res = AST.Tret AST.Tlong;
      AST.sig_cc = AST.cc_default}));
  Camlcoq.intern_string "free",
    AST.External (AST.EF_external (Camlcoq.coqstring_of_camlstring "free",
    { AST.sig_args = [AST.Tlong];
      AST.sig_res = AST.Tvoid;
      AST.sig_cc = AST.cc_default}));
]
