open Camlcoq

exception NoOpenedScope
exception NotFoundIdent of string
exception NotFoundName of atom

let h = ref [Hashtbl.create 10]
let r = Hashtbl.create 100

let open_scope () =
  h := Hashtbl.create 10 :: !h

let close_scope () =
  match !h with
  | [] -> raise NoOpenedScope
  | _ :: t -> h := t

let fresh_ident s =
  match !h with
  | [] -> raise NoOpenedScope
  | t :: _ ->
    let i = fresh_atom () in
    (* add i -> s in CompCert's name hashtable for error printing *)
    bind_print_name i s;
    Hashtbl.replace t s i;
    Hashtbl.replace r i s;
    i

let get_ident s =
  let rec aux h =
    match h with
    | [] -> raise (NotFoundIdent s)
    | t :: h ->
      match Hashtbl.find t s with
      | i -> i
      | exception Not_found -> aux h in
  match aux !h with
  | i -> i
  | exception (NotFoundIdent _) -> fresh_ident s

let get_name i =
  match Hashtbl.find r i with
  | i -> i
  | exception Not_found -> raise (NotFoundName i)