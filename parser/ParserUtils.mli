open Camlcoq

exception NoOpenedScope
exception NotFoundIdent of string
exception NotFoundName of atom

val open_scope: unit -> unit
val close_scope: unit -> unit

val fresh_ident: string -> atom
val get_ident: string -> atom
val get_name: atom -> string