open Format
open Diagnostics

module E = MenhirLib.ErrorReports
module L = MenhirLib.LexerUtil

module I = Bparser.MenhirInterpreter

let env checkpoint =
  match checkpoint with
  | I.HandlingError env ->
      env
  | _ ->
      assert false

let show text positions =
  E.extract text positions
  |> E.sanitize
  |> E.compress
  |> E.shorten 20

let get text checkpoint i =
  match I.get i (env checkpoint) with
  | Some (I.Element (_, _, pos1, pos2)) ->
      show text (pos1, pos2)
  | None -> "???"
let string_of_terminal: type a. a I.terminal -> string =
    fun t ->
  match t with
  | I.T_error -> "error"
  | I.T_XOR -> "^"
  | I.T_WHILE  -> "while"
  | I.T_TYPE  -> "type"
  | I.T_TILDE  -> "~"
  | I.T_STEP  -> "step"
  | I.T_STAR  -> "*"
  | I.T_SKIP  -> "skip"
  | I.T_SHR  -> ">>"
  | I.T_SHL  -> "<<"
  | I.T_SEMICOLON  -> ";"
  | I.T_RSQ  -> "]"
  | I.T_RPAR  -> ")"
  | I.T_RIGHTARROW  -> "->"
  | I.T_RETURN  -> "return"
  | I.T_RCU  -> "}"
  | I.T_RANGE  -> ".."
  | I.T_PLUS  -> "+"
  | I.T_OWNED  -> "own"
  | I.T_OR  -> "|"
  | I.T_NOT  -> "!"
  | I.T_NE  -> "!="
  | I.T_MUTABLE  -> "mut"
  | I.T_MOD  -> "%"
  | I.T_MINUS  -> "-"
  | I.T_LT  -> "<"
  | I.T_LSQ  -> "["
  | I.T_LPAR  -> "("
  | I.T_LET  -> "let"
  | I.T_LE  -> "<="
  | I.T_LCU  -> "{"
  | I.T_INT  -> "int"
  | I.T_IF  -> "if"
  | I.T_IDENT  -> "ident"
  | I.T_GT  -> ">"
  | I.T_GE  -> ">="
  | I.T_FUN  -> "fun"
  | I.T_FREE  -> "free"
  | I.T_FOR  -> "for"
  | I.T_FLOAT  -> "float"
  | I.T_EXTERN  -> "extern"
  | I.T_ERROR  -> "error"
  | I.T_EQUAL  -> "=="
  | I.T_EOF  -> "eof"
  | I.T_ELSE  -> "else"
  | I.T_DIV  -> "/"
  | I.T_DECR  -> "decr"
  | I.T_CONTINUE  -> "continue"
  | I.T_COMMA  -> ","
  | I.T_COLON  -> ":"
  | I.T_BREAK  -> "break"
  | I.T_BOR  -> "||"
  | I.T_BOOL  -> "bool"
  | I.T_BAND  -> "&&"
  | I.T_ASSIGN  -> "="
  | I.T_ASSERT  -> "assert"
  | I.T_ANNOT_INT  -> "int"
  | I.T_ANNOT_FLOAT  -> "float"
  | I.T_AND  -> "&"
  | I.T_ALLOC  -> "alloc"
  | I.T_EXIT -> "exit"

let string_of_nonterminal: type a. a I.nonterminal -> string =
  fun t ->
    match t with
    | I.N_arith_expr -> "arithmetic expression"
    | I.N_atom -> "atomic expression"
    | I.N_atom_stmt -> "atomic statement"
    | I.N_band_expr -> "expression without `||` outside parentheses"
    | I.N_binrel -> "comparison operator"
    | I.N_block -> "block"
    | I.N_bool_expr -> "boolean expression"
    | I.N_bor_expr -> "expression"
    | I.N_decr -> "decr"
    | I.N_end_block -> "end of block"
    | I.N_expr -> "expression"
    | I.N_fct -> "function"
    | I.N_fresh -> "identifier"
    | I.N_fundef -> "function definition"
    | I.N_goto_stmt -> "goto statement"
    | I.N_init -> "variable initialization"
    | I.N_nonempty_list_fresh_ -> "list of identifiers"
    | I.N_list_IDENT_ -> "list of identifiers"
    | I.N_list_fundef_ -> "list of function declarations"
    | I.N_list_syn_path_elem_ -> "list of path elements"
    | I.N_param -> "parameter"
    | I.N_prog -> "program"
    | I.N_separated_list_arith_expr_COMMA_ -> "list (with `,` as separator) of arithmetic expressions"
    | I.N_separated_list_expr_COMMA_ -> "list (with `,` as separator) of expressions"
    | I.N_separated_list_param_COMMA_ -> "list (with `,` as separator) of parameters"
    | I.N_start_block -> "start of block"
    | I.N_stmt -> "statement"
    | I.N_structure -> "structure"
    | I.N_syn_path -> "path"
    | I.N_syn_path_elem -> "path element"
    | I.N_typ -> "type"
    | I.N_var_decl -> "variable declaration"
    | I.N_var_decls -> "variable declarations"
    | I.N_option_IDENT_ -> "optional identifier"

let first (n: _ I.nonterminal) : I.xsymbol list =
  I.foreach_terminal_but_error (fun (x: I.xsymbol) l ->
    match x with
    | I.X (I.T t) -> if I.first n t then x :: l else l
    | I.X _ -> assert false) []

let rec feed (l: I.xsymbol list) p1 p2 e =
  match l with
  | [] -> Some e
  | I.X (I.T _) :: _ -> None
  | I.X ((I.N n as s)) :: xs ->
    match feed xs p1 p2 e with
    | None -> None
    | Some e ->
      match n with
      | I.N_list_IDENT_ -> Some (I.feed s p1 [] p2 e)
      | I.N_list_fundef_ -> Some (I.feed s p1 [] p2 e)
      | I.N_list_syn_path_elem_ -> Some (I.feed s p1 [] p2 e)
      | I.N_separated_list_arith_expr_COMMA_ -> Some (I.feed s p1 [] p2 e)
      | I.N_separated_list_expr_COMMA_ -> Some (I.feed s p1 [] p2 e)
      | I.N_separated_list_param_COMMA_ -> Some (I.feed s p1 [] p2 e)
      | _ -> None

let rec drop l n =
  if n <= 0 then l else
  match l with
  | [] -> raise (Invalid_argument "empty list")
  | _ :: xs -> drop xs (n - 1)

module StrSet = Set.Make(String)

let rec expected_tokens env =
  match I.top env with
  | Some (I.Element (state, _, _, _)) ->
      let items = I.items state in
      let exp = List.filter_map (fun (p, i) -> List.nth_opt (I.rhs p) i) items in
      let exp = List.fold_left (fun exp x ->
                  match x with
                  | I.X (I.T t) -> StrSet.add (string_of_terminal t) exp
                  | I.X (I.N n) ->
                    StrSet.union
                      (first n
                       |> List.map (function
                          | I.X (I.T t) -> string_of_terminal t
                          | I.X (I.N _) -> failwith "error: first returned a non-terminal symbol")
                       |> StrSet.of_list)
                      exp) StrSet.empty exp in
      List.fold_left (fun exp (p, i) ->
        let l = drop (I.rhs p) i in
        let p1, p2 = I.positions env in
        match feed l p1 p2 env with
        | None -> exp
        | Some e' ->
          let e' = I.force_reduction p e' in
          let exp' = expected_tokens e' in
          StrSet.union exp exp'
        ) exp items
  | None -> StrSet.empty

let succeed _v = assert false

let fail ifile text lexbuf buffer (checkpoint : _ I.checkpoint) =
  let location =
       L.range (E.last buffer)
    |> String.split_on_char ','
    |> List.tl
    |> List.map (fun s -> String.sub s 1 (String.length s - 1))
    |> String.concat ", " in
  let indication = sprintf "Syntax error %s.\n" (E.show (show text) buffer) in
  let exp = expected_tokens (env checkpoint) in
  let expmsg =
    let s = StrSet.fold (fun s msg -> msg ^ ", " ^ s) exp "" in
    "Expected " ^ String.sub s 2 (String.length s - 2) in
  let loc = file_loc ifile in
  error loc "%s%s%s" location indication expmsg

let parse ifile =
  let text, lb = L.read ifile in
  Berror.set_filename ifile;
  match Bparser.prog Blexer.token lb with
  | p -> p
  | exception Bparser.Error ->
    let lexbuf = L.init ifile (Lexing.from_string text) in
    let supplier = I.lexer_lexbuf_to_supplier Blexer.token lexbuf in
    let buffer, supplier = E.wrap_supplier supplier in
    let checkpoint = Bparser.Incremental.prog lexbuf.Lexing.lex_curr_p in
    I.loop_handle succeed (fail ifile text lexbuf buffer) supplier checkpoint;
    error_summary ();
    exit 1