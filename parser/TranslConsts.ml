open Bast
open Iast
open TypeInference

module CInt = Integers.Int
module CInt64 = Integers.Int64

type i = I32 of Types.signedness * CInt.int
       | I64 of Types.signedness * CInt64.int

type f = F32 of Floats.float32
       | F64 of Floats.float

module A = struct
  type int = i
  type float = f

  let string_of_int = function
    | I32(Types.Signed, n) ->
      n |> CInt.signed |> Camlcoq.camlint_of_coqint |> Int32.to_string
    | I32(Types.Unsigned, n) ->
      n |> CInt.signed |> Camlcoq.camlint_of_coqint |> Int32.to_string
    | I64(Types.Signed, n) ->
      n |> CInt64.signed |> Camlcoq.camlint64_of_coqint |> Int64.to_string
    | I64(Types.Unsigned, n) ->
      n |> CInt64.signed |> Camlcoq.camlint64_of_coqint |> Int64.to_string

  let string_of_float = function
    | F32 f -> f |> Camlcoq.camlfloat_of_coqfloat32 |> string_of_float
    | F64 f -> f |> Camlcoq.camlfloat_of_coqfloat |> string_of_float
end

module AAST = Iast.AST(A)

open AAST

let rec transl_expr ((l, e): IAST.liexpr) : liexpr =
  match e with
  | IAST.Iconst_int (Ttyp (Types.Tint (_, sg)) as t, n) ->
    let n = Integers.Int.repr (Camlcoq.coqint_of_camlint64 (Int64.of_string n)) in
    (l, Iconst_int (t, I32(sg, n)))
  | IAST.Iconst_int (Ttyp (Types.Tint64 sg) as t, n) ->
    let n = Integers.Int64.repr (Camlcoq.coqint_of_camlint64 (Int64.of_string n)) in
    (l, Iconst_int (t, I64(sg, n)))
  | IAST.Iconst_int (Ttyp t, _) -> IAST.error_loc l (IAST.Expected (t, IAST.integer_shape))
  | IAST.Iconst_int (Tvar _, _) -> IAST.error_loc l (IAST.NotInferrable e)
  | IAST.Iconst_float (Ttyp (Types.Tfloat fsz) as t, f) ->
    let p = String.length f.decpart in
    let base = Camlcoq.P.of_int 10 in
    let exp = Camlcoq.Z.of_sint64 (Int64.sub (Int64.of_string f.exp) (Int64.of_int p)) in
    begin match fsz with
    | Types.F32 ->
      let i = Int64.of_string (f.intpart ^ f.decpart) in
      let f =
        if i = Int64.zero then Floats.Float32.zero
        else
          let intpart =  Camlcoq.P.of_int64 i in
          Floats.Float32.from_parsed base intpart exp in
      (l, Iconst_float (t, F32 f))
    | Types.F64 ->
      let i = Int64.of_string (f.intpart ^ f.decpart) in
      let f =
        if i = Int64.zero then Floats.Float.zero
        else
          let intpart =  Camlcoq.P.of_int64 i in
          Floats.Float.from_parsed base intpart exp in
      (l, Iconst_float (t, F64 f))
    end
  | IAST.Iconst_float (Ttyp t, _) -> IAST.error_loc l (IAST.Expected (t, IAST.Shape IAST.Sfloat))
  | IAST.Iconst_float (Tvar _, _) -> IAST.error_loc l (IAST.NotInferrable e)
  | IAST.Iconst_bool b -> (l, Iconst_bool b)
  | IAST.Iacc (i, ls) -> (l, Iacc (i, transl_isyn_path_elem_list ls))
  | IAST.Icast (e, t1, t2) -> (l, Icast (transl_expr e, t1, t2))
  | IAST.Iunop (op, e, t) -> (l, Iunop (op, transl_expr e, t))
  | IAST.Ibinop (op, e1, e2, t) ->
    (l, Ibinop (op, transl_expr e1, transl_expr e2, t))
  | IAST.Iecall (idf, args) ->
    (l, Iecall (idf, transl_expr_list args))

and transl_isyn_path_elem (s: IAST.isyn_path_elem) =
  match s with
  | IAST.Icell l -> Icell (transl_expr_list l)

and transl_isyn_path_elem_list l =
  List.map transl_isyn_path_elem l

and transl_expr_list le = List.map transl_expr le

let rec transl_stmt ((l, s): IAST.listmt) : listmt =
  let s = match s with
    | IAST.Iskip -> Iskip
    | IAST.Idecl (i, t) -> Idecl (i, t)
    | IAST.Iassign ((i, ls), e) ->
      Iassign ((i, transl_isyn_path_elem_list ls), transl_expr e)
    | IAST.Ialloc i -> Ialloc i
    | IAST.Ifree i -> Ifree i
    | IAST.Iassert e ->
      Iassert (transl_expr e)
    | IAST.Icall (idv, idf, args) ->
      Icall (idv, idf, transl_expr_list args)
    | IAST.Ireturn None -> Ireturn None
    | IAST.Ireturn (Some e) -> Ireturn (Some (transl_expr e))
    | IAST.Iseq (s1, s2) -> Iseq (transl_stmt s1, transl_stmt s2)
    | IAST.Iifthenelse (c, s1, s2) ->
      Iifthenelse (transl_expr c, transl_stmt s1, transl_stmt s2)
    | IAST.Ifor (id, b, t, lo, hi, s) ->
      let lo = transl_expr lo in
      let hi = transl_expr hi in
      Ifor (id, b, t, lo, hi, transl_stmt s)
    | IAST.Ifor_step (id, b, t, lo, hi, step, s) ->
      let lo = transl_expr lo in
      let hi = transl_expr hi in
      let step = transl_expr step in
      Ifor_step (id, b, t, lo, hi, step, transl_stmt s)
    | IAST.Iwhile (c, s) ->
      Iwhile (transl_expr c, transl_stmt s)
    | IAST.Iloop s -> Iloop (transl_stmt s)
    | IAST.Iblock (blkid, s) -> Iblock (blkid, transl_stmt s)
    | IAST.Iexit blkid -> Iexit blkid
    | IAST.Ierror -> Ierror in
  (l, s)

let transl_function f =
  let szenv = Maps.PTree.map (fun _ llsz ->
                List.map (List.map transl_expr) llsz
              ) f.IAST.ifszenv in
  { ifloc    = f.IAST.ifloc;
    ifparams = f.IAST.ifparams;
    ifsg     = f.IAST.ifsg;
    iftenv   = f.IAST.iftenv;
    ifbody   = transl_stmt f.IAST.ifbody;
    ifszenv  = szenv;
    ifpenv   = f.IAST.ifpenv }

let transl_external_function ef =
  let szenv = Maps.PTree.map (fun _ llsz ->
                List.map (List.map transl_expr) llsz
              ) ef.IAST.ieszenv in
  { ieloc    = ef.IAST.ieloc;
    ieparams = ef.IAST.ieparams;
    iesg     = ef.IAST.iesg;
    ietenv   = ef.IAST.ietenv;
    ieszenv  = szenv;
    iepenv   = ef.IAST.iepenv }

let transl_fundef fd =
  match fd with
  | IAST.IInternal f -> IInternal (transl_function f)
  | IAST.IExternal ef -> IExternal (transl_external_function ef)

let transl_program (p: IAST.iprogram) : iprogram =
  { iloc = p.IAST.iloc;
    ifundefs = List.map (fun (id, fd) -> (id, transl_fundef fd))
                        p.IAST.ifundefs;
    imain    = p.IAST.imain }
