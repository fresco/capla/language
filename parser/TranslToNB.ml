open Berror
open Iast
open Types
open Format
open Syntax
open Maps
open Diagnostics
open Driveraux

module A = TranslConsts.AAST

exception TypeError of string

type unop = Bast.unop
type binop = Bast.binop

let kind_of_typ = function
  | Tbool      -> Ops.OInt
  | Tint   _   -> Ops.OInt
  | Tint64 _   -> Ops.OInt64
  | Tfloat F32 -> Ops.OFloat32
  | Tfloat F64 -> Ops.OFloat64
  | t          ->
    raise (TypeError (asprintf "Invalid operation on type %a." PrintB.print_type t))

let transl_unop =
  let open Bast in
  function
  | Onotbool -> Ops.Onotbool
  | Onotint  -> Ops.Onotint
  | Oneg     -> Ops.Oneg

let transl_binop_arith op sg =
  let open Bast in
  match op, sg with
  | Oadd, _ -> Ops.Oadd
  | Osub, _ -> Ops.Osub
  | Omul, _ -> Ops.Omul
  | Odiv, Signed   -> Ops.Odivs
  | Odiv, Unsigned -> Ops.Odivu
  | Omod, Signed   -> Ops.Omods
  | Omod, Unsigned -> Ops.Omodu
  | Oshl, _ -> Ops.Oshl
  | Oshr, Signed   -> Ops.Oshr
  | Oshr, Unsigned -> Ops.Oshru
  | Oand, _ -> Ops.Oand
  | Oor,  _ -> Ops.Oor
  | Oxor, _ -> Ops.Oxor
  |    _,        _ -> assert false

let transl_binop_cmp op =
  let open Bast in
  match op with
  | Oeq -> Ops.Oeq
  | One -> Ops.One
  | Olt -> Ops.Olt
  | Ole -> Ops.Ole
  | Ogt -> Ops.Ogt
  | Oge -> Ops.Oge
  |   _ -> assert false

let transl_binop_cmpu op =
  let open Bast in
  match op with
  | Olt -> Ops.Oltu
  | Ole -> Ops.Oleu
  | Ogt -> Ops.Ogtu
  | Oge -> Ops.Ogeu
  |   _ -> assert false

let kind_of_ityp = function
  | Ttyp Tbool | Ttyp (Tint _) -> Ops.OInt
  | Ttyp (Tint64 _) -> Ops.OInt64
  | Ttyp (Tfloat F32) -> Ops.OFloat32
  | Ttyp (Tfloat F64) -> Ops.OFloat64
  | _ -> assert false

let rec transl_expr (subst: SemPath.ident PTree.t) ((l, e) : A.liexpr) : expr =
  let transl_expr = transl_expr subst in
  match e with
  | A.Iconst_int (Ttyp (Tint (sz, sg)), TranslConsts.I32 (_, n)) ->
    Econst (Cint (sz, sg, n))
  | A.Iconst_int (Ttyp (Tint64 sg), TranslConsts.I64 (_, n)) ->
    Econst (Cint64 (sg, n))
  | A.Iconst_float (Ttyp (Tfloat F32), TranslConsts.F32 f) ->
    Econst (Cfloat32 f)
  | A.Iconst_float (Ttyp (Tfloat F64), TranslConsts.F64 f) ->
    Econst (Cfloat64 f)
  | A.Iconst_bool b -> Econst (Cbool b)
  | A.Iacc (i, ls) -> 
    let i = match PTree.get i subst with Some i' -> i' | None -> i in
    Eacc (i,  transl_syn_path_elem_list subst ls)
  | A.Icast (e, Ttyp t1, Ttyp t2) -> Ecast (transl_expr e, t1, t2)
  | A.Iunop (op, e, t) -> Eunop (transl_unop op, kind_of_ityp t, transl_expr e)
  | A.Ibinop (op, e1, e2, Ttyp t) ->
    let sg = match t with
      | Tint (_, sg) -> sg
      | Tint64 sg -> sg
      | Tfloat _  -> Signed
      | _ -> Unsigned in
    let k  = kind_of_typ t in
    let open Bast in
    begin match op with
    | Oadd | Osub | Omul | Odiv | Omod | Oshr | Oshl | Oand | Oor | Oxor ->
      let op = transl_binop_arith op sg in
      Ebinop_arith (op, k, transl_expr e1, transl_expr e2)
    | Oband | Obor -> failwith "Error: A lazy binary operator has not been eliminated."
    | Oeq | One ->
      let op = transl_binop_cmp op in
      Ebinop_cmp (op, k, transl_expr e1, transl_expr e2)
    | Olt | Ogt | Ole | Oge ->
      match sg with
      | Signed ->
        let op = transl_binop_cmp op in
        Ebinop_cmp (op, k, transl_expr e1, transl_expr e2)
      | Unsigned ->
        let op = transl_binop_cmpu op in
        Ebinop_cmpu (op, k, transl_expr e1, transl_expr e2)
    end
  | A.Iecall _ -> failwith "Error: A call in expressions has not been eliminated."
  | _ -> assert false

and transl_syn_path_elem subst = function
  | A.Icell lidx ->
    Scell (List.map (transl_expr subst) lidx)

and transl_syn_path_elem_list subst ls = List.map (transl_syn_path_elem subst) ls

let transl_syn_path subst (i, ls) =
  let i = match PTree.get i subst with Some i' -> i' | None -> i in
  (i, transl_syn_path_elem_list subst ls)

let check_writing_allowed pe subst l i =
  let i = match PTree.get i subst with Some i' -> i' | None -> i in
  match PTree.get i pe with
  | Some Shared -> A.error_loc l (A.WritingNotAllowed (ParserUtils.get_name i))
  | _ -> ()

let index_of x l =
  let rec aux n l =
    match l with
    | [] -> None
    | y :: l -> if x = y then Some n else aux n l in
  aux 0 l

let rec transl_stmt (pe: penv) (te: tenv) (subst: SemPath.ident PTree.t)
                    (blocks: SemPath.ident list)
                    ((l, s): A.listmt) : stmt * tenv =
  match s with
  | A.Iskip -> Sskip, te
  | A.Idecl (id, t) -> Sskip, PTree.set id t te
  | A.Iassign (p, e) -> check_writing_allowed pe subst l (fst p);
                        Sassign (transl_syn_path subst p, transl_expr subst e), te
  | A.Ialloc i -> check_writing_allowed pe subst l i; Salloc i, te
  | A.Ifree i -> Sfree i, te
  | A.Iassert e -> Sassert (transl_expr subst e), te
  | A.Icall (idv, idf, args) ->
    let args = List.map (fun (l, e) ->
                   match e with
                   | A.Iacc (i, ls) -> transl_syn_path subst (i, ls)
                   | _ -> failwith "Error: An expression argument has not been \
                                    changed into variable."
                 ) args in
    Scall (idv, idf, args), te
  | A.Ireturn None -> Sreturn None, te
  | A.Ireturn (Some e) -> Sreturn (Some (transl_expr subst e)), te
  | A.Iseq (s1, s2) ->
    let s1, te1 = transl_stmt pe te  subst blocks s1 in
    let s2, te2 = transl_stmt pe te1 subst blocks s2 in
    Sseq (s1, s2), te2
  | A.Iifthenelse (c, s1, s2) ->
    let s1, te1 = transl_stmt pe te  subst blocks s1 in
    let s2, te2 = transl_stmt pe te1 subst blocks s2 in
    Sifthenelse (transl_expr subst c, s1, s2), te2
  | A.Ifor (i, d, t, lo, hi, s) ->
    let s, te = transl_stmt pe te subst blocks s in
    let i_hi = ParserUtils.fresh_ident ("_" ^ ParserUtils.get_name i ^ "_hi") in
    let lo = transl_expr subst lo in
    let hi = transl_expr subst hi in
    let t = match t with
            | Ttyp t -> t
            | Tvar _ -> failwith "Error: A type variable has not been eliminated." in
    begin match coq_Sfor i i_hi lo hi d t s with
    | Errors.OK s -> s, PTree.set i t (PTree.set i_hi t te)
    | Errors.Error e -> A.error_loc l (A.Expected (t, A.integer_shape))
    end
  | A.Ifor_step (i, d, t, lo, hi, step, s) ->
    let s, te = transl_stmt pe te subst blocks s in
    let i_hi = ParserUtils.fresh_ident ("_" ^ ParserUtils.get_name i ^ "_hi") in
    let i_step = ParserUtils.fresh_ident ("_" ^ ParserUtils.get_name i ^ "_step") in
    let lo = transl_expr subst lo in
    let hi = transl_expr subst hi in
    let step = transl_expr subst step in
    let t = match t with
            | Ttyp t -> t
            | Tvar _ -> failwith "Error: A type variable has not been eliminated." in
    begin match coq_Sfor_step i i_hi i_step lo hi step d t s with
    | Errors.OK s -> s, PTree.set i t (PTree.set i_hi t (PTree.set i_step t te))
    | Errors.Error e -> A.error_loc l (A.Expected (t, A.integer_shape))
    end
  | A.Iwhile (c, s) ->
    let s, te = transl_stmt pe te subst blocks s in
    coq_Swhile (transl_expr subst c) s, te
  | A.Iloop s ->
    let s, te = transl_stmt pe te subst blocks s in
    Sloop s, te
  | A.Iblock (blkid, s) ->
    let s, te = transl_stmt pe te subst (blkid :: blocks) s in
    Sblock s, te
  | A.Iexit blkid ->
    match index_of blkid blocks with
    | Some n -> Sexit (Camlcoq.Nat.of_int n), te
    | None -> A.error_loc l (A.UnknownBlock (Camlcoq.extern_atom blkid))
    ; ;
  | A.Ierror -> Serror, te

let rec fold_liexpr (f: 'a -> BinNums.positive -> 'a) (a0: 'a) (e: A.liexpr) : 'a =
  match snd e with
  | A.Iconst_int _ | A.Iconst_float _ | A.Iconst_bool _ -> a0
  | A.Iacc (i, _) -> f a0 i
  | A.Icast (e, _, _) | A.Iunop (_, e, _) -> fold_liexpr f a0 e
  | A.Ibinop (_, e1, e2, _) ->
    let a = fold_liexpr f a0 e1 in
    fold_liexpr f a e2
  | A.Iecall _ -> a0

module WF = struct
  type error =
    | ParamDefinedTwice of string
    | NonParamSizeOfParam of string * string
    | MutableVarsInSize of string * string
    | NonPrimitiveRes of typ
    | FunDefinedTwice of string
  
  let error_loc l e =
    let f = file_loc l.loc_file in
    match e with
    | ParamDefinedTwice i ->
      fatal_error f "@[line %d, column %d@]: @,Parameter %s is defined twice.@.@.%a"
                    l.loc_lnum l.loc_cnum i A.pp_error_line l
    | NonParamSizeOfParam (i, sz) ->
      fatal_error f "@[line %d, column %d@]: @,Variable %s must be a parameter because it appears in size of parameter %s.@.@.%a"
                    l.loc_lnum l.loc_cnum sz i A.pp_error_line l
    | MutableVarsInSize (i, sz) ->
      fatal_error f "@[line %d, column %d@]: @,Variable %s must be immutable because it appears in size of %s.@.@.%a"
                    l.loc_lnum l.loc_cnum sz i A.pp_error_line l
    | NonPrimitiveRes t ->
      fatal_error f "@[line %d, column %d@]: @,Return type must be primitive. %s is not a primitive type.@.@.%a"
                    l.loc_lnum l.loc_cnum (PrintB.string_of_type t) A.pp_error_line l
    | FunDefinedTwice i ->
      fatal_error f "@[line %d, column %d@]: @,Function %s is defined twice.@.@.%a"
                    l.loc_lnum l.loc_cnum i A.pp_error_line l
   
  (** Checks that the sorted list [l] do not have any duplicates. *)
  let rec check_list_norepet loc ferr l =
    match l with
    | [] | _ :: [] -> ()
    | e1 :: e2 :: l ->
        if e1 = e2 then
          error_loc loc (ferr (ParserUtils.get_name e1))
        else check_list_norepet loc ferr (e2 :: l)
  
  (** Checks that the sorted lists [l1] and [l2] are disjoint. *)
  let rec check_list_disjoint loc l1 l2 =
    match l1, l2 with
    | [], _ | _, [] -> ()
    | e1 :: l1, e2 :: l2 ->
        let c = Camlcoq.P.compare e1 e2 in
        if c == 0 then assert false
          (* error_loc loc (ParamVarsNoDisjoint (ParserUtils.get_name e1)) *)
        else if c = 1 then check_list_disjoint loc l1 (e2 :: l2)
        else check_list_disjoint loc (e1 :: l1) l2

  let rec forall_vars f (exp: expr) =
    match exp with
    | Econst _ -> None
    | Eacc (i, l) ->
        if f i then
          let exception Invalid of string in
          try
            List.iter (function
              | Scell l -> List.iter (fun e ->
                  match forall_vars f e with
                  | None -> ()
                  | Some s -> raise (Invalid s)
                ) l
              ) l; None
          with Invalid i -> Some i
        else Some (ParserUtils.get_name i)
    | Ecast (e, _, _) -> forall_vars f e
    | Eunop (_, _, e) -> forall_vars f e
    | Ebinop_arith (_, _, e1, e2) | Ebinop_cmp (_, _, e1, e2) | Ebinop_cmpu (_, _, e1, e2) ->
        begin match forall_vars f e1 with
        | None -> forall_vars f e2
        | Some p -> Some p
        end

  (** Checks all size variables are U64. *)
  let check_param_size_of_params loc sze params =
    List.iter (fun p ->
      match PTree.get p sze with
      | Some llsz ->
        List.iter (List.iter (fun szexp ->
          match forall_vars (fun i -> List.mem i params) szexp with
          | None -> ()
          | Some i -> error_loc loc (NonParamSizeOfParam (ParserUtils.get_name p, i))
        )) llsz
      | None -> ()) params

  let make_safe_function loc sg params vars body tenv szenv szenv' penv =
    let params' = List.sort Camlcoq.P.compare params in
    check_list_norepet loc (fun i -> ParamDefinedTwice i) params';
    check_param_size_of_params loc szenv params;
    if Types.primitive_type sg.sig_res then ()
    else error_loc loc (NonPrimitiveRes sg.sig_res);
    make_function sg params vars body tenv szenv szenv' penv

  let make_safe_external_function loc name sg params tenv szenv penv =
    let params' = List.sort Camlcoq.P.compare params in
    check_list_norepet loc (fun i -> ParamDefinedTwice i) params';
    check_param_size_of_params loc szenv params;
    if Types.primitive_type sg.sig_res then ()
    else error_loc loc (NonPrimitiveRes sg.sig_res);
    make_external_function name sg params tenv szenv penv

  let make_safe_program loc fds main =
    let ifds = List.sort Camlcoq.P.compare (List.map fst fds) in
    check_list_norepet loc (fun i -> FunDefinedTwice i) ifds;
    make_program fds main

end

let transl_function (f: A.ifunction) : coq_function =
  let roparams = PTree.fold (fun rop i llsz ->
      let rop = match llsz with [] -> rop | _ -> PTree.set i () rop in
      List.fold_left (List.fold_left (fold_liexpr
        (fun rop sz -> 
          PTree.set sz () rop)
      )) rop llsz
    ) f.A.ifszenv PTree.empty in
  let subst, te = List.fold_left (fun (subst, te) i -> 
    match PTree.get i roparams with
    | Some _ -> (subst, te)
    | None ->
      let name = ParserUtils.get_name i in
      let i' = ParserUtils.fresh_ident ("__copy_" ^ name) in
      let subst = PTree.set i i' subst in
      let te = match PTree.get i te with
               | Some t -> PTree.set i' t te
               | None -> failwith "param not in tenv" in
      (subst, te)
  ) (PTree.empty, f.A.iftenv) f.A.ifparams in
  let s, te = transl_stmt f.A.ifpenv te subst [] f.A.ifbody in
  let s = PTree.fold (fun s i i' ->
      Sseq (Sassign ((i', []), Eacc (i, [])), s)
    ) subst s in
  let pparams = List.fold_left (fun p i -> PTree.set i () p) PTree.empty f.A.ifparams in
  let vars = List.filter_map (fun (i, _) ->
                 match PTree.get i pparams with
                 | None -> Some i
                 | Some _ -> None) (PTree.elements te) in
  let sze = List.fold_left (fun sze i -> match PTree.get i sze with
                                         | None -> PTree.set i [] sze
                                         | _ -> sze) f.A.ifszenv vars in
  let pe = List.fold_left (fun pe i -> match PTree.get i pe with
                                       | None -> PTree.set i Owned pe
                                       | _ -> pe) f.A.ifpenv vars in
  let szenv = PTree.map (fun _ llsz ->
      List.map (List.map (transl_expr subst)) llsz
    ) sze in
  let t = Hashtbl.create 10 in
  let szenv' = PTree.map (fun i llsz ->
      List.mapi (fun x -> List.mapi (fun y exp ->
          match PTree.get i pe with
          | Some Owned ->
              (* force owned arrays to have separated size variable
                 by generating a fresh one *)
              ParserUtils.fresh_ident
                (sprintf "%s_%d_%d" (ParserUtils.get_name i) (x + 1) (y + 1))
          | _ ->
            try Hashtbl.find t exp
            with Not_found ->
              let n = ParserUtils.fresh_ident
                (sprintf "%s_%d_%d" (ParserUtils.get_name i) (x + 1) (y + 1)) in
              Hashtbl.add t exp n;
              n
        )) llsz
    ) szenv in
  let t = Hashtbl.create 10 in
  let (szenv, szenv', tenv, penv, vars) = PTree.fold (fun x _ llsz ->
      List.fold_left (List.fold_left (fun (sze, sze', te, pe, vars) sz ->
        let vars = if Hashtbl.mem t sz then vars
                   else (Hashtbl.add t sz (); sz :: vars) in
        (PTree.set sz [] sze, PTree.set sz [] sze',
         PTree.set sz (Tint64 Unsigned) te, PTree.set sz Shared pe,
         vars))) x llsz
    ) szenv' (szenv, szenv', te, pe, vars) in
  let s = if f.A.ifsg.sig_res = Tvoid then Sseq (s, Sreturn None) else s in
  match WF.make_safe_function f.A.ifloc f.A.ifsg f.A.ifparams vars s tenv szenv szenv' penv with
  | Errors.OK f -> f
  | Errors.Error msg ->
      let loc = file_loc "" in
      fatal_error loc "%a" print_error msg

let transl_external_function name ef =
  let szenv = PTree.map (fun _ llsz ->
      List.map (List.map (transl_expr PTree.empty)) llsz
    ) ef.A.ieszenv in
  match WF.make_safe_external_function ef.A.ieloc name ef.A.iesg ef.A.ieparams ef.A.ietenv szenv ef.A.iepenv with
  | Errors.OK f -> f
  | Errors.Error msg ->
      let loc = file_loc "" in
      fatal_error loc "%a" print_error msg

let transl_fundef id (f: A.ifundef) : fundef =
  match f with
  | A.IInternal f -> Internal (transl_function f)
  | A.IExternal ef -> 
    let name = Camlcoq.coqstring_of_camlstring (Camlcoq.extern_atom id) in
    External (transl_external_function name ef)

let transl_program (p: A.iprogram) : program =
  let fds = List.map (fun (id, fd) -> (id, transl_fundef id fd)) p.A.ifundefs in
  match WF.make_safe_program p.A.iloc fds p.A.imain with
  | Errors.OK p -> p
  | Errors.Error msg ->
      let loc = file_loc "" in
      fatal_error loc "%a" print_error msg
