open Bast
open Iast
open Types
open Maps
open Format

open ParserUtils

open Berror
open Diagnostics

type tenv = ityp PTree.t
type szenv = (lexpr list list) PTree.t
type penv = Syntax.permission PTree.t
type nenv = loc PTree.t
type fenv = (signature * szenv * penv * ident list) PTree.t

exception UnificationError of typ * typ

module I = struct
  type int = string
  type float = float_info

  let string_of_int n = n
  let string_of_float f =
    if f.exp <> "0" then sprintf "%s.%se%s" f.intpart f.decpart f.exp
    else sprintf "%s.%s" f.intpart f.decpart
end

module IAST = Iast.AST(I)

open IAST

module ITyp = struct
  type t = ityp
  let compare x y =
    match x, y with
    | Tvar n1, Tvar n2 -> Int.compare n1 n2
    | Tvar _, Ttyp _ -> 1
    | Ttyp _, Tvar _ -> -1
    | Ttyp t1, Ttyp t2 ->
      if typ_beq t1 t2 then 0
      else raise (UnificationError (t1, t2))
end

module UEnv = BUnionFind.Make(ITyp)

let union = PTreeaux.coq_PTree_union

let fresh_tvar =
  let n = ref (-1) in
  fun () -> incr n; !n

let syntax_error_loc l msg =
  fatal_error (file_loc l.loc_file) "@[line %d, column %d@]: @,%s" l.loc_lnum l.loc_cnum msg

let rec get_type_syn_path l t0 d t ls =
  match ls, t with
  | [], _ -> t
  | Scell _ :: ls, Tarr t -> get_type_syn_path l t0 (d + 1) t ls
  | _, _ ->
    error_loc l (Expected (t0, Shape (Sarr (d + 1))))

let get_type (te: tenv) l (i, ls) =
  match PTree.get i te with
  | Some (Ttyp t) -> Ttyp (get_type_syn_path l t 0 t ls)
  | Some (Tvar t) -> begin match ls with
                     | [] -> Tvar t
                     | _  -> error_loc l InvalidAccessToPathInTvar
                     end
  | None -> error_loc l (VarNotExists (get_name i))

let rec get_type_isyn_path l t0 d t ls =
  match ls, t with
  | [], _ -> t
  | Icell _ :: ls, Tarr t -> get_type_isyn_path l t0 (d + 1) t ls
  | _, _ ->
    error_loc l (Expected (t0, Shape (Sarr (d + 1))))

let get_itype (te: tenv) l (i, ls) =
  match PTree.get i te with
  | Some (Ttyp t) -> Ttyp (get_type_isyn_path l t 0 t ls)
  | Some (Tvar t) -> begin match ls with
                     | [] -> Tvar t
                     | _  -> error_loc l InvalidAccessToPathInTvar
                     end
  | None -> error_loc l (VarNotExists (get_name i))

let rec get_size_syn_path llsz ls =
  match ls, llsz with
  | [], _ -> llsz
  | Scell _ :: ls, _ :: llsz -> get_size_syn_path llsz ls
  | _, _ -> assert false

let get_szenv_syn_path l sze (i, ls) =
  match PTree.get i sze with
  | Some llsz -> get_size_syn_path llsz ls
  | None -> let f = file_loc l.loc_file in
            fatal_error f "@[line %d, column %d@]: @,Undefined %s.@.@.%a"
                          l.loc_lnum l.loc_cnum (get_name i)
                          pp_error_line l

(* let check_initalized (ue: uenv) l i =
  match PTree.get i ue with
  | None -> ()
  | Some () -> error_loc l (VarNotInit (get_name i)) *)

let check_szvars_structure l sze0 sze args params =
  List.iter2 (fun a i ->
      match a with
      | (l, Eacc (j, ls)) ->
        let llsz0 = get_szenv_syn_path l sze0 (j, ls) in
        let llsz  = match PTree.get i sze with Some l -> l | _ -> assert false in
        List.iter2 (fun lsz0 lsz ->
            if List.length lsz0 <> List.length lsz then
              error_loc l (IncompatibleSizes (llsz0, llsz))
          ) llsz0 llsz
      | _ -> ()
    ) args params

let check_no_transfered_array_used_later ne_n pe args params =
  List.iter2 (fun (l, a) i ->
    match PTree.get i pe with
    | None -> failwith "unknown permission for parameter"
    | Some Syntax.Shared | Some Syntax.Mutable -> ()
    | Some Syntax.Owned ->
      match PTree.get i ne_n with
      | None -> ()
      | Some l' ->
        match a with
        | Eacc (i, _) ->
          error_loc l (OwnershipTransferUsedLater (get_name i, l'))
        | _ -> failwith "transfer of a non array object") args params

let check_permissions pe pe' args params =
  List.iter2 (fun (l, a) i ->
      match PTree.get i pe' with
      | None -> failwith "unknown permission for parameter"
      | Some p' ->
        let p = match a with
                | Eacc (i0, _) ->
                  match PTree.get i0 pe with
                  | None -> error_loc l (VarNotExists (get_name i0))
                  | Some p -> p
                  ; ;
                | _ -> Syntax.Owned in
        if Syntax.perm_le p' p then ()
        else error_loc l (InvalidPermission (p', p))
    ) args params

let rec erase_tvars u (l, ie) =
  let e = match ie with
    | Iconst_int (t, s) ->
      begin match UEnv.find u t with
      | Ttyp (Tint _) as t -> Iconst_int (t, s)
      | Ttyp (Tint64 _) as t -> Iconst_int (t, s)
      | Ttyp t -> error_loc l (ExprNotOfType (ie, t))
      | Tvar _ -> error_loc l (NotInferrable ie)
      end
    | Iconst_float (t, f) ->
      begin match UEnv.find u t with
      | Ttyp (Tfloat F32) as t -> Iconst_float (t, f)
      | Ttyp (Tfloat F64) as t -> Iconst_float (t, f)
      | Ttyp t -> error_loc l (ExprNotOfType (ie, t))
      | Tvar _ -> error_loc l (NotInferrable ie)
      end
    | Iconst_bool b -> Iconst_bool b
    | Iacc (i, ls) ->
      let ls = List.map (function
                         | Icell le -> Icell (List.map (erase_tvars u) le)
                        ) ls in
      Iacc (i, ls)
    | Icast (e, t1, t2) ->
      begin match UEnv.find u t1, UEnv.find u t2 with
      | (Ttyp _ as t1), (Ttyp _ as t2) -> Icast (erase_tvars u e, t1, t2)
      | _, _ -> error_loc l (NotInferrable (snd e))
      end
    | Iunop (op, e, t) ->
      begin match UEnv.find u t with
      | Ttyp _ as t -> Iunop (op, erase_tvars u e, t)
      | Tvar _ -> error_loc l (NotInferrable ie)
      end
    | Ibinop (op, e1, e2, t) ->
      begin match UEnv.find u t with
      | Ttyp _ as t -> Ibinop (op, erase_tvars u e1, erase_tvars u e2, t)
      | Tvar _ -> error_loc l (NotInferrable ie)
      end
    | Iecall (idf, args) -> Iecall (idf, List.map (erase_tvars u) args) in
  (l, e)

let erase_tvars_syn_path u (i, ls) =
  (i, List.map (function
                | Icell le -> Icell (List.map (erase_tvars u) le)
                ) ls)

let rec infer_expr' (fe: fenv) (sze: szenv) (pe: penv) (ne_n: nenv) (te: tenv)
                    (u: UEnv.t) (l, e) : nenv * liexpr * ityp =
  let infer_expr' = infer_expr' fe sze pe in
  match e with
  | Econst_int n ->
      let t = Tvar (fresh_tvar ()) in UEnv.add u t; PTree.empty, (l, Iconst_int (t, n)), t
  | Econst_float f ->
      let t = Tvar (fresh_tvar ()) in UEnv.add u t; PTree.empty, (l, Iconst_float (t, f)), t
  | Econst_annot_int (sz, sg, n) ->
      let t = Ttyp (Tint (sz, sg)) in UEnv.add u t; PTree.empty, (l, Iconst_int (t, n)), t
  | Econst_annot_int64 (sg, n) ->
      let t = Ttyp (Tint64 sg) in UEnv.add u t; PTree.empty, (l, Iconst_int (t, n)), t
  | Econst_annot_float (sz, f) ->
      let t = Ttyp (Tfloat sz) in UEnv.add u t; PTree.empty, (l, Iconst_float (t, f)), t
  | Econst_bool b ->
      let t = Ttyp Tbool in UEnv.add u t; PTree.empty, (l, Iconst_bool b), t
  | Eacc (i, ls) ->
      let t = get_type te l (i, ls) in
      (* check_initalized ue l i; *)
      let ne, ls = infer_syn_path_elem_list fe sze pe ne_n te u ls in
      UEnv.add u t;
      PTree.set i l ne, (l, Iacc (i, ls)), t
  | Ecast (e, t) ->
      let t = Ttyp t in UEnv.add u t;
      let ne, e, t' = infer_expr' ne_n te u e in
      ne, (l, Icast (e, t', t)), t
  | Eunop (op, e) ->
      let ne, e', t = infer_expr' ne_n te u e in
      let t' = match t with
        | Ttyp (Tint (_, sg)) -> Ttyp (Tint (I32, sg))
        | Ttyp Tbool | Ttyp (Tint64 _) | Ttyp (Tfloat _) -> t
        | Ttyp t -> error_loc l (InvalidApp (name_unop op, t))
        | Tvar _ -> error_loc l (NotInferrable (snd e'))
        in
      UEnv.add u t';
      ne, (l, Iunop (op, e', t')), t'
  | Ebinop (op, e1, e2) ->
      let ne2, e2', t2 = infer_expr' ne_n te u e2 in
      let ne1, e1', t1 = infer_expr' (union ne2 ne_n) te u e1 in
      let ne = union ne1 ne2 in
      begin match op with
      | Ogt | Oge | Ole | Olt | Oeq | One ->
        begin try UEnv.union u t1 t2
        with UnificationError(t1, t2) -> error_loc l (Expected (t2, Exact t1)) end;
        UEnv.add u (Ttyp Tbool);
        ne, (l, Ibinop (op, e1', e2', t1)), Ttyp Tbool
      | Oshl | Oshr ->
        let t' = match t1 with
          | Ttyp (Tint (sz, sg)) ->
            begin match sz with I32 -> Ttyp (Tint (sz, sg)) | _ -> Ttyp (Tint (I32, Signed)) end
          | Ttyp (Tint64 _) -> t1
          | Ttyp t -> error_loc l (InvalidApp (name_binop op, t))
          | Tvar _ -> error_loc l (NotInferrable (snd e1')) in
        begin match t2 with
        | Ttyp (Tint (_, Unsigned)) ->
          ne, (l, Ibinop (op, e1', e2', t1)), t'
        | Ttyp t -> error_loc l (Expected (t, Shape (Sint [I8; I16; I32])))
        | Tvar _ ->
          UEnv.add u (Ttyp (Tint (I32, Unsigned)));
          begin try UEnv.union u t2 (Ttyp (Tint (I32, Unsigned)));
          with UnificationError(t1, t2) -> error_loc l (Expected (t1, Exact t2)) end;
          ne, (l, Ibinop (op, e1', e2', t1)), t'
        end
      | _ ->
        begin try UEnv.union u t1 t2
        with UnificationError(t1, t2) -> error_loc l (Expected (t2, Exact t1)) end;
        let t' =
          match UEnv.find u t1 with
          | Ttyp (Tint (sz, sg)) ->
            begin match sz with I32 -> Ttyp (Tint (sz, sg)) | _ -> Ttyp (Tint (I32, Signed)) end
          | (Ttyp Tbool | Ttyp (Tint64 _) | Ttyp (Tfloat _)) as t -> t
          | Ttyp t -> error_loc l (InvalidApp ((name_binop op), t))
          | Tvar _ -> error_loc l (NotInferrableL ([snd e1'; snd e2'])) in
        UEnv.add u t';
        ne, (l, Ibinop (op, e1', e2', t1)), t'
      end
  | Ecall (idf, args) ->
    match PTree.get idf fe with
    | Some (sg, sze', pe', params) ->
      let nargs = List.length args in
      let nparams = List.length params in
      if nargs != nparams then error_loc l (WrongNbOfArguments (nparams, nargs));
      check_szvars_structure l sze sze' args params;
      check_no_transfered_array_used_later ne_n pe' args params;
      check_permissions pe pe' args params;
      let ne, args', _ = infer_exprlist fe sze pe ne_n te u args
                                    (List.map (fun t -> Ttyp t) sg.sig_args) in
      ne, (l, Iecall (idf, args')), Ttyp sg.sig_res
    | None -> error_loc l (FunNotExists (Camlcoq.extern_atom idf))
and infer_syn_path_elem_list (fe: fenv) (sze: szenv) (pe: penv) (ne_n: nenv) (te: tenv) (u: UEnv.t) ls =
  match ls with
  | [] -> PTree.empty, []
  | Scell le :: ls ->
    let ne, lidx, tlidx = infer_exprlist' fe sze pe ne_n te u le in
    UEnv.add u (Ttyp (Tint64 Unsigned));
    let lidx = List.map2 (fun (l, e) -> function
                           | Ttyp (Tint64 Unsigned) -> (l, e)
                           | Ttyp (Tint64 Signed) as t ->
                             (l, Icast ((l, e), t, Ttyp (Tint64 Unsigned)))
                           | Ttyp (Tint (sz, sg)) as t ->
                             UEnv.add u (Ttyp (Tint64 sg));
                             (l, Icast ((l, (Icast ((l, e), t, Ttyp (Tint64 sg)))),
                                        Ttyp (Tint64 sg),
                                        Ttyp (Tint64 Unsigned)))
                           | t -> try  UEnv.union u t (Ttyp (Tint64 Unsigned)); (l, e)
                                  with UnificationError(t1, t2) ->
                                    error_loc l (Expected (t2, Exact t1))) lidx tlidx in
    let nels, ls' = infer_syn_path_elem_list fe sze pe ne_n te u ls in
    union ne nels, (Icell lidx :: ls')
and infer_exprlist' (fe: fenv) (sze: szenv) (pe: penv) (ne_n: nenv) (te: tenv) (u: UEnv.t) le =
  List.fold_right (fun e (ne, le, lt) ->
      let ne', e, t = infer_expr' fe sze pe ne_n te u e in
      (union ne' ne, e :: le, t :: lt)) le (PTree.empty, [], [])

and infer_expr fe sze pe ne_n te u (e: lexpr) (rt: ityp) : nenv * liexpr * ityp =
  let ne, e', t = infer_expr' fe sze pe ne_n te u e in
  UEnv.add u rt;
  begin try UEnv.union u t rt
  with UnificationError(t1, t2) -> error_loc (fst e) (Expected (t1, Exact t2)) end;
  ne, e', UEnv.find u t

and infer_exprlist fe sze pe ne_n te u (le: lexpr list) (lrt: ityp list) =
  List.fold_right2 (fun e rt (ne, le, lt) ->
    let ne', e, t = infer_expr fe sze pe ne_n te u e rt in
    (union ne' ne, e :: le, t :: lt)) le lrt (PTree.empty, [], [])

let infer_syn_path fe sze pe ne_n te u (i, ls) =
  let ne, tls = infer_syn_path_elem_list fe sze pe ne_n te u ls in
  ne, (i, tls)

(*****************************************************************************)
(*                                                                           *)
(*                             WELL FORMEDNESS                               *)
(*                                                                           *)
(*****************************************************************************)

type wf_ret = Normal | Exit of ident | Return

module WFSet = Set.Make (struct
  type t = wf_ret
  let compare t t' =
    match t, t' with
    | Normal, Normal -> 0
    | Normal, _ -> -1
    | Exit _, Normal -> 1
    | Exit n, Exit m -> Camlcoq.P.compare n m
    | Exit _, Return -> -1
    | Return, Return -> 0
    | Return, _ -> 1
end)

let rec possible_results f fname currloop s =
  let possible_results = possible_results f fname in
  match snd s with
  | Sskip | Sassign _ | Scall _ | Sassert _ | Sdecl _
  | Salloc _ | Sfree _ -> WFSet.singleton Normal
  | Sreturn _ -> WFSet.singleton Return
  | Sexit n -> WFSet.singleton (Exit n)
  | Sbreak blkid ->
    begin match currloop with
    | None -> syntax_error_loc f.floc ("In function" ^ (Camlcoq.extern_atom fname) ^
                                       ", there is a break outside of a loop.")
    | Some id ->
      match blkid with
      | None    -> WFSet.singleton (Exit id)
      | Some id -> WFSet.singleton (Exit id)
    end
  | Scontinue blkid ->
    begin match currloop with
    | None -> syntax_error_loc f.floc ("In function" ^ (Camlcoq.extern_atom fname) ^
                                       ", there is a continue outside of a loop.")
    | Some id ->
      match blkid with
      | None ->
        let name = Camlcoq.extern_atom id in
        let id = Camlcoq.intern_string (name ^ "_loop_in") in
        WFSet.singleton (Exit id)
      | Some id -> WFSet.singleton (Exit id)
    end
  | Serror -> WFSet.empty
  | Sseq (s1, s2) ->
    let p = possible_results currloop s1 in
    if WFSet.mem Normal p
    then WFSet.union (WFSet.remove Normal p) (possible_results currloop s2)
    else p
  | Sifthenelse (_, s1, s2) ->
    WFSet.union (possible_results currloop s1) (possible_results currloop s2)
  | Sblock (blkid, (_,
            (Sloop s | Swhile (_, s)
             | Sfor (_, _, _, _, _, s) | Sfor_step (_, _, _, _, _, _, s)))) ->
    let p = possible_results (Some blkid) s in
    if WFSet.mem (Exit blkid) p
    then WFSet.(p |> remove (Exit blkid) |> add Normal)
    else WFSet.(p |> remove Normal)
  | Sblock (id, s) ->
    let p = possible_results currloop s in
    if WFSet.mem (Exit id) p
    then WFSet.(p |> remove (Exit id) |> add Normal)
    else WFSet.(p |> remove (Exit id))
  | Sloop _ | Sfor _ | Sfor_step _ | Swhile _ -> failwith "loop outside of a block"

let no_normal_or_exit out =
  not (WFSet.exists (function Normal | Exit _ -> true | _ -> false) out)

let well_formed f fname inloop s =
  no_normal_or_exit (possible_results f fname inloop s)

let check_well_formed_function f fname =
  let p = possible_results f fname None f.fbody in
  if WFSet.mem Normal p then
    match f.fret with
    | Tvoid -> ()
    | _ -> syntax_error_loc f.floc
             ("In function " ^ (Camlcoq.extern_atom fname) ^
              ", there is a return missing.\n\
               Only functions which return type is void are allowed not to\
               have a return in every branch.")
  ; ;
  WFSet.iter (function
    | Exit id ->
      syntax_error_loc f.floc ("In function " ^ (Camlcoq.extern_atom fname) ^
                               ", an instruction exit " ^ Camlcoq.extern_atom id ^
                               " does not correspond to any parent block.")
    | _ -> ()) p

(*****************************************************************************)
(*                                                                           *)
(*                     TYPE INFERENCE FOR STATEMENTS                         *)
(*                                                                           *)
(*****************************************************************************)

let empty : nenv = PTree.empty

let pTree_difference t1 t2 =
  PTree.combine (fun x1 x2 -> match x1, x2 with
                              | Some x, None -> Some x
                              | _ -> None) t1 t2

let rec infer_stmt (fe: fenv) (u: UEnv.t) (sze: szenv) (te: tenv) (pe: penv)
                   (ne_n: nenv) (ne_e: (ident * (bool * nenv)) list) (ne_r: nenv)
                   (rtype: typ) (l, s)
                   : listmt * szenv * tenv * penv * nenv =
  let infer_stmt = infer_stmt fe u in
  match s with
  | Sskip -> ((l, Iskip), sze, te, pe, ne_n)
  | Sdecl (id, t, llsz) ->
    let t = match t with
            | Some t -> Ttyp t
            | None -> Tvar (fresh_tvar ()) in
    UEnv.add u t;
    let te = match PTree.get id te with
             | None -> PTree.set id t te
             | Some t' ->
               try UEnv.union u t t'; te
               with UnificationError _ ->
                 syntax_error_loc l ("Variable is already declared with another type.") in
    let sze = match PTree.get id sze with
              | None -> PTree.set id llsz sze
              | Some llsz' ->
                if llsz = llsz' then sze
                else syntax_error_loc l ("Variable is already declared with a different size expression.") in
    let pe = match PTree.get id pe with
             | None -> PTree.set id Syntax.Owned pe
             | Some Syntax.Owned -> pe
             | Some _ ->
               syntax_error_loc l ("Variable is already declared as borrow.") in
    ((l, Iskip), sze, te, pe, PTree.remove id ne_n)
  | Sassign ((i, ls), e) ->
    let t =  get_type te l (i, ls) in
    let ne'_n = match ls with [] -> PTree.remove i ne_n | _ -> ne_n in
    let ne1, (i, ls) = infer_syn_path fe sze pe ne_n te u (i, ls) in
    let ne2, e, _ = infer_expr fe sze pe ne_n te u e t in
    (l, Iassign ((i, ls), e)), sze, te, pe, union (union ne1 ne2) ne'_n
  | Salloc i ->
    begin match PTree.get i te, PTree.get i sze with
    | Some (Ttyp (Tarr t)), Some [[szexpr]] ->
      let ne, _, _ = infer_expr fe sze pe ne_n te u szexpr (Ttyp (Tint64 Unsigned)) in
      if primitive_type t && t <> Tvoid then
        (l, Ialloc i), sze, te, pe, union ne (PTree.remove i ne_n)
      else error_loc l (NonPrimitiveType t)
    | Some (Ttyp (Tarr _)), Some _ ->
      fatal_error (file_loc l.loc_file) "@[line %d, column %d@]: @,Invalid size variables list for %s.@.@.%a"
                  l.loc_lnum l.loc_cnum (get_name i) pp_error_line l
    | Some (Ttyp t), _ -> error_loc l (Expected (t, Shape (Sarr 1)))
    | Some (Tvar _), _ -> error_loc l ArrayTvar
    | _, _ -> error_loc l (VarNotExists (get_name i))
    end
  | Sfree i ->
    begin match PTree.get i te, PTree.get i sze with
    | Some (Ttyp (Tarr t)), Some [[szexpr]] -> 
      begin match PTree.get i ne_n with
      | None ->
        let _ = infer_expr fe sze pe ne_n te u szexpr (Ttyp (Tint64 Unsigned)) in
        if primitive_type t && t <> Tvoid then
          (l, Ifree i), sze, te, pe, PTree.set i l ne_n
        else error_loc l (NonPrimitiveType t)
      | Some _ -> error_loc l (VarNotInit (get_name i))
      end
    | Some (Ttyp (Tarr _)), Some _ ->
      fatal_error (file_loc l.loc_file) "@[line %d, column %d@]: @,Invalid size variables list for %s.@.@.%a"
                  l.loc_lnum l.loc_cnum (get_name i) pp_error_line l
    | Some (Ttyp t), _ -> error_loc l (NonPrimitiveType t)
    | Some (Tvar _), _ -> error_loc l ArrayTvar
    | _, _ -> error_loc l (VarNotExists (get_name i))
    end
  | Sassert e ->
    let ne, e, _ = infer_expr fe sze pe ne_n te u e (Ttyp Tbool) in
    (l, Iassert (e)), sze, te, pe, union ne ne_n
  | Scall (idopt, idf, args) ->
    begin match PTree.get idf fe with
    | Some (sg, sze', pe', params) ->
      let nargs = List.length args in
      let nparams = List.length params in
      if nargs != nparams then error_loc l (WrongNbOfArguments (nparams, nargs));
      let ne, args', _ = infer_exprlist fe sze pe ne_n te u args
                                        (List.map (fun t -> Ttyp t) sg.sig_args) in
      check_szvars_structure l sze sze' args params;
      check_no_transfered_array_used_later ne_n pe' args params;
      check_permissions pe pe' args params;
      let ne_n' = match idopt with
                  | Some i -> PTree.remove i ne
                  | None -> ne_n in
      let () = match idopt with
               | Some i ->
                  ignore (get_type te l (i, []))
               | None -> () in
      ((l, Icall (idopt, idf, args')), sze, te, pe, union ne ne_n')
    | None ->
      error_loc l (FunNotExists (Camlcoq.extern_atom idf))
    end
  | Sreturn None ->
    ((l, Ireturn None), sze, te, pe, ne_r)
  | Sreturn (Some e) ->
    let ne, e, _ = infer_expr fe sze pe empty te u e (Ttyp rtype) in
    ((l, Ireturn (Some e)), sze, te, pe, union ne ne_r)
  | Sseq (s1, s2) ->
    (* 2 calls to infer s1 because needed variables are computed backward,
       but szenv, tenv and penv and computed forward. *)
    let   _, sze1, te1, pe1, _   = infer_stmt sze te pe ne_n ne_e ne_r rtype s1 in
    let s2', sze2, te2, pe2, ne2 = infer_stmt sze1 te1 pe1 ne_n ne_e ne_r rtype s2 in
    let s1', sze', te', pe', ne' = infer_stmt sze2 te2 pe2 ne2 ne_e ne_r rtype s1 in
    ((l, Iseq (s1', s2')), sze', te', pe', ne')
  | Sifthenelse (c, strue, sfalse) ->
    let strue,  sze1, te1, pe1, ne1_n = infer_stmt sze  te  pe  ne_n ne_e ne_r rtype strue  in
    let sfalse, sze2, te2, pe2, ne2_n = infer_stmt sze1 te1 pe1 ne_n ne_e ne_r rtype sfalse in
    let ne'_n = union ne1_n ne2_n in
    let ne, c, _ = infer_expr fe sze pe ne'_n te u c (Ttyp Tbool) in
    ((l, Iifthenelse (c, strue, sfalse)), sze2, te2, pe2, union ne ne'_n)
  | Sblock (blkid, (l', s)) ->
    begin match s with
    | Sfor (id, d, t, lo, hi, s) ->
      let t = match t with
              | Some t -> Ttyp t
              | None -> Tvar (fresh_tvar ()) in
      UEnv.add u t;
      let te    = PTree.set id t te in
      let sze   = PTree.set id [] sze in
      let pe    = PTree.set id Syntax.Owned pe in
      let ne_e  = (blkid, (true, ne_n)) :: ne_e in
      let ts, sze, te', pe', ne1_n = infer_stmt sze te pe empty ne_e ne_r rtype s in
      let  _,   _,   _,   _, ne2_n = infer_stmt sze te pe ne1_n ne_e ne_r rtype s in
      let nelo, lo, _ = infer_expr fe sze pe ne2_n te u lo t in
      let nehi, hi, _ = infer_expr fe sze pe ne2_n te u hi t in
      let ne = PTree.remove id ne2_n in
      ((l, Iblock (blkid, (l', Ifor (id, d, t, lo, hi, ts)))), sze, te', pe', union (union nelo nehi) ne)
    | Sfor_step (id, d, t, lo, hi, step, s) ->
      let t = match t with
              | Some t -> Ttyp t
              | None -> Tvar (fresh_tvar ()) in
      UEnv.add u t;
      let te    = PTree.set id t te in
      let sze   = PTree.set id [] sze in
      let pe    = PTree.set id Syntax.Owned pe in
      let ne_e  = (blkid, (true, ne_n)) :: ne_e in
      let ts, sze, te', pe', ne1_n = infer_stmt sze te pe empty ne_e ne_r rtype s in
      let  _,   _,   _,   _, ne2_n = infer_stmt sze te pe ne1_n ne_e ne_r rtype s in
      let ne = PTree.remove id ne2_n in
      let nelo, lo, _ = infer_expr fe sze pe ne te u lo t in
      let nehi, hi, _ = infer_expr fe sze pe ne te u hi t in
      let nestep, step, _ = infer_expr fe sze pe ne te u step t in
      ((l, Iblock (blkid, (l', Ifor_step (id, d, t, lo, hi, step, ts)))), sze, te', pe',
      union (union (union nelo nehi) nestep) ne)
    | Swhile (c, s) ->
      let ne_e  = (blkid, (true, ne_n)) :: ne_e in
      let ts, sze, te, pe, ne1_n = infer_stmt sze te pe empty ne_e ne_r rtype s in
      let   _,  _,  _,  _, ne2_n = infer_stmt sze te pe ne1_n ne_e ne_r rtype s in
      let nec, c, _ = infer_expr fe sze pe ne2_n te u c (Ttyp Tbool) in
      ((l, Iblock (blkid, (l', Iwhile (c, ts)))), sze, te, pe, union nec ne2_n)
    | Sloop s ->
      let ne_e  = (blkid, (true, ne_n)) :: ne_e in
      let ts, sze, te, pe, ne1_n = infer_stmt sze te pe empty ne_e ne_r rtype s in
      let  _,   _,  _,  _, ne2_n = infer_stmt sze te pe ne1_n ne_e ne_r rtype s in
      ((l, Iblock (blkid, (l', Iloop ts))), sze, te, pe, ne2_n)
    | _ ->
      let ne_e  = (blkid, (false, ne_n)) :: ne_e in
      let ts, sze, te, pe, ne = infer_stmt sze te pe ne_n ne_e ne_r rtype (l', s) in
      ((l, Iblock (blkid, ts)), sze, te, pe, ne)
    end
  | Sfor _ | Sfor_step _ | Swhile _ | Sloop _ -> failwith "loop outside of a block"
  | Scontinue blkid ->
    let blkid =
      match blkid with
      | Some id -> id
      | None ->
        List.find (fun (_, (b, _)) -> b) ne_e |> fst in
    let loop_name = Camlcoq.extern_atom blkid in
    let blkid' = Camlcoq.intern_string (loop_name ^ "_loop_in") in
    ((l, Iexit blkid'), sze, te, pe, List.assoc blkid' ne_e |> snd)
  | Sbreak blkid    ->
    let blkid =
      match blkid with
      | Some id -> id
      | None ->
        List.find (fun (_, (b, _)) -> b) ne_e |> fst in
    ((l, Iexit blkid), sze, te, pe, List.assoc blkid ne_e |> snd)
  | Sexit blkid ->
    ((l, Iexit blkid), sze, te, pe, List.assoc blkid ne_e |> snd)
  | Serror    ->
    ((l, Ierror), sze, te, pe, empty)

let rec erase_tvars_stmt (te: tenv) (u: UEnv.t) ((l, s): listmt) : tenv * listmt =
  match s with
  | Iskip -> te, (l, Iskip)
  | Idecl (i, t) ->
      let t = match PTree.get i te with
              | Some t -> t
              | None -> assert false in
      let t = match UEnv.find u t with
              | Ttyp t -> t
              | Tvar _ -> error_loc l (NotInferrableVar (ParserUtils.get_name i)) in
      PTree.set i (Ttyp t) te, (l, Idecl (i, t))
  | Iassign ((i, ls), e) ->
      let te, t = match PTree.get i te with
                  | Some (Ttyp _ as t ) -> te, t
                  | Some (Tvar _ as t ) -> PTree.set i (UEnv.find u t) te, t
                  | None -> assert false in
      let t = match get_itype te l (i, ls) with
              | Ttyp t -> t
              | Tvar _ -> error_loc l (NotInferrableVar (ParserUtils.get_name i)) in
      if primitive_type t then ()
      else error_loc l (NonPrimitiveType t);
      te, (l, Iassign (erase_tvars_syn_path u (i, ls), erase_tvars u e))
  | Iassert c -> te, (l, Iassert (erase_tvars u c))
  | Ialloc i -> te, (l, Ialloc i)
  | Ifree i -> te, (l, Ifree i)
  | Icall (None, f, args) ->
      te, (l, Icall (None, f, List.map (erase_tvars u) args))
  | Icall (Some i, f, args) ->
      let t = match PTree.get i te with
              | Some t -> t
              | None -> assert false in
      let t = match UEnv.find u t with
              | Ttyp t -> t
              | Tvar _ -> error_loc l (NotInferrableVar (ParserUtils.get_name i)) in
      if primitive_type t then ()
      else error_loc l (NonPrimitiveType t);
      PTree.set i (Ttyp t) te, (l, Icall (Some i, f, List.map (erase_tvars u) args))
  | Ireturn None -> te, (l, Ireturn None)
  | Ireturn (Some e) ->
      te, (l, Ireturn (Some (erase_tvars u e)))
  | Iseq (s1, s2) ->
      let te, s1 = erase_tvars_stmt te u s1 in
      let te, s2 = erase_tvars_stmt te u s2 in
      te, (l, Iseq (s1, s2))
  | Iifthenelse (c, s1, s2) ->
      let te, s1 = erase_tvars_stmt te u s1 in
      let te, s2 = erase_tvars_stmt te u s2 in
      te, (l, Iifthenelse (erase_tvars u c, s1, s2))
  | Ifor (i, b, t, lo, hi, s) ->
      let t = match PTree.get i te with
              | Some t -> t
              | None -> assert false in
      let t = match UEnv.find u t with
              | Ttyp t -> Ttyp t
              | Tvar _ -> error_loc l (NotInferrableVar (ParserUtils.get_name i)) in
      let te, s = erase_tvars_stmt te u s in
      PTree.set i t te, (l, Ifor (i, b, t, erase_tvars u lo, erase_tvars u hi, s))
  | Ifor_step (i, b, t, lo, hi, step, s) ->
      let t = match PTree.get i te with
              | Some t -> t
              | None -> assert false in
      let t = match UEnv.find u t with
              | Ttyp t -> Ttyp t
              | Tvar _ -> error_loc l (NotInferrableVar (ParserUtils.get_name i)) in
      let te, s = erase_tvars_stmt te u s in
      PTree.set i t te, (l, Ifor_step (i, b, t, erase_tvars u lo, erase_tvars u hi, erase_tvars u step, s))
  | Iwhile (c, s) ->
      let te, s = erase_tvars_stmt te u s in
      te, (l, Iwhile (erase_tvars u c, s))
  | Iloop s ->
      let te, s = erase_tvars_stmt te u s in
      te, (l, Iloop s)
  | Iblock (blkid, s) ->
      let te, s = erase_tvars_stmt te u s in
      te, (l, Iblock (blkid, s))
  | Iexit blkid -> te, (l, Iexit blkid)
  | Ierror -> te, (l, Ierror)

let infer_function (fe: fenv) (name: ident) (f: fct) : ifunction =
  check_well_formed_function f name;
  let te = List.fold_left (fun te (id, t) -> PTree.set id (Ttyp t) te)
                          PTree.Empty f.fparams in
  let szenv = List.fold_left (fun sze (id, lids) -> PTree.set id lids sze)
                              PTree.Empty f.fszvars in
  let penv  = List.fold_left (fun pe  (id, p) ->
                  PTree.set id p pe) PTree.Empty f.fperms in
  let u = UEnv.create () in
  let ifbody, ifszenv, iftenv, ifpenv, ne = infer_stmt fe u szenv te penv empty [] empty f.fret f.fbody in
  PTree.fold (fun () i loc ->
    if List.mem_assoc i f.fparams then ()
    else
      error_loc loc (VarNotInit (get_name i))) ne ();
  let iftenv, ifbody = erase_tvars_stmt iftenv u ifbody in
  let iftenv = PTree.map (fun i t -> match t with
                                     | Ttyp t -> Ttyp t
                                     | Tvar _ -> match UEnv.find u t with
                                                 | Ttyp t -> Ttyp t
                                                 | Tvar _ -> error_loc f.floc (NotInferrableVar (ParserUtils.get_name i))) iftenv in
  let ifszenv = PTree.map (fun id llsz ->
    List.map (List.map (fun sz ->
        let t = Tvar (fresh_tvar ()) in
        UEnv.add u t;
        let _, szexpr, _ = infer_expr fe szenv penv empty iftenv u sz t in
        match UEnv.find u t with
        | Ttyp _ as t -> let l, _ = szexpr in l, Icast (szexpr, t, Ttyp (Tint64 Unsigned))
        | Tvar _ ->
          UEnv.union u t (Ttyp (Tint64 Unsigned)); szexpr
      )) llsz) ifszenv in
  let ifszenv = PTree.map (fun i llsz ->
    List.map (List.map (fun szexp -> erase_tvars u szexp)) llsz) ifszenv in
  let ifsg = { Types.sig_args = List.map snd f.fparams;
               Types.sig_res = f.fret } in
  let ifszenv = PTree.fold (fun sze id _ ->
                  match PTree.get id sze with
                    None -> PTree.set id [] sze | _ -> sze) iftenv ifszenv in
  let iftenv = PTree.map (fun i t -> match t with
                                     | Ttyp t -> t
                                     | Tvar _ -> assert false) iftenv in
  List.iter (fun (i, p) -> 
    match p with
    | Syntax.Owned -> failwith "Ownership transfer is not supported yet."
    | Syntax.Mutable -> begin match PTree.get i iftenv with
                        | Some (Tarr _) -> ()
                        | _ -> syntax_error_loc f.floc
                                 "Only arrays can be mutable in parameters."
                        end
    | _ -> ()) f.fperms;
  (* let ifpenv  = PTree.fold (fun pe id _ ->
                  match PTree.get id pe with
                    None -> PTree.set id Syntax.Owned pe | _ -> pe) te penv in *)
  let ifparams = List.map fst f.fparams in
  let ifloc = f.floc in
  { ifloc; ifparams; ifsg; iftenv; ifbody; ifszenv; ifpenv }

let make_external_function (fe: fenv) (name: ident) (f: ef) : efunction =
  let ietenv = List.fold_left (fun te (id, t) -> PTree.set id t te)
                          PTree.Empty f.eparams in
  let ietenv' = List.fold_left (fun te (id, t) -> PTree.set id (Ttyp t) te)
                          PTree.Empty f.eparams in
  let szenv = List.fold_left (fun sze (id, lids) -> PTree.set id lids sze)
                              PTree.Empty f.eszvars in
  let iepenv  = List.fold_left (fun pe  (id, p) ->
                  PTree.set id p pe) PTree.Empty f.eperms in
  let u = UEnv.create () in
  let ieszenv = List.fold_left (fun sze (id, llsz) ->
      let llsz = List.map (List.map (fun sz ->
          let t = Tvar (fresh_tvar ()) in
          UEnv.add u t;
          let _, szexpr, _ = infer_expr fe szenv iepenv empty ietenv' u sz t in
          match UEnv.find u t with
          | Ttyp _ as t -> let l, _ = szexpr in l, Icast (szexpr, t, Ttyp (Tint64 Unsigned))
          | Tvar _ ->
            UEnv.union u t (Ttyp (Tint64 Unsigned)); szexpr
        )) llsz in
      PTree.set id llsz sze) PTree.Empty f.eszvars in
  let ieszenv = PTree.map (fun i llsz ->
    List.map (List.map (fun szexp -> erase_tvars u szexp)) llsz) ieszenv in
  let iesg = { Types.sig_args = List.map snd f.eparams;
               Types.sig_res = f.eret } in
  List.iter (fun (i, p) -> 
    match p with
    | Syntax.Owned -> failwith "Ownership transfer is not supported yet."
    | Syntax.Mutable -> begin match PTree.get i ietenv with
                        | Some (Tarr _) -> ()
                        | _ -> syntax_error_loc f.eloc
                                 "Only arrays can be mutable in parameters."
                        end
    | _ -> ()) f.eperms;
  let ieparams = List.map fst f.eparams in
  let ieloc = f.eloc in
  { ieloc; ieparams; iesg; ietenv; ieszenv; iepenv }

let fd_sig (fd: fundef) =
  match fd with
  | Internal f ->
    { Types.sig_args = List.map snd f.fparams;
      Types.sig_res  = f.fret }
  | External ef ->
    { Types.sig_args = List.map snd ef.eparams;
      Types.sig_res  = ef.eret }

let ptree_of_list l =
  List.fold_left (fun sze (id, lids) -> PTree.set id lids sze)
                 PTree.Empty l

let fd_szenv (fd: fundef) =
  let szlist = match fd with Internal f -> f.fszvars | External e -> e.eszvars in
  ptree_of_list szlist

let fd_penv (fd: fundef) =
  let plist = match fd with Internal f -> f.fperms | External e -> e.eperms in
  ptree_of_list plist

let fd_params = function
  | Internal f -> f.fparams
  | External e -> e.eparams

let make_fenv (lfd: (ident * fundef) list) =
  List.fold_left (fun e (id, fd) ->
      let s = fd_sig fd in
      let sze = fd_szenv fd in
      let pe = fd_penv fd in
      let params = List.map fst (fd_params fd) in
      PTree.set id (s, sze, pe, params) e) PTree.Empty lfd

let infer_fundef (fe: fenv) (name: ident) (fd: fundef) =
  match fd with
  | Internal f -> IInternal (infer_function fe name f)
  | External ef -> IExternal (make_external_function fe name ef)

let infer_program (p: program) : iprogram =
  let fe = make_fenv p.fundefs in
  let fds = List.map (fun (id, fd) ->
    (id, infer_fundef fe id fd)) p.fundefs in
  { iloc = p.ploc; ifundefs = fds; imain = p.main }
