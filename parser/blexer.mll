{
    open Bparser
    open Types

    let keywords = Hashtbl.create 30
    let () = List.iter (fun (s,t) -> Hashtbl.add keywords s t)
        ["fun", FUN; "for", FOR; "while", WHILE;
         "if", IF; "else", ELSE;
         "let", LET;
         "decr", DECR; "step", STEP;
         "return", RETURN; "extern", EXTERN;
         "continue", CONTINUE; "break", BREAK; "exit", EXIT;
         "skip", SKIP;
         "mut", MUTABLE; "own", OWNED;
         "true", BOOL true; "false", BOOL false;
         "assert", ASSERT; "error", ERROR;
         "alloc", ALLOC; "free", FREE]

    let types = Hashtbl.create 10
    let () = List.iter (fun (s,t) -> Hashtbl.add types s t)
        [ "u8", Tint ( I8, Unsigned);  "i8", Tint ( I8, Signed);
         "u16", Tint (I16, Unsigned); "i16", Tint (I16, Signed);
         "u32", Tint (I32, Unsigned); "i32", Tint (I32, Signed);
         "u64", Tint64 Unsigned;      "i64", Tint64 Signed;
         "f32" , Tfloat F32;          "f64", Tfloat F64;
         "void", Tvoid;               "bool", Tbool]

    let backjump lexbuf chars =
      let open Lexing in
      if chars < 0 || chars > lexbuf.lex_curr_pos - lexbuf.lex_start_pos then
        invalid_arg "Lexlib.backjump";
      let pos = lexbuf.lex_curr_p in
      lexbuf.lex_curr_pos <- lexbuf.lex_curr_pos - chars;
      lexbuf.lex_curr_p <- { pos with pos_cnum = pos.pos_cnum - chars }
}

let letters = ['a'-'z' 'A'-'Z']
let digits  = ['0'-'9']
let number  = digits+
let whitespaces = [' ' '\t']

rule token = parse
  | '\n'   { Lexing.new_line lexbuf; token lexbuf }
  | whitespaces+ {
      token lexbuf
    }
  | ','    { COMMA  }
  | ':'    { COLON  }
  | ';'    { SEMICOLON }
  | "//"   { linecomment lexbuf }
  | "/*"   { comment 1 lexbuf}
  | "=="   { EQUAL  }
  | "!="   { NE     }
  | ">="   { GE     }
  | "<="   { LE     }
  | '>'    { GT     }
  | '<'    { LT     }
  | '!'    { NOT    }
  | "&&"   { BAND   }
  | "||"   { BOR    }
  | '&'    { AND    }
  | '|'    { OR     }
  | '^'    { XOR    }
  | '+'    { PLUS   }
  | '-'    { MINUS  }
  | '*'    { STAR   }
  | '/'    { DIV    }
  | '%'    { MOD    }
  | '~'    { TILDE  }
  | "<<"   { SHL    }
  | ">>"   { SHR    }
  | '['    { LSQ    }
  | ']'    { RSQ    }
  | '('    { LPAR   }
  | ')'    { RPAR   }
  | '{'    { LCU    }
  | '}'    { RCU    }
  | "="    { ASSIGN }
  | ".."   { RANGE  }
  | "->"   { RIGHTARROW }
  | (letters|'_') (letters|digits|'_')* as s
      { try Hashtbl.find keywords s
        with Not_found ->
          try TYPE (Hashtbl.find types s)
          with Not_found -> IDENT s }
  | (("-"? digits+) as n) (("u" | "i") as sg) (("8" | "16" | "32" | "64") as sz)
      { ANNOT_INT (n, sz, Bast.signedness_of_string (String.make 1 sg)) }
  | (("-"? digits+) as n) ".."
      { backjump lexbuf 2; INT n }
  | (("-"? digits+) as n)
      { INT n }
  | ((("-" as sign)? ((digits+ as ip) '.' (digits* as dp)) ("e" (("-"? digits+) as exp))?)
     | (("-" as sign)? ((digits* as ip) '.' (digits+ as dp)) ("e" (("-"? digits+) as exp))?)) "f" (("32" | "64") as sz)
      { let fi = { Bast.sign = Option.fold ~none:false ~some:(fun _ -> true) sign;
                   Bast.intpart = ip;
                   Bast.decpart = dp;
                   Bast.exp = Option.fold ~none:"0" ~some:(fun id -> id) exp } in
        ANNOT_FLOAT (fi, Bast.floatsize_of_string sz) }
  | ((("-" as sign)? ((digits+ as ip) '.' (digits* as dp)) ("e" (("-"? digits+) as exp))?)
     | (("-" as sign)? ((digits* as ip) '.' (digits+ as dp)) ("e" (("-"? digits+) as exp))?))
      { let fi = { Bast.sign = Option.fold ~none:false ~some:(fun _ -> true) sign;
                   Bast.intpart = ip;
                   Bast.decpart = dp;
                   Bast.exp = Option.fold ~none:"0" ~some:(fun id -> id) exp } in
        FLOAT fi }
  | eof   { EOF }
  | _ { failwith "lexical error" }

and comment lvl = parse
  | "*/"   { if lvl <= 1 then token lexbuf else comment (lvl - 1) lexbuf }
  | "/*"   { comment (lvl + 1) lexbuf }
  | '\n'   { Lexing.new_line lexbuf; comment lvl lexbuf }
  | _      { comment lvl lexbuf }
  | eof    { failwith "Lexical error: End of comment missing." }

and linecomment = parse
  | '\n' { Lexing.new_line lexbuf; token lexbuf }
  | eof  { EOF }
  | _    { linecomment lexbuf }

{ }
