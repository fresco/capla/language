%{
    open Bast
    open Berror
    open Types
    open Camlcoq
    open ParserUtils

    let intsize_of_string = function
      | "8" -> I8 | "16" -> I16 | "32" -> I32
      | s -> raise (SyntaxError (Format.sprintf "%s is not a valid int size." s))

    let szvars_of_param_def_list =
      List.map (fun (id, _, _, llsz) -> (id, llsz))

    let perms_of_param_def_list =
      List.map (fun (id, p, _, _) -> (id, p))

    let params_of_param_def_list =
      List.map (fun (id, _, t, _) -> (id, t))

    let make_assign p (l, e) =
      match e with
      | Ecall (idf, args) ->
         let (i, ls) = p in
         begin match ls with
         | [] -> Scall (Some i, idf, args)
         | _  -> Sassign (p, (l, e))
         end
      | _ -> Sassign (p, (l, e))

    let make_function loc name params rtype s =
      let params = List.fold_right (@) params [] in
      let szvars = szvars_of_param_def_list params in
      (intern_string name,
      {
          floc    = loc;
          fparams = params_of_param_def_list params;
          fret    = rtype;
          fbody   = s;
          fszvars = szvars;
          fperms  = perms_of_param_def_list params
      })

    let make_external_function loc name params rtype =
      let params = List.fold_right (@) params [] in
      let szvars = szvars_of_param_def_list params in
      (intern_string name,
      {
          eloc    = loc;
          eparams = params_of_param_def_list params;
          eret    = rtype;
          eszvars = szvars;
          eperms  = perms_of_param_def_list params
      })

    let make_loop loc blkid loop body =
      let blk_loop   = intern_string blkid in
      let blk_inloop = intern_string (blkid ^ "_loop_in") in
      loc, Sblock (blk_loop, loop (loc, Sblock (blk_inloop, body)))
%}

(* operators *)
%token EQUAL NE LT GT LE GE
%token SHL SHR
%token OR AND XOR BAND BOR
%token PLUS MINUS STAR DIV MOD
%token NOT TILDE
(* parenthesis, square brackets and curly brackets *)
%token LPAR RPAR
%token LSQ RSQ LCU RCU
(* identifiers, types and values *)
%token <string> IDENT
%token <Types.typ> TYPE
%token <bool> BOOL
%token <string> INT
%token <string * string * Types.signedness> ANNOT_INT
%token <Bast.float_info> FLOAT
%token <Bast.float_info * Types.floatsize> ANNOT_FLOAT
(* keywords *)
%token LET FUN FOR DECR STEP WHILE IF ELSE RETURN EXTERN RANGE
%token CONTINUE BREAK EXIT SKIP ASSERT ERROR
(* other tokens *)
%token ASSIGN COMMA SEMICOLON COLON EOF RIGHTARROW
(* permissions *)
%token MUTABLE OWNED
(* builtin functions *)
%token ALLOC FREE

%nonassoc IF
%nonassoc ELSE

(* operators associativity *)
%left OR
%left XOR
%left AND
%left PLUS MINUS
%left STAR DIV MOD
%left SHL SHR

%on_error_reduce atom_stmt expr

%start <Bast.program> prog
%type <Bast.ident * Bast.fundef> fundef
%type <Bast.ident * Bast.fct> fct
%type <(Bast.ident * Syntax.permission * Types.typ * Bast.lexpr list list) list> param
%type <Types.typ * Bast.lexpr list list> typ
%type <Bast.lstmt> stmt
%type <Bast.init> init
%type <Bast.ident> fresh
%type <Bast.ident list> nonempty_list(fresh)
%type <(Berror.loc * (Bast.ident * Types.typ option * Bast.lexpr list list * Bast.init option)) list> var_decl
%type <(Berror.loc * (Bast.ident * Types.typ option * Bast.lexpr list list * Bast.init option)) list> var_decls
%type <Bast.lstmt> atom_stmt
%type <Bast.lstmt> goto_stmt
%type <Bast.lstmt> structure
%type <unit> start_block
%type <unit> end_block
%type <Bast.lstmt> block
%type <bool> decr
%type <Bast.lexpr> atom
%type <Bast.syn_path> syn_path
%type <Bast.syn_path_elem> syn_path_elem
%type <Bast.lexpr> expr
%type <Bast.lexpr> bor_expr
%type <Bast.lexpr> band_expr
%type <Bast.lexpr> bool_expr
%type <Bast.lexpr> arith_expr
%type <Bast.binop> binrel
%type <(Bast.ident * Syntax.permission * Types.typ * Bast.lexpr list list) list list> separated_list(param,COMMA)
%type <Bast.lexpr list> separated_list(expr,COMMA)
%type <Bast.lexpr list> separated_list(arith_expr,COMMA)
%type <Bast.syn_path_elem list> list(syn_path_elem)
%type <(Bast.ident * Bast.fundef) list> list(fundef)
%type <string list> list(IDENT)
%type <string option> option(IDENT)

%%

separated_list(X, SEP):
| { [] }
| x = X { [x] }
| x = X; SEP; l = separated_list(X, SEP) { x :: l }
;

list(X):
| { [] }
| x = X; l = list(X) { x :: l }
;

option(X):
| { None }
| x = X { Some x }
;

nonempty_list(X):
| x = X { [x] }
| x = X; l = nonempty_list(X) { x :: l }
;

prog:
  fdlist = list(fundef); EOF
    { let id_main = intern_string "main" in
      let loc = loc_of_lexing_pos $startpos $endpos in
      { ploc = loc; main = id_main; fundefs = fdlist } }
;

fundef:
| f = fct { let (id, f) = f in (id, Internal f) }
| EXTERN; FUN; name = IDENT; LPAR; params = separated_list(param, COMMA); RPAR; RIGHTARROW; rtype = TYPE
    { let (id, f) =
        make_external_function (loc_of_lexing_pos $startpos $endpos) name params rtype in
      (id, External f)
    }
;

fct:
| FUN; name = IDENT; LPAR; params = separated_list(param, COMMA); RPAR;
  RIGHTARROW; rtype = TYPE; start_block; s = stmt; end_block {
    make_function (loc_of_lexing_pos $startpos $endpos) name params rtype s
}
| FUN; name = IDENT; LPAR; params = separated_list(param, COMMA); RPAR;
  start_block; s = stmt; end_block {
    make_function (loc_of_lexing_pos $startpos $endpos) name params Types.Tvoid s
}
;

param:
| ids = list(IDENT); COLON; t = typ
  { let t, llsz = t in
    List.map (fun id -> (get_ident id, Syntax.Shared, t, llsz)) ids }
| ids = list(IDENT); COLON; MUTABLE; t = typ
  { let t, llsz = t in
    List.map (fun id -> (get_ident id, Syntax.Mutable, t, llsz)) ids }
| ids = list(IDENT); COLON; OWNED; t = typ
  { let t, llsz = t in
    List.map (fun id -> (get_ident id, Syntax.Owned, t, llsz)) ids }
;

typ:
| LSQ; t = typ; SEMICOLON; lsz = separated_list (arith_expr, COMMA); RSQ
  { let t, llsz = t in (Tarr t, lsz :: llsz) }
| t = TYPE { t, [] }
;

stmt:
| a = atom_stmt; SEMICOLON { a }
| s = structure { s }
| s1 = structure; s2 = stmt
  { loc_of_lexing_pos $startpos $endpos, Sseq (s1, s2) }
| s1 = atom_stmt; SEMICOLON; s2 = stmt
  { loc_of_lexing_pos $startpos $endpos, Sseq (s1, s2) }
;

init:
| e = expr { Expr e }
| ALLOC    { Alloc  }
;

var_decl:
| id = fresh; lids = nonempty_list(fresh); COLON; t = typ
  { let loc = loc_of_lexing_pos $startpos $endpos in
    List.map (fun i -> loc, (i, Some (fst t), snd t, None)) (id :: lids) }
| id = fresh; COLON; t = typ
  { let loc = loc_of_lexing_pos $startpos $endpos in
    [ loc, (id, Some (fst t), snd t, None) ] }
| id = fresh; COLON; t = typ; ASSIGN; e = init
  { let loc = loc_of_lexing_pos $startpos $endpos in
    [ loc, (id, Some (fst t), snd t, Some e) ] }
| id = fresh; ASSIGN; e = init
  { let loc = loc_of_lexing_pos $startpos $endpos in
    [ loc, (id, None, [], Some e) ] }
;

var_decls:
| d = var_decl { d }
| d = var_decl; COMMA; ld = var_decls { d @ ld }
;

atom_stmt:
(* skip *)
| SKIP { loc_of_lexing_pos $startpos $endpos, Sskip }
(* declarations *)
| LET; decls = var_decls
  { let loc = loc_of_lexing_pos $startpos $endpos in
    List.fold_left (fun s (l, (i, t, llsz, e)) ->
      match e with
      | None -> loc, Sseq ((l, Sdecl (i, t, llsz)), s)
      | Some e ->
        let init = match e with
          | Expr e -> make_assign (i, []) e
          | Alloc -> Salloc i in
        loc, Sseq ((l, Sseq (
              (l, Sdecl (i, t, llsz)),
              (l, init)
        )), s)
    ) (loc, Sskip) decls }
(* assignation *)
| s = syn_path; ASSIGN; e = expr
  { let (i, ls) = s in
    (loc_of_lexing_pos $startpos $endpos,
     make_assign (i, ls) e) }
(* assertion *)
| ASSERT; e = expr { loc_of_lexing_pos $startpos $endpos, Sassert e }
(* alloc *)
| s = syn_path; ASSIGN; ALLOC {
    let loc = loc_of_lexing_pos $startpos $endpos in
    let (i, s) = s in
    if s <> [] then
      let f = Diagnostics.file_loc loc.loc_file in
      Diagnostics.fatal_error f "Syntax Error: @[line %d, column %d@]@."
                              loc.loc_lnum loc.loc_cnum
    else ();
    loc, Salloc i
}
(* free *)
| FREE; i = IDENT {
    loc_of_lexing_pos $startpos $endpos, Sfree (get_ident i)
}
(* call *)
| idf = IDENT; LPAR; args = separated_list(expr, COMMA); RPAR
  { let loc = loc_of_lexing_pos $startpos $endpos in
    loc, Scall (None, intern_string idf, args) }
| s = goto_stmt { s }
;

goto_stmt:
(* error *)
| ERROR { loc_of_lexing_pos $startpos $endpos, Serror }
(* return *)
| RETURN; e = expr { loc_of_lexing_pos $startpos $endpos, Sreturn (Some e) }
| RETURN { loc_of_lexing_pos $startpos $endpos, Sreturn None }
(* continue *)
| CONTINUE; blkid = option(IDENT)
  { loc_of_lexing_pos $startpos $endpos,
    Scontinue (Option.map intern_string blkid) }
(* break *)
| BREAK; blkid = option(IDENT)
  { loc_of_lexing_pos $startpos $endpos,
    Sbreak (Option.map intern_string blkid) }
(* exit *)
| EXIT; blkid = IDENT
  { loc_of_lexing_pos $startpos $endpos,
    Sexit (intern_string blkid) }

structure:
(* condition *)
| IF; c = expr; strue = block; ELSE sfalse = block
  { loc_of_lexing_pos $startpos $endpos, Sifthenelse (c, strue, sfalse) }
| IF; c = expr; strue = block
  { let loc = loc_of_lexing_pos $startpos $endpos in
    loc, Sifthenelse (c, strue, (loc, Sskip)) }
(* for loop *)
| blkid = option(IDENT); FOR; b = decr; id = fresh; COLON; t = TYPE; ASSIGN; lo = arith_expr; RANGE; hi = arith_expr; s = block
  { let loc = loc_of_lexing_pos $startpos $endpos in
    let blkid = match blkid with None -> "__loop" | Some id -> id in
    make_loop loc blkid (fun s -> loc, Sfor (id, b, Some t, lo, hi, s)) s }
| blkid = option(IDENT); FOR; b = decr; id = fresh; ASSIGN; lo = arith_expr; RANGE; hi = arith_expr; s = block
  { let loc = loc_of_lexing_pos $startpos $endpos in
    let blkid = match blkid with None -> "__loop" | Some id -> id in
    make_loop loc blkid (fun s -> loc, Sfor (id, b, None, lo, hi, s)) s }
| blkid = option(IDENT); FOR; b = decr; id = fresh; COLON; t = TYPE; ASSIGN; lo = arith_expr; RANGE; hi = arith_expr; STEP; step = arith_expr; s = block
  { let loc = loc_of_lexing_pos $startpos $endpos in
    let blkid = match blkid with None -> "__loop" | Some id -> id in
    make_loop loc blkid (fun s -> loc, Sfor_step (id, b, Some t, lo, hi, step, s)) s }
| blkid = option(IDENT); FOR; b = decr; id = fresh; ASSIGN; lo = arith_expr; RANGE; hi = arith_expr; STEP; step = arith_expr; s = block
  { let loc = loc_of_lexing_pos $startpos $endpos in
    let blkid = match blkid with None -> "__loop" | Some id -> id in
    make_loop loc blkid (fun s -> loc, Sfor_step (id, b, None, lo, hi, step, s)) s }
(* while *)
| blkid = option(IDENT); WHILE; c = expr; s = block
  { let loc = loc_of_lexing_pos $startpos $endpos in
    let blkid = match blkid with None -> "__loop" | Some id -> id in
    make_loop loc blkid (fun s -> loc, Swhile (c, s)) s }
(* block *)
| blkid = IDENT; COLON; s = block
  { loc_of_lexing_pos $startpos $endpos,
    Sblock (intern_string blkid, s) }
;

fresh:
| id = IDENT { fresh_ident id }

start_block:
| LCU { open_scope() }
;

end_block:
| RCU { close_scope() }
;

block:
| a = goto_stmt; SEMICOLON { a }
| s = structure { s }
| start_block; s = stmt; end_block { s }
;

decr:
| { false }
| DECR { true}
;

atom:
| b = BOOL { loc_of_lexing_pos $startpos $endpos, Econst_bool b }
| n = INT  { loc_of_lexing_pos $startpos $endpos, Econst_int n  }
| n = ANNOT_INT
  { let loc = loc_of_lexing_pos $startpos $endpos in
    let n, sz, sg = n in
    match sz with
    | "64" -> loc, Econst_annot_int64 (sg, n)
    | _    -> loc, Econst_annot_int (intsize_of_string sz, sg, n) }
| f = FLOAT { loc_of_lexing_pos $startpos $endpos, Econst_float f }
| f = ANNOT_FLOAT
  { let loc = loc_of_lexing_pos $startpos $endpos in
    let f, sz = f in
    loc, Econst_annot_float (sz, f) }
| LPAR; t = TYPE; RPAR; e = atom
  { loc_of_lexing_pos $startpos $endpos, Ecast  (e, t) }
| LPAR; e = expr; RPAR { e }
| s = syn_path { loc_of_lexing_pos $startpos $endpos, let (i, ls) = s in Eacc (i, ls) }
| idf = IDENT; LPAR; args = separated_list(expr, COMMA); RPAR
  { let loc = loc_of_lexing_pos $startpos $endpos in
    loc, Ecall (intern_string idf, args) }
;

syn_path:
| id = IDENT; ls = list(syn_path_elem)
  { (get_ident id, ls) }
;

syn_path_elem:
| LSQ; lidx = separated_list(arith_expr, COMMA); RSQ
  { Scell lidx }
;

expr:
| bor_expr | band_expr | bool_expr { $1 }

bor_expr:
| e1 = bool_expr BOR e2 = bool_expr
| e1 = bor_expr BOR e2 = bool_expr
    { loc_of_lexing_pos $startpos $endpos, Ebinop (Obor, e1, e2) }

band_expr:
| e1 = bool_expr BAND e2 = bool_expr
| e1 = band_expr BAND e2 = bool_expr
    { loc_of_lexing_pos $startpos $endpos, Ebinop (Oband, e1, e2) }

bool_expr:
| a = arith_expr { a }
| NOT; e = atom
  { loc_of_lexing_pos $startpos $endpos, Eunop (Onotbool, e) }
| e1 = arith_expr; op = binrel; e2 = arith_expr
  { loc_of_lexing_pos $startpos $endpos, Ebinop (op, e1, e2) }

arith_expr:
| a = atom { a }
| op = unop; e = atom
  { loc_of_lexing_pos $startpos $endpos, Eunop  (op, e) }
| e1 = arith_expr; op = binop; e2 = arith_expr
  { loc_of_lexing_pos $startpos $endpos, Ebinop (op, e1, e2) }

binrel:
| EQUAL { Oeq   }
| NE    { One   }
| LT    { Olt   }
| LE    { Ole   }
| GT    { Ogt   }
| GE    { Oge   }

%inline binop:
| PLUS  { Oadd  }
| MINUS { Osub  }
| STAR  { Omul  }
| DIV   { Odiv  }
| MOD   { Omod  }
| AND   { Oand  }
| OR    { Oor   }
| XOR   { Oxor  }
| SHL   { Oshl  }
| SHR   { Oshr  }
;

%inline unop:
| MINUS { Oneg     }
| TILDE { Onotint  }
;
