fun add_matrix(a: [i64; m, n], b: [i64; m, n], dest: mut [i64; m, n], m: u64, n: u64) {
  for i = 0 .. m {
    for j = 0 .. n {
      dest[i, j] = a[i, j] + b[i, j];
    }
  }
}
