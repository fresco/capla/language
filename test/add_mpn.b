// fun mpn_add_1 (rp: mut [u64; n], ap: [u64; n], n b: u64) -> bool {
//   assert n > 0;

//   for i: u64 = 0 .. n {
//     let r : u64 = ap[i] + b;
//     b = (u64) (r < b);
//     rp[i] = r;
//   }

//   return (bool) b;
// }

fun mpn_add_n_b (rp: mut [u64; n], ap bp: [u64; n], n: u64) -> u64 {
  let cy: u8 = 0;
  let a b r: u64;

  for i: u64 = 0 .. n {
    a = ap[i]; b = bp[i];
    r = a + (u64) cy;
    cy = (u8) (r < (u64) cy);
    r = r + b;
    cy = (u8) (cy + (u8) (r < b));
    rp[i] = r;
  }

  return (u64) cy;
}