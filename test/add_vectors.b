fun add_vectors(a: [i64; n], b: [i64; n], dest: mut [i64; n], n: u64) {
    for i: u64 = 0 .. n {
        dest[i] = a[i] + b[i];
    }
}
