fun test(n: u64, x: i64) -> i64 {
    let t: [i64; n] = alloc;
    t[5] = x;
    // free t;
    test2(t, n);
    return t[5];
}

fun test2(t: mut [i64; n], n: u64) {
    t[5] = 2;
}

fun test3(t: [i64; n], n: u64) -> bool {
    return t[5] << 2 < 10;
}