fun min(a b: u64) -> u64 {
  if a < b { return a; } else { return b; }
}

fun block_mul_matrix(a: [i64; m, n], b: [i64; n, p], dest: mut [i64; m, p],
                     m n p bs: u64) {
  let s: i64 = 0;
  for I: u64 = 0 .. m step bs {
    let Imax: u64 = min(m, I + bs);
    for J: u64 = 0 .. p step bs {
      let Jmax: u64 = min(p, J + bs);
      for K: u64 = 0 .. n step bs {
        let Kmax: u64 = min(n, K + bs);
        for i: u64 = I .. Imax {
          for j: u64 = J .. Jmax {
            s = 0;
            for k: u64 = K .. Kmax {
              s = s + a[i, k] * b[k, j];
            }
            dest[i, j] = dest[i, j] + s;
          }
        }
      }
    }
  }
}
