fun eratosthene(prime: mut [bool; N], N: u64) {
  if N < 2 return;
  prime[0u32] = false;
  prime[1u32] = false;
  for k: u64 = 4 .. N step 2 {
    prime[k] = false;
  }
  let i: u64 = 3;
  while (i * i < N) {
    if prime[i] {
      for j: u64 = i .. (N / i + 1) step 2 {
        prime[i * j] = false;
      }
    }
    i = i + 1;
  }
}
