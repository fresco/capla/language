fun fact(n: i32) -> i32 {
  let x: i32 = 1;
  for i: i32 = 0 .. n {
    x = x * n;
  }
  return x;
}