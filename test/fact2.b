fun fact(n: i32) -> i32 {
  let x: i32 = 1;
  while (n > 0) {
    x = x * n;
    n = n - 1;
  }
  return x;
}