fun search_max_abs(A: [f64; n, m], n m: u64, r j: u64) -> i64 {
  let k = -1;
  let max: f64 = 0.;
  for i: u64 = r .. n {
    if A[i, j] > max {
      k = (i64) i;
      max = A[i, j];
    } else if - A[i, j] > max {
      k = (i64) i;
      max = - A[i, j];
    }
  }
  return k;
}

fun divide_line_by_const(A: mut [f64; n, m], n m: u64, i: u64, k: f64) {
  for j: u64 = 0 .. m {
    A[i, j] = A[i, j] / k;
  }
}

fun exchange_lines(A: mut [f64; n, m], n m: u64, l1 l2: u64) {
  let tmp: f64 = 0.;
  for j: u64 = 0 .. m {
    tmp = A[l1, j];
    A[l1, j] = A[l2, j];
    A[l2, j] = tmp;
  }
}

fun add_lines(A: mut [f64; n, m], n m: u64, l1 l2: u64, k: f64) {
  for j: u64 = 0 .. m {
    A[l1, j] = A[l1, j] + k * A[l2, j];
  }
}

fun gauss(A: mut [f64; n, m], n m: u64) {
  let r: u64 = 0;
  for j: u64 = 0 .. m {
    let k: i64 = search_max_abs(A, n, m, r, j);
    if (k == -1) break;
    if A[k, j] != 0. {
      divide_line_by_const(A, n, m, (u64) k, A[k, j]);
      if (u64) k != r {
        exchange_lines(A, n, m, (u64) k, r);
      }
      for i: u64 = 0 .. n {
        if i != r {
          add_lines(A, n, m, i, j, - A[i, j]);
        }
      }
      r = r + 1;
    }
  }
}
