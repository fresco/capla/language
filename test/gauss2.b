fun search_max_abs(N: u64, n: u32, m: u32, A: [f64; N], r: u32, j: u32) -> i32 {
  let k: i32 = -1i32;
  let max: f64 = 0.;
  for i: u32 = r .. n {
    if A[i * m + j] > max {
      k = (i32) i;
      max = A[i * m + j];
    } else if - A[i * m + j] > max {
      k = (i32) i;
      max = - A[i * m + j];
    }
  }
  return k;
}

fun divide_line_by_const(N: u64, m: u32, A: mut [f64; N], i: u32, k: f64) {
  for j: u32 = 0 .. m {
    A[i * m + j] = A[i * m + j] / k;
  }
}

fun exchange_lines(N: u64, m: u32, A: mut [f64; N], l1: u32, l2: u32) {
  let tmp: f64 = 0.;
  for j: u32 = 0 .. m {
    tmp = A[l1 * m + j];
    A[l1 * m + j] = A[l2 * m + j];
    A[l2 * m + j] = tmp;
  }
}

fun add_lines(N: u64, m: u32, A: mut [f64; N], l1: u32, k: f64, l2: u32) {
  for j: u32 = 0 .. m {
    A[l1 * m + j] = A[l1 * m + j] + k * A[l2 * m + j];
  }
}

fun gauss(N: u64, n: u32, m: u32, A: mut [f64; N]) {
  let r: u32 = 0;
  let c: f64 = 0.;
  for j: u32 = 0 .. m {
    let x: i32 = search_max_abs(N, n, m, A, r, j);
    if (x == -1i32) { break; }
    let k: u32 = (u32) x;
    if A[k * m + j] != 0. {
      c = A[k * m + j];
      divide_line_by_const(N, m, A, k, c);
      if k != r {
        exchange_lines(N, m, A, k, r);
      }
      for i: u32 = 0 .. n {
        if i != r {
          c = - A[i * m + j];
          add_lines(N, m, A, i, c, j);
        }
      }
      r = r + 1;
    }
  }
}
