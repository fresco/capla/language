fun f(a: [i32; n], b: [i32; n], n: u64) -> i32 {
  return (i32) (a[0u32] < b[0u32] || (a[0u32] == b[0u32] && a[1u32] < b[1u32]));
}