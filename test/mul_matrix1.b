fun mul_matrix(a: [i64; q], b: [i64; q], dest: mut [i64; q], m n p q: u64) {
  for i: u64 = 0 .. m {
    for j: u64 = 0 .. m {
      dest[i * m + j] = 0;
      for k: u64 = 0 .. m {
        dest[i * m + j] = dest[i * m + j] + a[i * m + k] * b[k * m + j];
      }
    }
  }
}
