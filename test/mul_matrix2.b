extern fun print_matrix(a: [i64; m, n], m n: u64) -> void
extern fun print_line() -> void

fun mul_matrix(a: [i64; m, n], b: [i64; n, p], dest: mut [i64; m, p], m n p: u64) {
  for i: u64 = 0 .. m
    for j: u64 = 0 .. p {
      dest[i, j] = 0;
      for k: u64 = 0 .. n {
        dest[i, j] = dest[i, j] + a[i, k] * b[k, j];
      }
    }
}
