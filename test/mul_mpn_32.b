/* 32-bit version of GMP's multiplication functions */

fun mpn_mul_1 (rp: mut [u32; n], up: [u32; n], n: u64, vl: u32) -> u32 {
    let cl hpl lpl: u32;
    let pl: u64;
    assert n >= 1;

    cl = 0;
    for i: u64 = 0 .. n {
        pl = (u64) up[i] * (u64) vl;
        lpl = (u32) pl;
        hpl = (u32) (pl >> 32);
        
        lpl = lpl + cl;
        cl = (u32) (lpl < cl) + hpl;
        
        rp[i] = lpl;
    }

    return cl;
}

fun mpn_addmul_1 (rp: mut [u32; n], up: [u32; n], n: u64, vl: u32) -> u32 {
    let cl hpl lpl rl: u32;
    let pl: u64;
    assert n >= 1;

    cl = 0;
    for i: u64 = 0 .. n {
        pl = (u64) up[i] * (u64) vl;
        lpl = (u32) pl;
        hpl = (u32) (pl >> 32);

        lpl = lpl + cl;
        cl = (u32) (lpl < cl) + hpl;

        rl = rp[i];
        lpl = rl + lpl;
        cl = cl + (u32) (lpl < rl);
        rp[i] = lpl;
    }

    return cl;
}

fun mpn_submul_1 (rp: mut [u32; n], up: [u32; n], n: u64, vl: u32) -> u32 {
    let cl hpl lpl rl: u32;
    let pl: u64;
    assert n >= 1;

    cl = 0;
    for i: u64 = 0 .. n {
        pl = (u64) up[i] * (u64) vl;
        lpl = (u32) pl;
        hpl = (u32) (pl >> 32);

        lpl = lpl + cl;
        cl = (u32) (lpl < cl) + hpl;

        rl = rp[i];
        lpl = rl - lpl;
        cl = cl + (u32) (lpl > rl);
        rp[i] = lpl;
    }

    return cl;
}

fun mpn_mul (rp: mut [u32; un + vn], up: [u32; un], un: u64, vp: [u32; vn], vn: u64) -> u32 {
    assert un >= vn;
    assert vn >= 1;
    rp[un] = mpn_mul_1(rp, up, un, vp[0]);

    for decr i: u64 = (vn - 1) .. 0 {
        /* ... */ skip;
    }

    return 0;
}