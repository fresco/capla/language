fun mul_poly(a: [i64; m], b: [i64; n], dest: mut [i64; m + n - 1], m n: u64) {
    for i: u64 = 0 .. m
        for j: u64 = 0 .. n
            dest[i + j] = dest[i + j] + a[i] * b[j];
}