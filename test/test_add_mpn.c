#include <stdio.h>
#include <stdlib.h>
#include <time.h>

// #define USEGMP
#ifdef USEGMP
#include <gmp.h>
#define GMPNAME "GMP"
#define GMPNAMETAB "\t"
#else
#include "../../../gmp/mini-gmp/mini-gmp.h"
#define GMPNAME "MINI-GMP"
#define GMPNAMETAB ""
void mpn_random(mp_limb_t* t, mp_limb_t n) {
  for (mp_limb_t i = 0; i < n; i++) {
    t[i] = ((mp_limb_t) rand() << 32) + rand();
  }
}
#endif


// extern void mpn_random(mp_limb_t*, mp_limb_t);
extern void mpn_add_n_b(mp_limb_t*, mp_limb_t*, mp_limb_t*, mp_limb_t);

void print_number(mp_limb_t N, mp_limb_t* x) {
  printf("[ ");
  for (mp_limb_t i = 0; i < N; i++)
    printf("%lx ", x[i]);
  printf("]\n");
}

#define ITER 1000000

int main() {
  srand(2);
  // srand(time(NULL));

  mp_limb_t N = 1000;
  clock_t t1, t2;

  mp_limb_t* x = malloc(N * sizeof(mp_limb_t));
  mp_limb_t* y = malloc(N * sizeof(mp_limb_t));

  mpn_random(x, N);

  t1 = clock();
  for (int i = 0; i < ITER; i++)
    mpn_add_n(y, x, x, N);
  t2 = clock();

  printf("%s:\t%s", GMPNAME, GMPNAMETAB);
  print_number(9, y);
  printf("%f s\n", (double)(t2 - t1) / (double)CLOCKS_PER_SEC);

  t1 = clock();
  for (int i = 0; i < ITER; i++)
    mpn_add_n_b(y, x, x, N);
  t2 = clock();

  printf("B program:\t");
  print_number(9, y);
  printf("%f s\n", (double)(t2 - t1) / (double)CLOCKS_PER_SEC);

  return 0;
}
