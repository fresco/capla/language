#include <stdio.h>
#include <assert.h>

#define ull unsigned long long

extern unsigned long search_max_abs(double*, ull, ull, ull, ull);
extern void gauss(double*, ull, ull);

void print_matrix(double* A, ull n, ull m) {
  assert(m > 0);
  for (ull i = 0; i < n; i++) {
    printf("[");
    for (ull j = 0; j < m - 1; j++)
      printf("%9.6g ", A[i * m + j]);
    printf("%9.6g]\n", A[i * m + (m - 1)]);
  }
}

int main() {
  double A1[] = { 8.0, 2.5, 3.0, 0.1,
                  3.1, 6.7, 2.2, 7.2,
                  4.0, 0.0, 9.4, 3.1 };

  double A2[] = { 8.0, 3.1, 4.0,
                  2.5, 6.7, 0.0,
                  3.0, 2.2, 9.4,
                  0.1, 7.2, 3.1 };

  double A3[] = {  2., -1.,  0.,
                  -1.,  2., -1.,
                   0., -1.,  2. };

  double A4[] = {  2., -1.,  0.,
                  -1.,  2., -1.,
                   0.,  3., -2. };

  print_matrix(A1, 3, 4);
  gauss(A1, 3, 4);
  printf("--\n");
  print_matrix(A1, 3, 4);
  printf("-------------------------------------\n");

  print_matrix(A2, 4, 3);
  gauss(A2, 4, 3);
  printf("--\n");
  print_matrix(A2, 4, 3);
  printf("-------------------------------------\n");

  print_matrix(A3, 3, 3);
  gauss(A3, 3, 3);
  printf("--\n");
  print_matrix(A3, 3, 3);
  printf("-------------------------------------\n");

  print_matrix(A4, 3, 3);
  gauss(A4, 3, 3);
  printf("--\n");
  print_matrix(A4, 3, 3);
  printf("-------------------------------------\n");

  return 0;
}
