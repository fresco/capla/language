#include <stdio.h>

typedef long long int ll;

extern void mul_poly(const ll*, const ll*, ll*, ll, ll);

int main() {
    ll a[] = {2, 3, 5};
    ll b[] = {1, 8, 2};
    ll dest[] = {0, 0, 0, 0, 0};
    mul_poly(a, b, dest, 3, 3);
    for (int i = 0; i < 5; i++)
        printf("%d ", dest[i]);
    printf("\n");
    return 0;
}