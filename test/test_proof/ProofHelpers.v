Require Import ssreflect ssrbool ssrfun.
Require Import BinNums ZArith String List Lia.

Import ListNotations.

From compcert Require Import Integers Floats Maps Coqlib Errors.

From compcert Require Import Tactics BUtils.
From compcert Require Import Syntax BValues BEnv Types.
From compcert Require Import Validity Alias SemPath Ops.
From compcert Require Import SemanticsCommon SemanticsNonBlocking.
From compcert Require ExprSem.
Module ES := ExprSem.
From compcert Require Import NBfacts BigStepNBSemantics.

Arguments PTree.get: simpl nomatch.
Arguments PTree.set: simpl nomatch.
Arguments PTree.empty: simpl nomatch.
(* Transparent get_env_path get_value_path. *)
Opaque nth_error.

Ltac compute_maxint :=
  unfold Int64.max_unsigned, Int64.modulus, Int64.wordsize, Wordsize_64.wordsize;
  unfold Int.max_unsigned, Int.modulus, Int.wordsize, Wordsize_32.wordsize;
  cbn.

Section EXPR_PATH_INV.

  (* Here we follow the ideas from the paper:
      Handcrafted Inversions Made Operational on Operational Semantics
      by Jean-François Monin, Xiaomu Shi *)

  Variable e: env.
  Variable f: function.

  Definition eval_identlist_inv {lids lv} (H: eval_identlist e f lids lv) :=
    let diag lids lv :=
      match lids with
      | [] => forall (X: list value -> Prop), X ([]: list value) -> X lv
      | i :: lids => forall (X: list value -> Prop),
        (forall v lv,
          e!i = Some v ->
          eval_identlist e f lids lv -> X (v :: lv)) ->
        X lv
      end in
    match H in (eval_identlist _ _ lids lv) return diag lids lv with
    | eval_ident_Enil _ _ => (fun X k => k)
    | eval_ident_Econs _ _ i v lids lv Ei Si Elids => (fun X k => k v lv Ei Elids)
    end.

  Definition eval_expr_Some_inv {exp v} (H: eval_expr e f exp (Some v)) :=
    let diag exp v :=
      match exp with
      | Econst (Cint sz sig n) => forall (X: value -> Prop), X (Vint n) -> X v
      | Econst (Cint64  sig n) => forall (X: value -> Prop), X (Vint64 n) -> X v
      | Econst (Cfloat32 f) => forall (X: value -> Prop), X (Vfloat32 f) -> X v
      | Econst (Cfloat64 f) => forall (X: value -> Prop), X (Vfloat64 f) -> X v
      | Econst (Cbool b) => forall (X: value -> Prop), X (Vbool b) -> X v
      | Ecast exp t1 t2 => forall (X: value -> Prop),
        (forall v1 v2,
          eval_expr e f exp (Some v1) ->
          sem_cast v1 t1 t2 = Some v2 ->
          X v2) -> X v
      | Eunop op k exp => forall (X: value -> Prop),
        (forall v1 v,
          eval_expr e f exp (Some v1) ->
          sem_unop op k v1 = Some v ->
          X v) -> X v
      | Ebinop_arith op k e1 e2 => forall (X: value -> Prop),
        (forall v1 v2 v,
          eval_expr e f e1 (Some v1) ->
          eval_expr e f e2 (Some v2) ->
          sem_binarith_operation op k v1 v2 = Some v ->
          X v) -> X v
      | Ebinop_cmp op k e1 e2 => forall (X: value -> Prop),
        (forall v1 v2 v,
          eval_expr e f e1 (Some v1) ->
          eval_expr e f e2 (Some v2) ->
          sem_cmp_operation op k v1 v2 = Some v ->
          X v) -> X v
      | Ebinop_cmpu op k e1 e2 => forall (X: value -> Prop),
        (forall v1 v2 v,
          eval_expr e f e1 (Some v1) ->
          eval_expr e f e2 (Some v2) ->
          sem_cmpu_operation op k v1 v2 = Some v ->
          X v) -> X v
      | Eacc p => forall (X: value -> Prop),
        (forall p' v,
          eval_path e f p (Some p') ->
          get_env_path e p' = Some v ->
          X v) -> X v
      end in
    match H in eval_expr _ _ exp (Some v) return diag exp v with
    | eval_Const_int _ _ _ _ n => (fun X k => k)
    | eval_Const_int64 _ _ _ n => (fun X k => k)
    | eval_Const_float32 _ _ f => (fun X k => k)
    | eval_Const_float64 _ _ f => (fun X k => k)
    | eval_Const_bool _ _ b => (fun X k => k)
    | eval_Cast _ _ _ _ _ v1 v2 Eexp H => (fun X k => k v1 v2 Eexp H)
    | eval_Unop _ _ _ _ _ v1 v Eexp H => (fun X k => k v1 v Eexp H)
    | eval_Binop_arith _ _ _ _ _ _ v1 v2 v E1 E2 OP => (fun X k => k v1 v2 v E1 E2 OP)
    | eval_Binop_cmp _ _ _ _ _ _ v1 v2 v E1 E2 OP => (fun X k => k v1 v2 v E1 E2 OP)
    | eval_Binop_cmpu _ _ _ _ _ _ v1 v2 v E1 E2 OP => (fun X k => k v1 v2 v E1 E2 OP)
    | eval_Access _ _ _ p' v EVp Ep _ => (fun X k => k p' v EVp Ep)
    end.

  Definition eval_exprlist_Some_inv {le lv} (H: eval_exprlist e f le (Some lv)) :=
    let diag le lv :=
      match le with
      | [] => forall (X: list value -> Prop), X [] -> X lv
      | exp :: le => forall (X: list value -> Prop),
        (forall v lv,
          eval_expr e f exp (Some v) ->
          eval_exprlist e f le (Some lv) ->
          X (v :: lv)) -> X lv
      end in
    match H in eval_exprlist _ _ le (Some lv) return diag le lv with
    | eval_Enil _ _ => (fun X k => k)
    | eval_Econs_Some_e _ _ _ v _ lv Eexp Ele => (fun X k => k v lv Eexp Ele)
    | _ => I
    end.

  Definition eval_path_elem_list_Some_inv {lsyn llsz lsem}
      (H: eval_path_elem_list e f lsyn llsz lsem) :=
    let diag lsyn llsz lsem :=
      match lsyn, llsz with
      | [], _ => forall (X: list sem_path_elem -> Prop), X [] -> X lsem
      | Scell lidx :: lsyn, lsz :: llsz => forall (X: list sem_path_elem -> Prop),
        (forall lvsz shape lvidx idx lsem,
          ES.eval_ident_list e f lsz = ES.Val lvsz ->
          natlist_of_Vint64 lvsz = Some shape ->
          NB.eval_exprlist e f lidx (Some lvidx) ->
          build_index lvidx shape = Some idx ->
          ES.valid_index lvidx lvsz ->
          NB.eval_path_elem_list e f lsyn llsz (Some lsem) ->
          X (Pcell idx :: lsem)) ->
        X lsem
      | _, _ => forall (X: Prop), X
      end in
    match H in eval_path_elem_list _ _ lsyn llsz (Some lsem)
      return diag lsyn llsz lsem with
    | eval_path_elem_list_Nil _ _ _ => (fun X k => k)
    | eval_Scell _ _ _ _ _ _ lvsz shape lvidx idx lsem Elsz Shape Elidx Bidx Vidx Elsyn =>
      (fun X k => k lvsz shape lvidx idx lsem
        (proj1 (eval_identlist_fixpoint_match _ _ _ _) Elsz) Shape Elidx Bidx Vidx Elsyn)
    end.

  Definition eval_path_Some_inv {p p'} (H: eval_path e f p (Some p')) :=
    let diag (p: syn_path) p' :=
      let (i, lsyn) := p in
      forall (X: sem_path -> Prop),
        (forall llsz lsem,
          (fn_szenv' f)!i = Some llsz ->
          NB.eval_path_elem_list e f lsyn llsz (Some lsem) ->
          X (i, lsem)) ->
        X p' in
    match H in eval_path _ _ p (Some p') return diag p p' with
    | eval_path_intro _ _ i lsyn llsz lsem Si Elsyn =>
      (fun X k => k llsz lsem Si Elsyn)
    end.

  Definition eval_full_path_Some_inv {p p'} (H: eval_full_path e f p (Some p')) :=
    let diag (p: syn_path) p' :=
      let (i, lsyn) := p in
      forall (X: sem_path -> Prop),
        (forall llsz lsem,
          (fn_szenv' f)!i = Some llsz ->
          length lsyn = length llsz ->
          NB.eval_path_elem_list e f lsyn llsz (Some lsem) ->
          X (i, lsem)) ->
        X p' in
    match H in eval_full_path _ _ p (Some p') return diag p p' with
    | eval_full_path_intro _ _ i lsyn llsz lsem Si LEN Elsyn =>
      (fun X k => k llsz lsem Si LEN Elsyn)
    end.

End EXPR_PATH_INV.

Section STMT_INV.

Variable prog: program.

Let ge := genv_of_program prog.
Let defaultid := 1%positive.

Definition param (n: nat) (f: function) :=
  nth n (fn_params f) defaultid.

Definition vint64_bind (f: int64 -> value) (v: value) :=
  match v with
  | Vint64 i => f i
  | _ => Vundef
  end.

Inductive eval_funcall_internal_OK_spec f args e'' r : Prop :=
| eval_funcall_internal_OK_spec_intro: forall e e' out
  (OUTR: r = outcome_result_value out)
  (BE: build_func_env (PTree.empty value) f.(fn_params) args = Some e)
  (IE: instantiate_size_vars e f = OK e')
  (BODY: exec_stmt ge (eval_funcall ge) f e' f.(fn_body) e'' out)
  (FOUT: valid_function_outcome out)
  (NOERR: out <> Out_error),
  eval_funcall_internal_OK_spec f args e'' r.

Lemma eval_funcall_internal_OK_inv:
  forall f args e'' r,
    eval_funcall ge (Internal f) args e'' r ->
    eval_funcall_internal_OK_spec f args e'' r.
Proof. inversion 1 => //; subst; econstructor => //; eassumption. Qed.

Inductive assign_OK_spec f e p exp e' out : Prop :=
| assign_OK_spec_intro: forall p' t v
    (OUT: out = Out_normal)
    (EP: NB.eval_full_path e f p (Some p'))
    (TP: get_tenv_path f.(fn_tenv) p' = Some t)
    (EEXP: NB.eval_expr e f exp (Some v))
    (UE: update_env_path' e p' (shrink t v) = Some e'),
    assign_OK_spec f e p exp e' out.

Lemma assign_OK_inv:
  forall f e p exp e' out,
    exec_stmt ge (eval_funcall ge) f e (Sassign p exp) e' out ->
    out <> Out_error ->
    assign_OK_spec f e p exp e' out.
Proof. inversion 1 => //; subst; intros _; econstructor => //; eassumption. Qed.

End STMT_INV.

Notation "E , F '|-' s '=>' X o" :=
    (exec_stmt ge (eval_funcall ge) F E s X o)
    (at level 200).

Notation "V P <- E" := (Sassign (V, P) E) (at level 200).

Notation "A +i64 B" := (Ebinop_arith Ops.Oadd Ops.OInt64 A B) (at level 200).

Notation "d 'i64'" := (Econst (Cint64 Types.Signed (Int64.repr d))) (at level 200).
Notation "d 'u64'" := (Econst (Cint64 Types.Unsigned (Int64.repr d))) (at level 200).

Notation "A" := (Scell A) (at level 200, only printing).
Notation "V P" := (Eacc (V, P)) (at level 200, only printing).

Ltac sinv T :=
  let H := fresh "H" in
  move/T => H; apply H => {H}.