Require Import ssreflect ssrbool ssrfun.
Require Import BinNums ZArith String List Lia.

Import ListNotations.

From compcert Require Import Integers Floats Maps Coqlib Errors.

From compcert Require Import Tactics BUtils.
From compcert Require Import Syntax BValues BEnv Types.
From compcert Require Import Validity Alias SemPath Ops.
From compcert Require Import SemanticsCommon SemanticsNonBlocking.
From compcert Require ExprSem.
Module ES := ExprSem.
From compcert Require Import NBfacts BigStepNBSemantics.

Require Import ProofHelpers.
Opaque nth_error.

Require Import incr.

Set Search Output Name Only.

Section IncrProof.

Let ge := genv_of_program program.

Theorem incr_spec:
  forall lv e1 vres,
    eval_funcall ge (Internal incr) [Varr lv] e1 vres ->
    get_env_path e1 (param 0 incr, [Pcell 0])
      = omap (vint64_bind (fun i => Vint64 (Int64.add i Int64.one)))
        (get_value_path [Pcell 0] (Varr lv)).
Proof.
  intros *. rewrite/incr/extract_res/make_function.
  repeat match goal with |- context [S _ ?s] => let s' := fresh "V" in set (s' := S _ s) end.
  set incr := {| fn_sig := _ |}.
  case/eval_funcall_internal_OK_inv =>> _ [<-].
  set E0 := (X in Errors.OK X = _) => - [<-].
  (**************************************************)
  move/assign_OK_inv => + /[swap] => /[apply].
  case=> p' t v -> /=; intros.
  move: EP TP UE. sinv eval_full_path_Some_inv => _ > [<-] _.
  sinv eval_path_elem_list_Some_inv => _ _ > [<-] [<-].
  sinv eval_exprlist_Some_inv =>>.
  sinv eval_expr_Some_inv; sinv eval_exprlist_Some_inv => - [<-] _.
  sinv eval_path_elem_list_Some_inv => - [<-].
  move: EEXP; sinv eval_expr_Some_inv =>>.
  sinv eval_expr_Some_inv =>>.
  sinv eval_path_Some_inv => _ > [<-].
  sinv eval_path_elem_list_Some_inv => _ _ > [<-] [<-].
  sinv eval_exprlist_Some_inv =>>.
  sinv eval_expr_Some_inv; sinv eval_exprlist_Some_inv => - [<-] _.
  sinv eval_path_elem_list_Some_inv =>> /=.
  case N: nth_error => [v0|] //= [<-].
  sinv eval_expr_Some_inv. case: v0 N => //= i N [<-].
  case R: replace_nth => [lv'|] //= [<-] /=.
  by rewrite (replace_nth_nth_error_same _ _ _ _ R).
Qed.

End IncrProof.