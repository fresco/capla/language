#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <cblas_64.h>

#define u64 unsigned long long int
#define i32 signed int
#define MOD 100

// #define CBLAS_INT int64_t

extern void zdotu(i32, void*, i32, void*, i32, void*);

void print_int(u64 i) { printf("%llu\n", i); }

struct cplx {
  double x; double y;
};

typedef struct cplx cplx;

cplx* rand_vector(CBLAS_INT N) {
  cplx* x = malloc(sizeof(cplx) * N);
  for (CBLAS_INT i = 0; i < N; i++) {
    x[i].x = rand() % MOD;
    x[i].y = rand() % MOD;
  }
  return x;
}

cplx** vector_cplx_to_ptr(CBLAS_INT N, cplx* x) {
  cplx** y = malloc(sizeof(cplx*) * N);
  for (CBLAS_INT i = 0; i < N; i++) {
    y[i] = malloc(sizeof(cplx));
    y[i]->x = x[i].x;
    y[i]->y = x[i].y;
  }
  return y;
}

void print_vector(CBLAS_INT N, cplx* x) {
  printf("[ ");
  for (CBLAS_INT i = 0; i < N; i++)
    printf("(%g, %g) ", x[i].x, x[i].y);
  printf("]");
}

#define ITER 4000000

int main() {
  /* srand(2); */
  srand(time(NULL));

  CBLAS_INT N = 1000;
  clock_t t1, t2;

  cplx* zx = rand_vector(N);
  cplx* zy = rand_vector(N);

  cplx res; res.x = 0.0; res.y = 0.0;
  cplx res2; res2.x = 0.0; res2.y = 0.0;

  /* printf("Dot product of\n");
     print_vector(N, zx); printf("\nand\n");
     print_vector(N, zy); printf("\n=\n"); */

  t1 = clock();
  for (int i = 0; i < ITER; i++)
    cblas_zdotu_sub_64(N, zx, 1, zy, 1, &res);
  t2 = clock();

  printf("BLAS:\t\t");
  printf("(%g, %g) -- ", res.x, res.y);
  printf("%f s\n", (double)(t2 - t1) / (double)CLOCKS_PER_SEC);

  t1 = clock();
  for (int i = 0; i < ITER; i++)
    zdotu(N, zx, 1, zy, 1, &res2);
  t2 = clock();

  printf("B program:\t");
  printf("(%g, %g) -- ", res2.x, res2.y);
  printf("%f s\n", (double)(t2 - t1) / (double)CLOCKS_PER_SEC);

  return 0;
}
