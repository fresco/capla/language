extern print_int(n: u64) -> void

fun add_mul_cplx(dest: mut [f64; 2], a b: [f64; 2]) {
  dest[0] = dest[0] + a[0] * b[0] - a[1] * b[1];
  dest[1] = dest[1] + a[1] * b[0] + a[0] * b[1];
}

fun zdotu(n: u64, zx zy: [[f64; 2]; n], incx incy: i32, res: mut [f64; 2]) {
  res[0] = 0.; res[1] = 0.;

  if n <= 0 return;

  if incx == 1 && incy == 1 {
    for i: u64 = 0 .. n {
      add_mul_cplx(res, zx[i], zy[i]);
    }
  } else {
    let ix: i32 = 1;
    let iy: i32 = 1;
    if (incx < 0) ix = (-(i32)n+1)*incx + 1;
    if (incy < 0) iy = (-(i32)n+1)*incy + 1;
    for i: u64 = 0 .. n {
      add_mul_cplx(res, zx[ix], zy[iy]);
      ix = ix + incx;
      iy = iy + incy;
    }
  }
}
